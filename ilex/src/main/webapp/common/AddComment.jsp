<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>


<html:html>
<head>
	<title><bean:message bundle = "pm" key = "common.addcommentpage.title"/></title>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css">
	<link rel = "stylesheet" href="styles/newGui.css" type="text/css">  
	<%@ include  file="/NMenu.inc" %>
	<meta http-equiv="X-UA-Compatible" content="IE=9"/>
	
</head>
<%

	int addRow = -1;
	boolean addSpace = false;
	int rowHeight = 120;
	int ownerListSize = 0;
	int j=2;
	int i=0;
	String checkowner=null;

	if(request.getAttribute("ownerListSize") != null)
		ownerListSize = Integer.parseInt(request.getAttribute("ownerListSize").toString());
	rowHeight += ownerListSize*18;

	
	/* if( request.getAttribute( "jobid" ) != null )
	{
		jobid = request.getAttribute( "jobid" ).toString();
		request.setAttribute( "jobid" , jobid );
	}else if(request.getAttribute( "jobid" ) == null && request.getParameter( "Job_Id" ) != null){
		jobid = request.getParameter( "Job_Id" ).toString();
		request.setAttribute( "jobid" , jobid );
	} */
	
	
	String MSA_Id ="";
	String msastatus="";
	String uploadstatus ="";
	String status ="";

	if(request.getAttribute("status")!=null){
		status = request.getAttribute("status").toString();
		msastatus = request.getAttribute("status").toString();
	}
	if(request.getAttribute("MSA_Id")!=null)
		MSA_Id= request.getAttribute("MSA_Id").toString();

	if(request.getAttribute("uploadstatus")!=null)
		uploadstatus= request.getAttribute("uploadstatus").toString();
		
		
	String Appendix_Id ="";
	String appendixStatus ="";
	String latestaddendumid ="";
	String appendixType = "";

	if(request.getAttribute("Appendix_Id")!=null)
		Appendix_Id= request.getAttribute("Appendix_Id").toString();
		
	if(request.getAttribute("appendixStatus")!=null)
		appendixStatus= request.getAttribute("appendixStatus").toString();
		
	if(request.getAttribute("latestaddendumid")!=null)
		latestaddendumid= request.getAttribute("latestaddendumid").toString();	
		
	if( request.getAttribute( "appendixType" ) != null)
		appendixType = (String) request.getAttribute( "appendixType" );



String jobStatus ="";
String job_type ="";
String appendixtype ="";
String Job_Id ="";
String chkadd ="";
String chkaddendum ="";
String chkaddendumactivity ="";
String addendum_id ="";
String from = "PM";

if(request.getAttribute("jobStatus")!=null)
	jobStatus= request.getAttribute("jobStatus").toString();
	
if(request.getAttribute("job_type")!=null)
	job_type= request.getAttribute("job_type").toString();
	
if(request.getAttribute("appendixtype")!=null)
	appendixtype= request.getAttribute("appendixtype").toString();	

if(request.getAttribute("Job_Id")!=null)
	Job_Id= request.getAttribute("Job_Id").toString();		
	
if(request.getAttribute("chkadd")!=null)
	chkadd= request.getAttribute("chkadd").toString();
	
if(request.getAttribute("chkaddendum")!=null)
	chkaddendum= request.getAttribute("chkaddendum").toString();	
	
if(request.getAttribute("chkaddendumactivity")!=null)
	chkaddendumactivity= request.getAttribute("chkaddendumactivity").toString();	
	
if(request.getAttribute("addendum_id")!=null)
	addendum_id= request.getAttribute("addendum_id").toString();	
	
	
%>
<%-- <%@ include  file="/MSAMenu.inc" %> --%>
<%--  <%@ include  file="/AppendixMenu.inc" %>  --%>
<script>
var str = '';
function bulkJobCreation()
{
   
	document.forms[0].target = "_self";

	document.forms[0].action = "BulkJobCreation.do?ref=bukJobCreationView&ref1=view&Appendix_Id=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}

function appendixAddComments()
{
    document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id %>";
	document.forms[0].submit();
	return true;
}

function appendixViewComments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=Appendix&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	 
}

function appendixEdit()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=Appendix&EditType=PM&Id=<%= Appendix_Id%>";
	document.forms[0].submit();
	return true;	
}

function appendixView( v )
{
	if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	 
  
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=Appendix&from_type=PM&Id=<%= Appendix_Id %>&jobId=<%= latestaddendumid %>";
	document.forms[0].submit();
	return true;	
}



<%-- <%@ include  file="/JobMenu.inc" %> --%>


<script>
var str = '';

function jobAddComment()
{
	document.forms[0].action = 'MenuFunctionAddCommentAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Type=Job&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id %>';
	document.forms[0].submit();
	return true;	 
	 
}

function jobViewComments()
{
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Type=Job&Id=<%= Job_Id %>';
	document.forms[0].submit();
	return true;	 
}

function jobDel() 
{
		<%if(job_type.equals("Default")|| jobStatus.equals("Approved"))
			{
				if(job_type.equals("Default")){%>
						alert( "<bean:message bundle = "pm" key = "job.detail.defaultjob"/>" );
						return false;
				<%}
				if(jobStatus.equals("Approved")){%>
						alert( "<bean:message bundle = "pm" key = "job.detail.approvedjob"/>" );
						return false;
				<%}%>	
		<%}else{%>
			
			var convel = confirm( "<bean:message bundle = "pm" key = "job.detail.delete.confirm"/>" );
			if( convel )
			{
				document.forms[0].action ="DeleteAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Type=Job&Appendix_Id=<%= Appendix_Id %>&Job_Id=<%= Job_Id%>";
				document.forms[0].submit();
				return true;	
			}	
		<%}%>
				
}

function jobUpload()
{
	<%if(job_type.equals("Default")){%>	
		document.forms[0].action = "EntityHistory.do?entityId=<%= Job_Id %>&jobId=<%= Job_Id %>&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Job";
	<%}else{%>
		document.forms[0].action = "EntityHistory.do?entityId=<%= Job_Id %>&jobId=<%= Job_Id %>&entityType=Deliverables&function=addSupplement&linkLibName=Job";
	<%}%>
	document.forms[0].submit();
	return true;
}

function jobViewDocuments()
{
	<%if(job_type.equals("Default")){%>	
		document.forms[0].action = "EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%=Appendix_Id%>&linkLibName=Job&function=supplementHistory&jobId=<%= Job_Id %>";
	<%}else{%>
		document.forms[0].action = "EntityHistory.do?entityType=Deliverables&appendixId=<%=Appendix_Id%>&linkLibName=Job&function=supplementHistory&jobId=<%= Job_Id %>";
	<%}%>
	document.forms[0].submit();
	return true;
}


</script>

<%@ include  file="/ActivityMenu.inc" %>
<%@ include file="/CommonResources.inc" %>	
<%@ include file="/MaterialMenu.inc" %>	
<%@ include file="/LaborMenu.inc" %>
<%@ include file="/FreightMenu.inc" %>	
<%@ include file="/TravelMenu.inc" %>	
<%@ include  file="/DashboardStatusScript.inc" %>


<% 
int addStatusRow = -1;
int addOwnerRow = -1;
boolean addStatusSpace = false;
boolean addOwnerSpace = false;
String checkstatus = null;

int jobOwnerListSize = 0;
if(request.getAttribute("jobOwnerListSize") != null)
	jobOwnerListSize = Integer.parseInt(request.getAttribute("jobOwnerListSize").toString());


%>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad ="leftAdjLayers();">
<html:form action = "/MenuFunctionAddCommentAction" target = "ilexmain">
  	<html:hidden property = "MSA_Id" /> 
 	<html:hidden property = "msaname"/>
 	<html:hidden property = "type" />
 	<html:hidden property = "appendix_Id" />
 	<html:hidden property = "job_Id" />
 	<html:hidden property = "activity_Id" />
 	<html:hidden property = "resource_Id" />
 	<html:hidden property = "ref" />
 	<html:hidden property = "authenticate" />
 	<html:hidden property = "addendum_id" />
 	<html:hidden property = "name" />
 	<html:hidden property = "fromflag" />
 	<html:hidden property = "dashboardid" />
 	<html:hidden property="otherCheck"/> 
	<html:hidden property="go"/>
	<html:hidden property="forwardId"/>
	<html:hidden property="appendixOtherCheck"/> 
	<html:hidden property="jobOwnerOtherCheck"/> 	

 		  
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "MSA">
	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        	<%-- <% if( msastatus.equals( "Signed" ) || msastatus.equals( "Transmitted" ) || uploadstatus.equals( "U" ) )  
							{boolean check = false;
								if( msastatus.equals( "Signed" ) || msastatus.equals( "Transmitted" ) ) 
								{check = true;
									if( msastatus.equals( "Signed" ) )
									{
								%>
									<td id = "pop1"  width = "100" height="19" class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegestatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
									<%}else{
								%>
								<td id = "pop1"  width = "100" height="19" class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
								<%}}if( uploadstatus.equals( "U" ) &&  !check )
								{%>
									<td id = "pop1"  width = "100" height="19" class = "Ntoprow1" align="center" ><a href = "javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegeupload"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
								<%}
							} 
							else 
							{ %>
	                 			 <td id = "pop1"  width = "100" height="19" class = "Ntoprow1" align="center" ><a href = "MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.edit"/></center></a></td>
							<% 
							} %> --%>
				<!--  New Menu  -->
						
							
							<%-- <td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
							<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
							<td id = "pop4" class = "Ntoprow1" colspan="4" width="450">&nbsp;</td> --%>
				
				<div id="menunav">
	<ul>	 
<% if( msastatus.equals( "Signed" ) || msastatus.equals( "Transmitted" ) || uploadstatus.equals( "U" ) )  
							{boolean check = false;
								if( msastatus.equals( "Signed" ) || msastatus.equals( "Transmitted" ) ) 
								{check = true;
									if( msastatus.equals( "Signed" ) )
									{
								%>
									<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')">Edit</a></li>
								<%}else{
								%>
									<%-- <li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')">Edit</a></li> --%>
								<%}}/* if( uploadstatus.equals( "U" ) &&  !check ) */
								 {%>
									<%-- <li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegeupload"/>')"><bean:message bundle = "pm" key = "msa.detail.edit"/></a></li> --%>
								<%} 
							} 
							else 
							{ %>
	                 			 <li><a class="drop" href="MSAEditAction.do?msaId=<%= MSA_Id%>&fromType=MSADetail"><bean:message bundle = "pm" key = "msa.detail.edit"/>Edit</a></li>
							<% 
							} %>
							<%-- <td id = "pop2"  width = "120" height="19" class = "Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Managemsa"/></center></a></td>
							<td id = "pop3" width = "120"  class = "Ntoprow1" align="center" ><a href = "AppendixUpdateAction.do?firstCall=true&MSA_Id=<%= MSA_Id%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)"    onMouseOver = "MM_swapImage('Image25','','images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "msa.detail.Appendices"/></center></a></td>
							<td id = "pop4" class = "Ntoprow1" colspan="8" width="450" >&nbsp;</td> --%>
							
	
		
		<li>
			<a href="#" class="drop"><span>Manage</span></a>
			<ul>
				<li>
					<%
					if((String) request.getAttribute("list") != "" ) {
						%>
						<a href="#"><span><bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/></span></a>
						<ul>
						
						
						
							<%= (String)request.getAttribute("list") %>
												
						</ul>
						<%
					} else {
						%>
						<a href="#"><bean:message bundle = "pm" key = "msa.detail.menu.changestatus.new"/></a>
						<%
					}
					%>
				</li>
				<li>
					<a href="#">View MSA</span></a>
					<ul>
						<li><a href="MSADetailAction.do?MSA_Id=<%= MSA_Id %>&Prjid=<%= MSA_Id %>">Details</a></li>
						<%
						if( msastatus.equals( "Signed" ) || msastatus.equals( "Approved" ) || msastatus.equals( "Pending Customer Review" )) {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewpdf"/></a></li>
							<li><a href="javascript:view('rtf');"><bean:message bundle = "pm" key = "msa.detail.menu.viewrtf"/></a></li>
						     <%
							if(request.getAttribute("new_addendumlist") != "" ) {
								%>
								<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "msa.detail.menu.viewhtml"/></a></li>
								<%
								/* if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) { */
								
								
									
								
// 								} 
								
					//			else {
									%>
									
									<%
					//			}
							} 
						}
						%> 
					</ul>
				</li>
				<%
				if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Cancelled" ) && !msastatus.equals( "Review" )) {
					%>
					<li><a href="javascript:sendemail();"><bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/></a></li>
					
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.sendMSA.nopriviledgestatus"/>');"><bean:message bundle = "pm" key = "msa.detail.menu.sendmsa"/></a></li>
					<%
				}
				if( msastatus.equals( "Signed" ) || msastatus.equals( "Inwork" ) || msastatus.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegetransstatus"/>')"><bean:message bundle = "pm" key = "msa.detail.formaldocdate"/></a></li>
					
					<%
				}  else {
					%>
					<li><a href="EditBdmAction.do?msaId=<%= MSA_Id %>&fromType=MSADetail">Edit BDM</a></li>
					
					<%
				} %>
				<% 
				if( msastatus.equals( "Signed" ) || msastatus.equals( "Inwork" ) || msastatus.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegeinworkstatus"/>')"><bean:message bundle = "pm" key = "msa.detail.formaldocdate.noprivilegetransstatus.cancelled"/></a></li>
					
					<%
				} else {
					%>
					<li><a href="javascript:setformaldocdate();"><bean:message bundle = "pm" key = "msa.detail.menu.setformaldocumentdate"/></a></li>
					
					<%
				}
				if(msastatus.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/></a></li>
					<%
				} 
				%>
				<li>
					<a href="#"><span>MSA Comments</span></a>
					<ul>
						<li><a href="javascript:viewcomments();"><bean:message bundle = "pm" key = "msa.detail.menu.viewall"/></a></li>
						<%
						if(msastatus.equals( "Cancelled" )) {
							%>
							<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:addcomment();"><bean:message bundle = "pm" key = "msa.detail.menu.addcomment"/></a></li>
							<%
						}
						%>
					</ul>
				</li>
				<%
				if( msastatus.equals( "Pending Customer Review" )) {
					%>
					<li><a href="EntityHistory.do?function=modifyDocument&entityId=<%= MSA_Id %>&entityType=MSA"><bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.upload.state"/>');"><bean:message bundle = "pm" key = "msa.detail.menu.uploadmsa"/></a></li>
					<%
				}
				if( !msastatus.equals( "Draft" ) && !msastatus.equals( "Review" )) {
					if(msastatus.equals( "Cancelled" )) {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus.cancelled"/>')"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>
						<%
					} else {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "msa.detail.edit.noprivilegetransstatus"/>')"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>
						<%
					}
				} else {
					%>
					<li><a href="javascript:editmsa();"><bean:message bundle = "pm" key = "msa.detail.menu.editmsa"/></a></li>	
					<%
				}
				%>
				
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "msa.detail.menu.documents"/></span></a>
					<ul>
						<li><a href="EntityHistory.do?entityId=<%= MSA_Id %>&entityType=MSA&function=viewHistory&msastatus=<%=msastatus%>">MSA  History</a></li>
						<li>
							<a href="#"><span>Support Documents</span></a>
							<ul>
								<li><a href="EntityHistory.do?entityId=<%= MSA_Id %>&msaId=<%= MSA_Id %>&entityType=MSA Supporting Documents&function=addSupplement&linkLibName=MSA">Upload</a></li>
								<li><a href="EntityHistory.do?entityType=MSA Supporting Documents&msaId=<%= MSA_Id %>&linkLibName=MSA&function=supplementHistory">History</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="javascript:del();"><bean:message bundle = "pm" key = "msa.detail.menu.deletemsa"/></a></li>
			</ul>
		</li>
		<li><a class="drop" href="AppendixUpdateAction.do??firstCall=true&MSA_Id=<%= MSA_Id%>">Appendices</a></li>
	</ul>
</div>
				
							
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a></div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td colspan="7">
	 						<h2>MSA<bean:message bundle = "pm" key = "common.addcommentpage.addcomment"/><bean:write name = "MenuFunctionAddCommentForm" property = "name" /></h2>
	 					</td>
	 				</tr>
	      		</table>
   				</td>
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_MSA();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_MSA();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "selectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionAddCommentForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionAddCommentForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionAddCommentForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<%
																checkowner = "javascript: checking('"+ownerId+"');";  
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "selectedOwners" onclick = "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal> 		  
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "Appendix">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
					
					<div id="menunav">
					<ul>	        
			        <% if( appendixStatus.equals( "Signed" ) || appendixStatus.equals( "Transmitted" ) )
					{
						if( appendixStatus.equals( "Signed" ) )
						{ %>
						<li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noeditappendix"/>')">Edit</a></li>
						<%}
						else
						{%>
							<%-- <li><a class="drop" href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noeditappendix"/>')">Edit</a></li> --%>
						<%}
					}
					else
					{%>	
						<li><a class="drop" href = "AppendixEditAction.do?appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>&fromType=AppendixDetail">Edit</a></li> 
					<%} %>	
	
					<%-- <td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
					<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>" onMouseOut = "MM_swapImgRestore();"    onMouseOver = "MM_swapImage('Image25','','.images/about1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "appendix.detail.jobs"/></center></a></td>
					<td id ="pop4" width ="840" class ="Ntoprow1" colspan="4">&nbsp;</td> --%>
		
		
		<li>
			<a href="#" class="drop"><span>Manage</span></a>
			<ul>
				<li>
					<%
					if((String) request.getAttribute("new_list") != "" ) {
						%>
						<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/></span></a>
						<ul>
							<%= (String) request.getAttribute("new_list") %>
						</ul>
						<%
					} else {
						%>
						<a href="#"><bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/></a>
						<%
					}
					%>
				</li>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.viewappendix"/></span></a>
					<ul>
						<li><a href="AppendixDetailAction.do?MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>">Details</a></li>
						<%
						if( appendixStatus.equals( "Signed" ) || appendixStatus.equals( "Approved" ) || appendixStatus.equals( "Pending Customer Review" )) {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewpdf"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:view('pdf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewpdf"/></a></li>
							<li><a href="javascript:view('rtf');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewrtf"/></a></li>
							<%
							if(request.getAttribute("new_addendumlist") != "" ) {
								%>
								<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
								<%
								if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) {
									%>
									<li>
										<a href="#"><span>History</span></a>
										<ul>
											<%= (String) request.getAttribute("new_addendumlist") %>
										</ul>
									</li>
									<li><a href="javascript:view('excel');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/></a></li>
									<%
								} else {
									%>
									<li>
										<a href="#"><span>History</span></a>
										<ul>
											<%= (String) request.getAttribute("new_addendumlist") %>
										</ul>
									</li>
									<%
								}
							} else {
								if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) {
									%>
									<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
									<li><a href="javascript:view('excel');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/></a></li>
									<%
								} else {
									%>
									<li><a href="javascript:view('html');"><bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/></a></li>
									<%
								}
							}
						}
						%>
					</ul>
				</li>
				<%
				if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )) {
					%>
					<li><a href="javascript:sendemail();"><bean:message bundle = "pm" key = "appendix.detail.menu.sendappendix"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.sendAppendix.noprivilegestatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.sendappendix"/></a></li>
					<%
				}
				if( appendixStatus.equals( "Signed" ) || appendixStatus.equals( "Inwork" ) || appendixStatus.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.formaldocdate.noprivilegeinworkappendixStatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.endcustomer.noprivilegeinworkappendixStatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:setformaldocdate();"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:setendcustomer();"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li>
					<%
				}
				if( appendixStatus.equals( "Signed" ) || appendixStatus.equals( "Inwork" ) || appendixStatus.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.formaldocdate.noprivilegeinworkappendixStatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.endcustomer.noprivilegeinworkappendixStatus"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:setformaldocdate();"><bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/></a></li>
					<li><a href="javascript:setendcustomer();"><bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/></a></li>
					<%
				}
				if(appendixStatus.equals( "Cancelled" )) {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:addcontact();"><bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/></a></li>
					<%
				}
				%>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.comments"/></span></a>
					<ul>
						<li><a href="javascript:viewcomments();"><bean:message bundle = "pm" key = "appendix.detail.menu.viewall"/></a></li>
						<%
						if(appendixStatus.equals( "Cancelled" )) {
							%>
							<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>');"><bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/></a></li>
							<%
						} else {
							%>
							<li><a href="javascript:addcomment();"><bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/></a></li>
							<%
						}
						%>
					</ul>
				</li>
				<%
				if( appendixStatus.equals( "Pending Customer Review" )) {
					%>
					<li><a href="EntityHistory.do?function=modifyDocument&entityId=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&entityType=Appendix"><bean:message bundle = "pm" key = "appendix.detail.menu.uploadappendix"/></a></li>
					<%
				} else {
					%>
					<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.upload.state"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.uploadappendix"/></a></li>
					<%
				}
				if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Review" )) {
					if(appendixStatus.equals( "Cancelled" )) {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>
						<%
					} else {
						%>
						<li><a href="javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>
						<%
					}
				} else {
					%>
					<li><a href="javascript:editappendix();"><bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/></a></li>	
					<%
				}
				%>
				<li><a href="ESAEditAction.do?ref=view&appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>">External Sales Agent</a></li>
				<li>
					<a href="#"><span><bean:message bundle = "pm" key = "appendix.detail.menu.documents"/></span></a>
					<ul>
						<li><a href="EntityHistory.do?entityId=<%= Appendix_Id %>&MSA_Id=<%= MSA_Id%>&entityType=Appendix&function=viewHistory">Appendix History</a></li>
						<li>
							<a href="#"><span>Support Documents</span></a>
							<ul>
								<li><a href="EntityHistory.do?entityId=<%= Appendix_Id %>&appendixId=<%= Appendix_Id %>&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Appendix">Upload</a></li>
								<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= Appendix_Id %>&linkLibName=Appendix&function=supplementHistory">History</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="javascript:del();"><bean:message bundle = "pm" key = "appendix.detail.menu.deleteappendix"/></a></li>
			</ul>
		</li>
		<li><a class="drop" href="JobUpdateAction.do?ref=View&Appendix_Id=<%= Appendix_Id%>"><bean:message bundle = "pm" key = "appendix.detail.jobs"/></a></li>
	</ul>
</div>
					
					
			        
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
					         <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a><a href="AppendixUpdateAction.do?MSA_Id=<bean:write name  ="MenuFunctionAddCommentForm" property ="MSA_Id"/>&firstCall=true"><bean:write name = "MenuFunctionAddCommentForm" property = "msaname"/></a></div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td colspan="7">
	 						<h2>Appendix<bean:message bundle = "pm" key = "common.addcommentpage.addcomment"/><bean:write name = "MenuFunctionAddCommentForm" property = "name" /></h2>
			 		   </td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><a href="#" class="imgstyle"><img src="images/month.gif" width="31" alt="Month" id="Image10" title="This Month" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/overmonth.gif',1)"  onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onClick="month_Appendix();"/></a></td>
							                      <td><a href="#" class="imgstyle"><img src="images/week.gif" alt="Week" name="Image11" width="31" height="30" border="0" id="Image11" title="This Week" onMouseOver="MM_swapImage('Image11','','images/Overweek.gif',1)"  onMouseOut="MM_swapImgRestore('Image11','','images/myTeam.gif',1)" onClick="week_Appendix();" /></a></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter" class="divstyle">
        											<table width="250" cellpadding="0" class="divtable">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption">Status View</td>
                          								</tr>
                          								<tr><td></td>
							                          	  	<td class="tabNormalText">
																<logic:present name ="statuslist" scope="request">
															    	<logic:iterate id = "list" name = "statuslist"> 
																 		<html:multibox property = "appendixSelectedStatus"> 
																			<bean:write name ="list" property = "statusid"/>
																 		</html:multibox>   
																	 	<bean:write name ="list" property = "statusdesc"/><br> 
																	</logic:iterate>
																</logic:present>
																<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
																<table cellpadding="0">
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionAddCommentForm" property ="monthWeekCheck" value ="week">This Week</html:radio></td></tr>
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionAddCommentForm" property ="monthWeekCheck" value ="month">This Month</html:radio></td></tr>	
																	<tr><td class="tabNormalText"><html:radio name ="MenuFunctionAddCommentForm" property ="monthWeekCheck" value ="clear">Clear</html:radio></td></tr>	
																</table>	
															</td>
	    												</tr>
                        							</table>
                        				</td>
					                    <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
					                    <td width="50%" valign="top">
					                    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          						<tr><td colspan="3" class="filtersCaption">User View</td></tr>
                          						<tr>
											         <td class="tabNormalText">
											    		<logic:present name ="ownerlist" scope="request">
													    	<logic:iterate id = "olist" name = "ownerlist">
													    		<bean:define id="ownerName" name = "olist" property="ownerName" type="java.lang.String"/>
													    		<bean:define id="ownerId" name = "olist" property="ownerId" type="java.lang.String"/>
													    		<%
																checkowner = "javascript: checking('"+ownerId+"');";  
																if((!ownerName.equals("Me")) && (!ownerName.equals("All")) && (!ownerName.equals("Other"))) {
																	addRow++;
																	addSpace = true;
																}	
																	
													    		if(addRow == 0) { %> 
														    		<br/>
																	&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
																	<br/>								
																<% } %>
													    		<% if(addSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
													    		<html:multibox property = "appendixSelectedOwners" onclick = "<%=checkowner%>"> 
																	<bean:write name ="olist" property = "ownerId"/>
														 		</html:multibox>   
																  <bean:write name ="olist" property = "ownerName"/>
																  <br/> 
															</logic:iterate>
														</logic:present>
												    </td>
                          						</tr>
                         
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onClick="show();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
					                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onClick="initialState();"/><br></a></td></tr>
					                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                            				</table>
                            			</td>
                        			</tr>
  							</table>											
						</div>
					</span>
				</td>
			</tr>
            </table>
        </td>
     </tr>
  </table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "MenuFunctionAddCommentForm"  property = "type" value = "Job">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
    				<tr>
			        <%if(jobStatus.equals("Approved")){%>
						
							<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditapprovedjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>
						<%}else{%>
						
										<% if( job_type.equals( "Default" ) || job_type.equals( "Addendum" ) || appendixtype.equals( "NetMedX")){%>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "job.detail.menu.noeditdefaultjob"/>')" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}else{ %>
											<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "JobEditAction.do?Job_Id=<%=Job_Id%>&appendix_Id=<%=Appendix_Id%>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.edit"/></center></a></td>								
										<%}%>
						<%}%>
						
							<td id = "pop2" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
							<td id = "pop3" width = "120" class = "Ntoprow1" align ="center" ><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu3',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu3',event)" class = "menufont" style="width: 120px"><center><bean:message bundle = "pm" key = "job.detail.activities"/></center></a></td>
							<td id ="pop4" width ="840" class ="Ntoprow1" align ="center" >&nbsp;</td>	
					</tr>
					<tr>
				        <td background="images/content_head_04.jpg" height="21" colspan="7">
					        <div id="breadCrumb">
								          <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
								    	  <a href="AppendixUpdateAction.do?MSA_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "MSA_Id"/>&Appendix_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "appendix_Id"/>"><bean:write name = "MenuFunctionAddCommentForm" property = "msaname"/></a>
								    	  <a href ="JobUpdateAction.do?Appendix_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "appendix_Id"/>&Name=<bean:write name = "MenuFunctionAddCommentForm" property = "appendixName"/>&ref=<%=chkaddendum%>"><bean:write name = "MenuFunctionAddCommentForm" property = "appendixName"/></a>
							</div>
						</td>
					</tr>
	 				<tr>
	 				<td colspan="7"><h2>Job<bean:message bundle = "pm" key = "common.addcommentpage.addcomment"/><bean:write name = "MenuFunctionAddCommentForm" property = "name" /></h2></td>
	 				</tr>
	 				
	 				
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "Activity">
<logic:equal name = "MenuFunctionAddCommentForm" property = "ref" value = "detailactivity">

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
			        <%if(  jobtype.equals( "Default" ) ||  jobtype.equals( "Addendum" ) ) { %>
						<%if(activityStatus.equals("Approved")){%>
							<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "activity.detail.menu.noeditapprovedactivity"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
						<%}%>
						<%if(activityStatus.equals("Draft")){%>
				        				<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "ActivityEditAction.do?Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
						<%}%>
		        	<%}else{%>
						<%if(activityType.equals("Overhead")){%>
					  		<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "activity.detail.menu.noeditoverheadactivity"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
				        	<%}else{%>
						   	<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "ActivityEditAction.do?Activity_Id=<%= Activity_Id %>&Job_Id=<%= Job_Id %>&ref=<%=chkaddendum%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
				        	<%}%>
		        	<%}%>
		        	<td id = "pop2" width = "120" class = "Ntoprow1"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px"><CENTER>Manage</CENTER></a></td>
		        	<td id = "pop3" width = "120" class = "Ntoprow1"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><CENTER>Resources</CENTER></a></td>
					<td id = "pop4" width = "840" class="Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        
			        </tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="7">
							<div id="breadCrumb">
					    		  <a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
					    		  <a href="AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "MSA_Id"/>"><bean:write name = "MenuFunctionAddCommentForm" property = "msaname"/></a>
					    		  <a href ="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "appendix_Id"/>"><bean:write name = "MenuFunctionAddCommentForm" property = "appendixName"/></a>
					    		  <a href ="ActivityUpdateAction.do?ref=<%=chkaddendum %>&Job_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "job_Id"/>"><bean:write name = "MenuFunctionAddCommentForm" property = "jobName"/></a>
				    		  </div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td colspan ="7"><h2><bean:message bundle = "pm" key = "common.addcommentpage.activity"/><bean:message bundle = "pm" key = "common.addcommentpage.addcomment"/><bean:write name = "MenuFunctionAddCommentForm" property = "name" /> </h2></td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>

<logic:notEqual name = "MenuFunctionAddCommentForm" property = "ref" value = "detailactivity">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr><td class="headerrow" height="19"><a href= "javascript:history.go(-1);" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><bean:message bundle = "pm" key = "common.sow.backtopmactivity"/></a></td></tr>
					<tr><td background="images/content_head_04.jpg" height="21" width="100%">&nbsp;</td></tr>
					<tr>
	 					<td colspan ="7"><h2><bean:message bundle = "pm" key = "common.addcommentpage.activity"/><bean:message bundle = "pm" key = "common.addcommentpage.addcomment"/><bean:write name = "MenuFunctionAddCommentForm" property = "name" /> </h2></td>
	 				</tr>
					
					
</table>
</logic:notEqual>
</logic:equal>


<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "pm_resM">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			      <tr>
					<%if(  commonResourceStatus.equals( "Draft" ) ||  commonResourceStatus.equals( "Cancelled" ) ) { %>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "MaterialEditAction.do?&ref=<%=chkaddendum%>&Material_Id=<%=Material_Id%>&Activity_Id=<%=Activity_Id%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><center>Edit</center></a></td>
					<%}else{%>
						<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "resource.detail.menu.noeditapprovedresource"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
					<%}%>
						<td id = "pop2" width = "120" class = "Ntoprow1" align ="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event);" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );popUp( 'elMenu2' , event );" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
						<td class = "Ntoprow1" width = "800">&nbsp;</td>
				</tr>
			        <tr>
			        	 <td background="images/content_head_04.jpg" height="21" colspan="7">
			        	  <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
			          		<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "MSA_Id" />">
			          		<bean:write name = "MenuFunctionAddCommentForm" property = "msaname"/></a>
			          		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "appendix_Id" />">
			   				<bean:write name = "MenuFunctionAddCommentForm" property = "appendixName"/></A>
			          		<A href="ActivityUpdateAction.do?ref=<%=refForActivity%>&Job_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "job_Id" />">
			       			<bean:write name = "MenuFunctionAddCommentForm" property = "jobName"/></A>
			       			<A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=M&Activity_Id=<%=Activity_Id%>">
			       			<bean:write name = "MenuFunctionAddCommentForm" property = "activityName"/></A>
			       		</td>	
			          	</div>  
	 				</tr>
	 				<tr>
	 					<td colspan="7"><h2><bean:message bundle = "pm" key = "pm.material.addcomment.label"/><bean:write name = "MenuFunctionAddCommentForm" property = "name" /></h2></td>
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>

<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "pm_resL">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
					<%if(  commonResourceStatus.equals( "Draft" ) ||  commonResourceStatus.equals( "Cancelled" ) ) { %>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "LaborEditAction.do?&ref=<%=chkaddendum%>&Labor_Id=<%=Labor_Id%>&Activity_Id=<%=Activity_Id%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><center>Edit</center></a></td>
					<%}else{%>
						<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "resource.detail.menu.noeditapprovedresource"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
					<%}%>
						<td id = "pop2" width = "120" class = "Ntoprow1" align ="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event);" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );popUp( 'elMenu2' , event );" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
						<td class = "Ntoprow1" width = "800">&nbsp;</td>
				</tr>
			        <tr>
			        	 <td background="images/content_head_04.jpg" height="21" colspan="7">
			        	  <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
			          		<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "MSA_Id" />">
			          		<bean:write name = "MenuFunctionAddCommentForm" property = "msaname"/></a>
			          		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "appendix_Id" />">
			   				<bean:write name = "MenuFunctionAddCommentForm" property = "appendixName"/></A>
			          		<A href="ActivityUpdateAction.do?ref=<%=refForActivity%>&Job_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "job_Id" />">
			       			<bean:write name = "MenuFunctionAddCommentForm" property = "jobName"/></A>
			       			<A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=L&Activity_Id=<%=Activity_Id%>">
			       			<bean:write name = "MenuFunctionAddCommentForm" property = "activityName"/></A>
			       		</td>	
			          	</div>  
	 				</tr>
	 				<tr>
	 					<td colspan ="7" ><h2><bean:message bundle = "pm" key = "pm.labor.addcomment.label"/><bean:write name = "MenuFunctionAddCommentForm" property = "name" /></h2></td>
	 					
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "pm_resF">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
					<%if(  commonResourceStatus.equals( "Draft" ) ||  commonResourceStatus.equals( "Cancelled" ) ) { %>
						<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "FreightEditAction.do?&ref=<%=chkaddendum%>&Freight_Id=<%=Freight_Id%>&Activity_Id=<%=Activity_Id%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><center>Edit</center></a></td>
					<%}else{%>
						<td id = "pop1" width = "100" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "resource.detail.menu.noeditapprovedresource"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
					<%}%>
						<td id = "pop2" width = "120" class = "Ntoprow1" align ="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event);" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );popUp( 'elMenu2' , event );" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
						<td class = "Ntoprow1" width = "800">&nbsp;</td>
				</tr>
			        <tr>
			        	 <td background="images/content_head_04.jpg" height="21" colspan="7">
			        	  <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
			          		<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "MSA_Id" />">
			          		<bean:write name = "MenuFunctionAddCommentForm" property = "msaname"/></a>
			          		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "appendix_Id" />">
			   				<bean:write name = "MenuFunctionAddCommentForm" property = "appendixName"/></A>
			          		<A href="ActivityUpdateAction.do?ref=<%=refForActivity%>&Job_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "job_Id" />">
			       			<bean:write name = "MenuFunctionAddCommentForm" property = "jobName"/></A>
			       			<A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=F&Activity_Id=<%=Activity_Id%>">
			       			<bean:write name = "MenuFunctionAddCommentForm" property = "activityName"/></A>
			       		</td>	
			          	</div>  
	 				</tr>
	 				<tr>
	 					<td colspan ="7" ><h2><bean:message bundle = "pm" key = "pm.freight.addcomment.label"/><bean:write name = "MenuFunctionAddCommentForm" property = "name" /></h2></td>
	 					
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>

</logic:equal>
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "pm_resT">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr>
					<%if(  commonResourceStatus.equals( "Draft" ) ||  commonResourceStatus.equals( "Cancelled" ) ) { %>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "TravelEditAction.do?&ref=<%=chkaddendum%>&Travel_Id=<%=Travel_Id%>&Activity_Id=<%=Activity_Id%>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><center>Edit</center></a></td>
					<%}else{%>
						<td id = "pop1" width = "120" class = "Ntoprow1" align ="center"><a href = "javascript:alert('<bean:message bundle = "pm" key = "resource.detail.menu.noeditapprovedresource"/>')" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 120px"><CENTER>Edit</CENTER></a></td>
					<%}%>
						<td id = "pop2" width = "120" class = "Ntoprow1" align ="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event);" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );popUp( 'elMenu2' , event );" class = "menufont" style="width: 120px"><center>Manage</center></a></td>
						<td class = "Ntoprow1" width = "800">&nbsp;</td>
				</tr>
			        <tr>
			        	 <td background="images/content_head_04.jpg" height="21" colspan="7">
			        	  <div id="breadCrumb"><a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
			          		<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "MSA_Id" />">
			          		<bean:write name = "MenuFunctionAddCommentForm" property = "msaname"/></a>
			          		<A href="JobUpdateAction.do?ref=<%=refForJob%>&Appendix_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "appendix_Id" />">
			   				<bean:write name = "MenuFunctionAddCommentForm" property = "appendixName"/></A>
			          		<A href="ActivityUpdateAction.do?ref=<%=refForActivity%>&Job_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "job_Id" />">
			       			<bean:write name = "MenuFunctionAddCommentForm" property = "jobName"/></A>
			       			<A href="ResourceListAction.do?ref=<%=refForResource%>&resourceListType=T&Activity_Id=<%=Activity_Id%>">
			       			<bean:write name = "MenuFunctionAddCommentForm" property = "activityName"/></A>
			       		</td>	
			          	</div>  
	 				</tr>
	 				<tr>
	 					<td colspan ="7" ><h2><bean:message bundle = "pm" key = "pm.travel.addcomment.label"/><bean:write name = "MenuFunctionAddCommentForm" property = "name" /></h2></td>
	 					
	 				</tr>
	      		</table>
   				</td>
   				
			    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
				<td valign="top" width="155">
				
    				<table width="100%" cellpadding="0" cellspacing="0" border="0">
        				<tr><td width="155" height="39"  class="headerrow" colspan="2">
        						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td width="150">
              							<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
              							    <tr>
							               		  <td><img src="images/disbmonth.gif" width="31"id="Image10" height="30" border="0"/></td>
							                      <td><img src="images/disbweek.gif"  name="Image11" width="31" height="30" border="0" id="Image11" /></td>   
							               </tr>
                  						</table>
                  						</td>
              						</tr>
            					</table>
            				</td>
            		 	</tr>
        				<tr><td height="21" background="images/content_head_04.jpg" width="140">
          						<table width="100%" cellpadding="0" cellspacing="0" border="0">
              						<tr><td nowrap="nowrap" colspan="2">
	                						<div id="featured1">
	                							<div id="featured">
													<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
												</div>								
	                						</div>		
											<span>
												<div id="filter">
        											<table width="250" cellpadding="0" class="divnoview">
                    									<tr><td width="50%" valign="top">
                    											<table width="100%" cellpadding="0" cellspacing="0" border="0">
                          											<tr><td colspan="3" class="filtersCaption"><font color="BLUE"><bean:message bundle = "pm" key = "detail.no.viewselector"/></font></td></tr>
		    	    												</tr>
                        										</table>
                        									</td>
					                        			</tr>
  													</table>											
												</div>
											</span>
										</td>
									</tr>
            					</table>
        					</td>
    				 </tr>
</table>
</td>
</tr>
</table>
</logic:equal>
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "prm_appendix">
<%@ include  file="/DashboardVariables.inc" %>
<%@ include  file="/AppedixDashboardMenu.inc" %>
<%@ include  file="/AppendixDashboardMenuScript.inc" %>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">

<tr>
	<td valign="top" width = "100%">
		<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			<tr>
				<td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
					<div id="breadCrumb">&nbsp;</div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><h2><bean:message bundle = "pm" key = "common.addcommentpage.addcomment"/> <bean:message bundle = "pm" key = "common.addcommentpage.for"/> <bean:write name = "MenuFunctionAddCommentForm" property = "name" /> <bean:message bundle = "pm" key = "common.addcommentpage.appendix"/></h2></td>
			</tr>
		</table>
	</td>
      
    <td width="35" valign="top"><img id="content_head_02" src="images/content_head_02.jpg" width="35" height="60" alt="" /></td>
	<td valign="top" width="155">
    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <tr>
		          <td width="155" height="39"  class="headerrow" colspan="2">
			          <table width="100%" cellpadding="0" cellspacing="0" border="0">
			              <tr>
			                <td width="150">
			                	<table width="100%" cellpadding="0" cellspacing="3" border="0">
				                   	<tr>
							            <td><a href="#" class="imgstyle"><img src="images/Icon_Scheduled.gif" width="31" alt="Month" id="Image10" title="Scheduled" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('II');"/></a></td>
	  									<td><a href="#" class="imgstyle"><img src="images/Icon_Schedule_Overdued.gif" width="31" alt="Month" id="Image10" title="Scheduled OverDue" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Scheduled_Overdued_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IO');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Onsite.gif" width="31" alt="Month" id="Image10" title="OnSite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Onsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('ION');"/></a></td>
										<td><a href="#" class="imgstyle"><img src="images/Icon_Ofsite.gif" width="31" alt="Month" id="Image10" title="Offsite" height="30" border="0" onMouseOver="MM_swapImage('Image10','','images/Icon_Ofsite_Over.gif',1)" onMouseOut="MM_swapImgRestore('Image10','','images/overMe.gif',1)" onclick="setStatus('IOF');"/></a></td>
				                    </tr>
			                  	</table>
			                </td>
			              </tr>
			          </table>
		          </td>
		        </tr>
        		<tr>
          		<td height="21" background="images/content_head_04.jpg" width="140">
          			<table width="100%" cellpadding="0" cellspacing="0" border="0">
              		<tr>
              		<td width ="70%" colspan="2">&nbsp;</td>
               			 <td nowrap="nowrap" colspan="2">
	                			<div id="featured1">
		                			<div id="featured">
											<li name="closeNow" id="closeNow"><a href="javascript:void(0)" title="Open View" onClick="ShowDiv();">My Views <img src="images/showFilter.gif" title="Open View" border="0" onClick="ShowDiv();"/></a><a href="javascript:void(0)" class="imgstyle"><img src="images/offFilter2.gif" border="0" title="Close View" onClick="hideDiv();"/></a></li>
									</div>								
	                			</div>		
						<span>
								<div id="filter" class="divstyle">
	        			<table width="250" cellpadding="0" class="divtable">
	                    <tr>
		                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		                          <tr>
                           	 <td colspan="3" class="filtersCaption">Status View</td>
                          </tr>
                          <tr>	
                          	  <td></td>
	                          	  <td class="tabNormalText">
										<logic:present name ="jobAppStatusList" scope="request">
									    	<logic:iterate id = "list" name = "jobAppStatusList"> 
									    		<bean:define id="statusName" name = "list" property="statusdesc" type="java.lang.String"/>
									    		<bean:define id="statusId" name = "list" property="statusid" type="java.lang.String"/>

												    	<%
											    		 checkstatus = "javascript: checkingStatus('"+statusId+"');";
														 if((!statusName.equals("Inwork")) && (!statusName.equals("Complete")) && (!statusName.equals("Closed")) && (!statusName.equals("Cancelled")) && (!statusName.equals("Hold"))) {
																addStatusRow++;
																addStatusSpace = true;
												    		}
												    	%>
												    	<% if(addStatusSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
										 		<html:multibox property = "jobSelectedStatus" onclick = "<%=checkstatus%>"> 
													<bean:write name ="list" property = "statusid"/>
										 		</html:multibox>   
											 	<bean:write name ="list" property = "statusdesc"/><br> 
											 	<%addStatusSpace = false;%>
											</logic:iterate>
										</logic:present>		
										<img src="images/hrfilterLine.gif" width="100px"  class="imagefilter"/>
										<table cellpadding="0">
											<tr><td class="tabNormalText"><html:radio name ="MenuFunctionAddCommentForm" property ="jobMonthWeekCheck" value ="week"  >This Week</html:radio></td></tr>
											<tr><td class="tabNormalText"><html:radio name ="MenuFunctionAddCommentForm" property ="jobMonthWeekCheck" value ="month" >This Month</html:radio></td></tr>	
											<tr><td class="tabNormalText"><html:radio name ="MenuFunctionAddCommentForm" property ="jobMonthWeekCheck" value ="clear" >Clear</html:radio></td></tr>	
										</table>	
									</td>
	    					</tr>
                        </table></td>
                      <td><img src="images/filterLine.gif" width="1px" height="<%= rowHeight%>" class="imagefilter"/></td>
                      <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td colspan="3" class="filtersCaption">User View</td>
                          </tr>
                          <tr>
					         <td class="tabNormalText">
					           <span id = "OwnerSpanId">
					   			<logic:present name ="jobOwnerList" scope="request">
							    	<logic:iterate id = "olist" name = "jobOwnerList">
							    		<bean:define id="jobOwnerName" name = "olist" property="ownerName" type="java.lang.String"/>
							    		<bean:define id="jobOwnerId" name = "olist" property="ownerId" type="java.lang.String"/>
								    		<% 
	
											checkowner = "javascript: checkingOwner('"+jobOwnerId+"');";
											if((!jobOwnerName.equals("Me")) && (!jobOwnerName.equals("All")) && (!jobOwnerName.equals("Other"))) {
												addOwnerRow++;
												addOwnerSpace = true;
											}	
								    		if(addOwnerRow == 0) { %> 
									    		<br/>
									    		<span class="ownerSpan">
												&nbsp;<bean:message bundle = "pm" key = "msa.tabular.other"/>
												<br/>								
											<% } %>
							    			<% if(addOwnerSpace) { %>&nbsp;&nbsp;&nbsp;&nbsp;<% } %> 
							    		<html:multibox property = "jobSelectedOwners" onclick = "<%=checkowner%>"> 
											<bean:write name ="olist" property = "ownerId"/>
								 		</html:multibox>   
										  <bean:write name ="olist" property = "ownerName"/>
										  <br/> 
									</logic:iterate>
									</span>
								</logic:present>
								</span>
						    </td>
                          </tr>
                         
                              <tr><td colspan="3"><img src="images/spacer.gif" height="34" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/showBtn.gif" width="47" height="15" border="0" class="divbutton" onclick ="changeFilter();"/><br></a></td></tr>
                              <tr><td colspan="3"><img src="images/spacer.gif" width ="2" /></td></tr>
                              <tr><td colspan="3" align="right"><a href="#"><img src="images/reset.gif" width="47" height="15" border="0" class="divbutton" onclick ="resetFilter();"/><br></a></td></tr>                              
                              <tr><td colspan="3"><img src="images/spacer.gif" height="8" /></td></tr>
                              </table></td>
                        </tr>
  			</table>											
		</div>
	</span>
				</td>
	
              </tr>
            </table></td>
        </tr>
      </table>
      </td>
 </tr>
 </table>
	
</logic:equal>

<logic:equal name = "MenuFunctionAddCommentForm"  property = "type" value = "Jobdashboard">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		 <tr>
    			<td valign="top" width = "100%">
    			<table cellpadding="0" cellspacing="0" border="0" width = "100%">
			        <tr><td class="headerrow" height="19">&nbsp;</td></tr>
			        <tr><td background="images/content_head_04.jpg" height="21" colspan="2">
					         	 <div id="breadCrumb">&nbsp;</div>
					    </td>
	 				</tr>
	 				<tr>
	 					<td><h2><bean:message bundle = "pm" key = "common.addcommentpage.addcomment"/> <bean:message bundle = "pm" key = "common.addcommentpage.for"/> Appendix:-> <%= request.getAttribute( "appendixname" ).toString() %>&nbsp;-><bean:message bundle = "pm" key = "common.addcommentpage.job"/>:<bean:write name = "MenuFunctionAddCommentForm" property = "name" /> </h2></td>
	 				</tr>
	      		</table>
   				</td>
		 </tr>
</table>
</logic:equal>
<%-- <logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "MSA">
<%@ include  file="/MSAMenuScript.inc" %>
</logic:equal>--%>
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "Appendix">
<%@ include  file="/AppendixMenuScript.inc" %>
</logic:equal> 
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "Job">
<%@ include  file="/JobMenuScript.inc" %>
</logic:equal>
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "Activity">
	<logic:equal name = "MenuFunctionAddCommentForm" property = "ref" value = "detailactivity">
		<%@ include  file="/ActivityMenuScript.inc" %>
	</logic:equal>
</logic:equal>
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "pm_resM">
<%@ include  file="/MaterialMenuScript.inc" %>
</logic:equal>
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "pm_resL">
<%@ include  file="/LaborMenuScript.inc" %>
</logic:equal>
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "pm_resF">
<%@ include  file="/FreightMenuScript.inc" %>
</logic:equal>
<logic:equal name = "MenuFunctionAddCommentForm" property = "type" value = "pm_resT">
<%@ include  file="/TravelMenuScript.inc" %>
</logic:equal>



								  
<table> 
		  
    						<logic:present name = "clickShow" scope = "request">
						  		<logic:notEqual name = "clickShow" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present> 	
							<logic:present name = "specialCase" scope = "request">
						  		<logic:notEqual name = "specialCase" value = "">
									<script>
										parent.ilexleft.location.reload();
									</script>	
								</logic:notEqual>
							</logic:present>
    		
 		  <tr>
 		  		<td>&nbsp;</td>
	 		  	<td class = "colDark"><bean:message bundle = "pm" key = "common.addcommentpage.comments"/></td> 
	 		  	<td class = "colLight">
	 		  		<html:textarea property = "comment" rows = "5" cols = "75" styleClass = "textarea"/>
	 		  	</td>
 		  </tr>
 		  <tr class = "rowLight">
 		  	<td>&nbsp;</td>
 		  	<td colspan = "3"  align="center">
 		  		<html:submit property = "add" styleClass = "button_c" onclick = "return validate();"><bean:message bundle = "pm" key = "common.addcommentpage.add"/></html:submit>
 		  		<html:reset property = "cancel" styleClass = "button_c">Cancel</html:reset>
	 		  		<logic:notEmpty name="MenuFunctionAddCommentForm" property ="fromflag">
	 		  			<html:button property="back" styleClass="button_c" onclick = "return Backactionjob();"><bean:message bundle="PRM" key="prm.button.back"/></html:button>
					</logic:notEmpty>
			</td> 		  		
 		  </tr>
</table>
</html:form>
</body>

<script>

function trimFields() 
{
	var field = document.forms[0];
	
	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='textarea') 
		{ 
			var temp1=field[i].value;
         
var temp2="";
var flag=0;
if(temp1.indexOf(" ",temp1.length-1)>=0) {
for(var j=temp1.length; j>0; j--){
if((temp1.substring(j-1,j)==" ")&&(flag==0)){
temp2=temp1.substring(0,j-1);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

temp1=temp2;
temp2="";
flag=0;
if(temp1.indexOf(" ")==0) { 
for(var j=0; j<temp1.length; j++){
if((temp1.substring(j,j+1)==" ")&&(flag==0)){
temp2=temp1.substring(j+1,temp1.length);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

field[i].value=temp2;
		}
	}
return true;
}


function validate()
{
	trimFields();
	document.forms[0].authenticate.value = "success";
	if( document.forms[0].comment.value == "" )
	{
		alert( "<bean:message bundle = "pm" key = "common.addcommentpage.entercomment"/>" );
		return false;
	}
	
	return true;
}
function onSubmit(){
	if( validate()){
		document.forms[0].action = "JobDashboardAction.do?appendix_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&Job_Id=<%= jobid %>";
		document.forms[0].submit();
	}else{
		return false;
	}
}


function Backactionjob()
{
	if( document.forms[0].type.value == 'Activity' )
	{
		document.forms[0].action = "ActivityDetailAction.do?from=<%= from %>&ref=<bean:write name = "MenuFunctionAddCommentForm" property = "ref" />&Activity_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "activity_Id" />&jobid=<%= jobid %>" ;
	}
	if( document.forms[0].type.value == 'pm_resM' )
	{
		document.forms[0].action = "MaterialDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "MenuFunctionAddCommentForm" property = "addendum_id" />&ref=<bean:write name = "MenuFunctionAddCommentForm" property = "ref" />&Material_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "forwardId" />";
	}
	
	if( document.forms[0].type.value == 'pm_resL' )
	{
		document.forms[0].action = "LaborDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "MenuFunctionAddCommentForm" property = "addendum_id" />&ref=<bean:write name = "MenuFunctionAddCommentForm" property = "ref" />&Labor_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "forwardId" />";
	}
	
	if( document.forms[0].type.value == 'pm_resF' )
	{
		document.forms[0].action = "FreightDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "MenuFunctionAddCommentForm" property = "addendum_id" />&ref=<bean:write name = "MenuFunctionAddCommentForm" property = "ref" />&Freight_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "forwardId" />";
	}
	
	if( document.forms[0].type.value == 'pm_resT' )
	{
		document.forms[0].action = "TravelDetailAction.do?from=<%= from %>&jobid=<%= jobid %>&addendum_id=<bean:write name = "MenuFunctionAddCommentForm" property = "addendum_id" />&ref=<bean:write name = "MenuFunctionAddCommentForm" property = "ref" />&Travel_Id=<bean:write name = "MenuFunctionAddCommentForm" property = "forwardId" />";
	}
	
	if( document.forms[0].type.value == 'Job' )
	{
		document.forms[0].action = "JobSetUpAction.do?type=2&appendix_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&Job_Id=<%= jobid %>";
	}
	
	
	
	
	document.forms[0].submit();
	return true;
}

function ShowDiv()
{
leftAdjLayers();
document.getElementById("filter").style.visibility="visible";
return false;
}

function hideDiv()
{
document.getElementById("filter").style.visibility="hidden";
return false;
}

function show()
{
	
	document.forms[0].go.value="true";
	
	if(document.forms[0].type.value=='MSA')
		document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&clickShow=true";
		
	if(document.forms[0].type.value=='Appendix')
		document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&clickShow=true";
		
		
	document.forms[0].submit();
	return true;
}

function initialState()
{
document.forms[0].go.value="";
	if(document.forms[0].type.value=='MSA')
		document.forms[0].action="MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&firstCall=true&home=home&clickShow=true";
		
	if(document.forms[0].type.value=='Appendix')
		document.forms[0].action="MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&firstCall=true&home=home&clickShow=true";
		
	document.forms[0].submit();
	return true;
}
function checking(checkboxvalue)
{
	if(document.forms[0].type.value=='MSA'){
		if(document.forms[0].otherCheck.value == 'otherOwnerSelected')
		{
			if(checkboxvalue == '%'){
				for(j=2;j<document.forms[0].selectedOwners.length;j++)
				{
					document.forms[0].selectedOwners[j].checked=false;
				}
			}
			else
			{
				for(j=2;j<document.forms[0].selectedOwners.length;j++)
				{
					if(document.forms[0].selectedOwners[j].checked) {
					 	document.forms[0].selectedOwners[1].checked = false;
					 	break;
					}
				}		
			}		
		}
		else
		{
	
			for(i=0;i<document.forms[0].selectedOwners.length;i++)
			{
					if(document.forms[0].selectedOwners[i].value =='0' && document.forms[0].selectedOwners[i].checked)
					{
						document.forms[0].otherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&opendiv=0";
		 				document.forms[0].submit();
					}
			}
		}
	}
	if(document.forms[0].type.value=='Appendix'){
			if(document.forms[0].appendixOtherCheck.value == 'otherOwnerSelected')
					{
					if(checkboxvalue == '%'){
					for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
					{
						document.forms[0].appendixSelectedOwners[j].checked=false;
					}
					}
			else
			{
				for(j=2;j<document.forms[0].appendixSelectedOwners.length;j++)
				{
					if(document.forms[0].appendixSelectedOwners[j].checked) {
					 	document.forms[0].appendixSelectedOwners[1].checked = false;
					 	break;
					}
				}		
			}
			}		
	
	else
	{
			for(i=0;i<document.forms[0].appendixSelectedOwners.length;i++)
			{
					if(document.forms[0].appendixSelectedOwners[i].value =='0' && document.forms[0].appendixSelectedOwners[i].checked)
					{
						document.forms[0].appendixOtherCheck.value = 'otherOwnerSelected';
						document.forms[0].action="MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&opendiv=0";
		 				document.forms[0].submit();
		
					}
			}
	}
	}
}
function month_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action="MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_MSA()
{
document.forms[0].go.value="true";
document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&week=0&clickShow=true";
document.forms[0].submit();
return true;
}
function month_Appendix()
{
document.forms[0].go.value="true";
document.forms[0].action="MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&month=0&clickShow=true";
document.forms[0].submit();
return true;
}
function week_Appendix()
{
document.forms[0].go.value="true";
//alert('in month_Appendix()');
document.forms[0].action="MenuFunctionAddCommentAction.do?Type=Appendix&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&week=0&clickShow=true";
document.forms[0].submit();
return true;
}




</script>
<script>
function changeFilter()
{
	document.forms[0].action="MenuFunctionAddCommentAction.do?Type=prm_appendix&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&showList=something";
	document.forms[0].submit();
	return true;
}

function resetFilter()
{
	
	document.forms[0].jobOwnerOtherCheck.value="";
	document.forms[0].action="MenuFunctionAddCommentAction.do?Type=prm_appendix&MSA_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'MSA_Id' />&Appendix_Id=<bean:write name = 'MenuFunctionAddCommentForm' property = 'forwardId' />&resetList=something";
	document.forms[0].submit();
	return true;

}
</script>
<script>
function gettingAjaxData(){
	document.forms[0].jobOwnerOtherCheck.value = 'otherOwnerSelected';
        var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 document.getElementById("OwnerSpanId").innerHTML = response;
               }
        }
     
 var me="false";
 var all="false";
 var id ="";
  
	if(document.forms[0].jobSelectedOwners[0].checked){
 		me="true";
 	}
 	if(document.forms[0].jobSelectedOwners[1].checked){
 		all="true";
 	}
 	
 	id = document.forms[0].appendix_Id.value;
 	
        ajaxRequest.open("POST", "OwnerList.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("project=something&ownerType=prj_job_new&Id="+id+"&ME="+me+"&All="+all); 
}
 
</script>
 <script>

var str = '';

function setformaldocdate()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "FormalDocDateAction.do?Id=<%=MSA_Id%>&Type=MSA";
	document.forms[0].submit();
	return true;	
}

//Start:Added By Amit For Upload CSV
function uploadCSV()
{
    
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadCSV.do?msaid=<%= MSA_Id %>&fromProposalMg=Yes";
	document.forms[0].submit();
	return true;
}
// Folowing Method is for Bulk Job Creation


function addcomment()
{
	document.forms[0].target = "_self";	 
	document.forms[0].action = "MenuFunctionAddCommentAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	  
}

function viewcomments()
{
	//str = 'MenuFunctionViewCommentAction.do?Type=MSA&Id=<%=MSA_Id%>';
	//p = showModelessDialog( str,window, 'status:false;dialogWidth:550px; dialogHeight:500px' );
	
	document.forms[0].target = "_self";
	document.forms[0].action = "MenuFunctionViewCommentAction.do?Type=MSA&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;	 
}

function sendemail()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "EmailAction.do?Type=MSA&MSA_Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}

function view(v)
{
    if(v=='html')
	    document.forms[0].target = "_blank";
	else
	    document.forms[0].target = "_self";	    
    document.forms[0].action = "ViewAction.do?View="+v+"&Type=MSA&from_type=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;	
}

function editmsa()
{
    document.forms[0].target = "_self";
    document.forms[0].action = "EditXmlAction.do?Type=MSA&EditType=PM&Id=<%= MSA_Id%>";
	document.forms[0].submit();
	return true;
		
}

function upload()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "UploadAction.do?ref=FromMSA&Id=<%= MSA_Id %>&Status=<%= msastatus%>";
	document.forms[0].submit();
	return true;	
}

function del() 
{
		document.forms[0].target = "_self";
		var convel = confirm( "<bean:message bundle = "pm" key = "msa.detail.deletemsaprompt"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=MSA&MSA_Id=<%= MSA_Id%>";
			document.forms[0].submit();
			return true;	
		}
	
}

function Netmedxcheck() 
{	
		alert("<bean:message bundle="pm" key="msa.detail.cannotmanage"/>" );
		return false;	
}

function viewdocuments()
{
	document.forms[0].target = "_self";
	document.forms[0].action = "ViewuploadedocumentAction.do?ref=Viewmsadocument&Id=<%= MSA_Id %>";
	document.forms[0].submit();
	return true;
}
</script> 

</html:html>
