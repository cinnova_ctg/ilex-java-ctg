<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ page language = "java" %>


<%
	String id1 = ""; 
	String id2 = "";
	String type = "";  
	String ref = ""; 
	String from = "";

	String flag = "";
	String id3 = "";
	


	
	if( request.getAttribute( "id1" ) != null )
	{
		id1 = ( String ) request.getAttribute( "id1" );
	}

	if( request.getAttribute( "id2" ) != null )
	{
		id2 = ( String ) request.getAttribute( "id2" );
	}

	if( request.getAttribute( "ref" ) != null )
	{
		ref = ( String ) request.getAttribute( "ref" );
	}

	if( request.getAttribute( "from" ) != null )
	{
		from = ( String ) request.getAttribute( "from" );
	}

	if( request.getAttribute( "type" ) != null )
	{
		type = ( String ) request.getAttribute( "type" );
	}

	if( request.getAttribute( "flag" ) != null )
	{
		flag = ( String ) request.getAttribute( "flag" );
	}

	if( request.getAttribute( "id3" ) != null )
	{
		id3 = ( String ) request.getAttribute( "id3" );
	}
	

	
	if( type.equals( "prj_act" ) )
	{
	%>
			<jsp:forward page = "/ActivityDetailAction.do">
				<jsp:param name = "jobid" value = "<%= id2 %>" /> 
				<jsp:param name = "Activity_Id" value = "<%= id1 %>" />
				<jsp:param name = "ref" value = "<%= ref %>" />
				<jsp:param name = "from" value = "<%= from %>" />
			
			</jsp:forward>
	<%
		}
	
	if( type.equals( "Activity" ) )
	{
	%>
			<jsp:forward page = "/ActivityDetailAction.do">
				<jsp:param name = "jobid" value = "<%= id2 %>" /> 
				<jsp:param name = "Activity_Id" value = "<%= id1 %>" />
				<jsp:param name = "ref" value = "<%= ref %>" />
				<jsp:param name = "from" value = "<%= from %>" />
				<jsp:param name = "addcommentflag" value = "<%= flag %>" />
				<jsp:param name = "addendum_id" value = "<%= id3 %>" />
			</jsp:forward>
	<%
		}

	if( type.equals( "jobdashboard_resM" ) )
	{
	%>
			<jsp:forward page = "/MaterialDetailAction.do">
				<jsp:param name = "jobid" value = "<%= id2 %>" /> 
				<jsp:param name = "Material_Id" value = "<%= id1 %>" />
				<jsp:param name = "ref" value = "<%= ref %>" />
				<jsp:param name = "from" value = "<%= from %>" />
				
			</jsp:forward>
	<%
		}

	if( type.equals( "jobdashboard_resL" ) )
	{
	%>
			<jsp:forward page = "/LaborDetailAction.do">
				<jsp:param name = "jobid" value = "<%= id2 %>" /> 
				<jsp:param name = "Labor_Id" value = "<%= id1 %>" />
				<jsp:param name = "ref" value = "<%= ref %>" />
				<jsp:param name = "from" value = "<%= from %>" />
				
			</jsp:forward>
	<%
		}

	if( type.equals( "jobdashboard_resF" ) )
	{
	%>
			<jsp:forward page = "/FreightDetailAction.do">
				<jsp:param name = "jobid" value = "<%= id2 %>" /> 
				<jsp:param name = "Freight_Id" value = "<%= id1 %>" />
				<jsp:param name = "ref" value = "<%= ref %>" />
				<jsp:param name = "from" value = "<%= from %>" />
				
			</jsp:forward>
	<%
		}

	if( type.equals( "jobdashboard_resT" ) )
	{
	%>
			<jsp:forward page = "/TravelDetailAction.do">
				<jsp:param name = "jobid" value = "<%= id2 %>" /> 
				<jsp:param name = "Travel_Id" value = "<%= id1 %>" />
				<jsp:param name = "ref" value = "<%= ref %>" />
				<jsp:param name = "from" value = "<%= from %>" />
				
			</jsp:forward>
	<%
		}
	if(from.equals("jobdashboard"))
	{
	
		if( type.equals( "Job" ) )
		{
		%>
				<jsp:forward page = "/JobDashboardAction.do">
					<jsp:param name = "appendixid" value = "<%= id1 %>" /> 
					<jsp:param name = "jobid" value = "<%= id2 %>" />
					<jsp:param name = "function" value = "<%= ref %>" />
					<jsp:param name = "from" value = "<%= from %>" />
					
				</jsp:forward>
		<%
			}
	}
	if( type.equals( "prm_appendix" ) )
	{
	%>
			<jsp:forward page = "/AppendixHeader.do">
				<jsp:param name = "appendixid" value = "<%= id1 %>" /> 
				<jsp:param name = "function" value = "view" />
			</jsp:forward>
	<%
		}
	%>