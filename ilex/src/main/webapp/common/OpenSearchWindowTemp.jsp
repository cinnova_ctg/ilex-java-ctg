<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<html:html>
<% 
	String setvalue = "";
	String resource_label;
	int size=0;
	int offset1=0;
	int offset2=0;
	int resource=0;
	int columns=0;

	String checkedresource = "";
	int checkedcount=0;
	String bgcolor1temp="";
	String bgcolortemp="";
	String checknetmedx="N";
	int k=1;
	
	if(request.getAttribute( "size" ).toString()!=null)
	{
		size =( int ) Integer.parseInt( request.getAttribute( "size" ).toString() ); 
	}
	if(request.getAttribute("checknetmedx")!=null)
	{
		checknetmedx=request.getAttribute("checknetmedx").toString();
	}
	if(request.getAttribute( "restype" ).toString()!=null)
	{
		if(request.getAttribute( "restype" ).equals("Material"))
		{
			resource=1;
		}
		else if(request.getAttribute( "restype" ).equals("Labor"))
		{
			resource=2;
		}
		else if(request.getAttribute( "restype" ).equals("Freight"))
		{
			resource=3;
		}
		else if(request.getAttribute( "restype" ).equals("Travel"))
		{
			resource=4;
		}
	}
	switch(resource){
	case 1:
	{
		resource_label="common.searchwindow.materialname";
		break;
	}
	case 2:
	{
		resource_label="common.searchwindow.laborname";
		break;
	}
	case 3:
	{
		resource_label="common.searchwindow.freightname";
		break;
	}
	case 4:
	{
		resource_label="common.searchwindow.travelname";
		break;
	}
		default:
		{
			resource_label="common.searchwindow.materialname";
		}
	}
%>
	<head>
		<title><bean:message bundle = "pm" key = "common.searchwindow.searchcriteria"/></title>
		<%@ include file="../Header.inc" %>
		<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
		<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
</head>
<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" >
<html:form action = "/OpenSearchWindowTempAction" >
<html:hidden property = "type" />
<html:hidden property = "activity_Id" />
<html:hidden property = "from" />
<html:hidden property = "fromflag" />
<html:hidden property = "MSA_Id" />
<html:hidden property = "ref" />
<html:hidden property = "addendum_id" />
<html:hidden property = "checknetmedx" />
<html:hidden property = "resourceListType" />
<html:hidden property = "jobId" />
<table  border="0" cellspacing="1" cellpadding="1" >
  <tr><td>&nbsp;</td></tr>
  <tr>
  	<td  width="2" height="0"></td>
  	<td><table border="0" cellspacing="1" cellpadding="1" > 
              <tr>
		  		<td class = "colDark"  >
		  			<bean:message bundle = "pm" key = "common.searchwindow.category"/>
		  		</td>
		  		<td class = "colLight" >
				 	<html:select property = "category" size = "1" styleClass = "select" onchange = "javascript:return refresh();">
						<html:optionsCollection name = "OpenSearchWindowTempForm" property = "categorycombo" value = "value" label = "label"/>    
					</html:select>
				</td>
		  			
		  		<td class = "colDark" >
		  			<bean:message bundle = "pm" key = "common.searchwindow.subcategory"/>
		  		</td>
		  		<td class = "colLight" >
				 	<html:select property = "subcategory" size = "1" styleClass = "select"  onchange = "javascript:return refresh();">
						<html:optionsCollection name = "OpenSearchWindowTempForm" property = "subcategorycombo" value = "value" label = "label"/>    
					</html:select>
				</td>
		  			
		  		<td class = "colDark" >
		  			<bean:message bundle = "pm" key = "common.searchwindow.subsubcategory"/>
		  		</td>
		  		<td class = "colLight" >
				 	<html:select property = "subsubcategory"  size = "1" styleClass = "select">
						<html:optionsCollection name = "OpenSearchWindowTempForm" property = "subsubcategorycombo" value = "value" label = "label"/>    
					</html:select>
				</td>
		  	 </tr>
		  
			<tr>
				<td class = "colDark" >
				 	<bean:message bundle = "pm" key = "common.searchwindow.keyword"/>
				</td>
				<td class = "colLight" >
				 	<html:text property = "keyword" styleClass = "textbox" size="15"/>
				</td>
				<td class = "colDark"  colspan = "4">
				 	<html:submit property = "search" styleClass = "button_c">
				 		<bean:message bundle = "pm" key = "common.searchwindow.search"/>
				 	</html:submit>
				</td>
			</tr>
		</table>
	</td>
  </tr>
</table>  

<table  border="0" cellspacing="1" cellpadding="1"  >
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" > 
			<logic:equal name = "OpenSearchWindowTempForm" property = "searchclick" value = "true">
			<tr>
    			<td  class="labeleboldwhite" height="30" colspan="7"><bean:message bundle="pm" key="common.searchwindow.basicsearchresults" /></td>
  			</tr>
  		
			<tr>
				<td class = "Ntryb">&nbsp;</td>
				<td class = "Ntryb">
					<bean:message bundle = "pm" key="<%=resource_label %>"/>
				</td>
				
				<%if(request.getAttribute( "restype" ).equals("Material"))
				{ %>
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.manpartnumber"/>
				</td>
				<%} %>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.unitcost"/>
				</td>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.cnspartnumber"/>
				</td>
				<%if(checknetmedx.equalsIgnoreCase("Y")) 
				{%>
				<td class = "Ntryb">
					Criticality
				</td>
				
				<td class = "Ntryb">
					Customer Labor Rate
				</td>
				<%}
				else
					{ %>
					
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.sellablequantity"/>
				</td>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.minimumquantity"/>
				</td>
				<%} %>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.alternateidentifier"/>
				</td>
				
				<td class = "Ntryb" colspan="2">
					<bean:message bundle = "pm" key = "common.searchwindow.lastmodifiedby"/>
				</td>
			</tr>
			
			<%int i=1;
		  	int j = 0;
			String bgcolor = ""; 
			
			
			%>
			<logic:iterate id = "resourcelist" name = "OpenSearchWindowTempForm" property = "resourcelist">
		 	<%if((i%2)==0)
				{
					
					bgcolor="Ntexto"; 	
				} 
			  else
				{
					bgcolor="Ntexte";
					
				}
				if( size == 1 ) 
				{ 
					//setvalue = "javascript:assignresource( '' );";
					checkedresource = "javascript:collectcheckedresources( '' );";
				}
				else
				{
					//setvalue = "javascript:assignresource( '"+j+"' );";
					checkedresource = "javascript:collectcheckedresources( '"+j+"' );";
				}%>
			
				<bean:define id = "resource_Id" name = "resourcelist" property = "resource_id" type = "java.lang.String"/>
				<html:hidden name = "resourcelist" property = "resource_id" />
				<html:hidden name = "resourcelist" property = "detail_id" />
				<html:hidden name = "resourcelist" property = "criticality" />
				<html:hidden name = "resourcelist" property = "clr" />
				<tr>
					<td class="<%=bgcolor %>">
						<!--<html:radio property = "resource_id" value = "<%= resource_Id %>" onclick = "<%= setvalue %>"/>-->
						<html:checkbox property = "check" value = "<%= resource_Id %>" onclick="<%= checkedresource %>"/>
					</td>
					
					<td  class="<%=bgcolor %>" nowrap>
						<bean:write name = "resourcelist" property = "resource_name" />
						<html:hidden name = "resourcelist" property = "resource_name" />
					</td>
					
					<%if(request.getAttribute( "restype" ).equals("Material"))
					{ %>
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "manufacturer_part_number" />
						<html:hidden name = "resourcelist" property = "manufacturer_part_number" />
					</td>
					<%} %>
					
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "unit_cost" />
						<html:hidden name = "resourcelist" property = "unit_cost" />
					</td>
					
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "cns_part_number" />
						<html:hidden name = "resourcelist" property = "cns_part_number" />
					</td>
					<%if(checknetmedx.equalsIgnoreCase("Y")) 
					{%>
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "criticality" />
						
						<html:hidden name = "resourcelist" property = "sellable_quantity" />
					</td>
					
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "clr" />
						
						<html:hidden name = "resourcelist" property = "minimum_quantity" />
					</td>
					<%}
					else
					{ %>
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "sellable_quantity" />
						<html:hidden name = "resourcelist" property = "sellable_quantity" />
					</td>
					
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "minimum_quantity" />
						<html:hidden name = "resourcelist" property = "minimum_quantity" />
					</td>
					
					<%} %>
					
					<td  class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "alternate_identifier" />
						<html:hidden name = "resourcelist" property = "alternate_identifier" />
					</td>
					
					<td class="<%=bgcolor %>" nowrap>
						<bean:write name = "resourcelist" property = "updatedBy" />
					</td>	
					<td class="<%=bgcolor %>">
						<bean:write name = "resourcelist" property = "updatedDate" /> 
					</td>
				</tr>
				<%i++;j++; %>
			</logic:iterate>
			<%if(i==1)
			{ %>
				<tr>
					<td>&nbsp;</td>
					<td  class="message" height="30" colspan="7"><bean:message bundle="pm" key="common.searchwindow.norecord" /></td>
				</tr>
			<%} %>
			
			<%if(i>1)
			{%>
			<tr>
				<td colspan = "1" class = "colDark">
					<html:button property = "ok" styleClass = "button_c" onclick="javascript:targetset();"><bean:message bundle = "pm" key = "common.searchwindow.ok"/></html:button>
				</td>
				<td colspan = "10" class = "colDark">
					<html:submit property = "searchmore" styleClass = "button_c" ><bean:message bundle = "pm" key = "common.searchwindow.searchmore"/></html:submit>
				</td>
			</tr>
			<% }%>
			</logic:equal>
 		</table> 
 	  </td>
   </tr>
   
      <%
		bgcolortemp="Ntexto";
	  	bgcolor1temp="Ntexto";	
	  %>
					  
				
    <table  border="0" cellspacing="1" cellpadding="1"  >
    <tr>
		<td height="10">
		&nbsp;
		</td>
	</tr>
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" id = "child"> 
	  		<tr style="display:block;">
    			<td  class="labeleboldwhite" height="30" colspan="7"><bean:message bundle="pm" key="common.searchwindow.selectedresources" /></td>
  			</tr>
	  	  		
	 <%-- <logic:equal name = "OpenSearchWindowTempForm" property = "searchclick" value = "true">--%>
	  		
	  		<tr style="display:block;">
	  		
				<td width="20">&nbsp;</td>
				<td class = "Ntryb">
					<bean:message bundle = "pm" key="<%=resource_label %>"/>
				</td>
				
				<%if(request.getAttribute( "restype" ).equals("Material"))
				{ %>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.manpartnumber"/>
				</td>
				<%} %>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.unitcost"/>
				</td>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.cnspartnumber"/>
				</td>
				<%if(checknetmedx.equalsIgnoreCase("Y")) 
				{%>
				<td class = "Ntryb">
					Criticality
				</td>
				
				<td class = "Ntryb">
					Customer Labor Rate
				</td>
				<%}
				else
				{ %>
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.sellablequantity"/>
				</td>
				
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.minimumquantity"/>
				</td>
				
				<%} %>
								
				<td class = "Ntryb">
					<bean:message bundle = "pm" key = "common.searchwindow.alternateidentifier"/>
				</td>
				
				<td class = "Ntryb" colspan="2">
					<bean:message bundle = "pm" key = "common.searchwindow.lastmodifiedby"/>
				</td>
				
			</tr>
	  			<%-- </logic:equal> --%> 
						<logic:present name="OpenSearchWindowTempForm" property = "resourcelist" >
			<logic:iterate id = "resourcelist" name = "OpenSearchWindowTempForm" property = "resourcelist">
				<tr style="display:none;">
										
						<td width="20">
							&nbsp;
						</td>
						<td  class="<%=bgcolor1temp %>" nowrap>
							<bean:write name = "resourcelist" property = "resource_name" />
							
						</td>
						
						<%if(request.getAttribute( "restype" ).equals("Material"))
						{ %>
						<td  class="<%=bgcolor1temp %>">
							<bean:write name = "resourcelist" property = "manufacturer_part_number" />
							
						</td>
						<%} %>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "resourcelist" property = "unit_cost" />
							
						</td>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "resourcelist" property = "cns_part_number" />
							
						</td>
						
						<%if(checknetmedx.equalsIgnoreCase("Y")) 
						{%>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "resourcelist" property = "criticality" />
							
						</td>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "resourcelist" property = "clr" />
							
						</td>
						<%}
						else
						{ %>
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "resourcelist" property = "sellable_quantity" />
							
						</td>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "resourcelist" property = "minimum_quantity" />
							
						</td>
						<%} %>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "resourcelist" property = "alternate_identifier" />
						</td>
						
						<td  class="<%=bgcolor1temp %>" nowrap>
							<bean:write name = "resourcelist" property = "updatedBy" />
						</td>
						<td  class="<%=bgcolor1temp %>">
							<bean:write name = "resourcelist" property = "updatedDate" /> 
						</td>
						
				</tr>
				
				<%checkedcount++; k++;%>
			
			</logic:iterate>
			</logic:present>
				<logic:present name = "templist" >
				
			 <logic:iterate id = "tlist" name = "templist" >
			
				
				<tr>
										
						<td width="20">
							&nbsp;
						</td>
						<td  class="<%=bgcolor1temp %>" nowrap>
							<bean:write name = "tlist" property = "resource_name" />
						</td>
						
						<%if(request.getAttribute( "restype" ).equals("Material"))
						{ %>
						<td  class="<%=bgcolor1temp %>">
							<bean:write name = "tlist" property = "manufacturer_part_number" />
						</td>
						<%} %>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "tlist" property = "unit_cost" />
						</td>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "tlist" property = "cns_part_number" />
						</td>
						
						<%if(checknetmedx.equalsIgnoreCase("Y")) 
						{%>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "tlist" property = "criticality" />
						</td>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "tlist" property = "clr" />
						</td>
						<%} 
						else
						{%>
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "tlist" property = "sellable_quantity" />
						</td>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "tlist" property = "minimum_quantity" />
						</td>
						<%} %>
						
						<td  class="<%=bgcolortemp %>">
							<bean:write name = "tlist" property = "alternate_identifier" />
						</td>
						
						<td  class="<%=bgcolor1temp %>" nowrap>
							<bean:write name = "tlist" property = "updatedBy" />
						</td>
						<td  class="<%=bgcolor1temp %>">
							<bean:write name = "tlist" property = "updatedDate" /> 
						</td>
				</tr>
				</logic:iterate>
			</logic:present>
  			</table> 
 	  </td>
   </tr>
</html:form>
</table>

<script>

function refresh()
{
	console.log('refresh!');
	document.forms[0].submit();
	return true;
}

function targetset()
{
	var url='OpenSearchWindowTempAction.do?ok=Ok'
	document.forms[0].action=url;
	document.forms[0].submit();
	//window.close();
	return true;
}

function assignresource( temp )
{
    console.log('assignresource!');
	var address = '';
	
	
	if( temp == '' )
	{
		if(document.forms[0].type.value=='Material')
		{
			
			window.opener.document.forms[0].newmaterialnamecombo.value = document.forms[0].resource_id.value;
			
			window.opener.document.forms[0].newmaterialname.value = document.forms[0].resource_name.value;
			window.opener.document.forms[0].newmaterialid.checked = true;
			window.opener.refresh();
		}
		if(document.forms[0].type.value=='Labor')
		{
			
			if(document.forms[0].clr.value=='yes')
			{
				var off1=document.forms[0].offset1.value;
				var off2=document.forms[0].offset2.value;
				var str=off1+"["+off2+"]";
				var str1="";
				eval('window.opener.document.forms[0].resourceid'+str).value = document.forms[0].resource_id.value;
				eval('window.opener.document.forms[0].resourcename'+str).value = document.forms[0].resource_name.value;
				
			}
			else
			{
				window.opener.document.forms[0].newlabornamecombo.value = document.forms[0].resource_id.value;
				
				window.opener.document.forms[0].newlaborname.value = document.forms[0].resource_name.value;
				window.opener.document.forms[0].newlaborid.checked = true;
				window.opener.document.forms[0].newcnspartnumber.value = document.forms[0].cns_part_number.value;
				
				window.opener.refresh();
			}
			
			
			
			
		}
		if(document.forms[0].type.value=='Freight')
		{
			
			window.opener.document.forms[0].newfreightnamecombo.value = document.forms[0].resource_id.value;
			
			window.opener.document.forms[0].newfreightname.value = document.forms[0].resource_name.value;
			window.opener.document.forms[0].newfreightid.checked = true;
			window.opener.refresh();
		}
		if(document.forms[0].type.value=='Travel')
		{
			
			window.opener.document.forms[0].newtravelnamecombo.value = document.forms[0].resource_id.value;
			
			window.opener.document.forms[0].newtravelname.value = document.forms[0].resource_name.value;
			window.opener.document.forms[0].newtravelid.checked = true;
			window.opener.refresh();
		}
		
	
	}
	
	else
	{
		if(document.forms[0].type.value=='Material')
		{
			
			window.opener.document.forms[0].newmaterialnamecombo.value = document.forms[0].resource_id[temp].value;
			window.opener.document.forms[0].newmaterialname.value = document.forms[0].resource_name[temp].value;
			window.opener.document.forms[0].newmaterialid.checked = true;
			window.opener.refresh();
		}
		
		
		if(document.forms[0].type.value=='Labor')
		{
			
			
			if(document.forms[0].clr.value=='yes')
			{
				var off1=document.forms[0].offset1.value;
				var off2=document.forms[0].offset2.value;
				var str=off1+"["+off2+"]";
				var str1="";
				eval('window.opener.document.forms[0].resourceid'+str).value = document.forms[0].resource_id[temp].value;
				eval('window.opener.document.forms[0].resourcename'+str).value = document.forms[0].resource_name[temp].value;
				
			}
			else
			{
				
				window.opener.document.forms[0].newlabornamecombo.value = document.forms[0].resource_id[temp].value;
				window.opener.document.forms[0].newlaborname.value = document.forms[0].resource_name[temp].value;
				window.opener.document.forms[0].newcnspartnumber.value = document.forms[0].cns_part_number[temp].value;
				
				window.opener.document.forms[0].newlaborid.checked = true;
				
				
				window.opener.refresh();
			}
			
		}
		
		if(document.forms[0].type.value=='Freight')
		{
			
			window.opener.document.forms[0].newfreightnamecombo.value = document.forms[0].resource_id[temp].value;
			window.opener.document.forms[0].newfreightname.value = document.forms[0].resource_name[temp].value;
			window.opener.document.forms[0].newfreightid.checked = true;
			window.opener.refresh();
		}
		
		if(document.forms[0].type.value=='Travel')
		{
			
			window.opener.document.forms[0].newtravelnamecombo.value = document.forms[0].resource_id[temp].value;
			window.opener.document.forms[0].newtravelname.value = document.forms[0].resource_name[temp].value;
			window.opener.document.forms[0].newtravelid.checked = true;
			window.opener.refresh();
		}
			
	}
	 
	window.close();
	
}
function collectcheckedresources(temp)
{

	
	if(temp=='')
	{
		if(document.forms[0].check.checked)
		{
			
			document.getElementById('child').rows[0].style.display = 'block';
			document.getElementById('child').rows[1].style.display = 'block';
			document.getElementById('child').rows[2].style.display = 'block';
		}
		else
		{
			
			document.getElementById('child').rows[0].style.display = 'block';
			document.getElementById('child').rows[1].style.display = 'block';
			document.getElementById('child').rows[1].style.display = 'none';
		}
	}
	else
	{
		
		
		if(document.forms[0].check[temp].checked)
		{
			
			document.getElementById('child').rows[eval(temp)+2].style.display = 'block';
		}
		else
		{
			
			document.getElementById('child').rows[eval(temp)+2].style.display = 'none';
		}
		
	}
	
}


</script>


</body>

</html:html>

