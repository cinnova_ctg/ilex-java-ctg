<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For creating and  updating  MSAs.
*
-->



<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<% 
//String mssize="javascript: moreAttach();";


%>


<html:html>

<head>

	<title><bean:message bundle = "pm" key = "msa.tabular.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>

<%
	String backgroundclass = null;
	String backgroundclass1 = null;
	boolean csschooser = true;
	int i = 0;
	String[] option1 =null;
	int j = 0;

%>


<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0">
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	<%--  <tr>
    	<td class = "toprow"> 
	    	<table width = "" border = "0" cellspacing = "1" cellpadding = "0" height = "18">
	        	<tr align = "center"> 
		    		<td class = "toprow1" width = "150"><a href = "AppendixHeader.do?appendixid=<bean:write name = "ChecklistForm" property = "appendix_Id" />&function=view" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "drop"><bean:message bundle = "pm" key = "common.mastertoappendixcopy.backtoappendixdashboard"/></a></td>		
		    		<td class = "toprow1" width = "650">&nbsp;&nbsp;</td>
	        	</tr>
	      	</table>
    	</td>
	</tr>--%>
</table>



<table>
<html:form action = "/AddQcChecklistQues">
<html:hidden property = "newrowsadded" value = "0"/>

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
	<tr>
		<td>
			<table  border = "0" cellspacing = "1" cellpadding = "1" width = "850">
    			<logic:present name = "addflag" scope = "request">
		    		<tr> 
    					
    					<td colspan = "5" class = "message" height = "30">
			    			<logic:equal name = "addflag" value = "0">
			    			<bean:message bundle = "pm" key = "common.qcchecklistques.addsuccess"/>				
			    			</logic:equal>
		    			
							<logic:equal name = "addflag" value = "-9001">
								<bean:message bundle = "pm" key = "common.qcchecklistques.addfailure1"/>	
							</logic:equal>
						
							<logic:equal name = "addflag" value = "-9002">
								<bean:message bundle = "pm" key = "common.qcchecklistques.addfailure2"/>	
							</logic:equal>
						</td>
					</tr>
						
		    	</logic:present>
		    	
    			<tr>
    				<td colspan="3">
    					<table border = "0" width = "700">
    								
    			
		    			<tr>
		    				<td class = "labeleboldwhite" colspan = "3" height = "30">
		    					<bean:message bundle = "pm" key = "common.qcchecklistques.header"/>
		    				</td>
		  					
		  				</tr>
		  
						<tr> 
		    				<td class = "tryb"  width = "200"><bean:message bundle = "pm" key = "common.qcchecklistques.groupname"/></td>
		    
							<td class = "tryb" width = "350"><bean:message bundle = "pm" key = "common.qcchecklistques.itemname"/></td>
							<td class = "tryb" width = "150"><bean:message bundle = "pm" key = "common.qcchecklistques.options"/></td>
							
		  				</tr>
					
						
							<%	
								if ( csschooser == true ) 
								{
									backgroundclass = "hyperodd";
									backgroundclass1 = "readonlytextodd";
									csschooser = false;
									
								}
						
								else
								{
									backgroundclass = "hypereven";
									backgroundclass1 = "readonlytexteven";
									csschooser = true;	
								}
							%>
							
							
							
							
							<tr>
								
								<td class = "<%= backgroundclass %>">
									
									<html:text  styleClass = "textbox"  size = "30"  property = "group" />
								</td>
								
								<td class = "<%= backgroundclass %>">
									<html:textarea  styleClass = "textbox"  property = "item" rows = "1" cols = "55" />
								</td>
								
								<td class = "<%= backgroundclass %>">
									<html:button property = "addOption" styleClass = "button" onclick = "return showOption();" ><bean:message bundle = "pm" key = "common.qcchecklistques.addoption"/></html:button>
								</td>
								
							</tr>
							
							<tr>	
								<td class = "buttonrow" width = "700" colspan = "3">
					  				<%--<html:submit property = "submit" styleClass = "button" onclick = "return validate();"><bean:message bundle = "pm" key = "common.mastertoappendixcopy.save"/></html:submit>
									<html:button property="addMoreOption" styleClass="button" onclick="<%=mssize%>" ><bean:message bundle = "pm" key = "common.mastertoappendixcopy.addmore"/> </html:button>--%>
									<html:submit property = "back" styleClass = "button" onclick = "javascript: Backaction();"><bean:message bundle = "PRM" key = "prm.button.back"/></html:submit>
								</td>
								
							</tr>
							<tr>
								<td width = "700" colspan = "3" width = "50">
					  				&nbsp;
								</td>
							</tr>
						
					  </table>
					  
					  
					 
    					<table id = "dynatable" border = "0" style = "display:none">
    						    				  
						<tr>
						<td colspan="3" class = "textwhitetop"> 
		    				<html:radio property = "optiontype" value = "S" /><bean:message bundle = "pm" key = "common.qcchecklistques.single"/>&nbsp;<html:radio property="optiontype" value="M" /><bean:message bundle = "pm" key = "common.qcchecklistques.multiple"/>
						</td>
						</tr>
						<tr> 
							<td width = "20">&nbsp;</td>
		    				<td class = "tryb"  width = "200"><bean:message bundle = "pm" key = "common.qcchecklistques.option"/></td>
		    
							<td width = "150"></td>
						</tr>
						
						<tr> 
		    				<td>
									<html:multibox styleId = '<%= "check"+i %>' property = "check">
										<%= i %>
									</html:multibox>
								</td>
		    				<td class = "<%= backgroundclass %>">
									<html:text  styleId = '<%= "option"+i %>' styleClass = "textbox"  size = "50" maxlength = "100" property = "option" />
							</td>
		    				<% String mssize = "javascript: moreAttach( i );";
		    				 String no_options = "javascript: validate( document.forms[0].newrowsadded.value );";
		    				 %>
		    				
							<td ><html:button property = "addmore" styleClass = "button" onclick = "<%= mssize %>" ><bean:message bundle = "pm" key = "common.qcchecklistques.addmore"/></html:button></td>
						</tr>
													
					 </table>
					 <table id = "tab2" style = "display:none">
					 	<tr>
					 		<td width = "20">&nbsp;</td>
					 		<td colspan = "2">
					 		<html:submit property = "save" styleClass = "button" onclick = "return validate( document.forms[0].newrowsadded.value );" ><bean:message bundle = "pm" key = "common.qcchecklistques.save"/></html:submit>	
					 		<%--<html:submit property = "save" styleClass = "button" onclick="<%=no_options%>" ><bean:message bundle = "pm" key = "common.qcchecklistques.save"/></html:submit> --%>
					 		</td>
					 	</tr>
					</table>	
							
					</td>
				</tr>
				
							
			</table>
		</td>
	</tr>

		<jsp:include page = '/Footer.jsp'>
		      <jsp:param name = 'colspan' value = '37'/>
		      <jsp:param name = 'helpid' value = 'msatabularpage'/>
		 </jsp:include>	

</table>

<html:hidden property = "appendix_Id" />


</html:form>
</table>

</body>


<script>

function validate(no_of_options)
{
	var checkflag = 'false';
	//alert("no_of_options"+no_of_options);
	
		if(document.forms[0].group.value=="" )
		{
			alert( "<bean:message bundle = "pm" key = "common.qcchecklistques.groupcannotbeblank"/>");
			
			document.forms[0].group.focus();
			return false;
		}
		if(document.forms[0].item.value=="")
		{
			
			alert( "<bean:message bundle = "pm" key = "common.qcchecklistques.itemcannotbeblank"/>");
			
			document.forms[0].item.focus();
			return false;
		}
		
		
		if(no_of_options==1)
		{
			if( !document.forms[0].check.checked )
			{
				
				alert( "<bean:message bundle = "pm" key = "common.qcchecklistques.selectoptions"/>" );
				return false;
			}
			else
			{
				if(document.forms[0].option.value=="")
				{
					alert( "<bean:message bundle = "pm" key = "common.qcchecklistques.optioncannotbeblank"/>");
					document.forms[0].option.focus();
					return false;
				}
			
			}
		
		
		}
		else
		{
			for( var i = 0; i < document.forms[0].check.length; i++ )
			{
				if( document.forms[0].check[i].checked )
				{
					if(document.forms[0].option[i].value=="")
					{
						alert( "<bean:message bundle = "pm" key = "common.qcchecklistques.optioncannotbeblank"/>");
						document.forms[0].option[i].focus();
						
						return false;
					}
				}
				if( document.forms[0].check[i].checked )
				{
					checkflag = 'true';
				}
			}
			if( checkflag == 'false' )
			{
				alert( "<bean:message bundle = "pm" key = "common.qcchecklistques.selectoptions"/>" );
				return false;
			}
		
		}
	
	
	
	return true;
}


function Backaction()
{

	document.forms[0].action = 'MasterchecklisttoAppendixcopyAction.do';
	//document.forms[0].submit();
	return true;
}

function moreAttach(){

	var arg = moreAttach.arguments;
	var i = (dynatable.rows.length-2) ;
	//var k = (dynatable.rows.length-1) ;

	if (arg[0]==1){
		if (i>=1){
		 return;
		 }		
	}
	if (arg[0]!=1)
	//if(!validateDivC())	return false;
	
	i = (dynatable.rows.length-2);
	//k = (dynatable.rows.length-1) ;
    var cls='';
	if(dynatable.rows.length%2==0)	cls= 'hyperodd';
	if(dynatable.rows.length%2==1)	cls= 'hypereven';
	
	var oRow=dynatable.insertRow();
	var oCell=oRow.insertCell(0);
	
	oCell.innerHTML="<input type='checkbox' id='check"+i+"' name='check' value="+i+" >";
	//oCell.className=cls;

	oCell=oRow.insertCell(1);
	oCell.innerHTML="<input type='text' id='option"+i+"' name='option' size=50 maxlength=100 class='textbox'>";
	
	oCell.className=cls;

	//oCell=oRow.insertCell(2);
	//oCell.innerHTML="";
	//oCell.className=cls;

		
	//alert('document.forms[0].newrowsadded.value='+document.forms[0].newrowsadded.value);
	document.forms[0].newrowsadded.value=eval(document.forms[0].newrowsadded.value)+1;
	
}	// END OF FUNCTION moreAttach()


function showOption()
{
document.forms[0].addOption.disabled="true";
document.all.dynatable.style.display="block";
document.all.tab2.style.display="block";
document.forms[0].newrowsadded.value=1;
}

</script>


</html:html>
  