<!DOCTYPE HTML>
<%@ page import = "com.mind.common.EnvironmentSelector" %>
<%@ page import = "java.util.ResourceBundle" %>
<%@ page import="com.mind.common.IlexConstants"%>
<%
 String googleKey = EnvironmentSelector.getBundleString("googlekey." + IlexConstants.SERVER_NAME);
%>

<script type="text/javascript" 
	src='http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=<%=googleKey %>'>
</script>
	