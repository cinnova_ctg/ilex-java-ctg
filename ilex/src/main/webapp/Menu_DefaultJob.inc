<div id="menunav">
    <ul>
  		<li>
        	<a href="#" class="drop"><span>Job</span></a>
  			<ul>
      			<li><a href="javascript:alert('<bean:message bundle = "PRM" key = "prm.Cannotchanestatusfordefault"/>')"><bean:message bundle = "PRM" key = "prm.changestatus"/></a></li>
      			<li><a href="javascript:alert('<bean:message bundle = "PRM" key = "prm.CannotupdateJobschedulefordefault"/>')"><bean:message bundle = "PRM" key = "prm.updateschedule"/></a></li>
		  		<li>
		        	<a href="#"><span>Documents</span></a>
		        	<ul>
		      			<li><a href="javascript:viewdocumentsDefaultJob(<%=Job_Id%>);">View</a></li>
		      			<li><a href="javascript:uploadDefaultJob(<%=Job_Id%>);">Upload</a></li>
		  			</ul>
		  		</li>
  			</ul>
  		</li>
  		<li>
        	<a href="#" class="drop"><span>Status Report</span></a>
  			<ul>
      			<li><a href="javascript:manageDefaultJobStatus( 'update' )">Update</a></li>
      			<li><a href="javascript:sendStatusReportDefaultJob()">Send</a></li>
      			<li><a href="javascript:manageDefaultJobStatus( 'view' )">View</a></li>
  			</ul>
  		</li>
	</ul>
</div>