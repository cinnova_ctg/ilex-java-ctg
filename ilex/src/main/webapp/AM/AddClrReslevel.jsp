<!DOCTYPE HTML>
<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: 
*
-->

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>



<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<TITLE></TITLE>
</head>
<script>
function del() 
{
				
		if(document.forms[0].clrresource.value!=null)
		{
			if(document.forms[0].clrresource.value!="0")
			{
				var msg="<bean:message bundle="AM" key="am.reslvl.deletemsg"/>"
				var convel = confirm( msg );
				if( convel )
				{
					
					document.forms[0].action = "AddClrReslevelAction.do?ref=delete&resid="+document.forms[0].clrresource.value;
					document.forms[0].submit();
					return true;	
				}
			}
			else
			{
				alert( "<bean:message bundle="AM" key="am.reslvl.selectdeletion"/>" );
			}
			
		}
		else
		{
			alert( "<bean:message bundle="AM" key="am.reslvl.selectdeletion"/>" );
			
		}
}

</script>

<%@ include  file="../Menu.inc" %>

<bean:define id="dcam" name = "dynamicclasscombo" scope = "request" />
<bean:define id="dcres" name = "dynamicresoursecombo" scope = "request" />


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >

 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center"> 
	        <td  colspan=3 width=200 align=left class="toprow1" ><a href="AddClrReslevelAction.do?ref=add"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="AM" key="am.reslvl.addnewskill"/> </a></td>
	        <td   width=200 align=left class="toprow1" ><a href="javascript:del();" onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="AM" key="am.reslvl.deleteskill"/> </a></td>
			<td  width=350 class="toprow1" >&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table>
<html:form      action="AddClrReslevelAction">


<html:hidden property="ref" />
<html:hidden property="refresh" />

<table border="0" cellspacing="1" cellpadding="1" >
<tr>
  <td width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" > 
  
	    <logic:present name = "AddClrReslevelForm" property="addmessage">
		<logic:equal name="AddClrReslevelForm" property="addmessage" value="0">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="AM" key="am.reslvl.addedsuccessfully"/></td>
	  		</tr> 
		</logic:equal>
		<logic:equal name="AddClrReslevelForm" property="addmessage" value="-9001">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="AM" key="am.reslvl.insertfailed"/></td>
	  		</tr> 
		</logic:equal>
		</logic:present>
		
		<logic:present name = "AddClrReslevelForm" property="deletemessage">
		<logic:equal name="AddClrReslevelForm" property="deletemessage" value="-9001">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="AM" key="am.reslvl.deletefailed"/></td>
	  		</tr> 
		</logic:equal>
		
		<logic:equal name="AddClrReslevelForm" property="deletemessage" value="0">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="AM" key="am.reslvl.deletedsuccessfully"/></td>
	  		</tr> 
		</logic:equal>
		</logic:present>
  
		  <tr>  
		    <td colspan="2" class="labeleboldwhite" height="30" ><bean:message bundle="AM" key="am.reslvl.Addskills"/></td>
		  </tr> 
		 
		  		  
		  <tr> 
		    <td  class="labelebold" height="25"><bean:message bundle="AM" key="am.reslvl.class"/></td>
		    <td  align=left class="labele">
			<html:select name="AddClrReslevelForm" property="clrclass" size="1" styleClass="comboe" onchange="return onComboChange();">
				<html:optionsCollection name = "dcam"  property = "classlist"  label = "label"  value = "value" /></html:select></td>	
		  </tr>
		  
		  <tr> 
		    <td  class="labelobold" height="25"><bean:message bundle="AM" key="am.reslvl.existingskill"/></td>
		    <td  align=left class="labelo">
			<html:select name="AddClrReslevelForm" property="clrresource" size="1" styleClass="comboe" onchange="return onResComboChange();">
				<html:optionsCollection name = "dcres"  property = "resourcelist"  label = "label"  value = "value" /></html:select></td>	
		  </tr>
		  
		  <tr> 
		    <td  class="labelebold" height="25"><bean:message bundle="AM" key="am.reslvl.newskill"/></td>
		    <td  class="labele">
			<html:text  styleClass="textbox" size="33" name="AddClrReslevelForm" property="name" maxlength="50"/></td>
		  </tr>
		  
		   <tr> 
		    <td  class="labelobold" height="25"><bean:message bundle="AM" key="am.reslvl.skillshortname"/></td>
		    <td  class="labelo">
			<html:text  styleClass="textbox" size="20" name="AddClrReslevelForm" property="shortname" maxlength="50"/></td>
		  </tr>
		  
		  <tr> 
		    <td colspan="2" class="buttonrow"> 
		     <html:submit property="save" styleClass="button" onclick = "return validate();"><bean:message bundle="AM" key="am.reslvl.save"/></html:submit>
		     <html:reset property="reset" styleClass="button"><bean:message bundle="AM" key="am.reslvl.reset" /></html:reset>
		  </tr>
		
		  <jsp:include page = '/Footer.jsp'>
		  	 <jsp:param name = 'colspan' value = '28'/>
		     <jsp:param name = 'helpid' value = 'roleprivilege'/>
		  </jsp:include>
  
  	  </table>
    </td>
  </tr>
</table>


</html:form>
</table>
</BODY>

<script>


function validate()
{
	
	trimFields();
		
	if(document.forms[0].clrclass.value==0)
	{
		alert("<bean:message bundle="AM" key="am.reslvl.selectclass"/>");		
		
		return false;
	}
	
	if(document.forms[0].name.value==null || document.forms[0].name.value=="")
	{
		alert("<bean:message bundle="AM" key="am.reslvl.namecannotblank"/>");
		
		return false;
	}
	if(document.forms[0].shortname.value==null || document.forms[0].shortname.value=="")
	{
		alert("<bean:message bundle="AM" key="am.reslvl.shortnamecannotblank"/>");
		
		return false;
	}
	return true;	
	
}
</script>
<script>
function trimFields()
{

	var field=document.forms[0];

	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text')
		{
			var temp1=field[i].value;

			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--){
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		field[i].value=temp2;
		}
	}
return true;
}

</script>
<!-- FUNCTION TRIM END -->

<script>

function CheckQuotes (obj,label)
{
	if(invalidChar(obj.value,label))
	{
		
		obj.focus();
		return false;
	}
	return true;
}

function onComboChange ()
{
	document.forms[0].clrresource.value="0";
	document.forms[0].name.value="";
	document.forms[0].shortname.value="";
	return true;
}

function onResComboChange ()
{
	
	document.forms[0].refresh.value="true";
	document.forms[0].submit();
	return true;
}

</script>

</html:html>
