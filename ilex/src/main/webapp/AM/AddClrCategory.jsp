<!DOCTYPE HTML>
<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: 
*
-->

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>



<html:html>
<HEAD>

<%@ include  file="/Header.inc" %>

<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet" type="text/css">
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language="javascript" src="javascript/ilexGUI.js"></script>
<TITLE></TITLE>
</head>
<script>

function del() 
{
				
		if(document.forms[0].clrexistingcategory.value!=null)
		{
			if(document.forms[0].clrexistingcategory.value!="0")
			{
				var msg="<bean:message bundle="AM" key="am.cat.deletemsg"/>"
				var convel = confirm( msg );
				if( convel )
				{
					
					document.forms[0].action = "AddClrCategoryAction.do?ref=delete&catshortname="+document.forms[0].clrexistingcategory.value;
					document.forms[0].submit();
					return true;	
				}
			}
			else
			{
				alert( "<bean:message bundle="AM" key="am.cat.selectdeletion"/>" );
			}
			
		}
		else
		{
			alert( "<bean:message bundle="AM" key="am.cat.selectdeletion"/>" );
			
		}
}



</script>

<%@ include  file="../Menu.inc" %>


<bean:define id="dccat" name = "dynamiccategorycombo" scope = "request" />




<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >

 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="" border="0" cellspacing="1" cellpadding="0" height="18">
        <tr align="center"> 
	        <td  colspan=3 width=200 align=left class="toprow1" ><a href="AddClrCategoryAction.do?ref=add"  onmouseout="MM_swapImgRestore();"    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="AM" key="am.cat.addonsitearrival"/> </a></td>
	        <td   width=200 align=left class="toprow1" ><a href="javascript:del();" onmouseout="MM_swapImgRestore(); "    onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);" class="drop"><bean:message bundle="AM" key="am.cat.deleteonsitearrival"/> </a></td>
			<td  width=350 class="toprow1" >&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table>
<html:form      action="AddClrCategoryAction">


<html:hidden property="ref" />
<html:hidden property="refresh" />

<table border="0" cellspacing="1" cellpadding="1" >
<tr>
  <td width="2" height="0"></td>
  <td><table border="0" cellspacing="1" cellpadding="1" > 
  
	    
	    <logic:present name = "AddClrCategoryForm" property="addmessage">
		<logic:equal name="AddClrCategoryForm" property="addmessage" value="0">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="AM" key="am.cat.arrivaloptionaddedsuccessfully"/></td>
	  		</tr> 
		</logic:equal>
		<logic:equal name="AddClrCategoryForm" property="addmessage" value="-9001">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="AM" key="am.cat.insertfailed"/></td>
	  		</tr> 
		</logic:equal>
		</logic:present>
		
		<logic:present name = "AddClrCategoryForm" property="updatemessage">
		<logic:equal name="AddClrCategoryForm" property="updatemessage" value="0">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="AM" key="am.cat.arrivaloptionupdatedsuccessfully"/></td>
	  		</tr> 
		</logic:equal>
		<logic:equal name="AddClrCategoryForm" property="updatemessage" value="-9002">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="AM" key="am.cat.updatefailed"/></td>
	  		</tr> 
		</logic:equal>
		</logic:present>
		
		<logic:present name = "AddClrCategoryForm" property="deletemessage">
		<logic:equal name="AddClrCategoryForm" property="deletemessage" value="-9001">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="AM" key="am.cat.deletefailed"/></td>
	  		</tr> 
		</logic:equal>
		
		<logic:equal name="AddClrCategoryForm" property="deletemessage" value="0">
			<tr>
				<td   colspan="2" class="message" height="30" ><bean:message bundle="AM" key="am.cat.deletedsuccessfully"/></td>
	  		</tr> 
		</logic:equal>
		</logic:present>
  
		  <tr>  
		    <td colspan="2" class="labeleboldwhite" height="30" ><bean:message bundle="AM" key="am.cat.addonsitearrivaloption"/></td>
		  </tr> 
		 
		  		  
		 
		  <tr> 
		    <td  class="labelobold" height="25"><bean:message bundle="AM" key="am.cat.existingonsiteoption"/></td>
		    <td  align=left class="labelo">
			<html:select name="AddClrCategoryForm" property="clrexistingcategory" size="1" styleClass="comboe" onchange="return onComboChange();">
				<html:optionsCollection name = "dccat"  property = "shortcatlist"  label = "label"  value = "value" /></html:select></td>	
		  </tr>
		  
		  <tr> 
		    <td  class="labelebold" height="25"><bean:message bundle="AM" key="am.cat.newonsiteoption"/></td>
		    <td  class="labele">
			<html:text  styleClass="textbox" size="20" name="AddClrCategoryForm" property="clrcategoryshortname" maxlength="50" onchange="return oncategoryChange();"/></td>
		  </tr>
		  
		   <tr> 
		    <td  class="labelobold" height="25"><bean:message bundle="AM" key="am.cat.onsitearrivaldesc"/></td>
		    <td  class="labelo">
			<html:text  styleClass="textbox" size="33" name="AddClrCategoryForm" property="clrcategory" maxlength="50"/></td>
		  </tr>
		  
		  <tr> 
		    <td class="tryb" ><bean:message bundle = "AM" key = "am.cat.criticality"/></td>
		    <td class="tryb" ><bean:message bundle = "AM" key = "am.cat.servicetime"/></td>
		  </tr>
			   
  		  <%
		  	int i=1;
		  	String bgcolorbold="";
			String bgcolor="";
			
			
			%>
			
		  <logic:present name="critservicelist" >
		  	<logic:iterate id="critlist" name="critservicelist">
		  	<html:hidden name="critlist" property="criticalityid" />
		  	<%if(i%2==0)
				{
					bgcolorbold="labelobold";
					bgcolor="labelo";
				
				} 
				else
				{
					bgcolorbold="labelebold";
					bgcolor="labele";					
				}
			%>
		  		<tr> 
		    		<td  class="<%= bgcolorbold%>" height="25"><bean:write name = "critlist" property="clrcriticality" /></td>
		    		<td  class="<%= bgcolor%>">
					<html:text  styleClass="textbox" size="20" name="critlist" property="clrservicetime" maxlength="50"/></td>
		 		 </tr>
		  	
		  	<%i++; %>
		  	</logic:iterate>
		  
		  </logic:present>
		  
		  <tr> 
		    <td colspan="2" class="buttonrow"> 
		     <html:submit property="save" styleClass="button" onclick = "return validate();"><bean:message bundle="AM" key="am.cat.save"/></html:submit>
		     <html:reset property="reset" styleClass="button"><bean:message bundle="AM" key="am.cat.reset" /></html:reset>
		  </tr>
		
		  <jsp:include page = '/Footer.jsp'>
		  	 <jsp:param name = 'colspan' value = '28'/>
		     <jsp:param name = 'helpid' value = 'roleprivilege'/>
		  </jsp:include>
  
  	  </table>
    </td>
  </tr>
</table>


</html:form>
</table>
</BODY>

<script>


function validate()
{
	
	trimFields();
		
	
}
</script>
<script>
function trimFields()
{

	var field=document.forms[0];

	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text')
		{
			var temp1=field[i].value;

			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--){
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		field[i].value=temp2;
		}
	}
return true;
}

</script>
<!-- FUNCTION TRIM END -->

<script>

function CheckQuotes (obj,label)
{
	if(invalidChar(obj.value,label))
	{
		
		obj.focus();
		return false;
	}
	return true;
}


function oncategoryChange ()
{
	document.forms[0].clrexistingcategory.value="0";
	
	return true;
}

function onComboChange ()
{
	
	
	document.forms[0].clrcategoryshortname.value="";
	document.forms[0].clrcategory.value="";
	document.forms[0].submit();
	return true;
}

</script>

</html:html>
