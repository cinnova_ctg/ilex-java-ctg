<!DOCTYPE HTML>

<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying details of labor resource of an activity.
*
-->
<% int sow_size =( int ) Integer.parseInt( request.getAttribute( "sowsize" ).toString()); 
	int assumption_size =( int ) Integer.parseInt( request.getAttribute( "assumptionsize" ).toString()); 

%> 

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%
	String Labor_Id = "";
	String Activitylibrary_Id = "";
	if( request.getAttribute( "Labor_Id" ) != null )
	{
		Labor_Id = ( String )request.getAttribute( "Labor_Id" );
	}

	if( request.getAttribute( "Activitylibrary_Id" ) != null )
	{	
		Activitylibrary_Id = ( String )request.getAttribute( "Activitylibrary_Id" );
	}
%>

<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.labor.detail.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>


<script>

function del() 
{
		var convel = confirm( "<bean:message bundle = "AM" key = "am.resource.delete.confirm"/>" );
		if( convel )
		{
			document.forms[0].action = "LaborResourceAction.do?ref=Delete&Labor_Id=<%= Labor_Id %>&Activitylibrary_Id=<%= Activitylibrary_Id %>";
			document.forms[0].submit();
			return true;	
		}
}

</script>


<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')">
<div id="menunav">
    <ul>
  		<li>
        	<a href="#" class="drop"><span>Resource</span></a>
  			<ul>
  				<li>
					<%
					if((String)request.getAttribute( "list" ) != "" ) {
						%>
						<a href="#"><span><bean:message bundle = "AM" key = "am.labor.detail.menu.changestatus"/></span></a>
						<ul>
							<%= (String)request.getAttribute( "list" ) %>
						</ul>
						<%
					} else {
						%>
	  					<a href="#"><bean:message bundle = "AM" key = "am.labor.detail.menu.changestatus"/></a>
						<%
					}
					%>
				</li>
 				<li><a href="javascript:del();">Delete</a></li>
				<li><a href="ManageSOWAction.do?function=View&Type=L&Id=<%= Labor_Id %>&Activity_Id=<bean:write name = "LaborResourceDetailForm" property = "activity_Id"/>"><bean:message bundle = "AM" key = "am.labor.detail.menu.managesow"/></a></li>
				<li><a href="ManageAssumptionAction.do?function=View&Type=L&Id=<%= Labor_Id %>&Activity_Id=<bean:write name = "LaborResourceDetailForm" property = "activity_Id"/>"><bean:message bundle = "AM" key = "am.labor.detail.menu.manageassumption"/></a></li>
  			</ul>
  		</li>
	</ul>
</div>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	<tr>
    	<td class = "toprow"> 
	    	<table border = "0" cellspacing = "0" cellpadding = "0" height = "18">
		        <tr>
		        	<td background="images/content_head_04.jpg" height="21" colspan=3>
			         <div id="breadCrumb">
						 <a href="#" class="bgNone">Activity Manager</a>
			         	 <a href="ActivitySummaryAction.do?ref=View&MSA_Id=<bean:write name = "LaborResourceDetailForm" property = "msa_id"/>" background="none;"><bean:write name = "LaborResourceDetailForm" property = "msaname"/></a>
			         	 <a href="ManageActivityAction.do?ref=View&type=AM&MSA_Id=<bean:write name = "LaborResourceDetailForm" property = "msa_id"/>" background="none;">Manage Activities</a>
			         	 <a href = "ActivityLibraryDetailAction.do?Activitylibrary_Id=<bean:write name = "LaborResourceDetailForm" property = "activity_Id"/> " background="none;"><bean:write name = "LaborResourceDetailForm" property = "activityname"/></a>
			         	 <a href = "AMResourceLibraryListAction.do?ref=view&Activitylibrary_Id=<bean:write name = "LaborResourceDetailForm" property = "activity_Id"/>&MSA_Id=<bean:write name = "LaborResourceDetailForm" property = "msa_id"/>" background="none;"><bean:write name = "LaborResourceDetailForm" property = "laborname"/></a>
						 <a><span class="breadCrumb1">Resource Detail</span></a>
					</div>
			    	</td>
				</tr>
	      	</table>
    	</td>
	</tr>
</table>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
  <tr> 
    <td><img src = "images/spacer.gif" width = "1" height = "1"></td>
  </tr>
</table>

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
  <tr> 
    <td ><img src = "images/spacer.gif" width = "1" height = "1"></td>
  </tr>
</table>

<table>
<html:form action = "/LaborResourceDetailAction">

	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr><td width="2" height="0"></td>
  			 <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "1500"> 
					<tr>    
						 <td colspan="5" height="10">&nbsp;</td>
					</tr>
					
					<tr>
  						<td colspan = "5"><img src = "images/spacer.gif" width = "1" height = "1"></td>
  					</tr>
  					
  					<tr> 
    					<td colspan = "2" class = "labellobold" height = "30" width = 17%><bean:message bundle = "AM" key = "am.labor.detail.general"/></td>
						<td colspan = "2" class = "labellobold" height = "30" width = 17%><bean:message bundle = "AM" key = "am.labor.detail.costprice"/></td>
						<td width=66%></td>
					</tr>  
					
					
					<tr> 
    					<td class = "labelobold" height = "20" width = 9%><bean:message bundle = "AM" key = "am.labor.detail.type"/></td>
    					<td class = "labelo" width = 8%><bean:write name = "LaborResourceDetailForm" property = "labortype"/></td>
						<td class = "labelobold" width = 10%><bean:message bundle = "AM" key = "am.labor.detail.estimatedhourlybasecost"/></td>
    					<td class = "readonlytextnumberodd" width = 5%><bean:write name = "LaborResourceDetailForm" property = "estimatedhourlybasecost"/></td>
  						<td width = 66%></td>
  					</tr>
  					
  					<tr> 
    					<td class = "labelebold" height = "20"><bean:message bundle = "AM" key = "am.labor.detail.quantityhours"/></td>
    					<td class = "labele"><bean:write name = "LaborResourceDetailForm" property = "quantityhours"/></td>
						<td class = "labelebold"><bean:message bundle = "AM" key = "am.labor.detail.estimatedtotalcost"/></td>
    					<td class = "readonlytextnumbereven"><bean:write name = "LaborResourceDetailForm" property = "estimatedtotalcost"/></td>
	  					<td width=200></td>
  					</tr>
  					
  					<tr> 
    					<td class = "labelobold" height = "20"><bean:message bundle = "AM" key = "am.labor.detail.status"/></td>
    					<td class = "labelo"><bean:write name = "LaborResourceDetailForm" property = "status"/></td>
						<td class = "labelobold"><bean:message bundle = "AM" key = "am.labor.detail.unitprice"/></td>
    					<td class = "readonlytextnumberodd"><bean:write name = "LaborResourceDetailForm" property = "priceunit"/></td>
  						<td width=200></td>
  					</tr>
  					
  					<tr> 
						<td class = "labelebold" height = "20"><bean:message bundle = "AM" key = "am.labor.detail.proformamargin"/></td>
						<td class = "labele"><bean:write name = "LaborResourceDetailForm" property = "proformamargin"/></td>
						<td class = "labelebold"><bean:message bundle = "AM" key = "am.labor.detail.extendedprice"/></td>
    					<td class = "readonlytextnumbereven"><bean:write name = "LaborResourceDetailForm" property = "priceextended"/></td>
  						<td width=200></td>
  					</tr>
  					
  					<tr> 
    					<td class = "labelobold" height = "20"><bean:message bundle = "AM" key = "am.labor.detail.minimumquantity"/></td>
						<td class = "labelo"><bean:write name = "LaborResourceDetailForm" property = "minimumquantity"/></td>
						<td class = "labelobold" colspan = "2">&nbsp;</td>
  						<td width=200></td>
  					</tr>
  					
  					<tr> 
    					<td class = "labelebold" height = "20"><bean:message bundle = "AM" key = "am.labor.detail.minimumsellablequantity"/></td>
						<td class = "labele"><bean:write name = "LaborResourceDetailForm" property = "sellablequantity"/></td>
						<td class = "labelebold" colspan = "2">&nbsp;</td>
  						<td width=200></td>
  					</tr>
  					
  					
  					<html:hidden property = "laborid" />
  					<html:hidden property = "activity_Id" />
  					<tr> 
    					<td class = "labelobold" height = "20"><bean:message bundle = "AM" key = "am.labor.detail.cnspartnumber"/></td>
						<td class = "labelo"><bean:write name = "LaborResourceDetailForm" property = "cnspartnumber"/></td>
						<td class = "labelobold" colspan = "2">&nbsp;</td>
  						<td width=200></td>
  					</tr>
 				    <tr>
						<td class = "labellobold" colspan = "4" height = "30"><bean:message bundle = "AM" key = "am.detail.sow"/></td>
						<td width=200></td>
					</tr>
					<% int k = 1; %>
					
					<% if( ( sow_size > 0 ) ) {%>
					
					<tr>
						<td class = "labelotop" colspan = "4">	
							<logic:iterate id = "ms" name = "sowlist">	
								<bean:write name = "ms" property = "sow" /><br><br>
								<% k++; %>
							</logic:iterate>
						</td>
						<td width=200></td>
					</tr>
					<%} %>
					<tr>
						<td class = "labellobold" colspan = "4" height = "30"><bean:message bundle = "AM" key = "am.detail.assumption"/></td>
						<td width=200></td>
					</tr>
					<%if(( assumption_size > 0 ) ) { %>
					<tr>
						<% k = 1; %>
						<td class = "labelotop" colspan = "4">	
							<logic:iterate id = "ms" name = "assumptionlist">	
								<bean:write name = "ms" property = "assumption" /> <br><br>
								<% k++; %>
							</logic:iterate>
						</td>
						<td width=200></td>
					</tr>	
					<%} %>

					<jsp:include page = '/Footer.jsp'>
					      <jsp:param name = 'colspan' value = '37'/>
					      <jsp:param name = 'helpid' value = 'amlabordetail'/>
		   			</jsp:include> 
				</table>
			</td>
		</tr>
	</table>
					
</html:form>
</table>
</body>
</html:html>
  					







