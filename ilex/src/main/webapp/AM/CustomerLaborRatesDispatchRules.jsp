<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<title>Insert title here</title>
<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
</head>
<body>
	<table cellspacing="0" cellpadding="0" border="0" width="650">
			
			<tr>
				<td>
					<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td>
								<h1 style="font-size: 14px; white-space: nowrap;">Dispatch Rules</h1>
							</td>
							<logic:notPresent name="viewPage"  scope="request">
								<td align="right"><input type="button" class="button_c1" name= "editableView" value ="Update" onclick="openEditableView();"></td>
							</logic:notPresent>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height=5></td>
			</tr>
			<tr>
				<td>
					<table  cellspacing="0" cellpadding="0" class="Ntextoleftalignnowrappaleyellowborder2">
						<!-- for link show discounted rates end-->
						<tr>
							<td style="padding-left: 2px; padding-top: 2px; padding-right: 2px; padding-bottom: 2px;">
								<table cellspacing="1" cellpadding="0" border="0">
									<tr>
										<td class="texthdNoBorder">&nbsp;</td>
										<td colspan=2 align="center" class="texthdNoBorder"><b>Labor Resource</b></td>
										<td align="center" class="texthdNoBorder"></td>
										<td colspan=2 align="center" class="texthdNoBorder"><b>Travel (per Dispatch)</b></td>
									</tr>
									<tr>
										<td align="left" class="texthdNoBorder"><b>Dispatch Type</b></td>
										<td  align="right" class="texthdNoBorder"><b>Onsite Minimum</b></td>
										<td  align="right" class="texthdNoBorder"><b>Additional Increment</b></td>
										<td align="center" class="texthdNoBorder"><b>2+n Discount</b></td>
										<td  align="right" class="texthdNoBorder"><b>Minimum</b></td>
										<td  align="right" class="texthdNoBorder"><b>Maximum</b></td>
									</tr>
									<%String classStyle = ""; %>
									<%for(int i =0; i <5; i++) { %>
									<%
										if(i%2==0) {
											classStyle="colDark";
										} else {
											classStyle="colLight";
										}
									%>
									<tr>
										<td align="left" style="padding-left: 5px;" nowrap="nowrap" class="<%=classStyle%>"><bean:write name = "CustomerLaborRatesForm" property="<%="dispatchRules"+i+"[0]"%>" /></td>
										<% for(int j=1; j<=5; j++) {%>
											<td  align="right" class="<%=classStyle%>">
												<logic:present name="viewPage"  scope="request">
													<html:text property="<%="dispatchRules"+i+"["+j+"]"%>" styleClass="text"  style="text-align:right" size = "8" readonly="false"></html:text>
												</logic:present>
												<logic:notPresent name="viewPage"  scope="request">
													<bean:write name = "CustomerLaborRatesForm" property="<%="dispatchRules"+i+"["+j+"]"%>"/>
												</logic:notPresent>
											</td>
										<% }%>
									</tr>
									<%} %>
									<tr>
										<td colspan=6 height=15 class="Nhyperevenfont"></td>
									</tr>
									<tr>
										<td colspan=2 class="texthdNoBorder" style="text-align: left;"><b>Rate Discount or Markup</b></td>
										<td colspan=4 class="Nhyperevenfont"></td>
									</tr>
									<% for(int i =0; i<5; i++) { %>
										<%
											if(i%2==0) {
												classStyle="colDark";
											} else {
												classStyle="colLight";
											}
										%>
										<tr>
											<td align="left" style="padding-left: 5px;" nowrap="nowrap" class="<%=classStyle%>"><bean:write name = "CustomerLaborRatesForm" property="<%="dispatchRateLabels["+i+"]"%>" /></td>
											<td align="right" class="<%=classStyle%>">
												<logic:present name="viewPage"  scope="request">
													<bean:define id="rateDiscount" name="CustomerLaborRatesForm" property="<%="dispatchRateDiscount["+i+"]"%>" type="java.lang.String"></bean:define>
													<html:text property="dispatchRateDiscount" styleId="<%="dispatchRateDiscount"+i%>" styleClass="text" style="text-align:right" size = "8"  value="<%=rateDiscount %>" readonly="false" onkeypress = "return onlyNumbers(event,this);" onchange="roundDEcimalTwoDigit(this);"></html:text>
												</logic:present>
												<logic:notPresent name="viewPage"  scope="request">
											<%--	<bean:write name = "CustomerLaborRatesForm" property="<%="dispatchRateDiscount["+i+"]"%>"/>   --%>
											<bean:write name = "CustomerLaborRatesForm" property="<%="dispatchRateDiscount["+i+"]"%>"/>
												</logic:notPresent>
											</td>
											<td colspan=4 class="Nhyperevenfont"></td>
										</tr>
									<%} %>	
									<tr>
										<td colspan=6 height=15 class="Nhyperevenfont"></td>
									</tr>
									<tr>
										<td colspan=2 class="texthdNoBorder" style="text-align: left;"><b>Other Rules</b></td>
										<td colspan=4 class="Nhyperevenfont"></td>
									</tr>	
									<tr>
										<td align="left" style="padding-left: 5px;" class="Nhyperevenfont">Travel</td>
										<td class="Nhyperevenfont">Portal to Portal</td>
										<td colspan=4 class="Nhyperevenfont"></td>
									</tr>
									<tr>
										<td align="left" style="padding-left: 5px;" class="Nhyperoddfont">Cancellation</td>
										<td colspan=5 class="Nhyperevenfont" style="white-space: normal;">Within 24 hours, cancellation fee applies plus charges for time spent by field personnel when cancellation occurs after field personnel have departed their origination point for the cancelled destination.</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
</body>
</html>
