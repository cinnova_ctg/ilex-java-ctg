<!DOCTYPE HTML>
<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For adding and updating labor resource of an activity
*
-->



<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<bean:define id = "checkfortext" name = "refresh" scope = "request"/>
<%
	String temp_str = null;
	String Id = ( String ) request.getAttribute( "Activity_Id" );
	int labor_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString());
%>


<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.labor.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>

<%@ include file="/Menu.inc" %>

<%
	String backgroundclass = "texto";
	String backgroundhyperlinkclass = null;
	String displaycombo = null;
	String readonlyclass = null;
	String readonlynumberclass = null;
	String backnew = "";
	int i = 0;
	String comboclass = null;
	boolean csschooser = true;
	String schdayscheck = null;
	String valstr = null;
	String arrcal = null;
%>




<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages( 'images/sales1b.gif' , 'images/services1b.gif' , 'images/about1b.gif' , 'images/cust-serv1b.gif' , 'images/offers1b.gif')">

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "0">
  <tr>
    <td class = "toprow"> 
      <table width = "" border = "0" cellspacing = "1" cellpadding = "0" height = "18">
        <tr align = "center"> 
          <td width = "140" class = "toprow1"><a href = "MaterialResourceAction.do?ref=View&Activitylibrary_Id=<%= Id %>&MSA_Id=<bean:write name = "LaborResourceForm" property = "MSA_Id" />" class = "drop"><bean:message bundle = "AM" key = "am.material"/></a></td>
          <td width = "150" class = "toprow1"><a href = "FreightResourceAction.do?ref=View&Activitylibrary_Id=<%= Id %>&MSA_Id=<bean:write name = "LaborResourceForm" property = "MSA_Id" />" class = "drop" ><bean:message bundle = "AM" key = "am.freight"/></a></td>
          <td width = "120" class = "toprow1"><a href = "TravelResourceAction.do?ref=View&Activitylibrary_Id=<%= Id %>&MSA_Id=<bean:write name = "LaborResourceForm" property = "MSA_Id" />" class = "drop"><bean:message bundle = "AM" key = "am.travel"/></a></td>
          <td width = "390" class = "toprow1"><a href = "ActivityLibraryDetailAction.do?ref=View&Activitylibrary_Id=<%= Id %>&type=AM&MSA_Id=<bean:write name = "LaborResourceForm" property = "MSA_Id" />" class = "drop"><bean:message bundle = "AM" key = "am.material.back"/></a></td>
          <!--  <td width = "390" class="toprow1">&nbsp;</td>-->
        </tr>
      </table>
    </td>
  </tr>
</table>


<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
 <tr>
 	
  	<td>
  		<table border = "0" cellspacing = "1" cellpadding = "1">
  		<!-- Start :Added By Amit -->
  		
  		<logic:present name = "changestatusflag" scope = "request">
  		<tr><td>&nbsp;</td>
	    	<td colspan = "5" class = "message" height = "30">
		  		<logic:equal name = "changestatusflag" value = "0">
				<bean:message bundle = "AM" key = "am.labor.resourcesapprovedsuccessfully"/>
				</logic:equal>
			</td>
			</tr>
  		</logic:present>
  		<!--  End Aded By Amit -->
		  		<logic:present name = "addflag" scope = "request">
		  		<tr><td>&nbsp;</td>
	    		<td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "addflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.labor.add.success"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "addflag" value = "-9001">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure1"/>
			    	</logic:equal>
			    	
					<logic:equal name = "addflag" value = "-9002">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure2"/>
			    	</logic:equal>
				
					<logic:equal name = "addflag" value = "-9003">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure3"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "addflag" value = "-9004">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure4"/>
			    	</logic:equal>
			    	
					<logic:equal name = "addflag" value = "-9005">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure5"/>
			    	</logic:equal>
				
					<logic:equal name = "addflag" value = "-9006">
			    		<bean:message bundle = "AM" key = "am.labor.add.failure6"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
				</logic:present>
		
				<logic:present name = "updateflag" scope = "request">
					<tr><td>&nbsp;</td>
	    			<td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "updateflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.labor.update.success"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "updateflag" value = "-9007">
			    		<bean:message bundle = "AM" key = "am.labor.update.failure1"/>
			    	</logic:equal>
			    	
					<logic:equal name = "updateflag" value = "-9008">
			    		<bean:message bundle = "AM" key = "am.labor.update.failure2"/>
			    	</logic:equal>
				
					<logic:equal name = "updateflag" value = "-9009">
			    		<bean:message bundle = "AM" key = "am.labor.update.failure3"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "updateflag" value = "-9010">
			    		<bean:message bundle = "AM" key = "am.labor.update.failure4"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
				</logic:present>
				
				<logic:present name = "deleteflag" scope = "request">
			    	<tr><td>&nbsp;</td>
	    			<td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "deleteflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.labor.delete.success"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9001">
			    		<bean:message bundle = "AM" key = "am.labor.delete.failure1"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9002">
			    		<bean:message bundle = "AM" key = "am.labor.delete.failure2"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9004">
			    		<bean:message bundle = "AM" key = "am.labor.delete.failure3"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
			    </logic:present>
			
			<tr> 
				<td>&nbsp; </td>
	    		<td class = "labeleboldhierrarchy" colspan = "13" height = "30">
	    			<bean:message bundle = "AM" key = "am.labor.header"/> 
	    			<bean:message bundle = "AM" key = "am.labor.for"/> 
	    			<a href = "ManageActivityAction.do?MSA_Id=<bean:write name = "LaborResourceForm" property = "msa_id"/>">
						<bean:write name = "LaborResourceForm" property = "msaname"/>
					</a>
	    			<bean:message bundle = "AM" key = "am.detail.arrow"/>&nbsp;
					<a href = "ActivityLibraryDetailAction.do?Activitylibrary_Id=<bean:write name = "LaborResourceForm" property = "activity_Id"/>">
						<bean:write name ="activityname" scope = "request"/> 
					</a>
	    		</td>
			</tr>
  
	  		<tr> 
	    		<td rowspan = "2">&nbsp; </td>
	    		<td class = "tryb" rowspan = "2">
	    			<bean:message bundle = "AM" key = "am.labor.name"/>
	    		</td>
	   
				<td class = "tryb" rowspan = "2">
					<bean:message bundle = "AM" key = "am.labor.type"/>
				</td>
				
				<td class = "tryb" rowspan = "2">
					<bean:message bundle = "AM" key = "am.labor.cnspartnumber"/>
				</td>
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.labor.hours"/>
			    </td>
			    
			    <td class = "tryb" colspan = "2">
			    	<bean:message bundle = "AM" key = "am.labor.estimatedcost"/>
			    </td>
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.labor.proformamargin"/>
			    </td>
			    <td class = "tryb" colspan = "2">
			    	<bean:message bundle = "AM" key = "am.labor.price"/>
			    </td>
			    
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.labor.status"/>
			    </td>
			    
			</tr>
  
	  		<tr> 
	    		<td class = "tryb"> 
	      			<div align = "center">
	      				<bean:message bundle = "AM" key = "am.labor.hourlybase"/>
	      			</div>
	    		</td>
	    
	    		<td class = "tryb"> 
	      			<div align = "center">
	      				<bean:message bundle = "AM" key = "am.labor.estimatedtotalcost"/>
	      			</div>
	    		</td>
	    
	    
			    <td class = "tryb"> 
			      <div align = "center">
			      	<bean:message bundle = "AM" key = "am.labor.priceunit"/>
			      </div>
			    </td>
	    
			    <td class = "tryb"> 
			      <div align = "center">
			      	<bean:message bundle = "AM" key = "am.labor.priceextended"/>
			      </div>
			    </td>
			</tr>
			
<html:form action = "/LaborResourceAction">
<html:hidden name = "LaborResourceForm" property = "MSA_Id" />
	<logic:present name = "laborresourcelist" scope = "request"> 
 		<logic:iterate id = "ms" name = "laborresourcelist">
			<bean:define id = "resourceId" name = "ms" property = "laborid" type = "java.lang.String"/>
			<bean:define id = "estUnitCost" name = "ms" property = "estimatedhourlybasecost" type = "java.lang.String"/>
		<%	
			temp_str = "marginComputation('"+resourceId+"', '"+estUnitCost+"', "+i+", "+labor_size+");"; 	
			if ( csschooser == true ) 
			{
				backgroundclass = "texto";
				backgroundhyperlinkclass = "hyperodd";
				comboclass = "combooddhidden";
				readonlyclass = "readonlytextodd";
				readonlynumberclass = "readonlytextnumberodd";
				csschooser = false;
			}
	
			else
			{
				csschooser = true;	
				comboclass = "comboevenhidden";
				backgroundhyperlinkclass = "hypereven";
				readonlyclass = "readonlytexteven";
				readonlynumberclass = "readonlytextnumbereven";
				backgroundclass = "texte";
			}
		%>
			<bean:define id = "val" name = "ms" property = "laborid" type = "java.lang.String"/>
			<bean:define id = "flag" name = "ms" property = "flag" type = "java.lang.String"/>
			<html:hidden name="ms" property="flag_laborid"/>
			<tr>		
				
				<td class = "labeleboldwhite"> 
					<logic:equal name = "ms" property = "status" value = "Approved">
						<%--<html:checkbox property = "check" value = "<%= flag+val %>" disabled = "true"/>	--%>
						<html:multibox property = "check"  disabled = "true"><bean:write  name="ms" property = "flag_laborid"/></html:multibox>	
					</logic:equal>
				
					<logic:notEqual name = "ms" property = "status" value = "Approved">
						<%--<html:checkbox property = "check" value = "<%= flag+val %>" /> --%>
						<html:multibox property = "check" ><bean:write  name="ms" property = "flag_laborid"/></html:multibox>	
					</logic:notEqual>	
	    		</td>
				
				<html:hidden name = "ms" property = "laborid" />
				<html:hidden name = "ms" property = "laborcostlibid" />
				
				<%  
					if( labor_size == 1 )
					{
						valstr = "javascript: document.forms[0].check.checked = true;updatetext(this,'');"; 
						displaycombo = "javascript: displaycombo(this,'')";
						schdayscheck = "javascript: document.forms[0].check.checked = true;";
						arrcal = "javascript: calarr( null );";	
					}
					else if( labor_size > 1 )
					{
						valstr = "javascript: document.forms[0].check['"+i+"'].checked = true;updatetext( this ,'"+i+"' );"; 
						schdayscheck = "javascript: document.forms[0].check['"+i+"'].checked = true;";
						displaycombo = "javascript: displaycombo( this , '"+i+"' )";
						arrcal = "javascript: calarr('"+i+"');";
					}
				%>
				
				<!-- for multiple resource select start -->
				<html:hidden name="ms" property="flag"/>
				<logic:equal name="ms" property="flag" value="T">
				<td class = "<%= backgroundhyperlinkclass %>"> 
					<bean:write name = "ms" property = "laborname" />
				</td>
				</logic:equal>
				<logic:equal name="ms" property="flag" value="P">
				<td class = "<%= backgroundhyperlinkclass %>"> 
					<a href = "LaborResourceDetailAction.do?Labor_Id=<bean:write name = "ms" property = "laborid"/>&Activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "laborname" /></a>
				</td>
				</logic:equal>
				<!-- for multiple resource select end -->
				<!--  <td class = "<%= backgroundhyperlinkclass %>"> 
					<a href = "LaborResourceDetailAction.do?Labor_Id=<bean:write name = "ms" property = "laborid"/>&Activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "laborname" /></a>
				</td>-->
				
				<html:hidden name = "ms" property = "laborname" />
				
				<td class = "<%= backgroundhyperlinkclass %>">
					<bean:write name = "ms" property = "labortype" />
					<html:hidden styleId = "labortype" name = "ms" property = "labortype"  />
				</td>
				
				<td class = "<%= backgroundhyperlinkclass %>">
					<bean:write name = "ms" property = "cnspartnumber" />
					<html:hidden styleId = "cnspartnumber" name = "ms"  property = "cnspartnumber" />
				</td>
			   
				
				<logic:equal name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text  name = "ms" property = "quantityhours" size = "6" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
					</td>
				</logic:equal>		
				
				<logic:notEqual name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "<%= "quantityhours"+i%>" name = "ms"  size = "6" maxlength = "6" property = "quantityhours" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "<%= schdayscheck %>" onblur = "<%= arrcal %>"/>
					</td>
				</logic:notEqual>
				
			
				<html:hidden name = "ms" property = "prevquantityhours" />
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "estimatedhourlybasecost" name = "ms" size = "6"  property = "estimatedhourlybasecost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td>
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "estimatedtotalcost" name = "ms" size = "10"  property = "estimatedtotalcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td> 
			   
				
				<logic:equal name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "<%= "proformamargin"+i%>" name = "ms" size = "9" maxlength = "10" property = "proformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true"/>
					</td>
					
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "priceunit" name = "ms" size = "8"  property = "priceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />&nbsp;&nbsp;<html:button styleClass = "buttonverticalalign" property="setmargin" disabled="true"><bean:message bundle = "AM" key = "am.common.set"/></html:button>
				    </td>
				</logic:equal>
				
				<logic:notEqual name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "<%= "proformamargin"+i%>" name = "ms" size = "9" maxlength = "10" property = "proformamargin" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "<%= schdayscheck %>" onblur = "<%= arrcal %>"/>
				    </td> 
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "priceunit" name = "ms" size = "8"  property = "priceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />&nbsp;&nbsp;<html:button styleClass = "buttonverticalalign" property="setmargin" onclick = "<%= temp_str%>"><bean:message bundle = "AM" key = "am.common.set"/></html:button>
				    </td>
				</logic:notEqual>
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "priceextended" name = "ms" size = "8"  property = "priceextended" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
			    </td>
			   
			    <td class = "<%= backgroundclass %>">
					<bean:write name = "ms" property = "status"/>		
				</td>
				<html:hidden name = "ms" property = "status"/>
				
				<html:hidden name = "ms" property = "sellablequantity" />
	   			<html:hidden name = "ms" property = "minimumquantity" />        	
	  		</tr>
  		
	   <% i++;  %>
		</logic:iterate>
	</logic:present>
	
				<%	
				  	if( labor_size != 0 )
				  	{
				  		if( labor_size % 2 == 1 )
				  		{
							backnew = "textenewelement";
				  			backgroundclass = "texte";
				  			i = 2;
							displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
							comboclass = "comboevenhidden";
							readonlyclass = "readonlytexteven";
							readonlynumberclass = "readonlytextnumbereven";
						}
				  		else
				  		{
							backnew = "textonewelement";
				  			backgroundclass = "texto";
				  			i = 1;
							displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
							comboclass = "combooddhidden";
							readonlyclass = "readonlytextodd";
							readonlynumberclass = "readonlytextnumberodd";
				  		}
				  	}
				  	else
					{
						backnew = "textonewelement";
						backgroundclass = "texto";
						i = 1;
						displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
						comboclass = "combooddhidden";
						readonlyclass = "readonlytextodd";
						readonlynumberclass = "readonlytextnumberodd";
					}
	   			%>
	   		
	   			<html:hidden property = "activity_Id" />
	   			<html:hidden property = "ref" /> 
	   			<html:hidden property = "newsellablequantity" />
	   			<html:hidden property = "newminimumquantity" />
	   		<tr> 
		    	<td class = "labeleboldwhite"> 
		    		<logic:equal name = "act_status"  value = "Approved" scope = "request">
		    			<html:checkbox property = "newlaborid" disabled = "true"/>
		    		</logic:equal>
		    		<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
		    			<html:checkbox property = "newlaborid" />
		    		</logic:notEqual>
		   		</td>
		   		
		   		<td class = "<%= backnew %>">
		   	 		<logic:equal name = "act_status"  value = "Approved" scope = "request">
		   	 			<html:text property = "newlaborname" size = "20" styleClass = "<%= readonlyclass %>" readonly = "true" />
			   			<html:hidden property = "newlabornamecombo" />
		   	 		</logic:equal>
		   	 		
		   	 		<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
			   	 		<html:text styleId = "newlaborname" property = "newlaborname" size = "20" styleClass = "textbox" onclick ="javascript:opensearchwindow();" readonly="true" onmouseover = "highlightTextField( this );" onmouseout = "unHighlightTextField( this );" />
			   			<html:select styleId = "newlabornamecombo" property = "newlabornamecombo" size = "1" styleClass = "<%= comboclass %>" onblur = "hidecombo( this );"    onchange = "document.forms[0].newlaborid.checked = true; updatetext( this , null ); return refresh();">
					    	<html:optionsCollection  property = "laborlist" value = "value" label = "label"/> 
				   		</html:select>
		   	 		</logic:notEqual>
		   	 		
		   	 		
		   		</td>
		   		
		   		<td class = "<%= backgroundclass %>">
		   	 		<bean:write name = "LaborResourceForm" property = "newlabortype" />
		   	 		<html:hidden styleId = "newlabortype" property = "newlabortype" />
		   		</td>
		   		
			    <td class = "<%= backgroundclass %>">
					<bean:write  name = "LaborResourceForm" property = "newcnspartnumber" />
					<html:hidden styleId = "newcnspartnumber"  property = "newcnspartnumber" />
				</td>
				
				<td class = "<%= backgroundclass %>">
					<logic:equal name = "act_status"  value = "Approved" scope = "request">
						<html:text size = "6" maxlength = "6" property = "newquantityhours" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
					</logic:equal>
					
					<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
						<% if( checkfortext.equals( "true" ) ) { %>	
							<html:text size = "6" maxlength = "6" property = "newquantityhours" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
						<%}else { %>
							<html:text styleId = "newquantityhours"  size = "6" maxlength = "6" property = "newquantityhours" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "document.forms[0].newlaborid.checked = true;" onblur = "calnew();" />
						<%} %>
					</logic:notEqual>
				</td>
				
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "newestimatedhourlybasecost"  size = "6"  property = "newestimatedhourlybasecost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td>
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "newestimatedtotalcost" size = "10" maxlength = "9"  property = "newestimatedtotalcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td> 
			   
				<td class = "<%= backgroundclass %>">
					<logic:equal name = "act_status"  value = "Approved" scope = "request">
						<html:text size = "6" maxlength = "5" property = "newproformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true"/>
					</logic:equal>
					
					<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
						<% if( checkfortext.equals( "true" ) ) { %>	
							<html:text size = "6" maxlength = "5" property = "newproformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true"/>
			   			<%}else { %>
			   				<html:text styleId = "newproformamargin" size = "6" maxlength = "5" property = "newproformamargin" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "document.forms[0].newlaborid.checked = true;" onblur = "calnew();"/>
			   			<%} %>
			   		</logic:notEqual>
			    </td> 
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "newpriceunit" size = "10" maxlength = "9"  property = "newpriceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
			    </td>
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "newpriceextended"  size = "10" maxlength = "9"  property = "newpriceextended" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
			    </td>
			   
			    <td class = "<%= backgroundclass %>">
					<bean:write name = "LaborResourceForm" property = "newstatus"/>		
				</td>        	
	  		</tr>
	  		
	  		
	  		<tr>	<td >&nbsp; </td>
			  	<td colspan = "9" class = "buttonrow">
			  		<html:submit property = "save" styleClass = "button" onclick = "return validate();"><bean:message bundle = "AM" key = "am.labor.submit"/></html:submit>
					<html:button property = "sort" styleClass = "button" onclick = "return sortwindow();"><bean:message bundle = "AM" key = "am.labor.sort"/></html:button>
					<!-- Start :Added By Amit for Mass delete -->
					<html:button property = "massDelete" styleClass = "button" onclick = "return del();"><bean:message bundle = "AM" key = "am.travel.massDelete"/></html:button>
					<html:button property = "approve" styleClass = "button" onclick = "return massApprove();"><bean:message bundle = "AM" key = "am.material.massApprove"/></html:button>
					<!-- End :Added By Amit for Mass delete -->
				</td>
				<td class = "buttonrow">
					<html:reset property = "cancel" styleClass = "button"><bean:message bundle = "AM" key = "am.labor.cancel"/></html:reset>
				</td>
			</tr>
	  		
	  		<jsp:include page = '/Footer.jsp'>
		      <jsp:param name = 'colspan' value = '37'/>
		      <jsp:param name = 'helpid' value = 'amlabortabular'/>
		   </jsp:include> 
			    
		</table>
	</td>
</tr>
</table>
  
</html:form>


<script>
function marginComputation(resourceId, estUnitCost, position, size)
{
	str = "MarginComputationAction.do?resourceId="+resourceId+"&estUnitCost="+estUnitCost+"&position="+position+"&size="+size;	
	suppstrwin = window.open(str, '', 'left = 300 , top = 250 , width = 320, height = 240 , resizable = yes , scrollbars = yes')
	suppstrwin.focus();
}
function opensearchwindow()
{
	str = "OpenSearchWindowTempAction.do?type=Labor&from=am&activity_Id=<bean:write name = "LaborResourceForm" property = "activity_Id" />";

	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600, height = 400 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();

}

function sortwindow()
{
	str = 'SortAction.do?Type=L&Activitylibrary_Id=<%= Id %>';
	window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );  */
}

function validate()
{
	document.forms[0].ref.value = "Submit";
	
	var submitflag = 'false';
	if( <%= labor_size %> != 0 )
	{
		if( <%= labor_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newlaborid.checked ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.labor.selectresource"/>" );
				return false;
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newlaborid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.labor.selectresource"/>" );
				return false;
		  	}
		}
	}
	
	else
	{
		if( !document.forms[0].newlaborid.checked )
		{
			alert( "<bean:message bundle = "AM" key = "am.labor.newlabor"/>" );
			return false;
		}
	}
	
	
	if( <%= labor_size %> != 0 )
	{
		if( <%= labor_size %> == 1 )
		{
			if( document.forms[0].check.checked ) 
	  		{
	  			if( document.forms[0].quantityhours.value == "" )
	  			{
	  				document.forms[0].quantityhours.value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].quantityhours.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
		 			document.forms[0].quantityhours.value = "";
		 			document.forms[0].quantityhours.focus();
					return false;
		 		}
		 		
		 		if( document.forms[0].proformamargin.value == "" )
	  			{
	  				alert( "<bean:message bundle = "AM" key = "am.material.proformamarginenter"/>" );
	  				document.forms[0].proformamargin.focus();
	  				return false;
	  			}
	 
		  		else
		  		{
		  			if( isFloat( document.forms[0].proformamargin.value ) )
		 			{
			 			if( document.forms[0].proformamargin.value > parseFloat( "1.0" ) )
			 			{
			 				alert( "<bean:message bundle = "AM" key = "am.material.proformamargingreaterthanone"/>" );
			 				document.forms[0].proformamargin.value = "";
			 				document.forms[0].proformamargin.focus();
							return false;
			 			}
		 			}
		 			else
		 			{
		 				alert( "<bean:message bundle = "AM" key = "am.material.numericvalue"/>" );
			 			document.forms[0].proformamargin.value = "";
			 			document.forms[0].proformamargin.focus();
						return false;
		 			}
		 		}	
	  		}
		}
		
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
			  		if( document.forms[0].quantityhours[i].value == "" )
		  			{
		  				document.forms[0].quantityhours[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].quantityhours[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
			 			document.forms[0].quantityhours[i].value = "";
			 			document.forms[0].quantityhours[i].focus();
						return false;
			 		}
			 		
			 		if( document.forms[0].proformamargin[i].value == "" )
		  			{
		  				alert( "<bean:message bundle = "AM" key = "am.material.proformamarginenter"/>" );
		  				document.forms[0].proformamargin[i].focus();
		  				return false;
		  			}
	 
			  		else
			  		{
			  			if( isFloat( document.forms[0].proformamargin[i].value ) )
			 			{
				 			if( document.forms[0].proformamargin[i].value > parseFloat( "1.0" ) )
				 			{
				 				alert( "<bean:message bundle = "AM" key = "am.material.proformamargingreaterthanone"/>" );
				 				document.forms[0].proformamargin[i].value = "";
				 				document.forms[0].proformamargin[i].focus();
								return false;
				 			}
			 			}
			 			else
			 			{
			 				alert( "<bean:message bundle = "AM" key = "am.material.numericvalue"/>" );
				 			document.forms[0].proformamargin[i].value = "";
				 			document.forms[0].proformamargin[i].focus();
							return false;
			 			}
			 		}	
		  		}
		  	}
		}
	}
	
	
	if( document.forms[0].newlaborid.checked ) 
	{
		if( document.forms[0].newlabornamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.labor.laborenter"/>" );
 			document.forms[0].newlaborname.focus();
 			return false;
 		}
		
		
		
		
		if( document.forms[0].newquantityhours.value == "" )
		{
		  	alert( "<bean:message bundle = "AM" key = "am.material.quantitymandatory"/>" );
		  	document.forms[0].newquantityhours.focus();
		  	return false;
		}
		 
  		if( !isFloat( document.forms[0].newquantityhours.value ) )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
 			document.forms[0].newquantityhours.value = "";
 			document.forms[0].newquantityhours.focus();
			return false;
 		}
			 		
	 	if( document.forms[0].newproformamargin.value == "" )
  		{
  			alert( "<bean:message bundle = "AM" key = "am.material.proformamarginenter"/>" );
  			document.forms[0].newproformamargin.focus();
  			return false;
  		}
 
  		else
  		{
  			if( isFloat( document.forms[0].newproformamargin.value ) )
 			{
	 			if( document.forms[0].newproformamargin.value > parseFloat( "1.0" ) )
	 			{
	 				alert( "<bean:message bundle = "AM" key = "am.material.proformamargingreaterthanone"/>" );
	 				document.forms[0].newproformamargin.value = "";
	 				document.forms[0].newproformamargin.focus();
					return false;
	 			}
 			}
 			else
 			{
 				alert( "<bean:message bundle = "AM" key = "am.material.numericvalue"/>" );
	 			document.forms[0].proformamargin.value = "";
	 			document.forms[0].proformamargin.focus();
				return false;
 			}
 		}	
	}
	return true;
}

function refresh()
{
	document.forms[0].ref.value = "Refresh";
	document.forms[0].submit();
	return true;
}



function calarr( i )
{
	if( <%= labor_size %> == 1 )
	{
		if( ( document.forms[0].quantityhours.value == "" ) || ( document.forms[0].quantityhours.value == "null" ) )
		{ 
			document.forms[0].quantityhours.value = "0.0";
		}
		
		if( isFloat( document.forms[0].quantityhours.value ) ) 
		{ 
			if( parseFloat( document.forms[0].quantityhours.value ) <  parseFloat( document.forms[0].minimumquantity.value ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.quantityadjustedagain"/>" );
				document.forms[0].quantityhours.value = document.forms[0].minimumquantity.value;
			}

			var divisor =  ( parseFloat( document.forms[0].quantityhours.value )  ) /  parseFloat( document.forms[0].sellablequantity.value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].quantityhours.value ) - (divisor * parseFloat( document.forms[0].sellablequantity.value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "AM" key = "am.quantityadjusted"/>" );
				document.forms[0].quantityhours.value = (parseFloat( document.forms[0].sellablequantity.value )*(divisor + 1)).toFixed(4) ;
			}
						
				
			document.forms[0].estimatedtotalcost.value = parseFloat( document.forms[0].quantityhours.value ) * parseFloat( document.forms[0].estimatedhourlybasecost.value );
			document.forms[0].estimatedtotalcost.value = round_func( document.forms[0].estimatedtotalcost.value );
		
			if( ( document.forms[0].proformamargin.value == "" ) || ( document.forms[0].proformamargin.value == "null" ) || ( document.forms[0].proformamargin.value >= "1" ) )
			{ 
				document.forms[0].proformamargin.value = "0.0";
			}
		
			if( isFloat( document.forms[0].proformamargin.value ) ) 
			{ 
				document.forms[0].priceunit.value = parseFloat( document.forms[0].estimatedhourlybasecost.value ) / ( 1 - parseFloat( document.forms[0].proformamargin.value ) );
				document.forms[0].priceunit.value = round_func( document.forms[0].priceunit.value );
			}
			
			if( isFloat( document.forms[0].quantityhours.value ) ) 
			{ 
				document.forms[0].priceextended.value = parseFloat( document.forms[0].priceunit.value ) * parseFloat( document.forms[0].quantityhours.value );
				document.forms[0].priceextended.value = round_func( document.forms[0].priceextended.value );
			}
		}
	}
	
	else
	{
		if( ( document.forms[0].quantityhours[i].value == "" ) || ( document.forms[0].quantityhours[i].value == "null" ) )
		{ 
			document.forms[0].quantityhours[i].value = "0.0";
		}
		
		if( isFloat( document.forms[0].quantityhours[i].value ) ) 
		{ 
			if( parseFloat( document.forms[0].quantityhours[i].value ) <  parseFloat( document.forms[0].minimumquantity[i].value ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.quantityadjustedagain"/>" );
				document.forms[0].quantityhours[i].value = document.forms[0].minimumquantity[i].value;
			}
				
			var divisor =  ( parseFloat( document.forms[0].quantityhours[i].value )  ) /  parseFloat( document.forms[0].sellablequantity[i].value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].quantityhours[i].value ) - (divisor * parseFloat( document.forms[0].sellablequantity[i].value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "AM" key = "am.quantityadjusted"/>" );
				document.forms[0].quantityhours[i].value = (parseFloat( document.forms[0].sellablequantity[i].value )*(divisor + 1)).toFixed(4) ;
			}
			
			document.forms[0].estimatedtotalcost[i].value = parseFloat( document.forms[0].quantityhours[i].value ) * parseFloat( document.forms[0].estimatedhourlybasecost[i].value );
			document.forms[0].estimatedtotalcost[i].value = round_func( document.forms[0].estimatedtotalcost[i].value );
			
			if( ( document.forms[0].proformamargin[i].value == "" ) || ( document.forms[0].proformamargin[i].value == "null" ) || ( document.forms[0].proformamargin[i].value >= "1" ) )
			{ 
				document.forms[0].proformamargin[i].value = "0.0";
			}
		
			if( isFloat( document.forms[0].proformamargin[i].value ) ) 
			{ 
				document.forms[0].priceunit[i].value = parseFloat( document.forms[0].estimatedhourlybasecost[i].value ) / ( 1 - parseFloat( document.forms[0].proformamargin[i].value ) );
				document.forms[0].priceunit[i].value = round_func( document.forms[0].priceunit[i].value );
			}
			
			if( isFloat( document.forms[0].quantityhours[i].value ) ) 
			{ 
				document.forms[0].priceextended[i].value = parseFloat( document.forms[0].priceunit[i].value ) * parseFloat( document.forms[0].quantityhours[i].value );
				document.forms[0].priceextended[i].value = round_func( document.forms[0].priceextended[i].value );
			}
		}
	}
	
}






function calnew()
{
	if( document.forms[0].newlabornamecombo.value == '0' )
	{
		alert( "<bean:message bundle = "AM" key = "am.labor.laborenter"/>" ); 
		return false;
	}
	
	if( ( document.forms[0].newquantityhours.value == "" ) || ( document.forms[0].newquantityhours.value == "null" ) )
	{ 
		document.forms[0].newquantityhours.value = "0.0";
	}
	
	if( isFloat( document.forms[0].newquantityhours.value ) ) 
	{ 
		if( parseFloat( document.forms[0].newquantityhours.value ) <  parseFloat( document.forms[0].newminimumquantity.value ) )
		{
			alert( "<bean:message bundle = "AM" key = "am.quantityadjustedagain"/>" );
			document.forms[0].newquantityhours.value = document.forms[0].newminimumquantity.value;
			
		}
		
		
		var divisor =  ( parseFloat( document.forms[0].newquantityhours.value )  ) /  parseFloat( document.forms[0].newsellablequantity.value );
		divisor = parseInt(divisor);
		var remainder = parseFloat( document.forms[0].newquantityhours.value ) - (divisor * parseFloat( document.forms[0].newsellablequantity.value ) );
		var zero=0;
		if(  remainder.toFixed(4) == zero.toFixed(4) )
		{
		}
		else
		{
			alert( "<bean:message bundle = "AM" key = "am.quantityadjusted"/>" );
			document.forms[0].newquantityhours.value = parseFloat( document.forms[0].newsellablequantity.value ) * (divisor + 1); 
		}
	
		
		/*var remainder = ( parseFloat( document.forms[0].newquantityhours.value ) - parseFloat( document.forms[0].newminimumquantity.value ) ) %  parseFloat( document.forms[0].newsellablequantity.value );
		
		if(  remainder == 0 )
		{
		}
		else
		{
			var temp = ( parseFloat( document.forms[0].newquantityhours.value ) - parseFloat( document.forms[0].newminimumquantity.value ) - parseFloat( remainder ) ) /  parseFloat( document.forms[0].newsellablequantity.value );
			alert( "<bean:message bundle = "AM" key = "am.quantityadjusted"/>" );
			document.forms[0].newquantityhours.value = parseFloat( document.forms[0].newminimumquantity.value ) + ( parseFloat( document.forms[0].newsellablequantity.value ) * ( parseFloat( temp ) + 1 ) ); 
		}*/
		
		document.forms[0].newestimatedtotalcost.value = parseFloat( document.forms[0].newquantityhours.value ) * parseFloat( document.forms[0].newestimatedhourlybasecost.value );
		document.forms[0].newestimatedtotalcost.value = round_func( document.forms[0].newestimatedtotalcost.value );
	
		if( ( document.forms[0].newproformamargin.value == "" ) || ( document.forms[0].newproformamargin.value == "null" ) || ( document.forms[0].newproformamargin.value >= "1" ) )
		{ 
			document.forms[0].newproformamargin.value = "0.0";
		}
	
		if( isFloat( document.forms[0].newproformamargin.value ) ) 
		{ 
			document.forms[0].newpriceunit.value = parseFloat( document.forms[0].newestimatedhourlybasecost.value ) / ( 1 - parseFloat( document.forms[0].newproformamargin.value ) );
			document.forms[0].newpriceunit.value = round_func( document.forms[0].newpriceunit.value );
		}
		
		if( isFloat( document.forms[0].newquantityhours.value ) ) 
		{ 
			document.forms[0].newpriceextended.value = parseFloat( document.forms[0].newpriceunit.value ) * parseFloat( document.forms[0].newquantityhours.value );
			document.forms[0].newpriceextended.value = round_func( document.forms[0].newpriceextended.value );
		}
	}
}

//Start :Added by Amit For Mass Delete


function del()
{

	
	var submitflag = 'false';
	if( <%= labor_size %> != 0 )
	{
		if( <%= labor_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newlaborid.checked ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassdelete"/>" );
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newlaborid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassdelete"/>" );
				return false;
		  	}
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassdelete"/>" );
	}
	
	if( document.forms[0].newlaborid.checked ) 
  	{
	  	
	  	if( document.forms[0].newlabornamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassdelete"/>" );
 			document.forms[0].newlaborname.focus();
 			return false;
 		}
	
  	}
  	else
  	{
  	}

	if(submitflag =='true')
	{

		var convel = confirm( "<bean:message bundle = "AM" key = "am.resource.delete.confirm"/>" );
		if( convel )
		{
			document.forms[0].action = "LaborResourceAction.do?ref=Delete&Activitylibrary_Id=<%= Id %>&fromResourceTabular=Labor";			
			document.forms[0].submit();
			return true;	
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassdelete"/>" );
		return false;
	}
	
}

//End : For Mass delete
//Start:Added By Amit For Mass Approve
function massApprove()
{
	var submitflag = 'false';
	if( <%= labor_size %> != 0 )
	{
		if( <%= labor_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newlaborid.checked ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassapprove"/>" );
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newlaborid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassapprove"/>" );
				return false;
		  	}
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassapprove"/>" );
	}
	
	if( document.forms[0].newlaborid.checked ) 
  	{
	  	
	  	if( document.forms[0].newlabornamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.labor.selectresourceformassapprove"/>" );
 			document.forms[0].newlaborname.focus();
 			return false;
 		}
	
  	}
  	else
  	{
  	}

	if(submitflag =='true')
	{

		document.forms[0].action = "LaborResourceAction.do?ref=MassApprove&Activitylibrary_Id=<%= Id %>&Type=L&Status=A";			
		document.forms[0].submit();
		return true;
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.material.selectresourceformassapprove"/>" );
		return false;
	}
}
//End :For Mass Approve



</script>

</body>
</html:html>