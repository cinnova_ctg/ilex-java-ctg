<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%  String msa_id = "";
	String category_Id = "";
	String msaname = "";
	String MSA_Id = "";
	
	if( request.getAttribute( "MSA_Id" ) != null )
		MSA_Id = ( String ) request.getAttribute( "MSA_Id" );
	//Start :Added By Amit For checkNetmedx
	if( request.getAttribute( "MSA_Id" ) != null )
		msa_id = ( String ) request.getAttribute( "MSA_Id" );
	if(request.getAttribute("msaname")!=null)
	{
		msaname = (String)request.getAttribute("msaname");
	}	
	//End :Added By Amit
	if( request.getAttribute( "MSA_Id" ) != null )
		msa_id = ( String ) request.getAttribute( "MSA_Id" );

	if( request.getAttribute( "category_Id" ) != null )
		category_Id = ( String ) request.getAttribute( "category_Id" );

	int activity_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString()); %> 
<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.tabular.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href = "docm/styles/style.css" type = "text/css">
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "javaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
</head>

<%
	String backgroundclass = "texto";
	String backgroundhyperlinkclass = null;
	String displaycombo = null;
	String readonlyclass = null;
	String readonlynumberclass = null;
	String backnew = "";
	int i = 0;
	String comboclass = null;
	boolean csschooser = true;
	String schdayscheck = null;
	String valstr = null;
%>

<Body>
	<div id="menunav">
	    <ul>
	  		<li>
	        	<a href="#" class="drop"><span>Activity</span></a>
	  			<ul>
	  				<li><a href="ManageActivityAction.do?MSA_Id=<%= MSA_Id %>&ref=addActivity">Add</a></li>
	 				<li><a href="ManageActivityAction.do?MSA_Id=<%= MSA_Id %>&ref=manageActivity">Manage</a></li>
	 				<li><a href="ActivitySummaryAction.do?ref=View&MSA_Id=<%=MSA_Id %>">View</a></li>
	 				<%
	 				if(request.getAttribute("checkformastercopy") != null && request.getAttribute("checkformastercopy").equals("N")) {
	 					%>
	 					<li><a href="MastertolibcopyAction.do?ref=showAllMSA&MSA_Id=<%= MSA_Id %>">Copy</a></li>
	 					<%
 					}
 					%>
	  			</ul>
	  		</li>
	  		<li>
	        	<a href="#" class="drop"><span><bean:message bundle = "AM" key = "am.summary.Partnerinfo"/></span></a>
	  			<ul>
	  				<li><a href="PartnerSOWAssumptionList.do?fromId=<%= MSA_Id %>&viewType=sow&fromType=am_act">Manage SOW</a></li>
	 				<li><a href="PartnerSOWAssumptionList.do?fromId=<%= MSA_Id %>&viewType=assumption&fromType=am_act">Manage Assumptions</a></li>
	  			</ul>
	  		</li>
		</ul>
	</div>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	<tr>
    	<td class = "toprow"> 
	    	<table  border = "0" cellspacing = "0" cellpadding = "0" height = "18">
		        <tr>
		        	<td background="images/content_head_04.jpg" height="21" colspan=3>
			         <div id="breadCrumb">
						 <a href="#" class="bgNone">Activity Manager</a>
			         	 <a href="ActivitySummaryAction.do?ref=View&MSA_Id=<%=msa_id %>" background="none;"><bean:write name = "msaname" scope = "request"/></a>
						 <a>
						 	<span class="breadCrumb1">Manage Activity&nbsp;
						 		<%if(request.getAttribute("categoeyName") != null && !request.getAttribute("categoeyName").equals(""))  {%>
						 			(<%=request.getAttribute("categoeyName")%>)
						 		<%}%>
						 	</span>
						 </a>
					</div>
			    	</td>
				</tr>
		        <tr><td height="2">&nbsp;</td></tr>	
	      	</table>
    	</td>
	</tr>
</table>

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
 <tr>
   	<td>
  		<table border = "0" cellspacing = "1" cellpadding = "1">
  				<!-- Start :Added By Amit -->
  			<logic:present name = "changestatusflag" scope = "request">
		  		<tr><td>&nbsp;</td>
			    	<td colspan = "5" class = "message" height = "30">
				  		<logic:equal name = "changestatusflag" value = "0">
						<bean:message bundle = "AM" key = "am.tabular.resourcesapprovedsuccessfully"/>
						</logic:equal>
						<logic:equal name = "changestatusflag" value = "-9002">
						<bean:message bundle = "AM" key = "am.tabular.resourcescannottapprovedsuccessfully"/>
						</logic:equal>
					</td>
					</tr>
		  		</logic:present>  
		  		
		  		<logic:present name = "Approved" scope = "request">
		  			<tr><td>&nbsp;</td>
			    	<td colspan = "5" class = "message" height = "30">
				    	<logic:equal name = "Approved" value = "true">
				    		<bean:message bundle = "AM" key = "am.tabular.allactivityapprovedesuccessfully"/>
				    	</logic:equal>
				    	<logic:equal name = "Approved" value = "false">
				    		<bean:message bundle = "AM" key = "am.tabular.noactivityapprovedesuccessfully"/>
				    	</logic:equal>
				    	<logic:equal name = "Approved" value = "some">
				    		<bean:message bundle = "AM" key = "am.tabular.someactivityapprovedesuccessfully"/>
				    		
				    	</logic:equal>
			       	</td>
			    	</tr>
		  		</logic:present>
		  				  		
		  		<logic:present name = "updateflag" scope = "request">
		  		<tr><td>&nbsp;</td>
		  			<td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "updateflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.tabular.update.success"/>
			    		<script>
								parent.ilexleft.location.reload();
						</script>
			    	</logic:equal>
			    	
			    	<logic:equal name = "updateflag" value = "-9001">
			    		<bean:message bundle = "AM" key = "am.tabular.update.failure1"/>		
			    	</logic:equal>
			    	
		
					<logic:equal name = "updateflag" value = "-9002">
			    		<bean:message bundle = "AM" key = "am.tabular.update.failure2"/>		
			    	</logic:equal>

			    	<logic:equal name = "deleteflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.tabular.deletemass.success"/>
			    		<script>
								parent.ilexleft.location.reload();
						</script>
			    	</logic:equal>

			    </td>
    		</tr>
				</logic:present>
  		
  		
				<logic:present name = "addflag" scope = "request">
				<tr><td>&nbsp;</td>
		  			<td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "addflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.tabular.add.success"/>
			    		<script>
								parent.ilexleft.location.reload();
						</script>
			    	</logic:equal>
			    	
			    	<logic:equal name = "addflag" value = "-9001">
			    		<bean:message bundle = "AM" key = "am.tabular.add.failure1"/>
			    	</logic:equal>
			    	
					<logic:equal name = "addflag" value = "-9002">
			    		<bean:message bundle = "AM" key = "am.tabular.add.failure2"/>
			    	</logic:equal>
				
					<logic:equal name = "addflag" value = "-9003">
			    		<bean:message bundle = "AM" key = "am.tabular.add.failure3"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "addflag" value = "-9004">
			    		<bean:message bundle = "AM" key = "am.tabular.add.failure4"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "addflag" value = "-9005">
			    		<bean:message bundle = "AM" key = "am.tabular.add.failure5"/>
			    	</logic:equal>
			    	</td>
    			</tr>
				</logic:present>
			
				<logic:present name = "deleteflag" scope = "request">
				<tr><td>&nbsp;</td>
		  			<td colspan = "5" class = "message" height = "30">
	    			<logic:equal name = "deleteflag" value = "0">
	    				<bean:message bundle = "AM" key = "am.tabular.delete.success"/>
    					<script>
							parent.ilexleft.location.reload();
						</script>
	    			</logic:equal>
	    			
	    			<logic:equal name = "deleteflag" value = "-9001">
    					<bean:message bundle = "AM" key = "am.tabular.delete.failure1"/>
    				</logic:equal>
	    			
    				<logic:equal name = "deleteflag" value = "-9002">
    					<bean:message bundle = "AM" key = "am.tabular.delete.failure2"/>
    				</logic:equal>
    				</td>
    			</tr>
	    		</logic:present>
	    		
	    		
	    		
	    		
	    		  <html:form action = "/ManageActivityAction">
  					<input type='hidden' name='category_Id' value='' />
  					<html:hidden name = "ManageActivityForm" property = "msaname" />
  					<logic:present name = "activitylist" scope = "request" > 
  					<% 
  						String previousCategory = "";
  						int divCheck=0;
  						boolean isAllApproved = true;
	  				%>
  					<logic:iterate id = "ms" name = "activitylist">
	    		
	    		
					    <bean:define id = "currentCategory" name = "ms" property = "category_type" type = "java.lang.String"/>
						<% if(!previousCategory.equals(currentCategory)) { 
							previousCategory = currentCategory;
							csschooser = false;
							if(i != 0) { %>
								</table></td></tr>
							</table></td></tr>		
						<%	}
						%>
						<tr>
						<td valign="top"><a href="javascript:toggleMenuSection('<%=currentCategory %>');"><img id='img_<%=currentCategory %>' src="images/Expand.gif" border="0" alt="+"  /></a></td>
						<td colspan=13><table border="0" cellspacing="0" cellpadding="0" width="700">
						<tr>
				    		<td class = "NHeadingFont" colspan = "17" style="text-align: left;" height="10">
				    			<B><bean:write name = "ms" property = "category_type" /></B>  
				    		</td>
				    	</tr>
				    	<tr>
				    		<td colspan = "17" height = "1">
				    		</td>
						</tr>
						<tr>
						<td colspan=17>
						<table width="100%" id = 'div_<%=currentCategory %>'  style="display: none" border="0" cellspacing="1" cellpadding="0">
	    		
						  		<tr> 
						    		<td rowspan = "2">&nbsp; </td>
						    		<td class = "Nhyperoddfont" rowspan = "2" align="center">
						    			<B><bean:message bundle = "AM" key = "am.tabular.name"/></B>
						    		</td>
						   
									<td class = "Nhyperoddfont" rowspan = "2" align="center">
										<B><bean:message bundle = "AM" key = "am.tabular.type"/></B>
									</td>
									
									<td class = "Nhyperoddfont" rowspan = "2" align="center">
										<B><bean:message bundle = "AM" key = "am.tabular.category"/></B>
									</td>
									
									<td class = "Nhyperoddfont" rowspan = "2" align="center">
										<B><bean:message bundle = "AM" key = "am.tabular.quantity"/></B>
									</td>
								    <td class = "Nhyperoddfont" colspan = "6" align="center">
								    	<B><bean:message bundle = "AM" key = "am.tabular.estimatedcost"/></B>
								    </td>
								    <td class = "Nhyperoddfont" rowspan = "2" align="center">
								    	<B><bean:message bundle = "AM" key = "am.tabular.extendedprice"/></B>
								    </td>
								    <td class = "Nhyperoddfont" rowspan = "2" align="center">
								    	<B><bean:message bundle = "AM" key = "am.tabular.proformamargin"/></B>
								    </td>
								    <td class = "Nhyperoddfont" rowspan = "2" align="center">
								    	<B><bean:message bundle = "AM" key = "am.tabular.status"/></B>
								    </td>
								    
								</tr>
						  
						  		<tr> 
						    		<td class = "Nhyperoddfont"> 
						      			<div align = "center">
						      				<B><bean:message bundle = "AM" key = "am.tabular.estimatedcnslabourcost"/></B>
						      			</div>
						    		</td>
						    
						    
						    		<td class = "Nhyperoddfont"> 
						      			<div align = "center">
						      				<B><bean:message bundle = "AM" key = "am.tabular.estimatedcontractlabourcost"/></B>
						      			</div>
						    		</td>
						    		<td class = "Nhyperoddfont"> 
						      			<div align = "center">
						      				<B><bean:message bundle = "AM" key = "am.tabular.estimatedmaterialcost"/></B>
						      			</div>
						    		</td>
						    
								    <td class = "Nhyperoddfont"> 
								      <div align = "center">
								      	<B><bean:message bundle = "AM" key = "am.tabular.estimatedfreightcost"/></B>
								      </div>
								    </td>
						    
								    <td class = "Nhyperoddfont"> 
								      <div align = "center">
								      	<B><bean:message bundle = "AM" key = "am.tabular.estimatedtravelcost"/></B>
								      </div>
								    </td>
						    
								    <td class = "Nhyperoddfont"> 
								      <div align = "center">
								      	<B><bean:message bundle = "AM" key = "am.tabular.estimatedtotalcost"/></B>
								      </div>
								    </td>
						    
								</tr>
						<%} %>
						<% if(previousCategory.equals(currentCategory)) { 			%>
							<%	
								if ( csschooser == true ) 
								{
									backgroundclass = "texto";
									backgroundhyperlinkclass = "hyperodd";
									comboclass = "combooddhidden";
									csschooser = false;
									readonlyclass = "readonlytextodd";
									readonlynumberclass = "readonlytextnumberodd";
								}
						
								else
								{
									csschooser = true;	
									comboclass = "comboevenhidden";
									backgroundhyperlinkclass = "hypereven";
									backgroundclass = "texte";
									readonlyclass = "readonlytexteven";
									readonlynumberclass = "readonlytextnumbereven";
								}
							%>
							<bean:define id = "val" name = "ms" property = "activity_Id" type = "java.lang.String"/>
		
					<tr>		
						<td class = "labeleboldwhite"> 
							<logic:equal name = "ms" property = "status" value = "Approved">
								<html:hidden property= "check" value = "0"/>
								
							</logic:equal>
							
							<logic:notEqual name = "ms" property = "status" value = "Approved">
								<html:checkbox property = "check" value = "<%= val %>" />
								<%isAllApproved = false; %>
							</logic:notEqual>	
			    		</td>
						
						<html:hidden name = "ms" property = "activity_Id" />
						<%  
							if( activity_size == 1 )
							{
								valstr = "javascript: document.forms[0].check.checked = true;updatetext(this,'');";
								displaycombo = "javascript: displaycombo(this,'')";
								schdayscheck = "javascript: document.forms[0].check.checked = true;";
								
							}
							else if( activity_size > 1 )
							{
								valstr = "javascript: document.forms[0].check['"+i+"'].checked = true;updatetext( this ,'"+i+"' );";
								schdayscheck = "javascript: document.forms[0].check['"+i+"'].checked = true;";
								displaycombo = "javascript: displaycombo( this , '"+i+"' )";
							}
						%>
						<td class = "<%= backgroundhyperlinkclass %>"> 
							<logic:notEqual name = "ms" property = "status" value = "Approved"> 
								<html:text  styleId = '<%= "name"+i%>' name="ms" property="name" size = "40" styleClass = "textboxActivity"  onchange = "<%= schdayscheck %>"></html:text>
							</logic:notEqual>
							 <logic:equal name = "ms" property = "status" value = "Approved">
							 	<html:text  styleId = '<%= "name"+i%>' name="ms" property="name" size = "40" styleClass = "<%= readonlyclass %>" readonly = "true" ></html:text>
							 </logic:equal>
							 <a href = "ActivityLibraryDetailAction.do?type=AM&Activitylibrary_Id=<bean:write name = "ms" property = "activity_Id" />&MSA_Id=<bean:write name = "ms" property = "msa_Id" />"><bean:message bundle = "AM" key = "am.libdetail.view"/></a>&nbsp;
						</td>
					
						<logic:equal name = "ms" property = "status" value = "Approved">
							<td id = "<%= "activitytypecombo"+i + "td"%>" name="activitytypecombotd" class = "<%= backgroundclass %>">
								<html:text styleId = '<%= "activitytype"+i%>' name = "ms" property = "activitytype" size = "22"  styleClass = "<%= readonlyclass %>" readonly = "true" />
								<html:hidden name = "ms" property = "activitytypecombo" />
							</td>
						</logic:equal>	
								
						<logic:notEqual name = "ms" property = "status" value = "Approved"> 		
							<td id = "<%= "activitytypecombo"+i + "td"%>" name="activitytypecombotd" class = "<%= backgroundclass %>">
								<html:text styleId = '<%= "activitytype"+i%>' name = "ms" property = "activitytype" size = "22" styleClass = "textboxActivity" onfocus = "<%= displaycombo %>" readonly = "true" />
								
								<html:select  styleId = '<%= "activitytypecombo"+i%>' name = "ms" property = "activitytypecombo" styleClass = "<%= comboclass %>" onblur = "hidecombo( this );"  onchange = "<%= valstr %>">
									<html:optionsCollection  property = "activity" value = "value" label = "label"/> 
								</html:select>
							</td>
						</logic:notEqual>
					
							<logic:equal name = "ms" property = "status" value = "Approved">
								<td id = "<%= "category_typecombo"+i + "td"%>" name="category_typecombotd" class = "<%= backgroundclass %>">
									<html:text styleId = '<%= "category_type"+i%>' name = "ms" property = "category_type" size = "15" styleClass = "<%= readonlyclass %>" readonly = "true" />
									<html:hidden name = "ms" property = "category_typecombo" />
								</td>
							</logic:equal>
							
							<logic:notEqual name = "ms" property = "status" value = "Approved">
								<td id = "<%= "category_typecombo"+i + "td"%>" name="category_typecombotd" class = "<%= backgroundclass %>">
									<html:text styleId = '<%= "category_type"+i%>' name = "ms" property = "category_type" size = "15" styleClass = "textboxActivity" onfocus = "<%= displaycombo %>" readonly = "true" />
									<html:select styleId = '<%= "category_typecombo"+i%>' name = "ms" property = "category_typecombo"  styleClass = "<%= comboclass %>" onblur = "hidecombo( this );"  onchange = "<%= valstr %>">
										   <html:optionsCollection  property = "category" value = "value" label = "label"/> 
									</html:select>
								</td>
							</logic:notEqual>	
						
						<html:hidden name = "ms" property = "cat_Id" />
						
						<td class = "<%= backgroundclass %>" style="text-align: right; padding-right: 3px;">  
							<bean:write name="ms" property="quantity"/>
						</td>
						
					   <td class = "<%= backgroundclass %>" style="text-align: right; padding-right: 3px;">
							<bean:write name="ms" property="estimatedcnslabourcost"/>
					   </td>
					   
					   <td class = "<%= backgroundclass %>" style="text-align: right; padding-right: 3px;">
							<bean:write name="ms" property="estimatedcontractlabourcost"/>
					   </td>
					   	<td class = "<%= backgroundclass %>" style="text-align: right; padding-right: 3px;">
							<bean:write name="ms" property="estimatedmaterialcost"/>
					   </td>
					   
					   
					   <td class = "<%= backgroundclass %>" style="text-align: right; padding-right: 3px;">
							<bean:write name="ms" property="estimatedfreightcost"/>
					   </td>
					   
					   <td class = "<%= backgroundclass %>" style="text-align: right; padding-right: 3px;">
							<bean:write name="ms" property="estimatedtravelcost"/>
					   </td> 
					   
					   <td class = "<%= backgroundclass %>" style="text-align: right; padding-right: 3px;">
							<bean:write name="ms" property="estimatedtotalcost"/>
					   </td> 
					   
					   <td class = "<%= backgroundclass %>" style="text-align: right; padding-right: 3px;">
							<bean:write name="ms" property="extendedprice"/>
					   </td>
					   
					   <td class = "<%= backgroundclass %>" style="text-align: right; padding-right: 3px;">
							<bean:write name="ms" property="proformamargin"/>
					   </td>
					   
					   <td class = "<%= backgroundclass %>" style="text-align: center;">
							&nbsp;<bean:write name = "ms" property = "status"/>&nbsp;		
						</td> 
			  		</tr>
  		<%} %>
		   		<% i++;  %>
		   		<%if(Integer.parseInt(request.getAttribute("Size").toString()) == i) { %>
		   			</table></td></tr>
		   		<%} %>
	  </logic:iterate>
	   </table></td></tr>
  
		<html:hidden property = "msa_Id" />  		
  		
		<%if(request.getAttribute("Size") != null && Integer.parseInt(request.getAttribute("Size").toString()) >0 ) { %>
		<%if(!isAllApproved) { %>
				<tr>	
					<td ></td>
				  	<td colspan = "13" class = "buttonrow">
				  		<html:submit property = "update" styleClass = "button" onclick = "return validate();">Submit</html:submit>
				  		<html:button property = "sort" styleClass = "button" onclick = "return sortwindow();"><bean:message bundle = "AM" key = "am.tabular.sort"/></html:button>
				  		<!-- Start :Added By Amit for Mass delete And Approve-->
				  		<logic:notEqual name="ManageActivityForm" property="msaname" value="NetMedX">
							<html:submit property = "delete" styleClass = "button" onclick = "return del();"><bean:message bundle = "AM" key = "am.tabular.massDelete"/></html:submit>
							<html:submit property = "approve" styleClass = "button" onclick = "return massApprove();"><bean:message bundle = "AM" key = "am.tabular.massApprove"/></html:submit>
						</logic:notEqual>
						<!-- End :Added By Amit for Mass delete -->
						<html:reset property = "cancel" styleClass = "button"><bean:message bundle = "AM" key = "am.tabular.cancel"/></html:reset>
					</td>
				</tr>
				<%} else { %>
					<tr>
						<td></td>	
					  	<td colspan = "13" class = "buttonrow" align="left" >
					  		<html:button property = "sort" styleClass = "button" onclick = "return sortwindow();"><bean:message bundle = "AM" key = "am.tabular.sort"/></html:button>
						</td>
					</tr>
				<%} %>
			<%} else { %>
				<tr>
				    <td colspan = "17" class = "message" height = "30" style="padding-left:10px;"> 
				    	<bean:message bundle = "AM" key = "am.summary.noactivity"/>
				    </td>  	
      			</tr>
			<%} %>
	</logic:present>
	<jsp:include page = '/Footer.jsp'>
		      <jsp:param name = 'colspan' value = '37'/>
		      <jsp:param name = 'helpid' value = 'ammanage'/>
		   </jsp:include>
	
		
	</table>
	</td>
</tr>
</table>
  
</html:form>

<script>
function sortwindow()
{
	if( <%= request.getAttribute( "category_Id" ) != null  %>  )
	{
		str = 'SortAction.do?Type=amc&MSA_Id=<%= msa_id %>&category_Id=<%= category_Id %>';
	}
	else
	{
		str = 'SortAction.do?Type=am&MSA_Id=<%= msa_id %>';
	}
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	/* p = showModelessDialog( str , window , 'status:false;dialogWidth:400px; dialogHeight:350px' );  */
	
	
}




function validate()
{
	var submitflag = 'false';
	if( <%= activity_size %> != 0 )
	{
		if( <%= activity_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ))
			{
				alert( "<bean:message bundle = "AM" key = "am.tabular.selectrow"/>" );
				return false;
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false')
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.tabular.selectrow"/>" );
				return false;
		  	}
		}
	}
	if( <%= activity_size %> != 0 )
	{
		if( <%= activity_size %> == 1 )
		{
			if( document.forms[0].check.checked )
			{
				if( document.forms[0].activitytypecombo.value == "0" )
				{
					alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivity"/>" );
 					document.forms[0].activitytype.focus();
 					return false;
				}
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			if( document.forms[0].activitytypecombo[i].value == "0" )
					{
						alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivity"/>" );
	 					document.forms[0].activitytype[i].focus();
	 					return false;
					}
		  		}
			}
		}
	}
	//alert(document.forms[0].category_Id.value)
	document.forms[0].category_Id.value = '<%= category_Id %>';
	return true;
}

//Start :Added by Amit For Mass Delete


function del()
{
	var submitflag = 'false';
	document.forms[0].category_Id.value = '<%= category_Id %>';
	if( <%= activity_size %> != 0 )
	{
		if( <%= activity_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ))
			{
				alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassdelete"/>" );
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassdelete"/>" );
				return false;
		  	}
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassdelete"/>" );
	}

	if(submitflag =='true')
	{
		
		var convel = confirm( "<bean:message bundle = "AM" key = "am.tabuler.delete.confirm"/>" );
		if( convel )
		{
			return true;	
		}
		else
		{
			return false;	
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassdelete"/>" );
		return false;
	}
	
}

//End : For Mass delete

//Start:Added By Amit For Mass Approve
function massApprove()
{
	var submitflag = 'false';
	document.forms[0].category_Id.value = '<%= category_Id %>';
	if( <%= activity_size %> != 0 )
	{
		if( <%= activity_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ))
			{
				alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassapprove"/>" );
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
			
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false')
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassapprove"/>" );
				return false;
		  	}
		}
	}
	else
	{
	//	alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivityformassapprove"/>" );
	}
	if(submitflag =='true')
	{
		document.forms[0].action = "ManageActivityAction.do?Type=am&Status=A";
		return true;
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.material.submitactivityforapprove"/>" );
		return false;
	}
}
//End :For Mass Approve

function toggleMenuSection(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	}
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
	}
	else {
		thisImage.src = "images/Expand.gif";
	}
}
function toggleDiv(divId) {
	var div  = document.getElementById(divId);
	var tableObj = document.getElementById(divId);
	if (div.offsetHeight > 0) {
		div.style.display = "none";
	} else {
		div.style.display = "block";
	}
}
function expandSingleCategory(categoryId, categoryName) {
	if(categoryId != '' && categoryName != '') {
		toggleMenuSection(categoryName);
	}
}
	$(document).ready(function() {
			expandSingleCategory('<%=category_Id %>','<%=request.getAttribute("categoeyName")!=null?request.getAttribute("categoeyName"):""%>');
 	}
	);


</script>
</body>
</html:html>