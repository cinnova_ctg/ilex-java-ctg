<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html:html>
<%
String Job_Id="0";
int no_of_techns=0;
int no_of_engs=0;
String type="";
String netmedxcatgid="0";
String MSA_Id="0";
String Activitylibrary_Id="0";
int totalresources=0;
int cpcrit=0;
int totalcategories=0;
String setvalue = "";
int size=0;
String jobName = "";

String clrrate = "clrrate";
String detailid = "detailid";
String resourceid = "resourceid";
String selectresourceid = "selectresourceid";
String resourcename = "resourcename";


if(request.getAttribute( "listsize" ).toString()!=null) {
	size =( int ) Integer.parseInt( request.getAttribute( "listsize" ).toString() ); 
}
if( ( String )request.getAttribute( "Job_Id" ) != "" ) {
	Job_Id=( String )request.getAttribute( "Job_Id" );
}
if( ( String )request.getAttribute( "type" ) != "" ) {
	type=( String )request.getAttribute( "type" );
}
if( ( String )request.getAttribute( "Activitylibrary_Id" ) != "" ) {
	Activitylibrary_Id=( String )request.getAttribute( "Activitylibrary_Id" );
}
if( ( String )request.getAttribute( "netmedxcatgid" ) != null ) {
	netmedxcatgid=( String )request.getAttribute( "netmedxcatgid" );
}

if(request.getAttribute("technos")!=null) {
	no_of_techns=Integer.parseInt(request.getAttribute("technos").toString());
}
if(request.getAttribute("enggnos")!=null) {
	no_of_engs=Integer.parseInt(request.getAttribute("enggnos").toString()); 
}
if(request.getAttribute("totalresources")!=null) {
	totalresources=Integer.parseInt(request.getAttribute("totalresources").toString()); 
}
/* for adding extra fields in CLR Form start*/
if(request.getAttribute("categoriespercriticality")!=null) {
	cpcrit=Integer.parseInt(request.getAttribute("categoriespercriticality").toString()); 
}
if(request.getAttribute("totalcategories")!=null) {
	totalcategories=Integer.parseInt(request.getAttribute("totalcategories").toString()); 
}

if(request.getAttribute("jobName") != null) {
	jobName = request.getAttribute("jobName").toString();
}
/* for adding extra fields in CLR Form end*/
%>

<head>
<title></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href = "styles/content.css" type = "text/css">
	<link rel = "stylesheet" href = "styles/summary.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>

<%@ include  file="/Menu.inc" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" >
<html:form action="CustomerLaborRates" >
<html:hidden name="CustomerLaborRatesForm" property="type"/>
<html:hidden name="CustomerLaborRatesForm" property="msa_Id"/>
<html:hidden name="CustomerLaborRatesForm" property="activitylibrary_Id"/>
<html:hidden name="CustomerLaborRatesForm" property="customerid"/>
<html:hidden name="CustomerLaborRatesForm" property="refclr"/>
<html:hidden name="CustomerLaborRatesForm" property="showAppendixId"/>
<html:hidden name="CustomerLaborRatesForm" property="showAppendixName"/>
<html:hidden name="CustomerLaborRatesForm" property="fromPage"/>

<logic:present  name="listsize">
 <logic:greaterThan name="listsize"  value="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
        <tr align="left"> 
	       <%if(request.getAttribute("showlink").equals("0"))
			{
	         if(request.getAttribute("type").equals("AM")|| request.getAttribute("type").equals("AM_EDIT"))
	         {
	         	if(request.getAttribute("type").equals("AM_EDIT"))
	         	{%>
	          	<td class ="Ntoprow1" width="80" align="center"><a href="ActivityLibraryDetailAction.do?ref=View&type=AM&MSA_Id=<%=request.getAttribute("MSA_Id") %>&Activitylibrary_Id=<%=request.getAttribute("Activitylibrary_Id") %>" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class = "menufont" style="width: 80px;"><bean:message bundle="AM" key="am.back"/></a><td>
	          <%}
		       else
			   { %>
			  	<td class ="Ntoprow1" width="80" align="center"><a href="ManageActivityAction.do?ref=View&MSA_Id=<%=request.getAttribute("MSA_Id") %>&Category_Id=<%=netmedxcatgid%>" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class = "menufont" style="width: 80px;"><bean:message bundle="AM" key="am.back"/></a></td>
			  <%} %>
			  <td width = "180" class ="Ntoprow1" align="center"><a href = "ActivityLibraryDetailAction.do?ref=View&type=AM_EDIT&MSA_Id=<%=request.getAttribute("MSA_Id") %>&Activitylibrary_Id=<%=request.getAttribute("Activitylibrary_Id") %>" onMouseOut = "MM_swapImgRestore();" onMouseOver = "MM_swapImage( 'Image25' , '' , 'images/about1b.gif' , 1 );" class = "menufont" style="width: 180px;"><bean:message bundle="AM" key="am.clr.adddefaultlaborrates"/></a></td>
	          <td class ="Ntoprow1" width="120" align="center"><a href="AddClrReslevelAction.do?ref=add" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class = "menufont" style="width: 120px;"><bean:message bundle="AM" key="am.clr.addnewskill"/></a><td>
	          <td class ="Ntoprow1" width="170" align="center"><a href="AddClrCategoryAction.do?ref=add" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class = "menufont" style="width: 170px;"><bean:message bundle="AM" key="am.clr.addnewarrivaloption"/></a><td>
	         <%}
		   }
		   else
		   { 
		   %>
			   <td class ="Ntoprow1" width="80" align="center" colspan="4"><a href="javascript:history.go(-1);" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class = "menufont" style="width: 80px;"><bean:message bundle="AM" key="am.back"/></a><td>
		   <%} %>
		  <td  width=530 class ="Ntoprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
      </table>
    </td>
  </tr>
  <tr>
		<td background="images/content_head_04.jpg" height="21" width="800">
			<div id="breadCrumb">
				<c:if test="${CustomerLaborRatesForm.type eq 'PM'}">
					<c:if test="${CustomerLaborRatesForm.fromPage eq 'jobdashboard'}">
						<a class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
						<a><bean:write name = "CustomerLaborRatesForm" property = "customername"/></a>
						<a href="AppendixHeader.do?function=view&appendixid=<bean:write name = "CustomerLaborRatesForm" property = "showAppendixId"/>&viewjobtype=A&ownerId=%msaId=<bean:write name = "CustomerLaborRatesForm" property = "msa_Id" />">
		  					<bean:write name = "CustomerLaborRatesForm" property = "showAppendixName"/>
		  				</a>
		  				<a href="JobDashboardAction.do?function=view&jobid=<%=Job_Id %>&appendixid=<bean:write name = "CustomerLaborRatesForm" property = "showAppendixId"/>"><%=jobName %></a>
		  				<a><span class="breadCrumb1">Customer Labor Rates</span></a>
					</c:if>
					<c:if test="${CustomerLaborRatesForm.fromPage ne 'jobdashboard'}">
						<a href="MSAUpdateAction.do?firstCall=true" class="bgNone"><bean:message bundle = "pm" key = "common.msa.list"/></a>
		  				<a href= "AppendixUpdateAction.do?firstCall=true&MSA_Id=<bean:write name = "CustomerLaborRatesForm" property = "msa_Id" />">
		  					<bean:write name = "CustomerLaborRatesForm" property = "customername"/>
		  				</a>
		  				<a href="JobUpdateAction.do?ref=detailjob&Appendix_Id=<bean:write name = "CustomerLaborRatesForm" property = "showAppendixId" />">
		  					<bean:write name = "CustomerLaborRatesForm" property = "showAppendixName"/>
		  				</a>
		      			<a href="ActivityUpdateAction.do?addendum_id=0&ref=View&Job_Id=<%=Job_Id %>"><%=jobName %></a>
						<a><span class="breadCrumb1">Customer Labor Rates</span></a>
					</c:if>
				</c:if>
				<c:if test="${CustomerLaborRatesForm.type eq 'AM' || CustomerLaborRatesForm.type eq 'AM_EDIT'}">
	      			<a background="none;" class="bgNone" style="padding-left: 1px;">NetMedX</a>
	      			<a><span class="breadCrumb1">Default Customer Labor Rates</span></a>
				</c:if>		
			</div>		
		</td>
	</tr>
</table>

<SCRIPT language=JavaScript1.2>

if (isMenu) {
arMenu1 = new Array(
70,
22,18,
"","",
"","",
"","",
"ll","#",0
)
document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
}

</script>
<table>

<table  border="0" cellspacing="1" cellpadding="1" width="1100">
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="800">
	  		<tr height="10"><td></td></tr>
	  		<tr>  
	  			<logic:present  name="addmessage" scope="request">
					<logic:equal  name="addmessage" value="-9001">
						<td  colspan="3" class="message" height="20" ><bean:message bundle="AM" key="am.clr.addfailed"/></td>
		 			</logic:equal>
			 		<logic:equal  name="addmessage" value="0">
						<td  colspan="3" class="message" height="20" ><bean:message bundle="AM" key="am.clr.addedsucessfully"/></td>
			 		</logic:equal>	
		 		</logic:present>
		 		
		 		<logic:present  name="deletemessage" scope="request">
			 		<logic:notEqual name="deletemessage" value="0">
						<td  colspan="3" class="message" height="20" ><bean:message bundle="AM" key="am.clr.deletefailed"/></td>
			 		</logic:notEqual>
			 		<logic:equal  name="deletemessage" value="0">
						<td  colspan="3" class="message" height="20" ><bean:message bundle="AM" key="am.clr.deletedsucessfully"/></td>
			 		</logic:equal>
		 		</logic:present>
			</tr>  
    <!--  for discount rates link-->
  <% if(request.getAttribute("showlink").equals("0")) {
  		if(request.getAttribute("editflag").equals("0")) { 
	  			if(request.getAttribute("type").equals("PM")) {
         	%>
         			<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.customername"/></td>
		   			 <td class="colLight"><html:text  styleClass="text" size="20" property="customername" readonly="true"/></td>
		   			</tr>
         		<% } 
         		else
         		{ } %>
		    	<tr> 
				    <td  class="colDark"><bean:message bundle="AM" key="am.clr.minimumfee"/></td>
				    <td class="colLight"><html:text  styleClass="text" size="10" property="minimumfee" /></td>
				</tr>
		
		  		<tr> 
		    		 <td  class="colDark"><bean:message bundle="AM" key="am.clr.travel"/></td>
		   			 <td  class="colLight"><html:text  styleClass="text" size="15" property="travel" /></td>
		   			 <td width="250" ></td>
		 		 </tr>
		 		 
		 		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.travelmax"/></td>
		   			 <td class="colLight"><html:text  styleClass="text" size="10" property="travelmax" /></td>
		  		</tr>

		  		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.volumediscount"/></td>
		   			 <td class="colLight"><html:text  styleClass="text" size="10" property="discount" />&nbsp;&nbsp;&nbsp;<a href="javascript: calldiscount();" ><bean:message bundle="AM" key="am.clr.showdiscountedrates"/> </a></td>
		  		</tr>
		  		
		  		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.effdate"/></td>
		   			 <td class="colLight"><html:text  styleClass="text" size="10" property="effectivedate" readonly="true"/>
		   			 <img src = "images/calendar.gif" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar(document.forms[0].effectivedate, document.forms[0].effectivedate, 'mm/dd/yyyy')" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></td>
		   		</tr>
		  		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.rules"/></td>
		   			 <td class="colLight"><html:textarea  styleClass="text" property="rules" cols="50" rows="4" /></td>
		  		</tr>
		  	</table>
		 </td>
	</tr>
	<tr>
		<td  width="2" height="0"></td>
		<td><table border="0" cellspacing="1" cellpadding="1" >	  		
  
			 		<logic:present  name="ratelist">
					  <tr>
			 			<td colspan="10" class="labeleboldwhite" height="30"><bean:message bundle="AM" key="am.clr.customerlaborrateswithoutdiscount"/></td>
			 		  </tr>
					  <tr> 
					    <td class="Ntryb" rowspan="2" width="220" ><bean:message bundle = "AM" key = "am.clr.criticality"/></td>
					    <td class="Ntryb" rowspan="2" width="220"><bean:message bundle = "AM" key = "am.clr.description"/></td>
					   
					   	<logic:present  name="TechEngtypeslist">
					   		<logic:iterate id="tetlist" name="TechEngtypeslist" >
					   		<bean:define id="tlist" name = "tetlist" property="classnums" type="java.lang.String"/>
					   		
					   		<td class="Ntryb" colspan="<%=tlist %>" ><bean:write name = "tetlist" property="classname" /></td>
					   		</logic:iterate>
					   	</logic:present>
					  </tr>
					   <tr>
						      <logic:present  name="resourcelevellist">
					   			<logic:iterate id="reslist" name="resourcelevellist" >
					   			<td class="Ntryb" width="50">
					   			<bean:define id="rname" name = "reslist" property="resourcename" type="java.lang.String"/>
					   			 
					   			<div align="center" ><bean:write name = "reslist" property="resourcename" /></div>
					   			</td>
					   			</logic:iterate>
					   			</logic:present>
					  </tr>
				
						<%int j=1;
						int colorflag=1;
						String bgcolor="";
						String bgcolorno="";
						String bgcolortext="";
						String bgcolortext1="";
						String color=""; %>
					
						<logic:iterate id="lrlist" name="ratelist" >
						<html:hidden name = "lrlist" property="criticalityid" />
						<%if(!(((j-1)%cpcrit)==0))
						{
							bgcolor="Ntexte";
							bgcolorno="readonlytextnumbereven";
							//bgcolortext="readonlytextevenclr";
							bgcolortext1="text";
						} 
						else
						{
							bgcolor="Ntexto";
							bgcolorno="readonlytextnumberodd";
							//bgcolortext="readonlytextoddclr";
							bgcolortext1="text";
						}
						%>
				  		<tr> 
						    <%if(!(((j-1)%cpcrit)==0))
							{ } 
							else
							{
								if((colorflag%2)==0)
								{
									color="Ntexte";
									bgcolortext="Ntexte";
								}
								else
								{
									color="Ntexto";
									bgcolortext="Ntexto";
								} 
								colorflag++; %>
								
								<td   class="<%=color %>" rowspan="<%=cpcrit %>"><bean:write name = "lrlist" property="criticalityname" />
								<br><bean:write name = "lrlist" property="criticalitydesc" />
								</td>
							<%} %>
							<td   class="<%=bgcolortext %>" >
							<bean:write name = "lrlist" property="servicetime" /><br><bean:write name = "lrlist" property="categoryshortname" /></td>
						
							<%for(int a=0;a<(totalresources);a++) { %> 
							<html:hidden name = "lrlist" property="<%= detailid+a %>" />
							<html:hidden name = "lrlist" property="<%= resourceid+a %>" />
							<% if( size == 1 ) { 
								setvalue = "javascript:opensearchwindow( '' );";
							} else {
								setvalue = "javascript:opensearchwindow( "+a+","+j+");";
							} %>
							<td  class="<%=bgcolortext %>" width="50">
						    	<html:text styleClass="<%=bgcolortext1 %>" name = "lrlist" property = "<%= clrrate+a%>" size = "8"   /><br>
						    	<html:text styleClass="<%=bgcolortext1 %>" name = "lrlist" property = "<%= resourcename+a %>" size = "8"   readonly="true" onclick ="<%=setvalue%>"/></td>
						    <% } %>
						  </tr>
						  <%j++; %>
						</logic:iterate>
						<%
						String funcvalidate="";
						funcvalidate="javascript:return validate("+totalresources+","+totalcategories+");";
						%>
					 <tr> 
		 		 	  <td colspan="<%=totalresources+2 %>" class="buttonrow"> 
		 		      <html:submit property="save" styleClass="button_C" onclick = "<%=funcvalidate %>"><bean:message bundle="AM" key="am.clr.save" /></html:submit>
		 			  <html:reset property="reset" styleClass="button_C" ><bean:message bundle="AM" key="am.clr.cancel" /></html:reset>
					  <%if(request.getAttribute("type").equals("PM"))
        			 {%>
					  <html:button property="delete" styleClass="button_C" onclick="javascript:deleteres();"><bean:message bundle="AM" key="am.clr.delete" /></html:button>
					  <%}
					  else
					  { %>
					  <%} %>
					  </td>
					 </tr>
					 <tr>
					 	<td colspan="<%=totalresources+2 %>" class="dblabel">
					 	<bean:message bundle="AM" key="am.clr.ppsmf" />
					 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					 	<bean:message bundle="AM" key="am.clr.nonppsmf" />
					 	
					 	</td>
					 </tr>
				
					 </logic:present>
				 </table>
			</td>
		</tr>
				
		<%}
		
		else
		{ %>


	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="600">		
		  		
		  		
		  		<%if(request.getAttribute("type").equals("PM"))
         		{%>
         			<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.customername"/></td>
		   			 <td class="colLight"><html:text  styleClass = "text" size="20" property="customername" readonly="true"/></td>
		   			</tr>
         		
         		<%} 
         		else
         		{%>
         		
         		<%} %>
		  		
		 
		    	<tr> 
				    <td  class="colDark"><bean:message bundle="AM" key="am.clr.minimumfee"/></td>
				    <td class="colLight"><html:text  styleClass="text" size="10" property="minimumfee" readonly="true"/></td>
				</tr>
		
		  		<tr> 
		    		 <td  class="colDark"><bean:message bundle="AM" key="am.clr.travel"/></td>
		   			 <td  class="colLight"><html:text  styleClass="text" size="15" property="travel" readonly="true"/></td>
		 		 </tr>
		 		 
		 		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.travelmax"/></td>
		   			 <td class="colLight"><html:text  styleClass="text" size="10" property="travelmax" readonly="true"/>
		   			 </td>
		  		</tr>
		 
		
		  		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.volumediscount"/></td>
		   			 <td class="colLight"><html:text  styleClass="text" size="10" property="discount" readonly="true"/></td>
		  		</tr>
		  		
		  		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.effdate"/></td>
		   			 <td class="colLight"><html:text  styleClass="text" size="10" property="effectivedate" />
		   			 </td>
		   		</tr>
		  		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.rules"/></td>
		   			 <td class="colLight"><html:textarea  styleClass="text" property="rules" cols="50" rows="4" readonly="true"/></td>
		  		</tr>
		  		
			</table>
		</td>
	</tr> 		
	
	<tr>
		<td  width="2" height="0" ></td>
		<td><table border="0" cellspacing="1" cellpadding="1" >	  			  		
		  		  			  
			 		<logic:present  name="ratelist">
			 		<tr>
			 			<td colspan="10" class="labeleboldwhite" height="30"><bean:message bundle="AM" key="am.clr.customerlaborratesdiscount"/></td>
			 		</tr>
						
					  <tr> 
					    <td class="Ntryb" rowspan="2" width="220" ><bean:message bundle = "AM" key = "am.clr.criticality"/></td>
					    <td class="Ntryb" rowspan="2" width="220"><bean:message bundle = "AM" key = "am.clr.description"/></td>
					   
					   	<logic:present  name="TechEngtypeslist">
					   		<logic:iterate id="tetlist" name="TechEngtypeslist" >
					   		<bean:define id="tlist" name = "tetlist" property="classnums" type="java.lang.String"/>
					   		
					   		<td class="Ntryb" colspan="<%=tlist %>" ><bean:write name = "tetlist" property="classname" /></td>
					   		</logic:iterate>
					   	</logic:present>
					 </tr>
					   	
					 <tr>
					     
						      <logic:present  name="resourcelevellist">
					   			<logic:iterate id="reslist" name="resourcelevellist" >
					   			<td class="Ntryb" width="50">
					   			<bean:define id="rname" name = "reslist" property="resourcename" type="java.lang.String"/>
					   			 
					   			<div align="center" ><bean:write name = "reslist" property="resourcename" /></div>
					   			</td>
					   			</logic:iterate>
					   			</logic:present>

					  </tr>
		 		
					   
						<%int j=1;
						int colorflag=1;
						String bgcolor="";
						String bgcolorno="";
						String bgcolortext="";
						String bgcolortext1="";
						String color=""; %>
					
						<logic:iterate id="lrlist" name="ratelist" >
						<html:hidden name = "lrlist" property="criticalityid" />
						<%if(!(((j-1)%cpcrit)==0))
						{
							bgcolor="Ntexte";
							bgcolorno="readonlytextnumbereven";
							//bgcolortext="readonlytextevenclr";
							bgcolortext1="text";
						} 
						else
						{
							bgcolor="Ntexto";
							bgcolorno="readonlytextnumberodd";
							//bgcolortext="readonlytextoddclr";
							bgcolortext1="text";
						}%>
					
						  <tr>
						    <%if(!(((j-1)%cpcrit)==0))
							{%>
								
							<%} 
							else
							{
							if((colorflag%2)==0)
								{
									color="Ntexte";
									bgcolortext="Ntexte";
								}
								else
								{
									color="Ntexto";
									bgcolortext="Ntexto";
								} %>
								 
								 <td   class="<%=color %>"  rowspan="<%=cpcrit %>" ><bean:write name = "lrlist" property="criticalityname" />
								 <br><br><bean:write name = "lrlist" property="criticalitydesc" />
								</td>
								<%colorflag++; %>
							<%} %>
							<td   class="<%=bgcolortext %>" >
							<bean:write name = "lrlist" property="servicetime" /><br><bean:write name = "lrlist" property="categoryshortname" /></td>
						
							
							<%for(int a=0;a<(totalresources);a++)
							{
							%> 
							<html:hidden name = "lrlist" property="<%=detailid+a %>" />
							<html:hidden name = "lrlist" property="<%=resourceid+a%>" />
							<html:hidden name = "lrlist" property="<%=selectresourceid+a %>" />
							
							<td  class="<%=bgcolortext %>" width="50">
						    	<html:text styleClass="<%=bgcolortext1 %>" name = "lrlist" property = "<%= clrrate+a %>" size = "8"   readonly="true"/><br>
						    							
							
						    <%} %>
							
							    			
						  </tr>
						
						  <%j++; %>
						</logic:iterate>
						
					
					 </logic:present>
				</table>
		   </td>
   	</tr> 	
			<%} 
			
}
else	
{%>
			
<!-- for link show discounted rates start-->			
			




	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" width="600">		
		  		
		  		
		  		<%if(request.getAttribute("type").equals("PM"))
         		{%>
         			<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.customername"/></td>
		   			 <td class="colLight" ><html:text  styleClass = "text" size="20" property="customername" readonly="true"/></td>
		   			</tr>
         		
         		<%} 
         		else
         		{%>
         		
         		<%} %>
		  		
		 
		    	<tr> 
				    <td  class="colDark" ><bean:message bundle="AM" key="am.clr.minimumfee"/></td>
				    <td class="colLight" ><html:text  styleClass="text" size="10" property="minimumfee" readonly="true"/></td>
				</tr>
		
		  		<tr> 
		    		 <td  class="colDark"><bean:message bundle="AM" key="am.clr.travel"/></td>
		   			 <td  class="colLight"><html:text  styleClass="text" size="15" property="travel" readonly="true"/></td>
		 		 </tr>
		 		 
		 		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.travelmax"/></td>
		   			 <td class="colLight"><html:text  styleClass="text" size="10" property="travelmax" readonly="true"/>
		   			 </td>
		  		</tr>
		 
		
		  		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.volumediscount"/></td>
		   			 <td class="colLight"><html:text  styleClass="text" size="10" property="discount" readonly="true"/></td>
		  		</tr>
		  		
		  		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.effdate"/></td>
		   			 <td class="colLight"><html:text  styleClass="text" size="10" property="effectivedate" />
		   			 </td>
		   		</tr>
		  		<tr>
		   			 <td class="colDark"><bean:message bundle="AM" key="am.clr.rules"/></td>
		   			 <td class="colLight"><html:textarea  styleClass="text" property="rules" cols="50" rows="4" readonly="true"/></td>
		  		</tr>
		  		
			</table>
		</td>
	</tr> 		
	
	<tr>
		<td  width="2" height="0" ></td>
		<td><table border="0" cellspacing="1" cellpadding="1" >	  			  		
		  		  			  
			 		<logic:present  name="ratelist">
			 		<tr>
			 			<td colspan="10" class="labeleboldwhite" height="30"><bean:message bundle="AM" key="am.clr.customerlaborratesdiscount"/></td>
			 		</tr>
						
					  <tr> 
					    <td class="Ntryb" rowspan="2" width="220" ><bean:message bundle = "AM" key = "am.clr.criticality"/></td>
					    <td class="Ntryb" rowspan="2" width="220"><bean:message bundle = "AM" key = "am.clr.description"/></td>
					   
					   	<logic:present  name="TechEngtypeslist">
					   		<logic:iterate id="tetlist" name="TechEngtypeslist" >
					   		<bean:define id="tlist" name = "tetlist" property="classnums" type="java.lang.String"/>
					   		
					   		<td class="Ntryb" colspan="<%=tlist %>" ><bean:write name = "tetlist" property="classname" /></td>
					   		</logic:iterate>
					   	</logic:present>
					 </tr>
					   	
					 <tr>
					     
						      <logic:present  name="resourcelevellist">
					   			<logic:iterate id="reslist" name="resourcelevellist" >
					   			<td class="Ntryb" width="50">
					   			<bean:define id="rname" name = "reslist" property="resourcename" type="java.lang.String"/>
					   			 
					   			<div align="center" ><bean:write name = "reslist" property="resourcename" /></div>
					   			</td>
					   			</logic:iterate>
					   			</logic:present>

					  </tr>
		 		
					   
						<%int j=1;
						int colorflag=1;
						String bgcolor="";
						String bgcolorno="";
						String bgcolortext="";
						String bgcolortext1="text";
						String color=""; %>
					
						<logic:iterate id="lrlist" name="ratelist" >
						<html:hidden name = "lrlist" property="criticalityid" />
						<%if(!(((j-1)%cpcrit)==0))
						{
							bgcolor="Ntexte";
							bgcolorno="readonlytextnumbereven";
							//bgcolortext="readonlytextevenclr";
						
						} 
						else
						{
							bgcolor="Ntexto";
							bgcolorno="readonlytextnumberodd";
							//bgcolortext="readonlytextoddclr";
						}%>
					
						  <tr>
						    <%if(!(((j-1)%cpcrit)==0))
							{%>
								
							<%} 
							else
							{
							if((colorflag%2)==0)
								{
									color="Ntexte";
									bgcolortext="Ntexte";
								}
								else
								{
									color="Ntexto";
									bgcolortext="Ntexto";
								} %>
								 
								 <td   class="<%=color %>"  rowspan="<%=cpcrit %>" ><bean:write name = "lrlist" property="criticalityname" />
								 <br><br><bean:write name = "lrlist" property="criticalitydesc" />
								</td>
								<%colorflag++; %>
							<%} %>
							<td   class="<%=bgcolortext %>" >
							<bean:write name = "lrlist" property="servicetime" /><br><bean:write name = "lrlist" property="categoryshortname" /></td>
						
							
							<%for(int a=0;a<(totalresources);a++)
							{
							%> 
							<html:hidden name = "lrlist" property="<%=detailid+a%>" />
							<html:hidden name = "lrlist" property="<%=resourceid+a %>" />
							<html:hidden name = "lrlist" property="<%=selectresourceid+a %>" />
							
							<td  class="<%=bgcolortext %>" width="50">
						    	<html:text styleClass="<%=bgcolortext1 %>" name = "lrlist" property = "<%= clrrate+a %>" size = "8"   readonly="true"/><br>
						    							
							
						    <%} %>
							
							    			
						  </tr>
						
						  <%j++; %>
						</logic:iterate>
						
					
					 </logic:present>
				</table>
		   </td>
   	</tr> 	
			
<%} %>			
<!-- for link show discounted rates end-->
			
		 
	 		  <jsp:include page = '/Footer.jsp'>
  	 			<jsp:param name = 'colspan' value = '28'/>
   				<jsp:param name = 'helpid' value = 'roleprivilege'/>
  			  </jsp:include>
	  		  
		
</table>

 

</table>
  </logic:greaterThan>
</logic:present>
 </html:form>
 
<logic:present  name="listsize" scope="request">
 <logic:equal  name="listsize" value="0">
 	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="toprow"> 
      <table width="100%" border="0" cellspacing=1" cellpadding="0" height="18">
        <tr align="left"> 
          <td class="toprow1" width=100><a href="javascript:history.go(-1);" onmouseout="MM_swapImgRestore(); " onmouseover="MM_swapImage('Image23','','images/sales1b.gif',1);" class="drop"><bean:message bundle="AM" key="am.back"/></a>
          <td  width=700 class="toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
      </table>
    </td>
  </tr>
</table>

<table  border="0" cellspacing="1" cellpadding="1">
	<tr>
	  <td  width="2" height="0"></td>
	  <td><table border="0" cellspacing="1" cellpadding="1" >
	  		<tr>
				<td  colspan="2" class="message" height="30" ><bean:message bundle="AM" key="am.nofieldlabors"/></td>
		 	</tr>
		  </table>
	  </td>
	</tr>
</table>
 </logic:equal>
</logic:present>
</body>

<script>

function validate(tresources,tcategories)
{
	//alert("totalresources "+tresources);
	//alert("totalcategories "+tcategories);
	document.forms[0].refclr.value="add";
	trimFields();
	
	
	if(isBlank(document.forms[0].minimumfee.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].minimumfee,"<bean:message bundle="AM" key="am.clr.minimumfee"/>",errormsg);
		
		return false;
	}
	
	if(!isFloat(document.forms[0].minimumfee.value))
	{
		errormsg="<bean:message bundle="RM" key="rm.warnmsgint"/>";
		
		warningEmpty(document.forms[0].minimumfee,"<bean:message bundle="AM" key="am.clr.minimumfee"/>",errormsg);
		
		return false;
	}
	
	if(isBlank(document.forms[0].travel.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].travel,"<bean:message bundle="AM" key="am.clr.travel"/>",errormsg);
		
		return false;
	}
	
	//Start :Added By Amit
	if(document.forms[0].travel.value!=null)
	{

	    obj=document.forms[0].travel.value;
	    object =  obj.split("");
 		for(i=0;i<object.length;++i)
  		{
		 	if(object[i]=='\\' || object[i]=='<' || object[i]=='>' || object[i]=='#' ||  object[i]=='"' || object[i]==';' || object[i]=='=' || object[i]=='&' || object[i]=='@'|| object[i]=='^' || object[i]=='*' || object[i]=='!'|| object[i]=='?'|| object[i]=='$' ||object[i]=='+' ||object[i]=='-'||object[i]=='_'||object[i]==':'||object[i]=='%'||object[i]=='~'||object[i]=='`'||object[i]=='('||object[i]==')'|| object[i]=='{' || object[i]=='}' || object[i]=='[' || object[i]==']' || object[i]==',')
		 	{
		 		errormsg="<bean:message bundle="RM" key="rm.warnmsgAlphaNumeric"/>";

		    	warningEmpty(document.forms[0].travel,"<bean:message bundle="AM" key="am.clr.travel"/>",errormsg);
		       	return false;
		 	}
  		}

	}
	
	//if(!isAlphanumeric(document.forms[0].travel.value))
	//{
		
	//	errormsg="<bean:message bundle="RM" key="rm.warnmsgAlphaNumeric"/>";
		
	//	warningEmpty(document.forms[0].travel,"<bean:message bundle="AM" key="am.clr.travel"/>",errormsg);
		
	//	return false;
	//}
	// End :Added By Amit
	if(isBlank(document.forms[0].travelmax.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].travelmax,"<bean:message bundle="AM" key="am.clr.travelmax"/>",errormsg);
		
		return false;
	}
	
	if(!isFloat(document.forms[0].travelmax.value))
	{
		errormsg="<bean:message bundle="RM" key="rm.warnmsgint"/>";
		
		warningEmpty(document.forms[0].travelmax,"<bean:message bundle="AM" key="am.clr.travelmax"/>",errormsg);
		
		return false;
	}
	if(isBlank(document.forms[0].discount.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].discount,"<bean:message bundle="AM" key="am.clr.volumediscount"/>",errormsg);
		
		document.forms[0].discount.focus();
		
		return false;
		
	}
	
	if(!isFloat(document.forms[0].discount.value))
	{
		//errormsg="<bean:message bundle="RM" key="rm.warnmsgint"/>";
		
		//warningEmpty(document.forms[0].discount,"<bean:message bundle="AM" key="am.clr.volumediscount"/>",errormsg);
		
		//return false;
	}
	
	if(isBlank(document.forms[0].effectivedate.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		//warningEmpty(document.forms[0].discount,"<bean:message bundle="AM" key="am.clr.effdate"/>",errormsg);
		alert(errormsg);
		return false;
	}
	
	
	if(isBlank(document.forms[0].rules.value))
	{
	
		errormsg="<bean:message bundle="RM" key="rm.warnmsg"/>";
		
		warningEmpty(document.forms[0].rules,"<bean:message bundle="AM" key="am.clr.rules"/>",errormsg);
		
		return false;
	}
	
	
	for(var j=0;j<tcategories;j++)
	{
		for(var i=0;i<tresources;i++)
		{
			var str=i+"["+j+"]";
			
			if(eval('document.forms[0].resourceid'+str).value==0)
			{
				alert("<bean:message bundle="AM" key="am.clr.alllaborratesnotmapped"/>");
				//eval('document.forms[0].resourceid'+str).focus();
				return false;
			}
		}
	}
	
	var str;
	var str1;
	for(var j=0;j<tcategories;j++)
	{
		for(var k=0;k<tresources;k++)
		{
		for(var i=0;i<tresources;i++)
		{
			if(i!=k)
			{
				 str=i+"["+j+"]";
				 str1=k+"["+j+"]";
				
				if(eval('document.forms[0].resourceid'+str1).value==eval('document.forms[0].resourceid'+str).value)
				{
					
					alert("<bean:message bundle="AM" key="am.clr.alllaborratesinsamecat"/>");
					//eval('document.forms[0].resourcename'+str1).className = 'textonmouseover';
					//eval('document.forms[0].resourceid'+str1).focus();
					return false;
				}
			}
		}
		}
	}
	return true;
	

}
</script>

<script>
function trimFields()
{

	var field=document.forms[0];

	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text')
		{
			var temp1=field[i].value;

			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--){
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		field[i].value=temp2;
		}
	}
return true;
}

</script>
<!-- FUNCTION TRIM END -->

<script>


function opensearchwindow(offset1,offset2)
{
	
	offset2=offset2-1;
	
	str = "OpenSearchWindowAction.do?type=Labor&from=am&clr=yes&activity_Id=<bean:write name = "CustomerLaborRatesForm" property = "activitylibrary_Id" />&offset1="+offset1+"&offset2="+offset2;

	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600, height = 400 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();

}

function CheckQuotes (obj,label)
{
	if(invalidChar(obj.value,label))
	{
		
		obj.focus();
		return false;
	}
	return true;
}

function deleteres()
{
 document.forms[0].refclr.value="delete";	
 document.forms[0].action="CustomerLaborRates.do?type="+document.forms[0].type.value+"&Activitylibrary_Id="+document.forms[0].activitylibrary_Id.value+"&MSA_Id="+document.forms[0].msa_Id.value+"&refclr=delete";
  document.forms[0].submit();
 //return true;
}


//for discount rates link
function calldiscount()
{
	document.forms[0].refclr.value="viewDiscountRates";
	document.forms[0].submit();
	return true;
}


</script>
</html:html>
