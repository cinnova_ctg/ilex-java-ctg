<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For creating , updating and deleting category.
*
-->


 
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>


<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.detail.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

</head>

<script>

function update() 
{
	if( document.forms[0].category_id.value == "0" )
	{
		alert( "<bean:message bundle = "AM" key = "am.category.update.select"/>" );
		return false;
	}
	document.forms[0].action = "ManageCategoryAction.do?ref=Update";
	document.forms[0].submit();
	return true;	
}		


function add() 
{
	document.forms[0].action = "ManageCategoryAction.do?ref=View&add=add";
	
	document.forms[0].submit();
	
	return true;	
		
}



function del() 
{
	if( document.forms[0].category_id.value == "0" )
	{
		alert( "<bean:message bundle = "AM" key = "am.category.update.select"/>" );
		return;
	}
	else
	{
		var convel = confirm( "<bean:message bundle = "AM" key = "am.category.delete.prompt"/>" );
		if( convel )
		{
			document.forms[0].action = "ManageCategoryAction.do?ref=Delete";
			document.forms[0].submit();
			return true;	
		}
	}
	return;
}

</script>


<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')">

	<div id="menunav">
	    <ul>
	  		<li><a href="javascript:add();" class="drop">Add Category</a></li>
	  		<li><a href="javascript:del();" class="drop">Delete Category</a></li>
		</ul>
	</div>

<table>
<html:form action = "/ManageCategoryAction">

	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
		<tr>
  			 <td>
 				<img src = "images/spacer.gif" width = "2" height = "0">
 			</td>
  			 <td>
  			 	<table border = "0" cellspacing = "1" cellpadding = "1" width = "500"> 
					
							<logic:present name = "addflag" scope = "request">
							<tr>
							<td colspan = "2" class = "message" height = "30">
					    		<logic:equal name = "addflag" value = "0">
					    			<bean:message bundle = "AM" key = "am.category.add.success"/>
					    		</logic:equal>
					    
						    	<logic:equal name = "addflag" value = "-9001">
						    		<bean:message bundle = "AM" key = "am.category.add.failure1"/>
						    	</logic:equal>
						    	
						    	<logic:equal name = "addflag" value = "-9006">
						    		<bean:message bundle = "AM" key = "am.category.add.failure2"/>
						    	</logic:equal>
						    	</td>
						    	</tr>
						    </logic:present>
						    
						    <logic:present name = "updateflag" scope = "request">
						    <tr>
							<td colspan = "2" class = "message" height = "30">
					    		<logic:equal name = "updateflag" value = "0">
					    			<bean:message bundle = "AM" key = "am.category.update.success"/>
					    		</logic:equal>
					    
						    	<logic:equal name = "updateflag" value = "-9002">
						    		<bean:message bundle = "AM" key = "am.category.update.failure1"/>
						    	</logic:equal>
						    	
						    	<logic:equal name = "updateflag" value = "-9006">
						    		<bean:message bundle = "AM" key = "am.category.update.failure2"/>
						    	</logic:equal>
						    	</td>
						    	</tr>
						    </logic:present>
						    
						    <logic:present name = "deleteflag" scope = "request">
						    	<tr>
									<td colspan = "2" class = "message" height = "30">
					    		<logic:equal name = "deleteflag" value = "0">
					    			<bean:message bundle = "AM" key = "am.category.delete.success"/>
					    				<script>
											parent.ilexleft.location.reload();
										</script>
					    		</logic:equal>
					    
						    	<logic:equal name = "deleteflag" value = "-9002">
						    		<bean:message bundle = "AM" key = "am.category.delete.failure1"/>
						    	</logic:equal>
						    	</td>
						    	</tr>
						    </logic:present>
						</td>
					</tr>
					
					
					
					<tr>    
						<td colspan = "2" class = "labeleboldwhite" height = "30" ><bean:message bundle = "AM" key = "am.category.label"/> </td>
					</tr>
					
					<tr>
  						<td colspan = "2"><img src = "images/spacer.gif" width = "1" height = "1"></td>
  					</tr>
  					<html:hidden property = "ref" />
  					 					
  					
  					<logic:present name = "category_list" scope = "request"> 
						<tr> 
	    					<td class = "labelobold" height = "20"><bean:message bundle = "AM" key = "am.category.select"/></td>
	    					<td class = "labelo">
	    						<html:select name = "ManageCategoryForm" property = "category_id" size = "1" styleClass = "comboo" onchange = "return refresh( this );">
									<html:optionsCollection name = "ManageCategoryForm"  property = "existingcategory"  label = "label"  value = "value" />
								</html:select>	
							</td>	
	  					</tr>
	  					
	  					<tr>
	  						<td class = "labelebold" height = "20"><bean:message bundle = "AM" key = "am.category.name"/></td>
	    					<td class = "labele"><html:text styleClass = "textbox" name = "ManageCategoryForm" property = "category_name" /></td>
  						</tr>
  						
  						
  						<tr>
						  	<td class = "buttonrow" colspan = "2">
						  		<html:submit property = "save" styleClass = "button" onclick = "return validate();"><bean:message bundle = "AM" key = "am.category.add"/></html:submit>
								<html:reset property = "cancel" styleClass = "button"><bean:message bundle = "AM" key = "am.category.cancel"/></html:reset>
							</td>
						</tr>
  					</logic:present>
  					 <tr> 
				    	<td colspan = "2"></td>
				        <td colspan = "2"></td>
				    </tr>

					<jsp:include page = '/Footer.jsp'>
				      <jsp:param name = 'colspan' value = '37'/>
				      <jsp:param name = 'helpid' value = 'amcategory'/>
		   			</jsp:include> 
				</table>
			</td>
		</tr>
	</table>
	<table>
<tr>
	<td ><html:text name = "ManageCategoryForm" styleClass="textboxdummy" size="1"
readonly="true" property="textboxdummy" /></td>
</tr>
</table>

					
</html:form>
</table>
</td></tr>
</table>
</body>

<script>
function trimFields() {
	var field=document.forms[0];
	for(var i=0; i<field.length; i++) {
		if(field[i].type=='text') {
			var temp1=field[i].value;
			var temp2="";
			var flag=0;
			if(temp1.indexOf(" ",temp1.length-1)>=0) {
				for(var j=temp1.length; j>0; j--) {
				if((temp1.substring(j-1,j)==" ")&&(flag==0)){
					temp2=temp1.substring(0,j-1);
				} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }

		temp1=temp2;
		temp2="";
		flag=0;
		if(temp1.indexOf(" ")==0) {
			for(var j=0; j<temp1.length; j++){
			if((temp1.substring(j,j+1)==" ")&&(flag==0)){
			temp2=temp1.substring(j+1,temp1.length);
			} else {flag=1; break;}
			}//for
		} else { temp2=temp1; }
		field[i].value=temp2;
		}
	}
return true;
}


function refresh( temp )
{
	
	var tempvariable = temp.value.substring( temp.value.indexOf( "!" ) + 1 , temp.value.length + 1 );
	if(tempvariable!=0)
	{
		document.forms[0].category_name.value = tempvariable;
	}
	else
	{
		document.forms[0].category_name.value="";
	}
	
	document.forms[0].submit();
}

function validate()
{
	trimFields();
	if( document.forms[0].category_name.value == "" )
	{
		alert(  "<bean:message bundle = "AM" key = "am.category.entercategoryname"/>" );
		document.forms[0].category_name.focus();
		return false;
	}
	document.forms[0].ref.value = "Submit";
}


</script>



</html:html>
  					
 	







