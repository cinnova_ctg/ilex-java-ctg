<!DOCTYPE HTML>

<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying details of labor resource of an activity.
*
-->


<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<% 
	String MSA_Id = "";
	if( request.getAttribute( "MSA_Id" ) != null )
		MSA_Id = ( String ) request.getAttribute( "MSA_Id" );
	
%>

<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.labor.detail.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href = "docm/styles/style.css" type = "text/css">
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>

<%
	String backgroundclass = "texto";
	String backgroundhyperlinkclass = null;
	int i = 0;
	boolean csschooser = true;
	String readonlynumberclass = null;
%>



<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')" bgcolor="#FFFFFF">
	<div id="menunav">
	    <ul>
	  		<li>
	        	<a href="#" class="drop"><span>Activity</span></a>
	  			<ul>
	  				<li><a href="ManageActivityAction.do?MSA_Id=<%= MSA_Id %>&ref=addActivity">Add</a></li>
	 				<li><a href="ManageActivityAction.do?MSA_Id=<%= MSA_Id %>&ref=manageActivity">Manage</a></li>
	 				<li><a href="ActivitySummaryAction.do?ref=View&MSA_Id=<%=MSA_Id %>">View</a></li>
	 				<%
	 				if(request.getAttribute("checkformastercopy") != null && request.getAttribute("checkformastercopy").equals("N")) {
	 					%>
	 					<li><a href="MastertolibcopyAction.do?ref=showAllMSA&MSA_Id=<%= MSA_Id %>">Copy</a></li>
	 					<%
 					}
					%>
	  			</ul>
	  		</li>
	  		<li>
	        	<a href="#" class="drop"><span><bean:message bundle = "AM" key = "am.summary.Partnerinfo"/></span></a>
	  			<ul>
	  				<li><a href="PartnerSOWAssumptionList.do?fromId=<%= MSA_Id %>&viewType=sow&fromType=am_act">Manage SOW</a></li>
	 				<li><a href="PartnerSOWAssumptionList.do?fromId=<%= MSA_Id %>&viewType=assumption&fromType=am_act">Manage Assumptions</a></li>
	  			</ul>
	  		</li>
		</ul>
	</div>


<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	<tr>
    	<td class = "toprow"> 
	    	<table  border = "0" cellspacing = "0" cellpadding = "0" height = "18">
		       <tr><td background="images/content_head_04.jpg" height="21" colspan="3">
			         <div id="breadCrumb">
			         <a href="#" class="bgNone">Activity Manager</a>
			         <a href="ActivitySummaryAction.do?ref=View&MSA_Id=<%=MSA_Id %>" background="none;"><bean:write name = "msaname" scope = "request"/></a>
					 <a><span class="breadCrumb1">Activity Summary</span></a>
			    	</td>
				</tr>
		        <tr><td height="2">&nbsp;</td></tr>	
	      	</table>
    	</td>
	</tr>
</table>
<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
	<tr><td width="2" height="0"></td>
	 
	  	<td>
	  		<table border = "0" cellspacing = "1" cellpadding = "1">
		  		<logic:present name = "msaactivitylist" scope = "request"> 
				<% String tempCategory = ""; %>
				<% 
			  		String previousCategory = "";
			  		int divCheck=0;
		  		%>
		
 			<logic:iterate id = "ms" name = "msaactivitylist">
 					
 					
 		<bean:define id = "currentCategory" name = "ms" property = "category_type" type = "java.lang.String"/>
		<% if(!previousCategory.equals(currentCategory)) { 
			previousCategory = currentCategory;
			csschooser = false;
			if(i != 0) { %>
				</table></td></tr>
			</table>		
		<%	}
		%>
		<tr>
		<td valign="top"><a href="javascript:toggleMenuSection('<%=currentCategory %>');"><img id='img_<%=currentCategory %>' src="images/Expand.gif" border="0" alt="+"  /></a></td>
		<td colspan=13><table border="0" cellspacing="0" cellpadding="0" width="770">
		<tr>
    		<td class = "NHeadingFont" colspan = "14" style="text-align: left;" >
    			<B><bean:write name = "ms" property = "category_type" /></B>  
    		</td>
    	</tr>
    	<tr>
    		<td colspan = "14" height = "1">
    		</td>
		</tr>
		<tr>
		<td colspan=14>
		<table width="100%" id = 'div_<%=currentCategory %>'  style="display: none" border="0" cellspacing="1" cellpadding="0">
    	
		<tr> 
			<td class = "Nhyperoddfont" align="center">
			<B><bean:message bundle = "AM" key = "am.summary.activity"/></B>
			</td>
			<td class = "NhyperoddfontActivity" align="center" >
				<B><bean:message bundle = "AM" key = "am.summary.totalcost"/></B>
			</td>
			 <td  class = "NhyperoddfontActivity" nowrap="nowrap" align="center">
		    	<B>Extended Price</B>
		    </td>
		    <td class = "NhyperoddfontActivity" nowrap="nowrap" align="center">
		    	<B>Pro Forma Margin</B>
		    </td>
		    <td class = "Nhyperoddfont" align="center">
		    	<B>Status</B>
		    </td>
		</tr>
 					
		
  		<%} %>			
 				
 		<% if(previousCategory.equals(currentCategory)) { 			%>			
 					
 					<%	
						
						if ( csschooser == true ) 
						{
							backgroundclass = "texto";
							csschooser = false;
							readonlynumberclass = "readonlytextnumberodd";	
							backgroundhyperlinkclass = "hyperodd";
						}
				
						else
						{
							csschooser = true;	
							backgroundclass = "texte";
							readonlynumberclass = "readonlytextnumbereven";
							backgroundhyperlinkclass = "hypereven";
						}
					%>
				<tr>
					
					<td class = "<%= backgroundhyperlinkclass %>" style="text-align: left;padding-left:3px;">
						<bean:write name = "ms" property = "name" />
					</td>
					
					<td class = "<%= backgroundhyperlinkclass %>" style="text-align: right;padding-right:3px;">
					<bean:write name = "ms" property = "estimatedtotalcost" />
					</td>
					<td class = "<%= backgroundhyperlinkclass %>" style="text-align: right;padding-right:3px;">
					<bean:write name = "ms" property = "extendedprice" />
					</td>
					
					<td class = "<%= backgroundhyperlinkclass %>" style="text-align: right;padding-right:3px;">
					<bean:write name = "ms" property = "proformamargin" />
					</td>
					<td class = "<%= backgroundhyperlinkclass %>" style="text-align: center;">
						<bean:write name = "ms" property = "status" />
					</td>
					
				</tr>
				<%} %>
				 <% i++;  %>
			</logic:iterate>
		</logic:present>
		<logic:notPresent name = "msaactivitylist" scope = "request">
				<tr>
				    <td class = "message" height = "30"> 
				    	<bean:message bundle = "AM" key = "am.summary.noactivity"/>
				    </td>  	
      			</tr>
		</logic:notPresent> 
				
				<jsp:include page = '/Footer.jsp'>
				      <jsp:param name = 'colspan' value = '37'/>
				      <jsp:param name = 'helpid' value = 'amsummary'/>
		   		</jsp:include> 
		
			</table>
			
		</td>
	</tr>
</table>
 <script>
 function toggleMenuSection(unique) {
		var divid="div_"+unique;
		thisDiv = document.getElementById(divid);
		try{
		if (thisDiv==null) { return ; }
		}catch(e){
		}
		action = "toggleType = toggleDiv('div_" + unique + "');";
		eval(action);
		action = "thisImage = document.getElementById('img_" + unique + "');";
		eval(action);
		if (document.getElementById('div_' + unique).offsetHeight > 0) {
			thisImage.src = "images/Collapse.gif";
		}
		else {
			thisImage.src = "images/Expand.gif";
		}
	}
	
	function toggleDiv(divId) {
		var div  = document.getElementById(divId);
		var tableObj = document.getElementById(divId);
		if (div.offsetHeight > 0) {
			div.style.display = "none";
		} else {
			div.style.display = "block";
		}
	}
	</script>
 
 

</body>
</html:html>
  					







