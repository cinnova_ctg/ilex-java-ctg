<!DOCTYPE HTML>
<%@ page import = "com.mind.common.*" %>
<%@ page import="com.mind.common.dao.*" %>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<bean:define id = "menustring" name = "menustring" scope = "request"/>
<bean:define id = "currState" name = "currState" scope = "request"/>
<bean:define id = "expansionState" name = "expansionState" scope = "request"/>

<bean:define id = "element" name = "element" scope = "request"/>
<bean:define id = "controller" name = "controller" scope = "request"/>
<html:html>


<head>
<%@ include file = "../Header.inc" %>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

<title>Hierarchy Navigation</title>
<style>
    body  {
    	font-family : "Verdana";
    	font-size : 10px;
    	cursor : auto;
    	color : #000000;
    	padding : 0px 0px 0px 0px;
    	white-space : nowrap;
    }

    /* Link Styles */
    a  {
    	color : #000000;
    }

    a:link  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:active  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:hover  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:visited  {
    	text-decoration : underline;
    	color : #000000;
    }
    
    .m2 {
        font-family : "Verdana";
        font-size : 10px;
        line-height: 10px;
    }    
</style>

<script language = "JavaScript" src = "javascript/ce_menu.js" type = "text/javascript"></script>

</head>

<body>
<%
MenuElement me = ((MenuTree)(element)).getRootElement();
MenuController mc =((MenuTree)(element)).getMenuController();
me.drawDirect(0,mc,response.getWriter());
%>
<script language="JavaScript">

document.getElementById('line0').className = 'm2';
document.getElementById('line0').style.visibility = 'visible';

currState="<%=((MenuTree)(element)).getCurrState()%>";
expansionState="<%=expansionState%>";
initNavigatorServer('AM');
</script>
<%
if(request.getParameter("filter")==null)
{
%>
 <div style="height:15px;">&nbsp;</div>
		    <div style="padding-left:12px;"><a href='http://192.168.32.103/sales/index.html' id="activityLibSearchDiv" target="ilexmain"><font style="color:#20477d;font-size:10px; font-weight: bold; vertical-align: top;">Activity Library Search</font></a></div>
<%
} 
else{
if(request.getParameter("filter").equals("Master"))
{
%>
 <div style="height:15px;">&nbsp;</div>
		    <div style="padding-left:12px;"><a href='http://192.168.32.103/sales/index.html' id="activityLibSearchDiv" target="ilexmain"><font style="color:#20477d;font-size:10px; font-weight: bold; vertical-align: top;">Activity Library Search</font></a></div>
<%
} 
}
%>
</body>
</html:html>