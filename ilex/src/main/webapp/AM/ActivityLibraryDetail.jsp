<!DOCTYPE HTML>

<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying activity details.
*
-->
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<% int sow_size =( int ) Integer.parseInt( request.getAttribute( "sowsize" ).toString()); 
	int assumption_size =( int ) Integer.parseInt( request.getAttribute( "assumptionsize" ).toString()); 
%> 

<%
	String Activitylibrary_Id = "";
	String MSA_Id = "";

	if( request.getParameter( "Activitylibrary_Id" ) != null )
		Activitylibrary_Id = request.getParameter( "Activitylibrary_Id" );

	else if( request.getAttribute( "Activitylibrary_Id" ) != null )
		Activitylibrary_Id = ( String ) request.getAttribute( "Activitylibrary_Id" );

	if( request.getParameter( "MSA_Id" ) != null )
		MSA_Id = request.getParameter( "MSA_Id" );

	else if( request.getAttribute( "MSA_Id" ) != null )
		MSA_Id = ( String ) request.getAttribute( "MSA_Id" );

	String backgroundclass = "texto";
	String backgroundhyperlinkclass = "hyperodd";
	int i = 0;
	boolean csschooser = true;
	String readonlynumberclass = null;
%>


<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.detail.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href = "docm/styles/style.css" type = "text/css">
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>


<script>

function del() 
{
		var convel = confirm( "<bean:message bundle = "AM" key = "am.detail.delete.confirm"/>" );
		if( convel )
		{
			document.forms[0].action = "DeleteAction.do?Type=am&Activitylibrary_Id=<%= Activitylibrary_Id%>&MSA_Id=<%= MSA_Id %>";
			document.forms[0].submit();
			return true;	
		}
}
</script>

<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages('images/sales1b.gif','images/services1b.gif','images/about1b.gif','images/cust-serv1b.gif','images/offers1b.gif')">
	<div id="menunav">
	    <ul>
	  		<li>
	        	<a href="#" class="drop"><span>Activity</span></a>
	  			<ul>
	  				<li>
			        	<%
			        	if( ( String )request.getAttribute( "list" ) != "") {
			        		%>
				        	<a href="#"><span><bean:message bundle = "AM" key = "am.detail.menu.changestatus"/></span></a>
				            <ul>
				            <%= ( String ) request.getAttribute( "list" ) %>
			      			</ul>
			        		<%
			        	} else {
			        		%>
			        		<a href="#"><bean:message bundle = "AM" key = "am.detail.menu.changestatus"/></a>
			        		<%
			        	}
			        	%>
		        	</li>
	 				<li><a href="javascript:del();"><bean:message bundle = "AM" key = "am.detail.menu.deleteactivity"/></a></li>
	  			</ul>
	  		</li>
	  		<li><a href="AMResourceLibraryListAction.do?ref=view&Activitylibrary_Id=<%=  Activitylibrary_Id %>&MSA_Id=<%= MSA_Id %>" class="drop">Resources</a></li>
		</ul>
	</div>

	<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
		<tr>
	    	<td class = "toprow"> 
		    	<table border = "0" cellspacing = "0" cellpadding = "0" height = "18">
			        <tr>
			        	<td background="images/content_head_04.jpg" height="21" colspan=3>
				         <div id="breadCrumb">
							 <a href="#" class="bgNone">Activity Manager</a>
				         	 <a href="ActivitySummaryAction.do?ref=View&MSA_Id=<%=MSA_Id %>" background="none;"><bean:write name = "ActivityLibraryDetailForm" property = "msaname"/></a>
				         	<!--  <a href="ActivitySummaryAction.do?ref=View&MSA_Id=<%=MSA_Id %>" background="none;">View Activities</a> -->
				         	<a href="ManageActivityAction.do?ref=View&type=AM&MSA_Id=<%=MSA_Id %>" background="none;">Manage Activities</a>
							 <a><span class="breadCrumb1"><bean:write name = "ActivityLibraryDetailForm" property = "activityname"/></span></a>
						</div>
				    	</td>
					</tr>
		      	</table>
	    	</td>
		</tr>
	</table>
	<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
	<html:form action = "/ActivityLibraryDetailAction">
		<tr>
			<td colspan=2 height="12"></td>
		</tr>
	    <logic:present name = "changestatusflag" scope = "request">	
	      <tr><td style="padding-left: 5px;"></td>
	      	 <td colspan = "5" class = "message" height = "30">
		     	<logic:equal name = "changestatusflag" value = "0">
		     		Status changed successfully
		     	</logic:equal>
		    	
		     	<logic:notEqual name = "changestatusflag" value = "0">
			     	Change status failure
		     	</logic:notEqual>
			    </td>
		    </tr>
	     </logic:present>
		
		<tr>
		<td width="5" height="0"></td>
		<td>
			<table border = "0" cellspacing = "1" cellpadding = "0" width = "800"> 
				<tr>    
					<td colspan=9 style="padding-left: 0px; padding-top:0px;	font-size: 11px;font-weight: bold;color: #2e3d80;">
						General
					</td>
				</tr>
				<tr>
					<td class = "labelobold" height = "20" width = 10%><bean:message bundle = "AM" key = "am.detail.name"/></td>
					<td class = "labelo" colspan = "7" width = 52%>&nbsp;<bean:write name = "ActivityLibraryDetailForm" property = "activityname"/></td>
					<td width = 38%></td>
				</tr>
						
				<tr>
					<td class = "labelebold" height = "20" width = 10%><bean:message bundle = "AM" key = "am.detail.status"/></td>
					<td class = "labele" colspan = "7" width = 52%>&nbsp;<bean:write name = "ActivityLibraryDetailForm" property = "status"/></td>
					
				</tr>
			
		 	   <tr>
				<td class = "Nhyperoddfont" colspan = "6" align="center">
					<bean:message bundle = "AM" key = "am.tabular.estimatedcost"/>
				</td>
				
				<td class = "Nhyperoddfont" rowspan = "2" align="center">
			    	<bean:message bundle = "AM" key = "am.libdetail.proformamargin"/>
			    </td>
				
				<td class = "Nhyperoddfont" rowspan = "2" align="center">
					<bean:message bundle = "AM" key = "am.tabular.extendedprice"/>
				</td>
			</tr>
			
			<tr> 
	    		<td class = "Nhyperoddfont"> 
	      			<div align = "center">
	      				<bean:message bundle = "AM" key = "am.tabular.estimatedcnslabourcost"/>
	      			</div>
	    		</td>
	
	    		<td class = "Nhyperoddfont"> 
	      			<div align = "center">
	      				<bean:message bundle = "AM" key = "am.tabular.estimatedcontractlabourcost"/>
	      			</div>
	    		</td>
	    		
	    		<td class = "Nhyperoddfont"> 
	      			<div align = "center">
	      				<bean:message bundle = "AM" key = "am.tabular.estimatedmaterialcost"/>
	      			</div>
	    		</td>
	    
			    <td class = "Nhyperoddfont"> 
			      <div align = "center">
			      	<bean:message bundle = "AM" key = "am.tabular.estimatedfreightcost"/>
			      </div>
			    </td>
	
			    <td class = "Nhyperoddfont"> 
			      <div align = "center">
			      	<bean:message bundle = "AM" key = "am.tabular.estimatedtravelcost"/>
			      </div>
			    </td>
	    
			    <td class = "Nhyperoddfont" > 
			      <div align = "center">
			      	<bean:message bundle = "AM" key = "am.tabular.estimatedtotalcost"/>
			      </div>
			    </td>
			</tr>
			
			<tr>
				<td class = "readonlytextnumberodd">
					<bean:write name = "ActivityLibraryDetailForm" property = "estimatedcnslabourcost"/>
				</td>
				
				<td class = "readonlytextnumberodd">
					<bean:write name = "ActivityLibraryDetailForm" property = "estimatedcontractlabourcost"/>
				</td>
				
				<td class = "readonlytextnumberodd">
					<bean:write name = "ActivityLibraryDetailForm" property = "estimatedmaterialcost"/>
				</td>
				
				<td class = "readonlytextnumberodd">
					<bean:write name = "ActivityLibraryDetailForm" property = "estimatedfreightcost"/>
				</td>
				
				<td class = "readonlytextnumberodd">
					<bean:write name = "ActivityLibraryDetailForm" property = "estimatedtravelcost"/>
				</td>
				
				<td class = "readonlytextnumberodd">
					<bean:write name = "ActivityLibraryDetailForm" property = "estimatedtotalcost"/>
				</td>
				
				<td class = "readonlytextnumberodd" >
					<bean:write name = "ActivityLibraryDetailForm" property = "proformamargin"/>
				</td>
				
				<td class = "readonlytextnumberodd">
					<bean:write name = "ActivityLibraryDetailForm" property = "extendedprice"/>
				</td>
			</tr>
	    </table>
	    </td>
    </tr>
    <tr>
    <td width="5" height="0"></td>
    <td style="padding-top: 10px;">
    <table border = "0" cellspacing = "1" cellpadding = "0" width = "500"> 
		<tr>
			<td colspan = "8" class="dbvalue">
				<font style="padding-left: 0px;	font-size: 11px;font-weight: bold;color: #2e3d80;"><bean:message bundle = "AM" key = "am.detail.resources"/></font>
			</td>
		</tr>
    				
		<tr>
			<td class = "Nhyperoddfont" rowspan = "2" align="center">
				<bean:message bundle = "AM" key = "am.detail.type"/>
			</td>
			
			<td class = "Nhyperoddfont" rowspan = "2" align="center">
		    	<bean:message bundle = "AM" key = "am.detail.name"/>
		    </td>
			
			<td class = "Nhyperoddfont" rowspan = "2" align="center">
				<bean:message bundle = "AM" key = "am.detail.quantity"/>
			</td>
			
			<td class = "Nhyperoddfont" colspan = "2" align="center">
				<bean:message bundle = "AM" key = "am.detail.estimatedcost"/>
			</td>
			
			<td class = "Nhyperoddfont" rowspan = "2" align="center">
				<bean:message bundle = "AM" key = "am.libdetail.proformamargin"/>
			</td>
			
			<td class = "Nhyperoddfont" colspan = "2" align="center">
				<bean:message bundle = "AM" key = "am.detail.extendedprice"/>
			</td>
		</tr>
		    		
		    		
		<tr> 
			<td class = "Nhyperoddfont"> 
	  			<div align = "center">
	  				<bean:message bundle = "AM" key = "am.detail.estimatedunitcost"/>
	  			</div>
			</td>
	
			<td class = "Nhyperoddfont"> 
	  			<div align = "center">
	  				<bean:message bundle = "AM" key = "am.detail.estimatedtotalcost"/>
	  			</div>
			</td>


			<td class = "Nhyperoddfont"> 
	  			<div align = "center">
	  				<bean:message bundle = "AM" key = "am.detail.priceunit"/>
	  			</div>
			</td>
	
		    <td class = "Nhyperoddfont"> 
		      <div align = "center">
		      	<bean:message bundle = "AM" key = "am.detail.priceextended"/>
		      </div>
		    </td>
		</tr>
	
		
		<logic:present name = "materiallist" scope = "request">
			<tr>
				<td colspan = "8" class = "labelobold"><bean:message bundle = "AM" key = "am.detail.Material"/></td>
			</tr>
			<logic:iterate id = "ms" name = "materiallist">
			<%	
			
			if ( csschooser == true ) 
			{
				backgroundclass = "texto";
				csschooser = false;
				readonlynumberclass = "readonlytextnumberodd";	
				backgroundhyperlinkclass = "hyperodd";
			}
	
			else
			{
				csschooser = true;	
				backgroundclass = "texte";
				readonlynumberclass = "readonlytextnumbereven";
				backgroundhyperlinkclass = "hypereven";
			}
		%>
		
		<tr>
			<td>&nbsp;</td>
			
			<td class = "<%= backgroundhyperlinkclass %>">
				<bean:write name = "ms" property = "materialname"/>&nbsp;
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "quantity" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "estimatedunitcost" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "estimatedtotalcost" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "proformamargin" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "priceunit" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "priceextended" />
			</td>
		</tr>
				
			</logic:iterate>
		</logic:present>
		<%
			backgroundclass = "texto";
			csschooser = true;	
			readonlynumberclass = "readonlytextnumberodd";
			backgroundhyperlinkclass = "hyperodd";	
		 %>
		<logic:present name = "laborlist" scope = "request">
			<tr>
				<td colspan = "8" class = "labelobold"><bean:message bundle = "AM" key = "am.detail.Labor"/></td>
			</tr>
			<logic:iterate id = "ms" name = "laborlist">
			<%	
			
			if ( csschooser == true ) 
			{
				backgroundclass = "texto";
				csschooser = false;	
				readonlynumberclass = "readonlytextnumberodd";	
				backgroundhyperlinkclass = "hyperodd";	
			}
	
			else
			{
				csschooser = true;	
				backgroundclass = "texte";
				readonlynumberclass = "readonlytextnumbereven";	
				backgroundhyperlinkclass = "hypereven";	
			}
		%>
		
		<tr>
			<td>&nbsp;</td>
			
			<td class = "<%= backgroundhyperlinkclass %>">
				<bean:write name = "ms" property = "laborname"/>&nbsp;
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "quantityhours" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "estimatedhourlybasecost" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "estimatedtotalcost" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "proformamargin" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "priceunit" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "priceextended" />
			</td>
		</tr>
				
			</logic:iterate>
		</logic:present>
		
		<%
			backgroundclass = "texto";
			csschooser = true;	
			readonlynumberclass = "readonlytextnumberodd";	
			backgroundhyperlinkclass = "hyperodd";		
		 %>
		<logic:present name = "freightlist" scope = "request">
			<tr>
				<td colspan = "8" class = "labelobold"><bean:message bundle = "AM" key = "am.detail.Freight"/></td>
			</tr>
			<logic:iterate id = "ms" name = "freightlist">
			<%	
			
			if ( csschooser == true ) 
			{
				backgroundclass = "texto";
				csschooser = false;
				readonlynumberclass = "readonlytextnumberodd";	
				backgroundhyperlinkclass = "hyperodd";		
			}
	
			else
			{
				csschooser = true;	
				backgroundclass = "texte";
				readonlynumberclass = "readonlytextnumbereven";
				backgroundhyperlinkclass = "hypereven";		
			}
		%>
		
		<tr>
			<td>&nbsp;</td>
			
			<td class = "<%= backgroundhyperlinkclass %>">
				<bean:write name = "ms" property = "freightname"/>&nbsp;
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "quantity" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "estimatedunitcost" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "estimatedtotalcost" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "proformamargin" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "priceunit" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "priceextended" />
			</td>
		</tr>
				
			</logic:iterate>
		</logic:present>
		
		<%
			backgroundclass = "texto";
			csschooser = true;	
			readonlynumberclass = "readonlytextnumberodd";
			backgroundhyperlinkclass = "hyperodd";				
		 %>
		<logic:present name = "travellist" scope = "request">
			<tr>
				<td colspan = "8" class = "labelobold"><bean:message bundle = "AM" key = "am.detail.Travel"/></td>
			</tr>
			<logic:iterate id = "ms" name = "travellist">
			<%	
			
			if ( csschooser == true ) 
			{
				backgroundclass = "texto";
				csschooser = false;
				readonlynumberclass = "readonlytextnumberodd";	
				backgroundhyperlinkclass = "hyperodd";				
			}
	
			else
			{
				csschooser = true;	
				backgroundclass = "texte";
				readonlynumberclass = "readonlytextnumbereven";	
				backgroundhyperlinkclass = "hypereven";			
			}
		%>
		
		<tr>
			<td>&nbsp;</td>
			
			<td class = "<%= backgroundhyperlinkclass %>">
				<bean:write name = "ms" property = "travelname"/>&nbsp;
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "quantity" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "estimatedunitcost" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "estimatedtotalcost" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "proformamargin" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "priceunit" />
			</td>
			
			<td class = "<%= readonlynumberclass %>">
				<bean:write name = "ms" property = "priceextended" />
			</td>
		</tr>
				
			</logic:iterate>
		</logic:present>
		</table>
		</td>
		</tr>
		<tr>
		<td width="5" height="0"></td>
		<td style="padding-top: 12px;">
		<table border = "0" cellspacing = "1" cellpadding = "1" width = "500"> 
			<tr>
				<td colspan = "8" style="padding-left: 0px;	font-size: 11px;font-weight: bold;color: #2e3d80;">
					<bean:message bundle = "AM" key = "am.detail.sow"/>
				</td>
			</tr>
			
			<% int k = 1; %>
			
			<% if( ( sow_size > 0 ) ) {%>
			
			<tr>
				<td class = "labelotop" width = "50%" colspan = "8" style="padding-right: 3px;">	
					<logic:iterate id = "ms" name = "sowlist">	
						 <bean:write name = "ms" property = "sow" /> <br><br>
						<% k++; %>
					</logic:iterate>&nbsp;
				</td>
			</tr>
			<%} else { %>
			<tr>
				<td class = "labelotop" width = "50%" colspan = "8">&nbsp;	
				</td>
			</tr>
			<%} %>
			<tr>
				<td height="15"></td>
			</tr>
			<tr>
				<td colspan = "8" style="padding-left: 0px;	font-size: 11px;font-weight: bold;color: #2e3d80;">
					<bean:message bundle = "AM" key = "am.detail.assumption"/>
				</td>
			</tr>
			
			<%if(( assumption_size > 0 ) ) { %>
			<tr>
				<% k = 1; %>
				<td class = "labelotop" width = "50%" colspan = "8" style="padding-right: 3px;">	
					<logic:iterate id = "ms" name = "assumptionlist">	
						 <bean:write name = "ms" property = "assumption" /> <br><br>
						<% k++; %>
					</logic:iterate>&nbsp;
				</td>
			</tr>	
			<%} else { %>
			<tr>
				<td class = "labelotop" width = "50%" colspan = "8">&nbsp;	
				</td>
			</tr>
			<%} %>
				
		<jsp:include page = '/Footer.jsp'>
		      <jsp:param name = 'colspan' value = '37'/>
		      <jsp:param name = 'helpid' value = 'amdetailpage'/>
		 </jsp:include>				
		
		</table>
		</td>
		</tr>
		</html:form>
		</table>    
</body>
</html:html>
  					







