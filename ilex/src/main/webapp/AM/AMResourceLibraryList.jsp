<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	String Id = (String) request.getAttribute("Activity_Id");
	int material_Size = 0;
	int labor_Size = 0;
	int freight_Size = 0;
	int travel_Size = 0;
	String temp_str_labor = null;
	String temp_str_material = null;
	String temp_str_freight = null;
	String temp_str_travel = null;
	String refForJob = "";
	String checkDefault = "";

	String laborschdayscheck = null;
	String laborarrcal = null;
	String labortransarr = null;

	String materialschdayscheck = null;
	String materialarrcal = null;

	String travelschdayscheck = null;
	String travelarrcal = null;

	String schdayscheck = null;
	String arrcal = null;

	if (request.getAttribute("material_Size") != null) {
		material_Size = Integer.parseInt(request
				.getAttribute("material_Size") + "");
	}
	if (request.getAttribute("labor_Size") != null) {
		labor_Size = Integer.parseInt(request
				.getAttribute("labor_Size") + "");
	}
	if (request.getAttribute("freight_Size") != null) {
		freight_Size = Integer.parseInt(request
				.getAttribute("freight_Size") + "");
	}
	if (request.getAttribute("travel_Size") != null) {
		travel_Size = Integer.parseInt(request
				.getAttribute("travel_Size") + "");
	}
%>
<html:html>
<head>
<title><bean:message bundle="AM" key="am.material.title" /></title>

<script language="JavaScript" src="javascript/JLibrary.js"></script>
<script language="JavaScript" src="javascript/ilexGUI.js"></script>
<script>


</script>
<script language="JavaScript" src="javascript/popcalendar.js"></script>
<script language="JavaScript" src="javascript/UtilityFunction.js"></script>

<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link href="styles/content.css" rel="stylesheet" type="text/css" />
<link href="styles/summary.css" rel="stylesheet" type="text/css" />

<%@ include file="/MSAMenu.inc"%>
<%@ include file="/AppendixMenu.inc"%>
<%@ include file="/AddendumMenu.inc"%>
<%@ include file="/DefaultJobMenu.inc"%>
<%@ include file="/ProjectJobMenu.inc"%>


</head>
<%@ include file="/CommonResources.inc"%>

<%
	String backgroundclass = "texto";
		String readonlynumberclass = null;
		String backgroundhyperlinkclass = null;
		int i = 0;
		boolean csschoosermaterial = true;
		boolean csschooserlabor = true;
		boolean csschooserfreight = true;
		boolean csschoosertravel = true;
		int j = 0;
		int k = 0;
		int l = 0;
		int colspan = 9;
		String from = "";
		String jobid = "";

		if (request.getAttribute("from") != null) {
			from = request.getAttribute("from").toString();
			if (from.equals("pm"))
				from = "";
			if (request.getAttribute("jobid") != null
					&& !((String) request.getAttribute("jobid")).trim()
							.equalsIgnoreCase(""))
				jobid = request.getAttribute("jobid").toString();
			else
				jobid = null;
			//request.setAttribute("from",from);
		}
%>

<body onload="leftAdjLayers();">
	<html:form action="/AMResourceLibraryListAction" method="post">
		<html:hidden name="ResourceListForm" property="fromflag" />
		<html:hidden property="checknetmedx" />
		<html:hidden property="activity_Id" />
		<html:hidden property="jobType" />
		<html:hidden property="jobStatus" />
		<html:hidden property="edit" />

		<html:hidden name="ResourceListForm" property="resourceListType" />
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td valign="top" width="100%">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td background="images/content_head_04.jpg" height="21">
								<div id="breadCrumb">
									<a href="#" class="bgNone">Activity Manager</a> <a
										href="ActivitySummaryAction.do?ref=View&MSA_Id=<bean:write name = "ResourceListForm" property = "msaId"/>"
										background="none;"><bean:write name="ResourceListForm"
											property="msaName" /></a> <a
										href="ManageActivityAction.do?ref=View&type=AM&MSA_Id=<bean:write name = "ResourceListForm" property = "msaId"/>"
										background="none;">View Activities</a> <a
										href="ActivityLibraryDetailAction.do?ref=View&type=AM&MSA_Id=<bean:write name = "ResourceListForm" property = "msaId"/>&Activitylibrary_Id=<bean:write name="ResourceListForm" property="activity_Id"/>"
										background="none;"><bean:write name="ResourceListForm"
											property="activityName"></bean:write></a> <a><span
										class="breadCrumb1">Resources</span></a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="20" height="0">&nbsp;</td>
			<tr>
				<!-- <td colspan="5"><h2><a class="head" href="ActivityDetailAction.do?ref=<%=refForActivity%>&Activity_Id=<bean:write name = "ResourceListForm" property = "activity_Id" />"><bean:write name = "ResourceListForm" property = "activityName"/></a><bean:message bundle = "pm" key = "resource.tabular.new.label"/></h2></td> -->
			</tr>
		</table>
		<table border="0" cellspacing="1" cellpadding="1">
			<logic:present name="sowaddflag" scope="request">
				<logic:equal name="sowaddflag" value="0">
					<tr>
						<td>&nbsp;</td>
						<td colspan="5" class="message" height="30"><bean:message
								bundle="pm" key="common.sow.add.success" /></td>
					</tr>
				</logic:equal>

				<logic:notEqual name="sowaddflag" value="0">
					<tr>
						<td>&nbsp;</td>
						<td colspan="5" class="message" height="30"><bean:message
								bundle="pm" key="common.sow.add.failure" /></td>
					</tr>
				</logic:notEqual>
			</logic:present>

			<logic:present name="sowupdateflag" scope="request">
				<logic:equal name="sowupdateflag" value="0">
					<tr>
						<td>&nbsp;</td>
						<td colspan="5" class="message" height="30"><bean:message
								bundle="pm" key="common.sow.update.success" /></td>
					</tr>
				</logic:equal>

				<logic:notEqual name="sowupdateflag" value="0">
					<tr>
						<td>&nbsp;</td>
						<td colspan="5" class="message" height="30"><bean:message
								bundle="pm" key="common.sow.update.failure" /></td>
					</tr>
				</logic:notEqual>
			</logic:present>

			<logic:present name="assumptionaddflag" scope="request">
				<logic:equal name="assumptionaddflag" value="0">
					<tr>
						<td>&nbsp;</td>
						<td colspan="5" class="message" height="30"><bean:message
								bundle="pm" key="common.assumption.add.success" /></td>
					</tr>
				</logic:equal>

				<logic:notEqual name="assumptionaddflag" value="0">
					<tr>
						<td>&nbsp;</td>
						<td colspan="5" class="message" height="30"><bean:message
								bundle="pm" key="common.assumption.add.failure" /></td>
					</tr>
				</logic:notEqual>
			</logic:present>

			<logic:present name="assumptionupdateflag" scope="request">
				<logic:equal name="assumptionupdateflag" value="0">
					<tr>
						<td>&nbsp;</td>
						<td colspan="5" class="message" height="30"><bean:message
								bundle="pm" key="common.assumption.update.success" /></td>
					</tr>
				</logic:equal>

				<logic:notEqual name="assumptionupdateflag" value="0">
					<tr>
						<td>&nbsp;</td>
						<td colspan="5" class="message" height="30"><bean:message
								bundle="pm" key="common.assumption.update.failure" /></td>
					</tr>
				</logic:notEqual>
			</logic:present>

			<!-- Message for add/update resources: Start -->
			<logic:present name="addresourceflag" scope="request">
				<tr>
					<td>&nbsp;</td>
					<td colspan="5" class="message" height="30"><logic:equal
							name="addresourceflag" value="0">
							<bean:message bundle="AM" key="am.resource.add.success" />
						</logic:equal> <logic:equal name="addresourceflag" value="-9001">
							<bean:message bundle="AM" key="am.resource.add.failure1" />
						</logic:equal> <logic:equal name="addresourceflag" value="-9002">
							<bean:message bundle="AM" key="am.resource.add.failure2" />
						</logic:equal> <logic:equal name="addresourceflag" value="-9003">
							<bean:message bundle="AM" key="am.resource.add.failure3" />
						</logic:equal> <logic:equal name="addresourceflag" value="-9004">
							<bean:message bundle="AM" key="am.resource.add.failure4" />
						</logic:equal> <logic:equal name="addresourceflag" value="-9005">
							<bean:message bundle="AM" key="am.resource.add.failure5" />
						</logic:equal> <logic:equal name="addresourceflag" value="-9006">
							<bean:message bundle="AM" key="am.resource.add.failure6" />
						</logic:equal></td>
				</tr>
			</logic:present>
			<logic:present name="updateresourceflag" scope="request">
				<tr>
					<td>&nbsp;</td>
					<td colspan="5" class="message" height="30"><logic:equal
							name="updateresourceflag" value="0">
							<bean:message bundle="AM" key="am.resource.update.success" />
						</logic:equal> <logic:equal name="updateresourceflag" value="-9007">
							<bean:message bundle="AM" key="am.resource.update.failure1" />
						</logic:equal> <logic:equal name="updateresourceflag" value="-9008">
							<bean:message bundle="AM" key="am.resource.update.failure2" />
						</logic:equal> <logic:equal name="updateresourceflag" value="-9009">
							<bean:message bundle="AM" key="am.resource.update.failure3" />
						</logic:equal> <logic:equal name="updateresourceflag" value="-9010">
							<bean:message bundle="AM" key="am.resource.update.failure4" />
						</logic:equal></td>
				</tr>
			</logic:present>

			<!-- Message for add/update resources: End -->


			<!-- Message for Delete: Start -->
			<logic:present name="deleteresourceflag" scope="request">
				<tr>
					<td>&nbsp;</td>
					<td colspan="5" class="message" height="30"><logic:equal
							name="deleteresourceflag" value="0">
							<bean:message bundle="AM" key="am.material.delete.success" />
						</logic:equal> <logic:equal name="deleteresourceflag" value="-9001">
							<bean:message bundle="AM" key="am.material.delete.failure1" />
						</logic:equal> <logic:equal name="deleteresourceflag" value="-9002">
							<bean:message bundle="AM" key="am.material.delete.failure2" />
						</logic:equal> <logic:equal name="deleteresourceflag" value="-9004">
							<bean:message bundle="AM" key="am.material.delete.failure4" />
						</logic:equal> <logic:equal name="deleteresourceflag" value="-90090">
							<bean:message bundle="AM" key="am.material.delete.failure3" />
						</logic:equal></td>
				</tr>
			</logic:present>
			<!-- Message for Delete: End -->
			<tr>
				<td width="10" rowspan="2">&nbsp;</td>
				<td class="Ntryb" rowspan="2" align="center"><bean:message
						bundle="AM" key="am.tabular.name" /></td>
				<td class="Ntryb" rowspan="2"><bean:message bundle="AM"
						key="am.material.cnspartnumber" /></td>
				<td class="Ntryb" rowspan="2"><bean:message bundle="AM"
						key="am.material.quantity" /></td>
				<td class="Ntryb" colspan="2"><bean:message bundle="AM"
						key="am.material.estimatedcost" /></td>
				<td class="Ntryb" rowspan="2"><bean:message bundle="AM"
						key="am.material.proformamargin" /></td>
				<td class="Ntryb" colspan="2"><bean:message bundle="AM"
						key="am.material.price" /></td>
				<td class="Ntryb" rowspan="2" colspan="2"><bean:message
						bundle="AM" key="am.tabular.action" /></td>
			</tr>

			<tr>
				<td class="Ntryb"><div align="center">
						<bean:message bundle="AM" key="am.material.estimatedunitcost" />
					</div></td>
				<td class="Ntryb"><div align="center">
						<bean:message bundle="AM" key="am.material.estimatedtotalcost" />
					</div></td>
				<td class="Ntryb"><div align="center">
						<bean:message bundle="AM" key="am.material.priceunit" />
					</div></td>
				<td class="Ntryb"><div align="center">
						<bean:message bundle="AM" key="am.material.priceextended" />
					</div></td>
			</tr>
			<!-- Labor List:Start -->
			<logic:present name="laborresourcelist" scope="request">
				<tr class="colorRow">
					<td height="2" class="whiteCol"></td>
					<td height="2" colspan="11"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="textresource" colspan="11">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="textresource" width="20"><img
									src="images/user.gif" width="12" id="Image10" height="12"
									border="0" />
								<td>
								<td class="textresource" width="50"><b><bean:message
											bundle="AM" key="am.labor.name" /></b></td>
								<td class="textresource"><c:if
										test="${ResourceListForm.edit eq 'editable'}">
										<a href="#" onclick="opensearchwindowlabor();">Add</a>
									</c:if></td>
							</tr>
						</table>
					</td>
				</tr>
				<%
					if (labor_Size > 0) {
				%>
				<logic:present name="laborresourcelist" scope="request">
					<logic:iterate id="ls" name="laborresourcelist">
						<bean:define id="resourceIdLabor" name="ls" property="laborid"
							type="java.lang.String" />
						<bean:define id="estUnitCostlabor" name="ls"
							property="laborEstimatedhourlybasecost" type="java.lang.String" />
						<bean:define id="troubleRepairFlag" name="ls"
							property="troubleRepairFlag" type="java.lang.String" />
						<%
							temp_str_labor = "marginComputationLabor('"
															+ resourceIdLabor + "', '"
															+ estUnitCostlabor + "', " + j
															+ ", " + labor_Size + ");";
													if (csschooserlabor == true) {
														backgroundclass = "Ntexto";
														backgroundhyperlinkclass = "Nhyperodd";
														csschooserlabor = false;
														readonlynumberclass = "Nreadonlytextnumberodd1";
													} else {
														csschooserlabor = true;
														backgroundclass = "Ntexte";
														backgroundhyperlinkclass = "Nhypereven";
														readonlynumberclass = "Nreadonlytextnumbereven1";
													}
						%>
						<tr>
							<td>&nbsp;</td>
							<html:hidden name="ls" property="laborid" />
							<html:hidden name="ls" property="laborcostlibid" />
							<html:hidden name="ls" property="labortype" />
							<%
								if (labor_Size == 1) {
															laborschdayscheck = "javascript: document.forms[0].laborCheck.checked = true;";
															laborarrcal = "javascript: laborcalarr( null );";
															labortransarr = "javascript: return caltransarr( null );";
															if (troubleRepairFlag != null
																	&& troubleRepairFlag
																			.equalsIgnoreCase("Y")) {
																checkDefault = "ValidateTicketResources(null);";
															} else {
																checkDefault = "ValidateTransitResource(null)";
															}
														} else if (labor_Size > 1) {
															laborschdayscheck = "javascript: document.forms[0].laborCheck['"
																	+ j + "'].checked = true;";
															laborarrcal = "javascript: laborcalarr('"
																	+ j + "');";
															labortransarr = "javascript: return caltransarr('"
																	+ j + "');";
															if (troubleRepairFlag != null
																	&& troubleRepairFlag
																			.equalsIgnoreCase("Y")) {
																checkDefault = "ValidateTicketResources('"
																		+ j + "');";
															} else {
																checkDefault = "ValidateTransitResource('"
																		+ j + "')";
															}

														}
														laborschdayscheck = laborschdayscheck;
							%>
							<td class="<%=backgroundhyperlinkclass%>"><c:if
									test="${ResourceListForm.edit eq 'editable'}">
									<html:multibox property="laborCheck">
										<bean:write name="ls" property="flag_laborid" />
									</html:multibox>
								</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
									<html:multibox property="laborCheck" disabled="true"
										styleClass="hiddenCheckBox">
										<bean:write name="ls" property="flag_laborid" />
									</html:multibox>
								</c:if> <a style="padding-left: 11px;"
								href="LaborResourceDetailAction.do?Labor_Id=<bean:write name = "ls" property = "laborid"/>&Activity_Id=<bean:write name = "ResourceListForm" property = "activity_Id"/>">
									<bean:write name="ls" property="laborname" />
							</a></td>
							<html:hidden name="ls" property="laborname" />

							<td class="<%=backgroundhyperlinkclass%>"><bean:write
									name="ls" property="laborCnspartnumber" /> <html:hidden
									styleId="laborCnspartnumber" name="ls"
									property="laborCnspartnumber" /></td>

							<logic:equal name="check_netmedx_appendix" value="Y">
								<logic:equal name="ls" property="laborStatus" value="Approved">
									<td class="<%=backgroundclass%>"><html:text name="ls"
											property="laborQuantityhours" size="10"
											styleClass="<%= readonlynumberclass %>" readonly="true" /></td>
								</logic:equal>

								<logic:notEqual name="ls" property="laborStatus"
									value="Approved">
									<td class="<%=backgroundclass%>"><c:if
											test="${ResourceListForm.edit eq 'editable'}">
											<html:text styleId='<%= "laborQuantityhours"+j%>' name="ls"
												size="10" maxlength="10" style="text-align:right"
												property="laborQuantityhours" styleClass="text"
												onmouseover="" onmouseout=""
												onchange="<%= laborschdayscheck %>"
												onblur="<%= laborarrcal %>" />
										</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
											<html:text name="ls" size="10" property="laborQuantityhours"
												styleClass="<%= readonlynumberclass %>" readonly="true" />
										</c:if></td>
								</logic:notEqual>
							</logic:equal>

							<logic:notEqual name="check_netmedx_appendix" value="Y">
								<td class="<%=backgroundclass%>"><c:if
										test="${ResourceListForm.edit eq 'editable'}">
										<html:text styleId='<%= "laborQuantityhours"+j%>' name="ls"
											size="10" maxlength="10" property="laborQuantityhours"
											style="text-align:right" styleClass="text" onmouseover=""
											onmouseout="" onchange="<%= laborschdayscheck %>"
											onblur="<%= laborarrcal %>" />
									</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
										<html:text name="ls" size="10" property="laborQuantityhours"
											styleClass="<%= readonlynumberclass %>" readonly="true" />
									</c:if></td>
							</logic:notEqual>

							<html:hidden name="ls" property="laborPrevquantityhours" />

							<td class="<%=backgroundclass%>"><html:text
									styleId="laborEstimatedhourlybasecost" name="ls" size="6"
									property="laborEstimatedhourlybasecost" readonly="true"
									styleClass="<%= readonlynumberclass %>" /></td>

							<td class="<%=backgroundclass%>"><html:text
									styleId="laborEstimatedtotalcost" name="ls" size="10"
									property="laborEstimatedtotalcost" readonly="true"
									styleClass="<%= readonlynumberclass %>" /></td>

							<logic:equal name="check_netmedx_appendix" value="Y">
								<td class="<%=backgroundclass%>"><c:if
										test="${ResourceListForm.edit eq 'editable'}">
										<logic:equal name="ls" property="labortype"
											value="Contract Labor">
											<html:text name="ls" size="9" property="laborProformamargin"
												styleClass="<%= readonlynumberclass %>" readonly="true" />
										</logic:equal>
										<logic:notEqual name="ls" property="labortype"
											value="Contract Labor">
											<html:text style="text-align:right;"
												styleId='<%= "laborProformamargin"+j%>' name="ls" size="9"
												maxlength="10" property="laborProformamargin"
												styleClass="text" />
										</logic:notEqual>
									</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
										<html:text name="ls" size="9" property="laborProformamargin"
											styleClass="<%= readonlynumberclass %>" readonly="true" />
									</c:if></td>

								<td class="<%=backgroundclass%>"><html:text
										styleId="laborPriceunit" name="ls" size="10" maxlength="10"
										property="laborPriceunit" readonly="true"
										styleClass="<%= readonlynumberclass %>" />&nbsp;&nbsp; <c:if
										test="${ResourceListForm.edit eq 'editable'}">
										<logic:equal name="ls" property="labortype"
											value="Contract Labor">
											<html:button styleClass="buttonverticalalign"
												property="setmargin" disabled="true">
												<bean:message bundle="AM" key="am.common.set" />
											</html:button>
										</logic:equal>
										<logic:notEqual name="ls" property="labortype"
											value="Contract Labor">
											<html:button styleClass="buttonverticalalign"
												property="setmargin" onclick="<%=temp_str_labor %>">
												<bean:message bundle="AM" key="am.common.set" />
											</html:button>
										</logic:notEqual>
									</c:if></td>
							</logic:equal>

							<logic:notEqual name="check_netmedx_appendix" value="Y">
								<td class="<%=backgroundclass%>"><c:if
										test="${ResourceListForm.edit eq 'editable'}">
										<html:text style="text-align:right;"
											styleId='<%= "laborProformamargin"+j%>' name="ls" size="9"
											maxlength="10" property="laborProformamargin"
											styleClass="text" onmouseover="" onmouseout=""
											onchange="<%= laborschdayscheck %>"
											onblur="<%= laborarrcal %>" />
									</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
										<html:text style="text-align:right;"
											styleId='<%= "laborProformamargin"+j%>' name="ls" size="9"
											maxlength="10" property="laborProformamargin"
											styleClass="<%= readonlynumberclass %>" readonly="true" />
									</c:if></td>

								<td class="<%=backgroundclass%>"><html:text
										styleId="laborPriceunit" name="ls" size="10"
										property="laborPriceunit" readonly="true"
										styleClass="<%= readonlynumberclass %>" />&nbsp;&nbsp; <c:if
										test="${ResourceListForm.edit eq 'editable'}">
										<html:button styleClass="buttonverticalalign"
											property="setmargin" onclick="<%=temp_str_labor%>">
											<bean:message bundle="AM" key="am.common.set" />
										</html:button>
									</c:if></td>
							</logic:notEqual>

							<td class="<%=backgroundclass%>"><html:text
									styleId="laborPriceextended" name="ls" size="10"
									property="laborPriceextended" readonly="true"
									styleClass="<%= readonlynumberclass %>" /></td>
							<td class="<%=backgroundclass%>"><logic:present
									name="check_netmedx_appendix">
									<logic:equal name="check_netmedx_appendix" value="Y">
										<c:if test="${ResourceListForm.edit eq 'editable'}">
											<html:multibox property="laborChecktransit"
												onclick="<%=labortransarr%>">
												<bean:write name="ls" property="laborid" />
											</html:multibox>
										</c:if>
										<c:if test="${ResourceListForm.edit ne 'editable'}">
											<html:multibox property="laborChecktransit" disabled="true">
												<bean:write name="ls" property="laborid" />
											</html:multibox>
										</c:if>
										<bean:message bundle="AM" key="am.labor.intransit" />
									</logic:equal>
								</logic:present> <a
								href="ManageSOWAction.do?function=View&Type=L&Id=<bean:write name ="ls" property = "laborid"/>&Activity_Id=<bean:write name="ResourceListForm" property="activity_Id"/>"><bean:message
										bundle="AM" key="am.tabular.sow" /></a>| <a
								href="ManageAssumptionAction.do?function=View&Type=L&Id=<bean:write name ="ls" property = "laborid"/>&Activity_Id=<bean:write name="ResourceListForm" property="activity_Id"/>" />
								<bean:message bundle="AM" key="am.tabular.assumption" /></a></td>
						</tr>
						<html:hidden name="ls" property="laborSellablequantity" />
						<html:hidden name="ls" property="laborMinimumquantity" />
						<html:hidden name="ls" property="laborCnspartnumber" />
						<html:hidden name="ls" property="laborPrevquantityhours" />
						<html:hidden name="ls" property="laborStatus" />
						<html:hidden name="ls" property="laborFlag" />
						<html:hidden name="ls" property="laborTransit" />
						<%
							j++;
						%>
					</logic:iterate>
					<jsp:include page="/PM/Resource/ValidateTicketResources.jsp" />
				</logic:present>
				<%
					} else {
				%>
				<tr>
					<td>&nbsp;</td>
					<td class="colLight" colspan="11"><font
						class="dbvalueDisabled">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
								bundle="AM" key="am.no.resources" /></td>
				</tr>
				<%
					}
				%>
			</logic:present>
			<!-- Material List:Start -->
			<logic:present name="materialresourcelist" scope="request">
				<tr class="colorRow">
					<td height="2" class="whiteCol"></td>
					<td height="2" colspan="11"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="textresource" colspan="11">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="textresource" width="20"><img
									src="images/materials.gif" width="12" id="Image10" height="12"
									border="0" />
								<td>
								<td class="textresource" width="50"><b><bean:message
											bundle="AM" key="am.materials" /></b></td>
								<td class="textresource"><c:if
										test="${ResourceListForm.edit eq 'editable'}">
										<a href="#" onclick="opensearchwindowmaterial();">Add</a>
									</c:if></td>
							</tr>
						</table>
					</td>
				</tr>
				<%
					if (material_Size > 0) {
				%>
				<logic:present name="materialresourcelist" scope="request">

					<logic:iterate id="ms" name="materialresourcelist">
						<bean:define id="resourceIdMaterial" name="ms"
							property="materialid" type="java.lang.String" />
						<bean:define id="estUnitCostMaterial" name="ms"
							property="materialEstimatedunitcost" type="java.lang.String" />
						<%
							temp_str_material = "marginComputationMaterial('"
															+ resourceIdMaterial
															+ "', '"
															+ estUnitCostMaterial
															+ "', "
															+ i
															+ ", " + material_Size + ");";
													if (csschoosermaterial == true) {
														backgroundclass = "Ntexto";
														backgroundhyperlinkclass = "Nhyperodd";
														readonlynumberclass = "Nreadonlytextnumberodd";
														csschoosermaterial = false;
													} else {
														csschoosermaterial = true;
														backgroundclass = "Ntexte";
														backgroundhyperlinkclass = "Nhypereven";
														readonlynumberclass = "Nreadonlytextnumbereven";
													}
						%>
						<bean:define id="val" name="ms" property="materialid"
							type="java.lang.String" />
						<bean:define id="materialFlag" name="ms" property="materialFlag"
							type="java.lang.String" />
						<html:hidden name="ms" property="flag_materialid" />
						<tr>
							<td>&nbsp;</td>
							<html:hidden name="ms" property="materialid" />
							<html:hidden name="ms" property="materialCostlibid" />
							<html:hidden name="ms" property="materialFlag" />
							<html:hidden name="ms" property="materialname" />

							<%
								if (material_Size == 1) {
															materialschdayscheck = "javascript: document.forms[0].materialCheck.checked = true;";
															materialarrcal = "javascript: materialcalarr(null);";
														} else if (material_Size > 1) {
															materialschdayscheck = "javascript: document.forms[0].materialCheck['"
																	+ i + "'].checked = true;";
															materialarrcal = "javascript: materialcalarr('"
																	+ i + "');";
														}
							%>

							<td class="<%=backgroundhyperlinkclass%>"><c:if
									test="${ResourceListForm.edit eq 'editable'}">
									<html:multibox property="materialCheck">
										<bean:write name="ms" property="flag_materialid" />
									</html:multibox>
								</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
									<html:multibox property="materialCheck"
										styleClass="hiddenCheckBox" disabled="true">
										<bean:write name="ms" property="flag_materialid" />
									</html:multibox>
								</c:if> <a style="padding-left: 11px;"
								href="MaterialResourceDetailAction.do?Material_Id=<bean:write name = "ms" property = "materialid"/>&Activity_Id=<bean:write name = "ResourceListForm" property = "activity_Id"/>">
									<bean:write name="ms" property="materialname" />,&nbsp;<bean:write
										name="ms" property="materialManufacturername" />
							</a></td>

							<html:hidden styleId="manufacturername" name="ms"
								property="materialManufacturername" />
							<html:hidden name="ms" property="materialname" />

							<td class="<%=backgroundhyperlinkclass%>"><bean:write
									name="ms" property="materialCnspartnumber" /> <html:hidden
									name="ms" property="materialCnspartnumber" /></td>

							<td class="<%=backgroundclass%>"><c:if
									test="${ResourceListForm.edit eq 'editable'}">
									<html:text style="text-align:right"
										styleId='<%= "materialQuantity"+i%>' name="ms" size="10"
										maxlength="10" property="materialQuantity" styleClass="text"
										onmouseover="" onmouseout=""
										onchange="<%= materialschdayscheck %>"
										onblur="<%= materialarrcal %>" />
								</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
									<html:text name="ms" size="10" property="materialQuantity"
										styleClass="<%=readonlynumberclass%>" />
								</c:if></td>

							<html:hidden name="ms" property="materialPrevquantity" />

							<td class="<%=backgroundclass%>"><html:text
									styleId="materialEstimatedunitcost" name="ms" size="6"
									property="materialEstimatedunitcost" readonly="true"
									styleClass="<%= readonlynumberclass %>" /></td>
							<td class="<%=backgroundclass%>"><html:text
									styleId="materialEstimatedtotalcost" name="ms" size="10"
									maxlength="9" property="materialEstimatedtotalcost"
									readonly="true" styleClass="<%= readonlynumberclass %>" /></td>

							<td class="<%=backgroundclass%>"><c:if
									test="${ResourceListForm.edit eq 'editable'}">
									<html:text style="text-align:right"
										styleId='<%= "materialProformamargin"+i%>' name="ms" size="9"
										maxlength="10" property="materialProformamargin"
										styleClass="text" onchange="<%= materialschdayscheck %>"
										onblur="<%= materialarrcal %>" />
								</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
									<html:text name="ms" size="9" property="materialProformamargin"
										styleClass="<%=readonlynumberclass %>" />
								</c:if></td>
							<td class="<%=backgroundclass%>"><html:text
									styleId="materialPriceunit" name="ms" size="10" maxlength="10"
									property="materialPriceunit"
									styleClass="<%= readonlynumberclass %>" />&nbsp;&nbsp; <c:if
									test="${ResourceListForm.edit eq 'editable'}">
									<html:button styleClass="buttonverticalalign"
										property="setmargin" onclick="<%= temp_str_material%>">
										<bean:message bundle="AM" key="am.common.set" />
									</html:button>
								</c:if></td>

							<td class="<%=backgroundclass%>"><html:text
									styleId="materialPriceextended" name="ms" size="10"
									maxlength="9" property="materialPriceextended"
									styleClass="<%= readonlynumberclass %>" /></td>

							<td class="<%=backgroundclass%>"><a
								href="ManageSOWAction.do?function=View&Type=M&Id=<bean:write name = "ms" property = "materialid"/>&Activity_Id=<bean:write name="ResourceListForm" property="activity_Id"/>"><bean:message
										bundle="AM" key="am.tabular.sow" /></a>| <a
								href="ManageAssumptionAction.do?function=View&Type=M&Id=<bean:write name = "ms" property = "materialid"/>&Activity_Id=<bean:write name="ResourceListForm" property="activity_Id"/>" />
								<bean:message bundle="AM" key="am.tabular.assumption" /></a></td>

						</tr>
						<html:hidden name="ms" property="materialSellablequantity" />
						<html:hidden name="ms" property="materialMinimumquantity" />
						<html:hidden name="ms" property="materialStatus" />

						<%
							i++;
						%>
					</logic:iterate>
				</logic:present>

				<%
					} else {
				%>
				<tr>
					<td>&nbsp;</td>
					<td class="colLight" colspan="11"><font
						class="dbvalueDisabled">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
								bundle="AM" key="am.no.resources" /></font></td>
				</tr>
				<%
					}
				%>
			</logic:present>
			<!-- Material List:End -->
			<!-- Travel List:Start -->
			<logic:present name="travelresourcelist" scope="request">
				<tr class="colorRow">
					<td height="2" class="whiteCol"></td>
					<td height="2" colspan="11"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="textresource" colspan="11" width="">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="textresource" width="20"><img
									src="images/travel.gif" width="12" id="Image10" height="12"
									border="0" />
								<td>
								<td class="textresource" width="50"><b><bean:message
											bundle="AM" key="am.travel.name" /></b></td>
								<td class="textresource"><c:if
										test="${ResourceListForm.edit eq 'editable'}">
										<a href="#" onclick="opensearchwindowtravel();">Add</a>
									</c:if></td>
							</tr>
						</table>
					</td>
				</tr>

				<%
					if (travel_Size > 0) {
				%>

				<logic:present name="travelresourcelist" scope="request">
					<logic:iterate id="ts" name="travelresourcelist">
						<bean:define id="resourceIdTravel" name="ts" property="travelid"
							type="java.lang.String" />
						<bean:define id="estUnitCostTravel" name="ts"
							property="travelEstimatedunitcost" type="java.lang.String" />
						<%
							temp_str_travel = "marginComputationTravel('"
															+ resourceIdTravel
															+ "', '"
															+ estUnitCostTravel
															+ "', "
															+ l
															+ ", " + travel_Size + ");";
													if (csschoosertravel == true) {
														backgroundclass = "Ntexto";
														backgroundhyperlinkclass = "Nhyperodd";
														readonlynumberclass = "Nreadonlytextnumberodd";
														csschoosertravel = false;
													}

													else {
														csschoosertravel = true;
														backgroundclass = "Ntexte";
														backgroundhyperlinkclass = "Nhypereven";
														readonlynumberclass = "Nreadonlytextnumbereven";
													}
						%>
						<bean:define id="val" name="ts" property="travelid"
							type="java.lang.String" />
						<bean:define id="flag" name="ts" property="travelFlag"
							type="java.lang.String" />
						<tr>
							<td>&nbsp;</td>
							<html:hidden name="ts" property="travelid" />
							<html:hidden name="ts" property="travelFlag" />
							<html:hidden name="ts" property="travelname" />
							<html:hidden name="ts" property="travelcostlibid" />

							<%
								if (travel_Size == 1) {
															travelschdayscheck = "javascript: document.forms[0].travelCheck.checked = true;";
															travelarrcal = "javascript: travelcalarr( null );";
														} else if (travel_Size > 1) {

															travelschdayscheck = "javascript: document.forms[0].travelCheck['"
																	+ l + "'].checked = true;";
															travelarrcal = "javascript: travelcalarr('"
																	+ l + "');";
														}
							%>

							<td class="<%=backgroundhyperlinkclass%>"><c:if
									test="${ResourceListForm.edit eq 'editable'}">
									<html:multibox property="travelCheck">
										<bean:write name="ts" property="flag_travelid" />
									</html:multibox>
								</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
									<html:multibox property="travelCheck"
										styleClass="hiddenCheckBox" disabled="true">
										<bean:write name="ts" property="flag_travelid" />
									</html:multibox>
								</c:if> <a style="padding-left: 11px;"
								href="TravelResourceDetailAction.do?Travel_Id=<bean:write name = "ts" property = "travelid"/>&Activity_Id=<bean:write name = "ResourceListForm" property = "activity_Id"/>">
									<bean:write name="ts" property="travelname" />

							</a></td>
							<td class="<%=backgroundhyperlinkclass%>"><bean:write
									name="ts" property="travelCnspartnumber" /> <html:hidden
									styleId="cnspartnumber" name="ts"
									property="travelCnspartnumber" /></td>

							<td class="<%=backgroundclass%>"><c:if
									test="${ResourceListForm.edit eq 'editable'}">
									<html:text styleId='<%="travelQuantity"+l%>' name="ts"
										size="10" style="text-align:right" maxlength="10"
										property="travelQuantity" styleClass="text" onmouseover=""
										onmouseout="" onchange="<%= travelschdayscheck %>"
										onblur="<%= travelarrcal %>" />
								</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
									<html:text name="ts" size="10" property="travelQuantity"
										styleClass="<%=readonlynumberclass %>" />
								</c:if></td>

							<html:hidden name="ts" property="travelPrevquantity" />

							<td class="<%=backgroundclass%>"><html:text
									styleId="travelEstimatedunitcost" name="ts" size="6"
									property="travelEstimatedunitcost" readonly="true"
									styleClass="<%= readonlynumberclass %>" /></td>
							<td class="<%=backgroundclass%>"><html:text
									styleId="travelEstimatedtotalcost" name="ts" size="10"
									maxlength="9" property="travelEstimatedtotalcost"
									readonly="true" styleClass="<%= readonlynumberclass %>" /></td>

							<logic:equal name="check_netmedx_appendix" value="Y">
								<td class="<%=backgroundclass%>"><c:if
										test="${ResourceListForm.edit eq 'editable'}">
										<logic:equal name="ts" property="traveltype" value="Hourly">
											<html:text name="ts" size="9" property="travelProformamargin"
												styleClass="<%=readonlynumberclass %>" />
										</logic:equal>
										<logic:notEqual name="ts" property="traveltype" value="Hourly">
											<html:text style="text-align:right"
												styleId='<%= "travelProformamargin"+l%>' name="ts" size="9"
												maxlength="10" property="travelProformamargin"
												styleClass="text" onmouseover="" onmouseout=""
												onchange="<%= travelschdayscheck %>"
												onblur="<%= travelarrcal %>" />
										</logic:notEqual>
									</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
										<html:text name="ts" size="9" property="travelProformamargin"
											styleClass="<%=readonlynumberclass %>" />
									</c:if></td>
							</logic:equal>
							<logic:notEqual name="check_netmedx_appendix" value="Y">
								<td class="<%=backgroundclass%>"><c:if
										test="${ResourceListForm.edit eq 'editable'}">
										<html:text style="text-align:right"
											styleId='<%= "travelProformamargin"+l%>' name="ts" size="9"
											maxlength="10" property="travelProformamargin"
											styleClass="text" onmouseover="" onmouseout=""
											onchange="<%= travelschdayscheck %>"
											onblur="<%= travelarrcal %>" />
									</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
										<html:text name="ts" size="9" property="travelProformamargin"
											styleClass="<%=readonlynumberclass %>" />
									</c:if></td>
							</logic:notEqual>


							<td class="<%=backgroundclass%>"><html:text
									styleId="travelPriceunit" name="ts" size="10" maxlength="9"
									property="travelPriceunit" readonly="true"
									styleClass="<%= readonlynumberclass %>" />&nbsp;&nbsp; <logic:equal
									name="check_netmedx_appendix" value="Y">
									<c:if test="${ResourceListForm.edit eq 'editable'}">
										<logic:equal name="ts" property="traveltype" value="Hourly">
											<html:button styleClass="buttonverticalalign"
												property="setmargin" disabled="true">
												<bean:message bundle="AM" key="am.common.set" />
											</html:button>
										</logic:equal>
										<logic:notEqual name="ts" property="traveltype" value="Hourly">
											<html:button styleClass="buttonverticalalign"
												property="setmargin" onclick="<%= temp_str_travel%>">
												<bean:message bundle="AM" key="am.common.set" />
											</html:button>
										</logic:notEqual>
									</c:if>
								</logic:equal> <logic:notEqual name="check_netmedx_appendix" value="Y">
									<c:if test="${ResourceListForm.edit eq 'editable'}">
										<html:button styleClass="buttonverticalalign"
											property="setmargin" onclick="<%= temp_str_travel%>">
											<bean:message bundle="AM" key="am.common.set" />
										</html:button>
									</c:if>
								</logic:notEqual></td>

							<td class="<%=backgroundclass%>"><html:text
									styleId="travelPriceextended" name="ts" size="10" maxlength="9"
									property="travelPriceextended" readonly="true"
									styleClass="<%= readonlynumberclass %>" /></td>

							<td class="<%=backgroundclass%>"><a
								href="ManageSOWAction.do?function=View&Type=T&Id=<bean:write name = "ts" property = "travelid"/>&Activity_Id=<bean:write name="ResourceListForm" property="activity_Id"/>"><bean:message
										bundle="AM" key="am.tabular.sow" /></a>| <a
								href="ManageAssumptionAction.do?function=View&Type=T&Id=<bean:write name = "ts" property = "travelid"/>&Activity_Id=<bean:write name="ResourceListForm" property="activity_Id"/>" />
								<bean:message bundle="AM" key="am.tabular.assumption" /></a></td>


							<html:hidden name="ts" property="travelStatus" />
							<html:hidden name="ts" property="travelSellablequantity" />
							<html:hidden name="ts" property="travelMinimumquantity" />
						</tr>

						<%
							l++;
						%>
					</logic:iterate>
				</logic:present>
				<%
					} else {
				%>
				<tr>
					<td>&nbsp;</td>
					<td class="colLight" colspan="11"><font
						class="dbvalueDisabled">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
								bundle="AM" key="am.no.resources" /></font></td>
				</tr>
				<%
					}
				%>
			</logic:present>

			<!-- Travel List:End -->

			<!-- Freight List:Start -->

			<logic:present name="freightresourcelist" scope="request">
				<tr class="colorRow">
					<td height="2" class="whiteCol"></td>
					<td height="2" colspan="11"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="textresource" colspan="11" width="">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="textresource" width="20"><img
									src="images/freight.gif" width="12" id="Image10" height="12"
									border="0" />
								<td>
								<td class="textresource" width="50"><b><bean:message
											bundle="AM" key="am.freight.name" /></b></td>
								<td class="textresource"><c:if
										test="${ResourceListForm.edit eq 'editable'}">
										<a href="#" onclick="opensearchwindowfreight();">Add</a>
									</c:if></td>
							</tr>
						</table>
					</td>
				</tr>
				<%
					if (freight_Size > 0) {
				%>
				<logic:present name="freightresourcelist" scope="request">
					<logic:iterate id="fs" name="freightresourcelist">
						<bean:define id="resourceIdFreight" name="fs" property="freightid"
							type="java.lang.String" />
						<bean:define id="estUnitCostFreight" name="fs"
							property="freightEstimatedunitcost" type="java.lang.String" />
						<%
							temp_str_freight = "marginComputationFreight('"
															+ resourceIdFreight
															+ "', '"
															+ estUnitCostFreight
															+ "', "
															+ k
															+ ", " + freight_Size + ");";

													if (csschooserfreight == true) {
														backgroundclass = "Ntexto";
														backgroundhyperlinkclass = "Nhyperodd";
														readonlynumberclass = "Nreadonlytextnumberodd";
														csschooserfreight = false;
													} else {
														csschooserfreight = true;
														backgroundclass = "Ntexte";
														backgroundhyperlinkclass = "Nhypereven";
														readonlynumberclass = "Nreadonlytextnumbereven";
													}
						%>
						<bean:define id="val" name="fs" property="freightid"
							type="java.lang.String" />
						<bean:define id="flag" name="fs" property="freightFlag"
							type="java.lang.String" />
						<tr>
							<td>&nbsp;</td>
							<html:hidden name="fs" property="freightid" />
							<html:hidden name="fs" property="freightcostlibid" />
							<html:hidden name="fs" property="freightFlag" />
							<html:hidden name="fs" property="freightname" />

							<%
								if (freight_Size == 1) {
															schdayscheck = "javascript: document.forms[0].freightCheck.checked = true;";
															arrcal = "javascript: calarr(null);";

														} else if (freight_Size > 1) {
															schdayscheck = "javascript: document.forms[0].freightCheck['"
																	+ k + "'].checked = true;";
															arrcal = "javascript: calarr('" + k
																	+ "');";
														}
							%>
							<td class="<%=backgroundhyperlinkclass%>"><c:if
									test="${ResourceListForm.edit eq 'editable'}">
									<html:multibox property="freightCheck">
										<bean:write name="fs" property="flag_freightid" />
									</html:multibox>
								</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
									<html:multibox property="freightCheck"
										styleClass="hiddenCheckBox" disabled="true">
										<bean:write name="fs" property="flag_freightid" />
									</html:multibox>
								</c:if> <a style="padding-left: 11px;"
								href="FreightResourceDetailAction.do?Freight_Id=<bean:write name = "fs" property = "freightid"/>&Activity_Id=<bean:write name = "ResourceListForm" property = "activity_Id"/>">
									<bean:write name="fs" property="freightname" />
							</a></td>
							<td class="<%=backgroundhyperlinkclass%>"><bean:write
									name="fs" property="freightCnspartnumber" /> <html:hidden
									styleId="cnspartnumber" name="fs"
									property="freightCnspartnumber" /></td>

							<td class="<%=backgroundclass%>"><c:if
									test="${ResourceListForm.edit eq 'editable'}">
									<html:text style="text-align:right"
										styleId='<%= "freightQuantity"+k%>' name="fs" size="10"
										maxlength="10" property="freightQuantity" styleClass="text"
										onmouseover="" onmouseout="" onchange="<%= schdayscheck %>"
										onblur="<%= arrcal %>" />
								</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
									<html:text name="fs" size="10" property="freightQuantity"
										styleClass="<%=readonlynumberclass %>" />
								</c:if></td>

							<html:hidden name="fs" property="freightPrevquantity" />
							<td class="<%=backgroundclass%>"><html:text
									styleId="freightEstimatedunitcost" name="fs" size="6"
									property="freightEstimatedunitcost" readonly="true"
									styleClass="<%= readonlynumberclass %>" /></td>
							<td class="<%=backgroundclass%>"><html:text
									styleId="freightEstimatedtotalcost" name="fs" size="10"
									maxlength="9" property="freightEstimatedtotalcost"
									readonly="true" styleClass="<%= readonlynumberclass %>" /></td>

							<td class="<%=backgroundclass%>"><c:if
									test="${ResourceListForm.edit eq 'editable'}">
									<html:text style="text-align:right"
										styleId='<%= "freightProformamargin"+k%>' name="fs" size="9"
										maxlength="10" property="freightProformamargin"
										styleClass="text" onmouseover="" onmouseout=""
										onchange="<%= schdayscheck %>" onblur="<%= arrcal %>" />
								</c:if> <c:if test="${ResourceListForm.edit ne 'editable'}">
									<html:text name="fs" size="9" property="freightProformamargin"
										styleClass="<%=readonlynumberclass %>" />
								</c:if></td>
							<td class="<%=backgroundclass%>"><html:text
									styleId="freightPriceunit" name="fs" size="10" maxlength="9"
									property="freightPriceunit" readonly="true"
									styleClass="<%= readonlynumberclass %>" />&nbsp;&nbsp; <c:if
									test="${ResourceListForm.edit eq 'editable'}">
									<html:button styleClass="buttonverticalalign"
										property="setmargin" onclick="<%=temp_str_freight%>">
										<bean:message bundle="AM" key="am.common.set" />
									</html:button>
								</c:if></td>

							<td class="<%=backgroundclass%>"><html:text
									styleId="freightPriceextended" name="fs" size="10"
									maxlength="9" property="freightPriceextended" readonly="true"
									styleClass="<%= readonlynumberclass %>" /></td>

							<td class="<%=backgroundclass%>"><a
								href="ManageSOWAction.do?function=View&Type=F&Id=<bean:write name = "fs" property = "freightid"/>&Activity_Id=<bean:write name="ResourceListForm" property="activity_Id"/>"><bean:message
										bundle="AM" key="am.tabular.sow" /></a>| <a
								href="ManageAssumptionAction.do?function=View&Type=F&Id=<bean:write name = "fs" property = "freightid"/>&Activity_Id=<bean:write name="ResourceListForm" property="activity_Id"/>" />
								<bean:message bundle="AM" key="am.tabular.assumption" /></a></td>

							<html:hidden name="fs" property="freightStatus" />
							<html:hidden name="fs" property="freightSellablequantity" />
							<html:hidden name="fs" property="freightMinimumquantity" />

						</tr>
						<%
							k++;
						%>
					</logic:iterate>
				</logic:present>
				<%
					} else {
				%>
				<tr>
					<td>&nbsp;</td>
					<td class="colLight" colspan="11"><font
						class="dbvalueDisabled">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message
								bundle="AM" key="am.no.resources" /></font></td>
				</tr>
				<%
					}
				%>
			</logic:present>
			<!-- Freight List:End -->

			<tr>
				<td>&nbsp;</td>
				<td colspan="11" class="Nbuttonrow">&nbsp;&nbsp;&nbsp; <c:if
						test="${empty ResourceListForm.edit}">
						<%--	<c:if test="${empty ResourceListForm.editableResource or ResourceListForm.editableResource eq 'true'}">  --%>
						<logic:notEqual name="act_status" value="Approved" scope="request">
							<html:submit property="edit" styleClass="button_c"
								onclick="return SubmitPage('view','edit');">&nbsp;Edit&nbsp;</html:submit>
						</logic:notEqual>
						<!--<html:submit property = "edit" styleClass = "button_c" disabled="true">&nbsp;Edit&nbsp;</html:submit>-->
					</c:if> <c:if test="${not empty ResourceListForm.edit}">
						<html:submit property="save" styleClass="button_c"
							onclick="return SubmitPage('update', 'save')">Save</html:submit>&nbsp;&nbsp;
							<html:reset property="cancel" styleClass="button_c">Cancel</html:reset>&nbsp;&nbsp;
							
								<html:submit property="delete" styleClass="button_c"
							onclick="return SubmitPage('update','delete');">Delete</html:submit>

					</c:if>
				</td>
			</tr>

		</table>

		<c:if test="${requestScope.appendixStatus eq 'prjAppendix'}">
			<c:if test="${requestScope.jobType eq 'Addendum'}">
				<%@ include file="/AddendumMenuScript.inc"%>
			</c:if>
			<c:if test="${requestScope.jobType eq 'Default'}">
				<%@ include file="/DefaultJobMenuScript.inc"%>
			</c:if>
			<c:if
				test="${requestScope.jobType eq 'inscopejob' || requestScope.jobType eq 'Newjob' }">
				<%@ include file="/ProjectJobMenuScript.inc"%>
			</c:if>
		</c:if>

	</html:form>
</body>
</html:html>
<script type="text/javascript">
function SubmitPage(action, action2)
{
	var confirmVal = true;
 	if(action2=='save'){
 		var creationTime =  <%=(Long) request.getAttribute("creationDate")%>;
 		var currentTime = new Date().getTime();
 		if((!isNaN(parseFloat(creationTime))) && 
 				(currentTime - creationTime) <= (1 * 60 * 60 * 1000)){//hour*mints*sec*milliSec
 			confirmVal = confirm("The activity you are trying to save has insufficient project coordination time allocated. The minimum per visit time to be alloted is one hour. Do you wish to continue?");
 		}
 	}
 	if(confirmVal){
 		document.forms[0].action="AMResourceLibraryListAction.do?ref="+action+"&Activitylibrary_Id=<bean:write name="ResourceListForm" property="activity_Id"/>&editable=editable";	
 	}else{
 		return false;
 	}
	
}


function  materialChecking()
{
	var submitflag = 'false';
	if( <%=material_Size%> != 0 )
	{
		if( <%=material_Size%> == 1 )
		{
			if( !( document.forms[0].materialCheck.checked ))
			{
			//	alert( "<bean:message bundle = "AM" key = "am.material.selectresource"/>" );
				return false;
			}
			else
			{
				submitflag = 'true';
			}
			
		}
		else
		{
			for( var i = 0; i<document.forms[0].materialCheck.length; i++ )
		  	{
		  		if( document.forms[0].materialCheck[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' )
		  	{
		  		//alert( "<bean:message bundle = "AM" key = "am.material.selectresource"/>" );
				return false;
		  	}
		}
	}
	
	else
	{
		
	}
	
	
	if( <%=material_Size%> != 0 )
	{
		if( <%=material_Size%> == 1 )
		{
			if( document.forms[0].materialCheck.checked ) 
	  		{
	  			if( document.forms[0].materialQuantity.value == "" )
	  			{
	  				document.forms[0].materialQuantity.value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].materialQuantity.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.material.numericvalue"/>" );
		 			document.forms[0].materialQuantity.value = "";
		 			document.forms[0].materialQuantity.focus();
					return false;
		 		}
		 		
		 		if( document.forms[0].materialProformamargin.value == "" )
	  			{
	  				document.forms[0].materialProformamargin.value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].materialProformamargin.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.material.numericvalue"/>" );
		 			document.forms[0].materialProformamargin.value = "";
		 			document.forms[0].materialProformamargin.focus();
					return false;
		 		}	
	  		}
		}
		
		else
		{
			for( var i = 0; i<document.forms[0].materialCheck.length; i++ )
		  	{
		  		if( document.forms[0].materialCheck[i].checked ) 
		  		{
			  		if( document.forms[0].materialQuantity[i].value == "" )
		  			{
		  				document.forms[0].materialQuantity[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].materialQuantity[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.material.numericvalue"/>" );
			 			document.forms[0].materialQuantity[i].value = "";
			 			document.forms[0].materialQuantity[i].focus();
						return false;
			 		}
			 		
			 		if( document.forms[0].materialProformamargin[i].value == "" )
		  			{
		  				document.forms[0].materialProformamargin[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].materialProformamargin[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.material.numericvalue"/>" );
			 			document.forms[0].materialProformamargin[i].value = "";
			 			document.forms[0].materialProformamargin[i].focus();
						return false;
			 		}	
			 		
		  		}
		  	}
		}
	}
return submitflag;
}


function laborChecking(){

	var submitflag = 'false';
	if( <%=labor_Size%> != 0 )
	{
		if( <%=labor_Size%> == 1 )
		{
			if( !( document.forms[0].laborCheck.checked ))
			{
				//alert( "<bean:message bundle = "AM" key = "am.labor.selectresource"/>" );
				return false;
			}
			else
			{
				submitflag = 'true';
			}
		}
		else
		{
			
			for( var i = 0; i<document.forms[0].laborCheck.length; i++ )
		  	{
		  		
		  		if( document.forms[0].laborCheck[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	
		  	if( submitflag == 'false')
		  	{
		  	//	alert( "<bean:message bundle = "AM" key = "am.labor.selectresource"/>" );
				return false;
		  	}
		}
	}
	
	else
	{
		
	}
	
	if( <%=labor_Size%> != 0 )
	{
		if( <%=labor_Size%> == 1 )
		{
			if( document.forms[0].laborCheck.checked ) 
	  		{
	  			if( document.forms[0].laborQuantityhours.value == "" )
	  			{
	  				document.forms[0].laborQuantityhours[i].value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].laborQuantityhours.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
		 			document.forms[0].laborQuantityhours.value = "";
		 			document.forms[0].laborQuantityhours.focus();
					return false;
		 		}
		 		
		 		if( document.forms[0].laborProformamargin.value == "" )
	  			{
	  				document.forms[0].laborProformamargin.value = "0.0";
	  			}
	 
		  		if(document.forms[0].checknetmedx.value!=null && document.forms[0].checknetmedx.value=="Y")
				{
			  		
			 	}
			 	else
			 	{
			 		if( !isFloat( document.forms[0].laborProformamargin.value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
			 			document.forms[0].laborProformamargin.value = "";
			 			document.forms[0].laborProformamargin.focus();
						return false;
			 		}
			 	}	
	  		}
		}
		
		else
		{
			for( var i = 0; i<document.forms[0].laborCheck.length; i++ )
		  	{
		  		if( document.forms[0].laborCheck[i].checked ) 
		  		{
			  		if( document.forms[0].laborQuantityhours[i].value == "" )
		  			{
		  				document.forms[0].laborQuantityhours[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].laborQuantityhours[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
			 			document.forms[0].laborQuantityhours[i].value = "";
			 			document.forms[0].laborQuantityhours[i].focus();
						return false;
			 		}
			 		
			 		if( document.forms[0].laborProformamargin[i].value == "" )
		  			{
		  				document.forms[0].laborProformamargin[i].value = "0.0";
		  			}
		 
			  		
			  		if(document.forms[0].checknetmedx.value!=null && document.forms[0].checknetmedx.value=="Y")
					{
			  		
			 		}
			 		else
			 		{
				  		if( !isFloat( document.forms[0].laborProformamargin[i].value ) )
				 		{
				 			alert( "<bean:message bundle = "AM" key = "am.labor.numericvalue"/>" );
				 			document.forms[0].laborProformamargin[i].value = "";
				 			document.forms[0].laborProformamargin[i].focus();
							return false;
				 		}
				 	}	
		  		}
		  	}
		}
	}
	
	return submitflag;	
}


function travelChecking(){
	
	var submitflag = 'false';
	if( <%=travel_Size%> != 0 )
	{
		if( <%=travel_Size%> == 1 )
		{
			if( !( document.forms[0].travelCheck.checked ))
			{
			 //	alert( "<bean:message bundle = "AM" key = "am.travel.selectresource"/>" );
				return false;
			}
			else
			{
				submitflag = 'true';
			}
			
		}
		else
		{
			for( var i = 0; i<document.forms[0].travelCheck.length; i++ )
		  	{
		  		if( document.forms[0].travelCheck[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' )
		  	{
		  	//	alert( "<bean:message bundle = "AM" key = "am.travel.selectresource"/>" );
				return false;
		  	}
		}
	}
	
	else
	{
		
	}
	
	if( <%=travel_Size%> != 0 )
	{
		if( <%=travel_Size%> == 1 )
		{
			if( document.forms[0].travelCheck.checked ) 
	  		{
	  			if( document.forms[0].travelQuantity.value == "" )
	  			{
	  				document.forms[0].travelQuantity.value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].travelQuantity.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.travel.numericvalue"/>" );
		 			document.forms[0].travelQuantity.value = "";
		 			document.forms[0].travelQuantity.focus();
					return false;
		 		}
		 		
		 		if( document.forms[0].travelProformamargin.value == "" )
	  			{
	  				document.forms[0].travelProformamargin.value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].travelProformamargin.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.travel.numericvalue"/>" );
		 			document.forms[0].travelProformamargin.value = "";
		 			document.forms[0].travelProformamargin.focus();
					return false;
		 		}	
	  		}
		}
		
		else
		{
			for( var i = 0; i<document.forms[0].travelCheck.length; i++ )
		  	{
		  		if( document.forms[0].travelCheck[i].checked ) 
		  		{
			  		if( document.forms[0].travelQuantity[i].value == "" )
		  			{
		  				document.forms[0].travelQuantity[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].travelQuantity[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.travel.numericvalue"/>" );
			 			document.forms[0].travelQuantity[i].value = "";
			 			document.forms[0].travelQuantity[i].focus();
						return false;
			 		}
			 		
			 		if( document.forms[0].travelProformamargin[i].value == "" )
		  			{
		  				document.forms[0].travelProformamargin[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].travelProformamargin[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.travel.numericvalue"/>" );
			 			document.forms[0].travelProformamargin[i].value = "";
			 			document.forms[0].travelProformamargin[i].focus();
						return false;
			 		}	
		  		}
		  	}
		}
	}
	
return submitflag;	
}

function freightChecking()
{
	var submitflag = 'false';
	if( <%=freight_Size%> != 0 )
	{
		if( <%=freight_Size%> == 1 )
		{
			if( !( document.forms[0].freightCheck.checked ))
			{
			//	alert( "<bean:message bundle = "AM" key = "am.freight.selectresource"/>" );
				return false;
			}
			else
			{
				submitflag = 'true';
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].freightCheck.length; i++ )
		  	{
		  		if( document.forms[0].freightCheck[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false')
		  	{
		  	//	alert( "<bean:message bundle = "AM" key = "am.freight.selectresource"/>" );
				return false;
		  	}
		}
	}
	
	else
	{
		
	}
	
	if( <%=freight_Size%> != 0 )
	{
		if( <%=freight_Size%> == 1 )
		{
			if( document.forms[0].freightCheck.checked ) 
	  		{
	  			if( document.forms[0].freightQuantity.value == "" )
	  			{
	  				document.forms[0].freightQuantity.value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].freightQuantity.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.freight.numericvalue"/>" );
		 			document.forms[0].freightQuantity.value = "";
		 			document.forms[0].freightQuantity.focus();
					return false;
		 		}
		 		
		 		if( document.forms[0].freightProformamargin.value == "" )
	  			{
	  				document.forms[0].freightProformamargin.value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].freightProformamargin.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.freight.numericvalue"/>" );
		 			document.forms[0].freightProformamargin.value = "";
		 			document.forms[0].freightProformamargin.focus();
					return false;
		 		}	
	  		}
		}
		
		else
		{
			for( var i = 0; i<document.forms[0].freightCheck.length; i++ )
		  	{
		  		if( document.forms[0].freightCheck[i].checked ) 
		  		{
			  		if( document.forms[0].freightQuantity[i].value == "" )
		  			{
		  				document.forms[0].freightQuantity[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].freightQuantity[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.freight.numericvalue"/>" );
			 			document.forms[0].freightQuantity[i].value = "";
			 			document.forms[0].freightQuantity[i].focus();
						return false;
			 		}
			 		
			 		if( document.forms[0].freightProformamargin[i].value == "" )
		  			{
		  				document.forms[0].freightProformamargin[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].freightProformamargin[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.freight.numericvalue"/>" );
			 			document.forms[0].freightProformamargin[i].value = "";
			 			document.forms[0].freightProformamargin[i].focus();
						return false;
			 		}	
		  		}
		  	}
		}
	}
	
return submitflag;	
	
}



function sortwindow()
{
	str = 'SortAction.do?addendum_id=<bean:write name = "ResourceListForm" property = "addendum_id" />&Type=<bean:write name = "ResourceListForm" property = "type"/>&Activitylibrary_Id=<bean:write name = "ResourceListForm" property = "activity_Id"/>&ref=<bean:write name = "ResourceListForm" property = "chkaddendum" />';
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );  */
}

function opensearchwindowmaterial()
{
	window.location = "OpenSearchWindowTempAction.do?from=am&type=Material&activity_Id=<bean:write name = "ResourceListForm" property = "activity_Id" />&resourceListType=M";
	//suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600, height = 400 , resizable = yes , scrollbars = yes' );
	//suppstrwin.focus();

}

function opensearchwindowlabor()
{
	window.location = "OpenSearchWindowTempAction.do?from=am&type=Labor&activity_Id=<bean:write name = "ResourceListForm" property = "activity_Id" />&resourceListType=L";
	//suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600, height = 400 , resizable = yes , scrollbars = yes' );
	//suppstrwin.focus();
}

function opensearchwindowtravel()
{
	window.location = "OpenSearchWindowTempAction.do?from=am&type=Travel&activity_Id=<bean:write name = "ResourceListForm" property = "activity_Id" />&resourceListType=T";
	//suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600, height = 400 , resizable = yes , scrollbars = yes' );
	//suppstrwin.focus();
}

function opensearchwindowfreight()
{
	window.location = "OpenSearchWindowTempAction.do?from=am&type=Freight&activity_Id=<bean:write name = "ResourceListForm" property = "activity_Id" />&resourceListType=F";
	//suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600, height = 400 , resizable = yes , scrollbars = yes' );
	//suppstrwin.focus();
}

function marginComputationLabor(resourceId, estUnitCost, position, size)
{
		str = "MarginComputationAction.do?size=0&resourceId="+resourceId+"&estUnitCost="+estUnitCost+"&position="+position+"&labor_resource_size="+size;	
		str = str.replace("#", "*!*");
		suppstrwin = window.open(str, '', 'left = 300 , top = 250 , width = 320, height = 240 , resizable = yes , scrollbars = yes')
		suppstrwin.focus();
}
function marginComputationMaterial(resourceId, estUnitCost, position, size)
{
		str = "MarginComputationAction.do?size=0resourceId="+resourceId+"&estUnitCost="+estUnitCost+"&position="+position+"&material_resource_size="+size;	
		str = str.replace("#", "*!*");
		suppstrwin = window.open(str, '', 'left = 300 , top = 250 , width = 320, height = 240 , resizable = yes , scrollbars = yes')
		suppstrwin.focus();
}
function marginComputationFreight(resourceId, estUnitCost, position, size)
{
		str = "MarginComputationAction.do?size=0resourceId="+resourceId+"&estUnitCost="+estUnitCost+"&position="+position+"&freight_resource_size="+size;	
		str = str.replace("#", "*!*");
		suppstrwin = window.open(str, '', 'left = 300 , top = 250 , width = 320, height = 240 , resizable = yes , scrollbars = yes')
		suppstrwin.focus();
}
function marginComputationTravel(resourceId, estUnitCost, position, size)
{
		str = "MarginComputationAction.do?size=0resourceId="+resourceId+"&estUnitCost="+estUnitCost+"&position="+position+"&travel_resource_size="+size;	
		str = str.replace("#", "*!*");
		suppstrwin = window.open(str, '', 'left = 300 , top = 250 , width = 320, height = 240 , resizable = yes , scrollbars = yes')
		suppstrwin.focus();
}

//-->
</script>

<script>
function materialcalarr( i )
{
	var unitCost = '';
	if( <%=material_Size%> == 1 )
	{
 		if( ( document.forms[0].materialQuantity.value == "" ) || ( document.forms[0].materialQuantity.value == "null" ) )
		{ 	
			document.forms[0].materialQuantity.value = "0.0";
		}
		
		if( isFloat( document.forms[0].materialQuantity.value ) ) 
		{ 
			if( parseFloat( document.forms[0].materialQuantity.value ) <  parseFloat( document.forms[0].materialMinimumquantity.value ) )
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
				document.forms[0].materialQuantity.value = document.forms[0].materialMinimumquantity.value;
			}
			
			var divisor =  ( parseFloat( document.forms[0].materialQuantity.value )  ) /  parseFloat( document.forms[0].materialSellablequantity.value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].materialQuantity.value ) - (divisor * parseFloat( document.forms[0].materialSellablequantity.value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
				document.forms[0].materialQuantity.value = (parseFloat( document.forms[0].materialSellablequantity.value )*(divisor + 1)).toFixed(4) ; 
			}
			
			unitCost = document.forms[0].materialEstimatedunitcost.value;
			document.forms[0].materialEstimatedtotalcost.value = parseFloat( document.forms[0].materialQuantity.value) * parseFloat(replaceAllCommaWithBlankspace(unitCost));
			document.forms[0].materialEstimatedtotalcost.value = round_func( document.forms[0].materialEstimatedtotalcost.value );
	
			if( ( document.forms[0].materialProformamargin.value == "" ) || ( document.forms[0].materialProformamargin.value == "null" ) || ( document.forms[0].materialProformamargin.value >= "1" ) )
			{ 
				document.forms[0].materialProformamargin.value = "0.0";
			}
		
			if( isFloat( document.forms[0].materialProformamargin.value ) ) 
			{ 
				document.forms[0].materialPriceunit.value = parseFloat( document.forms[0].materialEstimatedunitcost.value ) / ( 1 - parseFloat( document.forms[0].materialProformamargin.value ) );
				document.forms[0].materialPriceunit.value = round_func( document.forms[0].materialPriceunit.value );
			}
			
			if( isFloat( document.forms[0].materialQuantity.value ) ) 
			{ 
				document.forms[0].materialPriceextended.value = parseFloat( document.forms[0].materialPriceunit.value ) * parseFloat( document.forms[0].materialQuantity.value );
				document.forms[0].materialPriceextended.value = round_func( document.forms[0].materialPriceextended.value );
			}
		}
		
	}
	
	else
	{
		if( ( document.forms[0].materialQuantity[i].value == "" ) || ( document.forms[0].materialQuantity[i].value == "null" ) )
		{ 	
			document.forms[0].materialQuantity[i].value = "0.0";
		}
		
		if( isFloat( document.forms[0].materialQuantity[i].value ) ) 
		{ 
			if( parseFloat( document.forms[0].materialQuantity[i].value ) <  parseFloat( document.forms[0].materialMinimumquantity[i].value ) )
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
				document.forms[0].materialQuantity[i].value = document.forms[0].materialMinimumquantity[i].value;
			}
			
			var divisor =  ( parseFloat( document.forms[0].materialQuantity[i].value )  ) /  parseFloat( document.forms[0].materialSellablequantity[i].value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].materialQuantity[i].value ) - (divisor * parseFloat( document.forms[0].materialSellablequantity[i].value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
				document.forms[0].materialQuantity[i].value = (parseFloat( document.forms[0].materialSellablequantity[i].value )*(divisor + 1)).toFixed(4) ; 
			}
			
			unitCost = document.forms[0].materialEstimatedunitcost[i].value;
			document.forms[0].materialEstimatedtotalcost[i].value = parseFloat( document.forms[0].materialQuantity[i].value ) * parseFloat( replaceAllCommaWithBlankspace(unitCost) );
			document.forms[0].materialEstimatedtotalcost[i].value = round_func( document.forms[0].materialEstimatedtotalcost[i].value );
			
			
			if( ( document.forms[0].materialProformamargin[i].value == "0.0" ) || ( document.forms[0].materialProformamargin[i].value == "null" ) || ( document.forms[0].materialProformamargin[i].value >= "1" ) )
			{ 
				document.forms[0].materialProformamargin[i].value = "0.0";
			}

		
			if( isFloat( document.forms[0].materialProformamargin[i].value ) ) 
			{ 
				document.forms[0].materialPriceunit[i].value = parseFloat( document.forms[0].materialEstimatedunitcost[i].value ) / ( 1 - parseFloat( document.forms[0].materialProformamargin[i].value ) );
				document.forms[0].materialPriceunit[i].value = round_func( document.forms[0].materialPriceunit[i].value );
			}
			
			if( isFloat( document.forms[0].materialQuantity[i].value ) ) 
			{ 
				document.forms[0].materialPriceextended[i].value = parseFloat( document.forms[0].materialPriceunit[i].value ) * parseFloat( document.forms[0].materialQuantity[i].value );
				document.forms[0].materialPriceextended[i].value = round_func( document.forms[0].materialPriceextended[i].value );
			}
			
		
		}
	}
}

function laborcalarr( j ){
	var unitCost = '';
	
	if(document.forms[0].checknetmedx.value!=null && document.forms[0].checknetmedx.value=="Y")
	{
		if( <%=labor_Size%> == 1 )
		{
			if( ( document.forms[0].laborQuantityhours.value == "" ) || ( document.forms[0].laborQuantityhours.value == "null" ) )
			{ 
				document.forms[0].laborQuantityhours.value = "0.0";
			}
			
			if( isFloat( document.forms[0].laborQuantityhours.value ) ) 
			{ 
							
				document.forms[0].laborEstimatedtotalcost.value = parseFloat( document.forms[0].laborQuantityhours.value ) * parseFloat( document.forms[0].laborEstimatedhourlybasecost.value );
				document.forms[0].laborEstimatedtotalcost.value = round_func( document.forms[0].laborEstimatedtotalcost.value );
			
				if( ( document.forms[0].laborProformamargin.value == "" ) || ( document.forms[0].laborProformamargin.value == "null" ) || ( document.forms[0].laborProformamargin.value >= "1" ) )
				{ 
					document.forms[0].laborProformamargin.value = "0.0";
				}
			
				if( isFloat( document.forms[0].laborProformamargin.value ) ) 
				{ 
					document.forms[0].laborPriceunit.value = round_func( document.forms[0].laborPriceunit.value );
				}
				
				if( isFloat( document.forms[0].laborQuantityhours.value ) ) 
				{ 
					document.forms[0].laborPriceextended.value = parseFloat( document.forms[0].laborPriceunit.value ) * parseFloat( document.forms[0].laborQuantityhours.value );
					document.forms[0].laborPriceextended.value = round_func( document.forms[0].laborPriceextended.value );
				}
			}
		}
		
		else
		{
			if( ( document.forms[0].laborQuantityhours[j].value == "" ) || ( document.forms[0].laborQuantityhours[j].value == "null" ) )
			{ 
				document.forms[0].laborQuantityhours[j].value = "0.0";
			}
			
			if( isFloat( document.forms[0].laborQuantityhours[j].value ) ) 
			{ 
				document.forms[0].laborEstimatedtotalcost[j].value = parseFloat( document.forms[0].laborQuantityhours[j].value ) * parseFloat( document.forms[0].laborEstimatedhourlybasecost[j].value );
				document.forms[0].laborEstimatedtotalcost[j].value = round_func( document.forms[0].laborEstimatedtotalcost[j].value );
				
				if( ( document.forms[0].laborProformamargin[j].value == "" ) || ( document.forms[0].laborProformamargin[j].value == "null" ) || ( document.forms[0].laborProformamargin[j].value >= "1" ) )
				{ 
					document.forms[0].laborProformamargin[j].value = "0.0";
				}
			
				if( isFloat( document.forms[0].laborProformamargin[j].value ) ) 
				{ 
					document.forms[0].laborPriceunit[j].value = round_func( document.forms[0].laborPriceunit[j].value );
				}
				
				if( isFloat( document.forms[0].laborQuantityhours[j].value ) ) 
				{ 
					document.forms[0].laborPriceextended[j].value = parseFloat( document.forms[0].laborPriceunit[j].value ) * parseFloat( document.forms[0].laborQuantityhours[j].value );
					document.forms[0].laborPriceextended[j].value = round_func( document.forms[0].laborPriceextended[j].value );
				}
			}
		}
	}
	else
	{
		if( <%=labor_Size%> == 1 )
		{
			if( ( document.forms[0].laborQuantityhours.value == "" ) || ( document.forms[0].laborQuantityhours.value == "null" ) )
			{ 
				document.forms[0].laborQuantityhours.value = "0.0";
			}
			
			if( isFloat( document.forms[0].laborQuantityhours.value ) ) 
			{ 
				if( parseFloat( document.forms[0].laborQuantityhours.value ) <  parseFloat( document.forms[0].laborMinimumquantity.value ) )
				{
					alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
					document.forms[0].laborQuantityhours.value = document.forms[0].laborMinimumquantity.value;
				}
				
				var divisor =  ( parseFloat( document.forms[0].laborQuantityhours.value )  ) /  parseFloat( document.forms[0].laborSellablequantity.value );
				divisor = parseInt(divisor.toFixed(4));
				var remainder = parseFloat( document.forms[0].laborQuantityhours.value ) - (divisor * parseFloat( document.forms[0].laborSellablequantity.value ) );
				var zero=0;
				
				if(  parseInt(remainder.toFixed(4)*10000) == zero )
				{
				}
				else
				{
					alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
					document.forms[0].laborQuantityhours.value = (parseFloat( document.forms[0].laborSellablequantity.value )*(divisor + 1)).toFixed(4) ; 
				}

				unitCost = document.forms[0].laborEstimatedhourlybasecost.value; 
				document.forms[0].laborEstimatedtotalcost.value = parseFloat( document.forms[0].laborQuantityhours.value ) * parseFloat( replaceAllCommaWithBlankspace(unitCost) );
				document.forms[0].laborEstimatedtotalcost.value = round_func( document.forms[0].laborEstimatedtotalcost.value );
			
				if( ( document.forms[0].laborProformamargin.value == "" ) || ( document.forms[0].laborProformamargin.value == "null" ) || ( document.forms[0].laborProformamargin.value >= "1" ) )
				{ 
					document.forms[0].laborProformamargin.value = "0.0";
				}
			
				if( isFloat( document.forms[0].laborProformamargin.value ) ) 
				{ 
					document.forms[0].laborPriceunit.value = parseFloat( document.forms[0].laborEstimatedhourlybasecost.value ) / ( 1 - parseFloat( document.forms[0].laborProformamargin.value ) );
					document.forms[0].laborPriceunit.value = round_func( document.forms[0].laborPriceunit.value );
				}
				
				if( isFloat( document.forms[0].laborQuantityhours.value ) ) 
				{ 
					document.forms[0].laborPriceextended.value = parseFloat( document.forms[0].laborPriceunit.value ) * parseFloat( document.forms[0].laborQuantityhours.value );
					document.forms[0].laborPriceextended.value = round_func( document.forms[0].laborPriceextended.value );
				}
			}
		}
		
		else
		{
			
			if( ( document.forms[0].laborQuantityhours[j].value == "" ) || ( document.forms[0].laborQuantityhours[j].value == "null" ) )
			{ 
				document.forms[0].laborQuantityhours[j].value = "0.0";
			}
			
			if( isFloat( document.forms[0].laborQuantityhours[j].value ) ) 
			{ 
				if( parseFloat( document.forms[0].laborQuantityhours[j].value ) <  parseFloat( document.forms[0].laborMinimumquantity[j].value ) )
				{
					alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
					document.forms[0].laborQuantityhours[j].value = document.forms[0].laborMinimumquantity[j].value;
				}
			
				var divisor =  ( parseFloat( document.forms[0].laborQuantityhours[j].value )  ) /  parseFloat( document.forms[0].laborSellablequantity[j].value );
				divisor = parseInt(divisor.toFixed(4));
				var remainder = parseFloat( document.forms[0].laborQuantityhours[j].value ) - (divisor * parseFloat( document.forms[0].laborSellablequantity[j].value ) );
				var zero=0;
				if(  parseInt(remainder.toFixed(4)*10000) == zero )
				{
				}
				else
				{
					alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
					document.forms[0].laborQuantityhours[j].value = (parseFloat( document.forms[0].laborSellablequantity[j].value )*(divisor + 1)).toFixed(4) ; 
				}

				unitCost = document.forms[0].laborEstimatedhourlybasecost[j].value;
				document.forms[0].laborEstimatedtotalcost[j].value = parseFloat( document.forms[0].laborQuantityhours[j].value ) * parseFloat( replaceAllCommaWithBlankspace(unitCost) );
				document.forms[0].laborEstimatedtotalcost[j].value = round_func( document.forms[0].laborEstimatedtotalcost[j].value );
			
		
			
				
				if( ( document.forms[0].laborProformamargin[j].value == "" ) || ( document.forms[0].laborProformamargin[j].value == "null" ) || ( document.forms[0].laborProformamargin[j].value >= "1" ) )
				{ 
					document.forms[0].laborProformamargin[j].value = "0.0";
				}
			
				if( isFloat( document.forms[0].laborProformamargin[j].value ) ) 
				{ 
					document.forms[0].laborPriceunit[j].value = parseFloat( document.forms[0].laborEstimatedhourlybasecost[j].value ) / ( 1 - parseFloat( document.forms[0].laborProformamargin[j].value ) );
					document.forms[0].laborPriceunit[j].value = round_func( document.forms[0].laborPriceunit[j].value );
				}
				
				if( isFloat( document.forms[0].laborQuantityhours[j].value ) ) 
				{ 
					document.forms[0].laborPriceextended[j].value = parseFloat( document.forms[0].laborPriceunit[j].value ) * parseFloat( document.forms[0].laborQuantityhours[j].value );
					document.forms[0].laborPriceextended[j].value = round_func( document.forms[0].laborPriceextended[j].value );
				}
			}
		}
	}
}


function caltransarr( j )
{
	
	if( <%=labor_Size%> != 0 )
	{
		if( <%=labor_Size%> == 1 )
		{
			
		}
		else
		{
		  for( var p = 0; p < <%=labor_Size%>; p++ )
		     {
	  			if(j!=p)
	  			{
			  					  		
			  		if(document.forms[0].laborTransit[j].value == 'N') {
				  		if((document.forms[0].laborname[j].value == document.forms[0].laborname[p].value) && (document.forms[0].laborTransit[p].value == 'Y')) 
				  		{
				  		    alert( "<bean:message bundle = "AM" key = "am.labor.sameresourcecannot"/>" );
				  			return false;	
				  		}
				  		
			  		}
			  		
			  		if(document.forms[0].laborTransit[j].value == 'Y') {
				  		if((document.forms[0].laborname[j].value == document.forms[0].laborname[p].value) && (document.forms[0].laborTransit[p].value == 'N')) 
				  		{
				  		    alert( "<bean:message bundle = "AM" key = "am.labor.sameresourcecannot"/>" );
				  			return false;	
				  		}
			  		}
			  	}
		  	}
		  
		}
	}
	
	
	
	if( j == null)
	{
		
		
		if(document.forms[0].laborTransit.value=='N')
		{
			document.forms[0].laborTransit.value='Y';
		}
		else
		{
			document.forms[0].laborTransit.value='N';
		}
	}
	
	else
	{
		
		if(document.forms[0].laborTransit[j].value=='N')
		{
			
			document.forms[0].laborTransit[j].value='Y';
			
		}
		else
		{
			
			document.forms[0].laborTransit[j].value='N';
			
		}	
	}
	
}


function travelcalarr( l )
{
	var unitCost  = '';
	if( <%=travel_Size%> == 1 )
	{
		if( ( document.forms[0].travelQuantity.value == "" ) || ( document.forms[0].travelQuantity.value == "null" ) )
		{ 
			document.forms[0].travelQuantity.value = "0.0";
		}
		
		if( isFloat( document.forms[0].travelQuantity.value ) ) 
		{ 
			if( parseFloat( document.forms[0].travelQuantity.value ) <  parseFloat( document.forms[0].travelMinimumquantity.value ) )
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
				document.forms[0].travelQuantity.value = document.forms[0].travelMinimumquantity.value;
			}
			
			var divisor =  ( parseFloat( document.forms[0].travelQuantity.value )  ) /  parseFloat( document.forms[0].travelSellablequantity.value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].travelQuantity.value ) - (divisor * parseFloat( document.forms[0].travelSellablequantity.value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
				document.forms[0].travelQuantity.value = (parseFloat( document.forms[0].travelSellablequantity.value )*(divisor + 1)).toFixed(4) ; 
			}
			
			
			unitCost = document.forms[0].travelEstimatedunitcost.value;
			document.forms[0].travelEstimatedtotalcost.value = parseFloat( document.forms[0].travelQuantity.value ) * parseFloat( replaceAllCommaWithBlankspace(unitCost) );
			document.forms[0].travelEstimatedtotalcost.value = round_func( document.forms[0].travelEstimatedtotalcost.value );

			if( ( document.forms[0].travelProformamargin.value == "" ) || ( document.forms[0].travelProformamargin.value == "null" ) || ( document.forms[0].travelProformamargin.value >= "1" ) )
			{ 
				document.forms[0].travelProformamargin.value = "0.0";
			}
		
			if( isFloat( document.forms[0].travelProformamargin.value ) ) 
			{ 
				document.forms[0].travelPriceunit.value = parseFloat( document.forms[0].travelEstimatedunitcost.value ) / ( 1 - parseFloat( document.forms[0].travelProformamargin.value ) );
				document.forms[0].travelPriceunit.value = round_func( document.forms[0].travelPriceunit.value );
			}
			
			if( isFloat( document.forms[0].travelQuantity.value ) ) 
			{ 
				document.forms[0].travelPriceextended.value = parseFloat( document.forms[0].travelPriceunit.value ) * parseFloat( document.forms[0].travelQuantity.value );
				document.forms[0].travelPriceextended.value = round_func( document.forms[0].travelPriceextended.value );
			}
		}
	}
	
	
	else
	{
		if( ( document.forms[0].travelQuantity[l].value == "" ) || ( document.forms[0].travelQuantity[l].value == "null" ) )
		{ 
			document.forms[0].travelQuantity[l].value = "0.0";
		}
		
		if( isFloat( document.forms[0].travelQuantity[l].value ) ) 
		{ 
			if( parseFloat( document.forms[0].travelQuantity[l].value ) <  parseFloat( document.forms[0].travelMinimumquantity[l].value ) )
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
				document.forms[0].travelQuantity[l].value = document.forms[0].travelMinimumquantity[l].value;
			}
			
			var divisor =  ( parseFloat( document.forms[0].travelQuantity[l].value )  ) /  parseFloat( document.forms[0].travelSellablequantity[l].value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].travelQuantity[l].value ) - (divisor * parseFloat( document.forms[0].travelSellablequantity[l].value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
				document.forms[0].travelQuantity[l].value = (parseFloat( document.forms[0].travelSellablequantity[l].value )*(divisor + 1)).toFixed(4) ; 
			}
			unitCost = document.forms[0].travelEstimatedunitcost[l].value;
			document.forms[0].travelEstimatedtotalcost[l].value = parseFloat( document.forms[0].travelQuantity[l].value ) * parseFloat(replaceAllCommaWithBlankspace(unitCost));
			document.forms[0].travelEstimatedtotalcost[l].value = round_func( document.forms[0].travelEstimatedtotalcost[l].value );
		
			if( ( document.forms[0].travelProformamargin[l].value == "" ) || ( document.forms[0].travelProformamargin[l].value == "null" ) || ( document.forms[0].travelProformamargin[l].value >= "1" ) )
			{ 
				document.forms[0].travelProformamargin[l].value = "0.0";
			}
		
			if( isFloat( document.forms[0].travelProformamargin[l].value ) ) 
			{ 
				document.forms[0].travelPriceunit[l].value = parseFloat( document.forms[0].travelEstimatedunitcost[l].value ) / ( 1 - parseFloat( document.forms[0].travelProformamargin[l].value ) );
				document.forms[0].travelPriceunit[l].value = round_func( document.forms[0].travelPriceunit[l].value );
			}
			
			if( isFloat( document.forms[0].travelQuantity[l].value ) ) 
			{ 
				document.forms[0].travelPriceextended[l].value = parseFloat( document.forms[0].travelPriceunit[l].value ) * parseFloat( document.forms[0].travelQuantity[l].value );
				document.forms[0].travelPriceextended[l].value = round_func( document.forms[0].travelPriceextended[l].value );
			}
		}
	}
}



function calarr( k )
{
	var unitCost = '';
	if( <%=freight_Size%> == 1 )
	{
		if( ( document.forms[0].freightQuantity.value == "" ) || ( document.forms[0].freightQuantity.value == "null" ) )
		{ 
			document.forms[0].freightQuantity.value = "0.0";
		}
		
		if( isFloat( document.forms[0].freightQuantity.value ) ) 
		{ 
			if( parseFloat( document.forms[0].freightQuantity.value ) <  parseFloat( document.forms[0].freightMinimumquantity.value ) )
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
				document.forms[0].freightQuantity.value = document.forms[0].freightMinimumquantity.value;
			}
			
			var divisor =  ( parseFloat( document.forms[0].freightQuantity.value )  ) /  parseFloat( document.forms[0].freightSellablequantity.value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].freightQuantity.value ) - (divisor * parseFloat( document.forms[0].freightSellablequantity.value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
				document.forms[0].freightQuantity.value = (parseFloat( document.forms[0].freightSellablequantity.value )*(divisor + 1)).toFixed(4) ;
			}

			unitCost = document.forms[0].freightEstimatedunitcost.value;
			document.forms[0].freightEstimatedtotalcost.value = parseFloat( document.forms[0].freightQuantity.value ) * parseFloat(replaceAllCommaWithBlankspace(unitCost));
			document.forms[0].freightEstimatedtotalcost.value = round_func( document.forms[0].freightEstimatedtotalcost.value );
		
			if( ( document.forms[0].freightProformamargin.value == "" ) || ( document.forms[0].freightProformamargin.value == "null" ) || ( document.forms[0].freightProformamargin.value >= "1" ) )
			{ 
				document.forms[0].freightProformamargin.value = "0.0";
			}
		
			if( isFloat( document.forms[0].freightProformamargin.value ) ) 
			{ 
				document.forms[0].freightPriceunit.value = parseFloat( document.forms[0].freightEstimatedunitcost.value ) / ( 1 - parseFloat( document.forms[0].freightProformamargin.value ) );
				document.forms[0].freightPriceunit.value = round_func( document.forms[0].freightPriceunit.value );
			}
			
			if( isFloat( document.forms[0].freightQuantity.value ) ) 
			{ 
				document.forms[0].freightPriceextended.value = parseFloat( document.forms[0].freightPriceunit.value ) * parseFloat( document.forms[0].freightQuantity.value );
				document.forms[0].freightPriceextended.value = round_func( document.forms[0].freightPriceextended.value );
			}
		}
	}
	
	else
	{
		if( ( document.forms[0].freightQuantity[k].value == "" ) || ( document.forms[0].freightQuantity[k].value == "null" ) )
		{ 
			document.forms[0].freightQuantity[k].value = "0.0";
		}
		
		if( isFloat( document.forms[0].freightQuantity[k].value ) ) 
		{ 
			if( parseFloat( document.forms[0].freightQuantity[k].value ) <  parseFloat( document.forms[0].freightMinimumquantity[k].value ) )
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjustedagain"/>" );
				document.forms[0].freightQuantity[k].value = document.forms[0].freightMinimumquantity[k].value;
			}
			
			var divisor =  ( parseFloat( document.forms[0].freightQuantity[k].value )  ) /  parseFloat( document.forms[0].freightSellablequantity[k].value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].freightQuantity[k].value ) - (divisor * parseFloat( document.forms[0].freightSellablequantity[k].value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "pm" key = "pm.quantityadjusted"/>" );
				document.forms[0].freightQuantity[k].value = (parseFloat( document.forms[0].freightSellablequantity[k].value )*(divisor + 1)).toFixed(4) ;
			}
			unitCost = document.forms[0].freightEstimatedunitcost[k].value;
			document.forms[0].freightEstimatedtotalcost[k].value = parseFloat( document.forms[0].freightQuantity[k].value ) * parseFloat( replaceAllCommaWithBlankspace(unitCost) );
			document.forms[0].freightEstimatedtotalcost[k].value = round_func( document.forms[0].freightEstimatedtotalcost[k].value );
			
			if( ( document.forms[0].freightProformamargin[k].value == "" ) || ( document.forms[0].freightProformamargin[k].value == "null" ) || ( document.forms[0].freightProformamargin[k].value >= "1" ) )
			{ 
				document.forms[0].freightProformamargin[k].value = "0.0";
			}
		
			if( isFloat( document.forms[0].freightProformamargin[k].value ) ) 
			{ 
				document.forms[0].freightPriceunit[k].value = parseFloat( document.forms[0].freightEstimatedunitcost[k].value ) / ( 1 - parseFloat( document.forms[0].freightProformamargin[k].value ) );
				document.forms[0].freightPriceunit[k].value = round_func( document.forms[0].freightPriceunit[k].value );
			}
			
			if( isFloat( document.forms[0].freightQuantity[k].value ) ) 
			{ 
				document.forms[0].freightPriceextended[k].value = parseFloat( document.forms[0].freightPriceunit[k].value ) * parseFloat( document.forms[0].freightQuantity[k].value );
				document.forms[0].freightPriceextended[k].value = round_func( document.forms[0].freightPriceextended[k].value );
			}
		}
	}
}




</script>


<script><!--
function ShowDiv()
{
	leftAdjLayers();
	
	document.getElementById("filter").style.visibility="visible";
	return false;
}

function hideDiv()
{
	document.getElementById("filter").style.visibility="hidden";
	return false;
}

function leftAdjLayers(){
		var leftAdj=0;
		leftAdj =document.body.clientWidth;
		if( <%=material_Size%> > 0 && <%=labor_Size%> > 0 && <%=travel_Size%> > 0 && <%=freight_Size%> > 0 ){
	 	leftAdj = leftAdj -60;
	 	}
	 	else{ 
		 	leftAdj = leftAdj -200;
		 }
		
		//document.getElementById("filter").style.left=leftAdj;
		//alert(document.getElementById("top").pixel);
}
function replaceAllCommaWithBlankspace(stringValue) {
	var strLen = stringValue.length;
	var index = -1;
	if(strLen > 0) {
		index = stringValue.indexOf(',');
		while(index > 0) {
			stringValue = stringValue.substring(0,index) +''+ stringValue.substring(index+1,strLen);
			index = stringValue.indexOf(',');
		}
	}
	return stringValue;
}
</script>

