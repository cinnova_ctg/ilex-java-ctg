<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<% // String MSA_Id = "";
	int masterlib_size = 0;
	
	if(request.getAttribute( "Size" ) != null) {
		masterlib_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() ); 
	} 
	if(request.getAttribute( "Size" ) != null) {
		masterlib_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() ); 
	} 
	%>
	
	
	

<%@page import="org.apache.log4j.Category"%>
<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.tabular.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link rel = "stylesheet" href = "docm/styles/style.css" type = "text/css">
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "javaScript" src = "javascript/JLibrary.js"></script>
	
</head>

<%
	String backgroundclass = "texto";
	String backgroundhyperlinkclass = null;
	String displaycombo = null;
	String readonlyclass = null;
	String readonlynumberclass = null;
	int i = 0;
	String comboclass = null;
	boolean csschooser = true;
	String schdayscheck = null;
	String valstr = null;
%>

<body>
<%@ include file="/NMenu.inc" %>
<html:form action = "/MastertolibcopyAction">
<html:hidden property = "msa_Id" name='MastertolibcopyForm'/>
<%-- <html:form action="NetMedXDashboardAction"> 
<html:hidden property="appendixid" />
<html:hidden property="viewjobtype" />
<html:hidden property="nettype" />
<html:hidden property="ownerId" />
<html:hidden property="from_type" />
<html:hidden property="ownerId" />
<html:hidden property="jobOwnerOtherCheck"/>
<html:hidden property="jobid"/> --%>
<bean:define id="MSA_Id" property = "msa_Id" name='MastertolibcopyForm'></bean:define>
<div id="menunav">
	    <ul>
	  		<li>
	        	<a class="drop" href="#"><span>Activity</span></a>
	  			<ul>
	  				<li><a href="ManageActivityAction.do?MSA_Id=<bean:write name='MastertolibcopyForm' property='msa_Id' />&amp;ref=addActivity">Add</a></li>
	 				<li><a href="ManageActivityAction.do?MSA_Id=<bean:write name='MastertolibcopyForm' property='msa_Id' />&amp;ref=manageActivity">Manage</a></li>
	 				<li><a href="ActivitySummaryAction.do?ref=View&amp;MSA_Id=<bean:write name='MastertolibcopyForm' property='msa_Id' />">View</a></li>
	 				
	 					<li><a href="MastertolibcopyAction.do?ref=showAllMSA&amp;MSA_Id=<bean:write name='MastertolibcopyForm' property='msa_Id' />">Copy</a></li>
	 					
	  			</ul>
	  		</li>
	  		<li>
	        	<a class="drop" href="#"><span>Partner Information</span></a>
	  		
	  			
					<ul>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<bean:write name='MastertolibcopyForm' property='msa_Id' />&viewType=sow&fromType=am_act">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<bean:write name='MastertolibcopyForm' property='msa_Id' />&viewType=assumption&fromType=am_act">Manage Assumptions</a></li>
					<%-- 	<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li> --%>
        			</ul>
        		
	  		
	  		</li>
		</ul>
	</div>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr><td height="21" background="images/content_head_04.jpg" colspan="7">
			         <div id="breadCrumb">
			         	<a href="#" class="bgNone">Activity Manager</a>
			         	<a href="ActivitySummaryAction.do?ref=View&MSA_Id=<bean:write name='MastertolibcopyForm' property='msa_Id' />" background="none;"><bean:write name = "msaname" scope = "request"/></a>
						 <a><span class="breadCrumb1">Copy Activity</span></a></div>
								</div>		
			    </div></td>
			</tr>
	       
		</tbody>
		</table>	     
		        



<%-- <table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	<tr>
    	<td class = "toprow"> 
	    	<table  border = "0" cellspacing = "0" cellpadding = "0" height = "18">
		       <tr>
		       		<td id="pop1" width="120" height="19" class="Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu1',event)" class = "menufont" style="width: 120px">Activity</a></td>
		       		<td id="pop2" width="120" height="19" class="Ntoprow1" align="center"><a href = "#" onMouseOut = "MM_swapImgRestore(); popDown('elMenu2',event)" onMouseOver = "MM_swapImage('Image24','','images/services1b.gif',1);popUp('elMenu2',event)" class = "menufont" style="width: 120px"><bean:message bundle = "AM" key = "am.summary.Partnerinfo"/></a></td>
		       		<td id ="pop3" width ="840" class ="Ntoprow1">&nbsp;</td>
		       </tr>
		        <tr>
		        	<td background="images/content_head_04.jpg" height="21" colspan=3>
			         <div id="breadCrumb">
			         	<a href="#" class="bgNone">Activity Manager</a>
			         	<a href="ActivitySummaryAction.do?ref=View&MSA_Id=<bean:write name='MastertolibcopyForm' property='msa_Id' />" background="none;"><bean:write name = "msaname" scope = "request"/></a>
						 <a><span class="breadCrumb1">Copy Activity</span></a></div>
			    	</td>
				</tr>
		        <tr><td height="8"></td></tr>	
	      	</table>
    	</td>
	</tr>
</table>
 --%>
<%@include  file = "/ActivityManagerMenuScript.inc"%>
<table border="0" cellspacing = "1" cellpadding = "1">
	<tr>
		<td class="labeleboldwhite" style="padding-left: 24px;">Select Library:&nbsp;</td>
		<td class="labeleboldwhite">
			 <html:select property = "selectedMSAId" size = "1" styleClass = "select" onchange="getActivityList(this);">
		    	<html:optionsCollection  property = "msaList" value = "value" label = "label"/> 
	   		</html:select>
		</td>
	</tr>
	<tr>
		<td height="3"></td>
	</tr>
</table>

<logic:present name="result">

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
 <tr>
   	<td>
  		<table border = "0" cellspacing = "1" cellpadding = "1">		
<%-- 
		<tr>
			<td>&nbsp; </td>
    		<td class = "labeleboldwhite" colspan = "13" height = "30"><bean:message bundle = "AM" key = "am.masterlib.header"/> <bean:write name = "msaname" scope = "request" />  
    		</td>
		</tr>
  --%>
  		
  

  
  <logic:present name = "masterliblist" scope = "request" > 
  
  	<% 
  		String previousCategory = "";
  		int divCheck=0;
  	%>
  
 	
 	<logic:iterate id = "ms" name = "masterliblist">
	
		<bean:define id = "val" name = "ms" property = "activity_Id" type = "java.lang.String"/>
		<bean:define id = "currentCategory" name = "ms" property = "category_type" type = "java.lang.String"/>
		<% if(!previousCategory.equals(currentCategory)) { 
			previousCategory = currentCategory;
			csschooser = false;
			if(i != 0) { %>
				</table></td></tr>
			</table></td></tr>		
		<%	}
		%>
		<tr>
		<td valign="top"><a href="javascript:toggleMenuSection('<%=currentCategory %>');"><img id='img_<%=currentCategory %>' src="images/Expand.gif" border="0" alt="+"  /></a></td>
		<td colspan=13><table border="0" cellspacing="0" cellpadding="0" width="700">
		<tr>
    		<td class = "Nhyperoddfont" colspan = "14" style="text-align: left;" height="10">
    			<B><bean:write name = "ms" property = "category_type" /></B>  
    		</td>
    	</tr>
    	<tr>
    		<td colspan = "14" height = "1">
    		</td>
		</tr>
		<tr>
		<td colspan=14>
		<table width="100%" id = 'div_<%=currentCategory %>'  style="display: none" border="0" cellspacing="1">
    	
		<tr> 
			<td></td>
    		<td class = "Nhyperoddfont"  align="center" colspan=2 rowspan = "2">
    			<B><bean:message bundle = "AM" key = "am.tabular.name"/></B>
    		</td>
   
			<td class = "Nhyperoddfont" align="center" rowspan = "2">
				<B><bean:message bundle = "AM" key = "am.tabular.type"/></B>
			</td>
			<td class = "Nhyperoddfont" align="center" rowspan = "2">
				<B><bean:message bundle = "AM" key = "am.tabular.quantity"/></B>
			</td>
		    <td class = "Nhyperoddfont" align="center" colspan = "6">
		    	<B><bean:message bundle = "AM" key = "am.tabular.estimatedcost"/></B>
		    </td>
		    <td class = "Nhyperoddfont" align="center" rowspan = "2">
		    	<B><bean:message bundle = "AM" key = "am.tabular.extendedprice"/></B>
		    </td>
		    <td class = "Nhyperoddfont" align="center" rowspan = "2">
		    	<B><bean:message bundle = "AM" key = "am.tabular.proformamargin"/></B>
		    </td>
		    <td class = "Nhyperoddfont" align="center" rowspan = "2">
		    	<B><bean:message bundle = "AM" key = "am.tabular.status"/></B>
		    </td>
		    
		</tr>

  		<tr> 
  		<td></td>
    		<td class = "Nhyperoddfont" align="center" > 
      			<div align = "center">
      				<B><bean:message bundle = "AM" key = "am.tabular.estimatedmaterialcost"/></B>
      			</div>
    		</td>
    
    		<td class = "Nhyperoddfont" align="center" > 
      			<div align = "center">
      				<B><bean:message bundle = "AM" key = "am.tabular.estimatedcnslabourcost"/></B>
      			</div>
    		</td>
    
    
    		<td class = "Nhyperoddfont" align="center" > 
      			<div align = "center">
      				<B><bean:message bundle = "AM" key = "am.tabular.estimatedcontractlabourcost"/></B>
      			</div>
    		</td>
    
		    <td class = "Nhyperoddfont" align="center" > 
		      <div align = "center" align="center" >
		      	<B><bean:message bundle = "AM" key = "am.tabular.estimatedfreightcost"/></B>
		      </div>
		    </td>
    
		    <td class = "Nhyperoddfont" align="center" > 
		      <div align = "center">
		      	<B><bean:message bundle = "AM" key = "am.tabular.estimatedtravelcost"/></B>
		      </div>
		    </td>
    
		    <td class = "Nhyperoddfont" align="center" > 
		      <div align = "center">
		      	<B><bean:message bundle = "AM" key = "am.tabular.estimatedtotalcost"/></B>
		      </div>
		    </td>
    
		</tr>
		
  		<%} %>
  		<% if(previousCategory.equals(currentCategory)) { 			%>
  			
  			<%	
			if ( csschooser == true ) 
			{
				backgroundclass = "texto";
				backgroundhyperlinkclass = "hyperodd";
				comboclass = "combooddhidden";
				csschooser = false;
				readonlyclass = "readonlytextodd";
				readonlynumberclass = "readonlytextnumberodd";
			}
	
			else
			{
				csschooser = true;	
				comboclass = "comboevenhidden";
				backgroundhyperlinkclass = "hypereven";
				backgroundclass = "texte";
				readonlyclass = "readonlytexteven";
				readonlynumberclass = "readonlytextnumbereven";
			}
			%>
  			<tr>		
			<td></td>
			<td class = "labeleboldwhite"> 
				<html:checkbox property = "check" value = "<%= val %>" />	
    		</td>
			
			<html:hidden name = "ms" property = "activity_Id" />
			
			
			<td class = "<%= backgroundhyperlinkclass %>"> 
				&nbsp;<bean:write name = "ms" property = "name" />&nbsp;
			</td>
			
			<html:hidden name = "ms" property = "name"/>
		
			<td class = "<%= backgroundclass %>">
				&nbsp;<bean:write name = "ms" property = "activitytype" />&nbsp;
				<html:hidden name = "ms" property = "activitytype" />
				<html:hidden name = "ms" property = "activitytypecombo" />
			</td>
			
			<html:hidden name = "ms" property = "category_type" />
			<!-- <td class = "<%= backgroundclass %>">
				<bean:write name = "ms" property = "category_type" />
				
				
			</td>
			 -->
			<html:hidden name = "ms" property = "cat_Id" />
			<html:hidden name = "ms" property = "duration" />
			
			
			<!-- <td class = "<%= backgroundclass %>">  
				<bean:write name = "ms" property = "duration" />
				<html:hidden name = "ms" property = "duration" />
			</td>
			 -->
			
			
			<td class = "<%= backgroundclass %>">  
				&nbsp;<bean:write name = "ms" property = "quantity" />&nbsp;
				<html:hidden name = "ms" property = "quantity" />
			</td>
			
			<td class = "<%= backgroundclass %>">
				&nbsp;<bean:write name = "ms" property = "estimatedmaterialcost" />&nbsp;
				<html:hidden name = "ms" property = "estimatedmaterialcost" />
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				&nbsp;<bean:write name = "ms" property = "estimatedcnslabourcost" />&nbsp;
				<html:hidden name = "ms" property = "estimatedcnslabourcost" />
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				&nbsp;<bean:write name = "ms" property = "estimatedcontractlabourcost" />&nbsp;
				<html:hidden name = "ms" property = "estimatedcontractlabourcost" />
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				&nbsp;<bean:write name = "ms" property = "estimatedfreightcost" />&nbsp;
				<html:hidden name = "ms" property = "estimatedfreightcost" />
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				&nbsp;<bean:write name = "ms" property = "estimatedtravelcost" />&nbsp;
				<html:hidden name = "ms" property = "estimatedtravelcost" />
		   </td> 
		   
		   <td class = "<%= backgroundclass %>">
				&nbsp;<bean:write name = "ms" property = "estimatedtotalcost" />&nbsp;
				<html:hidden name = "ms" property = "estimatedtotalcost" />
		   </td> 
		   
		   <td class = "<%= backgroundclass %>">
				&nbsp;<bean:write name = "ms" property = "extendedprice" />&nbsp;
				<html:hidden name = "ms" property = "extendedprice" />
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				&nbsp;<bean:write name = "ms" property = "proformamargin" />&nbsp;
				<html:hidden name = "ms" property = "proformamargin" />
		   </td>
		   
		   <td class = "<%= backgroundclass %>">
				&nbsp;<bean:write name = "ms" property = "status"/>&nbsp;		
			</td>        	
  		</tr>
  			
  		<%} %>
  		
	   <% i++;  %>
	  </logic:iterate>
	  
	  </table></td></tr>
  		
		<html:hidden property = "ref" />
		
		<tr>	
		  	<td colspan = "14" class = "buttonrow">
		  		<html:submit  styleClass = "button" onclick = "return validate();"/>
		  		<html:reset property = "cancel" styleClass = "button"><bean:message bundle = "AM" key = "am.tabular.cancel"/></html:reset>	
			</td>
		</tr>
	</logic:present>
  	
	

	<jsp:include page = '/Footer.jsp'>
		      <jsp:param name = 'colspan' value = '37'/>
		      <jsp:param name = 'helpid' value = 'ammanage'/>
		   </jsp:include>
	
	</table>
	</td>
	</tr>
	</table>
	</logic:present>
	</html:form>
<script>
function getActivityList(obj) {
	document.forms[0].action = "MastertolibcopyAction.do?ref=mastertolibcopy&MSA_Id="+document.forms[0].msa_Id.value+"&selectedMSAId="+obj.value;
	document.forms[0].submit();
	return true;
}
</script>
<script>

function validate()
{
	document.forms[0].ref.value = 'Submit';
	var submitflag = 'false';
	return true;
}

function toggleMenuSection(unique) {
	var divid="div_"+unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	}
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) {
		thisImage.src = "images/Collapse.gif";
	}
	else {
		thisImage.src = "images/Expand.gif";
	}
}
function toggleDiv(divId) {
	var div  = document.getElementById(divId);
	var tableObj = document.getElementById(divId);
	if (div.offsetHeight > 0) {
		div.style.display = "none";
	} else {
		div.style.display = "block";
	}
}
</script>
	</body>
	</html:html>
	

