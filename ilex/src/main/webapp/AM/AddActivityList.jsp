<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%  String msa_id = "";
	String category_Id = "";
	String msaname = "";
	String MSA_Id = "";
	
	if( request.getAttribute( "MSA_Id" ) != null )
		MSA_Id = ( String ) request.getAttribute( "MSA_Id" );
	//Start :Added By Amit For checkNetmedx
	if( request.getAttribute( "MSA_Id" ) != null )
		msa_id = ( String ) request.getAttribute( "MSA_Id" );
	if(request.getAttribute("msaname")!=null)
	{
		msaname = (String)request.getAttribute("msaname");
	}	
	//End :Added By Amit
	if( request.getAttribute( "MSA_Id" ) != null )
		msa_id = ( String ) request.getAttribute( "MSA_Id" );

	if( request.getAttribute( "category_Id" ) != null )
		category_Id = ( String ) request.getAttribute( "category_Id" );

	int activity_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString()); %> 
<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.tabular.title"/></title>
	<%@ include file="/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script language = "javaScript" src = "javascript/JLibrary.js"></script>
</head>

<%
	String backgroundclass = "texto";
	String backgroundhyperlinkclass = null;
	String displaycombo = null;
	String readonlyclass = null;
	String readonlynumberclass = null;
	String backnew = "";
	int i = 0;
	String comboclass = null;
	boolean csschooser = true;
	String schdayscheck = null;
	String valstr = null;
%>



<body>
	<div id="menunav">
	    <ul>
	  		<li>
	        	<a href="#" class="drop"><span>Activity</span></a>
	  			<ul>
	  				<li><a href="ManageActivityAction.do?MSA_Id=<%= MSA_Id %>&ref=addActivity">Add</a></li>
	 				<li><a href="ManageActivityAction.do?MSA_Id=<%= MSA_Id %>&ref=manageActivity">Manage</a></li>
	 				<li><a href="ActivitySummaryAction.do?ref=View&MSA_Id=<%=MSA_Id %>">View</a></li>
	 				<%
	 				if(request.getAttribute("checkformastercopy") != null && request.getAttribute("checkformastercopy").equals("N")) {
	 					%>
	 					<li><a href="MastertolibcopyAction.do?ref=showAllMSA&MSA_Id=<%= MSA_Id %>">Copy</a></li>
	 					<%
 					}
 					%>
	  			</ul>
	  		</li>
	  		<li>
	        	<a href="#" class="drop"><span><bean:message bundle = "AM" key = "am.summary.Partnerinfo"/></span></a>
	  			<ul>
	  				<li><a href="PartnerSOWAssumptionList.do?fromId=<%= MSA_Id %>&viewType=sow&fromType=am_act">Manage SOW</a></li>
	 				<li><a href="PartnerSOWAssumptionList.do?fromId=<%= MSA_Id %>&viewType=assumption&fromType=am_act">Manage Assumptions</a></li>
	  			</ul>
	  		</li>
		</ul>
	</div>
<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
	<tr>
    	<td class = "toprow"> 
	    	<table  border = "0" cellspacing = "0" cellpadding = "0" height = "18">
		        <tr>
		        	<td background="images/content_head_04.jpg" height="21" colspan=3>
			         <div id="breadCrumb">
						 <a href="#" class="bgNone">Activity Manager</a>
			         	 <a href="ActivitySummaryAction.do?ref=View&MSA_Id=<%=msa_id %>" background="none;"><bean:write name = "msaname" scope = "request"/></a>
						 <a><span class="breadCrumb1">Add Activity</span></a></div>
			    	</td>
				</tr>
		        <tr><td height="2">&nbsp;</td></tr>	
	      	</table>
    	</td>
	</tr>
</table>

<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
 <tr>
   	<td style="padding-left: 10px;">
  		<table border = "0" cellspacing = "1" cellpadding = "1">
	  		<html:form action = "/ManageActivityAction">
  			<html:hidden name = "ManageActivityForm" property = "msaname" />
  			<html:hidden property="newactivity_Id" value="addActivity"/>
  		
		  		<%	
						i = 1;
						displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
			   %>	
  	
				<html:hidden property = "msa_Id" />  		
				 <tr>
				 	<td class = "colDarkNoWidthSpaces" >
		    			<bean:message bundle = "AM" key = "am.tabular.name"/>:
		    		</td>
				 	<td class = "colLight">
				   	 <html:text styleId = "newname" property = "newname" size = "60" styleClass = "text"/>
				   </td>
				 </tr>
				 <tr>
				 	<td class = "colDarkNoWidthSpaces" >
						<bean:message bundle = "AM" key = "am.tabular.type"/>:
					</td>
				 	<td  class = "colLight">
				   	   <html:select property = "newactivitytypecombo" size = "1" styleClass = "select">
						   <html:optionsCollection  property = "activity" value = "value" label = "label"/> 
					   </html:select>
				   </td>
				 </tr>
				 <tr>
				 	<td class = "colDarkNoWidthSpaces">
						<bean:message bundle = "AM" key = "am.tabular.category"/>:
					</td>
				 	<td class = "colLight">
						   <!--<html:text styleId = "newcategorytype" property = "newcategorytype" size = "15" styleClass = "textbox" readonly = "true"  onfocus = "<%= displaycombo %>"  onmouseover = "highlightTextField( this );" onmouseout = "unHighlightTextField( this );"/>-->
					   	   <html:select property = "newcategorytypecombo" size = "1" styleClass = "select">
							   <html:optionsCollection  property = "category" value = "value" label = "label"/> 
						   </html:select>
					</td>
				 </tr>
					<tr>	
					  	<td colspan = "2" class = "colLight">
					  		<html:submit property = "update" styleClass = "button_c" onclick = "return validate();">Save</html:submit>
					  		<html:reset property = "cancel" styleClass = "button_c"><bean:message bundle = "AM" key = "am.tabular.cancel"/></html:reset>
							<!-- End :Added By Amit for Mass delete -->
						</td>
				  	</tr>
					<jsp:include page = '/Footer.jsp'>
						      <jsp:param name = 'colspan' value = '37'/>
						      <jsp:param name = 'helpid' value = 'ammanage'/>
					</jsp:include>
		
			</table>
	</td>
</tr>
</table>
  
</html:form>

<script>

function validate()
{
	var submitflag = 'false';
	if( document.forms[0].newname.value == "" )
	  	{
	  		alert( "<bean:message bundle = "AM" key = "am.tabular.entername"/>");
	  		document.forms[0].newname.focus();
	  		return false;
	  	}
	  	
	if( document.forms[0].newactivitytypecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.tabular.selectactivity"/>" );
 			return false;
 		}
	if( document.forms[0].newcategorytypecombo.value == "0" )
		{
			alert( "Please select Activity Category." );
			return false;
		}
	document.forms[0].newactivity_Id.value="addActivity";  
	return true;
}



</script>
</body>
</html:html>