<!DOCTYPE HTML>
<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For adding and updating freight resource of an activity
*
-->



<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<bean:define id = "checkfortext" name = "refresh" scope = "request"/>
<%
	String temp_str = null;
	String Id = ( String ) request.getAttribute( "Activity_Id" );
	int freight_size =( int ) Integer.parseInt( request.getAttribute( "Size" ).toString() );
%>


<html:html>
<head>
	<title><bean:message bundle = "AM" key = "am.freight.title"/></title>
	<%@ include file = "/Header.inc" %>
	<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
	<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>

<%@ include file = "/Menu.inc" %>

<%
	String backgroundclass = "texto";
	String displaycombo = null;
	String readonlyclass = null;
	String backgroundhyperlinkclass = null;
	String readonlynumberclass = null;
	String backnew = "";
	int i = 0;
	String comboclass = null;
	boolean csschooser = true;
	String schdayscheck = null;
	String valstr = null;
	String arrcal = null;
%>




<body leftmargin = "0" topmargin = "0" marginwidth = "0" marginheight = "0" onLoad = "MM_preloadImages( 'images/sales1b.gif' , 'images/services1b.gif' , 'images/about1b.gif' , 'images/cust-serv1b.gif' , 'images/offers1b.gif')">

<table width = "100%" border = "0" cellspacing = "0" cellpadding = "0">
  <tr>
    <td class = "toprow"> 
      <table width = "" border = "0" cellspacing = "1" cellpadding = "0" height = "18">
        <tr align = "center"> 
          <td width = "140" class = "toprow1"><a href = "MaterialResourceAction.do?ref=View&Activitylibrary_Id=<%= Id %>&MSA_Id=<bean:write name = "FreightResourceForm" property = "MSA_Id" />" class = "drop"><bean:message bundle = "AM" key = "am.material"/></a></td>
          <td width = "150" class = "toprow1"><a href = "LaborResourceAction.do?ref=View&Activitylibrary_Id=<%= Id %>&MSA_Id=<bean:write name = "FreightResourceForm" property = "MSA_Id" />" class = "drop"><bean:message bundle = "AM" key = "am.labor"/></a></td>
          <td width = "120" class = "toprow1"><a href = "TravelResourceAction.do?ref=View&Activitylibrary_Id=<%= Id %>&MSA_Id=<bean:write name = "FreightResourceForm" property = "MSA_Id" />" class = "drop"><bean:message bundle = "AM" key = "am.travel"/></a></td>
          <td width = "390" class = "toprow1"><a href = "ActivityLibraryDetailAction.do?ref=View&Activitylibrary_Id=<%= Id %>&type=AM&MSA_Id=<bean:write name = "FreightResourceForm" property = "MSA_Id" />" class = "drop"><bean:message bundle = "AM" key = "am.material.back"/></a></td>
         <!--   <td width = "390" class=  "toprow1">&nbsp;&nbsp;&nbsp;&nbsp;</td>-->
        </tr>
      </table>
    </td>
  </tr>
</table>


<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
 <tr>
  	<td>
  		<table border = "0" cellspacing = "1" cellpadding = "1">
  			<!-- Start :Added By Amit -->
  		
		  		<logic:present name = "changestatusflag" scope = "request">
		  		<tr><td>&nbsp;</td>
			    	<td colspan = "5" class = "message" height = "30">
				  		<logic:equal name = "changestatusflag" value = "0">
						<bean:message bundle = "AM" key = "am.freight.resourcesapprovedsuccessfully"/>
						</logic:equal>
					</td>
					</tr>
		  		</logic:present>
  		<!--  End Aded By Amit -->
		  		<logic:present name = "addflag" scope = "request">
			    	<tr>
		  			<td>&nbsp;</td>
				    <td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "addflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.freight.add.success"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "addflag" value = "-9001">
			    		<bean:message bundle = "AM" key = "am.freight.add.failure1"/>
			    	</logic:equal>
			    	
					<logic:equal name = "addflag" value = "-9002">
			    		<bean:message bundle = "AM" key = "am.freight.add.failure2"/>
			    	</logic:equal>
				
					<logic:equal name = "addflag" value = "-9003">
			    		<bean:message bundle = "AM" key = "am.freight.add.failure3"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "addflag" value = "-9004">
			    		<bean:message bundle = "AM" key = "am.freight.add.failure4"/>
			    	</logic:equal>
			    	
					<logic:equal name = "addflag" value = "-9005">
			    		<bean:message bundle = "AM" key = "am.freight.add.failure5"/>
			    	</logic:equal>
				
					<logic:equal name = "addflag" value = "-9006">
			    		<bean:message bundle = "AM" key = "am.freight.add.failure6"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
				</logic:present>
				
				<logic:present name = "updateflag" scope = "request">
					<tr>
		  			<td>&nbsp;</td>
				    <td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "updateflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.freight.update.success"/>	
			    	</logic:equal>
			    	
			    	<logic:equal name = "updateflag" value = "-9007">
			    		<bean:message bundle = "AM" key = "am.freight.update.failure1"/>
			    	</logic:equal>
			    	
					<logic:equal name = "updateflag" value = "-9008">
			    		<bean:message bundle = "AM" key = "am.freight.update.failure2"/>
			    	</logic:equal>
				
					<logic:equal name = "updateflag" value = "-9009">
			    		<bean:message bundle = "AM" key = "am.freight.update.failure3"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "updateflag" value = "-9010">
			    		<bean:message bundle = "AM" key = "am.freight.update.failure4"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
				</logic:present>
		  		
		  		<logic:present name = "deleteflag" scope = "request">
			    	<tr>
		  			<td>&nbsp;</td>
				    <td colspan = "5" class = "message" height = "30">
			    	<logic:equal name = "deleteflag" value = "0">
			    		<bean:message bundle = "AM" key = "am.freight.delete.success"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9001">
			    		<bean:message bundle = "AM" key = "am.freight.delete.failure1"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9002">
			    		<bean:message bundle = "AM" key = "am.freight.delete.failure2"/>
			    	</logic:equal>
			    	
			    	<logic:equal name = "deleteflag" value = "-9004">
			    		<bean:message bundle = "AM" key = "am.freight.delete.failure3"/>
			    	</logic:equal>
			    	</td>
			    	</tr>
			    </logic:present>
	  		
  		
			<tr> 
				<td>&nbsp; </td>
	    		<td class = "labeleboldhierrarchy" colspan = "13" height = "30">
	    			<bean:message bundle = "AM" key = "am.freight.header"/>
	    			<bean:message bundle = "AM" key = "am.freight.for"/>
	    			<a href = "ManageActivityAction.do?MSA_Id=<bean:write name = "FreightResourceForm" property = "msa_id"/>">
						<bean:write name = "FreightResourceForm" property = "msaname"/>
					</a>
	    			<bean:message bundle = "AM" key = "am.detail.arrow"/>&nbsp;
					<a href = "ActivityLibraryDetailAction.do?Activitylibrary_Id=<bean:write name = "FreightResourceForm" property = "activity_Id"/>">
						<bean:write name ="activityname" scope = "request"/> 
					</a>
	    		</td>
			</tr>
  
	  		<tr> 
	    		<td rowspan = "2">&nbsp; </td>
	    		<td class = "tryb" rowspan = "2">
	    			<bean:message bundle = "AM" key = "am.freight.name"/>
	    		</td>
	   
				<td class = "tryb" rowspan = "2">
					<bean:message bundle = "AM" key = "am.freight.type"/>
				</td>
				
				<td class = "tryb" rowspan = "2">
					<bean:message bundle = "AM" key = "am.freight.cnspartnumber"/>
				</td>
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.freight.quantity"/>
			    </td>
			  
			    <td class = "tryb" colspan = "2">
			    	<bean:message bundle = "AM" key = "am.freight.estimatedcost"/>
			    </td>
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.freight.proformamargin"/>
			    </td>
			    <td class = "tryb" colspan = "2">
			    	<bean:message bundle = "AM" key = "am.freight.price"/>
			    </td>
			    
			    <td class = "tryb" rowspan = "2">
			    	<bean:message bundle = "AM" key = "am.freight.status"/>
			    </td>
			    
			</tr>
  
	  		<tr> 
	    		<td class = "tryb"> 
	      			<div align = "center">
	      				<bean:message bundle = "AM" key = "am.freight.estimatedunitcost"/>
	      			</div>
	    		</td>
	    
	    		<td class = "tryb"> 
	      			<div align = "center">
	      				<bean:message bundle = "AM" key = "am.freight.estimatedtotalcost"/>
	      			</div>
	    		</td>
	    
	    
			    <td class = "tryb"> 
			      <div align = "center">
			      	<bean:message bundle = "AM" key = "am.freight.priceunit"/>
			      </div>
			    </td>
	    
			    <td class = "tryb"> 
			      <div align = "center">
			      	<bean:message bundle = "AM" key = "am.freight.priceextended"/>
			      </div>
			    </td>
			</tr>
			
<html:form action = "/FreightResourceAction">
<html:hidden name = "FreightResourceForm" property = "MSA_Id" />
	<logic:present name = "freightresourcelist" scope = "request"> 
 		<logic:iterate id = "ms" name = "freightresourcelist">
			<bean:define id = "resourceId" name = "ms" property = "freightid" type = "java.lang.String"/>
			<bean:define id = "estUnitCost" name = "ms" property = "estimatedunitcost" type = "java.lang.String"/>
		<%	
			temp_str = "marginComputation('"+resourceId+"', '"+estUnitCost+"', "+i+", "+freight_size+");"; 

			if ( csschooser == true ) 
			{
				backgroundclass = "texto";
				backgroundhyperlinkclass = "hyperodd";
				comboclass = "combooddhidden";
				readonlyclass = "readonlytextodd";
				csschooser = false;
				readonlynumberclass = "readonlytextnumberodd";
			}
	
			else
			{
				csschooser = true;	
				comboclass = "comboevenhidden";
				backgroundhyperlinkclass = "hypereven";
				readonlyclass = "readonlytexteven";
				backgroundclass = "texte";
				readonlynumberclass = "readonlytextnumbereven";
			}
		%>
			<bean:define id = "val" name = "ms" property = "freightid" type = "java.lang.String"/>
			<bean:define id = "flag" name = "ms" property = "flag" type = "java.lang.String"/>
			<html:hidden name="ms" property="flag_freightid"/>
			<tr>		
				
				<td class = "labeleboldwhite"> 
					<logic:equal name = "ms" property = "status" value = "Approved">
						<%--<html:checkbox property = "check" value = "<%= flag+val %>" disabled = "true"/>	--%>
						<html:multibox property = "check"  disabled = "true"><bean:write  name="ms" property = "flag_freightid"/></html:multibox>	
					</logic:equal>
				
					<logic:notEqual name = "ms" property = "status" value = "Approved">
						<%--<html:checkbox property = "check" value = "<%= flag+val %>" /> --%>
						<html:multibox property = "check" ><bean:write  name="ms" property = "flag_freightid"/></html:multibox>	
					</logic:notEqual>	
	    		</td>
	    		
	    		<html:hidden name = "ms" property = "freightid"/>
	    		<html:hidden name = "ms" property = "freightcostlibid" />
				
				<%  
					if( freight_size == 1 )
					{
						valstr = "javascript: document.forms[0].check.checked = true;updatetext(this,'');"; 
						displaycombo = "javascript: displaycombo(this,'')";
						schdayscheck = "javascript: document.forms[0].check.checked = true;";	
						arrcal = "javascript: calarr(null);";
						
					}
					else if( freight_size > 1 )
					{
					valstr = "javascript: document.forms[0].check['"+i+"'].checked = true;updatetext( this ,'"+i+"' );"; 
					schdayscheck = "javascript: document.forms[0].check['"+i+"'].checked = true;";
					displaycombo = "javascript: displaycombo( this , '"+i+"' )";
					arrcal = "javascript: calarr('"+i+"');";
					}
				%>
				
				<!-- for multiple resource select start -->
				<html:hidden name="ms" property="flag"/>
				<logic:equal name="ms" property="flag" value="T">
				<td class = "<%= backgroundhyperlinkclass %>"> 
					<bean:write name = "ms" property = "freightname" />
				</td>
				</logic:equal>
				<logic:equal name="ms" property="flag" value="P">
				<td class = "<%= backgroundhyperlinkclass %>"> 
					<a href = "FreightResourceDetailAction.do?Freight_Id=<bean:write name = "ms" property = "freightid"/>&Activity_Id=<bean:write name = "FreightResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "freightname" /></a>
				</td>
				</logic:equal>
				<!-- for multiple resource select end -->
				<!-- <td class = "<%= backgroundhyperlinkclass %>"> 
					<a href = "FreightResourceDetailAction.do?Freight_Id=<bean:write name = "ms" property = "freightid"/>&Activity_Id=<bean:write name = "FreightResourceForm" property = "activity_Id"/>"><bean:write name = "ms" property = "freightname" /></a>
				</td> -->
				
				<html:hidden name = "ms" property = "freightname" />
				
				<td class = "<%= backgroundhyperlinkclass %>">
					<bean:write name = "ms" property = "freighttype" /> 
					<html:hidden styleId = "freighttype" name = "ms" property = "freighttype" />
				</td>
				
				<td class = "<%= backgroundhyperlinkclass %>">
					<bean:write name = "ms" property = "cnspartnumber" /> 
					<html:hidden styleId = "cnspartnumber" name = "ms"  property = "cnspartnumber" />
				</td>
				
				
				<logic:equal name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text  name = "ms" property = "quantity" size = "6" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
					</td>
				</logic:equal>		
				
				<logic:notEqual name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "<%= "quantity"+i%>" name = "ms"  size = "6" maxlength = "6" property = "quantity" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "<%= schdayscheck %>" onblur = "<%= arrcal %>"/>
					</td>
				</logic:notEqual>
				
			   
				
				
				<html:hidden name = "ms" property = "prevquantity" />
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "estimatedunitcost" name = "ms" size = "6"  property = "estimatedunitcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td>
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "estimatedtotalcost" name = "ms" size = "10" maxlength = "9"  property = "estimatedtotalcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td> 
				
				<logic:equal name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "<%= "proformamargin"+i%>" name = "ms" size = "9" maxlength = "10" property = "proformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true"/>
					</td>
					
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "priceunit" name = "ms" size = "10" maxlength = "9"  property = "priceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />&nbsp;&nbsp;<html:button styleClass = "buttonverticalalign" property="setmargin" disabled="true"><bean:message bundle = "AM" key = "am.common.set"/></html:button>
				    </td>
				</logic:equal>
				
				<logic:notEqual name = "ms" property = "status" value = "Approved">
					<td class = "<%= backgroundclass %>">
						<html:text styleId = "<%= "proformamargin"+i%>" name = "ms" size = "9" maxlength = "10" property = "proformamargin" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "<%= schdayscheck %>" onblur = "<%= arrcal %>"/>
				    </td> 
				   	<td class = "<%= backgroundclass %>">
						<html:text styleId = "priceunit" name = "ms" size = "10" maxlength = "9"  property = "priceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />&nbsp;&nbsp;<html:button styleClass = "buttonverticalalign" property="setmargin" onclick = "<%= temp_str%>"><bean:message bundle = "AM" key = "am.common.set"/></html:button>
				    </td>
				</logic:notEqual>
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "priceextended" name = "ms" size = "10" maxlength = "9"  property = "priceextended" readonly = "true"  styleClass = "<%= readonlynumberclass %>"/>
			    </td>
			   
			    <td class = "<%= backgroundclass %>">
					<bean:write name = "ms" property = "status"/>		
				</td>
				
				<html:hidden name = "ms" property = "status"/>
				
				<html:hidden name = "ms" property = "sellablequantity" />
	   			<html:hidden name = "ms" property = "minimumquantity" />        	
	  		</tr>
  		
	   <% i++;  %>
		</logic:iterate>
	</logic:present>
	
				<%	
				  	if( freight_size != 0 )
				  	{
				  		if( freight_size % 2 == 1 )
				  		{
							backnew = "textenewelement";
				  			backgroundclass = "texte";
				  			i = 2;
							displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
							comboclass = "comboevenhidden";
							readonlyclass = "readonlytexteven";
							readonlynumberclass = "readonlytextnumbereven";
						}
				  		else
				  		{
							backnew = "textonewelement";
				  			backgroundclass = "texto";
				  			i = 1;
							displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
							comboclass = "combooddhidden";
							readonlyclass = "readonlytextodd";
							readonlynumberclass = "readonlytextnumberodd";
				  		}
				  	}
				  	else
					{
						backnew = "textonewelement";
						backgroundclass = "texto";
						i = 1;
						displaycombo = "javascript:displaynewcombo( this , '"+i+"' )";
						comboclass = "combooddhidden";
						readonlyclass = "readonlytextodd";
						readonlynumberclass = "readonlytextnumberodd";
					}
	   			%>
	   		
	   			<html:hidden property = "activity_Id" />
	   			<html:hidden property = "ref" /> 
	   			<html:hidden property = "newsellablequantity" />
	   			<html:hidden property = "newminimumquantity" />
	   		<tr> 
		    	<td class = "labeleboldwhite"> 
		    		<logic:equal name = "act_status"  value = "Approved" scope = "request">
		    			<html:checkbox property = "newfreightid" disabled = "true"/>
		    		</logic:equal>
		    		
		    		<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
		    			<html:checkbox property = "newfreightid" />
		    		</logic:notEqual>
		   		</td>
		   		
		   		<td class = "<%= backnew %>">
		   	 		<logic:equal name = "act_status"  value = "Approved" scope = "request">
		   	 			<html:text property = "newfreightname" size = "20" styleClass = "<%= readonlyclass %>" readonly = "true"/>
			   			<html:hidden property = "newfreightnamecombo" />
		   	 		</logic:equal>
		   	 		
		   	 		<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
		   	 			<html:text styleId = "newfreightname" property = "newfreightname" size = "20" styleClass = "textbox" onclick ="javascript:opensearchwindow();" readonly="true" onmouseover = "highlightTextField( this );" onmouseout = "unHighlightTextField( this );" />
			   			<html:select styleId = "newfreightnamecombo" property = "newfreightnamecombo" size = "1" styleClass = "<%= comboclass %>" onblur = "hidecombo( this );"    onchange = "document.forms[0].newfreightid.checked = true; updatetext( this , null ); return refresh();">
					    	<html:optionsCollection  property = "freightlist" value = "value" label = "label"/> 
				   		</html:select>
		   	 		</logic:notEqual>
		   		</td>
		   		
		   		<td class = "<%= backgroundclass %>">
		   	 		<bean:write name = "FreightResourceForm" property = "newfreighttype" />
		   	 		<html:hidden styleId = "newfreighttype" property = "newfreighttype" />
		   		</td>
		   		
			    <td class = "<%= backgroundclass %>">
					<bean:write name = "FreightResourceForm" property = "newcnspartnumber" />
					<html:hidden styleId = "newcnspartnumber"  property = "newcnspartnumber" />
				</td>
				
				<td class = "<%= backgroundclass %>">
					<logic:equal name = "act_status"  value = "Approved" scope = "request">
						<html:text size = "6" maxlength = "6" property = "newquantity" styleClass = "<%= readonlynumberclass %>" readonly = "true"/>
					</logic:equal>
					
					<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
						<% if( checkfortext.equals( "true" ) ) { %>	
							<html:text size = "6" maxlength = "6" property = "newquantity" styleClass = "<%= readonlynumberclass %>" readonly = "true"/>
						<%}else { %>
							<html:text styleId = "newquantity"  size = "6" maxlength = "6" property = "newquantity" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "document.forms[0].newfreightid.checked = true;" onblur = "calnew();" />
						<%} %>
					</logic:notEqual>
				</td>
				
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "newestimatedunitcost"  size = "6"  property = "newestimatedunitcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td>
			   
				<td class = "<%= backgroundclass %>">
					<html:text styleId = "newestimatedtotalcost" size = "10" maxlength = "9"  property = "newestimatedtotalcost" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
				</td> 
			   
				<td class = "<%= backgroundclass %>">
					<logic:equal name = "act_status"  value = "Approved" scope = "request">
						<html:text size = "6" maxlength = "5" property = "newproformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
			    	</logic:equal>
			    	
			    	<logic:notEqual name = "act_status"  value = "Approved" scope = "request">
			    		<% if( checkfortext.equals( "true" ) ) { %>	
			    			<html:text size = "6" maxlength = "5" property = "newproformamargin" styleClass = "<%= readonlynumberclass %>" readonly = "true" />
			    		<%}else { %>
			    			<html:text styleId = "newproformamargin" size = "6" maxlength = "5" property = "newproformamargin" styleClass = "textboxnumber" onmouseover = "highlightTextFieldright( this );" onmouseout = "unHighlightTextFieldright( this );" onchange = "document.forms[0].newfreightid.checked = true;" onblur = "calnew();"/>
			    		<%} %>
			    	</logic:notEqual>
			    </td> 
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "newpriceunit" size = "10" maxlength = "9"  property = "newpriceunit" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
			    </td>
			   
			    <td class = "<%= backgroundclass %>">
					<html:text styleId = "newpriceextended"  size = "10" maxlength = "9"  property = "newpriceextended" readonly = "true"  styleClass = "<%= readonlynumberclass %>" />
			    </td>
			   
			    <td class = "<%= backgroundclass %>">
					<bean:write name = "FreightResourceForm" property = "newstatus"/>		
				</td>        	
	  		</tr>
	  		
	  		
	  		<tr>	<td >&nbsp; </td>
			  	<td colspan = "9" class = "buttonrow">
			  		<html:submit property = "save" styleClass = "button" onclick = "return validate();"><bean:message bundle = "AM" key = "am.freight.submit"/></html:submit>
					<html:button property = "sort" styleClass = "button" onclick = "return sortwindow();"><bean:message bundle = "AM" key = "am.freight.sort"/></html:button>
					<!-- Start :Added By Amit for Mass delete -->
					<html:button property = "massDelete" styleClass = "button" onclick = "return del();"><bean:message bundle = "AM" key = "am.travel.massDelete"/></html:button>
					<html:button property = "approve" styleClass = "button" onclick = "return massApprove();"><bean:message bundle = "AM" key = "am.freight.massApprove"/></html:button>
					<!-- End :Added By Amit for Mass delete -->
				</td>
				<td class = "buttonrow">
					<html:reset property = "cancel" styleClass = "button"><bean:message bundle = "AM" key = "am.freight.cancel"/></html:reset>
				</td>
			</tr> 
			
			<jsp:include page = '/Footer.jsp'>
		      <jsp:param name = 'colspan' value = '37'/>
		      <jsp:param name = 'helpid' value = 'amfreighttabular'/>
		   </jsp:include>  
		</table>
	</td>
</tr>
</table>
  
</html:form>


<script>
function marginComputation(resourceId, estUnitCost, position, size)
{
	str = "MarginComputationAction.do?resourceId="+resourceId+"&estUnitCost="+estUnitCost+"&position="+position+"&size="+size;	
	suppstrwin = window.open(str, '', 'left = 300 , top = 250 , width = 300, height = 240 , resizable = yes , scrollbars = yes')
	suppstrwin.focus();
}
function opensearchwindow()
{
	str = "OpenSearchWindowTempAction.do?type=Freight&from=am&activity_Id=<bean:write name = "FreightResourceForm" property = "activity_Id" />";
	suppstrwin = window.open( str , '' , 'left = 300 , top = 250 , width = 600, height = 400 , resizable = yes , scrollbars = yes' );
	suppstrwin.focus();
}

function sortwindow()
{
	str = 'SortAction.do?Type=F&Activitylibrary_Id=<%= Id %>';
	p=window.open( str , "_blank", 'height=350, width=450, status=yes, toolbar=no, menubar=no, location=no,addressbar=no' );
	/* p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' );  */
	
}
function validate()
{
	document.forms[0].ref.value = "Submit";
	
	var submitflag = 'false';
	if( <%= freight_size %> != 0 )
	{
		if( <%= freight_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newfreightid.checked ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.freight.selectresource"/>" );
				return false;
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newfreightid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.freight.selectresource"/>" );
				return false;
		  	}
		}
	}
	
	else
	{
		if( !document.forms[0].newfreightid.checked )
		{
			alert( "<bean:message bundle = "AM" key = "am.freight.newfreight"/>" );
			return false;
		}
	}
	
	
	
	
	//from Not to write
	if( <%= freight_size %> != 0 )
	{
		if( <%= freight_size %> == 1 )
		{
			if( document.forms[0].check.checked ) 
	  		{
	  			if( document.forms[0].quantity.value == "" )
	  			{
	  				document.forms[0].quantity.value = "0.0";
	  			}
	 
		  		if( !isFloat( document.forms[0].quantity.value ) )
		 		{
		 			alert( "<bean:message bundle = "AM" key = "am.freight.numericvalue"/>" );
		 			document.forms[0].quantity.value = "";
		 			document.forms[0].quantity.focus();
					return false;
		 		}
		 		
		 		if( document.forms[0].proformamargin.value == "" )
	  			{
	  				alert( "<bean:message bundle = "AM" key = "am.material.proformamarginenter"/>" );
	  				document.forms[0].proformamargin.focus();
	  				return false;
	  			}
	 
		  		else
		  		{
		  			if( isFloat( document.forms[0].proformamargin.value ) )
		 			{
			 			if( document.forms[0].proformamargin.value > parseFloat( "1.0" ) )
			 			{
			 				alert( "<bean:message bundle = "AM" key = "am.material.proformamargingreaterthanone"/>" );
			 				document.forms[0].proformamargin.value = "";
			 				document.forms[0].proformamargin.focus();
							return false;
			 			}
		 			}
		 			else
		 			{
		 				alert( "<bean:message bundle = "AM" key = "am.material.numericvalue"/>" );
			 			document.forms[0].proformamargin.value = "";
			 			document.forms[0].proformamargin.focus();
						return false;
		 			}
		 		}	
	  		}
		}
		
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
			  		if( document.forms[0].quantity[i].value == "" )
		  			{
		  				document.forms[0].quantity[i].value = "0.0";
		  			}
		 
			  		if( !isFloat( document.forms[0].quantity[i].value ) )
			 		{
			 			alert( "<bean:message bundle = "AM" key = "am.freight.numericvalue"/>" );
			 			document.forms[0].quantity[i].value = "";
			 			document.forms[0].quantity[i].focus();
						return false;
			 		}
			 		
			 		if( document.forms[0].proformamargin[i].value == "" )
		  			{
		  				alert( "<bean:message bundle = "AM" key = "am.material.proformamarginenter"/>" );
		  				document.forms[0].proformamargin[i].focus();
		  				return false;
		  			}
	 
			  		else
			  		{
			  			if( isFloat( document.forms[0].proformamargin[i].value ) )
			 			{
				 			if( document.forms[0].proformamargin[i].value > parseFloat( "1.0" ) )
				 			{
				 				alert( "<bean:message bundle = "AM" key = "am.material.proformamargingreaterthanone"/>" );
				 				document.forms[0].proformamargin[i].value = "";
				 				document.forms[0].proformamargin[i].focus();
								return false;
				 			}
			 			}
			 			else
			 			{
			 				alert( "<bean:message bundle = "AM" key = "am.material.numericvalue"/>" );
				 			document.forms[0].proformamargin[i].value = "";
				 			document.forms[0].proformamargin[i].focus();
							return false;
			 			}
			 		}	
		  		}
		  	}
		}
	}
	//till Not to write
	
	if( document.forms[0].newfreightid.checked ) 
  	{
  		if( document.forms[0].newfreightnamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.freight.freightenter"/>" );
 			document.forms[0].newfreightname.focus();
 			return false;
 		}
  	
  	
  	
	  	if( document.forms[0].newquantity.value == "" )
  		{
  			alert( "<bean:message bundle = "AM" key = "am.material.quantitymandatory"/>" );
  			document.forms[0].newquantity.focus();
  			return false;
  		}
 
	  	if( !isFloat( document.forms[0].newquantity.value ) )
	 	{
	 		alert( "<bean:message bundle = "AM" key = "am.freight.numericvalue"/>" );
	 		document.forms[0].newquantity.value = "";
	 		document.forms[0].newquantity.focus();
			return false;
	 	}
	 		
	 	if( document.forms[0].newproformamargin.value == "" )
  		{
  			alert( "<bean:message bundle = "AM" key = "am.material.proformamarginenter"/>" );
  			document.forms[0].newproformamargin.focus();
  			return false;
  		}
	 
  		else
  		{
  			if( isFloat( document.forms[0].newproformamargin.value ) )
 			{
	 			if( document.forms[0].newproformamargin.value > parseFloat( "1.0" ) )
	 			{
	 				alert( "<bean:message bundle = "AM" key = "am.material.proformamargingreaterthanone"/>" );
	 				document.forms[0].newproformamargin.value = "";
	 				document.forms[0].newproformamargin.focus();
					return false;
	 			}
 			}
 			else
 			{
 				alert( "<bean:message bundle = "AM" key = "am.material.numericvalue"/>" );
	 			document.forms[0].newproformamargin.value = "";
	 			document.forms[0].newproformamargin.focus();
				return false;
 			}
 		}	
  	}
	return true;

}

function refresh()
{
	document.forms[0].ref.value = "Refresh";
	document.forms[0].submit();
	return true;
}


function calarr( i )
{
	if( <%= freight_size %> == 1 )
	{
		if( ( document.forms[0].quantity.value == "" ) || ( document.forms[0].quantity.value == "null" ) )
		{ 
			document.forms[0].quantity.value = "0.0";
		}
		
		if( isFloat( document.forms[0].quantity.value ) ) 
		{ 
			if( parseFloat( document.forms[0].quantity.value ) <  parseFloat( document.forms[0].minimumquantity.value ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.quantityadjustedagain"/>" );
				document.forms[0].quantity.value = document.forms[0].minimumquantity.value;
			}
			
		
		var divisor =  ( parseFloat( document.forms[0].quantity.value )  ) /  parseFloat( document.forms[0].sellablequantity.value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].quantity.value ) - (divisor * parseFloat( document.forms[0].sellablequantity.value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "AM" key = "am.quantityadjusted"/>" );
				document.forms[0].quantity.value = (parseFloat( document.forms[0].sellablequantity.value )*(divisor + 1)).toFixed(4) ;
			}
		
		/*	var remainder = ( parseFloat( document.forms[0].quantity.value ) - parseFloat( document.forms[0].minimumquantity.value ) ) %  parseFloat( document.forms[0].sellablequantity.value );
			var zero=0;
			if(  remainder.toFixed(2)== zero.toFixed(2) )
			{
			}
			else
			{
				var temp = ( parseFloat( document.forms[0].quantity.value ) - parseFloat( document.forms[0].minimumquantity.value ) - parseFloat( remainder ) ) /  parseFloat( document.forms[0].sellablequantity.value );
				alert( "<bean:message bundle = "AM" key = "am.quantityadjusted"/>" );
				document.forms[0].quantity.value = parseFloat( document.forms[0].minimumquantity.value ) + ( parseFloat( document.forms[0].sellablequantity.value ) * ( parseFloat( temp ) + 1 ) ); 
			}*/
			
			
			document.forms[0].estimatedtotalcost.value = parseFloat( document.forms[0].quantity.value ) * parseFloat( document.forms[0].estimatedunitcost.value );
			document.forms[0].estimatedtotalcost.value = round_func( document.forms[0].estimatedtotalcost.value );
		
			if( ( document.forms[0].proformamargin.value == "" ) || ( document.forms[0].proformamargin.value == "null" ) || ( document.forms[0].proformamargin.value >= "1" ) )
			{ 
				document.forms[0].proformamargin.value = "0.0";
			}
		
			if( isFloat( document.forms[0].proformamargin.value ) ) 
			{ 
				document.forms[0].priceunit.value = parseFloat( document.forms[0].estimatedunitcost.value ) / ( 1 - parseFloat( document.forms[0].proformamargin.value ) );
				document.forms[0].priceunit.value = round_func( document.forms[0].priceunit.value );
			}
			
			if( isFloat( document.forms[0].quantity.value ) ) 
			{ 
				document.forms[0].priceextended.value = parseFloat( document.forms[0].priceunit.value ) * parseFloat( document.forms[0].quantity.value );
				document.forms[0].priceextended.value = round_func( document.forms[0].priceextended.value );
			}
		}
	}
	
	else
	{
		if( ( document.forms[0].quantity[i].value == "" ) || ( document.forms[0].quantity[i].value == "null" ) )
		{ 
			document.forms[0].quantity[i].value = "0.0";
		}
		
		if( isFloat( document.forms[0].quantity[i].value ) ) 
		{ 
			if( parseFloat( document.forms[0].quantity[i].value ) <  parseFloat( document.forms[0].minimumquantity[i].value ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.quantityadjustedagain"/>" );
				document.forms[0].quantity[i].value = document.forms[0].minimumquantity[i].value;
			}
			
					
			var divisor =  ( parseFloat( document.forms[0].quantity[i].value )  ) /  parseFloat( document.forms[0].sellablequantity[i].value );
			divisor = parseInt(divisor.toFixed(4));
			var remainder = parseFloat( document.forms[0].quantity[i].value ) - (divisor * parseFloat( document.forms[0].sellablequantity[i].value ) );
			var zero=0;
			if(  parseInt(remainder.toFixed(4)*10000) == zero )
			{
			}
			else
			{
				alert( "<bean:message bundle = "AM" key = "am.quantityadjusted"/>" );
				document.forms[0].quantity[i].value = (parseFloat( document.forms[0].sellablequantity[i].value )*(divisor + 1)).toFixed(4) ;
			}
			
			/*var remainder = ( parseFloat( document.forms[0].quantity[i].value ) - parseFloat( document.forms[0].minimumquantity[i].value ) ) %  parseFloat( document.forms[0].sellablequantity[i].value );
			
			if(  remainder == 0 )
			{
			}
			else
			{
				var temp = ( parseFloat( document.forms[0].quantity[i].value ) - parseFloat( document.forms[0].minimumquantity[i].value ) - parseFloat( remainder ) ) /  parseFloat( document.forms[0].sellablequantity[i].value );
				alert( "<bean:message bundle = "AM" key = "am.quantityadjusted"/>" );
				document.forms[0].quantity[i].value = parseFloat( document.forms[0].minimumquantity[i].value ) + ( parseFloat( document.forms[0].sellablequantity[i].value ) * ( parseFloat( temp ) + 1 ) ); 
			}*/
			
			document.forms[0].estimatedtotalcost[i].value = parseFloat( document.forms[0].quantity[i].value ) * parseFloat( document.forms[0].estimatedunitcost[i].value );
			document.forms[0].estimatedtotalcost[i].value = round_func( document.forms[0].estimatedtotalcost[i].value );
			
			if( ( document.forms[0].proformamargin[i].value == "" ) || ( document.forms[0].proformamargin[i].value == "null" ) || ( document.forms[0].proformamargin[i].value >= "1" ) )
			{ 
				document.forms[0].proformamargin[i].value = "0.0";
			}
		
			if( isFloat( document.forms[0].proformamargin[i].value ) ) 
			{ 
				document.forms[0].priceunit[i].value = parseFloat( document.forms[0].estimatedunitcost[i].value ) / ( 1 - parseFloat( document.forms[0].proformamargin[i].value ) );
				document.forms[0].priceunit[i].value = round_func( document.forms[0].priceunit[i].value );
			}
			
			if( isFloat( document.forms[0].quantity[i].value ) ) 
			{ 
				document.forms[0].priceextended[i].value = parseFloat( document.forms[0].priceunit[i].value ) * parseFloat( document.forms[0].quantity[i].value );
				document.forms[0].priceextended[i].value = round_func( document.forms[0].priceextended[i].value );
			}
		}
	}
}


function calnew()
{
	if( document.forms[0].newfreightnamecombo.value == '0' )
	{
		alert( "<bean:message bundle = "AM" key = "am.freight.freightenter"/>" ); 
		return false;
	}
	
	if( ( document.forms[0].newquantity.value == "" ) || ( document.forms[0].newquantity.value == "null" ) )
	{ 
		document.forms[0].newquantity.value = "0.0";
	}
	
	if( isFloat( document.forms[0].newquantity.value ) ) 
	{ 
		if( parseFloat( document.forms[0].newquantity.value ) <  parseFloat( document.forms[0].newminimumquantity.value ) )
		{
			alert( "<bean:message bundle = "AM" key = "am.quantityadjustedagain"/>" );
			document.forms[0].newquantity.value = document.forms[0].newminimumquantity.value;
		}
		
		
		var divisor =  ( parseFloat( document.forms[0].newquantity.value )  ) /  parseFloat( document.forms[0].newsellablequantity.value );
		divisor = parseInt(divisor);
		var remainder = parseFloat( document.forms[0].newquantity.value ) - (divisor * parseFloat( document.forms[0].newsellablequantity.value ) );
		var zero=0;
		if(  remainder.toFixed(4) == zero.toFixed(4) )
		{
		}
		else
		{
			alert( "<bean:message bundle = "AM" key = "am.quantityadjusted"/>" );
			document.forms[0].newquantity.value = parseFloat( document.forms[0].newsellablequantity.value ) * (divisor + 1); 
		}
		
		/*var remainder = ( parseFloat( document.forms[0].newquantity.value ) - parseFloat( document.forms[0].newminimumquantity.value ) ) %  parseFloat( document.forms[0].newsellablequantity.value );
		
		if(  remainder == 0 )
		{
		}
		else
		{
			var temp = ( parseFloat( document.forms[0].newquantity.value ) - parseFloat( document.forms[0].newminimumquantity.value ) - parseFloat( remainder ) ) /  parseFloat( document.forms[0].newsellablequantity.value );
			alert( "<bean:message bundle = "AM" key = "am.quantityadjusted"/>" );
			document.forms[0].newquantity.value = parseFloat( document.forms[0].newminimumquantity.value ) + ( parseFloat( document.forms[0].newsellablequantity.value ) * ( parseFloat( temp ) + 1 ) ); 
		}*/
		
		document.forms[0].newestimatedtotalcost.value = parseFloat( document.forms[0].newquantity.value ) * parseFloat( document.forms[0].newestimatedunitcost.value );
		document.forms[0].newestimatedtotalcost.value = round_func( document.forms[0].newestimatedtotalcost.value );
		
		if( ( document.forms[0].newproformamargin.value == "" ) || ( document.forms[0].newproformamargin.value == "null" ) || ( document.forms[0].newproformamargin.value >= "1" ) )
		{ 
			document.forms[0].newproformamargin.value = "0.0";
		}
	
		if( isFloat( document.forms[0].newproformamargin.value ) ) 
		{ 
			document.forms[0].newpriceunit.value = parseFloat( document.forms[0].newestimatedunitcost.value ) / ( 1 - parseFloat( document.forms[0].newproformamargin.value ) );
			document.forms[0].newpriceunit.value = round_func( document.forms[0].newpriceunit.value );
		}
		
		if( isFloat( document.forms[0].newquantity.value ) ) 
		{ 
			document.forms[0].newpriceextended.value = parseFloat( document.forms[0].newpriceunit.value ) * parseFloat( document.forms[0].newquantity.value );
			document.forms[0].newpriceextended.value = round_func( document.forms[0].newpriceextended.value );
		}
	}
}

//Start :Added by Amit For Mass Delete

function del()
{

	
	var submitflag = 'false';
	if( <%= freight_size %> != 0 )
	{
		if( <%= freight_size %> == 1 )
		{
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newfreightid.checked ) )
			{
				alert( "<bean:message bundle = "AM" key = "am.freight.selectresourceformassdelete"/>" );
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
		}
		else
		{
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  		}
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newfreightid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.freight.selectresourceformassdelete"/>" );
				return false;
		  	}
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.freight.selectresourceformassdelete"/>" );
	}
	
	if( document.forms[0].newfreightid.checked ) 
  	{
	  	
	  	if( document.forms[0].newfreightnamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.freight.freightenter"/>" );
 			document.forms[0].newfreightname.focus();
 			return false;
 		}
	
  	}
  	else
  	{
  	}

	if(submitflag =='true')
	{
		var convel = confirm( "<bean:message bundle = "AM" key = "am.resource.delete.confirm"/>" );
		if( convel )
		{
			document.forms[0].action = "FreightResourceAction.do?ref=Delete&Activitylibrary_Id=<%= Id %>&fromResourceTabular=Freight";			
			document.forms[0].submit();
			return true;	
		}
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.freight.selectresourceformassdelete"/>" );
		return false;
	}
	
}

//End : For Mass delete
//Start:Added By Amit For Mass Approve
function massApprove()
{
	
	
	var submitflag = 'false';
	if( <%= freight_size %> != 0 )
	{
		
		if( <%= freight_size %> == 1 )
		{
			
			if( !( document.forms[0].check.checked ) && !(document.forms[0].newfreightid.checked ) )
			{
				
				alert( "<bean:message bundle = "AM" key = "am.freight.selectresourceformassapprove"/>" );
				return false;
			}
			if(document.forms[0].check.checked )
			{
				submitflag = 'true';
			}
		}
		else
		{
			
			for( var i = 0; i<document.forms[0].check.length; i++ )
		  	{
		  		if( document.forms[0].check[i].checked ) 
		  		{
		  			submitflag = 'true';
		  			
		  		}
		  		
		  	}
		  	if( submitflag == 'false' && !( document.forms[0].newfreightid.checked ) )
		  	{
		  		alert( "<bean:message bundle = "AM" key = "am.freight.selectresourceformassapprove"/>" );
				return false;
		  	}
		}
	}
	else
	{
		if( !document.forms[0].newfreightid.checked )
		{
			alert( "<bean:message bundle = "AM" key = "am.freight.selectresourceformassapprove"/>" );
		}
	}
	
	if( document.forms[0].newfreightid.checked ) 
  	{
	  	
	  	if( document.forms[0].newfreightnamecombo.value == "0" )
 		{
 			alert( "<bean:message bundle = "AM" key = "am.freight.selectresourceformassapprove"/>" );
 			document.forms[0].newfreightname.focus();
 			return false;
 		}
	
  	}
  	else
  	{
  	}

	if(submitflag =='true')
	{

		document.forms[0].action = "FreightResourceAction.do?ref=MassApprove&Activitylibrary_Id=<%= Id %>&Type=F&Status=A";			
		document.forms[0].submit();
		return true;
	}
	else
	{
		alert( "<bean:message bundle = "AM" key = "am.freight.selectresourceformassapprove"/>" );
		return false;
	}
}
//End :For Mass Approve





</script>

</body>
</html:html>