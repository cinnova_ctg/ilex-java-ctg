<!DOCTYPE HTML>

<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ page language = "java" %>


<%
	String Activitylibrary_Id = ( String ) request.getAttribute( "Activitylibrary_Id" );
	String querystring = ( String ) request.getAttribute( "querystring" );
	if( request.getAttribute( "Type" ).equals( "M" ) )
	{
%>
		<jsp:forward page="../MaterialResourceAction.do">
			
				<jsp:param name = "ref" value = "View" /> 
				<jsp:param name = "Activitylibrary_Id" value = "<%= Activitylibrary_Id %>" /> 
				<jsp:param name = "querystring" value = "<%= querystring %>" /> 
		</jsp:forward>
<%
	}

	if( request.getAttribute( "Type" ).equals( "L" ) )
	{
%>
		<jsp:forward page="../LaborResourceAction.do">
			
				<jsp:param name = "ref" value = "View" /> 
				<jsp:param name = "Activitylibrary_Id" value = "<%= Activitylibrary_Id %>" /> 
				<jsp:param name = "querystring" value = "<%= querystring %>" /> 
		</jsp:forward>
<%
	}

	if( request.getAttribute( "Type" ).equals( "F" ) )
	{
%>
		<jsp:forward page="../FreightResourceAction.do">
			
				<jsp:param name = "ref" value = "View" /> 
				<jsp:param name = "Activitylibrary_Id" value = "<%= Activitylibrary_Id %>" /> 
				<jsp:param name = "querystring" value = "<%= querystring %>" /> 
		</jsp:forward>
<%
	}

	if( request.getAttribute( "Type" ).equals( "T" ) )
	{
%>
		<jsp:forward page="../TravelResourceAction.do">
			
				<jsp:param name = "ref" value = "View" /> 
				<jsp:param name = "Activitylibrary_Id" value = "<%= Activitylibrary_Id %>" /> 
				<jsp:param name = "querystring" value = "<%= querystring %>" /> 
		</jsp:forward>
<%
	}
%>