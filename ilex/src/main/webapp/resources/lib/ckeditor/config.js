﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	config.toolbar = 'MyToolbar'; 
	config.toolbar_MyToolbar =[];
	CKEDITOR.config.width = 450;
	CKEDITOR.config.height = 200; 
	config.resize_minWidth = 450;
	config.resize_minHeight = 200;
	config.resize_maxWidth = 450;
	config.resize_maxHeight = 200;
	config.disableObjectResizing = true;
	config.toolbarCanCollapse = false;
	config.fullPage = false;
	config.scayt_autoStartup = true;
	config.pasteFromWordRemoveStyles = false;
};