<%
	String Material_Id = "";

	if( request.getAttribute( "Material_Id" ) != null )
		Material_Id = ( String ) request.getAttribute( "Material_Id" );
	
%>



<script>

function materialDel() 
{
		var convel = confirm( "<bean:message bundle = "AM" key = "am.resource.delete.confirm"/>" );
		if( convel )
		{
			document.forms[0].action = "ResourceListAction.do?type=Delete&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&Material_Id=<%= Material_Id %>&Activity_Id=<%= Activity_Id %>&viewM=M";
			document.forms[0].submit();
			return true;	
		}
}


function materialAddComment()
{
	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&addendum_id=<%=addendum_id%>&Type=pm_resM&ref=<%=chkaddendum%>&Resource_Id=<%= Material_Id %>&Activity_Id=<%= Activity_Id %>';
	<%}
	else
	{%>
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?addendum_id=<%=addendum_id%>&Type=pm_resM&ref=<%=chkaddendum%>&Resource_Id=<%= Material_Id %>&Activity_Id=<%= Activity_Id %>';
	<%}%>
	
	
	
	document.forms[0].submit();
	return true;
}

function materialViewComment()
{
	
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?addendum_id=<%=addendum_id%>&Type=pm_resM&ref=<%=chkaddendum%>&Id=<%= Material_Id %>&Activity_Id=<%=Activity_Id%>';
	document.forms[0].submit();
	return true;
}

function materialManageSow()
{
	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'ManageSOWAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resM&Id=<%= Material_Id %>&Activity_Id=<%=Activity_Id%>';
	
	<%}
	else
	{%>
		document.forms[0].action = 'ManageSOWAction.do?ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resM&Id=<%= Material_Id %>&Activity_Id=<%=Activity_Id%>';
	<%}%>
	
	document.forms[0].submit();
	return true;
}


function materialManageAssumption()
{
	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'ManageAssumptionAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resM&Id=<%= Material_Id %>&Activity_Id=<%=Activity_Id%>';
	<%}
	else
	{%>
		document.forms[0].action = 'ManageAssumptionAction.do?ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resM&Id=<%= Material_Id %>&Activity_Id=<%=Activity_Id%>';
	<%}%>
	
	document.forms[0].submit();
	return true;
}

function materialViewDetails() {
		document.forms[0].action = 'MaterialDetailAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Material_Id=<%=Material_Id%>&Activity_Id=<%=Activity_Id%>';
		document.forms[0].submit();
		return true;
}




</script>