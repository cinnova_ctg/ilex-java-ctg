<SCRIPT language=JavaScript1.2>
    if (isMenu) {
<% if (!nettype . equals("dispatch")) { %>
        arMenu1 = new Array(
                140,
                findPosX('pop1'), findPosY('pop1'),
                "", "",
                "", "",
                "", "",
                "New Ticket", "JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX", 0,
                "Contract Documents", "#", 1,
                "Custom Fields", "AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX", 0,
                "Customer Reference", "AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX", 0,
                "External Sales Agent", "ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX", 0,
                "Documents", "#", 1,
                "View Job Sites", "SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX", 0,
                "<bean:message bundle = "AM" key = "am.summary.Partnerinfo"/>", "#", 1,
                "Team", "AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX", 0,
                "MPO", "MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX", 0,
                "Job Notes", "JobNotesAction.do?appendixid=<%= appendixid %>", 0,
                "Workflow Checklist Items", "ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>", 0
                )
                arMenu1_2 = new Array(
                        "NetMedX", "#", 1
                        )

                arMenu1_2_1 = new Array(
                        "Send", "javascript:sendemail();", 0,
                        "View", "#", 1
                        )

                arMenu1_2_1_2 = new Array(
                        "PDF", "javascript:view('pdf');", 0
                        )

                arMenu1_6 = new Array(
                        "NetMedX History", "EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX", 0,
                        "Support Documents", "", 1
                        )

                arMenu1_6_2 = new Array(
                        "Upload", "EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix", 0,
                        "History", "EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX", 0
                        )

                arMenu1_8 = new Array(
                        "<bean:message bundle = "AM" key="am.Partnerinfo.sow"/>", "PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=<%= nettype %>&ownerId=<%= loginUserId %>&fromPage=NetMedX", 0,
                        "<bean:message bundle = "AM" key="am.Partnerinfo.assumptions"/>", "PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=<%= nettype %>&ownerId=<%= loginUserId %>&fromPage=NetMedX", 0,
                        "<bean:message bundle = "AM" key="am.Partnerinfo.Inst_cond"/>", "PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX", 0
                        )
                arMenu2 = new Array(
                        120,
                        findPosX('pop2'), findPosY('pop2'),
                        "", "",
                        "", "",
                        "", "",
                        "<bean:message bundle = "PRM" key = "prm.netmedx.redlinereport"/>", "javascript:openreportschedulewindow('redline')", 0,
                        "<bean:message bundle = "PRM" key = "prm.netmedx.detailedsummary"/>", "javascript:openreportschedulewindow('detailsummary')", 0,
                        "<bean:message bundle = "PRM" key = "prm.netmedx.csvsummary"/>", "/content/reports/csv_summary/", 0,
                        "<bean:message bundle = "PRM" key = "prm.netmedx"/>", "#", 1
                        )

                arMenu2_4 = new Array(
                        "<bean:message bundle = "PRM" key = "prm.netmedx.detailedsummary.report"/>", "#", 1
                        )

                arMenu2_4_1 = new Array(
                        "<bean:message bundle = "PRM" key = "prm.netmedx.update.report"/>", "ReportGeneration.do?firsttime=true", 0
                        )
<% } else { %>
        arMenu1 = new Array(
                        140,
                        findPosX('pop2'), findPosY('pop2'),
                        "", "",
                        "", "",
                        "", "",
                        "<bean:message bundle = "PRM" key = "prm.netmedx.redlinereport"/>", "javascript:openreportschedulewindowdispatch('redline')", 0,
                        "<bean:message bundle = "PRM" key = "prm.netmedx.detailedsummary"/>", "javascript:openreportschedulewindowdispatch('detailsummary')", 0,
                        "<bean:message bundle = "PRM" key = "prm.netmedx.csvsummary"/>", "/content/reports/csv_summary/", 0,
                        "Account Summary Report", "javascript:summarywindow('accountsummary')", 0,
                        "Owner Summary Report", "javascript:ownersummarywindow()", 0
                        )
<% } %>
          document.write("<SCRIPT LANGUAGE='JavaScript1.2' SRC='javascript/hierMenus.js'><\/SCRIPT>");
    }
</SCRIPT>