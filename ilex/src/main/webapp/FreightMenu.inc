<%
	String Freight_Id = "";

	if( request.getAttribute( "Freight_Id" ) != null )
		Freight_Id = ( String )request.getAttribute( "Freight_Id" );
	
%>

<script>

function freightDel() 
{
		var convel = confirm( "<bean:message bundle = "AM" key = "am.resource.delete.confirm"/>" );
		if( convel )
		{
			document.forms[0].action = "ResourceListAction.do?type=Delete&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&Freight_Id=<%= Freight_Id %>&Activity_Id=<%= Activity_Id %>&viewF=F";
			document.forms[0].submit();
			return true;	
		}
}

function freightAddComment()
{

	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&addendum_id=<%=addendum_id%>&Type=pm_resF&ref=<%=chkaddendum%>&Resource_Id=<%= Freight_Id %>&Activity_Id=<%= Activity_Id %>';
	<%}
	else
	{%>
		document.forms[0].action = 'MenuFunctionAddCommentAction.do?addendum_id=<%=addendum_id%>&Type=pm_resF&ref=<%=chkaddendum%>&Resource_Id=<%= Freight_Id %>&Activity_Id=<%= Activity_Id %>';
	<%}%>
	
	document.forms[0].submit();
	return true;  
}

function freightManageSow()
{
	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'ManageSOWAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resF&Id=<%= Freight_Id %>&Activity_Id=<%= Activity_Id %>';
	
	<%}
	else
	{%>
		document.forms[0].action = 'ManageSOWAction.do?ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resF&Id=<%= Freight_Id %>&Activity_Id=<%= Activity_Id %>';
	<%}%>
	
	document.forms[0].submit();
	return true;
}

function freightManageAssumption()
{
	<%if( request.getAttribute( "from" ) != null )
	{
		fromflag = request.getAttribute( "from" ).toString();  
	%>
		document.forms[0].action = 'ManageAssumptionAction.do?from=<%= fromflag %>&jobid=<%= jobid %>&ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resF&Id=<%= Freight_Id %>&Activity_Id=<%= Activity_Id %>'
	<%}
	else
	{%>
		document.forms[0].action = 'ManageAssumptionAction.do?ref=<%=chkaddendum%>&addendum_id=<%=addendum_id%>&function=View&Type=pm_resF&Id=<%= Freight_Id %>&Activity_Id=<%= Activity_Id %>'
	<%}%>
	
	document.forms[0].submit();
	return true;
}

function freightViewComment()
{
	document.forms[0].action = 'MenuFunctionViewCommentAction.do?addendum_id=<%=addendum_id%>&Type=pm_resF&ref=<%=chkaddendum%>&Id=<%= Freight_Id %>&Activity_Id=<%= Activity_Id %>';
	document.forms[0].submit();
	return true;
}

function freightViewDetails() {
		document.forms[0].action = 'FreightDetailAction.do?addendum_id=<%=addendum_id%>&ref=<%=chkaddendum%>&Freight_Id=<%=Freight_Id%>&Activity_Id=<%=Activity_Id%>';
		document.forms[0].submit();
		return true;
}

</script>