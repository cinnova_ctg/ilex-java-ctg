<SCRIPT language = JavaScript1.2>
var appendixSend = '<bean:message bundle = "pm" key = "appendix.detail.sendAppendix.noprivilegestatus"/>';
var appendixCRS = '<bean:message bundle = "pm" key = "appendix.detail.upload.state"/>';
if (isMenu) {
arMenu1 = new Array()
arMenu2 = new Array(
130,
findPosX( 'pop2' ),findPosY( 'pop2' ),
"","",
"","",
"","",
	<%if( ( String )request.getAttribute( "appendixStatusList" ) != "" ) {%>
	"<bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/>","#",1,
	<%} else{%>
	"<bean:message bundle = "pm" key = "appendix.detail.menu.changestatus.new"/>","#",0,
	<%}%>
	"<bean:message bundle = "pm" key = "appendix.detail.viewappendix"/>","#",1,
	
	<%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
    	"<bean:message bundle = "pm" key = "appendix.detail.menu.sendappendix"/>","javascript:appendixSendEmail();",0,
	<%} else {%>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.sendappendix"/>","javascript:alert(appendixSend)",0,
	<% }
	
	if( appendixStatus.equals( "Signed" ) && appendixStatus.equals( "Inwork" )|| appendixStatus.equals( "Cancelled" ) ){%>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.formaldocdate.noprivilegeinworkstatus"/>')",0,
		"<bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.endcustomer.noprivilegeinworkstatus"/>')",0,
	<%}else{%>	
		"<bean:message bundle = "pm" key = "appendix.detail.menu.setformaldocumentdate"/>","javascript:appendixFormalDocDate();",0,
		"<bean:message bundle = "pm" key = "appendix.detail.menu.setendcustomer"/>","javascript:setendcustomer();",0,
	<%}%>
	
	<%if(appendixStatus.equals( "Cancelled" )){%>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>');",0,
	<%}else{%>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.projectmanager"/>","javascript:addcontact();",0,
	<%}%>
	"<bean:message bundle = "pm" key = "appendix.detail.comments"/>","#",1,
	
	<%if( appendixStatus.equals( "Pending Customer Review" )){%>
		 "<bean:message bundle = "pm" key = "appendix.detail.menu.uploadappendix"/>","EntityHistory.do?function=modifyDocument&entityId=<%= Appendix_Id %>&MSA_Id=<%=MSA_Id%>&entityType=Appendix",0,
	<%}else{%>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.uploadappendix"/>","javascript:alert(appendixCRS)",0,	
	<%}%>
	
	<%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Review" )){%>
		<%if(appendixStatus.equals( "Cancelled" )){%>
			"<bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>')",0,		
		<%}else{%>
			"<bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix"/>')",0,		
		<%}%>
	<%}else{%>	
	"<bean:message bundle = "pm" key = "appendix.detail.menu.editappendix"/>","javascript:appendixEdit();",0,
	<%}%>
	"External Sales Agent","ESAEditAction.do?ref=view&appendixId=<%= Appendix_Id %>&msaId=<%= MSA_Id%>",0,
	"<bean:message bundle = "pm" key = "appendix.detail.menu.documents"/>","",1,
	"<bean:message bundle = "pm" key = "appendix.detail.menu.deleteappendix"/>","javascript:appendixDel();",0
)

<% 
if( ( String )request.getAttribute( "appendixStatusList" ) != "" ) {%>
arMenu2_1=new Array(
	<%= ( String ) request.getAttribute( "appendixStatusList" ) %>
)
<%}%>

arMenu2_2=new Array(
	"Details","AppendixDetailAction.do?MSA_Id=<%= MSA_Id%>&Appendix_Id=<%= Appendix_Id%>",0,
	<%if( !appendixStatus.equals( "Draft" ) && !appendixStatus.equals( "Cancelled" ) && !appendixStatus.equals( "Review" )){%>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.viewpdf"/>","javascript:appendixView('pdf');",0
	<%}else{%>
	
		"<bean:message bundle = "pm" key = "appendix.detail.menu.viewpdf"/>","javascript:appendixView('pdf');",0,
		"<bean:message bundle = "pm" key = "appendix.detail.menu.viewrtf"/>","javascript:appendixView('rtf');",0,
		<% if( request.getAttribute( "addendumlist" ) != "" ){%>
			"<bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/>","javascript:appendixView('html');",0,
			<% if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) { %>
			"History","",1,
			"<bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/>","javascript:appendixView('excel');",0
			<%}else {%>
			"History","",1
			<%}
		}else{%>
			<% if(!appendixType.equalsIgnoreCase("NetMedX") && !appendixType.equalsIgnoreCase("Hardware")) { %>
				"<bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/>","javascript:appendixView('html');",0,
				"<bean:message bundle = "pm" key = "appendix.detail.menu.viewexcel"/>","javascript:appendixView('excel');",0
			<%}else {%>
				"<bean:message bundle = "pm" key = "appendix.detail.menu.viewhtml"/>","javascript:appendixView('html');",0
			<%}
		}
	}%>
)

<% if( ( String )request.getAttribute( "addendumlist" ) != "" ) {%>
arMenu2_2_5=new Array(
<%= ( String ) request.getAttribute( "addendumlist" ) %>
)
<%} %>

	arMenu2_7 = new Array(
	"<bean:message bundle = "pm" key = "appendix.detail.menu.viewall"/>","javascript:appendixViewComments();",0,
	<%if(appendixStatus.equals( "Cancelled" )){%>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/>","javascript:alert('<bean:message bundle = "pm" key = "appendix.detail.menu.noedittransappendix.cancelled"/>');",0
	<%}else{%>
		"<bean:message bundle = "pm" key = "appendix.detail.menu.addcomment"/>","javascript:appendixAddComments();",0
	<%}%>
	)
		
	arMenu2_11 = new Array(
	"Appendix History","EntityHistory.do?entityId=<%= Appendix_Id %>&MSA_Id=<%=MSA_Id%>&entityType=Appendix&function=viewHistory",0,
	"Support Documents","",1
	)

	arMenu2_11_2 = new Array(
	"Upload","EntityHistory.do?entityId=<%= Appendix_Id %>&appendixId=<%= Appendix_Id %>&entityType=Appendix Supporting Documents&function=addSupplement&linkLibName=Appendix",0,
	"History","EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= Appendix_Id %>&linkLibName=Appendix&function=supplementHistory",0
	)
	
document.write("<SCRIPT LANGUAGE = 'JavaScript1.2' SRC = 'javascript/hierMenus.js'><\/SCRIPT>");
}
</script>