<!DOCTYPE HTML>

<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "javascript/popcalendar.js"></script>
<script language = "JavaScript" src = "javascript/JQueryMain.js"></script>
	<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
	<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />
<title>MPOList</title>


<script>
		function fun_HideShowBlock(index,isClicked)
		{
			if(isClicked!=""){
				<c:if test="${requestScope.add ne 'add'}">
						url = "MPOAction.do?type="+index;
				</c:if>
				<c:if test="${requestScope.add eq 'add'}">
					url = "MPOAction.do?type="+index+"&actionTaken=AddNewMPO";
				</c:if>
				document.forms[0].action=url;
				document.forms[0].submit();
			}
			else{
		        var len=document.all.tab.length;
		        for(var id=0;id<len;id++){
		        	if(document.all.tab[id].style.display=="block"){
		        		document.all.tab[id].style.display="none";
		        		}
		        }
				document.all.tab[index].style.display="block"; 
				buttonPressed(index);
			}	
 		}
	function openListType(nonPm){
			var len=document.all.rec.length;
			for(var id=0;id<len;id++){
        		document.all.rec[id].style.display="block";
        	}
        	if(nonPm=="yes"){
				var nonPMlen=document.all.nonPMRec.length;
		       	for(var id=0;id<nonPMlen;id++){
	        		document.all.nonPMRec[id].style.display="block";
	        	}
        	}else if(nonPm=="no"){
				var pMlen=document.all.PMRec.length;
		       	for(var id=0;id<pMlen;id++){
	        		document.all.PMRec[id].style.display="block";
	        	}
        	}
 		
	}
		function openAllTab(len,show,isClicked)
		{
		for(var i=0;i<len;i++){
			document.all.TOP_BUTTON[3*i].style.display='block';
			document.all.TOP_BUTTON[3*i+1].style.display='block';
			document.all.TOP_BUTTON[3*i+2].style.display='block';
		    document.all.TOP_BUTTON[3*i+1].style.display='block';
			}
		fun_HideShowBlock(show,isClicked);
		if(show==1)
			document.all.mainTable.style.display='';
		}
		
		function buttonPressed(idx)
		{
			for(var i=0;i<document.all.TOP_BUTTON.length/3;i++) {

				document.all.TOP_BUTTON[3*i+1].style.fontSize='10';
				//document.all.TOP_BUTTON[3*i+1].width='150';
				document.all.TOP_BUTTON[3*i+1].width=document.all.TOP_BUTTON[3*i+1].innerHTML.length*8;
				document.all.TOP_BUTTON[3*i+1].style.fontFamily='Verdana|Arial|Helvetica|sans-serif';
				document.all.TOP_TAB[i].style.width='5';

				if(i==idx)
				{
					if(idx==0){
						document.all.TOP_BUTTON[3*i].style.display='block';
						document.all.TOP_BUTTON[3*i+1].style.display='block';
						document.all.TOP_BUTTON[3*i+2].style.display='block';
					}
					document.all.TOP_BUTTON[3*i].innerHTML='<img src="docm/images/left-cor-light.gif" width="5" height="18" border="0">';
					document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#D9DFEF';
					document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="docm/images/right-cor-light.gif" width="5" height="18"  border="0">';
					document.all.TOP_BUTTON[3*i+1].style.borderBottom = '0px #D9DFEF solid';
					document.all.TOP_BUTTON[3*i+1].style.fontWeight='bold';
				}
				else//Restore Remaining
				{
					if(idx==0){
						document.all.TOP_BUTTON[3*i].style.display='none';
						document.all.TOP_BUTTON[3*i+1].style.display='none';
						document.all.TOP_BUTTON[3*i+2].style.display='none';
					 }else{
						document.all.TOP_BUTTON[3*i].innerHTML='<img src="docm/images/left-cor.gif" width="5" height="18"  border="0">';
						document.all.TOP_BUTTON[3*i+1].style.backgroundColor = '#B5C8D9';
						document.all.TOP_BUTTON[3*i+2].innerHTML='<img src="docm/images/right-cor.gif" width="5" height="18"  border="0">';
					    document.all.TOP_BUTTON[3*i+1].style.borderBottom = '2px #B5C8D9 solid';
						document.all.TOP_BUTTON[3*i+1].style.fontWeight='normal';
					}
				}
			}
		}
		function HideShowDocumentListAddTab(type){
			if(type=='openAdd'){
				document.all.addTab.style.display="block";
				var len=document.all.ListTab.length;
				for(var i=0;i<len;i++){
					document.all.ListTab[i].style.display="none";
				}			
			}else if(type=='search'){
				document.all.buttonRow.style.display="block";
				document.all.addTab.style.display="block";
				var len=document.all.ListTab.length;
				var len1=document.all.action.length;
				var len2=document.all.check.length;
				for(var i=0;i<len;i++){
				document.all.ListTab[i].style.display="block";
				if((i>1) && (i%2==0))
				document.all.ListTab[i].disabled=true;
				}	
				for(var i=0;i<len1;i++){
					document.all.action[i].style.display="none";
				}	
				for(var i=0;i<len2;i++){
					document.all.check[i].style.display="block";
				}		
			}
		}
		
</script>
<script>
function exceedSubtotal(index,textObj)
{
	var d = new Date();
	var percentageEstimatedCost = 0;
	var percentageEstimatedTotalCost = 0;
	var planSubTotal = 0;
	var tableObj = document.getElementById('messageTable');
	tableObj.innerText = "";
	var len = document.all.mpoActivityResource.length;
	if(!len){
		if(document.all.mpoSubTotal.value == 0.0){
			planSubTotal = document.all.mpoActivityPlanUnitCost.value;
			percentageEstimatedCost = ( document.all.mpoUnit.value * 5 )/100;
			percentageEstimatedTotalCost = eval(document.all.mpoUnit.value) + eval(percentageEstimatedCost);
		} else{
			planSubTotal = document.all.mpoActivityPlanSubTotal.value;
			percentageEstimatedCost = ( document.all.mpoSubTotal.value * 5 )/100;
			percentageEstimatedTotalCost = eval(document.all.mpoSubTotal.value) + eval(percentageEstimatedCost);
		}
	} else{
		if(document.all.mpoSubTotal[index].value == 0.0){
			planSubTotal = document.all.mpoActivityPlanUnitCost[index].value;
			percentageEstimatedCost = ( document.all.mpoUnit[index].value * 5 )/100;
			percentageEstimatedTotalCost = eval(document.all.mpoUnit[index].value) + eval(percentageEstimatedCost);
		} else{
			planSubTotal = document.all.mpoActivityPlanSubTotal[index].value;
			percentageEstimatedCost = ( document.all.mpoSubTotal[index].value * 5 )/100;
			percentageEstimatedTotalCost = eval(document.all.mpoSubTotal[index].value) + eval(percentageEstimatedCost);
		}
	}
	if(planSubTotal > percentageEstimatedTotalCost)
	{
		alert('Planned Subtotal Cost cannot exceed Estimated Subtotal Cost by 5%.');
		var oRow=tableObj.insertRow();
		var oCell=oRow.insertCell(0);
		oCell.className="colLight";
		oCell.innerHTML="<b>Planned Subtotal Cost cannot exceed Estimated Subtotal Cost by 5%.</b>";
		document.getElementById("hideMessageRow").style.display = '';
		/*
		tableObj.rows(0).cells(0).style.display ="";
		tableObj.rows(0).cells(0).innerText = "Planned Subtotal Cost cannot exceed Estimated Subtotal Cost by 5%."
		tableObj.rows(0).cells(0).className ="colLight";
		*/
		textObj.focus();
	} else{
		tableObj.innerText = "";
		document.getElementById("hideMessageRow").style.display = 'none';
	}
//	var d1 = new Date();
//	alert("exceedSubtotal::"+d1.getSeconds()+ " - " + d.getSeconds());
	
}// end of Function
</script>

<script>
function onSave(obj)
{
	var confirmed ;
		confirmed = 1;
	trimTextAreaFields();
	if(obj == '1')
	{
		var percentageEstimatedCost;
		var percentageEstimatedTotalCost;
		if(isExists(document.all.mpoActivityResource))
		{
			var len = document.all.mpoActivityResource.length;
			if(!len)
			{
				if( document.all.mpoActivityIsWOShow.checked == true && document.all.mpoActivityResource.checked != true ){
						alert('Firstly resource needs to be checked.')
						return false;
				}
				// commented for making page behaviour similar for all mpoTypes
				/*if(document.all.mpoItemType.value == 'fp'){
					document.all.mpoActivityPlanQuantity.value = '';
					document.all.mpoActivityPlanUnitCost.value = '';
				}*/
				if(document.all.mpoActivityResource.checked != true ){
					document.all.resourceHidden.value=document.all.mpoActivityResource.value+'~'+"N";
				}
				else
				{
					document.all.resourceHidden.value=document.all.mpoActivityResource.value+'~'+"Y";
				}
				if(document.all.mpoActivityResource.checked == true ){
					if( document.all.mpoActivityIsWOShow.checked != true){
						document.all.isShowHidden.value='N'
					}
				}
				// commented for making page behaviour similar for all mpoTypes
				//if(document.all.mpoItemType.value!='fp'){
					if(document.all.mpoActivityResource.checked == true)
					{
						if(document.all.mpoActivityPlanQuantity.value == ''){
							alert('Please fill Planned Quantity.');
							document.all.mpoActivityPlanQuantity.focus();
							return false;
						}
						if(document.all.mpoActivityPlanUnitCost.value == ''){
							alert('Please fill Planned Unit Cost.');
							document.all.mpoActivityPlanUnitCost.focus();
							return false;
						}
					}
					if(document.all.mpoActivityResource.checked == true)
					{
						if(document.all.mpoActivityPlanQuantity.value != '' && document.all.mpoActivityPlanUnitCost.value != ''){
							document.all.mpoTotal.value = (eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value));
							document.all.mpoSubTotal.value = (eval (document.forms[0].mpoUnit.value * eval (document.all.mpoTotal.value)));
							document.all.mpoActivityPlanSubTotal.value = (document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
							percentageEstimatedCost = ( document.all.mpoSubTotal.value * 5 )/100;
							percentageEstimatedTotalCost = eval(document.all.mpoSubTotal.value) + eval(percentageEstimatedCost);
							if(document.all.mpoActivityPlanSubTotal.value > percentageEstimatedTotalCost)
							{
								alert('As Planned Subtotal cannot exceed Estimated Subtotal by 5% ');
								//document.forms[0].Save.disabled = "true";
								document.forms[0].mpoActivityPlanUnitCost.focus();
								return false;
							}
							
							
						}
					}
				//}
			}
			else
			{
				for(var i=0;i<len;i++)
				{
					if( document.all.mpoActivityIsWOShow[i].checked == true && document.all.mpoActivityResource[i].checked != true ){
						alert('Firstly resource needs to be checked.')
						return false;
					}
					// commented for making page behaviour similar for all mpoTypes
					/*if(document.all.mpoItemType.value == 'fp'){
						document.all.mpoActivityPlanQuantity[i].value = '';
						document.all.mpoActivityPlanUnitCost[i].value = '';
					}*/
					if(document.all.mpoActivityResource[i].checked != true ){
						document.all.resourceHidden[i].value=document.all.mpoActivityResource[i].value+'~'+"N";
					}
					else
					{
						document.all.resourceHidden[i].value=document.all.mpoActivityResource[i].value+'~'+"Y";
					}
					if(document.all.mpoActivityResource[i].checked == true ){
						
						if( document.all.mpoActivityIsWOShow[i].checked != true){
							document.all.isShowHidden[i].value='N'
						}
					}
					// commented for making page behaviour similar for all mpoTypes
					//if(document.all.mpoItemType.value!='fp'){
						if(document.all.mpoActivityResource[i].checked == true)
						{
							if(isBlank(document.all.mpoActivityPlanQuantity[i].value)){
								alert('Please fill Planned Quantity.');
								document.all.mpoActivityPlanQuantity[i].focus();
								return false;
							}
							if(isBlank(document.all.mpoActivityPlanUnitCost[i].value) ){
								alert('Please fill Planned Unit Cost.');
								document.all.mpoActivityPlanUnitCost[i].focus();
								return false;
							}
						}
						if(document.all.mpoActivityResource[i].checked == true)
						{
							if(document.all.mpoActivityPlanQuantity[i].value != '' && document.all.mpoActivityPlanUnitCost[i].value != ''){
								document.all.mpoTotal[i].value = (eval (document.forms[0].mpoAct[i].value * document.forms[0].mpoRes[i].value));
								document.all.mpoSubTotal[i].value = (eval (document.forms[0].mpoUnit[i].value * eval (document.all.mpoTotal[i].value)));
								document.all.mpoActivityPlanSubTotal[i].value = (document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
								percentageEstimatedCost = ( document.all.mpoSubTotal[i].value * 5 )/100;
								percentageEstimatedTotalCost = eval(document.all.mpoSubTotal[i].value) + eval(percentageEstimatedCost);
								if(document.all.mpoActivityPlanSubTotal[i].value > percentageEstimatedTotalCost)
								{
									alert('As Planned Subtotal cannot exceed Estimated Subtotal by 5% ');
									//document.forms[0].Save.disabled = "true";
									document.forms[0].mpoActivityPlanUnitCost[i].focus();
									return false;
								}
								
								
							}
						}
					//}
				}
			}
		}
		var deltaCost;
		deltaCost = eval(document.forms[0].estimatedAuthorizedCost.value)+eval(document.forms[0].estimatedAuthorizedCost.value*10)/100;
		if(document.forms[0].estimatedTotalCost.value > deltaCost  )
		{
			confirmed = confirm("Authorized cost exceeds the allowable margin 10%.")
		}
	}
	/*if(obj=='2')
	{
		if(isBlank(document.all.special_condition.value))
		{
			alert('Special Conditions can not be blank');
			return false.
		}
	}*/
	if(obj == '3')
	{	
		var len = document.all.woRE.length;
		if(!len)
		{
			if(document.all.woRE.checked!=true)
				document.all.workOrderCheck.value = document.all.woRE.value+'~'+'N';
			else
				document.all.workOrderCheck.value = document.all.woRE.value+'~'+'Y';
		}
		else
		{
			for(var i=0;i<len;i++)
			{
				if(document.all.woRE[i].checked!=true)
					document.all.workOrderCheck[i].value = document.all.woRE[i].value+'~'+'N';
				else
					document.all.workOrderCheck[i].value = document.all.woRE[i].value+'~'+'Y';
			}
		}
	}
	if(obj=='5')
	{
		if(is_empty(document.all.devTitle,'Title')){
			return false;
		}
		var val=getCheckedValue(document.all.devType);
		if(is_radio(val,'Type')) {
			return false;
		}
		//added mobile upload radio button validation
		var mobileVal=getCheckedValue(document.all.mobileAllowed);
		if(is_radio(mobileVal,'Mobile')) {
			return false;
		}
		if(document.all.devFormat.value == -1){
			alert('Format is required.');
			return false;
		}
	}
	<c:if test="${empty requestScope.clicked}">
		var url = "MPOAction.do?hmode=Save&type="+obj+"&mode=add";
	</c:if>
	<c:if test="${not empty requestScope.clicked}">
		var url = "MPOAction.do?hmode=Save&type="+obj+"&mode=edit";
	</c:if>
	if(confirmed > 0){
		document.forms[0].action=url;
		document.forms[0].submit();
	}
	else{
		return false;
	}
}
	function is_empty(chkObj, message)
		{
			if(chkObj.value == '')
				{
					alert(message + ' is required.');
					chkObj.focus();
					return true;
				}
			return false;

		}

		function is_radio(chkObj, message)
		{
			if (!chkObj.checked) {
				alert(message +' is required.');
				return true;
				}
			return false;
		}

		function getCheckedValue(radioObj) {
			if(!radioObj)
				return "";
			var radioLength = radioObj.length;
			if(radioLength == undefined)
				if(radioObj.checked)
					return radioObj;
				else
					return "";
			for(var i = 0; i < radioLength; i++) {
				if(radioObj[i].checked) {
					return radioObj[i];
				}
			}
			return "";
		}
</script>

<script>
	function isExists(obj)
	{
		if(obj)
			return true;
		else
			return false;
		
	}
	
	function executeOnLoad()
	{

		<c:if test="${requestScope.tabId eq '1' &&  empty requestScope.add}">
			calculateAll();
		</c:if>
		<c:if test="${requestScope.tabId eq '1' &&  empty requestScope.isContinue}">
			changePOItem();
		</c:if>
		<c:if test="${not empty requestScope.add}">
			openAllTab(2,'<c:out value="${requestScope.tabId}"/>','<c:out value="${requestScope.isClicked }"/>');
		</c:if>
		<c:if test="${empty requestScope.add}">
			openAllTab(6,'<c:out value="${requestScope.tabId}"/>','<c:out value="${requestScope.isClicked }"/>');
		</c:if>

	}
	
$(document).ready(function(){
  	executeOnLoad();
 });
	
</script>

<%@ include file = "/NMenu.inc" %> 
<%@ include  file="/DashboardVariables.inc" %>
</head>
<body>
<form action="MPOAction" method='post' name="MPOBean"
	type="com.mind.MPO.formbean.MPOBean">
	<input type="hidden" name="msaId" value="<c:out value='${MPOBean.msaId }'/>" />
	<input type="hidden" name="mpoId" value="<c:out value='${MPOBean.mpoId }'/>" />
	
	<input type="hidden" name="fromPage" value="<c:out value='${MPOBean.fromPage }'/>" /> <html:hidden
	property="docIdList" name="MPOBean" /> <html:hidden
	property="mp_appendix_id" name="MPOBean" /> <c:if
	test="${MPOBean.mpoItemType eq 'fp'}">
	<html:hidden name ="MPOBean" property = "msaName"/>
	<html:hidden name ="MPOBean" property = "appendixName"/>
	<html:hidden name ="MPOBean" property = "isPM"/>
	<html:hidden name ="MPOBean" property = "appendixType"/>
	<c:set var="mpoItem" scope="request" value='Fixed Price' />
</c:if> <c:if test="${MPOBean.mpoItemType eq 'ht'}">
	<c:set var="mpoItem" scope="request" value='Hourly Type' />
</c:if> <c:if test="${MPOBean.mpoItemType eq 'mt'}">
	<c:set var="mpoItem" scope="request" value='Materials' />
</c:if> <c:if test="${MPOBean.mpoItemType eq 'si'}">
	<c:set var="mpoItem" scope="request" value='Show Items' />
</c:if>

					<logic:equal name = "MPOBean" property = "fromPage" value = "NetMedX">
						
						<%@ include  file="/NetMedXDashboardMenu.inc" %>
		<%if(!nettype.equals("dispatch")){ %>
							<div id="menunav">
						
						<!--  Menu change for new  -->
						
						<ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li>
        			
          	<a href="JobDashboardAction.do?tabId=2&appendixid=<%= appendixid %>&ticketType=newTicket&fromPage=NetMedX">New Ticket</a>
        </li>
        
        <li>
	        		
	       <a href="#"><span>Contract Documents</span></a>
	        			
	       <ul>
			
			<li><a href="#"><span>NetMedX</span></a><ul><li><a href="javascript:sendemail();">Send</a></li><li><a href="#"><span>View</span></a><ul><li><a href="javascript:view('pdf');">PDF</a></li></ul></li></ul></li>
	        			
	        </ul>
	        
	        </li>
				
        		
        	<li><a href="AppendixCustomerInformation.do?function=add&appendixid=<%= appendixid %>&fromPage=NetMedX">Custom Fields</a></li>
        	<li><a href="AddCustRef.do?typeid=<%= appendixid %>&appendixid=<%= appendixid %>&type=Appendix&fromPage=NetMedX">Customer Reference</a></li>
        	<li><a href="ESAEditAction.do?ref=view&appendixid=<%= appendixid %>&fromPage=NetMedX">External Sales Agent</a></li>
        	<li>
        
        	<a href="#"><span>Documents</span></a>
        				<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&entityType=Appendix&function=viewHistory&fromPage=NetMedX">NetMedX History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&appendixId=<%= appendixid %>&entityType=Appendix Supporting Documents&function=addSupplement&fromPage=NetMedX&linkLibName=Appendix">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&appendixId=<%= appendixid %>&linkLibName=Appendix&function=supplementHistory&fromPage=NetMedX">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&appendixid=<%= appendixid %>&initialCheck=true&pageType=jobsites&fromPage=NetMedX"> View Job Sites</a></li>
        		<!-- <li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=3225&amp;viewjobtype=ION&amp;ownerId=7">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=3225&amp;type=P&amp;page=appendixdashboard&amp;pageid=3225&amp;viewjobtype=ION&amp;ownerId=7">Update</a></li>
						
        			</ul>
        		</li> -->
        		<!-- <li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li> -->
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=sow&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?&fromId=<%= appendixid %>&viewType=assumption&fromType=prm_act&nettype=&ownerId=<%=ownerId%>&fromPage=NetMedX">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&fromType=prm_app&fromPage=NetMedX">Manage Special</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&appendixid=<%= appendixid %>&fromPage=NetMedX">Team</a></li>
        <!--  		<li><a href="OnlineBid.do?appendixId=3225&amp;fromPage=Appendix">Online Bid</a></li>  -->
        		<li><a href="MPOAction.do?NonPm=no&appendixid=<%= appendixid %>&fromPage=NetMedX">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
        	
        
        
        
        
        
        
        
        
        
        
        
        
        
        		<!-- <li>
        			
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">NetMedx</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=3225">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=3225">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li> -->
				
         			  	 	<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
				
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${MPOBean.mp_appendix_id }'/>&msaId=<c:out value='${MPOBean.msaId }'/>&fromPage=viewselector" class ="menufont" style="width: 120px">Search</a>
  		</li>
  		
	</ul>
</li>
</div>
	<%}else{%>
	<div id="menunav">
<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
				<li><a href="#"><span>NetMedX</span></a><ul><li><a href="#"><span>Detailed Summary Report</span></a><ul><li><a href="javascript:ReportRegenarate()">Update Report</a></li></ul></li></ul></li>
				
				<!-- <li><a href="#">NetMedx Report</a>
				<li><a href="#">Detailed Summary</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Update Report</a><li> -->
				
				
			
        		<!-- <li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li> -->
				
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${MPOBean.mp_appendix_id }'/>&msaId=<c:out value='${MPOBean.msaId }'/>&fromPage=viewselector" class ="menufont" style="width: 120px">Search</a>
  		</li>
  		
	</ul>
</li>
</div>
<%} %>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
		        <tbody><tr><td width="100%" height="21" background="images/content_head_04.jpg" colspan="4">
			     <div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${requestScope.msaId }'/>&app=null"   class="bgNone"><c:out value='${requestScope.msaName }'/></a>
														 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<%= appendixid %>"><c:out value='${requestScope.appendixName }'/></a>
														 <a ><span class="breadCrumb1">Master Purchase Order Management</span></a>	
			        </td>
			        <tr><td class="message"><c:if test="${not empty requestScope.mpoExist}"> MPO with same name already exist.</c:if></td></tr>
		        </tr>
		        
		        <tr>
					
				
				
					</td>
				</tr>
	      </tbody></table>	
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
					
							<%--  <table cellpadding="0" cellspacing="0" border="0" width = "100%">
						        <tr>
									<%if(!nettype.equals("dispatch")){ %>
									<td class=Ntoprow1 id=pop1 width=120><a class="menufont" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px"><center>Manage</center></a></td>
									<td class=Ntoprow1 id=pop2 width=120><a class="menufont" onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)" href="#" style="width: 120px"><center>Reports</center></a></td>
									<td class=Ntoprow1 id =pop3 width =120><a class="menufont" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>" style="width: 120px"><center>Search</center></a></td>
									<td class=Ntoprow1 id=pop4 width=500>&nbsp;</td>
									<%}else{%>
									<td class=Ntoprow1 id=pop2 width=120><a class=menufont onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)" href="#" style="width: 120px">Reports</a></td>
									<td class=Ntoprow1 id =pop3 width =120><a class="menufont" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${requestScope.appendixId }'/>&ownerId=<c:out value='${requestScope.ownerId }'/>&nettype=<%=nettype %>" style="width: 120px"><center>Search</center></a></td>
									<td class=Ntoprow1 id=pop4 width=640 colspan="2">&nbsp;</td>
									<%} %>
								</tr>
						        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
							          		<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${MPOBean.msaId }'/>&app=null"   class="bgNone"><c:out value='${MPOBean.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${MPOBean.mp_appendix_id }'/>"><c:out value='${MPOBean.appendixName }'/></a>
										 <a><span class="breadCrumb1" style="padding:0 cm;">Master Purchase Order Management</span></a>
												</div>		
							        </td>
						        </tr>
						           <tr><td height="5" >&nbsp;</td></tr>
						          <tr><td class="message"><c:if test="${not empty requestScope.mpoExist}"> MPO with same name already exist.</c:if></td></tr> 
					      </table> --%>
					     
					      <%@ include  file="/NetMedXDashboardMenuScript.inc" %>
	      			</logic:equal>
					<logic:notEqual name = "MPOBean" property = "fromPage" value = "NetMedX">
						<%@ include  file="/AppedixDashboardMenu.inc" %>
						<%-- <%@ include  file="/AppendixDashboardMenuScriptAPIAccess.inc" %> --%>
	<div id="menunav">
    <ul>
  		<li>
        	<a class="drop" href="#"><span>Manage</span></a>
        	<ul>
        		<li><a href="JobEditAction.do?ref=inscopeview&amp;appendix_Id=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;Job_Id=0">Add Job</a></li>
        		<li>
        			<a href="#"><span>Bulk Job</span></a>
        			<ul>
        				<li><a href="javascript:bulkJobCreation();">Standard Add</a></li>
        				<li><a href="LargeDeplBulkJobCreateAction.do?ref=bukJobCreationView&amp;ref1=view&amp;Appendix_Id=<%= appendixid %>">Large Deployment Add</a></li>
        				<li><a href="ChangeJOSAction.do?ref=bukJobCreationView&amp;appendixid=<%= appendixid %>">Change Job Owner/Scheduler</a></li>
        			</ul>
        		</li>
				
	        		<% if( contractDocMenu != "" ) { %>
	        		<li>
	        			<a href="#"><span>Contract Documents</span></a>
	        			<ul>
							<%= contractDocMenu %>
	        			</ul>
	        		</li>
				<% } %>
				
        		<li><a href="AppendixCustomerInformation.do?function=add&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Custom Fields</a></li>
        		<li><a href="AddCustRef.do?typeid=<%= appendixid %>&amp;type=Appendix&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Customer Reference</a></li>
        		<li><a href="ESAEditAction.do?ref=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">External Sales Agent</a></li>
        		<li>
        			<a href="#"><span>Documents</span></a>
        			<ul>
        				<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;entityType=Appendix&amp;function=viewHistory&amp;fromPage=Appendix">Appendix History</a></li>
        				<li>
        					<a href="#"><span>Support Documents</span></a>
        					<ul>
        						<li><a href="EntityHistory.do?entityId=<%= appendixid %>&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;entityType=Appendix Supporting Documents&amp;function=addSupplement">Upload</a></li>
        						<li><a href="EntityHistory.do?entityType=Appendix Supporting Documents&amp;appendixId=<%= appendixid %>&amp;fromPage=Appendix&amp;linkLibName=Appendix&amp;function=supplementHistory">History</a></li>
        					</ul>
    					</li>
        			</ul>
        		</li>
        		<li><a href="SiteManagement.do?function=view&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;initialCheck=true&amp;pageType=jobsites">Job Sites</a></li>
        		<li>
        			<a href="#"><span>Schedule</span></a>
        			<ul>
						<li><a href="CostSchedule.do?appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">View</a></li>
						
							<li><a href="ScheduleUpdate.do?function=update&amp;typeid=<%= appendixid %>&amp;type=P&amp;page=appendixdashboard&amp;pageid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Update</a></li>
						
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Comments</span></a>
        			<ul>
						<li><a href="javascript:viewcomments();">View</a></li>
						<li><a href="javascript:addcomment();">Add</a></li>
        			</ul>
        		</li>
        		<li>
        			<a href="#"><span>Partner Information</span></a>
        			<ul>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=sow&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage SOW</a></li>
						<li><a href="PartnerSOWAssumptionList.do?fromId=<%= appendixid %>&amp;viewType=assumption&amp;fromType=prm_act&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Assumptions</a></li>
						<li><a href="PartnerSISCAction.do?appendixId=<%= appendixid %>&amp;fromType=prm_app&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>">Manage Special Instructions/Conditions</a></li>
        			</ul>
        		</li>
        		<li><a href="AssignTeam.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">Team</a></li>
        		<li><a href="OnlineBid.do?appendixId=<%= appendixid %>&amp;fromPage=Appendix">Online Bid</a></li>
        		<li><a href="MPOAction.do?NonPm=no&amp;appendixid=<%= appendixid %>&amp;fromPage=appendix">MPO</a></li>
        		<li><a href="JobNotesAction.do?appendixid=<%= appendixid %>">Job Notes</a></li>
        		<li><a href="ProjectLevelWorkflowAction.do?hmode=unspecified&amp;appendixid=<%= appendixid %>">Workflow Checklist Items</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="#"><span>Reports</span></a>
        	<ul>
				<li><a href="javascript:openreportschedulewindow('redline')">Redline Report</a></li>
				<li><a href="javascript:openreportschedulewindow('detailsummary')">Detailed Summary</a></li>
				<li><a href="/content/reports/csv_summary/">CSV Summary</a></li>
        		<li>
        			<a href="#"><span>Status</span></a>
        			<ul>
						<li><a href="javascript:managestatus( 'view' )">View</a></li>
						<li><a href="javascript:managestatus( 'update' )">Update</a></li>
						<li><a href="javascript:sendstatusreport()">Send</a></li>
        			</ul>
        		</li>
				<li><a href="MSPCCReportAction.do?function=View&amp;appendixid=<%= appendixid %>&amp;viewjobtype=ION&amp;ownerId=<%=ownerid %>&amp;fromPage=Appendix">MSP C/C Report</a></li>
        	</ul>
  		</li>
  		<li>
        	<a class="drop" href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<%= appendixid %>&ownerId=<%=ownerid %>"><center>Search</center></a></td>
						
  		</li>
  	
	</ul>
</div>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
		        <tbody><tr><td width="100%" height="21" background="images/content_head_04.jpg" colspan="4">
			  	<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${MPOBean.msaId }'/>&app=null"   class="bgNone"><c:out value='${MPOBean.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${MPOBean.mp_appendix_id }'/>"><c:out value='${MPOBean.appendixName }'/></a>
										 <a><span class="breadCrumb1" style="padding:0 cm;">Master Purchase Order Management</span></a>
			        </td>
		        </tr><tr>
					
				
				
					</td>
				</tr>
	      </tbody></table>						
						
						
						
						
						
						
						<!-- End of Menu -->
						
						<%-- <table cellpadding="0" cellspacing="0" border="0" width="100%">
					        <tr> 
						          <td  id="pop1" width="120" class="Ntoprow2" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu1',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu1',event)" class="menufont" style="width: 120px"><center>Manage</center></a></td>
						          <td  id="pop2" width="120" class="Ntoprow2" align="center"><a href="#" onmouseout="MM_swapImgRestore(); popDown('elMenu2',event)"     onmouseover="MM_swapImage('Image25','','images/about1b.gif',1);popUp('elMenu2',event)" class="menufont" style="width: 120px"><center><bean:message bundle="PRM" key="prm.appendix.reports"/></center></a></td>
						     	  <td  id="pop5" width="120" class="Ntoprow2" align="center"><a href="AppendixHeader.do?function=view&viewjobtype=jobSearch&fromLink=jobSearch&appendixid=<c:out value='${MPOBean.mp_appendix_id }'/>&msaId=<c:out value='${MPOBean.msaId }'/>&fromPage=viewselector" class ="menufont" style="width: 120px"><center>Search</center></a></td>
						          <td  id="pop6" class="Ntoprow2" width="100%">&nbsp;</td>
							</tr>
					        <tr><td background="images/content_head_04.jpg" height="21" colspan ="4" width="100%">
						          		<div id="breadCrumb"><a background="none;" href="SiteMenu.do?function=view&initialCheck=true&pageType=mastersites&viewjobtype=ION&ownerId=1&msaid=<c:out value='${MPOBean.msaId }'/>&app=null"   class="bgNone"><c:out value='${MPOBean.msaName }'/></a>
										 <a href="AppendixHeader.do?function=view&fromPage=appenndix&appendixid=<c:out value='${MPOBean.mp_appendix_id }'/>"><c:out value='${MPOBean.appendixName }'/></a>
										 <a><span class="breadCrumb1" style="padding:0 cm;">Master Purchase Order Management</span></a>
						        </td>
					        </tr>
					        <tr><td height="5" >&nbsp;</td></tr>
							<tr><td class="message"><c:if test="${not empty requestScope.mpoExist}"> MPO with same name already exist.</c:if></td></tr>
				      </table>--%>
				    
				      <%@ include  file="/AppedixDashboardMenuScript.inc" %>
				     </logic:notEqual>
	
	<TABLE cellspacing="0" cellpadding="0" style="padding-left: 12px;" border=0>
		<tr >
			<td>
				<TABLE WIDTH="100%"  cellspacing="0" cellpadding="0"  border="0">
					<tr>
						<td>
						     <table border="0" align="left" cellpadding="0" cellspacing="0" >
								 <tr>
								 	<% 
								 		String[] tabNames = {
								 			"List",
								 			"Activities/Resources",
								 			"Purchase Order",
								 			"Work Order",
								 			"Document Management",
								 			"Deliverables"
								 			};
								 	for(int tabIndex=0;tabIndex<tabNames.length;tabIndex++) {
								 	%>
								      <td id="TOP_TAB" width="10" class='leftRightBorder'>
											<table cellpadding="0" cellspacing="0" >
												<tr>
													<td id="TOP_BUTTON" style="cursor:hand;"
														onClick="fun_HideShowBlock('<%=tabIndex%>','<%=tabIndex%>');" align="center" style="display:none;"
														nowrap><img src="images/left-cor.gif" border="0"></td>
													<td id="TOP_BUTTON" style="cursor:hand;" style="display:none;"
														onClick="fun_HideShowBlock('<%=tabIndex%>','<%=tabIndex%>');" align="center"  class='drop' 
														nowrap bgcolor="#B5C8D9"><%=tabNames[tabIndex]%></td>
													<td id="TOP_BUTTON" style="cursor:hand;" style="display:none;"
														onClick="fun_HideShowBlock('<%=tabIndex%>','<%=tabIndex%>');" align="center" 
														nowrap><img src="images/right-cor.gif" border="0"></td>
												</tr>
											</table>
										</td>
									<%
								 	}
									%>
									</tr>
								</table>
							</td>
						</tr>
					      <!--- Include SubMPOList.jsp for tab0. -->
							  <tr valign="top" id="tab" style="display:none;">
					  			<td width="100%">
					  			<c:if test="${requestScope.tabId eq '0'}">
									<jsp:include page="SubMPOList.jsp"/>
								</c:if>
					  			</td>
					  		</tr>
							
						   <!--- Include ActivityResource.jsp for tab1. -->
							  <tr valign="top" id="tab" style="display:none;">
					  			<td width="100%">
					  			<c:if test="${requestScope.tabId eq '1'}">
					  				<jsp:include  page="MPOActivityResource.jsp"/>
					  			</c:if>
					  			</td>
					  		</tr>

						<!--- Include PurchaseOrderTab.jsp for tab1.-->
						<tr id="tab" style="display: none;">
				  			<td  width="100%" >
				  			<c:if test="${requestScope.tabId eq '2'}">
						   		<jsp:include page="PurchaseOrderTab.jsp"/> 
						   </c:if>
							</td>
						  </tr>
						   <!--- Include MPOWorkOrder.jsp for tab1. -->
						   <tr id="tab" style="display: none;">
				  			<td width="100%" >
				  			<c:if test="${requestScope.tabId eq '3'}">
				  				<jsp:include  page="MPOWorkOrder.jsp"></jsp:include>
				  			</c:if>
				  			</td>
						   </tr>
						   
						   <!--- Include DocumentManagement.jsp for tab1.-->
						   <tr valign="top" id="tab" style="display: none;">
				  			<td width="100%" >
				  			<c:if test="${requestScope.tabId eq '4'}">
								<jsp:include page="DocumentManagement.jsp"/>
							</c:if>
							</td> 
						  </tr>
							<!--- Include MPODeliverablesList.jsp for tab1. -->
						   <tr valign="top" id="tab" style="display: none;">
				  			<td width="100%" >
				  				<c:if test="${requestScope.tabId eq '5'}">
							  	 	<jsp:include  page="MPODeliverablesList.jsp"></jsp:include>
							   </c:if>
							</td> 
						  </tr>
					</table>
				</td>
			</tr>
		</TABLE>
							
</form>
</body>
</html>