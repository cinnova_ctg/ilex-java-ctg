<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<head>
<script>
function addEditDev(devId,obj,tabId)
{
		url = "MPOAction.do?hmode=fetchMPOInsertedData&type="+tabId+"&devId="+devId+"&clicked="+obj;
		document.forms[0].action=url;
		document.forms[0].submit();
		//document.all.devListTable.style.display="none";
		//document.all.devEditTable.style.display="";
}
</script>
<script>
function deleteDev(obj1,tabId)
{
	var returnVal = confirm("Do you really want to delete the deliverable")	;
	if (returnVal>0){
		url = "MPOAction.do?hmode=deleteDev&type="+tabId+"&devId="+obj1;
		document.forms[0].action=url;
		document.forms[0].submit();
		return true;
	}
	else
		return false;
		
}
</script>

</head>
     	<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" 
     	 height="300" cellspacing="1" cellpadding="0" width="100%">
     		<tr valign="top" height="10">
     			<td  width="100%"  colspan="7" cellspacing="1" cellpadding="0">
     				<table width="100%" cellspacing="0" cellpadding="0" >
     					<tr>
     						<td>
			     				<table width="100%" cellspacing="0" cellpadding="0">
			     					<tr>
			     						<td width="50%"  align="left" nowrap class='Ntextoleftalignnowrappaleyellow'>
			     							<span nowrap ><b>Name:</b></span><span nowrap > <c:out value='${MPOBean.mpoName}'/></span>
			     						</td>
						     			<td width="50%" align="right" nowrap style="padding-right: 6px;" class='Ntextoleftalignnowrappaleyellow'>
						     			
						     				<span nowrap ><b>Master PO Item:</b></span><span nowrap > <c:out value="${mpoItem}"/></span>
						     			</td>
						     		</tr>
						     	</table>
						     </td>
					     </tr>
				    </table>
			     </td>
     		</tr>
     		<tr valign="top">
     			<td>
					<table cellpadding="2" cellspacing="1" width="100%">
					<tr><td>
					<table  width="100%" cellspacing="1" cellpadding="0">
			     		<tr valign="top">
			     			<td>
						   	  <table width="100%" border="0" id="devListTable" cellpadding="1" cellspacing="1" <c:if test="${not empty requestScope.clicked}">style="display:none;"</c:if>>
						   	  		<tr>
										<td class = "texthNoBorder" style="width: 200px;">Actions/Workflow</td>
						         		<td class = "texthNoBorder" width="150">
								    		<table width="100%" border="0" cellpadding="1" cellspacing="1">
								    			<tr>
								    				<td class = "texthNoBorder" align="center"  width="100%">
								    					Title</b>
								    				</td>
								    				<td class = "texthNoBorder" align="left">
								    					<table width="100%"border="0">
								    						<tr>
								    							<td  align="right">
								    								<input type="button" name="Add" value="Add"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if>  onclick="addEditDev('0','add','5');" class="NbuttonBrown">
								    							</td>
								    						</tr>
								    					</table>
								    				</td>
								    			</tr>
								    		</table>
							    		</td>
					         			<td class = "texthNoBorder"  >Type </td>
			           					<td class = "texthNoBorder"  >Format </td>
				           				<td class = "texthNoBorder" width="350">Description</td>
				           				<td class = "texthNoBorder" >Mobile</td>
					         		</tr>
						         	<c:forEach var="list" items="${requestScope.devList}" varStatus="count">						         	
							         		<tr>
								         		<td class = "Ntexto" align="center"> <c:if test="${MPOBean.mpoStatus ne 'Approved'}"> [&nbsp;<a href = "#" onclick="addEditDev('<c:out value="${list[1]}"/>','edit','5');">Edit</a>&nbsp;|&nbsp;<a href = "#" onclick="deleteDev('<c:out value="${list[1]}"/>','5');">Delete</a>&nbsp;]</c:if></td>
								         		<td class = "Nhyperodd" ><c:out value="${list[2]}"/></td>
								         		<td class = "Nhyperodd" align="center"><c:out value="${list[3]}"/></td>
								         		<td class = "Ntextoleftalignnowrap" align="center"><c:if test="${list[4] eq '-1'}"> </c:if><c:if test="${list[4] ne '-1'}"><c:out value="${list[4]}"/></c:if></td>
								         		<td class = "Ntexto" style="white-space: normal;"><c:out value="${list[5]}"/></td>
								         		<td class = "Ntexto" ><c:if test="${list[6] eq 'y'}">Yes </c:if><c:if test="${list[6] eq 'n'}">No </c:if></td>		    
							         		</tr>
						         	</c:forEach>
						  		</table>
							</td>
						  </tr>
						  <tr valign="top">
						  	<td>
						  		<table id="devEditTable" width="100%" border="0" <c:if test="${empty requestScope.clicked}">style="display:none;"</c:if> cellpadding="1" cellspacing="1">
						  			<input type="hidden" name="devId" value="<c:out value="${MPOBean.devId}"/>">
						  			<input type="hidden" name="devMode" value="<c:out value="${MPOBean.devMode}"/>">
						  			<tr>
						  				<td class = "Ntextoleftalignnowrap">
						  					Title <font class="redbold">*</font>
						  				</td>
						  				<td class='Ntexteleftalignnowrap'>
						  					<input type="text" name="devTitle" size="80" class="text" value="<c:out value='${MPOBean.devTitle}'/>">
						  				</td>
						  			</tr>
						  			<tr>
						  				<td class = "Ntextoleftalignnowrap">
						  					Type <font class="redbold">*</font>
						  				</td>
						  				<td class='Ntexteleftalignnowrap'>
						  					<input type="radio" name="devType" value="Internal" <c:if test="${MPOBean.devType eq 'Internal'}">checked="checked"</c:if>>&nbsp;Internal
						  					<input type="radio" name="devType" value="Customer" <c:if test="${MPOBean.devType eq 'Customer'}">checked="checked"</c:if>>&nbsp;Customer
						  				</td>
						  			</tr>
						  					<tr>
						  				<td class = "Ntextoleftalignnowrap">
						  					Mobile<font class="redbold">*</font>
						  				</td>
						  				<td class='Ntexteleftalignnowrap'>
						  					<input type="radio" name="mobileAllowed" value="y" <c:if test="${MPOBean.mobileAllowed eq 'y'}">checked="checked"</c:if>>&nbsp;Yes
						  					<input type="radio" name="mobileAllowed" value="n" <c:if test="${MPOBean.mobileAllowed eq 'n'}">checked="checked"</c:if>>&nbsp;No
						  				</td>
						  			</tr>
						  			<tr>
						  				<td class = "Ntextoleftalignnowrap">
						  					Format <font class="redbold">*</font>
						  				</td>
						  				<td class='Ntexteleftalignnowrap'>
						  					<select name="devFormat" class="select">
						  						<option value="-1" <c:if test="${MPOBean.devFormat eq '-1'}">selected="selected"</c:if>>Select Format</option>
						  						<option value="PDF" <c:if test="${MPOBean.devFormat eq 'PDF'}">selected="selected"</c:if>>PDF</option>
						  						<option value="Image" <c:if test="${MPOBean.devFormat eq 'Image'}">selected="selected"</c:if>>Image</option>
						  					</select>
						  				</td>
						  			</tr>
						  			<tr>
						  				<td class = "Ntextoleftalignnowrap">
						  					Description
						  				</td>
						  				<td class='Ntexteleftalignnowrap'>
						  					<textarea  name="devDescription"  class="textarea" rows="5" cols="80" ><c:out value='${MPOBean.devDescription}'/></textarea>
						  				</td>
						  			</tr>
						  			 <tr>
									   	<td  class = "colLight" align="left" colspan='2'>
									   		<input type="button" name="button1" class="button_c" value="Save" onclick="onSave('5')"/>
									   		<input type="reset" name="button2" class="button_c" value="Reset" />
									   	</td>
									</tr>
						  		</table>
						 	</td>
						</tr>	
					</table>	
					</td></tr></table>
				</td>
			</tr>
		</table>	


