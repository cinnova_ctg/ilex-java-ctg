<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<head>
<% String nonPm=request.getParameter("NonPm");%>
<script>
var estimatedCost = 0 ;
var contractLabourCost = 0;
var corporateLabourCost = 0;
var materialCost = 0;
var frieghtCost = 0;
var travelFixedCost = 0;
var travelHourlyCost = 0;
var PVSCostByContract = 0;
var PVSCostByHour = 0;
var speedPayByContract = 0;
var speedPayByHour = 0;
var mmByContract = 0;
var mmByHour = 0;
var contractLabourCostGen = 0;
var travelHourlyCostGen = 0;
var estimatedAuthorizedCost = 0;
</script>
<script>
	function showTable(obj)
	{
		if(isBlank(document.all.mpoName.value))
		{
			alert('Please enter MPO Name')
			return false;
		}
		url = "MPOAction.do?hmode=continuePressed&type="+obj+"&NonPm=<%=nonPm%>";
		document.forms[0].action=url;
		document.forms[0].submit();
		
	}
</script>
<script>
	function changePOItem()
	{
		var tableObj = document.getElementById('actTable');
		if(document.all.planned == null || document.all.planned == 'undefined')
			return true;
		var len = document.all.est.length;
		var length = document.all.planned.length;
		var totalLength = document.all.mpoActivityResource.length;
		document.getElementById("imgMeetOrBeat").colSpan="7";

		if(!totalLength)
		{
			if(document.all.mpoActivityResource.checked == true)
				{	
				
					if(document.all.mpoActivityPlanQuantity.value=='0.00' && (document.all.mpoActivityPlanUnitCost.value=='0.0000' || document.all.mpoActivityPlanUnitCost.value=='0.00')){
						document.all.mpoActivityPlanQuantity.value='';
						document.all.mpoActivityPlanUnitCost.value='';
					}
					if(isBlank(document.all.mpoActivityPlanQuantity.value) && isBlank(document.all.mpoActivityPlanUnitCost.value)){		
						document.all.mpoActivityPlanQuantity.value = roundTo2Digit(document.all.mpoTotal.value);
						document.all.mpoActivityPlanUnitCost.value = roundTo2Digit(eval(document.all.mpoUnit.value));
						tableObj.rows[2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
						document.all.mpoActivityPlanSubTotal.value = document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value;
					}
				}
		}
		else
		{
			$("input[name='mpoActivityResource']:checked").each(function(i){
					var index = jQuery(this).parent('td').parent('tr').prevAll().length - 2;
					if(document.all.mpoActivityPlanQuantity[index].value=='0.00' && (document.all.mpoActivityPlanUnitCost[index].value=='0.0000' || document.all.mpoActivityPlanUnitCost[index].value=='0.00')){
						document.all.mpoActivityPlanQuantity[index].value='';
						document.all.mpoActivityPlanUnitCost[index].value='';
					}
						if(isBlank(document.all.mpoActivityPlanQuantity[index].value) && isBlank(document.all.mpoActivityPlanUnitCost[index].value)){		
							document.all.mpoActivityPlanQuantity[index].value = roundTo2Digit(document.all.mpoTotal[index].value);
							document.all.mpoActivityPlanUnitCost[index].value = roundTo2Digit(eval(document.all.mpoUnit[index].value));
							tableObj.rows[i+2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity[index].value * document.forms[0].mpoActivityPlanUnitCost[index].value);
							document.all.mpoActivityPlanSubTotal[index].value = document.forms[0].mpoActivityPlanQuantity[index].value * document.forms[0].mpoActivityPlanUnitCost[index].value;
					}
			})
		}
		calCostByResType();
		calEstimateCost()
		setCost();
		calForPVS();
		calForGeneral('speed','24');
		calForGeneral('mm','18');
		calculateAll();
	}
</script>
<script>
function checkSelectAll(tableName)
{
	PVSCostByContract = 0;
	contractLabourCost = 0;
	corporateLabourCost = 0;
	materialCost = 0;
	frieghtCost = 0;
	travelFixedCost = 0;
	travelHourlyCost = 0;
	estimatedCost = 0 ;
	PVSCostByHour = 0;
	estimatedAuthorizedCost = 0;
	tableObj = document.getElementById(tableName);
	var costTable = document.getElementById("costTable");
	var len = document.all.mpoActivityResource.length;
	if(!len)
	{
			document.all.mpoActivityResource.checked = true;
			tableObj.rows[2].cells[6].innerText = round_funcfor4digits(eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value));
			document.all.mpoTotal.value = (eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value));
			tableObj.rows[2].cells[8].innerText = round_func(eval (document.forms[0].mpoUnit.value * eval (document.all.mpoTotal.value)));
			document.all.mpoSubTotal.value = (eval (document.forms[0].mpoUnit.value * eval (document.all.mpoTotal.value)));
			if((isBlank(document.all.mpoActivityPlanQuantity.value)||isBlank(document.all.mpoActivityPlanUnitCost.value))){
				document.all.mpoActivityPlanQuantity.value = roundTo2Digit(eval(document.all.mpoTotal.value));
				document.all.mpoActivityPlanUnitCost.value = roundTo2Digit(eval(document.all.mpoUnit.value));
				tableObj.rows[2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
				document.all.mpoActivityPlanSubTotal.value = (document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
			}
	}
	else
	{
		for(var i=0;i<len;i++)
		{
			document.all.mpoActivityResource[i].checked = true;
			tableObj.rows[i+2].cells[6].innerText = round_funcfor4digits(eval (document.forms[0].mpoAct[i].value * document.forms[0].mpoRes[i].value));
			document.all.mpoTotal[i].value = (eval (document.forms[0].mpoAct[i].value * document.forms[0].mpoRes[i].value));
			tableObj.rows[i+2].cells[8].innerText = round_func(eval (document.forms[0].mpoUnit[i].value * eval (document.all.mpoTotal[i].value)));
			document.all.mpoSubTotal[i].value = (eval (document.forms[0].mpoUnit[i].value * eval (document.all.mpoTotal[i].value)));
			if((isBlank(document.all.mpoActivityPlanQuantity[i].value)||isBlank(document.all.mpoActivityPlanUnitCost[i].value))){
				document.all.mpoActivityPlanQuantity[i].value = roundTo2Digit(eval(document.all.mpoTotal[i].value));
				document.all.mpoActivityPlanUnitCost[i].value = roundTo2Digit(eval(document.all.mpoUnit[i].value));
				tableObj.rows[i+2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
				document.all.mpoActivityPlanSubTotal[i].value = (document.forms[0].mpoActivityPlanQuantity[i].value * document.forms[0].mpoActivityPlanUnitCost[i].value);
			}
		}
	}		
	calCostByResType();
	calForPVS();
	calEstimateCost()
	setCost();
	calForGeneral('speed','24');
	calForGeneral('mm','18');
	
}
</script>
<script>

function setCost()
{
	var costTable = document.getElementById("costTable");
	if(isExists(costTable))
	{
		costTable.rows[1].cells[1].innerText = formatCurrency((contractLabourCost));
		costTable.rows[2].cells[1].innerText = formatCurrency((corporateLabourCost));
		
		costTable.rows[1].cells[2].innerText = '';
		costTable.rows[2].cells[2].innerText = '';
		costTable.rows[1].cells[3].innerText = '';
		costTable.rows[2].cells[3].innerText = '';
		costTable.rows[1].cells[4].innerText = '';
		costTable.rows[2].cells[4].innerText = '';	
		costTable.rows[1].cells[5].innerText = '';
		costTable.rows[2].cells[5].innerText = '';
		
		costTable.rows[3].cells[1].innerText = formatCurrency((eval(contractLabourCost) + eval(corporateLabourCost)));
		costTable.rows[1].cells[2].innerText = formatCurrency((materialCost));
		costTable.rows[3].cells[2].innerText = formatCurrency((materialCost));
		costTable.rows[1].cells[3].innerText = formatCurrency((frieghtCost));
		costTable.rows[3].cells[3].innerText = formatCurrency((frieghtCost));
		costTable.rows[1].cells[4].innerText = formatCurrency((eval(travelFixedCost) + eval(travelHourlyCost)));
		costTable.rows[3].cells[4].innerText = formatCurrency((eval(travelFixedCost) + eval(travelHourlyCost)));
		costTable.rows[3].cells[5].innerText = formatCurrency((eval(eval(contractLabourCost)+eval(corporateLabourCost)+eval(materialCost)+eval(frieghtCost)+eval(travelFixedCost)+eval(travelHourlyCost))))
	}
	
}
</script>
<script>

function calculateSubSubTotal(resType,index,costType)
{
	if(index!='test')
	{
		if(document.all.mpoItemType.value=='fp'){
			if(sign!='-')
				costType = eval(eval(costType) + eval(document.all.mpoSubTotal[index].value));
			else
				costType = eval(eval(costType) - eval(document.all.mpoSubTotal[index].value));
		}
		else{
			if(sign!='-')
				costType = eval(eval(costType) + eval(document.all.mpoActivityPlanSubTotal[index].value));
			else
				costType = eval(eval(costType) - eval(document.all.mpoActivityPlanSubTotal[index].value));
		}
	}
	else
	{
		if(document.all.mpoItemType.value=='fp'){
			if(sign!='-')
				costType = eval(eval(costType) + eval(document.all.mpoSubTotal.value));
			else
				costType = eval(eval(costType) - eval(document.all.mpoSubTotal.value));
		}
		else{
			if(sign!='-')
				costType = eval(eval(costType) + eval(document.all.mpoActivityPlanSubTotal.value));
			else
				costType = eval(eval(costType) - eval(document.all.mpoActivityPlanSubTotal.value));
		}
	}
}
</script>
<script>

function checkDeSelectAll(tableName)
{
	contractLabourCost = 0;
	corporateLabourCost = 0;
	materialCost = 0;
	frieghtCost = 0;
	travelFixedCost = 0;
	travelHourlyCost = 0;
	estimatedCost = 0 ;
	PVSCostByHour = 0;
	PVSCostByContract = 0;
	estimatedAuthorizedCost = 0;
	tableObj = document.getElementById(tableName);
	var len = document.all.mpoActivityResource.length;
	if(!len)
	{
		document.all.mpoActivityResource.checked = false;
		document.forms[0].mpoActivityPlanQuantity.value = '';
		document.forms[0].mpoActivityPlanUnitCost.value = ''
		tableObj.rows[2].cells[11].innerText = '0.00'
		document.all.mpoActivityIsWOShow.checked = false;
	}
	else
	{
		for(var i=0;i<len;i++)
		{
			document.all.mpoActivityResource[i].checked = false;
			document.forms[0].mpoActivityPlanQuantity[i].value = '';
			document.forms[0].mpoActivityPlanUnitCost[i].value = ''
			tableObj.rows[i+2].cells[11].innerText = '0.00'
			document.all.mpoActivityIsWOShow[i].checked = false;
		}
	}
	
	var costTable = document.getElementById("costTable");

	calCostByResType();
	calForPVS();
	calForGeneral('speed','24');
	calForGeneral('mm','18');
	calEstimateCost();
	setCost();

}
</script>
<script>

function calculateSubTotal(resCheckObj,tableName, index)
{
	var d=new Date();
	var subTotal = 0;
	tableObj = document.getElementById(tableName);
	var len = tableObj.rows.length;
	if(len==6)
	{	
		
			tableObj.rows[2].cells[6].innerText = round_funcfor4digits(eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value));
			document.forms[0].mpoTotal.value = (eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value));
			tableObj.rows[2].cells[8].innerText = round_func(eval (document.forms[0].mpoUnit.value * document.forms[0].mpoTotal.value));
			document.forms[0].mpoSubTotal.value = (document.forms[0].mpoUnit.value * document.forms[0].mpoTotal.value);

			document.forms[0].mpoActivityPlanQuantity.value = round_funcfor4digits(document.forms[0].mpoTotal.value);
			document.forms[0].mpoActivityPlanUnitCost.value =roundTo2Digit(document.forms[0].mpoUnit.value);
			tableObj.rows[2].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);
			document.forms[0].mpoActivityPlanSubTotal.value = (document.forms[0].mpoActivityPlanQuantity.value * document.forms[0].mpoActivityPlanUnitCost.value);

			if(resCheckObj.checked != true)
			{
				document.forms[0].mpoTotal.value = '';
				document.forms[0].mpoSubTotal.value = '';
				document.forms[0].mpoActivityPlanQuantity.value = '';
				document.forms[0].mpoActivityPlanUnitCost.value = ''
				tableObj.rows[2].cells[11].innerText = '0.00';
				document.all.mpoActivityIsWOShow.checked = false;
			}

		
	}
	else
	{
		i =	index + 3;
		tableObj.rows[i-1].cells[6].innerText = round_funcfor4digits(eval (document.forms[0].mpoAct[i-3].value * document.forms[0].mpoRes[i-3].value));
		document.all.mpoTotal[eval(i-3)].value = (eval (document.forms[0].mpoAct[i-3].value * document.forms[0].mpoRes[i-3].value));
		tableObj.rows[i-1].cells[8].innerText = round_func(eval (eval(document.forms[0].mpoUnit[i-3].value) * document.all.mpoTotal[eval(i-3)].value));
		document.forms[0].mpoSubTotal[i-3].value = (eval (eval(document.forms[0].mpoUnit[i-3].value) * document.all.mpoTotal[eval(i-3)].value));

		document.forms[0].mpoActivityPlanQuantity[i-3].value = round_funcfor4digits(eval(document.forms[0].mpoTotal[i-3].value));
		document.forms[0].mpoActivityPlanUnitCost[i-3].value = roundTo2Digit(eval(document.forms[0].mpoUnit[i-3].value));
		tableObj.rows[i-1].cells[11].innerText = round_func(document.forms[0].mpoActivityPlanQuantity[i-3].value * document.forms[0].mpoActivityPlanUnitCost[i-3].value);
		document.forms[0].mpoActivityPlanSubTotal[i-3].value = (document.forms[0].mpoActivityPlanQuantity[i-3].value * document.forms[0].mpoActivityPlanUnitCost[i-3].value);
		if(resCheckObj.checked != true)
		{
			document.forms[0].mpoSubTotal[i-3].value = '';
			document.all.mpoTotal[eval(i-3)].value = '';
			tableObj.rows[i-1].cells[11].innerText = '0.00';
			document.forms[0].mpoActivityPlanQuantity[i-3].value = '';
			document.forms[0].mpoActivityPlanUnitCost[i-3].value = '';
			document.all.mpoActivityIsWOShow[i-3].checked = false;
		}
	}
			

	calCostByResType();
	
	calForPVS();
	
	calForGeneral('speed','24');
	
	calForGeneral('mm','18');
	
	calEstimateCost();
	

	setCost();
	 var d1=new Date();
}
</script>
<script>
	function calForPVS()
	{	
		PVSCostByHour = 0;
		PVSCostByContract = 0;
		var tableObj = document.getElementById('CVPT');
		if(isExists(tableObj))
		{
			PVSCostByContract =  contractLabourCost ;
			tableObj.rows[3].cells[1].innerText = formatCurrency(PVSCostByContract);
			PVSCostByHour = travelHourlyCost;
			tableObj.rows[3].cells[2].innerText = formatCurrency((PVSCostByHour));
			tableObj.rows[3].cells[3].innerText = formatCurrency((PVSCostByContract + PVSCostByHour));
		}
	}
</script>
<script>
	function calEstimateCost()
	{
		if(isExists(document.all.mpoActivityResource))
		{
		estimatedCost=0;
		estimatedAuthorizedCost=0;
			var len = $("input[name='mpoActivityResource']").length;
			if(!len)
			{
				if( $("#mpoActivityResourceId:checked"))
				{
					estimatedAuthorizedCost = eval(eval(estimatedAuthorizedCost) + eval(document.forms[0].mpoSubTotal.value));
					estimatedCost = eval(eval(estimatedCost) + eval(document.forms[0].mpoActivityPlanSubTotal.value));

				}
			}
			else
			{
				$("input[name='mpoActivityResource']:checked").each(function(i){
					var index = jQuery(this).parent('td').parent('tr').prevAll().length - 2;
					estimatedAuthorizedCost = ((estimatedAuthorizedCost) + eval(document.forms[0].mpoSubTotal[index].value));
					estimatedCost = ((estimatedCost) + eval(document.forms[0].mpoActivityPlanSubTotal[index].value));
				})
			}
			document.forms[0].estimatedCost.value = round_func(estimatedCost);
			document.forms[0].estimatedAuthorizedCost.value = estimatedAuthorizedCost;
			document.forms[0].estimatedTotalCost.value = estimatedCost;
		}
		
	}
</script>
<script>
	function calCostByResType()
	{
	
	contractLabourCost = 0;
	corporateLabourCost = 0;
	materialCost = 0;
	frieghtCost = 0;
	travelFixedCost = 0;
	travelHourlyCost = 0;
	estimatedCost = 0 ;
	PVSCostByHour = 0;
	PVSCostByContract = 0;
	estimatedAuthorizedCost = 0;
	if(isExists(document.all.mpoActivityResource))
	{
		var len = $("input[name='mpoActivityResource']").length;
			if(!len)
			{
				if( $("#mpoActivityResourceId:checked"))
			{
				if( document.all.mpoTypeName.value == 'Labor')
				{
					if(document.all.mpoActResourceSubName.value == 'Contract Labor' )
					{
							contractLabourCost = eval(eval(contractLabourCost) + eval(document.all.mpoActivityPlanSubTotal.value));
					}
					if( document.all.mpoActResourceSubName.value == 'Corporate Labor' )
					{
							corporateLabourCost = eval(eval(corporateLabourCost) + eval(document.all.mpoActivityPlanSubTotal.value));
					}
				}
				else if (document.all.mpoTypeName.value == 'Materials')
				{
						materialCost = eval(eval(materialCost) + eval(document.all.mpoActivityPlanSubTotal.value));
				}
				else if ( document.all.mpoTypeName.value == 'Freight')
				{
						frieghtCost = eval(eval(frieghtCost) + eval(document.all.mpoActivityPlanSubTotal.value));
				}
				else 
				{
					if(document.all.mpoActResourceSubName.value == 'Fixed'){
							travelFixedCost = eval(eval(travelFixedCost) + eval(document.all.mpoActivityPlanSubTotal.value));							
					}
					else
					{
								travelHourlyCost = eval(eval(travelHourlyCost) + eval(document.all.mpoActivityPlanSubTotal.value));
					}
				}
			}
		}
		else
		{
			//for(var index=0;index<len;index++)
				var formObj = document.forms[0];
				$("input[name='mpoActivityResource']:checked").each(function(i){
					var index = jQuery(this).parent('td').parent('tr').prevAll().length - 2;
					if( formObj.mpoTypeName[index].value == 'Labor')
					{
						if(formObj.mpoActResourceSubName[index].value == 'Contract Labor' )
						{
								contractLabourCost = ((contractLabourCost) + eval(formObj.mpoActivityPlanSubTotal[index].value));
						}
						if( formObj.mpoActResourceSubName[index].value == 'Corporate Labor' )
						{
								corporateLabourCost = ((corporateLabourCost) + eval(formObj.mpoActivityPlanSubTotal[index].value));
						}
					}
					else if (formObj.mpoTypeName[index].value == 'Materials')
					{
							materialCost = ((materialCost) + eval(formObj.mpoActivityPlanSubTotal[index].value));
					}
					else if ( formObj.mpoTypeName[index].value == 'Freight')
					{
							frieghtCost = ((frieghtCost) + eval(formObj.mpoActivityPlanSubTotal[index].value));
					}
					else 
					{
						if(formObj.mpoActResourceSubName[index].value == 'Fixed'){
								travelFixedCost = ((travelFixedCost) + eval(formObj.mpoActivityPlanSubTotal[index].value));							
						}
						else
						{
								travelHourlyCost = ((travelHourlyCost) + eval(formObj.mpoActivityPlanSubTotal[index].value));
						}
					}
			})
		}
	}
}
</script>
<script>
	function calForGeneral(type,typeVal)
	{
		var subTotalVal = 0;
		var planSubTotal = 0;
	 	contractLabourCostGen = 0;
 		travelHourlyCostGen = 0;
 		subTotalVal = 0;
 		speedPayByContract = 0;
 		speedPayByHour = 0;
 		mmByContract = 0;
		mmByHour = 0;
		if(isExists(document.all.mpoActivityResource))
		{
			var len = $("input[name='mpoActivityResource']").length;
			if(!len)
			{
				if( $("#mpoActivityResourceId:checked"))
				{
					if( document.all.mpoTypeName.value == 'Labor')
					{
						if(document.all.mpoActResourceSubName.value == 'Contract Labor' )
						{
								subTotalVal = document.all.mpoActivityPlanQuantity.value * typeVal;
							contractLabourCostGen = eval(eval(contractLabourCostGen) + eval(subTotalVal));
						}
					}
					else 
					{
						if(document.all.mpoActResourceSubName.value == 'Hourly'){
								subTotalVal = document.all.mpoActivityPlanQuantity.value * typeVal;
							travelHourlyCostGen = eval(eval(travelHourlyCostGen) + eval(subTotalVal));
						}
					}
				}
			}
			else
			{
				var formObj = document.forms[0];
				$("input[name='mpoActivityResource']:checked").each(function(i){
					var index = jQuery(this).parent('td').parent('tr').prevAll().length - 2;
						subTotalVal = formObj.mpoTotal[index].value * typeVal;
						//alert("jquery"+$("#mpoTotal:nth-child("+index+")"));
						if( formObj.mpoTypeName[index].value == 'Labor')
						{
							if(formObj.mpoActResourceSubName[index].value == 'Contract Labor' )
							{
									subTotalVal = formObj.mpoActivityPlanQuantity[index].value * typeVal;

								contractLabourCostGen = ((contractLabourCostGen) + eval(subTotalVal));
							}
						}
						else 
						{
							if(formObj.mpoActResourceSubName[index].value == 'Hourly'){
									subTotalVal = formObj.mpoActivityPlanQuantity[index].value * typeVal;
								travelHourlyCostGen = ((travelHourlyCostGen) + eval(subTotalVal));
							}
						}
						
				})
			}
		
		var tableObj = document.getElementById('CVPT');
		if(type=='speed'){
			speedPayByContract = contractLabourCostGen;
			speedPayByHour = travelHourlyCostGen;
			tableObj.rows[2].cells[1].innerText = formatCurrency((speedPayByContract));
			tableObj.rows[2].cells[2].innerText = formatCurrency((speedPayByHour));
			tableObj.rows[2].cells[3].innerText = formatCurrency((speedPayByContract + speedPayByHour));
			tableObj.rows[2].cells[4].innerText = formatCurrency((eval(contractLabourCost+travelHourlyCost) - eval(speedPayByContract + speedPayByHour )));
		}
		else
		{
			mmByContract = contractLabourCostGen;
			mmByHour = travelHourlyCostGen;
			tableObj.rows[1].cells[1].innerText =formatCurrency( (mmByContract));
			tableObj.rows[1].cells[2].innerText =formatCurrency( (mmByHour));
			tableObj.rows[1].cells[3].innerText = formatCurrency((mmByContract) + (mmByHour)) ;
			tableObj.rows[1].cells[4].innerText = formatCurrency((eval(contractLabourCost+travelHourlyCost) - eval(mmByContract + mmByHour )));
		}
	}
}
		
</script>

<script>
	function calCostForPlan(textObj)
	{
		
		var len = document.all.mpoActivityResource.length;
		var tableObj = document.getElementById('actTable');
		if(!len)
		{
			if(!isFloat(document.all.mpoActivityPlanQuantity.value)){
					alert('Only numeric value is allowed.');
					document.all.mpoActivityPlanQuantity.focus();
					return false;
				}
				if(!isFloat(document.all.mpoActivityPlanUnitCost.value)){
					alert('Only numeric value is allowed.');
					document.all.mpoActivityPlanUnitCost.focus();
					return false;
				}
				if(textObj == document.all.mpoActivityPlanQuantity || textObj == document.all.mpoActivityPlanUnitCost)
				{
					document.all.mpoActivityResource.checked = true;
					if(textObj == document.all.mpoActivityPlanQuantity  && isBlank(document.all.mpoActivityPlanUnitCost.value)){
						document.all.mpoActivityPlanUnitCost.value = 0;
					}
					if(textObj == document.all.mpoActivityPlanUnitCost  && isBlank(document.all.mpoActivityPlanQuantity.value)){
						document.all.mpoActivityPlanQuantity.value= 0;
					}
					document.all.mpoTotal.value = eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value);
					document.forms[0].mpoSubTotal.value = eval (eval(document.forms[0].mpoUnit.value) * document.all.mpoTotal.value);
					document.all.mpoActivityPlanSubTotal.value = eval (eval(document.all.mpoActivityPlanQuantity.value) * eval(document.all.mpoActivityPlanUnitCost.value));	
					tableObj.rows[2].cells[6].innerText = round_funcfor4digits(document.all.mpoTotal.value);
					tableObj.rows[2].cells[8].innerText = round_func(document.forms[0].mpoSubTotal.value);
					tableObj.rows[2].cells[11].innerText = round_func(document.all.mpoActivityPlanSubTotal.value);
				}	
				
		}
		else
		{
			for(var i=0;i < len ; i++)
			{
				if(!isFloat(document.all.mpoActivityPlanQuantity[i].value)){
					alert('Only numeric value is allowed.');
					document.all.mpoActivityPlanQuantity[i].focus();
					return false;
				}
				if(!isFloat(document.all.mpoActivityPlanUnitCost[i].value)){
					alert('Only numeric value is allowed.');
					document.all.mpoActivityPlanUnitCost[i].focus();
					return false;
				}
				if(textObj == document.all.mpoActivityPlanQuantity[i] || textObj == document.all.mpoActivityPlanUnitCost[i])
				{
					document.all.mpoActivityResource[i].checked = true;
					if(textObj == document.all.mpoActivityPlanQuantity[i]  && isBlank(document.all.mpoActivityPlanUnitCost[i].value)){
						document.all.mpoActivityPlanUnitCost[i].value = 0;
					}
					if(textObj == document.all.mpoActivityPlanUnitCost[i]  && isBlank(document.all.mpoActivityPlanQuantity[i].value)){
						document.all.mpoActivityPlanQuantity[i].value= 0;
					}
					document.all.mpoTotal[i].value = eval (document.forms[0].mpoAct[i].value * document.forms[0].mpoRes[i].value);
					document.forms[0].mpoSubTotal[i].value = eval (document.forms[0].mpoUnit[i].value * document.all.mpoTotal[eval(i)].value);
					document.all.mpoActivityPlanSubTotal[i].value = eval (document.all.mpoActivityPlanQuantity[i].value  * document.all.mpoActivityPlanUnitCost[i].value);	
					tableObj.rows[i+2].cells[6].innerText = round_funcfor4digits(document.all.mpoTotal[i].value);
					tableObj.rows[i+2].cells[8].innerText = round_func(document.forms[0].mpoSubTotal[i].value);
					tableObj.rows[i+2].cells[11].innerText = round_func(document.all.mpoActivityPlanSubTotal[i].value);
				}
			}
		}
		calCostByResType();
		calEstimateCost()
		setCost();
		calForPVS();
		calForGeneral('speed','24');
		calForGeneral('mm','18');
	}
</script>


<script>
	function calculateAll()
	{
		var tableObj = document.getElementById('actTable');
	 	if(isExists(document.all.mpoActivityResource))
	 	{
/*	 		var len = document.all.mpoActivityResource.length;
/		 	if(!len)
		 	{
		 		//if(document.all.mpoActivityResource.checked == true){
			 		document.all.mpoTotal.value = eval (document.forms[0].mpoAct.value * document.forms[0].mpoRes.value);
			 		tableObj.rows[2].cells[6].innerText = round_funcfor4digits(document.all.mpoTotal.value) ;
					document.forms[0].mpoSubTotal.value = eval (eval(document.forms[0].mpoUnit.value) * document.all.mpoTotal.value);
					tableObj.rows[2].cells[8].innerText = round_func(document.forms[0].mpoSubTotal.value);
					document.all.mpoActivityPlanSubTotal.value = eval (eval(document.all.mpoActivityPlanQuantity.value) * eval(document.all.mpoActivityPlanUnitCost.value));	
					tableObj.rows[2].cells[11].innerText = round_func(document.all.mpoActivityPlanSubTotal.value) ;
					//}
				//}
		 	}
		 	else
		 	{
*/				var formObj = document.forms[0];
		 		$("input[name='mpoActivityResource']").each(function(i){
				$checkBoxObj = jQuery(this);
				
				var tableObjRow = tableObj.rows[i+2];
				
		 				formObj.mpoTotal[i].value = (round_func(formObj.mpoAct[i].value) * round_func(formObj.mpoRes[i].value));
		 				var mpoTotal = formObj.mpoTotal[i].value;
						formObj.mpoSubTotal[i].value =  ((formObj.mpoUnit[i].value) * mpoTotal);
			 			tableObjRow.cells[6].innerText = round_funcfor4digits(mpoTotal) ;
						tableObjRow.cells[8].innerText = round_func(formObj.mpoSubTotal[i].value)
						formObj.mpoActivityPlanSubTotal[i].value = ((formObj.mpoActivityPlanQuantity[i].value ) * (formObj.mpoActivityPlanUnitCost[i].value));	
						tableObjRow.cells[11].innerText = round_func(formObj.mpoActivityPlanSubTotal[i].value);

		 		})
//		 	}
		 }
		calCostByResType();
		calEstimateCost();
		setCost();
		calForPVS();
		calForGeneral('speed','24');
		calForGeneral('mm','18');
	}
	
	function checkForBlank(objVal)
	{
		if(jQuery.trim(objVal) == '')
		{
			return 0;
		}
		return objVal;
	}
	
</script>
<script>
	function roundTo2Digit(num)
	{
		if(isNaN(num)) num = 0;
		num = parseFloat(num);
		num = num;
		num = num.toFixed(2);
		return num;
	}
</script>
<script>
	function round_func(num)
	{
		if(isNaN(num)) num = 0;
		num = parseFloat(num);
		num = num;
		num = num.toFixed(2);
		return num;
	}
	function round_funcfor4digits(num)
	{
		if(isNaN(num)) num = 0;
		num = parseFloat(num);
		num = num;
		num = num.toFixed(4);
		return num;
	}
</script>
<script>
	function formatCurrency(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + '$' + num + '.' + cents);
	
	}
</script>
<script>
function resetToPrevios()
{
		url = "MPOAction.do?type=1";	
		var serverSidePoType = '<c:out value="${MPOBean.mpoItemType}"/>';
		if(isExists(document.all.mpoActivityResource))
		{
			var len = document.all.mpoActivityResource.length;
			if(!len)
			{
				if(document.all.mpoItemType.value != serverSidePoType){
						document.all.mpoActivityPlanQuantity.value = '';
						document.all.mpoActivityPlanUnitCost.value = '';
				}
			}
			else
			{
				for(var i=0;i<len;i++)
				{
					if(document.all.mpoItemType.value != serverSidePoType){
						document.all.mpoActivityPlanQuantity[i].value = '';
						document.all.mpoActivityPlanUnitCost[i].value = '';
					}
				}
			}
		}
		document.forms[0].action=url;
		document.forms[0].submit();
}
</script>
</head>
<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" cellpadding="0" cellspacing="1" height="300">
	<tr valign="top">
		<td colspan=2>
			<table width="100%" cellpadding="1" cellspacing="1" <c:if test="${empty requestScope.tabId}"> style="display:none" </c:if>  id="mainTable">
				<tr>
					<td colspan="2" class = 'Ntextoleftalignnowrappaleyellow'><b>Name:&nbsp;&nbsp;&nbsp;&nbsp;</b>
					<input type="text" name="mpoName" value="<c:out value='${MPOBean.mpoName}'/>"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if>  size="80" class="text"></td>
					<td class = 'Ntextoleftalignnowrappaleyellow' colspan="2"><b>Master PO Item:&nbsp;&nbsp;&nbsp;&nbsp;</b>
						<select name="mpoItemType" class="select" onchange="changePOItem();"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> >
							<option value="fp" <c:if test="${MPOBean.mpoItemType eq 'fp'}">selected = "selected"</c:if>>Fixed Price Services</option>
							<option value="ht" <c:if test="${MPOBean.mpoItemType eq 'ht'}">selected = "selected"</c:if>>Hourly Services</option>
							<option value="mt" <c:if test="${MPOBean.mpoItemType eq 'mt'}">selected = "selected"</c:if>>Material</option>
							<option value="si" <c:if test="${MPOBean.mpoItemType eq 'si'}">selected = "selected"</c:if>>Show Items</option>
						</select>
					<td>
				</tr>
				<c:if test="${empty requestScope.isContinue}">
					<tr>
						<td width="100%" colspan="4" align="right" style="padding-right: 5px;">
							<input type="button" name="continue" class="button_c" value="Continue" onclick="showTable('1');">
						</td>
					</tr>
				</c:if>
				<c:if test="${requestScope.listSize <= '0'}">
					<tr>
						<td class="message" colspan="4">No Activities Found.</td>
					</tr>
				</c:if>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<c:set var="prevJobfield"  value=""/>
		<c:set var="currJobfield"  value=""/>
		<c:set var="prevActfield" value=""/>
		<c:set var="currActfield" value=""/>
			<table cellpadding="2" cellspacing="1" width="100%">
				<tr><td>
				<table cellpadding="1" cellspacing="1" width="100%" id="actTable" border="0">
					<c:forEach var="list" items="${requestScope.activitylist}" varStatus="count">
						<c:if test="${count.first}">
							<tr>
								<td align="center" class="texthNoBorder" rowspan="2">Job/<br>Addendum</td>							
								<td align="center" class="texthNoBorder" rowspan="2">Activity</td>
								<td align="center" class="texthNoBorder" rowspan="2">Resource</td>
								<td align="center" class="texthNoBorder" rowspan="2">Type</td>
								<td align="center" class="texthNoBorder" colspan="3">Quantity</td>
								<td align="center" class="texthNoBorder" colspan="2">Estimated Cost</td>
								<td align="center" class="texthNoBorder" colspan="3" id="planned">As Planned</td>
								<td align="center" class="texthNoBorder" rowspan="2">Show<br>on<br>WO</td>
							</tr>
							<tr>
								<td class="texthNoBorder">Activity</td>
								<td class="texthNoBorder">Resource</td>
								<td class="texthNoBorder">Total</td>
								<td class="texthNoBorder">Unit</td>
								<td class="texthNoBorder">Subtotal</td>
								<td class="texthNoBorder" id="planned" colspan='1'>Quantity</td>
								<td class="texthNoBorder" id="planned">Unit Cost</td>
								<td class="texthNoBorder" id="planned">Subtotal</td>
							</tr>
						</c:if>
						<c:if test="${prevJobfield eq list[2]}">
							<c:set var="currJobfield"  value=""/>
							<c:if test="${prevActfield eq list[4]}">
								<c:set var="currActfield"  value=""/>
							</c:if>
							<c:if test="${prevActfield ne list[4]}">
								<c:set var="currActfield"  value="${list[4]}"/>
								<c:set var="prevActfield"  value="${list[4]}"/>
							</c:if>
						</c:if>
						<c:if test="${prevJobfield ne list[2]}">
							<c:set var="currJobfield"  value="${list[2]}"/>
							<c:set var="currActfield"  value="${list[4]}"/>
							<c:set var="prevJobfield"  value="${list[2]}"/>
							<c:set var="prevActfield"  value="${list[4]}"/>
						</c:if>
								<tr>
									<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> valign="middle"><c:out value="${currJobfield}" /></td>
									<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> valign="middle"><c:out value="${currActfield}" /></td>
									<td 
										<c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if>
										<c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> valign="middle">
										<input type="checkbox" id="mpoActivityResourceId" name="mpoActivityResource" 
											<c:forEach var="innerList" items="${MPOBean.mpoActivityResource}" varStatus="innerCount"> 
												<c:if test="${list[6] eq MPOBean.mpoActivityResource[innerCount.index]}"> checked = "checked"</c:if>
											</c:forEach> 
												<c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if>
													class="chkbx" value="<c:out value="${list[6]}" />" height="5" onclick="calculateSubTotal(this,'actTable', <c:out value='${count.index}'/>);exceedSubtotal('<c:out value="${count.index}"/>',this);" >
													&nbsp;&nbsp;<c:out value="${list[7]}"/> 
													<input type="hidden" name="resourceHidden" value="<c:out value="${list[6]}" />"/>
									</td>
									<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> title="<c:if test="${list[19] eq 'Travel'}"><c:out value="${list[21]}" /></c:if><c:if test="${list[19] ne 'Travel'}"><c:out value="${list[19]}" /></c:if>"><c:out value="${list[10]}" /></td>
									<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center" ><c:out value="${list[5]}" /></td>
									<td align="right" <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"><fmt:formatNumber value="${list[8]}" type="currency" pattern="#####0.0000"/></td>
									<td align="right" <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"></td>
									<td align="right" <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"><fmt:formatNumber value="${list[11]}" type="currency" pattern="#####0.00"/></td>
									<td align="right" <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"></td>
									<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> id="planned" ><input type="text" name="mpoActivityPlanQuantity"  <c:forEach var="innerList" items="${MPOBean.mpoActivityResource}" varStatus="innerCount">
																																																																		<c:if test="${list[6] eq MPOBean.mpoActivityResource[innerCount.index]}">
																																																																			value="<fmt:formatNumber value="${MPOBean.mpoActivityPlanQuantity[innerCount.index]}" type="currency" pattern="#####0.0000"/>" 
																																																																		</c:if>
																																																																		
																																																																	</c:forEach>  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if>  class="text" style="text-align:right"
																																																																	 size="8" onChange="calCostForPlan(this);exceedSubtotal('<c:out value="${count.index}"/>',this);return false;"/>
																																																																	</td>
									<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> id="planned"><input type="text" name="mpoActivityPlanUnitCost"  <c:forEach var="innerList" items="${MPOBean.mpoActivityResource}" varStatus="innerCount">
																																																																		<c:if test="${list[6] eq MPOBean.mpoActivityResource[innerCount.index]}">
																																																																			value="<fmt:formatNumber value="${MPOBean.mpoActivityPlanUnitCost[innerCount.index]}" type="currency" pattern="#####0.00"/>" 
																																																																		</c:if>
																																																																		
																																																																	</c:forEach>  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if>  class="text" style="text-align:right"
																																																																	size="8" onChange="calCostForPlan(this);exceedSubtotal('<c:out value="${count.index}"/>',this);return false;"/></td>
									<td align="right" <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> id="planned" align="center"></td>
									<td <c:if test="${(count.index % 2)eq '0'}">class='Ntextoleftalignnowrap'</c:if><c:if test="${(count.index % 2) eq '1'}">class='Ntexteleftalignnowrap'</c:if> align="center"><input type="checkbox" name="mpoActivityIsWOShow" class="chkbx1"<c:forEach var="innerList" items="${MPOBean.mpoActivityResource}" varStatus="innerCount">
																																																																					<c:if test="${list[6] eq MPOBean.mpoActivityResource[innerCount.index]}">
																																																																						<c:if test="${ MPOBean.mpoActivityIsWOShow[innerCount.index] eq 'Y'}">
																																																																							checked = "checked"
																																																																						</c:if>
																																																																					</c:if>
																																																																		
																																																																				</c:forEach>  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if>  value="<c:out value="${list[6]}" />" ><input type="hidden" name="isShowHidden" value="Y"/></td>
									<input type="hidden" id="mpoActResourceSubName" name="mpoActResourceSubName" value="<c:if test="${list[19] eq 'Travel'}"><c:out value="${list[21]}" /></c:if><c:if test="${list[19] ne 'Travel'}"><c:out value="${list[19]}" /></c:if>"/>
									<input type="hidden" name="mpoActId" value="<c:out value="${list[3]}" />"/>
									<input type="hidden" name="mpoJobId" value="<c:out value="${list[1]}" />"/>
									<input type="hidden" name="mpoActResourceSubId" value="<c:out value="${list[18]}" />"/>
									<input type="hidden" id="mpoTotal" name="mpoTotal" value="">
									<input type="hidden" name="mpoUnit" value="<c:out value="${list[11]}" />">
									<input type="hidden" name="mpoSubTotal" value="">
									<input type="hidden" id="mpoActivityPlanSubTotal" name="mpoActivityPlanSubTotal" value="">
									<input type="hidden" name="mpoRes" value="<c:out value="${list[8]}" />">
									<input type="hidden" name="mpoAct" value="<c:out value="${list[5]}" />">
									<input type="hidden" name="mpoType" value="<c:out value="${list[9]}" />">
									<input type="hidden" id="mpoTypeName" name="mpoTypeName" value="<c:out value="${list[10]}" />">
								</tr>
						
							<c:if test="${count.last}">
								<tr id="est">
									<td valign="top" rowspan="3" colspan=2 class='Ntexteleftalignnowrap'><input type="button" name="selectAll" value="Select All" class="button_c" onclick="checkSelectAll('actTable');">
										<input type="button" name="deSelectAll" value="Deselect All" class="button_c" onclick="checkDeSelectAll('actTable');">
									</td>
									<td id='imgMeetOrBeat' align="right" valign='middle' rowspan="3" colspan=7 class='Ntexteleftalignnowrap' valign="top"><img src="images/MeetOrBeat_small.gif" ></td>
									<td  colspan="4" class='Ntexteleftalignnowrap' >&nbsp;</td>
								</tr>
								
								<tr>
									<td colspan="3" align="right" valign='middle' class='Ntexteleftalignnowrap' >
										Estimated Total Cost:&nbsp;
										<input  type="text" name="estimatedCost"  readonly="readonly"  class="text" size="10" style="text-align:right" <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if>></td>
										<input type="hidden" name="estimatedAuthorizedCost"/>
										<input type="hidden" name="estimatedTotalCost"/>
									<td class='Ntexteleftalignnowrap' >&nbsp;</td>
								</tr>
								<tr>
									<td align="right" class = "colLight" colspan="3">
									&nbsp;
									</td>
									<td class = "colLight">&nbsp;</td>
								</tr>
								<tr id="hideMessageRow" style="display: none;">
									<td align="right" class = "colLight" colspan="12">
										<table cellpadding="0" cellspacing="0" border="0" id="messageTable" style="display: block;">
											
										</table>
									</td>
									<td class = "colLight">&nbsp;</td>
								</tr>
								<tr>
									<td align="right" class = "colLight" colspan="12">
										<c:if test="${not empty requestScope.tabId}">
											<input type="button" name="Save" value="Save" class="button_c" onclick="onSave('1');"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> >
											<input type="button" name="Reset" value="Reset" onclick="resetToPrevios('1');" class="button_c"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> >
										</c:if>
										<c:if test="${empty requestScope.tabId}">
											&nbsp;
										</c:if>
									</td> 
									<td class = "colLight">&nbsp;</td>
								</tr>							
							</c:if>
						</c:forEach>
				</table>
				</tr></td>
			</table>	
		</td>
	</tr>
	<tr <c:if test="${requestScope.listSize <=0 || empty requestScope.listSize}"> style="display:none" </c:if> id="generalTable" >
		<td >
			<table cellpadding="1" cellspacing="1" >
				<tr>
					<td ><h4 id="header">Cost Breakdown and Variations </h2></td>
				</tr>
				<tr>
					<td>
						<table class="labelowhitebackground" id="costTable" >
							<tr>
								<td class="labelowhitebackground" align="center" width="80">&nbsp;</td>
								<td class="labelowhitebackground" align="center" width="80">Labor</td>
								<td class="labelowhitebackground" align="center" width="80">Materials</td>
								<td class="labelowhitebackground" align="center" width="80">Freight</td>
								<td class="labelowhitebackground" align="center" width="80">Travel</td>
								<td class="labelowhitebackground" align="center" width="80"><b>Totals</b></td>
							</tr>
							<tr>
								<td class="labelowhitebackground">Contract</td>
								<td class="labelorightwhitebackground" >$1,000</td>
								<td class="labelorightwhitebackground" >$1,000</td>
								<td class="labelorightwhitebackground" >$1,000</td>
								<td class="labelorightwhitebackground" >$1,000</td>
								<td class="labelorightwhitebackground" >$1,000</td>
							</tr>
							<tr>
								<td class="labelowhitebackground">Contingent</td>
								<td class="labelorightwhitebackground" >$1,000</td>
								<td class="labelorightwhitebackground" >$1,000</td>
								<td class="labelorightwhitebackground" >$1,000</td>
								<td class="labelorightwhitebackground" >$1,000</td>
								<td class="labelorightwhitebackground" >$1,000</td>
							</tr>
							<tr>
								<td class="labelowhitebackground"><b>Totals</b></td>
								<td class="labelorightwhitebackground" >$X,XXX.XX</td>
								<td class="labelorightwhitebackground" >$X,XXX.XX</td>
								<td class="labelorightwhitebackground" >$X,XXX.XX</td>
								<td class="labelorightwhitebackground" >$X,XXX.XX</td>
								<td class="labelorightwhitebackground" >$X,XXX.XX</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	
		<td >
			<table cellpadding="1" cellspacing="1" >
				<tr>
					<td ><h4 id="header">Cost Variations by PO Type </h2></td>
					</td>
				</tr>
				<tr>
					<td>
						<table  class="labelowhitebackground" id="CVPT">
							<tr>
								<td class="labelowhitebackground" width="80">&nbsp;
								</td>
								<td class="labelowhitebackground" width="105" nowrap="nowrap">Contractor Labor
								</td>
								<td class="labelowhitebackground" width="80">Hourly Travel
								</td>
								<td class="labelowhitebackground" width="80">Total
								</td>
								<td class="labelowhitebackground" width="80"><b>Cost Savings</b>
								</td>
							</tr>
							<tr>
								<td class="labelowhitebackground">Minuteman
								</td>
								<td class="labelorightwhitebackground" >$1,000
								</td>
								<td class="labelorightwhitebackground" >$1,000
								</td>
								<td class="labelorightwhitebackground" >$2,000
								</td>
								<td class="labelorightwhitebackground" >$2,000
								</td>
							</tr>
							<tr>
								<td class="labelowhitebackground">Speedpay
								</td>
								<td class="labelorightwhitebackground" >$1,500
								</td>
								<td class="labelorightwhitebackground" >$1,500
								</td>
								<td class="labelorightwhitebackground" >$3,000
								</td>
								<td class="labelorightwhitebackground" >$1,000
								</td>
							</tr>
							<tr>
								<td class="labelowhitebackground">PVS
								</td>
								<td class="labelorightwhitebackground" >$2,500
								</td>
								<td class="labelorightwhitebackground" >$2,500
								</td >
								<td class="labelorightwhitebackground">$4,000
								</td>
								<td class="colDarkNoWidth" >&nbsp;
								</td>
							</tr>
						</table> 
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>