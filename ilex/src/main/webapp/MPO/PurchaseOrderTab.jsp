<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<head>
	<script>
function getSowAssumption(checkType){
	   var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 if(checkType == 'partner'){
               		 	document.forms[0].woNOA.value =  response;
               		 }else if(checkType == 'specialIns'){
               		 	document.forms[0].woSI.value =  response;
               		 }else if(checkType == 'specialCondt'){
               		 	document.forms[0].special_condition.value =  response;
               		 }
               }
        }
        
        ajaxRequest.open("POST", "MPOAction.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("hmode=sowAssumption&mpoId="+document.forms[0].mpoId.value+"&check="+checkType+"&appendixId="+document.forms[0].mp_appendix_id.value); 
}
 
</script>
	
</head>
     	<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" 
     	 height="300" cellspacing="1" cellpadding="0" width="100%">
     		<tr valign="top" height="10">
     			<td  width="100%"  colspan="7" cellspacing="1" cellpadding="0">
     				<table width="100%" cellspacing="0" cellpadding="0" >
     					<tr>
     						<td>
			     				<table width="100%" cellspacing="0" cellpadding="0">
			     					<tr>
			     						<td width="50%"  align="left" nowrap class='Ntextoleftalignnowrappaleyellow'>
			     							<span nowrap><b>Name:</b></span><span nowrap > <c:out value='${MPOBean.mpoName}'/></span>
			     						</td>
						     			<td width="50%" align="right" nowrap style="padding-right: 6px;" class='Ntextoleftalignnowrappaleyellow'>
						     				<span nowrap ><b>Master PO Item:</b></span><span nowrap > <c:out value="${mpoItem}"/></span>
						     			</td>
						     		</tr>
						     	</table>
						     </td>
					     </tr>
				    </table>
			     </td>
     		</tr>
     		<tr valign="top">
     			<td>
     				<table cellpadding="2" cellspacing="1" width="100%">
					<tr><td>
					<table width="100%" cellspacing="1" cellpadding="0" border="0">
			     		<tr align="center" valign="top" height="25">
			     			<td align="center" class = "labelolightnoborder">Special Conditions</td>
			     			<td width="30%"></td>
			     		</tr>
			     		<tr valign="center" >
			     			<td ><textarea  name="special_condition" cols="160" rows="8" class="textarea"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> ><c:out value='${MPOBean.special_condition}'/></textarea></td>
			     			<td width="30%">
			     				&nbsp;<input type="button" name="defaultPartnerSowAssumption" onclick="getSowAssumption('specialCondt');" class="button_c" value="Default"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> />
			     			</td>
			     		</tr>
			     		<tr align="center" valign="top">
						   	<td  class = "colLight" align="left" >
						  		<input type="button" name="purchaseOrder" onclick="onSave('2');" class="button_c" value="Save"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> />
								<input type="reset" name="resetPOtab" class="button_c" value="Reset"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> />
							</td>
							<td width="30%"></td>
					   </tr>
				 </table>
				 </td></tr>
				 </table>
				</td>
			</tr>
     	</table>
     