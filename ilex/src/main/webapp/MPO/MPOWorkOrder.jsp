<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<head>
	<script>
function getSowAssumption(checkType){
	   var ajaxRequest;  // The variable that makes Ajax possible!
        
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }
        // Create a function that will receive data sent from the server
        ajaxRequest.onreadystatechange = function(){
               if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
               		 var response = ajaxRequest.responseText;
               		 if(checkType == 'partner'){
               		 	document.forms[0].woNOA.value =  response;
               		 }else if(checkType == 'specialIns'){
               		 	document.forms[0].woSI.value =  response;
               		 }else if(checkType == 'specialCondt'){
               		 	document.forms[0].special_condition.value =  response;
               		 }
               }
        }
        
        ajaxRequest.open("POST", "MPOAction.do", true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send("hmode=sowAssumption&mpoId="+document.forms[0].mpoId.value+"&check="+checkType+"&appendixId="+document.forms[0].mp_appendix_id.value); 
}
 
</script>
<script>
			
	function getAjaxRequestObject(){
	var ajaxRequest;  // The variable that makes Ajax possible!
        try{
               // Opera 8.0+, Firefox, Safari
               ajaxRequest = new XMLHttpRequest();
        } catch (e){
               // Internet Explorer Browsers
               try{
                       ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
               } catch (e) {
                       try{
                               ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                       } catch (e){
                               // Something went wrong
                               alert("Your browser broke!");
                               return false;
                       }
               }
        }	
	return ajaxRequest;        
}



	function getStandardContactLine() {			
		var ajaxRequest = getAjaxRequestObject();
		var initialSI = document.getElementById("sitextarea").value;
		//alert("initialSI: "+initialSI)	;
		    ajaxRequest.onreadystatechange = function(){
       if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){ 
           var xmlDoc=ajaxRequest.responseXML;        		
           		
           	var id;
			 id = xmlDoc.getElementsByTagName("standardConLine")[0];			    
			 var standardContactLine=id.getElementsByTagName("standardContactLine")[0].firstChild.nodeValue;					 
			 //alert("standardContactLine: "+standardContactLine);
			 document.getElementById("sitextarea").value = "";	
			 document.getElementById("sitextarea").value = initialSI+ standardContactLine;
		}
    }
   
    
		var url = "POAction.do?hmode=getStandarContactLine";		
        ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajaxRequest.send(null); 
	
	}
</script>	
</head>
     	<table style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" 
     	 height="300" cellspacing="1" cellpadding="0" border=0>
     		<tr valign="top" height="10">
     			<td  cellspacing="1" cellpadding="0">
     				<table width="100%" cellspacing="0" cellpadding="0" >
     					<tr>
     						<td>
			     				<table width="100%" cellspacing="0" cellpadding="0">
			     					<tr>
			     						<td width="50%"  align="left" nowrap class='Ntextoleftalignnowrappaleyellow'>
			     							<span nowrap ><b>Name:</b></span><span nowrap > <c:out value='${MPOBean.mpoName}'/></span>
			     						</td>
						     			<td width="50%" align="right" nowrap style="padding-right: 6px;" class='Ntextoleftalignnowrappaleyellow'>
						     				<span nowrap ><b>Master PO Item:</b></span><span nowrap > <c:out value="${mpoItem}"/></span>
						     			</td>
						     		</tr>
						     	</table>
						     </td>
					     </tr>
				    </table>
			     </td>
     		</tr>
     		<tr valign="top">
     			<td>
     			<table cellpadding="2" cellspacing="1" width="100%">
					<tr><td>
					<table   cellspacing="1" cellpadding="0" border=0>
			     		<tr valign="top" height="25" >
			     			<td align="center"  colspan="3" class = "labelolightnoborder">Description of Work Requested</td>
			     		</tr>
					     <tr>
								<td class='Ntextoleftalignwrap'  valign="top" nowrap>
									NATURE OF ACTIVITY <br>(SOW and Assumptions)
								</td>
								<td class='colLight' valign="middle" >
								<table  height='100%' valign='middle' cellspacing="1" cellpadding="0" border=0><tr><TD>
									<textarea  name="woNOA" class="textarea" rows="6" cols="145"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> ><c:out value='${MPOBean.woNOA}'/></textarea>
								</td><td>&nbsp;&nbsp;
									<input type="button" name="defaultPartnerSowAssumption" class="button_c" value="Default" onclick="getSowAssumption('partner');"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> />
								</TD></tr></table>
								
							    </td>
							
						</tr>
						<tr>
							<td class='Ntextoleftalignwrap' valign="top" nowrap>
								SPECIAL INSTRUCTIONS <br>(Additional details may be attached)
							</td>
							<td class='colLight' valign="middle">
							<table  height='100%' valign='middle' cellspacing="1" cellpadding="0" border=0><tr><TD>
								<textarea id="sitextarea" name="woSI"  class="textarea" rows="6" cols="145"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> ><c:out value='${MPOBean.woSI}'/></textarea>
								</td><td>&nbsp;&nbsp;
								<input type="button" name="defaultPartnerSowAssumption" class="button_c" value="Default" onclick="getSowAssumption('specialIns');"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> />
								</br>&nbsp;&nbsp;&nbsp;<input type="button" name="add_contact" class="button_c" value="Add Contact" onclick="getStandardContactLine();" />
								</TD></tr></table>
							</td>
						</tr>
						<tr>
							<td class='Ntextoleftalignwrap'  valign="top" nowrap>
								REQUIRED EQUIPMENT <br>(Additional equipment may be required)
							</td>
							<td class='Ntexteleftalignnowrap' colspan ="2">
								<table cellpadding="0" cellspacing="0" border="0" >
									<c:forEach var="list" items="${MPOBean.woToolsList}" varStatus="count">						         	
										<c:if test="${(count.index)%4 eq 0}">			         								
							         		<tr>
							         	</c:if>	
							         	<c:if test="${(count.index)%4 eq 0}">			         								
							         		<td  class='Ntexteleftalignnowrapfirstborder'>
							         	</c:if>	
							    		<c:if test="${(count.index)%4 ne 0 && (count.index+1)%4 ne 0 }">			         								
							         		<td  class='Ntexteleftalignnowraprightborder'>
							         	</c:if>
							           	<c:if test="${(count.index+1)%4 eq 0}">
							         		<td class='Ntexteleftalignnowrapwithoutborder' >
							         	</c:if>
												<input type="checkbox" name="woRE" class="chkbx"  value="<c:out value='${list[0]}'/>" 
													<c:forEach var="innerList" items="${MPOBean.woRE}" varStatus="innerCount">	
														<c:if test="${list[0] eq MPOBean.woRE[innerCount.index]}">			         	
														checked="checked"
														</c:if>
													</c:forEach> <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> 	
												> 
												<c:if test="${list[5] eq 'N'}">
													<font color='red'>	
														<i><c:out value="${list[1]}"/> (Inactive!)<i>
													</font>
												</c:if>
												<c:if test="${list[5] ne 'N'}">
													<c:out value="${list[1]}"/>
												</c:if>
												<input type="hidden" name="workOrderCheck" value="<c:out value='${list[0]}'/>"></td>

										<c:if test="${(count.index+1)%4 eq 0 || count.last}">			         								
											<c:if test="${count.last && (count.index+1)%4 ne 0}">
												<c:if test="${(count.index+1)%4 eq 1}">
													<td  class='Ntexteleftalignnowraprightborder'>&nbsp;</td>
													<td  class='Ntexteleftalignnowraprightborder'>&nbsp;</td>
													<td  class='Ntexteleftalignnowrapwithoutborder'>&nbsp;</td>
												</c:if>
												<c:if test="${(count.index+1)%4 eq 2}">
													<td  class='Ntexteleftalignnowraprightborder'>&nbsp;</td>
													<td  class='Ntexteleftalignnowrapwithoutborder'>&nbsp;</td>
												</c:if>
												<c:if test="${(count.index+1)%4 eq 3}">
													<td  class='Ntexteleftalignnowrapwithoutborder'>&nbsp;</td>
												</c:if>
								         	</c:if>	
							         		</tr>
							         	</c:if>	

						         	</c:forEach>
						         	
								</table>
							</td>
						</tr>
					   <tr>
						   	<td  class = "colLight" align="left" colspan='3'>
						   		<input type="button" name="button1" class="button_c" value="Save" onclick="onSave('3')"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> />
						   		<input type="reset" name="button2" class="button_c" value="Reset"  <c:if test="${MPOBean.mpoStatus eq 'Approved'}">disabled="disabled"</c:if> />
						   	</td>
						</tr>
				</table>
				</td></tr></table>
			</td>
		</tr>
	</table>