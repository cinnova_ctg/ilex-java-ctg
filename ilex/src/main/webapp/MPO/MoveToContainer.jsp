<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<title>Insert title here</title>
<script>
function callOpenningPage()
{
	<c:if test="${empty param.devId}">
		opener.redirrectToOpener('<c:out value="${param.docIdList}"/>');
	</c:if>
	
	<c:if test="${not empty param.devId}">
		opener.insertDoc('<c:out value="${param.devId}"/>','<c:out value="${param.poDocId}"/>');
	</c:if>
	window.close ();
}
</script>
</head>
<body onload="callOpenningPage();">
	<div class="text">please wait....</div>
</body>
</html>