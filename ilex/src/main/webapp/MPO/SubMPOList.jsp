<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<% 
	String backgroundclass = null; //- td style class
	boolean csschooser = true;	
	String backgroundclas = null; //- td style class
	boolean csschoosers = true;// - for alternet change color of rows
%>
<head>
<script>
function editClickedMPO(mpoId,tabId)
	{
	url = "MPOAction.do?type="+tabId+"&clicked=edit&mpoId="+mpoId;
	document.forms[0].action=url;
	document.forms[0].submit();
	}
</script>
<script>
	function approveMPO(mpoId,appendixId)
	{
		<c:if test="${not empty MPOBean.isPM}">
			var returnVal = confirm("You want to  Approve this MPO?");
		</c:if>
		
		<c:if test="${empty MPOBean.isPM}">
			var returnVal = confirm("Are you sure, you want to request for the Approval?");
		</c:if>
		
		var url = ""; 
		if (returnVal>0){
		<c:if test="${not empty MPOBean.isPM}">
			url = "MPOAction.do?hmode=approveMPO&type=0&mpoId="+mpoId+"&status=Approved&appendixId="+document.forms[0].mp_appendix_id.value;
		</c:if>
		<c:if test="${empty MPOBean.isPM}">
			url = "MPOAction.do?hmode=approveMPO&type=0&mpoId="+mpoId+"&status=Pending&appendixId="+document.forms[0].mp_appendix_id.value;
		</c:if>		
		document.forms[0].action = url;
		document.forms[0].submit();
		}
		else
			return false;
	}
</script>
<script>
	function deleteMPO(mpoId,appendixId,tabId)
	{
		var returnVal = confirm("Do you really want to delete the MPO?")	;
		if (returnVal>0){
			var url = "MPOAction.do?hmode=deleteMPO&mpoId="+mpoId+"&type="+tabId; 
			document.forms[0].action = url;
			document.forms[0].submit();
		}
		else
			return false;
	}
</script>
<script>
function resetToDraft(mpoId,appendixId)
{
	url = "MPOAction.do?hmode=resetToDraft&type=0&mpoId="+mpoId+"&appendixId="+appendixId+"&status=Draft";		
	
	document.forms[0].action = url;
	document.forms[0].submit();
}
</script>
<script>
function showTabs(buttonPressed,tabIndex)
		{
		url = "MPOAction.do?type="+tabIndex+"&actionTaken="+buttonPressed;
		document.forms[0].action=url;
		document.forms[0].submit();
		}
</script>
</head>
  <table  style="border-color: #D9DFEF;border-width: 2px;border-style: solid;" height="300" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td>
       	  <table cellpadding="0" cellspacing="1">
       	  <tr>
		    <td class = "Ntryb" >
			    <table width="100%" border="0" cellpadding="0" cellspacing="0">
			    <tr>
			    	<td class = "texthNoBorder" align="center"  width="100%">
			    		Name
			    	</td>
			    	<td class = "texthNoBorder" align="left">
			    		<table width="100%" border="0"><tr><td  align="right">
			    		<input type="button" name="Add" value="Add" onclick="showTabs('AddNewMPO','1');" class="NbuttonBrown"
			    		<c:if test="${requestScope.isTicket eq 'Ticket'}">disabled="disabled"</c:if>>
			    	</td></tr></table>
			    	</td>
			    </tr>
			    </table>
		    </td>
           	<td class = "texthNoBorder"  >Master PO Item</td>
           	<td class = "texthNoBorder" >Estimated Total Cost</td>
           	<td class = "texthNoBorder" align ="center">Status</td>
           	<td class = "texthNoBorder" align ="center">Actions/Workflow</td>
         	</tr>
         	<c:if test="${not empty requestScope.mpoList}">
				<c:forEach items="${requestScope.mpoList}" var="list" varStatus="status" >
				<%
					if ( csschoosers == true ){
						backgroundclas = "Ntextoleftalignnowrap";
						csschoosers = false;
					}else{
						csschoosers = true;	
						backgroundclas = "Ntexteleftalignnowrap";
					}
				%>
				
					<tr>		
						<td class = <%=backgroundclas%>> <a href ="#" onclick="editClickedMPO('<c:out value="${list[0]}"/>','1')"><c:out value="${list[1]}"/></a></td>
						<td class = <%=backgroundclas%> align="center"><c:if test="${list[4] eq 'fp'}">Fixed Price</c:if>
																	   <c:if test="${list[4] eq 'ht'}">Hourly Travel</c:if>
																	   <c:if test="${list[4] eq 'mt'}">Material</c:if>
																	   <c:if test="${list[4] eq 'si'}">Show Item</c:if>			
						
						<td  class = <%=backgroundclas%> align="right"><fmt:formatNumber value='${list[5]}' type='currency' pattern='$ #####0.00'/></td>
						<td class = <%=backgroundclas%> align="center"><c:if test="${list[2] eq 'Pending' || list[2] eq 'Draft'}">Draft</c:if>
																<c:if test="${list[2] ne 'Pending' && list[2] ne 'Draft'}">
																	<c:out value="${list[2]}"/>
																</c:if>
						</td>
						<td class = <%=backgroundclas%> >[
							<c:if test="${list[2] eq 'Pending' || list[2] eq 'Draft'}">
								<a href = "#" onclick="editClickedMPO('<c:out value="${list[0]}"/>','1')">Edit</a>&nbsp;|&nbsp;<a href = "#" onclick="deleteMPO('<c:out value="${list[0]}"/>','<c:out value="${list[8]}"/>','0');">Delete</a>
								&nbsp;|&nbsp;
							</c:if>						
							<c:if test="${empty MPOBean.isPM}">
								<c:if test="${list[2] eq 'Draft'}">
									<a href = "#" onclick="approveMPO('<c:out value="${list[0]}"/>','<c:out value="${list[8]}"/>');">Request Approval</a>&nbsp;|
								</c:if>	
								<c:if test="${list[2] eq 'Pending'}">
									Request Approval&nbsp;|
								</c:if>	
							</c:if>
							<c:if test="${not empty MPOBean.isPM}">
								<c:if test="${list[2] eq 'Draft' || list[2] eq 'Pending'}">
									<a href = "#" onclick="approveMPO('<c:out value="${list[0]}"/>','<c:out value="${list[8]}"/>');">Approve</a>&nbsp;|
								</c:if>
								<c:if test="${list[2] eq 'Approved'}">
									<a href = "#" onclick = "resetToDraft('<c:out value="${list[0]}"/>','<c:out value="${list[8]}"/>');">Reset to Draft</a>&nbsp;|
								</c:if>
							</c:if>
							<a >PO</a>&nbsp;|&nbsp;<a >WO&nbsp;]</a>
							<c:if test="${list[2] eq 'Pending' }">
								<img src="./images/WorkFlow.gif" alt="Already Requested" height="10" width="10">
							</c:if>
						</td>
			  		</tr>
			  	</c:forEach>
			  </c:if>
			  <c:if test="${empty requestScope.mpoList}">
			  	<tr>
			  		<td  nowrap class = "Simpletext" colspan="7" >
			  			There are no Master Purchase Orders (MPOs) defined for this appendix. Click the Add button to define a new MPO.
			  		</td>
			  	</tr>
			  </c:if>
      </table>
     </td>
    </tr>
  </table>




