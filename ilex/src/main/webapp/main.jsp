<!DOCTYPE HTML>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java"%>


<c:if test="${username ne null}">
	<jsp:forward page="/index.jsp">
		<jsp:param name="mode" value="${param.mode}" />
	</jsp:forward>

</c:if>




<html>
<head>
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<style type="text/css">
#login_page {
    text-align: center;
    padding: 0px;
    padding-top: 10px;
    margin: 0px;
    background: #fff;
}

#login_page h4 {
    margin-top: 0px;
    margin-bottom: 22px;
}

.form-wrap {
    width: 230px;
    margin: 0px auto 20px;
}

#login_page {
    color: #000;
}

#login_page article {
    display: block;
    text-align: center;
    width: 650px;
    margin: 0 auto;
    padding-top: 30px;
}

#login_page a {
    color: #dc8100;
    text-decoration: none;
}

#login_page a:hover {
    color: #333;
    text-decoration: none;
}

#login_page p span {
    width: 50px;
    margin-left: -30px;
    display: inline-block;
    text-align: right;
}

#login_page p.failed,#login_page p.logged_out {
    font-weight: bold;
    font-size: 12px;
    margin-bottom: -5px;
    margin-top: 20px;
}

#login_page p.logged_out {
    color: #4f81bd;
}

#login_page p.failed {
    color: #fc0103;
}

#btn-login {
    width: 50%;
}

.panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
    box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
}

.panel-default {
    border-color: #dedede;
}

.panel-default {
    border-color: #ddd;
}

.top_right_header {
    background: url(images/background_top_right.jpg) no-repeat right top;
    width: 575px;
    height: 43px;
    vertical-align: top;
    padding-top: 22px;
}

.top_right_header span {
    color: #818181;
    float: right;
    font-family: "scala_sans_procondensed";
    text-transform: lowercase;
    font-size: 25px;
    padding-right: 20px;
    display: inline-block;
    background: url(css/images/bod.jpg) no-repeat left -19px;
    margin-left: 8px;
    padding-left: 8px !important;
    line-height: 27px;
}

.top_right_header_sep {
    background: none !important;
    color: #818181;
    font-family: "scala_sans_probold" !important;
    font-size: 25px !important;
    text-transform: uppercase !important;
    padding-right: 0px !important;
}

.ilex-menu {
    float: left;
    width: 100%;
    background: url("images/gray_bg.gif") repeat center bottom;
    height: 28px;
}

.form-horizontal .form-group {
    margin-bottom: 10px;
}

#btn-login {
    width: 50%;
}

.form-control {
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}

.input-sm, .form-group-sm .form-control {
    height: 30px;
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
}

.input-sm, .form-horizontal .form-group-sm .form-control, .form-control {
    padding: 4px 4px;
    font-size: 11px;
    line-height: 10px;
}

.input-sm, select.input-sm, .form-horizontal .form-group-sm .form-control:not([type=file]):not([multiple]):not(textarea):not([size]) {
    height: 24px;
}

.btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}

.popup, .btn {
    display: inline-block;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-size: 10px;
    padding: 1px 8px 1px;
    margin-bottom: 0;
    line-height: 18px;
    color: #333333;
    text-align: center;
    vertical-align: middle;
    cursor: pointer;
    background: #fff;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#e6e6e6', GradientType=0);
    border-color: #e6e6e6 #e6e6e6 #bfbfbf;
    border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
    filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
    border: 1px solid #cccccc;
    border-bottom-color: #b3b3b3;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
    -moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
    box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
}

.btn-primary {
    color: #fff;
    background-color: #337ab7;
    border-color: #2e6da4;
}

.btn-primary {
    background-color: #a5b8da;
    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #a5b8da), color-stop(100%, #7089b3));
    background-image: -webkit-linear-gradient(top, #a5b8da, #7089b3);
    background-image: -moz-linear-gradient(top, #a5b8da, #7089b3);
    background-image: -ms-linear-gradient(top, #a5b8da, #7089b3);
    background-image: -o-linear-gradient(top, #a5b8da, #7089b3);
    background-image: linear-gradient(top, #a5b8da, #7089b3);
    border-top: 1px solid #758fba;
    border-right: 1px solid #6c84ab;
    border-bottom: 1px solid #5c6f91;
    border-left: 1px solid #6c84ab;
    -webkit-box-shadow: inset 0 1px 0 0 #aec3e5;
    box-shadow: inset 0 1px 0 0 #aec3e5;
    color: #fff;
    text-align: center;
    text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.45);
}

#login_page .panel-body {
    position: relative;
    padding: 10px 12px;
}

#login_page .cover {
    position: absolute;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    z-index: 100;
    background: rgb(240,240,240);
    background: rgba(240,240,240,0.8);
    border: 1px solid rgba(150,150,150,0.2);
}

#login_page .spinner {
    margin: 55px auto 0px;
    width: 150px;
}
</style>

<script src="/Ilex/javascript/jquery-1.11.0.min.js"></script>
<script src="/Ilex/javascript/cookie_management.js"></script>
<script>
    var fail_msg = '${CnsLoginForm.failmessage}';
    var logout = '<%=request.getParameter("logout")%>';
    
    $(document).ready(function() {
        $('#login-form').submit(function() {
            //e.preventDefualt();
            
            var pass = document.getElementById("passwordbox").value;

            pass = encodeURIComponent(pass);
            document.getElementById("passwordbox").value = pass;
            
            $('#logging_in').show();
        });
    
        if (logout == 'true' || fail_msg === 'Empty Response') {
            $('#error_msg').hide();
            deleteCookie('cns-user_data');
            deleteCookie('cns-token');
            $('#input_user_id').val('');
            $('#input_token').val('');
            $('#logging_in').hide();
        } else if (typeof fail_msg === 'undefined' || fail_msg === '') {
            // Check token for single sign on.
            var user_data = getCookie('cns-user_data');
            var token = getCookie('cns-token');

            if (token !== null && token !== '' && user_data !== null && user_data !== '') {
                try {
                    user_data = JSON.parse(user_data);
                    if (typeof user_data.id !== 'undefined' && !isNaN(user_data.id)) {
                        $('#input_user_id').val(user_data.id);
                        $('#input_token').val(token);
                        $('#login-form').submit();
                    } else {
                        $('#logging_in').hide();
                    }
                } catch (err) {
                    $('#logging_in').hide();
                }
            } else {
                $('#logging_in').hide();
            }
        } else {
            deleteCookie('cns-user_data');
            deleteCookie('cns-token');
            $('#input_user_id').val('');
            $('#input_token').val('');
            $('#logging_in').hide();
        }
    });
</script>

</head>
<body id="login_page" style="padding: 0px; margin: 0px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tableIId">
        <tr style="background-color: white;">
            <td><img src="images/medius_logo.jpg" alt="" align="left" /></td>
            <td align="right" class="top_right_header">
                <span id="submittername">ilex</span>
                <span class="top_right_header_sep">Login</span>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="ilex-menu" style="float: none">&nbsp;</td>
        </tr>
    </table>
    <article>
        <div class="panel panel-default">
            <div class="panel-body">
                <div id="logging_in" class="cover">
                    <img src="/Ilex/images/ajax_loader2.gif" class="spinner">
                </div>
                <div style="text-align: center;">
                    <img src="/img/logo_contingent.png" style="width: 300px; margin-bottom: 30px;">
                </div>
                <div class="col-xs-12">
                    <h4>This area is restricted. Please login to continue.</h4>
                    <div class="form-wrap">
                        <html:form action="/Login.do" styleId="login-form" styleClass="form-horizontal">
                            <html:hidden property="userid" styleId="input_user_id" />
                            <html:hidden property="token" styleId="input_token" />
                            <div class="form-group form-group-sm">
                                <html:text property="username" styleClass="form-control" />
                            </div>
                            <div class="form-group form-group-sm">
                                <html:password property="password" styleClass="form-control" styleId="passwordbox" />
                            </div>
                            <html:submit styleId="btn-login" styleClass="btn btn-primary" value="Login"></html:submit>
                            <br />
                            <span id="error_msg" style="color: red;font-size: 12px;"><bean:write property="failmessage" name="CnsLoginForm" /></span>
                        </html:form>
                    </div>
                </div>
            </div>
        </div>
    </article>
</body>
</html>




