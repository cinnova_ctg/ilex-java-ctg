<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: Generating left frame
*
-->

<!DOCTYPE HTML>
<%@ page import = "com.mind.common.*" %>
<%@ page import = "com.mind.common.dao.*" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<bean:define id = "menustring" name = "menustring" scope = "request"/>
<bean:define id = "currState" name = "currState" scope = "request"/>
<bean:define id = "expansionState" name = "expansionState" scope = "request"/>

<bean:define id = "element" name = "element" scope = "request"/>
<bean:define id = "controller" name = "controller" scope = "request"/>

<html:html>


<head>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

<title>Hierarchy Navigation</title>
<style>
    body  {
    	font-family : "Verdana";
    	font-size : 10px;
    	cursor : auto;
    	color : #000000;
    	padding : 0px 0px 0px 0px;
    	white-space : nowrap;
    }

    /* Link Styles */
    a  {
    	color : #000000;
    }

    a:link  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:active  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:hover  {
    	text-decoration : underline;
    	color : #000000;
    }

    a:visited  {
    	text-decoration : underline;
    	color : #000000;
    }
    
    .m2 {
        font-family : "Verdana";
        font-size : 10px;
        line-height: 10px;
    }    
</style>

<script language="JavaScript" src="javascript/ce_menu.js" type="text/javascript"></script>
</head>

<body onload="MM_preloadImages('images/t_msa.gif','images/t_appendix.gif','images/t_job.gif');"  text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/sidebg.jpg">
<%
MenuElement me = ((MenuTree)(element)).getRootElement();
MenuController mc =((MenuTree)(element)).getMenuController();
me.drawDirect(0,mc,response.getWriter());
%>
<script language="JavaScript">

document.getElementById('line0').className = 'm2';
document.getElementById('line0').style.visibility = 'visible';


currState="<%=((MenuTree)(element)).getCurrState()%>";
expansionState="<%=expansionState%>";
initNavigatorServer('UM');
</script>
</body>

</html:html>