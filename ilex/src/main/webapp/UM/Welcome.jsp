<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: Frame set for user manager having two frames, named ilexleft, ilexmain
*
-->

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<html:html>

<head>
<title>ILEX</title>
<meta name = "GENERATOR" content = "Microsoft FrontPage 4.0">
</head>



<frameset framespacing = "0" border = "false" frameborder = "0" cols = "213,*">
  <frame name = "ilexleft" scrolling = "yes"  src = "../MenuTreeAction.do?menucriteria=UM"  marginwidth = "0" marginheight = "0" STYLE="border-right: 1px outset #1D3F71">
<frameset framespacing="0" border="false" frameborder="0" rows="*,20">
	<frame name="ilexmain" id="ilexmain" scrolling="yes"  src="Home.jsp"
	 marginwidth="0" marginheight="0">
	<frame name="ilexfooter" scrolling="no"  src="../Footerframe.jsp"
	 marginwidth="0" marginheight="0">
</frameset>   
    <noframes>
  <body>
  <p>This page uses frames, but your browser doesn't support them.</p>
  </body>
  </noframes>
</frameset>
</frameset>
</html:html>
