<%@page import="java.sql.ResultSet"%>
<%@page import="com.mind.formbean.MasterSite.MasterSiteInformationBean"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<%@ include  file="/Header.inc" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Master Site</title>
 <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<link href="MasterSite/masterSite-style.css" type="text/css"
	rel="stylesheet" media="all" />
	
	

<script src="MasterSite/typeahead.js" type="text/javascript"></script>




<style type="text/css">
.headStyling {
	font-size: 11px;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	/*cursor: pointer;
	cursor: hand;*/
	background-color: #b5c8d9; !
	important padding-right: 15px;
	text-align: left;
	padding-top: 5px;
	padding-left: 5px;
	padding-bottom: 5px;
}

.tt-dataset-typeahead{
	background-color: white;
}    

      .typeahead-wrapper {
        display: block;
        
      }

	
      .tt-dropdown-menu {
        background-color: #E8EEF7;
        border: 1px solid #000;
        font-size: 12px;
        font-family: Verdana, Arial, Helvetica, sans-serif;
        /*width: 145px;*/
        
      }

      .tt-suggestion.tt-is-under-cursor {
        background-color: #B5C8D9;
         
      }
    
      .triggered-events {
        float: right;
        width: 500px;
        height: 300px;
      }
      
      
      
      
    </style>





<% 
ArrayList<String[]> resourceLst = new ArrayList<String[]>();
ArrayList<String[]> reasonLst = new ArrayList<String[]>();
/*ArrayList<String> costLst = new ArrayList<String>();
ArrayList<String> avgTimeLst = new ArrayList<String>();
ArrayList<String> compositeOwnerLst = new ArrayList<String>();*/

if(request.getAttribute("ResourcesChartDate")!=null){
	resourceLst =  (ArrayList<String[]>) request.getAttribute("ResourcesChartDate");		
	reasonLst =  (ArrayList<String[]>) request.getAttribute("ReasonsChartDate");
	/*costLst =  (ArrayList<String>) request.getAttribute("costChartDate");		
	avgTimeLst =  (ArrayList<String>) request.getAttribute("AverageTimeTechOnSiteChartDate");
	compositeOwnerLst =  (ArrayList<String>) request.getAttribute("CompositeOwnerChartDate");*/
}

%>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">



function openPopupWindow(url, name){
	window.open(url, '_blank', 'width=1100,height=700,scrollbars=yes');
}


	google.load("visualization", "1", {
		packages : [ "corechart" ]
	});
	
	google.setOnLoadCallback(drawAllChart);
	
	function drawAllChart(){
		/*drawResourcesChart();
		drawReasonsChart();
		drawCostChart();
		avgTimeChart();
		compositeOwnerChart();*/
	}
	
	function drawResourcesChart() {
		var dataArr =
			[ 
				[ 'Type', 'Sum' ]
			 <% for(int i=0;i<resourceLst.size();i++){
				 String[] st = resourceLst.get(i);
				%>
				,['<%= st[0]%>', <%=st[1]%> ]
				<%
				 
				 
			 }%>
			  ]; 
		
		var data = google.visualization.arrayToDataTable(dataArr);

		var options = {
			title : 'Resources (Site Specific)',
			pieHole : 0.7,
			chartArea: {left:25,top:40,width:"250px",height:"250px"},			
			legend: {position: 'bottom', alignment: 'start', textStyle: {color: 'Black', fontSize: 12, width: 100, height: 100}},
			colors: ["#009900", "#3d8da9", "#a2b5e2", "#cc3399", "#6e1e4e", "#410082", "#6699ff", "#A18F50", "#AFBC9C", "#C3EA7A"],
			pieSliceText: 'none',

			//legend.alignment: 'start'
			//legend.position: 'bottom',
		};

		var chart = new google.visualization.PieChart(document
				.getElementById('ResourcesChar'));
		chart.draw(data, options);
	}
	
	function drawReasonsChart() {
		var dataArr =
			[ 
				[ 'Description', 'Count' ]
			 <% for(int i=0;i<reasonLst.size();i++){
				 String[] st = reasonLst.get(i);
				%>
				,['<%= st[0]%>', <%=st[1]%> ]
				<%
				 
				 
			 }%>
			  ]; 
		
		var data = google.visualization.arrayToDataTable(dataArr);

		var options = {
			title : 'Reasons (Site Specific)',
			pieHole : 0.7,
			chartArea: {left:25,top:40,width:"250px",height:"250px"},			
			legend: {position: 'bottom', alignment: 'start', textStyle: {color: 'Black', fontSize: 12, width: 100, height: 100}},
			colors: ["#009900", "#3d8da9", "#a2b5e2", "#cc3399", "#6e1e4e", "#410082", "#6699ff", "#A18F50", "#AFBC9C", "#C3EA7A"],
			pieSliceText: 'none',

			//legend.alignment: 'start'
			//legend.position: 'bottom',
		};

		var chart = new google.visualization.PieChart(document
				.getElementById('ReasonsChart'));
		chart.draw(data, options);
	}
	
	function drawCostChart() {
        var data = google.visualization.arrayToDataTable([
          ['Weeks', 'Cost']
<%--           <% //for(int i=0;i<costLst.size();i++){				  --%>
<%-- 				%> --%>
<%-- 				,[<%=// costLst.get(i).toString()%>] --%>
<%-- 				<% --%>
				 
				 
<%-- 			// }%> --%>
        ]);

        var options = {
          title: 'Cost',
          hAxis: {title: 'Weeks'},
          vAxis: {count: 4},
          pointSize:5,
        };
<%--         <% //if(costLst.size()>0){ %> --%>
//         var chart = new google.visualization.LineChart(document.getElementById('cost_chart_div'));
//         chart.draw(data, options);
<%--         <%//}%> --%>
      }
	
	function avgTimeChart() {
        var data = google.visualization.arrayToDataTable([
          ['Weeks', 'Hours']
<%--           <% //for(int i=0;i<avgTimeLst.size();i++){ --%>
<%-- 				%> --%>
<%-- 				,[<%= //avgTimeLst.get(i)%>] --%>
<%-- 				<% --%>
				 
				 
<%-- 			// }%> --%>
        ]);

        var options = {
          title: 'Avg Time Tech on-Site',
          hAxis: {title: 'Weeks'},
          vAxis: {count: 4},
          pointSize:5,
        };
<%--         <% //if(avgTimeLst.size()>0){ %> --%>
//         var chart = new google.visualization.LineChart(document.getElementById('avg_time_chart_div'));
//         chart.draw(data, options);
<%--         <%//}%> --%>
      }


	
	function compositeOwnerChart() {
        var data = google.visualization.arrayToDataTable([
          ['Weeks', 'Client', 'Contingent', 'LEC', 'Other3rdParty']
<%--           <% //for(int i=0;i<compositeOwnerLst.size();i++){%> --%>
<%--           ,[<%=//compositeOwnerLst.get(i).toString()%>] --%>
 				
<%-- 			<%//}%> --%>
<%--           ,[<%="'1', 07, 13, 00, 00"%>] --%>
//           ,['2', 08, 14, 00, 00]
//           ,['3', 09, 15, 00, 00]
//           ,['4', 10, 16, 00, 00]
//           ,['5', 11, 17, 00, 00]
//           ,['6', 12, 18, 00, 00]
        ]);

        var options = {
          title: 'Composite Owner By Month| By Weeks',
          hAxis: {title: 'Weeks'},
          vAxis: {title: 'Events', count: 4},
          pointSize:5,
          legend: {position: 'bottom', alignment: 'start'},
          colors: ["#33339F", "#FF00FF", "#FFCC00", "#76EE00"],
        };
<%--         <% //if(compositeOwnerLst.size()>0){ %> --%>
//         var chart = new google.visualization.LineChart(document.getElementById('composit_owner_chart_div'));
//         chart.draw(data, options);
<%--         <%//}%> --%>
      }
	
	
// 	function openTicketSummary(ticketId) {

// 		var newWindow1 = window.open('','','width=1250,height=820,scrollbars=yes');
		
		
// 		$.ajax({
// 			type : 'GET',
// 			data : {"ticketId" : ticketId},
// 			url :  '../Ilex-WS/hdMainTable/hdTicketSummary.html',
// 			async : false,
// 			success : function(data) {

				
// 				newWindow1.document.write(data);
// 				newWindow1.document.close();

			

// 			}
// 		});
// 	}
		
	function populateSites(mmId){
		
		window.open("masterSiteAction.do?mmId="+mmId, "_self");
		
	}
	
	
	function populateDtl(siteName, mmId){
		siteName = siteName.replace('#', '-,-,-');
		window.open("masterSiteAction.do?siteName="+siteName+"&mmId="+mmId, "_self");
		
	}
	
	function advancedSearchPopup(){
		var newwindow = window.open("masterSiteAction.do?function=advancedSiteSearch", '_blank', 'width=1100,height=700,scrollbars=yes');
		if (window.focus) {newwindow.focus()}
	}
	
	
</script>

<style>

	 ul li {list-style: none; cursor: pointer;}
    li.smart_autocomplete_highlight {background-color: #C1CE84;}
    ul { margin: 10px 0; padding: 5px; background-color: #E3EBBC; }
</style>

</head>
<body>



	<div style="width: 1088px; margin: 5px;height:30px;">
		<html:form action="masterSiteAction.do" method="post" style="float:left;margin-right:11px;">
			<!-- onchange="appendData();" -->
			<div class="twitter-typeahead">

				<html:text property="siteName" style="width: 285px;"
					styleId="siteName"></html:text>
				<select name="searchType" id="searchType" style="width: 85px;">
					<option value="site">Site#</option>
					<option value="address">Address</option>
				</select>
				<input type="submit" id="searchBtn" value="Search Site"
					class="FMBUT0006"  style="width: 100px;" />
			</div>


		</html:form>

<input type="button" id="advSearchBtn" value="Advanced Search" onclick="advancedSearchPopup();"
					class="FMBUT0006"  style="width: 125px;"/>

		<html:form action="masterSiteAction.do" method="post"  style="float:right;">
			<html:select style="width: 200px;float:right;margin-left: 5px;" property="siteName" onchange="populateDtl(this.value, ${MasterSiteForm.mmId});">
				<option value="">Select Site</option>
				<html:optionsCollection name="MasterSiteForm"
					property="siteMasterSitesList" label="label" value="label" />
			</html:select>
			<html:select style="width: 200px;float:right;margin-right: 5px;" property="mmId"
				onchange="populateSites(this.value);">
				<option value="">Select Client</option>
				<html:optionsCollection name="MasterSiteForm"
					property="siteMasterClientList" label="label" value="value" />
				<%-- 				<c:forEach items="${MasterSiteForm.clientsList}" var="clientsList"> --%>
				<%-- 							<option value="${clientsList.value}">${clientsList.label}</option> --%>
				<%-- 				</c:forEach> --%>
			</html:select>
		</html:form>


	</div>
	<div style="width: 1700px; margin: 5px;">
		<div style="width: 500px; float: left;">

			<div class="tableWrapper " style="">
				<div style="width: 445px; margin: 0px;" class="containerTitle">
					<div style="padding: 4px; width: 250px; margin: 0px;"
						class="divFloatLeft">Site Details</div>
				</div>
				
				<table class="container display dataTable" border="0" width="480px"
				<thead>
							<tr>
								<th class="headStyling" colspan="2" style="height: 13px;"><c:out
										value="${MasterSiteForm.siteNameLabel}" /></th>
							</tr>
						</thead>
				
				</table>
				
				<div class="dataTables_wrapper" style="overflow-y: auto; width: 480px;">
					<table class="container display dataTable" border="0" width="480px"
						cellpadding="0" cellspacing="1" align="left">
<!-- 						<thead> -->
<!-- 							<tr> -->
<%-- 								<th class="headStyling" colspan="2" style="height: 13px;"><c:out --%>
<%-- 										value="${MasterSiteForm.siteName}" /></th> --%>
<!-- 							</tr> -->
<!-- 						</thead> -->
						<tbody>
							<tr>
								<td valign="middle">Address</td>
								<td valign="middle" style="word-break: keep-all;"><c:out
										value="${MasterSiteForm.siteAddress}" /></td>
							</tr>
							<tr>
								<td valign="middle">Phone Number</td>
								<td valign="middle"><c:out
										value="${MasterSiteForm.phoneNumber}" /></td>
							</tr>
							<tr>
								<td valign="middle">Name of Primary Contact</td>
								<td valign="middle"><c:out
										value="${MasterSiteForm.primaryContactName}" /></td>
							</tr>
							<tr>
								<td valign="middle">Email of Primary Contact</td>
								<td valign="middle"><c:out
										value="${MasterSiteForm.primaryContactEmail}" /></td>
							</tr>

						</tbody>
					</table>
				</div>
			</div>




			<div class="tableWrapper" style="">
				<div style="width: 445px; margin: 0px;" class="containerTitle">
					<div style="padding: 4px; width: 250px; margin: 0px;"
						class="divFloatLeft">Help Desk Tickets</div>
				</div>
				
				<table class="container display dataTable" border="0" width="480px"
						cellpadding="0" cellspacing="1" align="left">
				<thead>
							<tr>

								<th style="width:70px;" class="headStyling">Ticket</th>
								<th style="width:100px;" class="headStyling">Resolution</th>
								<th style="width:55px;" class="headStyling">Date</th>
								<th style="width:35px;" class="headStyling">Install Notes</th> 
								<th style="width:60px;" class="headStyling">Deliverables</th>

							</tr>
						</thead>
				
				</table>
			
				
				<div class="dataTables_wrapper" style="overflow-y: auto; width: 480px;  ">
					<table class="container display dataTable" border="0" width="480px"
						cellpadding="0" cellspacing="1" align="left">
						<tbody>
							<c:forEach items="${MasterSiteForm.helpDeskDetailsList}"
								var="bean">
								<tr>
									<td style="width:70px;word-break: keep-all;"><a
									href="javascript:void(0)" onclick="openPopupWindow('JobDashboardAction.do?appendix_Id=<c:out value="${bean.appendixId}" />&Job_Id=<c:out value="${bean.jobId}" />', 'Name3')">
									<c:out value="${bean.ticketName}" />
									</a></td>
									<td style="width:100px; word-break: keep-all; word-break: break-word;"><c:out value="${bean.resolutionDesc}" /></td>
									<td style="width:55px;"><c:out value="${bean.createdDateString}" /></td>
									<td style="width:35px;">
									<c:if test="${bean.installNoteCount eq '0'}" >
									n/a
									</c:if>
									<c:if test="${bean.installNoteCount ne '0'}" >
									<a href="javascript:void(0)" onclick="openPopupWindow('ViewInstallationNotes.do?function=view&from=jobdashboard&from=Jobdashboard&jobid=<c:out value="${bean.jobId}" />&appendixid=<c:out value="${bean.appendixId}" />', 'Name1')"
										> <c:out value="${bean.installNoteCount}" />
									</a>
									</c:if>
									</td>
									<td style="width:60px;">
<%-- 									<c:if test="${bean.deliverableCount eq '0'}" > --%>
<!-- 									n/a -->
<%-- 									</c:if> --%>
<%-- 									<c:if test="${bean.deliverableCount ne '0'}" > --%>
<%-- 									<a href="javascript:void(0)" onclick="openPopupWindow('POAction.do?hmode=unspecified&poId=<c:out value="${bean.poWoId}" />&tabId=3&mode=view&appendix_Id=<c:out value="${bean.appendixId}" />&jobId=<c:out value="${bean.jobId}" />', 'Name2')" --%>
<!-- 										> -->
										<c:out value="${bean.deliverableCount}" />
<!-- 									</a> -->
<%-- 									</c:if> --%>
									</td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
				</div>
			</div>


			<div class="tableWrapper" style="">
<!-- 			<div class="tableWrapper" style="height: 415px;"> -->
				<div style="width: 445px; margin: 0px;" class="containerTitle">
					<div style="padding: 4px; width: 250px; margin: 0px;"
						class="divFloatLeft">Jobs</div>
				</div>
				
				
				
				
				<table class="container display dataTable" border="0" width="480px"
						cellpadding="0" cellspacing="1" align="left">
				<thead>
							<tr>
								<th class="headStyling" style="width:120px;">Job</th>
								<th class="headStyling" style="width:50px;">Date</th>
								<th class="headStyling" style="width:60px;">Install Notes</th>
								<th class="headStyling" style="width:60px;">Deliverables</th>
							</tr>
						</thead>
				
				</table>
				
				
				
				
				
<!-- 				<div class="dataTables_wrapper"> -->
				<div class="dataTables_wrapper" style="overflow-y: auto; width: 480px;  ">
				<table class="container display dataTable" border="0" width="480px"
						cellpadding="0" cellspacing="1" align="left">
						<tbody>
							<c:forEach items="${MasterSiteForm.jobDetailsList}" var="bean">
								<tr>
									<td style="width:120px;word-break: keep-all;"><a
									href="javascript:void(0)" onclick="openPopupWindow('JobDashboardAction.do?appendix_Id=<c:out value="${bean.appendixId}" />&Job_Id=<c:out value="${bean.jobId}" />', 'Name3')">
									<c:out value="${bean.ticketName}" />
									</a></td>
									<td style="width:50px;"><c:out value="${bean.createdDateString}" /></td>
									<td style="width:60px;">
									<c:if test="${bean.installNoteCount eq '0'}" >
									n/a
									</c:if>
									<c:if test="${bean.installNoteCount ne '0'}" >
									<a
									href="javascript:void(0)" onclick="openPopupWindow('ViewInstallationNotes.do?function=view&from=jobdashboard&from=Jobdashboard&jobid=<c:out value="${bean.jobId}" />&appendixid=<c:out value="${bean.appendixId}" />', 'Name4')" >
									<c:out value="${bean.installNoteCount}" />
									</a>
									</c:if>
									</td>
									<td style="width:60px;">
<%-- 										<c:if test="${bean.deliverableCount eq '0'}" > --%>
<!-- 									n/a -->
<%-- 									</c:if> --%>
<%-- 									<c:if test="${bean.deliverableCount ne '0'}" > --%>
<!-- 										<a -->
<%-- 										href="javascript:void(0)" onclick="openPopupWindow('POAction.do?hmode=unspecified&poId=<c:out value="${bean.poWoId}" />&tabId=3&mode=view&appendix_Id=<c:out value="${bean.appendixId}" />&jobId=<c:out value="${bean.jobId}" />', 'Name5')" --%>
<!-- 										> -->
										<c:out value="${bean.deliverableCount}" />
<%-- 									</a></c:if> --%>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>


		<div style="width: 600px; float: left;">


			<div class="tableWrapper">
				<div style="width: 445px; margin: 0px;" class="containerTitle">
					<div style="padding: 4px; width: 250px; margin: 0px;"
						class="divFloatLeft">Deliverables</div>
				</div>
				<table class="container display dataTable" border="0" width="585px"
						cellpadding="0" cellspacing="1" align="left">
				<thead>
							<tr>
								<th class="headStyling" style="width:50px;">Date</th>
								<th class="headStyling" style="width:120px;">Job/Ticket</th>
								<th nowrap="nowrap" class="headStyling" style="width:40px;">PO/WO</th>
								<th class="headStyling" style="width:85px;">Deliverable</th>
							</tr>
						</thead>
				
				</table>
				<div class="dataTables_wrapper" style="overflow-y: auto; width: 585px;">
					<table class="container display dataTable" border="0" width="585px"
						cellpadding="0" cellspacing="1" align="left">
						<tbody>
							<c:forEach items="${MasterSiteForm.deliverableDetailsList}"
								var="bean">
								<tr>
								<td style="width:50px;"><c:out value="${bean.createdDateString}" /></td>
								<td style="width:120px;word-break: keep-all;"><a
									href="javascript:void(0)" onclick="openPopupWindow('JobDashboardAction.do?appendix_Id=<c:out value="${bean.appendixId}" />&Job_Id=<c:out value="${bean.jobId}" />', 'Name3')">
									<c:out value="${bean.ticketName}" />
									</a></td>
									<td style="width:40px;"><a
									href="javascript:void(0)" onclick="openPopupWindow('POAction.do?hmode=unspecified&poId=<c:out value="${bean.poWoId}" />&tabId=3&mode=view&appendix_Id=<c:out value="${bean.appendixId}" />&jobId=<c:out value="${bean.jobId}" />', 'Name6')"
										><c:out value="${bean.poWoId}" /></a></td>
									<td style="width:85px;word-break: keep-all;"><a
									href="javascript:void(0)" onclick="openPopupWindow('POAction.do?hmode=unspecified&poId=<c:out value="${bean.poWoId}" />&tabId=3&mode=view&appendix_Id=<c:out value="${bean.appendixId}" />&jobId=<c:out value="${bean.jobId}" />', 'Name7')"
										><c:out value="${bean.deliverableDesc}" /></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>



			<div class="tableWrapper" style="">
				<div style="width: 445px; margin: 0px;" class="containerTitle">
					<div style="padding: 4px; width: 250px; margin: 0px;"
						class="divFloatLeft">Assets Deployed to Site/Support</div>
				</div>
				
				<table class="container display dataTable" border="0" width="585px"
						cellpadding="0" cellspacing="1" align="left">
				<thead>
							<tr>
								<th class="headStyling" style="width:50px;">Date</th>
								<th class="headStyling" style="width:120px;">CL</th>
								<th class="headStyling" style="width:40px;">Quantity</th>
								<th class="headStyling" style="width:85px;">Serial Number</th>								
							</tr>
						</thead>
				
				

				
				
				</table>
				
				
				
				</table>
				<div class="dataTables_wrapper" style="overflow-y: auto; width: 585px;  ">
					<table class="container display dataTable" border="0" width="585px"
						cellpadding="0" cellspacing="1" align="left">
						<tbody>
							<c:forEach items="${MasterSiteForm.assetsDeployedList}"
								var="bean">
								<tr>
									<td style="width:50px;"><c:out value="${bean[0]}" /></td>
									<td style="width:120px;word-break: keep-all;"><c:out value="${bean[1]}" /></td>
									<td style="width:40px;"><c:out value="${bean[3]}" /></td>
									<td style="width:85px;word-break: keep-all;"><c:out value="${bean[2]}" /></td>									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>


<!-- 			<div id="ResourcesChar" style="width: 295px; height: 360px; float: left;"></div> -->
<!-- 			<div id="ReasonsChart"  style="width: 295px; height: 360px; float: left;"></div> -->

			<div class="tableWrapper">
				<div style="width: 445px; margin: 0px;" class="containerTitle">
					<div style="padding: 4px; width: 250px; margin: 0px;"
						class="divFloatLeft">Device Attribute</div>
				</div>

				<table class="container display dataTable" border="0" width="585px"
					<thead>
							<tr>
								<th class="headStyling" style="height: 13px;width: 30%;">Attribute</th>
								<th class="headStyling" style="height: 13px;">Value</th>
							</tr>
						</thead></table>

<div class="dataTables_wrapper"
					style="overflow-y: auto; width: 585px;padding-bottom: 5px; max-height: 291px; height: 291px;">
<c:forEach items="${MasterSiteForm.masterSiteDeviceAttributelst}"
								var="bean">
				<div style="overflow-y: auto; width: 585px;padding-bottom: 5px;" >
					<table class="container display dataTable" border="0" width="585px"
						cellpadding="0" cellspacing="1" align="left" >
						<tbody>
						<tr>
								<td valign="middle" style="width: 30%;">Device Name</td>
								<td valign="middle" style="word-break: keep-all;"><c:out
										value="${bean.siteNumber}" /></td>
							</tr>
							<tr>
								<td valign="middle" style="width: 30%;">Circuit Type</td>
								<td valign="middle" style="word-break: keep-all;"><c:out
										value="${bean.circuitType}" /></td>
							</tr>
							<tr>
								<td valign="middle" style="width: 30%;">Modem Type</td>
								<td valign="middle" style="word-break: keep-all;"><c:out
										value="${bean.modemType}" /></td>
							</tr>
							<tr>
								<td valign="middle" style="width: 30%;">Internet Service Line #</td>
								<td valign="middle" style="word-break: keep-all;"><c:out
										value="${bean.internetLineNo}" /></td>
							</tr>
							<tr>
								<td valign="middle" style="width: 30%;">ISP</td>
								<td valign="middle" style="word-break: keep-all;"><c:out
										value="${bean.isp}" /></td>
							</tr>
							<tr>
								<td valign="middle" style="width: 30%;">ISP Support #</td>
								<td valign="middle" style="word-break: keep-all;"><c:out
										value="${bean.ispSupport}" /></td>
							</tr>
							<tr>
								<td valign="middle" style="width: 30%;">Circuit IP</td>
								<td valign="middle" style="word-break: keep-all;"><c:out
										value="${bean.circuitIP}" /></td>
							</tr>
							<tr>
								<td valign="middle" style="width: 30%;">Site Contact #</td>
								<td valign="middle" style="word-break: keep-all;"><c:out
										value="${bean.siteContactNo}" /></td>
							</tr>
							<tr>
								<td valign="middle" style="width: 30%;">Location</td>
								<td valign="middle" style="word-break: keep-all;"><c:out
										value="${bean.location}" /></td>
							</tr>
							<tr>
								<td valign="middle" style="width: 30%;">Account</td>
								<td valign="middle" style="word-break: keep-all;"><c:out
										value="${bean.account}" /></td>
							</tr>

						</tbody>
					</table>
				</div>
				
				</c:forEach>
				</div>
				
				
			</div>

		</div>


		<div style="width: 600px; float: left;">

			<div class="tableWrapper" style="">
				<div style="width: 445px; margin: 0px;" class="containerTitle">
					<div style="padding: 4px; width: 250px; margin: 0px;"
						class="divFloatLeft">KPIs</div>
				</div>
				<table class="container display dataTable" border="0" width="585px"
						cellpadding="0" cellspacing="1" align="left">
				<thead>
							<tr>
								<th class="headStyling" style="width: 250px;">NetMedX</th>
								<th class="headStyling">7 Days</th>
								<th class="headStyling">30 Days</th>
								<th class="headStyling">60 Days</th>
							</tr>
						</thead>
				</table>
				
				
				
				<div class="dataTables_wrapper" style="overflow-y: auto; width: 585px;  ">
					<table class="container display dataTable" border="0" width="585px"
						cellpadding="0" cellspacing="1" align="left">
<!-- 						<thead> -->
<!-- 							<tr> -->
<!-- 								<th class="headStyling" style="width: 250px;">NetMedX</th> -->
<!-- 								<th class="headStyling">7 Days</th> -->
<!-- 								<th class="headStyling">30 Days</th> -->
<!-- 								<th class="headStyling">60 Days</th> -->
<!-- 							</tr> -->
<!-- 						</thead> -->
						<tbody>
							<c:forEach items="${MasterSiteForm.kIPDtlList}" var="bean">
								<tr>
									<td style="width: 250px;"><c:out value="${bean[0]}" /></td>
									<td><c:out value="${bean[1]}" /></td>
									<td><c:out value="${bean[2]}" /></td>
									<td><c:out value="${bean[3]}" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>


		  	<div id="dev_img0"
				style="width: 590px; height: 250px; margin: 3px 0px;">
				</div>
			<div id="dev_img1"
				style="width: 590px; height: 250px; margin: 3px 0px;">
				</div>
				
			<div id="dev_img2"
				style="width: 590px; height: 250px; margin: 3px 0px;">
				</div>
		<!--  	<div id="cost_chart_div"
				style="width: 590px; height: 250px; margin: 3px 0px;"></div>
			<div id="avg_time_chart_div"
				style="width: 590px; height: 250px; margin: 3px 0px;"></div>
			<div id="composit_owner_chart_div"
				style="width: 590px; height: 250px; margin: 3px 0px;"></div>-->

		</div>



	</div>
</body>
<script type="text/javascript">


function replaceSearchVal(){
	var vvalue = $('#siteName').val();
	alert(vvalue);
	vvalue =$('#siteName').val("Testing");
	alert("----22---"+vvalue);
}


$(document).ready(function () {
	
	
	
// 	$('#siteName').typeahead({ 
		
// 		  name: 'siteName',                                                          
// 		  remote: 'masterSiteAction.do?function=updatePOByAjax&siteName=%QUERY',                                         
// 		  limit: 10                                                                   
// 		});
	
	
	
	
	$('#siteName').typeahead({
		  name: 'typeahead',
		  remote: {
		    url: 'masterSiteAction.do?function=updatePOByAjax',
		    replace: function(url, uriEncodedQuery) {
		      var value0 = $('#searchType').val();
		      var value1 = $('#siteName').val();
		      url = url + '&siteName='+encodeURIComponent(value1)+'&searchType=' + encodeURIComponent(value0);
		      
		      return url;
		      }
		    },
		    limit: 10   
		});
	
	
	
	
	

		var isOpera = !!(window.opera && window.opera.version);  // Opera 8.0+
	var isFirefox = testCSS('MozBoxSizing');                 // FF 0.8+
	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
	    // At least Safari 3+: "[object HTMLElementConstructor]"
	var isChrome = !isSafari && testCSS('WebkitTransform');  // Chrome 1+
	var isIE = /*@cc_on!@*/false || testCSS('msTransform');  // At least IE6

	function testCSS(prop) {
	    return prop in document.documentElement.style;
	}
	
	
	$(".twitter-typeahead").css("display","inline");
	$(".tt-hint").remove();
	
	
	if (isIE) {
	$("#searchBtn").css("height","21px");
	//$("#searchBtn").css("width","90px");
	//$(".tt-dropdown-menu").css("top","6px");
	//$(".tt-dropdown-menu").css("width","140px");
	}else 
		{
		
		//$("#searchBtn").css("width","100px");
		//$(".tt-dropdown-menu").css("width","155px");
	$(".tt-dropdown-menu").css("top","22px");
		}
	$(".tt-dropdown-menu").css("right","2px");
	$(".tt-dropdown-menu").css("left","2px");
	
	$(".tt-suggestion").css("word-break","break-all");
	$(".tt-dropdown-menu").css("max-height","200px");
	$(".tt-dropdown-menu").css("overflow-y","auto");
	
	
	//$("#query").smartAutoComplete({source: "masterSiteAction.do?function=updatePOByAjax", maxResults: 20 });
	
<% if(request.getAttribute("ImgId0")!=null){
	%>
	$("#dev_img0").html("<img src='DocumentMaster.do?hmode=viewResizedFile&fileId=&docId=<%=request.getAttribute("ImgId0") %>' alt='Image'>")
	<%
}%>

<% if(request.getAttribute("ImgId1")!=null){
	%>
	$("#dev_img1").html("<img src='DocumentMaster.do?hmode=viewResizedFile&fileId=&docId=<%=request.getAttribute("ImgId1") %>' alt='Image'>")
	<%
}%>

<% if(request.getAttribute("ImgId2")!=null){
	%>
	$("#dev_img2").html("<img src='DocumentMaster.do?hmode=viewResizedFile&fileId=&docId=<%=request.getAttribute("ImgId2") %>' alt='Image'>")
	<%
}%>




});
</script>
</html>