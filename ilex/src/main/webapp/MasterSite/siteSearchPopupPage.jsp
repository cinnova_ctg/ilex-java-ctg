<%@page import="java.sql.ResultSet"%>
<%@page import="com.mind.formbean.MasterSite.MasterSiteInformationBean"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/Header.inc"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Master Site</title>
<link href="MasterSite/masterSite-style.css" type="text/css"
	rel="stylesheet" media="all" />
<style type="text/css">
.textCSS {
	width: 197px;
	position: relative;
	vertical-align: top;
	background-color: transparent;
}

.headStyling {
	font-size: 11px;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	/*cursor: pointer;
	cursor: hand;*/
	background-color: #b5c8d9; !
	important padding-right: 15px;
	text-align: left;
	padding-top: 5px;
	padding-left: 5px;
	padding-bottom: 5px;
}
</style>
<script>
function refreshParentWindow(siteName, mmId){
	siteName = siteName.replace("#","-,-,-");
	
	window.opener.location.href="masterSiteAction.do?siteName="+siteName+"&mmId="+mmId
    self.close();
    
    
// 	window.opener.location = "masterSiteAction.do?siteName="+siteName+"&mmId="+mmId;
// 	window.close();
}
</script>
</head>
<body>
	<div class="containerTitle" style="margin: 10px 0px; height: 30px;">
		<div class="divFloatLeft"
			style="padding: 7px; font-size: 15px; margin: 0px;">Advanced
			Site Search</div>
	</div>


	<form action="masterSiteAction.do?function=advancedSiteSearch" style="margin-bottom: 15px;" 
		method="post" name="advSiteSearch">



		<table>
			<tr>
				<td>City:</td>
				<td><input type="text" placeholder="Site City" name="siteCity"
					id="siteCity" class="textCSS" style="margin: 0 15px 0 0;" /></td>
				<td>State:</td>
				<td><input type="text" placeholder="Site State"
					name="siteState" id="siteState" class="textCSS" style="margin: 0 15px 0 0;" /></td>
				<td>ZIP:</td>
				<td><input type="text" placeholder="Site ZIP" name="siteZip"
					id="siteZip" class="textCSS" style="margin: 0 15px 0 0;"/></td>

				<td>Address:</td>
				<td><input type="text" placeholder="Site Address"
					name="siteAdress" id="siteAdress" class="textCSS" style="margin: 0 15px 0 0;"/></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" name="advSearch" id="advSearch"
					value="Search" /></td>
			</tr>
		</table>
	</form>
	<div style="width: 100%;" class="tableWrapper">

		<table width="100%" cellspacing="1" cellpadding="0" border="0"
			align="left" class="container display dataTable">
			<thead>
				<tr>
					<th style="width: 150px;" class="headStyling">Site Name</th>
					<th style="width: 150px;" class="headStyling">Site Number</th>
					<th style="width: 100px;" class="headStyling">City</th>
					<th style="width: 80px;" class="headStyling">State</th>
					<th style="width: 80px;" class="headStyling">ZIP</th>
					<th style="width: 250px;" class="headStyling">Address</th>
					<th style="width: 150px;" class="headStyling">Client</th>
				</tr>
			</thead>
		</table>
		<div style="width: 100%; overflow: inherit;"
			class="dataTables_wrapper">
			<table width="100%" cellspacing="1" cellpadding="0" border="0"
				align="left" class="container display dataTable">
				<tbody>
					<c:forEach items="${MasterSiteForm.sitesList}" var="bean">
						<tr>
							<td style="width: 150px;"><a href="javaScript:void(0);"
								onclick="refreshParentWindow('<c:out value='${bean[3]}' />',<c:out value='${bean[1]}' />);"><c:out
										value="${bean[2]}" /></a></td>
							<td style="width: 150px;"><c:out value="${bean[3]}" /></td>
							<td style="width: 100px;"><c:out value="${bean[4]}" /></td>
							<td style="width: 80px;"><c:out value="${bean[5]}" /></td>
							<td style="width: 80px;"><c:out value="${bean[6]}" /></td>
							<td style="width: 250px;"><c:out value="${bean[7]}" /></td>
							<td style="width: 150px;"><c:out value="${bean[8]}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>