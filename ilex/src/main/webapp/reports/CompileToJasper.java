

import java.sql.Connection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperPrint;

public class CompileToJasper {

	public static void main(String[] args) {

		JasperPrint jasperPrint = new JasperPrint();
		Connection conn = null;
		Locale locale = new Locale("en","US");
		ResourceBundle res = ResourceBundle.getBundle("ilex", locale);
		Map inputParameters = new HashMap();
		inputParameters.put(JRParameter.REPORT_LOCALE,locale);
		inputParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE,res);
		conn = SqlConnection.getConnection();
		System.out.println("Datasource Connection" + conn + "");
		try {
			System.out.println("Compiling.....");
	//		JasperReport jasperReport = JasperCompileManager
		//			.compileReport("rpt/subrpt_ilex_table.jrxml");
	//		jasperPrint = JasperFillManager.fillReport(jasperReport,
		//			inputParameters, conn);
			
		JasperCompileManager.compileReportToFile("rpt/subrpt_type_cric_table.jrxml",			
				"rpt/subrpt_type_cric_table.jasper");
		
			// for a pdf
			System.out.println("Compiled.....");
		//	JasperExportManager.exportReportToPdfFile(jasperPrint,
			///		"test.pdf");
		//	JasperViewer.viewReport(jasperPrint,true);
			System.out.println("end of program....");
		} catch (Exception e) {
			System.out.println("" + e);
			e.printStackTrace();
		}
		finally{
			SqlConnection.closeConnection();
		}
	}
}
