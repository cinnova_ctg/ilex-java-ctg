<!-- 
* Copyright (C) 2005 MIND
* All rights reserved.
* The information contained here in is confidential and
* proprietary to MIND and forms the part of MIND
* Project	: ILEX
* Description	: For displaying partner summary.
*
-->
<!DOCTYPE HTML>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html:html>
<HEAD>
<%@ include  file="/Header.inc" %>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="styles/style.css" rel="stylesheet"	type="text/css">
<LINK href="styles/content.css" rel="stylesheet"	type="text/css">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script language="javascript" src="javascript/ilexGUI.js"></script>
<title>Assign Project</title>
</head>
<body>
<html:form action="/RawVendexViewAction">
	<table border = "0" width =580  >
		<tr>
        	<td  colspan ="3"  height="21">
			</td>
				
        </tr>
        <tr><td class = "labeleboldwhite" style ="padding-left: 15px;" >Raw Vendex View: Enable/Disable MSA</td></tr>
        <c:if test="${requestScope.Successfully eq '0'}">
        	<tr><td class = "message" style ="padding-left: 15px;" >Updated Successfully.</td></tr>
        </c:if>
	</table>
	<table><tr><td style="padding-left: 15px;" > 
		<table border="0" cellspacing="1" cellpadding="0" width="100%" >
			<% String tdColor = "" ;%>
			<logic:iterate id="projectlist" property="msaList" name ="RawVendexViewForm">
					
					<logic:equal name="projectlist" property="isMsa" value ="true" >
						<tr>
						<%
								if(tdColor.equals("Ntexto")) {
									tdColor = "Ntexte";
								} else {
									tdColor = "Ntexto";
								}
						
					 		%>
							<logic:equal name="projectlist" property="countendCustomer" value ="0" >
								<td class = "<%= tdColor %>" width = 1%>
											<html:multibox property="check" styleClass="smallSelect">
												<bean:write name="projectlist" property="msaId" filter="false"/>
											</html:multibox>&nbsp;&nbsp;
								</td>
							</logic:equal>
							<logic:notEqual name="projectlist" property="countendCustomer" value ="0">
								<td class = "<%= tdColor %>" width = 1%></td>
							</logic:notEqual>
							<td class = "<%= tdColor %>"><b><bean:write name = "projectlist" property="msaName"/></b></td>
								
						</tr>
					</logic:equal>
					<logic:notEqual name="projectlist" property="countendCustomer" value ="0">
							<%
								if(tdColor.equals("Ntexto")) {
									tdColor = "Ntexte";
								} else {
									tdColor = "Ntexto";
								}
						
					 		%>
						<tr>
							<td class = "<%= tdColor %>" width = 1%>
										<html:multibox property="check" styleClass="smallSelect">
											<bean:write name="projectlist" property="msaId" filter="false"/>
										</html:multibox>&nbsp;&nbsp;
							</td>
							<td class = "<%= tdColor %>">
									&nbsp;&nbsp;<bean:write name = "projectlist" property="endCustomer"/>
							</td>
						</tr>
					</logic:notEqual>
					
			</logic:iterate>
			<tr>
				<td colspan=2><html:submit styleClass="button_c" property = "saveEndCustomer" onclick="validate()">Save</html:submit> &nbsp;
					<html:reset   styleClass="button_c" property = "cancel">Cancel</html:reset>
				</td>
			</tr>
			
		</table>	
		
	</td></tr></table>
</html:form>
</body>
<script>
function validate() {
		document.forms[0].action="RawVendexViewAction.do?hmode=MasterRawVedexSave&msaId=<%=request.getParameter("msaid")%>"
		//document.forms[0].submit();
		return true;
	}
</script>
</html:html>