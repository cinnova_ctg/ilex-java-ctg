<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel = "stylesheet" href = "styles/style_common.css" type = "text/css">
</head>
<body>
<div>
	<table class="WASTD0001">
		<tr>
			<TD class="PCSTD0001">
				CSS Standards
			</TD>
		</tr>	
		<tr>
			<TD class="PCSSH0002 PCSSH0001">
			style_common.css
			</TD>
		</tr>
	</table>
</div>
<div>
	<table class="WASTD0001">
<pre>
/*--------------Page Content Standards-------------*/
.PCTXT0001
 { font-family:Verdana, Arial, Helvetica, sans-serif;  
 padding-left:2;
 font-size:11px; 
 font-weight:normal; 
 color:#000000; 
 vertical-align: middle;
 margin-top: 1px;
 margin-bottom: 1px ; 
 background-color: #ffffff; 
 text-align: right; 
 border-color: #E0E0DC #E6E6E3 #8E8E8C #808080; 
 border-style: solid; 
 border-top-width: 1px;
 border-right-width: 1px; 
 border-bottom-width: 1px;
 border-left-width: 1px
  }
.TBTXT0003
{
text-align: left;
font-size: 11px;
font-weight: bold;
white-space:nowrap;
font-family:Verdana, Arial, Helvetica, sans-serif;
padding-left:4;
padding-right:4;
color:#000000;
vertical-align: middle;
margin-top: 1px;
margin-bottom: 1px;
background-color: #d7e1eb;
border-color: #E0E0DC #E6E6E3 #8E8E8C #808080;
             
	}
.PCTXT0003
{
text-align: right;
font-size: 10px;
font-weight: bold;
white-space:nowrap;
font-family:Verdana, Arial, Helvetica, sans-serif;
padding-left:4;
padding-right:4;
color:#000000;
vertical-align: middle;
margin-top: 1px;
margin-bottom: 1px;
background-color: #d7e1eb;
border-color: #E0E0DC #E6E6E3 #8E8E8C #808080;

	}
.PCSSH0001
{
	font-size: 12px;
	font-weight: bold;
	color: #2e3c97;
	font-family: Arial, Tahoma, Verdana;
	text-align: left;
	white-space: nowrap;
	}

.PCSSH0002
{
margin-top: 25px
}
.PCLAB0001
{
 font-size: 11px;
 font-weight: bold;
 font-family:Verdana, Arial, Helvetica, sans-serif;
 vertical-align: top;
 padding-bottom: 5px;
 padding-top: 5px;
}

.PCSTD0001
{
 font-size: 11px;
 font-weight: bold;
 font-family:Verdana, Arial, Helvetica, sans-serif;
 vertical-align: top;
 padding-bottom: 5px;
 padding-top: 5px;
}
.PCSTD0002
{
 font-size: 11px;
 text-align: left;
 vertical-align: top;
 padding-bottom: 5px;
 padding-top: 5px;
 font-family: Arial, Tahoma, Verdana;
}
.PCSTD0003
{
border-right:1px solid;
border-bottom:1px solid;
border-color: white;
}
.PCSTD0004{
white-space:nowrap;
font-family:Verdana, Arial, Helvetica, sans-serif;
padding-left:4;
padding-right:4;
font-size:11px;
font-weight:normal;
color:#000000;
vertical-align: middle;
margin-top: 1px;
margin-bottom: 1px;
background-color: #d7e1eb;
text-align: center;
border-color: #E0E0DC #E6E6E3 #8E8E8C #808080;
}
.PCSTD0005{
border-bottom:1px solid;
border-color: white;
}
.PCSTDODD0001 {
white-space:nowrap;
font-family:Arial, Tahoma, Verdana;
font-size: 10px;
padding: 2px 4px 2px 4px;
color: #000000;
background: #ececec;
}
.PCSTDEVEN0001 {
white-space:nowrap;
font-family:Arial, Tahoma, Verdana;
font-size: 10px;
padding: 2px 4px 2px 4px;
color: #000000;
background: #f9f9f9;
}
/*-------------- Page Content End-------------*/


/*--------------Work Area  standards -------------*/
.WASTD0001
{
margin-left: 20px;
margin-top: 20px;
}
.WASTD0002
{
margin-left: 20px;
}
/*--------------work area  standards end  -------------*/


/*--------------Message standards -------------*/

.MSSUC0001
{
font-weight: bold; 
font-size:12px;
white-space: nowrap;
font-family: Arial, Tahoma, Verdana;
color: #657383
	}
.MSERR0001
{
font-weight: bold;
 font-size:12px;
 font-family: Arial, Tahoma, Verdana;
 color: red;
 white-space: nowrap;
}
	
/*-------------- Message standards End-------------*/

/*-------------- Table Standards -------------*/
.TBDAT0001
{
text-align: center;
white-space: nowrap;
}

.TBMON0001
{
text-align: right;
white-space: nowrap;
}

.TBTXT0001
{
text-align: left;
white-space: nowrap;
}
.TBSTD0001 {
font-family:arial;
font-weight: bold;
font-size: 11px;
color: #000000;
text-align: left;
white-space:nowrap;
padding: 2px 5px 2px 5px;
margin:6px 0pt 6px;
}

.TBSTD0001 thead tr {
background-color: #D9DFEF;
text-align: center;
padding: 2px 5px 2px 5px;
}
 
.TBSTD0001 thead tr th{
padding: 5px 5px 5px 5px;
margin:10px 0pt 15px;
white-space: nowrap;
}
.TBWID0001 {
margin-left:20px;
font-family:arial;
font-weight: bold;
font-size: 11px;
color: #000000;
text-align: left;
white-space:nowrap;
padding: 2px 5px 2px 5px;
margin:6px 0pt 6px;
}

.TBWID0001 thead tr {
background-color: #D9DFEF;
text-align: center;
padding: 2px 5px 2px 5px;
}

.TBWID0001 thead tr th{
padding: 5px 10px 5px 10px;
margin:10px 0pt 15px;
white-space: nowrap;
}
/*-------------- table standards End-------------*/



/*--------------BreadCrumb--------------*/
#BCMEN0001{
font-size: 9px;
font-family: Arial, Helvetica, sans-serif;
color: #a1a1a1;
padding: 0px 0px 0px 0px;
}

#BCMEN0001 a{
margin: 0px 0px 0px 0px;
padding-left:15px;
padding-right:0px;
display: inline;
background: url(../images/historyBullets_1.gif) left no-repeat;
background-position: .10em;
color: #B99C2C;
text-decoration: none;
}

#BCMEN0001 a:hover{
text-decoration: none;
color: #7A681D;
}

#BCMEN0001 .currentSelected{
margin: 0px 0px 0px 4px;
padding: 0px 0px 0px 8px;
background: url(../docm/images/historyBullets_1.gif) left no-repeat;
background-position: .10em;
color: #808080;
}

#BCMEN0001 .BCNAV0002{
margin: 0px 0px 0px 0px;
background: none;
}
.BCMEN0002{
font-size: 9px;
font-family: Arial, Helvetica, sans-serif;
font-weight: bold;
color: #0A3488 ;
padding: 0px 0px 0px 4px;
}

.BCNAV0001 { 
background-color: #C9D9E8;
font-weight:bold;
text-align: left;
border-left: 1px solid #cdcdcd;
}

.BCPAG0001
{
font-family:Verdana, Arial, Helvetica, sans-serif;
line-height:12px;
font-weight:normal;
font-size:10px;
color:#000000;
text-decoration: none;
white-space:nowrap; 
display: inline;
z-index: 1;
display: block;
float: left;
padding: 2px 15px 4px 15px;
}
/*--------------BreadCrumb  End--------------*/


/*--------------pop up window --------------*/
.PUSSH0001{
font-size: 12px; 
font-weight:bold;
white-space:nowrap;
font-family: 'Verdana', Arial, Helvetica, 'sans-serif';
padding: 4px;
color: #000000; 
background: #FAF8CC;
}
.PUBDR0001{
border:'3px solid #aaa';
}
/*--------------End--------------*/




/*--------------form Button Standards--------------*/
.FMBUT0001
{ font-family : verdana;
padding-left:2;
font-size : 10px;
margin-top: 20px;
font-style : normal;
font-weight : normal;
color : #000000;
letter-spacing : normal;
word-spacing : normal;
border : 1px #333333 solid;
background : #d7e1eb;
}
.FMBUT0002
{ font-family : verdana;
padding-left:2;
font-size : 10px;
font-style : normal;
font-weight : normal;
color : #000000;
letter-spacing : normal;
word-spacing : normal;
border : 1px #333333 solid;
background : #d7e1eb;
}
/*--------------End--------------*/


</pre>
</table>
</div>
</body>
</html>