<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<html:html>

<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hide Appendix</title>
	<%@ include file="/Header.inc" %>
	<LINK href="styles/style.css" rel="stylesheet" type="text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
	<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
</head>

<%  String backgroundclass = null;
	String backgroundclasstoleftalign = null;
	boolean csschooser = true;
	int size = 0;

	if(request.getAttribute("appSize") != null) {
		size = (int) Integer.parseInt(request.getAttribute("appSize").toString());
	}
%>

<body>
	<html:form action ="/HideAppendixAction">
		<table border="0" cellspacing="1" cellpadding="1" width=100%>
		<tr>
		  <td  width="1" height="0"></td>
		  <td>	
			<table border="0" cellspacing="1" cellpadding="1" > 
				<tr height="2"><td>&nbsp;</td></tr>
				<logic:present name = "codes" property = "appendixlist" scope = "request">
					<tr><td class = "labeleboldwhite"><bean:message bundle = "pm" key = "pmt.hideAppendix.pagetitle"/></td></tr>
				</logic:present>
				
				<logic:notPresent name= "codes" property = "appendixlist" scope = "request">
					<tr><td class = "labeleboldwhite"><bean:message bundle = "pm" key = "pmt.hideAppendix.pagetitle.first"/></td></tr>
				</logic:notPresent>
				
				
				<logic:present name="saveHiddenApp">
						<logic:equal name= "saveHiddenApp" value = "0">
							<tr height="25">
								<td class = "messagewithwrap"><bean:message bundle = "pm" key = "pmt.hideAppendix.success"/></td>
							</tr>
						</logic:equal>
				
						<logic:notEqual name="saveHiddenApp" value="0">
							<tr height="25">
								<td class = "messagewithwrap"><bean:message bundle = "pm" key = "pmt.hideAppendix.failure"/></td>
							</tr>
						</logic:notEqual>
				</logic:present>
				
				<tr>
					<td class = "labeleboldwhite"><bean:message bundle = "pm" key = "pmt.MSA"/>&nbsp;&nbsp;
						<html:select name = "HideAppendixForm" property = "msaId" size = "1" styleClass = "Ncomboe">
					   		<html:optionsCollection name = "codes" property = "customername" value = "value" label = "label"/> 
					    </html:select>&nbsp;&nbsp;
			    		<html:submit styleClass="button_c" property = "go" onclick = "return checkMSA();"><bean:message bundle="pm" key="pmt.hideAppendix.go"/></html:submit>
			    	</td>
				</tr>
			</table>
		 </td>	
		</tr>
		</table>
 	 
	<logic:present name = "codes" property = "appendixlist" scope = "request">
		<table border="0" cellspacing="1" cellpadding="1" width="100%">
			<tr><td width="1" height="0"></td></tr>
			<tr>
				<td width="1" height="0"></td>
				<td>
					<table border="0" cellspacing="1" cellpadding="1"> 
						<tr>
							<td class = "Ntryb">&nbsp; </td>
		  					<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.hideAppendix.appName"/></td>
  							<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.hideMSA.bdm"/></td>
  							<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.hideAppendix.prManager"/></td>
  							<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.hideMSA.msaStatus"/></td>
		  				</tr>
		  				
		  				<% if(size == 0) { %>
		  					<tr><td class="message" colspan="5">&nbsp;</td></tr>
		  					<tr>
		  						<td class="message" colspan="5"><bean:message bundle = "pm" key = "pmt.hideAppendix.noApp"/></td>
		  					</tr>
						<%} else {%>
		  				<logic:iterate id = "applist" name = "codes" property="appendixlist">
		  					<%	
								if ( csschooser == true ){
									backgroundclass = "Ntexto";
									backgroundclasstoleftalign ="Ntextoleftalignnowrap";
									csschooser = false;
								}else{
									csschooser = true;	
									backgroundclass = "Ntexte";
									backgroundclasstoleftalign = "Ntexteleftalignnowrap";
								}
							%>
							<tr>
								<td class = "<%= backgroundclass %>">
									<html:multibox property="check" styleClass="smallSelect">
										<bean:write name="applist" property="appendixId"/>
									</html:multibox>
									
								</td>
								<td class = "<%= backgroundclass %>"><bean:write name = "applist" property = "appendixName" /></td>
								<td class = "<%= backgroundclass %>"><bean:write name = "applist" property = "appendixBDM" /></td>
								<td class = "<%= backgroundclass %>"><bean:write name = "applist" property = "appendixPrManager" /></td>
								<td class = "<%= backgroundclass %>"><bean:write name = "applist" property = "appStatus" /></td>
							</tr>				
		  				</logic:iterate>
		  				</table>
		  				
		  			<table border="0" cellspacing="1" cellpadding="1" width="100%">  
		  				<tr class="labeleboldwhite">
							<td class = "labeleboldwhite"></td>
							<td class = "labeleboldwhite"><html:submit styleClass="button_c" property = "goHideAppendix"><bean:message bundle="pm" key="pmt.hideAppendix" /></html:submit> &nbsp;
								<html:reset styleClass="button_c" property = "cancel"><bean:message bundle="pm" key="pmt.cancel" /></html:reset>
							</td>
						</tr>
						<%} %>
					</table>
				</td>
			</tr>
		</table>
		
	</logic:present>
	
	</html:form>
</body>

<script>
function checkMSA() {
	if(document.forms[0].msaId.value == 0) {
		alert('Please select an MSA.');
		return false;
	} else {
		return true;
	}
}
</script>

</html:html>