<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script language = "JavaScript" src = "javascript/JLibrary.js"></script>
<script language = "JavaScript" src = "javascript/switchJobStatusJS.js"></script>

<html:html>

<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hide Appendix</title>
	<%@ include file="/Header.inc" %>
	<LINK href="styles/style.css" rel="stylesheet" type="text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
	<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
</head>
<%  String backgroundclass = null;
	boolean csschooser = true;
%>
<body>
<html:form action="/SwitchJobStatusAction">
	<table border="0" cellspacing="1" cellpadding="1" >
		<logic:present name="jobSwitched">
			<tr>
				<td width="1" height="0"></td>
			 	<td>
			 		<table border="0" cellspacing="1" cellpadding="1"> 	
			 			<tr><td>&nbsp;</td></tr>
						<logic:equal name= "jobSwitched" value = "0">
							<tr height="10">
								<td class = "messagewithwrap">Job Status Changed Successfully.</td>
							</tr>
						</logic:equal>
				
						<logic:notEqual name="jobSwitched" value="0">
							<tr height="10">
								<td class = "messagewithwrap">Failed to Switch Job Status.</td>
							</tr>
						</logic:notEqual>
					</table>
				</td>
			</tr>
		</logic:present>
		
		<tr>
		  <td  width="1" height="0"></td>
		  <td>	
			<table border="0" cellspacing="1" cellpadding="1" width="400"> 
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr height="20"><td class = "labeleboldwhite" colspan="4">Switch Closed Job/Ticket to Complete</td></tr>
				<tr height="20">
					<td class="labeleboldwhite" valign="middle" width="10">Invoice:&nbsp;</td>
					<td class="labeleboldwhite" valign="middle" width="20"><html:text name="SwitchJobStatusForm" property="invoiceNumber" size="15" styleClass="text" />&nbsp;</td>
					<td class="labeleboldwhite" valign="middle" width="10"><html:submit styleClass="button_c" property = "go" onclick="return validateGo();">Go</html:submit></td>
					<td class="labeleboldwhite" valign="middle" width="360">&nbsp;</td>
				</tr>
			</table>
		 </td>	
		</tr>
	</table>
	
	<logic:present name="codes" property="joblist" scope="request">
		<table border="0" cellspacing="1" cellpadding="1" width="600">
			<tr><td width="1" height="0"></td></tr>
			<tr>
				<td width="1" height="0"></td>
				<td>
					<table border="0" cellspacing="1" cellpadding="1"> 
						<tr>
		  					<td class = "Ntryb">Job Title</td>
							<td class = "Ntryb">Site Number</td>
							<td class = "Ntryb">Site Address</td>
							<td class = "Ntryb">Site City</td>
							<td class = "Ntryb">Site State</td>
							<td class = "Ntryb">Customer Name</td>
							<td class = "Ntryb"></td>
		  				</tr>
		  				<c:choose>
			  				<c:when test="${requestScope.jobListSize <= 0}">
				  				<tr>
		  							<td class="message" colspan="7">There are no job records for this Invoice Number.</td>
		  						</tr>
		  					</c:when>
		  					<c:otherwise>
		  						<logic:iterate id="jobList" name="codes" property="joblist">
		  							<%	
										if ( csschooser == true ){
											backgroundclass = "Ntexto";
											csschooser = false;
										}else{
											csschooser = true;	
											backgroundclass = "Ntexte";
										}
									%>
									<tr>
										<td class = "<%= backgroundclass %>"><a href="JobDashboardAction.do?appendix_Id=<c:out value="${jobList.appendixId}"/>&Job_Id=<c:out value="${jobList.jobId}"/>"><bean:write name = "jobList" property = "jobTitle"/></a></td>
										<td class = "<%= backgroundclass %>"><bean:write name = "jobList" property = "siteNumber"/></td>
										<td class = "<%= backgroundclass %>"><bean:write name = "jobList" property = "siteAddress"/></td>
										<td class = "<%= backgroundclass %>"><bean:write name = "jobList" property = "siteCity"/></td>
										<td class = "<%= backgroundclass %>"><bean:write name = "jobList" property = "siteState"/></td>
										<td class = "<%= backgroundclass %>"><bean:write name = "jobList" property = "msaName"/></td>
										<td class = "<%= backgroundclass %>"><a href="javascript:switchStatusForJob('<c:out value="${jobList.jobId}"/>');">Switch from Closed to Complete</a></td>
									</tr>
		  						</logic:iterate>
		  					</c:otherwise>
		  				</c:choose>
					</table>
				</td>
			</tr>
		</table>
	</logic:present>	
	
</html:form>
</body>
</html:html>
