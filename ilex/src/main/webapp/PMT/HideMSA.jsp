<!DOCTYPE HTML>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>
<html:html>

<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hide MSA</title>
	<%@ include file="/Header.inc" %>
	<LINK href="styles/style.css" rel="stylesheet" type="text/css">
	<link rel = "stylesheet" href="styles/content.css" type="text/css"> 
	<link rel = "stylesheet" href = "styles/summary.css" type="text/css">
</head>

<%	String backgroundclass = null;
	String backgroundclasstoleftalign = null;
	boolean csschooser = true;
	int size = 0;
	if(request.getAttribute("msaSize") != null) {
		size = (int) Integer.parseInt(request.getAttribute("msaSize").toString());
	}
%>

<body>
<html:form action ="/HideMSAAction">
	<table border="0" cellspacing="1" cellpadding="1" width=100%>
		<tr>
		  <td  width="1" height="0"></td>
		  <td>	
			<table border="0" cellspacing="1" cellpadding="1" > 
				<tr height="2"><td>&nbsp;</td></tr>
				<tr><td class = "labeleboldwhite"><bean:message bundle = "pm" key = "pmt.hideMSA.pagetitle"/></td></tr>
			</table>
		 </td>	
		</tr>
	</table>
		
	
	<logic:present name="saveMsa">
		<table border="0" cellspacing="1" cellpadding="1" width="100%">
			<logic:equal name= "saveMsa" value = "0">
				<tr height="25">
					<td width="1" height="0"></td>
					<td class = "messagewithwrap"><bean:message bundle = "pm" key = "pmt.hideMSA.success"/></td>
				</tr>
			</logic:equal>
				
			<logic:notEqual name="saveMsa" value="0">
				<tr height="25">
					<td width="1" height="0"></td>
					<td class = "messagewithwrap"><bean:message bundle = "pm" key = "pmt.hideMSA.failure"/></td>
				</tr>
			</logic:notEqual>
		</table>
	</logic:present>
	
	<logic:present name = "msalist" scope = "request">
		<table border="0" cellspacing="1" cellpadding="1" width="100%">
			<tr><td width="1" height="0"></td></tr>
			<tr>
				<td width="1" height="0"></td>
				<td>
					<table border="0" cellspacing="1" cellpadding="1"> 
						<tr>
							<td class = "Ntryb">&nbsp; </td>
		  					<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.hideMSA.msaName"/></td>
  							<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.hideMSA.bdm"/></td>
  							<td class = "Ntryb"><bean:message bundle = "pm" key = "pmt.hideMSA.msaStatus"/></td>
		  				</tr>
		  				
		  				<% if(size == 0) { %>
		  					<tr><td class="message" colspan="4">&nbsp;</td></tr>
		  					<tr>
		  						<td class="message" colspan="4"><bean:message bundle = "pm" key = "pmt.hideMSA.noMSA"/></td>
		  					</tr>
						<%} else {%>
		  				<logic:iterate id = "msalist" name = "msalist">
		  					<%	
								if ( csschooser == true ){
									backgroundclass = "Ntexto";
									backgroundclasstoleftalign ="Ntextoleftalignnowrap";
									csschooser = false;
								}else{
									csschooser = true;	
									backgroundclass = "Ntexte";
									backgroundclasstoleftalign = "Ntexteleftalignnowrap";
								}
							%>
							<tr>
								
								<td class = "<%= backgroundclass %>">
									<html:multibox property="check" styleClass="smallSelect">
										<bean:write name="msalist" property="msaId"/>
									</html:multibox>
								</td>
								<td class = "<%= backgroundclass %>"><bean:write name = "msalist" property = "msaName" /></td>
								<td class = "<%= backgroundclass %>"><bean:write name = "msalist" property = "msaBDM" /></td>
								<td class = "<%= backgroundclass %>"><bean:write name = "msalist" property = "msaStatus" /></td>
							</tr>				
		  				</logic:iterate>
						<%} %>
					</table>
				</td>
			</tr>
		</table>
	</logic:present>
	
	<% if(size>0) { %>
		<table border="0" cellspacing="1" cellpadding="1" width="100%">
			<tr class="labeleboldwhite">
				<td>&nbsp;</td>
				<td><html:submit styleClass="button_c" property = "goHideMSA"><bean:message bundle="pm" key="pmt.hideMSA" /></html:submit> &nbsp;
					<html:reset styleClass="button_c" property = "cancel"><bean:message bundle="pm" key="pmt.cancel" /></html:reset>
				</td>
			</tr>
		</table>
	<%} %>
	
</html:form>
</body>

<script>

function validate() {
	var submitPage = 'false';
	
	if(<%=size%> != 0) {
		if(<%= size %> == 1) {
			if(!document.forms[0].check.checked) {
				alert('Select MSA to hide.');
			return false;
			} 
		} 
		else 
		{
			for(var i = 0; i < document.forms[0].check.length ; i++ ) {
				if(document.forms[0].check[i].checked) {
					//alert('111--'+document.forms[0].check[i].value);
					submitPage = 'true';
				}
			}
			
			if(submitPage == 'false') {
				alert('Select atleast one MSA to hide.');
				return false;
			}
		}
	}
	alert('validated!!!');
	return true;
}
</script>
</html:html>
