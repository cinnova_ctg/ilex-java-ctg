<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"  %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"  %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Master Tool Definitions</title>
<link href="docm/styles/style.css" rel="stylesheet" type="text/css" />
<link href="docm/styles/content.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="docm/javascript/JLibrary.js"></script>
<script language="javascript" src="docm/javascript/ilexGUI.js"></script>
<%@ include  file="/NMenu.inc" %>
<script>
	function showTool() {
		document.forms[0].mode.value = "view";
		validateAndClose();
	}
	function addMe() {
		document.forms[0].mode.value = "add";
		validateAndClose();
	}
	function modifyMe() {
		document.forms[0].mode.value = "modify";
		validateAndClose();
	}
	function deleteMe() {
		document.forms[0].mode.value = "delete";
		validateAndClose();
	}
	function validateAndClose() {
		trimFields();
		if(document.forms[0].name.value == '' && document.forms[0].mode.value!='delete' && document.forms[0].mode.value!='view') {
			alert('Please Enter Tool Name');
			document.forms[0].name.focus();
			return false;
		}
		if(document.forms[0].mode.value=='delete' && document.forms[0].toolId.value==-1) {
			alert('Please select a tool to delete');
			document.forms[0].toolId.focus();
			return false;
		}
		if(document.forms[0].mode.value=='modify' && document.forms[0].toolId.value==-1) {
			alert('Please select a tool to update');
			document.forms[0].toolId.focus();
			return false;
		}
		
		document.forms[0].action = "ToolsDescAction.do";
		document.forms[0].submit();
	}
</script>


</head>
<body>
	<html:form action = "/ToolsDescAction">
		<html:hidden property = "mode" value =""/>
		
		
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr><td class="headerrow" height="19">&nbsp;</td></tr>
				<tr><td background="images/content_head_04.jpg" height="21">&nbsp;</td></tr>
				<tr><td><h2>Master Tool Definitions</h2></td></tr>
		</table>
		<table width = "100%" border = "0" cellspacing = "1" cellpadding = "1" align = "center">
					<tr><td colspan='2' class="message">
						<logic:equal name="add" value="0">
								Tool Added successfully.
						</logic:equal>
						<logic:equal name="update" value="0">
								Tool Updated successfully.
						</logic:equal>
						<logic:equal name="delete" value="0">
								Tool Deleted successfully.
						</logic:equal>
						<logic:equal name="add" value="-9005">
								Tool name already exists.
						</logic:equal>
						<logic:equal name="update" value="-9005">
								Tool name already exists.
						</logic:equal>
						<logic:equal name="delete" value="-9003">
								Delete action could not be performed!>
						</logic:equal>
						<logic:equal name="errorOccured" value="-1">
								Action could not be performed!
						</logic:equal>
					</td></tr>
					<tr>
						<td  class = "colDark">Select Tool </td>
						<td  class = "colLight">
								 <html:select   property="toolId" styleClass="select" onchange = "showTool();">
										<html:optionsCollection name = "dynamicComboTool"  property = "toolslist"  label = "label" value = "value"  />
								</html:select>   
						</td>
					</tr>
					<tr>
						<td  class = "colDark">Tool Name</td>
						<td  class = "colLight"><html:text size="30"  property = "name" styleClass = "text"/>
						</td>
					</tr>
					<tr>
						<td  class = "colDark">Tool Description</td>
						<td  class = "colLight"><html:text  size="60"  property = "description" styleClass = "text"/></td>
					</tr>
					<tr>
						<td class = "colDark">Is Active?
						</td>
						<td class = "colLight">
							<html:checkbox  property="isActive"  styleClass="smallSelect" value="Y"/>
						</td>
					</tr>
					<tr>
						<td colspan = "2" class = "colLight" align ="center">		
							<html:button property="add" styleClass="button_c" onclick="addMe();">Add</html:button>&nbsp;&nbsp;
							<html:button property="modify" styleClass="button_c" onclick="modifyMe();">Update</html:button>&nbsp;&nbsp;
							<html:button property="delete" styleClass="button_c" onclick="deleteMe();">Delete</html:button>&nbsp;&nbsp;
							<input type="reset" name="button2" class="button_c" value="Reset" />
					  	</td>
					</tr>
		</table>
	</html:form>
</body>
</html>