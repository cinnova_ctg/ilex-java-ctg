<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<head>
	<link rel = "stylesheet" href = "styles/style_common.css" type = "text/css">
	<script language = "JavaScript" src = "javascript/JQueryMain.js?timestamp="+(new Date()*1)></script>
		<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
	<script>
$(document).ready(function(){
	$('#popbutton').click(function() {
		openChangesTicketOwnerPopup();
	}); 
	
});
$('#popbutton').click(function() {
	openChangesTicketOwnerPopup();
});
function openChangesTicketOwnerPopup() {
	blockUnblockTicketOwner();
	var uri="CssStandardsAction.do?hmode=popUpWindowStandards1";
	$("#ticketOwnerItemValue").load(uri);
	$("#ticketOwnerItem").text('Change Ticket Owner for [ticket#],[Site Number]');
	}
function blockUnblockTicketOwner() {
	$.blockUI(
		{ 
			message: $('#ticketOwnerModal'),
			css: { 
				width: '400px',
				height:'300px',
				top:'120px', 
			    left:'200px', 
			    position: 'fixed',
				color:'lightblue'
				},
			overlayCSS:  { 
		        backgroundColor: '#f0f0f0', 
		        opacity:         0.3 
		    },
		    centerX: false,  
		    centerY: false,
		    fadeIn:  0,
		    fadeOut: 0
		}
		);
}
function closeChangeTicketOwnerPopup() {
    $.unblockUI(); 
	$("#ticketOwnerItemValue").empty().html('<center><img src="./images/waiting_tree.gif" /></center>');
	document.getElementById("ticketOwnerItem").innerHTML = '';
   // return false; 
}
</script>
</head>

<form action="InvoicePriceAction"> 
<div>
<%@ include file = "/NMrnu1.inc" %> 
<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
				<!-- Menu -->
		<table cellpadding="0" cellspacing="0" border="0"  >
			<tr>
		       	<td id="pop1" height="19" class="BCMEN0001" align="center"><a href="#"  onMouseOut="MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver="MM_swapImage('Image24','','./images/services1b.gif',1);popUp('elMenu1',event)"  class="BCMEN0002" style="width: 100px"><center>Manage</center></a></td>
				<td id="pop2" height="19" class="BCMEN0001" align="center"><a href="#"  class="BCMEN0002" style="width: 100px"><center>Search</center></a></td>
				<td id="pop6"  class ="BCMEN0001" width="100%">&nbsp;</td>
			</tr>
		</table>
			</td>
		</tr>
		<tr>
			<!-- Bread Crumb -->
			<td background="images/content_head_04.jpg" height="21">
				<div id="BCNAV0001">
				 	<a class="BCNAV0002">Invoices</a>
				 	<a>[msaName]</a>
				 	<a>[AppendixName]</a>
				 	<a><span class="BCPAG0001">Complete Jobs to be Invoiced</span></a>
				</div>
		    </td>
		</tr>
	</table>
</div>
<div>
	<table class="WASTD0001">
	<%@include file="/PMT/PMTTopMenuScript.inc" %>
		<tr>
			<TD class="PCSTD0001">
				CSS Standards-Pop Up Window
			</TD>
		</tr>	
		<tr>
			<TD class=" PCSSH0001 PCSSH0002 ">
			
				 PUSSH0001 &nbsp; &nbsp; &nbsp;PUBDR0001 
			</TD>
		</tr>
	
		<tr>
			<td>
					<table cellspacing = "0" cellpadding = "0">
							<tr>
								<td >
									 <input type="button" class="FMBUT0001" id="popbutton" value="Click To Open Pop Window "></input>
								</td>
							</tr>
					
					</table>
			</td>
		</tr>
	</table>
<div id="ticketOwnerModal"
	style="display: none; cursor: default; border: 1px; border-color: red">
<div style="border: 2px; background-color: blue">
<table   border='0' cellspacing="0" cellpadding="0"
	class="PUSSH0001" width="100%">
	<tr>
		<td align="left" id="ticketOwnerItem" 
			></td>
		<td align="right"><img height="15" width="15" alt="Close"
			src="images/delete.gif" id="closeShipping"
			onclick="closeChangeTicketOwnerPopup();"></td>
	</tr>
</table>
</div>
<div id="ticketOwnerItemValue" class="modalValue" align="left" style="padding- top: 4px; overflow: visible; height: 300px; overflow-x: auto; overflow-y: auto;"><img src="./images/waiting_tree.gif" /></div>

</div>

</div>
</body>
</form>

</html>

