<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<head>
	<link rel = "stylesheet" href = "styles/style_common.css" type = "text/css">
		<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
</head>

<form action="InvoicePriceAction"> 
<div>
<%@ include file = "/NMrnu1.inc" %> 
<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
				<!-- Menu -->
		<table cellpadding="0" cellspacing="0" border="0"  >
			<tr>
		       	<td id="pop1" height="19" class="BCMEN0001" align="center"><a href="#"  onMouseOut="MM_swapImgRestore(); popDown('elMenu1',event)" onMouseOver="MM_swapImage('Image24','','./images/services1b.gif',1);popUp('elMenu1',event)"  class="BCMEN0002" style="width: 100px"><center>Manage</center></a></td>
				<td id="pop2" height="19" class="BCMEN0001" align="center"><a href="#"  class="BCMEN0002" style="width: 100px"><center>Search</center></a></td>
				<td id="pop6"  class ="BCMEN0001" width="100%">&nbsp;</td>
			</tr>
		</table>
			</td>
		</tr>
		<tr>
			<!-- Bread Crumb -->
			<td background="images/content_head_04.jpg" height="21">
				<div id="BCNAV0001">
				 	<a class="BCNAV0002">Invoices</a>
				 	<a>[msaName]</a>
				 	<a>[AppendixName]</a>
				 	<a><span class="BCPAG0001">Complete Jobs to be Invoiced</span></a>
				</div>
		    </td>
		</tr>
	</table>
</div>
<div>
	<table class="WASTD0001">
	<%@include file="/PMT/PMTTopMenuScript.inc" %>
		<tr>
			<TD class="PCLAB0001">
				CSS Standards-Page Contents
			</TD>
		</tr>	
		<tr>
			<TD class="PCSSH0001">
				 PCSTD0001 &nbsp; &nbsp; &nbsp;PCSTD0002&nbsp; &nbsp; &nbsp;PCSTD0003&nbsp; &nbsp; &nbsp;PCSTD0004&nbsp; &nbsp; &nbsp;PCSTD0005&nbsp; &nbsp; &nbsp; PCSSH0001 &nbsp; &nbsp; &nbsp;PCSSH0002
			</TD>
		</tr>
			
		<tr>
			<td>
				<table class="PCSSH0002">
  						<tr>
					    	<td class="PCSSH0001" colspan="4">
					    	Search By Contingent Invoice Number</td>
						</tr> 
					  	<tr>
					    	<td  class="PCLAB0001">&nbsp;Invoice Number :</td>
					    	<td colspan="1"><input type="text" class="PCTXT0001"/>					    	
					    	<td colspan="1">&nbsp;&nbsp;<input type="button" class="FMBUT0002" value="Search Invoice"></input></td>
							<td colspan="1">&nbsp;&nbsp;<input type="button" class="FMBUT0002" value="Cancel"></input></td>
					  </tr>
					  <tr>
						    <td colspan="4" class="MSERR0001"> Buttons used here are  using FMBUT0002 class  having no top margin. 
						    </td>
						    </tr>
				</table>
			</td>
		</tr>
		
	   <tr>
		   <td>
			   <table class="PCSSH0002">
				   <tr>
						<td  class="PCSSH0001 " >Search Result</td>
			    	</tr>		
					<tr>
						<td class = "PCLAB0001">&nbsp;Customer:</td>
						<td class = "PCSTD0002"  > IBM Corporation</td>
					</tr>
					<tr>
						<td class = "PCLAB0001">&nbsp;Appendix Title: </td>
						<td class = "PCSTD0002"  >Appendix 11 - TWTC</td>
					</tr>
					<tr>
						<td class = "PCLAB0001">&nbsp;Sr. PM:</td>
						<td class = "PCSTD0002" >David Kuhr</td>
					</tr>
					<tr>
						<td class = "PCLAB0001">&nbsp;PM:</td>
						<td class = "PCSTD0002" >Michele Zinser</td>
					</tr>
			   </table>
		   </td>
	   </tr>
      <tr>
	      <td>
	     	 <table  border = "0" cellspacing = "0" cellpadding = "0">
			    <tr>
		      		<td rowspan="2">&nbsp;</td>
			    	<td  class="PCSTD0004 PCSTD0003" rowspan="2" >Invoice Number</td>
					<td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Invoice Date</td>
				    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Invoice Price</td>
				    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" colspan="3">Job Title</td>
				    <td class="PCSTD0004 PCSTD0003"   rowspan="2" valign="middle">Job Owner</td>
				    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" valign="middle"><bean:message bundle = "IM" key = "im.customerreference"/></td>
			  	</tr>
			   	<tr> 
				</tr>
	 			<tr>		
			
					<td class="FMRAD0001" >
			   			<input type="radio"  />	
					</td>
					<td   class="PCSTDODD0001 PCSTD0003 TBDAT0001" >
						<input type="text" class="PCTXT0001"/>
					</td>
					
					<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" >
							<input type="text" class="PCTXT0001"/>
					</td>
					<td   class="PCSTDODD0001 PCSTD0003 TBMON0001" >
						$200.00
					</td>
									
										<td   class="PCSTDODD0001  PCSTD0005 TBTXT0001" colspan="1">
											<a href="#">TWTC Cisco</a>
										</td>
										<td  class="PCSTDODD0001 TBDAT0001 PCSTD0005"  colspan="1">
				
										&nbsp;&nbsp;<img name = "outscopeactivityflag"  src = "images/oos1.gif" width = "14" height = "11" border = "0" alt = '<bean:message bundle = "IM" key = "im.outscopeflagpresent"/>'/>
									</td>
										<td  class="PCSTDODD0001  TBDAT0001 PCSTD0003"  colspan="1">
											<a href="#"><img name = "icon"  src = "images/icon.gif" width="13" height="13" "  border = "0" /></a>
										</td>
								
				
					<td   class="PCSTDODD0001 PCSTD0003 TBTXT0001" >
						Anna Hoskins
					</td>
					
					
					
					<td  class="PCSTDODD0001 PCSTD0003 TBTXT0001" >
				4553930
				  </tr>
				</table>
				</td>
				</tr>
		  <tr>
		    <td> 
					    &nbsp;&nbsp;&nbsp;&nbsp;
						    <input type="button" class="FMBUT0001" value="Switch to Offsite"></input>
							&nbsp;&nbsp;  <input type="button" class="FMBUT0001" value="Cancel" ></input>
		    </td>
		    <tr>
		    <td class="MSERR0001"> Buttons used here are  using FMBUT0001 class  having top margin 20 px. 
		    </td>
		    </tr>
		  </tr> 

	</table>
</body>
</form>

</html>
