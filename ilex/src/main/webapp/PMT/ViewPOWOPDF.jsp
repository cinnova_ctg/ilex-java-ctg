<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>

<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>View PO/WO PDF</title>
	
	<link href="styles/style.css" rel="stylesheet" type="text/css" />
	<link href="styles/content.css" rel="stylesheet" type="text/css" />
	<link href="styles/summary.css" rel="stylesheet" type="text/css" />
	
<script>
function onlyNumbers(e)
{
	var keynum; var keychar; var numcheck;
	
	if(window.event){ // IE
		keynum = e.keyCode;
	}
	keychar = String.fromCharCode(keynum);
	numcheck = /\d/;
	return numcheck.test(keychar);
}
function validateNumbers(obj)
{
	if(parseInt(obj.value) != obj.value);{
		obj.value=parseInt(obj.value);
	}
	if(!isNaN(obj.value)){
		return true;
	}
	else{
		obj.value = '';
	}
}

</script>

</head>
<body>
	<table>
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
	</table>
	<html:form action="/ViewPOWOPDFAction">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>&nbsp;</td>
		<td style=" padding-left: 8px;">
			<table cellpadding="1" cellspacing="1" border="0">
				<logic:present name="isNaN" scope="request">
					<tr>
						<td class="message">	
							No file Found.
						</td>
					</tr>
				</logic:present>
				<tr>
					<td class = "labeleboldwhite" height = "30" colspan="2">View PO/WO PDF
					</td>
				</tr>
				<tr>
					<td class = "tabtexto">
						<b>PO/WO#:&nbsp;</b>
					</td>
					<td class="pvstexto">
						<html:text name="ViewPOWOPDFForm" property="poId" styleClass="text" size="20" onkeypress="return onlyNumbers(event);" onblur="validateNumbers(this);" />
					</td>
				</tr>
				<tr>
					<td class = "tabtexto">
						<b>Type:&nbsp;</b>
					</td>
					<td class="pvstexto">
						PO&nbsp;<html:radio name="ViewPOWOPDFForm" property="type" value="P" styleClass="redio"/>&nbsp;
						WO&nbsp;<html:radio name="ViewPOWOPDFForm" property="type" value="W" styleClass="redio"/>
					</td>
				</tr>
				<tr>
					<td class = "tabtexto">
					</td>
					<td class="pvstexto">
						<html:submit property="go" styleClass="button_c">GO</html:submit>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
	</html:form>
</body>
</html:html>