<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>

<head>
	<link rel = "stylesheet" href = "styles/style_common.css" type = "text/css">
</head>

<form action="InvoicePriceAction">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
				<!-- Menu -->
		<table cellpadding="0" cellspacing="0" border="0" width="100%" >
			<tr>
		       	<td id="pop1" height="19" class="BCMEN0001" align="center"><a href="#"  class="BCMEN0002" style="width: 100px"><center>Menu Item</center></a></td>
			</tr>
		</table>
			</td>
		</tr>
		<tr>
			<!-- Bread Crumb -->
			<td background="images/content_head_04.jpg" height="21">
				<div id="BCNAV0001">
				 	<a class="BCNAV0002" href="#">Level1</a>
				 	<a href="#">Level2</a>
				 	<a><span class="BCPAG0001">Page Title</span></a>
				</div>
		    </td>
		</tr>
	</table>
<table cellspacing = "0" cellpadding = "0" class="WASTD0001" >
		<TR>
			<TD >
				<table>
  						<tr>
					    	<td class="PCSSH0001" height="20"  colspan="21" >
					    	<div>Subsection Title 1</div></td>
					   </tr> 
					  <tr>
					    	<td class = "PCSTD0002 MSERR0001"  colspan="3">[ Section Titles are One Font Size Increment larger than   </td>
					    </tr>
					     <tr>
					    	<td class = "PCSTD0002 MSERR0001"  colspan="3"> content font Size used for content.Bold in dark blue color. </td>
					    </tr>
					    <tr>
					    	<td class = "PCSTD0002 MSERR0001"  colspan="3">Always Contains one line break above and below it. ] </td>
					  </tr>
			</TABLE>
</TD>
</TR>
				   <tr>
					   <td>
						   <table class="PCSSH0002">
							   <tr>
									<td  height="20" class="PCSSH0001" colspan="21">
						    			<div >Subsection Title2</div></td>
								</tr>
								<tr>
									<td class = "PCLAB0001">&nbsp;Customer:</td>
									<td class = "PCSTD0002"  colspan="3"> [MSA Name]</td>
								</tr>
								<tr>
									<td class = "PCLAB0001">&nbsp;Appendix Title:</td>
									<td class = "PCSTD0002"  colspan="3">[Appendix Title Name]</td>
								</tr>
								<tr>
									<td class = "PCLAB0001">&nbsp;Sr. PM:</td>
									<td class = "PCSTD0002" colspan="3">[Sr. PM First Name and Last Name]</td>
								</tr>
								<tr>
									<td class = "PCLAB0001">&nbsp;PM:</td>
									<td class = "PCSTD0002" colspan="3">[PM First Name and Last Name]</td>
								</tr>
						   </table>
					   </td>
				   </tr>
				   <tr>
					    	<td class = "PCSTD0002 MSERR0001"  colspan="3">[ All Content are of same size unless explicitly identified in design document.</td>
					    </tr>
					     <tr>
					    	<td class = "PCSTD0002 MSERR0001"  colspan="3"> Labels are bold </td>
					    </tr>
					    <tr>
					    	<td class = "PCSTD0002 MSERR0001"  colspan="3">Form elements are non bold ] </td>
					  </tr>
      <tr>
	      <td>
	     	 <table cellpadding="0px" cellspacing="0px">
	      <tr>
      <td rowspan="2">&nbsp;</td>
	    	<td  class="PCSTD0004 PCSTD0003" rowspan="2" >Invoice Number</td>
			<td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Invoice Date</td>
		    <td  class="PCSTD0004 PCSTD0003"  rowspan="2" >Invoice Price</td>
	  	</tr>
   <tr> 
	  </tr>
	  <tr>		
			
			<td class="FMRAD0001" >
	   			<input type="radio"  />	
			</td>
			<td   class="PCSTDODD0001 PCSTD0003 TBDAT0001" >
				<input type="text" class="PCTXT0001"/>
			</td>
			
			<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" >
					<input type="text" class="PCTXT0001"/>
			</td>
			<td  class="PCSTDODD0001 TBDAT0001 TBMON0001" style="border-bottom: 1px solid; border-color: white " colspan="1">$10,123.00</td>
							
	  </tr>
	  <tr>		
			
			<td class="TBMON0001" >
	   			<input type="radio"  />	
			</td>
			<td   class="PCSTDEVEN0001 PCSTD0003 TBDAT0001" >
				<input type="text" class="PCTXT0001"/>
			</td>
			
			<td  class="PCSTDEVEN0001 PCSTD0003 TBDAT0001" >
					<input type="text" class="PCTXT0001"/>
			</td>
			<td  class="PCSTDEVEN0001 TBDAT0001 TBMON0001" >$10,123.00</td>
							
	  </tr>
	  <tr>		
			
			<td class="TBMON0001" >
	   			<input type="radio"  />	
			</td>
			<td   class="PCSTDODD0001 PCSTD0003 TBDAT0001" >
				<input type="text" class="PCTXT0001"/>
			</td>
			
			<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" >
					<input type="text" class="PCTXT0001"/>
			</td>
			<td  class="PCSTDODD0001 TBDAT0001 TBMON0001" >$10,123.00</td>
							
	  </tr>
	   <tr>		
			
			<td class="TBMON0001" >
	   			<input type="radio"  />	
			</td>
			<td   class="PCSTDEVEN0001 PCSTD0003 TBDAT0001" >
				<input type="text" class="PCTXT0001"/>
			</td>
			
			<td  class="PCSTDEVEN0001 PCSTD0003 TBDAT0001" >
					<input type="text" class="PCTXT0001"/>
			</td>
			<td  class="PCSTDEVEN0001 TBDAT0001 TBMON0001" >$10,123.00</td>
							
	  </tr>
	  <tr>		
			
			<td class="TBMON0001" >
	   			<input type="radio"  />	
			</td>
			<td   class="PCSTDODD0001 PCSTD0003 TBDAT0001" >
				<input type="text" class="PCTXT0001"/>
			</td>
			
			<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" >
					<input type="text" class="PCTXT0001"/>
			</td>
			<td  class="PCSTDODD0001 TBDAT0001 TBMON0001" >$10,123.00</td>
							
	  </tr>
	   <tr>		
			
			<td class="TBMON0001" >
	   			<input type="radio"  />	
			</td>
			<td   class="PCSTDEVEN0001 PCSTD0003 TBDAT0001" >
				<input type="text" class="PCTXT0001"/>
			</td>
			
			<td  class="PCSTDEVEN0001 PCSTD0003 TBDAT0001" >
					<input type="text" class="PCTXT0001"/>
			</td>
			<td  class="PCSTDEVEN0001 TBDAT0001 TBMON0001">$10,123.00</td>
							
	  </tr>
	  <tr>		
			
			<td class="TBMON0001" >
	   			<input type="radio"  />	
			</td>
			<td   class="PCSTDODD0001 PCSTD0003 TBDAT0001" >
				<input type="text" class="PCTXT0001"/>
			</td>
			
			<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001" >
					<input type="text" class="PCTXT0001"/>
			</td>
			<td  class="PCSTDODD0001 TBDAT0001 TBMON0001">$10,123.00</td>
							
	  </tr>
	  </table>
	 <tr>
		    <td> 
					    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						    <input type="button" class="FMBUT0001" value="Switch to Offsite"></input>
							&nbsp;&nbsp;  <input type="button" class="FMBUT0001" value="Cancel" ></input>
		    </td>
     <tr>
     <td> 
      <table class="PCSSH0002">
  						<tr>
					    	<td class="PCSSH0001" height="20"  colspan="21" >
					    	<div>Subsection Title 3</div></td>
					   </tr> 
					  <tr>
					    	<td class="MSSUC0001"  >General -Muted Gray and Bold</td>
					    </tr>
					    <tr>
					    	<td class="MSERR0001 " >Error -Muted Red and Bold</td>	
					  </tr>
			</TABLE>
      </td> 
</tr>
</table>
</body>
</form>

</html>

