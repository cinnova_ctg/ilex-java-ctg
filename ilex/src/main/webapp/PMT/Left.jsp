
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
 
<html:html>
<head>
<%@ include  file="/Header.inc" %>
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>

<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<script language = 'JavaScript' src = '../javascript/jquery-1.3.2.min.js'></script>
<script language="javascript" src="../javascript/ilexGUI.js"></script>
<script language = "JavaScript" src = "../javascript/jquery-tab.min.js?timestamp="+(new Date()*1)></script>
<script language = "JavaScript" src = "../javascript/jquery.corner.js?timestamp="+(new Date()*1)></script>
<title>Hierarchy Navigation</title>
<link href="../styles/jquery-tab.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../styles/style.css" type="text/css">
<link rel = "stylesheet" href = "../styles/style_common.css" type = "text/css">
<style>
    body  {
    	font-family : "Verdana";
    	font-size : 11px;
    	cursor : auto;
    	color : #000000;
    	padding : 0px 0px 0px 0px;
    	white-space : nowrap;
    }
    .sectionHeader{
    color: #20477d;
font-size: 11px;
font-weight: bold;
vertical-align: top;}
</style>
</head>

<script>	
	$(document).ready(function(){
		$("#tabs").tabs();
		$("#tab-1").corner("top");
		$("#tab-2").corner("top");
	});	
</script>
<body   text="#000000" marginwidth="0" marginheight="0"  onLoad="MM_preloadImages('../images/t_msa.gif','../images/t_appendix.gif')" style="background-color: #f4f9fd;">
<div id="tabs" style=" background-color: #f4f9fd;">
	    <ul style=" background-color: #f4f9fd;">
	    	 <li id="tab-1"><a href="#fragment-1" style="width:75px;text-align: center;"><span>Integrations</span></a></li>
	         <li id="tab-2" ><a href="#fragment-2" style="width: 75px;text-align: center;"><span>General</span></a></li>      
	    </ul>
	    <div id="fragment-1" style="border: 0px;padding: 0px;">
	    	<div style="margin-top:20px;margin-left: 10px;" class="sectionHeader">Chick-fill-A</div>
	    	<div style="margin-left:30px;">
	    	<div style="margin-top: 10px;"><a href="/Ilex-WS/serviceNowIntegrate/recentIncidents.html?msaId=462&appendixId=2023&userid=${sessionScope.userid}" target="ilexmain" style="text-decoration: none;color: #5188d3 !important">Integrations</a></div>
	    	<div style="margin-top: 10px;"><a href="/Ilex-WS/serviceNowIntegrate/getProactiveIncidentCreatePage.html?msaId=462&appendixId=2023&userid=${sessionScope.userid}" target="ilexmain" style="text-decoration: none;color: #5188d3 !important">Create New Incident</a></div>
	    	</div>
	    	<div style="margin-top:20px;margin-left: 10px;" class="sectionHeader">Texas Roadhouse</div>
	    	<div style="margin-left:30px;">
	    	<div style="margin-top: 10px;"><a href="/Ilex-WS/serviceNowIntegrate/recentIncidents.html?msaId=740&appendixId=3018&userid=${sessionScope.userid}" target="ilexmain" style="text-decoration: none;color: #5188d3 !important">Integrations</a></div>
	    	<div style="margin-top: 10px;"><a href="/Ilex-WS/serviceNowIntegrate/getProactiveIncidentCreatePage.html?msaId=740&appendixId=3018&userid=${sessionScope.userid}" target="ilexmain" style="text-decoration: none;color: #5188d3 !important">Create New Incident</a></div>
	    	</div>
	    	<div style="margin-top:20px;margin-left: 10px;" class="sectionHeader">Error Log</div>
	    	<div style="margin-left:30px;">
	    	<div style="margin-top: 10px;"><a href="/Ilex-WS/serviceNowIntegrate/errorLogs.html?msaId=462&appendixId=2023&userid=${sessionScope.userid}"  target="ilexmain" style="text-decoration: none;color: #5188d3 !important">Recent Errors</a></div>
	    	</div>
	     </div>
		<div id="fragment-2" style="border: 0px;padding: 0px;">
		
				<table border="0" cellspacing="1" cellpadding="0" style="margin-top: 20px;">

   <tr>
    <td height="30">&nbsp;</td>
    <td class="toplink" height="20" ><b><bean:message bundle = "pm" key = "pmt.pmt"/></b></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
    <td class="link" height="20" ><img src='../images/t_msa.gif' width=15 height=15 border=0></img>&nbsp;<a href="../EditXmlAction.do?Type=MSA&EditType=PMT" target="ilexmain"><bean:message bundle = "pm" key = "pmt.msa"/></a></td>
   </tr>
  
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img>&nbsp;<a href="../EditXmlAction.do?Type=NetMedX&EditType=PMT" target="ilexmain"><bean:message bundle = "pm" key = "pmt.netmedx"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img>&nbsp;<a href="../EditXmlAction.do?Type=NewNetMedX&EditType=PMT" target="ilexmain"><bean:message bundle = "pm" key = "pmt.newnetmedx"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img>&nbsp;<a href="../EditXmlAction.do?Type=Appendix&EditType=PMT" target="ilexmain"><bean:message bundle = "pm" key = "pmt.appendix"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img>&nbsp;<a href="../EditXmlAction.do?Type=Hardware&EditType=PMT" target="ilexmain"><bean:message bundle = "pm" key = "pmt.hardware"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img>&nbsp;<a href="../EditMasterAction.do?type=emailbody" target="ilexmain"><bean:message bundle = "pm" key = "pmt.email"/></a></td>
   </tr>
   <!-- MCSA: Start -->
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img>&nbsp;MCSA-PO Version</td>
   </tr>
   
   <tr>
	<td class="link" style="padding-left: 25px" colspan="2">Minuteman</a></td>
   </tr>
    <tr> 
	<td class="link" colspan="2" style="padding-left: 30px">- <a href="../EditMasterAction.do?type=minutemanterm" target="ilexmain">Domestic</a></td>
   </tr>
   <tr> 
	<td class="link" colspan="2" style="padding-left: 30px">- <a href="../EditMasterAction.do?type=inminutemanterm" target="ilexmain">International</a></td>
   </tr>
   
   <tr>
	<td class="link" style="padding-left: 25px" colspan="2">Speedpay</a></td>
   </tr>
    <tr> 
	<td class="link" colspan="2" style="padding-left: 30px">- <a href="../EditMasterAction.do?type=speedterm" target="ilexmain">Domestic</a></td>
   </tr>
   <tr> 
	<td class="link" colspan="2" style="padding-left: 30px">- <a href="../EditMasterAction.do?type=inspeedterm" target="ilexmain">International</a></td>
   </tr>
   
   <tr>
	<td class="link" style="padding-left: 25px" colspan="2">PVS</a></td>
   </tr>
    <tr> 
	<td class="link" colspan="2" style="padding-left: 30px">- <a href="../EditMasterAction.do?type=poterm" target="ilexmain">Domestic</a></td>
   </tr>
   <tr> 
	<td class="link" colspan="2" style="padding-left: 30px">- <a href="../EditMasterAction.do?type=inpoterm" target="ilexmain">International</a></td>
   </tr>
   <!-- MCSA: End -->
   
    <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><img src='../images/t_appendix.gif' width=15 height=15 border=0></img>&nbsp;<a href="../EditMasterAction.do?type=pospinst" target="ilexmain"><bean:message bundle = "pm" key = "pmt.pospinst"/></a></td>
   </tr>
</table>

<table border="0" cellspacing="1" cellpadding="0" >
   <tr>
   	<td height="30">&nbsp;</td>
    <td class="toplink" height="20" ><b>Support Tools</b></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><a href="../MasterchecklisttoAppendixcopyAction.do" target="ilexmain"><bean:message bundle = "pm" key = "pmt.processchecklist"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><a href="../DeleteJobAction.do?flag=true" target="ilexmain"><bean:message bundle = "pm" key = "pmt.deletejob"/></a></td>
   </tr>
   
    <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><a href="../HideMSAAction.do" target="ilexmain"><bean:message bundle = "pm" key = "pmt.hideMSA"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><a href="../HideAppendixAction.do" target="ilexmain"><bean:message bundle = "pm" key = "pmt.hideAppendix"/></a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><a href="../ToolsDescAction.do" target="ilexmain">Master Tool Definitions</a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><a href="../RawVendexViewAction.do?hmode=MasterRawVedexView" target="ilexmain">RAW Vendex View</a></td>
   </tr>
   
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><a href="../ViewPOWOPDFAction.do?firsttime=firsttime" target="ilexmain">View PO/WO PDF</a></td>
   </tr>
   
    <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><a href="../SwitchJobStatusAction.do" target="ilexmain">Open Closed Jobs</a></td>
   </tr>
    <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><a href="../InvoiceJobCommissionPO.do" target="ilexmain">Commission PO Search</a></td>
   </tr>
   <tr> 
    <td height="20">&nbsp;</td>
	<td class="link" height="20" ><a href="/Ilex-WS/getTicketRequestPage.html?appendixId=885&userid=${sessionScope.userid}" target="ilexmain">Test Dispatch Request</a></td>
   </tr>
   
 </table>
 <table border="0" cellspacing="1" cellpadding="0" >
    <tr>
    <td class="toplink" height="20" style="padding-left: 10px" ><b>Standards</b></td>
   </tr>
    <tr>
		<td  class="link" style="padding-left: 10px;color: black;" colspan="2">CSS</td>
   </tr>
    <tr> 
		<td class="link" colspan="2" height="20"  style="padding-left: 10px"><a href="../CssStandardsAction.do?hmode=namingConventions" target="ilexmain">Class Naming Convention</a></td>
   </tr>
    <tr> 
		<td class="link" colspan="2" height="20"  style="padding-left: 10px"><a href="../CssStandardsAction.do?hmode=breadcrumbStandards" target="ilexmain">Breadcrumb</a></td>
   </tr>
   <tr> 
		<td class="link" colspan="2" height="20" style="padding-left: 10px"><a href="../CssStandardsAction.do?hmode=workAreaStandards" target="ilexmain">Work Area</a></td>
   </tr>
   <tr> 
		<td class="link" colspan="2" height="20" style="padding-left: 10px"><a href="../CssStandardsAction.do?hmode=pageContentStandards" target="ilexmain">Page Content</a></td>
   </tr>
   <tr> 
		<td class="link" colspan="2" height="20" style="padding-left: 10px"><a href="../CssStandardsAction.do?hmode=tableStandards" target="ilexmain">Tables</a></td>
   </tr>
   <tr> 
		<td class="link" colspan="2" height="20" style="padding-left: 10px"><a href="../CssStandardsAction.do?hmode=formsStandards" target="ilexmain" 	 rel='stylesheet' >Forms</a></td>
   </tr>
    <tr>
		<td class="link" colspan="2" height="20" style="padding-left: 10px"><a href="../CssStandardsAction.do?hmode=commonPageStandards" type="text/css" target="ilexmain" >Common Page Standards</a></td>
   </tr>
   <tr>
		<td class="link" colspan="2" height="20" style="padding-left: 10px"><a href="../CssStandardsAction.do?hmode=responseMessageStandards" type="text/css" target="ilexmain" >ResponseMessageStandards</a></td>
   </tr>
   <tr>
		<td class="link" colspan="2" height="20" style="padding-left: 10px"><a href="../CssStandardsAction.do?hmode=popUpWindowStandards" type="text/css" target="ilexmain" >Pop Up Window Standards</a></td>
   </tr>
   <tr>
		<td class="link" colspan="2" height="20" style="padding-left: 10px"><a href="../CssStandardsAction.do?hmode=implementation" type="text/css" target="ilexmain" >Implementation</a></td>
   </tr>
   
 </table>
</div>
</div>
</body>
</html:html>
