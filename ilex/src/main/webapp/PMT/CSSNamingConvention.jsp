<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel = "stylesheet" href = "styles/style_common.css" type = "text/css">
<title>Insert title here</title>
</head>
<body>
<div>
	<table class="WASTD0001">
		<tr>
			<TD class="PCLAB0001">
				CSS Standards
			</TD>
		</tr>
		<tr>
			<TD class="PCLAB0001">
				Class Naming Conventions
			</TD>
		</tr>	
	</table>
</div>
<div class="WASTD0001 PCLAB0001">
<pre>
<strong>PEDESNNNN</strong>

PE=Page Element
DES=Short descriptive 3 letter abreviations 
NNNN=Unique number

<strong>Page Elements</strong>

BC = breadcrumb
WA = work area
PC = page content
TB = table
FM = forms
MS = messages 
PU = popup window (javascript)

Short Decriptive Abbriviation - 3 letters. Based on the current styles,the 
following abbreviations should be sufficient

Wide = WID
Standard = STD 
Page = PAG
Navigation = NAV
Page Content = PCT
Subsection Header = SSH
Money = MON
Date = DAT
Text = TXT
Button = BUT
Label = LAB
Radio = RAD
Menu = MEN
Error = ERR 
Border = BDR

</pre>
</div>
</body>
</html>