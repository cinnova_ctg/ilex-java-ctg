<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<link rel = "stylesheet" href = "styles/style_common.css" type = "text/css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language = "JavaScript" src = "javascript/ilexGUI.js"></script>
<title>Insert title here</title>
</head>
<body>
<div>
 
<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
				<!-- Menu -->
		<table cellpadding="0" cellspacing="0" border="0"  >
			<tr>
		       	<td id="pop1" height="19" class="BCMEN0001" align="center"><a href="#"  class="BCMEN0002" style="width: 100px"><center>Manage</center></a></td>
				<td id="pop2" height="19" class="BCMEN0001" align="center"><a href="#"  class="BCMEN0002" style="width: 100px"><center>Search</center></a></td>
				<td id="pop6"  class ="BCMEN0001" width="100%">&nbsp;</td>
			</tr>
		</table>
			</td>
		</tr>
		<tr>
			<!-- Bread Crumb -->
			<td background="images/content_head_04.jpg" height="21">
				<div id="BCNAV0001">
				 	<a class="BCNAV0002">Invoices</a>
				 	<a>[msaName]</a>
				 	<a>[AppendixName]</a>
				 	<a><span class="BCPAG0001">Complete Jobs to be Invoiced</span></a>
				</div>
		    </td>
		</tr>
	</table>
</div>
<div>
	<table class="WASTD0001">
		<tr>
			<TD class="PCSTD0001">
				CSS Standards
			</TD>
		</tr>
		<tr>
			<TD class="PCSTD0001">
				Tables
			</TD>
		</tr>	
		
	

		<tr>
			<td>
				<table>
					<tr>
						<TD class=PCSSH0001>
							TBSTD0001
						</TD>
					</tr>
					<tr>
					<td>
					<table border="0" cellpadding="0" cellspacing="1" class="TBSTD0001">
				 		<thead>
							<tr>
								<th rowspan="2">BDM</th>
								<th colspan="4">Expense</th>
								<th rowspan="2">Revenue</th>
								<th colspan="3" >VGPM</th>
								<th rowspan="2">Count</th>
							</tr>
							<tr>
								<th>Pro Forma</th>
								<th >PO</th>
								<th >Corrected</th>
								<th >Total</th>
								<th>Pro Forma</th>
								<th >Actual</th>
								<th >Delta</th>
							</tr>
			  				
						</thead>
					</table>
					</td>
					</tr>
				</table>
			</td>
		</tr>

	<tr>
			<td>
				<table>
					<tr>
						<TD class=PCSSH0001>
							TBSTD0002
						</TD>
					</tr>
					<tr>
					<td>
					<table border="0" cellpadding="0" cellspacing="1" class="TBSTD0002">
				 		<thead>
							<tr>
								<th rowspan="2">BDM</th>
								<th colspan="4">Expense</th>
								<th rowspan="2">Revenue</th>
								<th colspan="3" >VGPM</th>
								<th rowspan="2">Count</th>
							</tr>
							<tr>
								<th>Pro Forma</th>
								<th >PO</th>
								<th >Corrected</th>
								<th >Total</th>
								<th>Pro Forma</th>
								<th >Actual</th>
								<th >Delta</th>
							</tr>
			  				
						</thead>
					</table>
					</td>
					</tr>
				</table>
			</td>
		</tr>

	<tr>
		<td>
		<table>
		<tr>
		<TD class="PCSSH0001">
			TBWID0001
		</TD>
		
	</tr>
		<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="1"  class="TBWID0001">
		  		<thead>
 						<tr>
 							<th colspan="14" align="left" > 2011</th>
 						</tr>
 						<tr>
	  						<th>Name</th>
	  						<th>Jan</th>
	  						<th>Feb</th>
	  						<th>Mar</th>
	  						<th>Apr</th>
	  						<th>May</th>
	  						<th>Jun</th>
	  						<th>Jul</th>
	  						<th>Aug</th>
	  						<th>Sep</th>
	  						<th>Oct</th>
	  						<th>Nov</th>
	  						<th>Dec</th>
  							<th>Total</th>
 						</tr>
		  			</thead>
		  			<tbody>
		  						<tr>
		  									<td class="PCSTDODD0001 PCSTD0003 TBTEXT0001">Carrion, Victor</td>
		  									<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">11</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">20</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">30</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">10</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">10</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">10</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">20</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">40</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">50</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">600</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">30</td>
  											<td   class=" PCSTDODD0001 PCSTD0003 TBMON0001">30</td>
  											<td class="PCSTDODD0001 PCSTD0003 TBMON0001">20</td>
		  						</tr>
		  						<tr>
		  									<td class="PCSTDEVEN0001 PCSTD0003 TBTEXT0001">Carrion, Victor</td>
		  									<td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">11</td>
  											<td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">20</td>
  											<td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">30</td>
  											<td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">10</td>
  											<td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">10</td>
  											<td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">10</td>
  											<td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">20</td>
  											<td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">40</td>
  											<td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">50</td>
  											<td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">600</td>
  											<td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">30</td>
  											<td   class=" PCSTDEVEN0001 PCSTD0003 TBMON0001">30</td>
  											<td class="PCSTDEVEN0001 PCSTD0003 TBMON0001">20</td>
		  						</tr>
		  						<tr>
		  									<td class="PCSTDODD0001 PCSTD0003 TBTEXT0001">Carrion, Victor</td>
		  									<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">11</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">20</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">30</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">10</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">10</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">10</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">20</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">40</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">50</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">600</td>
  											<td   class="PCSTDODD0001 PCSTD0003 TBMON0001">30</td>
  											<td   class=" PCSTDODD0001 PCSTD0003 TBMON0001">30</td>
  											<td class="PCSTDODD0001 PCSTD0003 TBMON0001">20</td>
		  						</tr>
		  						<tr>
		  									<td class="TBTXT0003 PCSTD0003 TBTEXT0001">Total</td>
		  									<td   class="TBTXT0003 PCSTD0003 TBMON0001">11</td>
  											<td   class="TBTXT0003 PCSTD0003 TBMON0001">20</td>
  											<td   class="TBTXT0003 PCSTD0003 TBMON0001">30</td>
  											<td   class="TBTXT0003 PCSTD0003 TBMON0001">10</td>
  											<td   class="TBTXT0003 PCSTD0003 TBMON0001">10</td>
  											<td   class="TBTXT0003 PCSTD0003 TBMON0001">10</td>
  											<td   class="TBTXT0003 PCSTD0003 TBMON0001">20</td>
  											<td   class="TBTXT0003 PCSTD0003 TBMON0001">40</td>
  											<td   class="TBTXT0003 PCSTD0003 TBMON0001">50</td>
  											<td   class="TBTXT0003 PCSTD0003 TBMON0001">600</td>
  											<td   class="TBTXT0003 PCSTD0003 TBMON0001">30</td>
  											<td   class=" TBTXT0003 PCSTD0003 TBMON0001">30</td>
  											<td class="TBTXT0003 PCSTD0003 TBMON0001">20</td>
		  						</tr>
		  			</tbody>
		  		</table>
		  		</td>
		  		</tr>
		  		</table>
		  	</td>
		  </tr>
		  				
	
		

	<tr>
		<td>
		<tr>
		<td>
			<table >	
				<tr>
					<TD class="PCSSH0001">
						TBTXT0001 &nbsp;&nbsp;&nbsp;TBDAT0001&nbsp;&nbsp;&nbsp; TBMON0001
					</TD>
				</tr>
			<tr>
			<td>
			<table border="0" cellpadding="0" cellspacing="0">
		  		<tr>			
					<td   class="PCSTDODD0001 PCSTD0003 TBTXT0001">
						Manual
					</td>
					<td class="PCSTDODD0001 PCSTD0003 TBDAT0001">
						03/09/2011
					</td>
					<td class="PCSTDODD0001 PCSTD0003 TBDAT0001">
						03/09/2011
					</td>
					<td class="PCSTDODD0001 PCSTD0003 TBMON0001">
						$0.00
					</td>
					<td class="PCSTDODD0001 PCSTD0003 TBMON0001">
						$1,122.34
					</td>
					<td   class="PCSTDODD0001  PCSTD0005 TBTXT0001" colspan="1">
											<a href="#">TWTC Cisco</a>
										</td>
										<td  class="PCSTDODD0001 PCSTD0005 TBDAT0001" colspan="1">
				
										&nbsp;&nbsp;<img name = "outscopeactivityflag"  src = "images/oos1.gif" width = "14" height = "11" border = "0" alt = '<bean:message bundle = "IM" key = "im.outscopeflagpresent"/>'/>
									</td>
										<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001"  colspan="1">
											<a href="#"><img name = "icon"  src = "images/icon.gif" width="13" height="13" style="vertical-align: middle;"  border = "0" /></a>
										</td>
					 <td   class="PCSTDODD0001 PCSTD0003 TBMON0001">
						123345
					</td>
					<td   class="PCSTDODD0001 PCSTD0003 TBTXT0001">
						David Kuhr
					</td>
					<td   class="PCSTDODD0001 PCSTD0003 TBTXT0001" >
						Anna Hoskins
					</td>
						 <td class="PCSTDODD0001 PCSTD0003"><a  href="#" > Install Notes</a>, <a href="#">Problem Summary</a></td>		
	 			</tr>
	  			<tr>		
					<td   class="PCSTDEVEN0001 PCSTD0003 TBTXT0001">
						Manual
					</td>
					<td class="PCSTDEVEN0001 PCSTD0003 TBDAT0001">
						03/09/2011
					</td>
					<td class="PCSTDEVEN0001 PCSTD0003 TBDAT0001">
						03/09/2011
					</td>
					<td class="PCSTDEVEN0001 PCSTD0003 TBMON0001">
						$0.00
					</td>
					<td class="PCSTDEVEN0001 PCSTD0003 TBMON0001">
						$1,122.34
					</td>
					<td   class="PCSTDEVEN0001 TBTXT0001" colspan="1" style="border-bottom: 1px solid;  border-color: white ">
											<a href="#">TWTC Cisco</a>
										</td>
										<td  class="PCSTDEVEN0001 TBDAT0001" style="border-bottom: 1px solid; border-color: white " colspan="1">
				
										&nbsp;&nbsp;<img name = "outscopeactivityflag"  src = "images/oos1.gif" width = "14" height = "11" border = "0" alt = '<bean:message bundle = "IM" key = "im.outscopeflagpresent"/>'/>
									</td>
										<td  class="PCSTDEVEN0001 PCSTD0003 TBDAT0001"  colspan="1">
											<a href="#"><img name = "icon"  src = "images/icon.gif" width="13" height="13" style="vertical-align: middle;"  border = "0" /></a>
										</td>
					 <td   class="PCSTDEVEN0001 PCSTD0003 TBMON0001">
						123345
					</td>
					<td   class="PCSTDEVEN0001 PCSTD0003 TBTXT0001">
						David Kuhr
					</td>
					<td   class="PCSTDEVEN0001 PCSTD0003 TBTXT0001" >
						Anna Hoskins
					</td>
						 <td class="PCSTDEVEN0001 PCSTD0003"><a  href="#" > Install Notes</a>, <a href="#">Problem Summary</a></td>		
				</tr>
	  			<tr>		
					<td   class="PCSTDODD0001 PCSTD0003 TBTXT0001">
						Manual
					</td>
					<td class="PCSTDODD0001 PCSTD0003 TBDAT0001">
						03/09/2011
					</td>
					<td class="PCSTDODD0001 PCSTD0003 TBDAT0001">
						03/09/2011
					</td>
					<td class="PCSTDODD0001 PCSTD0003 TBMON0001">
						$0.00
					</td>
					<td class="PCSTDODD0001 PCSTD0003 TBMON0001">
						$1,122.34
					</td>
					<td   class="PCSTDODD0001 TBTXT0001 PCSTD0005" colspan="1" >
											<a href="#">TWTC Cisco</a>
										</td>
										<td  class="PCSTDODD0001 TBDAT0001 PCSTD0005" >
				
										&nbsp;&nbsp;<img name = "outscopeactivityflag"  src = "images/oos1.gif" width = "14" height = "11" border = "0" alt = '<bean:message bundle = "IM" key = "im.outscopeflagpresent"/>'/>
									</td>
										<td  class="PCSTDODD0001 PCSTD0003 TBDAT0001"  colspan="1">
											<a href="#"><img name = "icon"  src = "images/icon.gif" width="13" height="13" style="vertical-align: middle;"  border = "0" /></a>
										</td>
					 <td   class="PCSTDODD0001 PCSTD0003 TBMON0001">
						123345
					</td>
					<td   class="PCSTDODD0001 PCSTD0003 TBTXT0001">
						David Kuhr
					</td>
					<td   class="PCSTDODD0001 PCSTD0003 TBTXT0001" >
						Anna Hoskins
					</td>
						 <td class="PCSTDODD0001 PCSTD0003"><a  href="#" > Install Notes</a>, <a href="#">Problem Summary</a></td>		
				  </tr>
				</table>
				</td>
				</tr>
				</table>
				</td>
			</tr>
		</td>
		</tr>
				<tr>
					<td>
						<table>	  	
					  				<tr>
								    	<td class = "PCSTD0002 MSERR0001"  colspan="3">[ Table Contents never expands 100%.Should size to fit contents.</td>
								    </tr>
								     <tr>
								    	<td class = "PCSTD0002 MSERR0001"  colspan="3"> As deemed necessary,specific sizes will be specified. </td>
								    </tr>
								    <tr>
								    	<td class = "PCSTD0002 MSERR0001"  colspan="3">Text-align Left </td>
								  </tr>
								   <tr>
								    	<td class = "PCSTD0002 MSERR0001"  colspan="3">Dates-align center and format as MM/DD/YYYY </td>
								  </tr>
								   <tr>
								    	<td class = "PCSTD0002 MSERR0001"  colspan="3">Numeric -align right. Use comma delimiter.For money values,Precede with "$" symbols ] </td>
								  </tr>
						  </table>
					  </td>
				 </tr>
					
</table>


</body>
</html>