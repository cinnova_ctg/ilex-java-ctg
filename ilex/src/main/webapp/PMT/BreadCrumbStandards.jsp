<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel = "stylesheet" href = "styles/style_common.css" type = "text/css">
</head>
<body>
	<table class="WASTD0001">
		<tr>
			<TD class="PCLAB0001">
				CSS Standards
			</TD>
		</tr>
		<tr>
			<TD class="PCLAB0001">
				Menu
			</TD>
		</tr>
		<tr><td class="PCSSH0002">&nbsp;</td></tr>
		<tr>
			<TD class="PCSSH0001 PCSSH0002">
					BCMEN0001 &nbsp;&nbsp;&nbsp;
					BCMEN0002 &nbsp;&nbsp;&nbsp;
			</TD>
		</tr>	

		<tr>
			<td>
				<!-- Menu -->
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
				       	<td id="pop1" height="19" class="BCMEN0001" align="center"><a href="#"  class="BCMEN0002" style="width: 100px"><center>Manage</center></a></td>
						<td id="pop2" height="19" class="BCMEN0001" align="center"><a href="#"  class="BCMEN0002" style="width: 100px"><center>Search</center></a></td>
						<td id="pop6" width="100%" class ="BCMEN0001">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td class="PCSSH0002">&nbsp;</td></tr>
			
		<tr>
			<TD class="PCLAB0001">
				Bread Crumb
			</TD>
		</tr>
		<tr><td class="PCSSH0002">&nbsp;</td></tr>
		<tr>
			<TD class="PCSSH0001 PCSSH0002">
					BCNAV0001 &nbsp;&nbsp;&nbsp;
					BCNAV0002 &nbsp;&nbsp;&nbsp;
					BCPAG0001 	
			</TD>
		</tr>
		
		<tr>
			<!-- Bread Crumb -->
			<td background="images/content_head_04.jpg" height="21">
				<div id="BCNAV0001">
				 	<a href="#" class="BCNAV0002">Invoices</a>
				 	<a href="#">[msaName]</a>
				 	<a href="#">[AppendixName]</a>
				 	<a><span class="BCPAG0001">Complete Jobs to be Invoiced</span></a>
				</div>
		    </td>
		</tr>
</table>
</body>
</html>