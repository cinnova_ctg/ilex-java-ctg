<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script>
var contextPath = "<%=request.getContextPath() %>";
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/multiAxisChart.js"></script>
<link href="<%=request.getContextPath() %>/css/style.css" rel="stylesheet" type="text/css" />
<script>
var leftFrameVisible = true;
function toggleLeftMenu () {
	leftFrameVisible = !leftFrameVisible;
	parent.document.getElementById("bodyFrameset").cols = leftFrameVisible ? '200px,*' : '0,*';
	var imgSrc = leftFrameVisible ? "../images/leftArrow.png" : "../images/rightArrow.png";
	$('#toggleImg').attr("src", imgSrc);
}

</script>
</head>
<body style="height: 100%;">
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="position:absolute;bottom:0px;">
		<tr>
			<td align="left" onclick="javascript:toggleLeftMenu();" width="18px"><img id="toggleImg" src="../images/leftArrow.png" height="18px" width="18px" align="middle"></img></td>
			<td width="200px" class="smallGrayLeftAlign" align="left">&nbsp;Version: 1.0.13(1)</td>
			<td class="smallGrayLeftAlign" align="right" style="padding-right: 2px;"><a target="_blank" href="<%=request.getContextPath() %>/docs/Ilex_Reporting_UI_Requirements_v2 0.doc"><font color="#838384">Requirements version: 2.0</font></a></td>
		</tr>
	</table>
</body>
</html>
