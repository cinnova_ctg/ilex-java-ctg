<%@ page language="java" contentType="text/html; charset=iso-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.net.URLEncoder" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Job Audit Analysis - Summary</title>
<style>
    body {
	font-family : Arial, Helvetica, sans-serif;
	font-size : 12px;
	cursor : auto;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
    }
    .columnHeader2 {
		font-size : 9pt;
		font-weight : normal;
		text-align : center;
		vertical-align : middle;
		padding : 3px 10px 3px 10px;
		background-color: #E0E0DC;
    } 
    
    .rptBody01 {
		 padding : 0px 0px 0px 0px;
		 margin : 0px 0px 0px 0px;
    }
    
    .columnHeader1 {
		font-size : 10pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
		background-color: #ddf0ff;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : middle;
		padding : 1px 1px 1px 1px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #fffced;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ffffff;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }    
        
    .toBeScheduled1, .scheduled1, .inwork1, .complete1, .closed1 {
      	font-size : 9pt;
		font-weight : normal;
		padding : 1px 1px 1px 3px;    
    }
    .toBeScheduled1 {
		background-color : #fff1f0; 
    }
    .scheduled1 {
		background-color : #fffced; 
    }
    .inwork1 {
		background-color : #e0edf5; 
    }
    .complete1 {
		background-color : #ffccc9; 
    }
    .closed1 {
		background-color : #e5f9de; 
    }
    
    .toBeScheduled2, .scheduled2, .inwork2, .complete2, .closed2 {
      	font-size : 9pt;
		font-weight : normal;
		padding : 1px 2px 1px 2px;    
    }
    .toBeScheduled2 {
		background-color : #fff1f0; 
    }
    .scheduled2 {
		background-color : #fffced; 
    }
    .inwork2 {
		background-color : #e0edf5; 
    }
    .complete2 {
		background-color : #ffccc9; 
    }
    .closed2 {
		background-color : #e5f9de; 
    }
    
    .inbounds {
        font-weight : Normal;
		padding : 1px 3px 1px 3px;
		text-align : left;
		background-color : #e5f9de;
}

.outbounds {
        font-weight : Normal;
		padding : 1px 3px 1px 3px;
		text-align : left;
		background-color : #ffccc9;
}
 </style>
</head>
<body>
<%
String[] use_class = new String[23];

Integer i_view = (Integer)request.getAttribute("view");

int view = i_view.intValue();


%>

 <table width="1015" border="0" cellspacing="1" cellpadding="0">
  
</table>
<table width="1015" border="0" cellspacing="1" cellpadding="0">
<%

 use_class[0] = "inbounds";
 use_class[1] = "outbounds";
 use_class[2] = "inbounds";
 use_class[3] = "outbounds";
 use_class[4] = "inbounds";
 use_class[5] = "outbounds";
 use_class[6] = "inbounds";
 use_class[7] = "outbounds";
 use_class[8] = "outbounds";
 use_class[9] = "inbounds";
 use_class[10] = "outbounds";
 use_class[11] = "outbounds";
 use_class[12] = "complete1";
 use_class[13] = "closed1";
 
 use_class[19] = "outbounds";
 use_class[20] = "inbounds";
 use_class[21] = "outbounds";
 use_class[22] = "outbounds";
 %>
 <tr>
    <td width="210" class="columnHeader2">Customer</td>
    <td width="210" class="columnHeader2">Project</td>
    <td width="210" class="columnHeader2">Job/Ticket</td>
    <td width="50"  class="columnHeader2">Days</td>
       <%
 if (view >= 20) {%>
	  <td width="110" class="columnHeader2">PO/WO</td>
<%}
 %>
    <td width="59"  class=columnHeader2>Date</td>
    <td width="110" class="columnHeader2">Owner</td>
    <td width="110" class="columnHeader2">PM</td>
  </tr>
 <%
 
 Vector rows = (Vector)request.getAttribute("rows");
 for (int i=0; i<rows.size(); i++) {
	
	 String[] row = (String[])rows.get(i);
 %>
    <tr>
      <td width="210" class="<%=use_class[view]%>"><%=row[0]%></td>
      <td width="210" class="<%=use_class[view]%>"><%=row[1]%></td>
 <%
 if (view > 8 && view < 12) {
	 if (row[7].length() > 0) {
		 row[2] += " <b>("+row[7]+")</b>";
	 }
 }
 %>
      <td width="210" class="<%=use_class[view]%>"><%=row[2]%></td>
      <td width="50"  class="<%=use_class[view]%>"><%=row[3]%></td>
        <%if (view >= 20) {%>
	  <td width="110" style="text-align: center;" class="<%=use_class[view]%>"><a href="/Ilex/POAction.do?hmode=unspecified&poId=<%=row[8]%>&tabId=3&jobId=<%=row[9]%>" target="mainBodyFrame"><%=row[8]%>/<%=row[8]%></a></td>
<%}%>
      <td width="59"  class="<%=use_class[view]%>"><%=row[4]%></td>
      <td width="105"  class="<%=use_class[view]%>"><%=row[5]%></td>
      <td width="115"  class="<%=use_class[view]%>"><%=row[6]%></td>
    </tr>
 <%
 }
 %>
</table>
</body>
</html>