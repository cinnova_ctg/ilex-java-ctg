        // the return will be XML, so lets set up a reader
        reader: new Ext.data.XmlReader({
               // records will have an "job" tag
               record: 'job'
           }, [
                'customer', 'project', 'job_title', 'delta', 'date', 'owner', 'pm' 
           ])
    });

    var cm = new Ext.grid.ColumnModel([
	    {header: "Customer",           width: 160, dataIndex: 'customer'},
		{header: "Project",            width: 145, dataIndex: 'project'},
		{header: "Job",                width: 135, dataIndex: 'job_title'},
		{header: "Delta",              width: 50,  dataIndex: 'delta'},
		{header: "Date",               width: 60,  dataIndex: 'date'},
		{header: "Owner",              width: 105,  dataIndex: 'owner'},
		{header: "PM",                 width: 105,  dataIndex: 'pm'}
	]);