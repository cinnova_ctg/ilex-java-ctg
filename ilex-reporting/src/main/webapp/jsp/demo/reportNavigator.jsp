<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

<%
	String section = "";
	String sectionReport = "";
	String reportTitle = "";

	section = request.getParameter("section");
	sectionReport = request.getParameter("sectionReport");
	reportTitle = request.getParameter("reportTitle");
%>	

</head>

<body>
<table width="100%" border="0" style="background: url(<%=request.getContextPath() %>/images/bggrey.jpg) repeat-x; margin: 0;" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<ul class="dropdown">
			    <li><a href="#">Menu 1&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/white-arrow.gif" border="0"></img></a>
			        <ul class="sub_menu">
			            <li><a href="#">Report Option 1</a></li>
						<li><a href="#">Report Option 1</a></li>
						<li><a href="#">Report Option 1</a></li>
						<li><a href="#">Report Option 1</a></li>
			        </ul>
			    </li>
			    <li><a href="#">Menu 2&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/white-arrow.gif" border="0"></img></a>
			        <ul class="sub_menu">
			            <li><a href="#">Report Another Option 1</a></li>
						<li><a href="#">Report Another Option 1</a></li>
						<li><a href="#">Report Another Option 1</a></li>
						<li><a href="#">Report Another Option 1</a></li>
			        </ul>
			    </li>
			</ul>
		</td>
	</tr>
	<tr>
		<td id="PageContentBreadCrumb">
			<jsp:include page="../BreadCrumb.jsp">
				<jsp:param value="<%=section %>" name="section"/>
				<jsp:param value="<%=sectionReport %>" name="sectionReport"/>
				<jsp:param value="<%=reportTitle %>" name="reportTitle"/>
			</jsp:include>
			
		<%--
		<div class="BreadCrumbText1"><a href="#">Activity Manager</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<a
			href="#">Master Library</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<a href="#">Manage activities</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;Inventory and Report</div></td>
		--%>		
	
	</tr>
</table>
<script>
activateDDMenu();
</script>
</body>
</html>
