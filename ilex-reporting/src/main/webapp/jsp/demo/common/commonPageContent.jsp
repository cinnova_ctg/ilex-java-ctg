<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<body id="bodyId">
<table id="contentTable" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="padding-left: 0px;padding-right: 0px;">
	<tr>
    	<td>
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
      			<tr>
        			<td valign="top" class="pageContentPadding">
        				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 0px;">
          					<tr>
            					<td valign="top" id="PageContentBg" style="padding: 0px;"><div id="pageContentDiv"></div></td>
          					</tr>
          					<tr>
						    	<td id="PageContentDate">Content Date:&nbsp;&nbsp;<span id="currDate"></span></td>
						  	</tr>
        				</table>
        			</td>
  				</tr>
  			</table>
  		</td>
  	</tr>
</table>
</body>
</html>
