<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="<%=request.getContextPath() %>/js/css_browser_selector.js"></script>
<link href="<%=request.getContextPath() %>/css/style.css" rel="stylesheet" type="text/css" />
<style>
body, html {height: 100%}
</style>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
</head>
<body>
<html:form action="FactBoardAction.do">
<table class="factBoard" border="1" cellspacing="0" cellpadding="0" style="border-color: #ccc;">
     <tr>
       <td id="PageContentBg1">
   		<strong>Quick Financial Facts</strong><br />
         <br />
         <span class="BreadCrumbText" style="padding-left: 0px;"><strong>Year to Date</strong></span><br />
         <table width="90%" border="0" cellpadding="3" cellspacing="3">
           <tr>
           <bean:define id="ytdProForma" name="FactBoardForm" property="ytdProForma"  type="java.lang.String"></bean:define>
             <%
             ytdProForma=ytdProForma.substring(0,ytdProForma.indexOf("."));
             %>
             <td class="BreadCrumbText">Pro&nbsp;Forma</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText" align="right">$&nbsp;<%=ytdProForma %></td>
           </tr>
           <tr>
           <bean:define id="ytdExpense" name="FactBoardForm" property="ytdExpense"  type="java.lang.String"></bean:define>
             <%
             ytdExpense=ytdExpense.substring(0,ytdExpense.indexOf("."));
             %>
             <td class="BreadCrumbText">Expense</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText" align="right">$&nbsp;<%=ytdExpense %></td>
           </tr>
           <tr>
           <bean:define id="ytdRevenue" name="FactBoardForm" property="ytdRevenue"  type="java.lang.String"></bean:define>
             <%
             ytdRevenue=ytdRevenue.substring(0,ytdRevenue.indexOf("."));
             %>
             <td class="BreadCrumbText">Revenue</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText" align="right">$&nbsp;<%=ytdRevenue %></td>
           </tr>
           <tr>
             <td class="BreadCrumbText">VGPM</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText"><bean:write name="FactBoardForm" property="ytdVGPM"/></td>
           </tr>
           <tr>
             <td class="BreadCrumbText">VGPM&nbsp;Delta</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText"><bean:write name="FactBoardForm" property="ytdVGPMDelta"/></td>
           </tr>
         </table>
          <br />
         <span class="BreadCrumbText" style="padding-left: 0px;"><strong>Current Month</strong></span><br />
         <table width="90%" border="0" cellpadding="3" cellspacing="3">
           <tr>
           <bean:define id="cmProForma" name="FactBoardForm" property="cmProForma"  type="java.lang.String"></bean:define>
             <%
             cmProForma=cmProForma.substring(0,cmProForma.indexOf("."));
             %>
             <td class="BreadCrumbText">Pro&nbsp;Forma</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText">$&nbsp;<%=cmProForma %></td>
           </tr>
           <tr>
           <bean:define id="cmExpense" name="FactBoardForm" property="cmExpense"  type="java.lang.String"></bean:define>
             <%
             cmExpense=cmExpense.substring(0,cmExpense.indexOf("."));
             %>
             <td class="BreadCrumbText">Expense</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText">$&nbsp;<%=cmExpense %></td>
           </tr>
           <tr>
           <bean:define id="cmRevenue" name="FactBoardForm" property="cmRevenue"  type="java.lang.String"></bean:define>
             <%
             cmRevenue=cmRevenue.substring(0,cmRevenue.indexOf("."));
             %>
             <td class="BreadCrumbText">Revenue</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText">$&nbsp;<%=cmRevenue %></td>
           </tr>
           <tr>
             <td class="BreadCrumbText">VGPM</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText"><bean:write name="FactBoardForm" property="cmVGPM"/></td>
           <tr>
             <td class="BreadCrumbText">VGPM&nbsp;Delta</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText"><bean:write name="FactBoardForm" property="cmVGPMDelta"/></td>
           </tr>
           </tr>
         </table>
          <br />
         <span class="BreadCrumbText" style="padding-left: 0px;"><strong>2012</strong></span><br />
         <table width="90%" border="0" cellpadding="3" cellspacing="3">
           <tr>
           <bean:define id="pytdProFormaExpense" name="FactBoardForm" property="pytdProFormaExpense"  type="java.lang.String"></bean:define>
             <%
             pytdProFormaExpense=pytdProFormaExpense.substring(0,pytdProFormaExpense.indexOf("."));
             %>
             <td class="BreadCrumbText">Pro&nbsp;Forma</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText" align="right">$&nbsp;<%=pytdProFormaExpense %></td>
           </tr>
           <tr>
           <bean:define id="pytdExpense" name="FactBoardForm" property="pytdExpense"  type="java.lang.String"></bean:define>
             <%
             pytdExpense=pytdExpense.substring(0,pytdExpense.indexOf("."));
             %>
             <td class="BreadCrumbText">Expense</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText" align="right">$&nbsp;<%=pytdExpense %></td>
           </tr>
           <tr>
           <bean:define id="pytdRevenue" name="FactBoardForm" property="pytdRevenue"  type="java.lang.String"></bean:define>
             <%
             pytdRevenue=pytdRevenue.substring(0,pytdRevenue.indexOf("."));
             %>
             <td class="BreadCrumbText">Revenue</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText" align="right">$&nbsp;<%=pytdRevenue %></td>
           </tr>
           <tr>
             <td class="BreadCrumbText">VGPM</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText"><bean:write name="FactBoardForm" property="pytdVGPM"/></td>
           </tr>
           <tr>
             <td class="BreadCrumbText">VGPM&nbsp;Delta</td>
             <td class="BreadCrumbText">:</td>
             <td class="BreadCrumbText"><bean:write name="FactBoardForm" property="pytdVGPMDelta"/></td>
           </tr>
         </table>
          <br />
         <strong>Report Options</strong><br /><br />
         <div>
             <table border="0" cellspacing="0" cellpadding="0">
             	<tr>
             		<td class="comboDropdown" onclick="display(); return false;"><a href="#" style="color: #000000;">&nbsp;View all report options</a></td>
             	</tr>
             </table>
          </div>
          <div id="menuList" style="display: none;position: absolute;right: 35px;vertical-align: top;">
          	<jsp:include page="../../jsp/common/appMenu/listMenu.jsp"></jsp:include>
          </div>
           <br />
           <br />
           </td>
     </tr>
</table>
</html:form>
</body>
