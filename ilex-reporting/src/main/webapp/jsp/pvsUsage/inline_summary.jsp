<%@ page language="java" contentType="text/html; charset=iso-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.net.URLEncoder" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>PVS Analysis - Summary</title>
<style>
    body {
	font-family : Arial, Helvetica, sans-serif;
	font-size : 12px;
	cursor : auto;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
    }
    .columnHeader2 {
		font-size : 9pt;
		font-weight : normal;
		text-align : center;
		vertical-align : middle;
		padding : 3px 10px 3px 10px;
		background-color: #E0E0DC;
    } 
    
    .rptBody01 {
		 padding : 0px 0px 0px 0px;
		 margin : 0px 0px 0px 0px;
    }
    
    .columnHeader1 {
		font-size : 10pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 0px 0px 0px 0px;
		background-color: #ddf0ff;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : middle;
		padding : 1px 1px 1px 1px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #fffced;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ffffff;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }    
        
    .toBeScheduled1, .scheduled1, .inwork1, .complete1, .closed1 {
      	font-size : 9pt;
		font-weight : normal;
		padding : 1px 1px 1px 3px;    
    }
    .toBeScheduled1 {
		background-color : #fff1f0; 
    }
    .scheduled1 {
		background-color : #fffced; 
    }
    .inwork1 {
		background-color : #e0edf5; 
    }
    .complete1 {
		background-color : #ffccc9; 
    }
    .closed1 {
		background-color : #e5f9de; 
    }
    
    .toBeScheduled2, .scheduled2, .inwork2, .complete2, .closed2 {
      	font-size : 9pt;
		font-weight : normal;
		padding : 1px 2px 1px 2px;    
    }
    .toBeScheduled2 {
		background-color : #fff1f0; 
    }
    .scheduled2 {
		background-color : #fffced; 
    }
    .inwork2 {
		background-color : #e0edf5; 
    }
    .complete2 {
		background-color : #ffccc9; 
    }
    .closed2 {
		background-color : #e5f9de; 
    }
    
    .inbounds {
        font-weight : Normal;
		padding : 1px 3px 1px 3px;
		text-align : left;
		background-color : #e5f9de;
}

.outbounds {
        font-weight : Normal;
		padding : 1px 3px 1px 3px;
		text-align : left;
		background-color : #ffccc9;
}
 </style>
</head>
<body>

<table width="1015" border="0" cellspacing="1" cellpadding="0">
  
</table>
<table width="1015" border="0" cellspacing="1" cellpadding="0">
<tr>
	<td colspan="3">
    <a href="pvsUsageAction.do?form_action=pvsExport">Export
    <img alt="CSV Export" src="images/csvImage.gif" style="border: none;" />
    </a></td>
</tr>
 <tr>
    <td nowrap="nowrap" class="columnHeader2">Customer</td>
    <td nowrap="nowrap" class="columnHeader2">Project</td>
    <td nowrap="nowrap" class="columnHeader2">Justification</td>
    <td nowrap="nowrap" class="columnHeader2">Core Competency</td>
    <td nowrap="nowrap" class="columnHeader2">Job/Ticket</td>
    <td nowrap="nowrap" class="columnHeader2">PVS</td>
    <td nowrap="nowrap" class="columnHeader2">City</td>
    <td nowrap="nowrap" class="columnHeader2">State</td>
    <td nowrap="nowrap" class="columnHeader2">Zip</td>    
    <td nowrap="nowrap" class=columnHeader2>Date</td>
    <td nowrap="nowrap" class="columnHeader2">Owner</td>
    <td nowrap="nowrap" class="columnHeader2">PM</td>    
  </tr> 
 <%
 
 request.getSession().setAttribute("rows", request.getAttribute("rows"));
 request.getSession().setAttribute("URL", "http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/pvsUsageReports/");
 request.getSession().setAttribute("path", request.getServletContext().getRealPath("/pvsUsageReports")+"/");
 
 Vector rows = (Vector)request.getAttribute("rows"); 
 
 for (int i=0; i<rows.size(); i++) {
	
	 String[] row = (String[])rows.get(i);
 %>
    <tr>
      <td nowrap="nowrap" class="inbounds"><%=row[0]%></td>
      <td nowrap="nowrap" class="inbounds"><%=row[1]%></td>
      <td nowrap="nowrap" class="inbounds"><%=row[2]%></td>
      <td nowrap="nowrap" class="inbounds"><%=row[15]%></td>
      <td nowrap="nowrap" width="50"  class="inbounds">
	      <a href="/Ilex/SiteManage.do?method=Modify&siteID=<%=row[12]%>&appendixid=<%=row[13]%>&ownerId=<%=row[14]%>&siteSearchStatus=A&pageType=jobsites">
	      	<%=row[3]%>
	      </a>
      </td>
      <td nowrap="nowrap" class="inbounds"><a href="/Ilex/Partner_Edit.do?function=Update&formPVSSearch=fromPVSSearch&pid=<%=row[11]%>"><%=row[4]%></a></td>
      <td nowrap="nowrap" class="inbounds"><%=row[5]%></td>
      <td nowrap="nowrap" class="inbounds"><%=row[9]%></td>
      <td nowrap="nowrap" class="inbounds"><%=row[10]%></td>
      <td nowrap="nowrap" class="inbounds"><%=row[6]%></td>
      <td nowrap="nowrap" class="inbounds"><%=row[7]%></td>
      <td nowrap="nowrap" class="inbounds"><%=row[8]%></td>
    </tr>
 <%
 }
 %>
</table>
</body>
</html>