<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="com.ilex.reports.bean.pvsUsage.PVSCoutnBean"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=iso-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/Element.tld" prefix="el"%>
<%@ page import = "java.util.Vector" %>
<%@ page import = "java.net.URLEncoder" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>PVS Usage - Summary</title>

    <%
    
    ArrayList<PVSCoutnBean> justificationCoutnList = (ArrayList<PVSCoutnBean>) request.getAttribute("PVSJustificationCoutnBeanLst");
    ArrayList<PVSCoutnBean> pmCoutnList = (ArrayList<PVSCoutnBean>) request.getAttribute("PVSCoutnBeanListByPM");

    
	String ext_file = "tbs.txt";
	//String ext_url = request.getContextPath()+"/JSA.do?form_action=audit_view&view=1";
	String hidden = "";
	
	String view = (String) request.getAttribute("view");
	
	if (view == null) {
		view = "";
	} else {
		 hidden = "<input type=\"hidden\" name=\"view\", value=\""+ view+ "\">\n";
	}

    if (!view.equals("")) {
    	 // get ext_file and ext_url
    	
    %>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/js/ext-1.0.1/resources/css/ext-all.css" />

    <!-- GC --> 
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/js/ext-1.0.1/resources/css/xtheme-aero.css" />
    
    <!-- LIBS -->
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/ext-1.0.1/adapter/yui/yui-utilities.js"></script>   
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/ext-1.0.1/adapter/yui/ext-yui-adapter.js"></script>   
    <!-- ENDLIBS -->
    
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/ext-1.0.1/ext-all.js"></script>

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/js/ext-1.0.1/examples/grid/grid-examples.css" />
    <%
    }
    %>
    
<script language="JavaScript">
	var tempcal = '';
	var	fixedX = -1			// x position (-1 if to appear below control)
	var	fixedY = -1			// y position (-1 if to appear below control)
	var startAt = 1			// 0 - sunday ; 1 - monday
	var showWeekNumber = 1	// 0 - don't show; 1 - show
	var showToday = 1		// 0 - don't show; 1 - show
	var imgDir = "\\rpt\\images\\"	
	var gotoString = "Go To Current Month"
	var todayString = "Today is"
	var weekString = "WkNo"
	var scrollLeftMessage = "Click to scroll to previous month. Hold mouse button to scroll automatically."
	var scrollRightMessage = "Click to scroll to next month. Hold mouse button to scroll automatically."
	var selectMonthMessage = "Click to select a month."
	var selectYearMessage = "Click to select a year."
	var selectDateMessage = "Select [date] as date." // do not replace [date], it will be replaced by date.

	var	crossobj, crossMonthObj, crossYearObj, monthSelected, yearSelected, dateSelected, omonthSelected, oyearSelected, odateSelected, monthConstructed, yearConstructed, intervalID1, intervalID2, timeoutID1, timeoutID2, ctlToPlaceValue, ctlNow, dateFormat, nStartingYear

	var	bPageLoaded=false
	var	ie=document.all
	var	dom=document.getElementById

	var	ns4=document.layers
	var	today =	new	Date()
	var	dateNow	 = today.getDate()
	var	monthNow = today.getMonth()
	var	yearNow	 = today.getYear()
	var	imgsrc = new Array("drop1.gif","drop2.gif","left1.gif","left2.gif","right1.gif","right2.gif")
	var	img	= new Array()

	var bShow = false;

    /* hides <select> and <applet> objects (for IE only) */
    function hideElement( elmID, overDiv )
    {
      if( ie )
      {
        for( i = 0; i < document.all.tags( elmID ).length; i++ )
        {
          obj = document.all.tags( elmID )[i];
          if( !obj || !obj.offsetParent )
          {
            continue;
          }
      
          // Find the element's offsetTop and offsetLeft relative to the BODY tag.
          objLeft   = obj.offsetLeft;
          objTop    = obj.offsetTop;
          objParent = obj.offsetParent;
          
          while( objParent.tagName.toUpperCase() != "BODY" )
          {
            objLeft  += objParent.offsetLeft;
            objTop   += objParent.offsetTop;
            objParent = objParent.offsetParent;
          }
      
          objHeight = obj.offsetHeight;
          objWidth = obj.offsetWidth;
      
          if(( overDiv.offsetLeft + overDiv.offsetWidth ) <= objLeft );
          else if(( overDiv.offsetTop + overDiv.offsetHeight ) <= objTop );
          else if( overDiv.offsetTop >= ( objTop + objHeight ));
          else if( overDiv.offsetLeft >= ( objLeft + objWidth ));
          else
          {
            obj.style.visibility = "hidden";
          }
        }
      }
    }
     
    /*
    * unhides <select> and <applet> objects (for IE only)
    */
    function showElement( elmID )
    {
      if( ie )
      {
        for( i = 0; i < document.all.tags( elmID ).length; i++ )
        {
          obj = document.all.tags( elmID )[i];
          
          if( !obj || !obj.offsetParent )
          {
            continue;
          }
        
          obj.style.visibility = "";
        }
      }
    }

	function HolidayRec (d, m, y, desc)
	{
		this.d = d
		this.m = m
		this.y = y
		this.desc = desc
	}

	var HolidaysCounter = 0
	var Holidays = new Array()

	function addHoliday (d, m, y, desc)
	{
		Holidays[HolidaysCounter++] = new HolidayRec ( d, m, y, desc )
	}

	if (dom)
	{
		for	(i=0;i<imgsrc.length;i++)
		{
			img[i] = new Image
			img[i].src = imgDir + imgsrc[i]
		}
	// set the table width 
		var datePointer = '4';
		document.write ("<div onclick='bShow=true' id='calendar'	style='z-index:+999;position:absolute;visibility:hidden;'>   <table	width="+((showWeekNumber==1)?180:220)+" style='font-family:arial;font-size:11px;border-width:1;border-style:solid;border-color:#a0a0a0;font-family:arial; font-size:11px}' bgcolor='#ffffff'><tr bgcolor=teal><td><table width='"+((showWeekNumber==1)?200:218)+"'><tr><td style='padding:2px;font-family:arial; font-size:11px;'><font color='#ffffff'><B><span id='caption'></span></B></font></td><td align=right><a href='javascript:clearCalendar();'>	<IMG SRC='"+imgDir+"clear.jpg' WIDTH='15' HEIGHT='13' BORDER='0' ALT='Clear the Date'></a></td><td align=right><a href='javascript:hideCalendar()'><IMG SRC='"+imgDir+"close.gif' WIDTH='15' HEIGHT='13' BORDER='0' ALT='Close the Calendar'>					</a></td></tr></table></td></tr><tr><td style='padding:5px' bgcolor=#ffffff><span id='content'></span></td></tr>")
			
		if (showToday==1)
		{
			document.write ("<tr bgcolor=lightgrey><td style='padding:1px' align=center><span id='lblToday'></span></td></tr>")
		}
			
		document.write ("</table></div><div id='selectMonth' style='z-index:+999;position:absolute;visibility:hidden;'></div><div id='selectYear' style='z-index:+999;position:absolute;visibility:hidden;'></div>");
	}

	var	monthName =	new	Array("January","February","March","April","May","June","July","August","September","October","November","December")
	var	monthName2 = new Array("JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC")
	if (startAt==0)
	{
		dayName = new Array	("Sun","Mon","Tue","Wed","Thu","Fri","Sat")
	}
	else
	{
		dayName = new Array	("Mon","Tue","Wed","Thu","Fri","Sat","Sun")
	}
	var	styleAnchor="text-decoration:none;color:black;"
	var	styleLightBorder="border-style:solid;border-width:1px;border-color:#a0a0a0;"

	function swapImage(srcImg, destImg){
		if (ie)	{ document.getElementById(srcImg).setAttribute("src",imgDir + destImg) }
	}

	
	function init()	{
		if (!ns4)
		{
			if (!ie) { yearNow += 1900	}

			crossobj=(dom)?document.getElementById("calendar").style : ie? document.all.calendar : document.calendar
			hideCalendar()

			crossMonthObj=(dom)?document.getElementById("selectMonth").style : ie? document.all.selectMonth	: document.selectMonth

			crossYearObj=(dom)?document.getElementById("selectYear").style : ie? document.all.selectYear : document.selectYear

			monthConstructed=false;
			yearConstructed=false;

			if (showToday==1)
			{
				document.getElementById("lblToday").innerHTML =	todayString + " <a onmousemove='window.status=\""+gotoString+"\"' onmouseout='window.status=\"\"' title='"+gotoString+"' style='"+styleAnchor+"' href='javascript:monthSelected=monthNow;yearSelected=yearNow;constructCalendar();'>"+dayName[(today.getDay()-startAt==-1)?6:(today.getDay()-startAt)]+", " + dateNow + " " + monthName[monthNow].substring(0,3)	+ "	" +	yearNow	+ "</a>"
			}

			sHTML1="<span id='spanLeft'	style='border-style:solid;border-width:1;border-color:#3366FF;cursor:pointer' onmouseover='swapImage(\"changeLeft\",\"left2.gif\");this.style.borderColor=\"#88AAFF\";window.status=\""+scrollLeftMessage+"\"' onclick='javascript:decMonth()' onmouseout='clearInterval(intervalID1);swapImage(\"changeLeft\",\"left1.gif\");this.style.borderColor=\"#3366FF\";window.status=\"\"' onmousedown='clearTimeout(timeoutID1);timeoutID1=setTimeout(\"StartDecMonth()\",500)'	onmouseup='clearTimeout(timeoutID1);clearInterval(intervalID1)'>&nbsp<IMG id='changeLeft' SRC='"+imgDir+"left1.gif' width=10 height=11 BORDER=0>&nbsp</span>&nbsp;"
			sHTML1+="<span id='spanRight' style='border-style:solid;border-width:1;border-color:#3366FF;cursor:pointer'	onmouseover='swapImage(\"changeRight\",\"right2.gif\");this.style.borderColor=\"#88AAFF\";window.status=\""+scrollRightMessage+"\"' onmouseout='clearInterval(intervalID1);swapImage(\"changeRight\",\"right1.gif\");this.style.borderColor=\"#3366FF\";window.status=\"\"' onclick='incMonth()' onmousedown='clearTimeout(timeoutID1);timeoutID1=setTimeout(\"StartIncMonth()\",500)'	onmouseup='clearTimeout(timeoutID1);clearInterval(intervalID1)'>&nbsp<IMG id='changeRight' SRC='"+imgDir+"right1.gif'	width=10 height=11 BORDER=0>&nbsp</span>&nbsp"
			sHTML1+="<span id='spanMonth' style='border-style:solid;border-width:1;border-color:#3366FF;cursor:pointer'	onmouseover='swapImage(\"changeMonth\",\"drop2.gif\");this.style.borderColor=\"#88AAFF\";window.status=\""+selectMonthMessage+"\"' onmouseout='swapImage(\"changeMonth\",\"drop1.gif\");this.style.borderColor=\"#3366FF\";window.status=\"\"' onclick='popUpMonth()'></span>&nbsp;"
			sHTML1+="<span id='spanYear' style='border-style:solid;border-width:1;border-color:#3366FF;cursor:pointer' onmouseover='swapImage(\"changeYear\",\"drop2.gif\");this.style.borderColor=\"#88AAFF\";window.status=\""+selectYearMessage+"\"'	onmouseout='swapImage(\"changeYear\",\"drop1.gif\");this.style.borderColor=\"#3366FF\";window.status=\"\"'	onclick='popUpYear()'></span>&nbsp;"
			
			document.getElementById("caption").innerHTML  =	sHTML1

			bPageLoaded=true
		}
	}

	function hideCalendar()	{
		crossobj.visibility="hidden"
		if (crossMonthObj != null){crossMonthObj.visibility="hidden"}
		if (crossYearObj !=	null){crossYearObj.visibility="hidden"}

	    showElement( 'SELECT' );
		showElement( 'APPLET' );
	}

	function padZero(num) {
		return (num	< 10)? '0' + num : num ;
	}

	function constructDate(d,m,y)
	{
		sTmp = dateFormat
		sTmp = sTmp.replace	("dd","<e>")
		sTmp = sTmp.replace	("d","<d>")
		sTmp = sTmp.replace	("<e>",padZero(d))
		sTmp = sTmp.replace	("<d>",d)
		sTmp = sTmp.replace	("mmmm","<p>")
		sTmp = sTmp.replace	("mmm","<o>")
		sTmp = sTmp.replace	("mm","<n>")
		sTmp = sTmp.replace	("m","<m>")
		sTmp = sTmp.replace	("<m>",m+1)
		sTmp = sTmp.replace	("<n>",padZero(m+1))
		sTmp = sTmp.replace	("<o>",monthName[m])
		sTmp = sTmp.replace	("<p>",monthName2[m])
		sTmp = sTmp.replace	("yyyy",y)
		return sTmp.replace ("yy",padZero(y%100))
	}

	function closeCalendar() {
		var	sTmp

		hideCalendar();
		ctlToPlaceValue.value =	constructDate(dateSelected,monthSelected,yearSelected)
		ctlToPlaceValue.focus()
	}


	function clearCalendar() {
		var	sTmp

		hideCalendar();
		ctlToPlaceValue.value =	'';
		ctlToPlaceValue.focus();
	}
	/*** Month Pulldown	***/

	function StartDecMonth()
	{
		intervalID1=setInterval("decMonth()",80)
	}

	function StartIncMonth()
	{
		intervalID1=setInterval("incMonth()",80)
	}

	function incMonth () {
		monthSelected++
		if (monthSelected>11) {
			monthSelected=0
			yearSelected++
		}
		constructCalendar()
	}

	function decMonth () {
		monthSelected--
		if (monthSelected<0) {
			monthSelected=11
			yearSelected--
		}
		constructCalendar()
	}

	function constructMonth() {
		popDownYear()
		if (!monthConstructed) {
			sHTML =	""
			for	(i=0; i<12;	i++) {
				sName =	monthName[i];
				if (i==monthSelected){
					sName =	"<B>" +	sName +	"</B>"
				}
				sHTML += "<tr><td id='m" + i + "' onmouseover='this.style.backgroundColor=\"#FFCC99\"' onmouseout='this.style.backgroundColor=\"\"' style='cursor:pointer' onclick='monthConstructed=false;monthSelected=" + i + ";constructCalendar();popDownMonth();event.cancelBubble=true'>&nbsp;" + sName + "&nbsp;</td></tr>"
			}

			document.getElementById("selectMonth").innerHTML = "<table width=70	style='font-family:arial; font-size:11px; border-width:1; border-style:solid; border-color:#a0a0a0;' bgcolor='#FFFFDD' cellspacing=0 onmouseover='clearTimeout(timeoutID1)'	onmouseout='clearTimeout(timeoutID1);timeoutID1=setTimeout(\"popDownMonth()\",100);event.cancelBubble=true'>" +	sHTML +	"</table>"

			monthConstructed=true
		}
	}

	function popUpMonth() {
		constructMonth()
		crossMonthObj.visibility = (dom||ie)? "visible"	: "show"
		crossMonthObj.left = parseInt(crossobj.left) + 50
		crossMonthObj.top =	parseInt(crossobj.top) + 26

		hideElement( 'SELECT', document.getElementById("selectMonth") );
		hideElement( 'APPLET', document.getElementById("selectMonth") );			
	}

	function popDownMonth()	{
		crossMonthObj.visibility= "hidden"
	}

	/*** Year Pulldown ***/

	function incYear() {
		for	(i=0; i<7; i++){
			newYear	= (i+nStartingYear)+1
			if (newYear==yearSelected)
			{ txtYear =	"&nbsp;<B>"	+ newYear +	"</B>&nbsp;" }
			else
			{ txtYear =	"&nbsp;" + newYear + "&nbsp;" }
			document.getElementById("y"+i).innerHTML = txtYear
		}
		nStartingYear ++;
		bShow=true
	}

	function decYear() {
		for	(i=0; i<7; i++){
			newYear	= (i+nStartingYear)-1
			if (newYear==yearSelected)
			{ txtYear =	"&nbsp;<B>"	+ newYear +	"</B>&nbsp;" }
			else
			{ txtYear =	"&nbsp;" + newYear + "&nbsp;" }
			document.getElementById("y"+i).innerHTML = txtYear
		}
		nStartingYear --;
		bShow=true
	}

	function selectYear(nYear) {
		yearSelected=parseInt(nYear+nStartingYear);
		yearConstructed=false;
		constructCalendar();
		popDownYear();
	}

	function constructYear() {
		popDownMonth()
		sHTML =	""
		if (!yearConstructed) {

			sHTML =	"<tr><td align='center'	onmouseover='this.style.backgroundColor=\"#FFCC99\"' onmouseout='clearInterval(intervalID1);this.style.backgroundColor=\"\"' style='cursor:pointer'	onmousedown='clearInterval(intervalID1);intervalID1=setInterval(\"decYear()\",30)' onmouseup='clearInterval(intervalID1)'>-</td></tr>"

			var j =	0
			nStartingYear =	yearSelected-3
			for	(i=(yearSelected-3); i<=(yearSelected+3); i++) {
				sName =	i;
				if (i==yearSelected){
					sName =	"<B>" +	sName +	"</B>"
				}

				sHTML += "<tr><td id='y" + j + "' onmouseover='this.style.backgroundColor=\"#FFCC99\"' onmouseout='this.style.backgroundColor=\"\"' style='cursor:pointer' onclick='selectYear("+j+");event.cancelBubble=true'>&nbsp;" + sName + "&nbsp;</td></tr>"
				j ++;
			}

			sHTML += "<tr><td align='center' onmouseover='this.style.backgroundColor=\"#FFCC99\"' onmouseout='clearInterval(intervalID2);this.style.backgroundColor=\"\"' style='cursor:pointer' onmousedown='clearInterval(intervalID2);intervalID2=setInterval(\"incYear()\",30)'	onmouseup='clearInterval(intervalID2)'>+</td></tr>"

			document.getElementById("selectYear").innerHTML	= "<table width=44 style='font-family:arial; font-size:11px; border-width:1; border-style:solid; border-color:#a0a0a0;'	bgcolor='#FFFFDD' onmouseover='clearTimeout(timeoutID2)' onmouseout='clearTimeout(timeoutID2);timeoutID2=setTimeout(\"popDownYear()\",100)' cellspacing=0>"	+ sHTML	+ "</table>"

			yearConstructed	= true
		}
	}

	function popDownYear() {
		clearInterval(intervalID1)
		clearTimeout(timeoutID1)
		clearInterval(intervalID2)
		clearTimeout(timeoutID2)
		crossYearObj.visibility= "hidden"
	}

	function popUpYear() {
		var	leftOffset

		constructYear()
		crossYearObj.visibility	= (dom||ie)? "visible" : "show"
		leftOffset = parseInt(crossobj.left) + document.getElementById("spanYear").offsetLeft
		if (ie)
		{
			leftOffset += 6
		}
		crossYearObj.left =	leftOffset
		crossYearObj.top = parseInt(crossobj.top) +	26
	}

	/*** calendar ***/
   function WeekNbr(n) {
      // Algorithm used:
      // From Klaus Tondering's Calendar document (The Authority/Guru)
      // hhtp://www.tondering.dk/claus/calendar.html
      // a = (14-month) / 12
      // y = year + 4800 - a
      // m = month + 12a - 3
      // J = day + (153m + 2) / 5 + 365y + y / 4 - y / 100 + y / 400 - 32045
      // d4 = (J + 31741 - (J mod 7)) mod 146097 mod 36524 mod 1461
      // L = d4 / 1460
      // d1 = ((d4 - L) mod 365) + L
      // WeekNumber = d1 / 7 + 1
 
      year = n.getFullYear();
      month = n.getMonth() + 1;
      if (startAt == 0) {
         day = n.getDate() + 1;
      }
      else {
         day = n.getDate();
      }
 
      a = Math.floor((14-month) / 12);
      y = year + 4800 - a;
      m = month + 12 * a - 3;
      b = Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400);
      J = day + Math.floor((153 * m + 2) / 5) + 365 * y + b - 32045;
      d4 = (((J + 31741 - (J % 7)) % 146097) % 36524) % 1461;
      L = Math.floor(d4 / 1460);
      d1 = ((d4 - L) % 365) + L;
      week = Math.floor(d1/7) + 1;
 
      return week;
   }

	function constructCalendar () {
		var aNumDays = Array (31,0,31,30,31,30,31,31,30,31,30,31)

		var dateMessage
		var	startDate =	new	Date (yearSelected,monthSelected,1)
		var endDate

		if (monthSelected==1)
		{
			endDate	= new Date (yearSelected,monthSelected+1,1);
			endDate	= new Date (endDate	- (24*60*60*1000));
			numDaysInMonth = endDate.getDate()
		}
		else
		{
			numDaysInMonth = aNumDays[monthSelected];
		}

		datePointer	= 0
		dayPointer = startDate.getDay() - startAt
		
		if (dayPointer<0)
		{
			dayPointer = 6
		}

		sHTML =	"<table	 border=0 align=center style='font-family:verdana;font-size:10px;'><tr>"

	//	if (showWeekNumber==1)
	//	{
	//		sHTML += "<td width=27><b>" + weekString + "</b></td><td width=1 rowspan=7 bgcolor='#d0d0d0' style='padding:0px'><img src='"+imgDir+"divider.gif' width=1></td>"
	//	}

		for	(i=0; i<7; i++)	{
			sHTML += "<td width='22' align='right'><B>"+ dayName[i]+"</B></td>"
		}
		sHTML +="</tr><tr>"
		
	/*	if (showWeekNumber==1)
		{
			sHTML += "<td align=right>" + WeekNbr(startDate) + "&nbsp;</td>"
		}
*/

		for	( var i=1; i<=dayPointer;i++ )
		{
			sHTML += "<td>&nbsp;</td>"
		}
	
		for	( datePointer=1; datePointer<=numDaysInMonth; datePointer++ )
		{
			dayPointer++;
			sHTML += "<td align=right>"
			sStyle=styleAnchor
			if ((datePointer==odateSelected) &&	(monthSelected==omonthSelected)	&& (yearSelected==oyearSelected))
			{ sStyle+=styleLightBorder }

			sHint = ""
			for (k=0;k<HolidaysCounter;k++)
			{
				if ((parseInt(Holidays[k].d)==datePointer)&&(parseInt(Holidays[k].m)==(monthSelected+1)))
				{
					if ((parseInt(Holidays[k].y)==0)||((parseInt(Holidays[k].y)==yearSelected)&&(parseInt(Holidays[k].y)!=0)))
					{
						sStyle+="background-color:#FFDDDD;"
						sHint+=sHint==""?Holidays[k].desc:"\n"+Holidays[k].desc
					}
				}
			}

			var regexp= /\"/g
			sHint=sHint.replace(regexp,"&quot;")

			dateMessage = "onmousemove='window.status=\""+selectDateMessage.replace("[date]",constructDate(datePointer,monthSelected,yearSelected))+"\"' onmouseout='window.status=\"\"' "

			if ((datePointer==dateNow)&&(monthSelected==monthNow)&&(yearSelected==yearNow))
			{ sHTML += "<b><a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' href='javascript:dateSelected="+datePointer+";closeCalendar();'><font color=#ff0000>&nbsp;" + datePointer + "</font>&nbsp;</a></b>"}
			else if	(dayPointer % 7 == (startAt * -1)+1)
			{ sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' href='javascript:dateSelected="+datePointer + ";closeCalendar();'>&nbsp;<font color=#909090>" + datePointer + "</font>&nbsp;</a>" }
			else
			{ sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' href='javascript:dateSelected="+datePointer + ";closeCalendar();'>&nbsp;" + datePointer + "&nbsp;</a>" }

			sHTML += ""
			if ((dayPointer+startAt) % 7 == startAt) { 
				sHTML += "</tr><tr>" 
				//if ((showWeekNumber==1)&&(datePointer<numDaysInMonth))
				//{
				//	sHTML += "<td align=right>" + (WeekNbr(new Date(yearSelected,monthSelected,datePointer+1))) + "&nbsp;</td>"
				//}
			}
		}

		document.getElementById("content").innerHTML   = sHTML
		document.getElementById("spanMonth").innerHTML = "&nbsp;" +	monthName[monthSelected] + "&nbsp;<IMG id='changeMonth' SRC='"+imgDir+"drop1.gif' WIDTH='12' HEIGHT='10' BORDER=0>"
		document.getElementById("spanYear").innerHTML =	"&nbsp;" + yearSelected	+ "&nbsp;<IMG id='changeYear' SRC='"+imgDir+"drop1.gif' WIDTH='12' HEIGHT='10' BORDER=0>"
	}

	function popUpCalendar(ctl,	ctl2, format) {
		var	leftpos=0
		var	toppos=0
		tempcal = ctl2;
		if (bPageLoaded)
		{
			if ( crossobj.visibility ==	"hidden" ) {
				ctlToPlaceValue	= ctl2
				dateFormat=format;

				formatChar = " "
				aFormat	= dateFormat.split(formatChar)
				if (aFormat.length<3)
				{
					formatChar = "/"
					aFormat	= dateFormat.split(formatChar)
					if (aFormat.length<3)
					{
						formatChar = "."
						aFormat	= dateFormat.split(formatChar)
						if (aFormat.length<3)
						{
							formatChar = "-"
							aFormat	= dateFormat.split(formatChar)
							if (aFormat.length<3)
							{
								// invalid date	format
								formatChar=""
							}
						}
					}
				}

				tokensChanged =	0
				if ( formatChar	!= "" )
				{
					// use user's date
					aData =	ctl2.value.split(formatChar)

					for	(i=0;i<3;i++)
					{
						if ((aFormat[i]=="d") || (aFormat[i]=="dd"))
						{
							dateSelected = parseInt(aData[i], 10)
							tokensChanged ++
						}
						else if	((aFormat[i]=="m") || (aFormat[i]=="mm"))
						{
							monthSelected =	parseInt(aData[i], 10) - 1
							tokensChanged ++
						}
						else if	(aFormat[i]=="yyyy")
						{
							yearSelected = parseInt(aData[i], 10)
							tokensChanged ++
						}
						else if	(aFormat[i]=="mmm")
						{
							for	(j=0; j<12;	j++)
							{
								if (aData[i]==monthName[j])
								{
									monthSelected=j
									tokensChanged ++
								}
							}
						}
						else if	(aFormat[i]=="mmmm")
						{
							for	(j=0; j<12;	j++)
							{
								if (aData[i]==monthName2[j])
								{
									monthSelected=j
									tokensChanged ++
								}
							}
						}
					}
				}

				if ((tokensChanged!=3)||isNaN(dateSelected)||isNaN(monthSelected)||isNaN(yearSelected))
				{
					dateSelected = dateNow
					monthSelected =	monthNow
					yearSelected = yearNow
				}

				odateSelected=dateSelected
				omonthSelected=monthSelected
				oyearSelected=yearSelected

				aTag = ctl
				do {
					aTag = aTag.offsetParent;
					leftpos	+= aTag.offsetLeft;
					toppos += aTag.offsetTop;
				} while(aTag.tagName!="BODY");

				crossobj.left =	fixedX==-1 ? ctl.offsetLeft	+ leftpos :	fixedX
				crossobj.top = fixedY==-1 ?	ctl.offsetTop +	toppos + ctl.offsetHeight +	2 :	fixedY
				constructCalendar (1, monthSelected, yearSelected);
				crossobj.visibility=(dom||ie)? "visible" : "show"

				hideElement( 'SELECT', document.getElementById("calendar") );
				hideElement( 'APPLET', document.getElementById("calendar") );			

				bShow = true;
			}
			else
			{
				hideCalendar()
				if (ctlNow!=ctl) {popUpCalendar(ctl, ctl2, format)}
		}
	
			ctlNow = ctl

		}
	}

	document.onkeypress = function hidecal1 () { 
		if (event.keyCode==27) 
		{

			hideCalendar()
		}
	}
	document.onclick = function hidecal2 () { 		
		if (!bShow)
		{
			hideCalendar()
		}
		bShow = false
	}

	if(ie)
	{
		init()
	}
	else
	{
		window.onload=init
	}
</script>

<script type="text/javascript" language="JavaScript">

var cX = 0; var cY = 0; var rX = 0; var rY = 0;
function UpdateCursorPosition(e){ cX = e.pageX; cY = e.pageY;}
function UpdateCursorPositionDocAll(e){ cX = event.clientX; cY = event.clientY;}
if(document.all) { document.onmousemove = UpdateCursorPositionDocAll; }
else { document.onmousemove = UpdateCursorPosition; }
function AssignPosition(d) {
if(self.pageYOffset) {
	rX = self.pageXOffset;
	rY = self.pageYOffset;
	}
else if(document.documentElement && document.documentElement.scrollTop) {
	rX = document.documentElement.scrollLeft;
	rY = document.documentElement.scrollTop;
	}
else if(document.body) {
	rX = document.body.scrollLeft;
	rY = document.body.scrollTop;
	}
if(document.all) {
	cX += rX; 
	cY += rY;
	}
d.style.left = (cX+10) + "px";
d.style.top = (cY+10) + "px";
}
function HideContent(d) {
if(d.length < 1) { return; }
document.getElementById(d).style.display = "none";
}
function ShowContent(d) {
if(d.length < 1) { return; }
var dd = document.getElementById(d);
AssignPosition(dd);
dd.style.display = "block";
}
function ReverseContentDisplay(d) {
if(d.length < 1) { return; }
var dd = document.getElementById(d);
AssignPosition(dd);
if(dd.style.display == "none") { dd.style.display = "block"; }
else { dd.style.display = "none"; }
}
</script>
<style>
    body {
    	font-family : Arial, Helvetica, sans-serif;
    	font-size : 12px;
    	cursor : auto;
    }
    
    .rptBody01 {
		 padding : 10px 0px 0px 10px;
    }
    
    .toBeScheduled1, .scheduled1, .inwork1, .complete1, .closed1 {
      	font-size : 9pt;
		font-weight : bold;
		padding : 2px 6px 0px 6px;
		text-align : center;
    
    }
    .toBeScheduled1 {
		background-color : #fff1f0; 
    }
    .scheduled1 {
		background-color : #fffced; 
    }
    .inwork1 {
		background-color : #e0edf5; 
    }
    .complete1 {
		background-color : #ffccc9; 
    }
    .closed1 {
		background-color : #e5f9de; 
    }
    
    .toBeScheduled2, .scheduled2, .inwork2, .complete2, .closed2 {
      	font-size : 8pt;
		font-weight : Normal;
		padding : 1px 3px 1px 3px;
		text-align : center;
    
    }
    .toBeScheduled2 {
		background-color : #fff1f0; 
    }
    .scheduled2 {
		background-color : #fffced; 
    }
    .inwork2 {
		background-color : #e0edf5; 
    }
    .complete2 {
		background-color : #ffccc9; 
    }
    .closed2 {
		background-color : #e5f9de; 
    }
    
    
    .menu {
		font-size : 8pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 2px 0px 0px 1px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }
    .date1 {
        font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
    }


    .titleLine {
		font-size : 11pt;
		font-weight : bold;
		text-align : left;
		vertical-align : Middle;
		padding : 0px 0px 0px 0px;
    } 
    
    .subTitleLine {
		font-size : 10pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 0px 0px 0px 0px;
    }
 
    .detailTitleLine {
		font-size : 9pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 0px 0px 5px 0px;
    }
 
     .formLabels {
        font-size : 8pt;
		font-weight : normal;
		padding : 1px 0px 1px 0px;
    }
 
 
    .label1, .label2 {
        font-size : 8pt;
		font-weight : normal;
		text-align : right;
		vertical-align : top;
		padding : 1px 0px 1px 0px;
    }
    .formLabels {
        font-size : 9pt;
		font-weight : normal;
		padding : 0px 0px 0px 0px;
    }
    .elementContainer {
    padding : 0px 2px 0px 2px;
    }
    .columnHeader1 {
		font-size : 9pt;
		font-weight : bold;
		text-align : center;
		vertical-align : middle;
		padding : 3px 10px 3px 10px;
		background-color: #E0E0DC;
    }    
    .columnHeader2 {
		font-size : 9pt;
		font-weight : normal;
		text-align : center;
		vertical-align : middle;
		padding : 3px 10px 3px 10px;
		background-color: #E0E0DC;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0,
    .cDataLeft1, .cDataCenter1, .cDataRight1  {
		font-size : 9pt;
		font-weight : normal;
		text-align : left;
		vertical-align : middle;
		padding : 1px 4px 1px 4px;
    }    
    .cDataLeft0, .cDataCenter0, .cDataRight0 {
		background-color : #fffced;
    }   
    .cDataLeft0{
		text-align : left;
    }   
    .cDataCenter0 {
		text-align : center;
    }   
    .cDataRight0 {
		text-align : right;
    }   
    .cDataLeft1, .cDataCenter1, .cDataRight1 {
		background-color : #ffffff;
    }   
    .cDataLeft1{
		text-align : left;
    }   
    .cDataCenter1 {
		text-align : center;
    }   
    .cDataRight1 {
		text-align : right;
    }        
    .menu {
		font-size : 9pt;
		font-weight : normal;
		vertical-align : bottom;
		padding : 0px 0px 0px 0px;
    }    
       
   .button {
   		font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
		text-align : center;
		width: 26px;
        height: 24px;
    }
    .date1 {
        font-size : 8pt;
		font-weight : normal;
		padding : 0px 1px 0px 1px;
    }
    .bottomLine, .bottomLineCenter {
    	font-size : 8pt;
		font-weight : bold;
		text-align : right;
		vertical-align : middle;
		padding : 2px 2px 2px 0px;
		background-color: #e0edf5;
    }
    .bottomLineCenter {
		text-align : center;
    }
.rptBody01_iframe {
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
}

.ledgendTitle {
        font-size : 9pt;
		font-weight : bold;
		text-align : left;
		vertical-align : top;
		padding : 3px 10px 3px 5px;
}

.inboundsTitle {
        font-size : 9pt;
		font-weight : bold;
		color: Green;
		text-align : left;
		vertical-align : top;
		padding : 3px 10px 3px 10px;
}

.outboundsTitle {
        font-size : 9pt;
		font-weight : bold;
		color: Red;
		text-align : left;
		vertical-align : top;
		padding : 3px 10px 3px 10px;
	
}

.inbounds {
        font-weight : Normal;
		padding : 1px 3px 1px 3px;
		text-align : center;
		background-color : #e5f9de;
	    a:link      { color:black; text-decoration:none; }
        a:visited   { color:black; text-decoration:none; }
        a:hover     { color:black; text-decoration:none;}
        a:active    { color:black; text-decoration:none; }		
}

.outbounds {
        font-weight : Normal;
		padding : 1px 3px 1px 3px;
		text-align : center;
		background-color : #ffccc9;
	    a:link      { color:black; text-decoration:none; }
        a:visited   { color:black; text-decoration:none; }
        a:hover     { color:black; text-decoration:none;}
        a:active    { color:black; text-decoration:none; }		
}

.outboundshelp {
        font-size : 9pt;
		font-weight : bold;
		color: Red;
		text-align : left;
		vertical-align : top;
		padding : 3px 10px 3px 10px;	    
		a:link      { color:black; text-decoration:none; }
        a:visited   { color:black; text-decoration:none; }
        a:hover     { color:black; text-decoration:none;}
        a:active    { color:black; text-decoration:none; }		
}
</style>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>  
<script type="text/javascript">       
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {
	drawChartByVal(90);	
	}
	
	function drawChartByVal(val){
		drawChartByValForPM(val);
		drawChartByValForJustification(val);
		drawBarChart(val);
	}

function drawChartByValForJustification(val) {
	
	var day = "day"+val;
	
	

	var dat = new Array();
	dat.push(['justificationName', 'justificationId']);
<%
	for(int i=0; i< justificationCoutnList.size(); i++){
		PVSCoutnBean bean = justificationCoutnList.get(i);
	if(bean.getJustifcationName() != null){
%>
	if(val==1){//year 
		dat.push(["<%=bean.getJustifcationName()%>", <%=bean.getYearToDate_count()%>]);	
	}else if(val ==7){//7 days
		dat.push(["<%=bean.getJustifcationName()%>", <%=bean.getDays_7_count()%>]);
	}else if(val ==30){
		dat.push(["<%=bean.getJustifcationName()%>", <%=bean.getDays_30_count()%>]);
	}else if(val ==60){
		dat.push(["<%=bean.getJustifcationName()%>", <%=bean.getDays_60_count()%>]);
	}else if(val ==90){
		dat.push(["<%=bean.getJustifcationName()%>", <%=bean.getDays_90_count()%>]);
	}
	<%}
	}%>
	
	dat.sort(sortMultiDimensional);
	
	var data = google.visualization.arrayToDataTable(dat);
	var	titleTxt = 'Justification, '+val +' days';
	if(val==1){
		titleTxt = 'Justification, Year to Date';
	}
	
	var options = {
			title: titleTxt,
			colors: ["#009900", "#3d8da9", "#a2b5e2", "#cc3399", "#6e1e4e", "#410082", "#6699ff", "#A18F50", "#AFBC9C", "#C3EA7A"]
			};
	
	var chart = new google.visualization.PieChart(document.getElementById('justification_chart_div'));
	chart.draw(data, options);

	}
	
function drawChartByValForPM(val) {
	
	var day = "pmday"+val;
	
	
var day = "day"+val;
	
	

	var dat = new Array();
	dat.push(['PMName', 'PMId']);
<%
	for(int i=0; i< pmCoutnList.size(); i++){
		PVSCoutnBean bean = pmCoutnList.get(i);
	if(bean.getPmName() != null){
%>
	if(val==1){//year 
		dat.push(["<%=bean.getPmName()%>", <%=bean.getYearToDate_count()%>]);	
	}else if(val ==7){//7 days
		dat.push(["<%=bean.getPmName()%>", <%=bean.getDays_7_count()%>]);
	}else if(val ==30){
		dat.push(["<%=bean.getPmName()%>", <%=bean.getDays_30_count()%>]);
	}else if(val ==60){
		dat.push(["<%=bean.getPmName()%>", <%=bean.getDays_60_count()%>]);
	}else if(val ==90){
		dat.push(["<%=bean.getPmName()%>", <%=bean.getDays_90_count()%>]);
	}
	<%}
	}%>
	dat.sort(sortMultiDimensional);
	var data = google.visualization.arrayToDataTable(dat);

	var titleTxt = 'Percent of PVS Usage by SrPM, '+val +' days';
	if(val==1){
		titleTxt = 'Percent of PVS Usage by SrPM, Year to Date';	
	}
	
	var options = {title: titleTxt,
			colors: ["#009900", "#3d8da9", "#a2b5e2", "#cc3399", "#6e1e4e", "#410082", "#6699ff", "#A18F50", "#AFBC9C", "#C3EA7A"]			
	};
	var chart = new google.visualization.PieChart(document.getElementById('PM_chart_div'));
	chart.draw(data, options);

	}
	
	function sortMultiDimensional(a,b)
	{
	    // this sorts the array using the second element   
	    return ((a[1] < b[1]) ? -1 : ((a[1] > b[1]) ? 1 : 0));
	}
	
	function drawBarChart(val) {
		
		var day = "pmday"+val;
		var alljob = "allJobDay"+val;

		var dat = new Array();
		dat.push(['SR PM', 'PVS Jobs', 'Total Jobs']);
	<%
		for(int i=0; i< pmCoutnList.size(); i++){
			PVSCoutnBean bean = pmCoutnList.get(i);
		if(bean.getPmName() != null){
	%>
		if(val==1){//year 
			dat.push(["<%=bean.getPmName()%>", <%=bean.getYearToDate_count()%>,<%=bean.getYearToDate_count_all_jobs()%>]);	
		}else if(val ==7){//7 days
			dat.push(["<%=bean.getPmName()%>", <%=bean.getDays_7_count()%>,<%=bean.getDays_7_count_all_jobs()%>]);
		}else if(val ==30){
			dat.push(["<%=bean.getPmName()%>", <%=bean.getDays_30_count()%>,<%=bean.getDays_30_count_all_jobs()%>]);
		}else if(val ==60){
			dat.push(["<%=bean.getPmName()%>", <%=bean.getDays_60_count()%>,<%=bean.getDays_60_count_all_jobs()%>]);
		}else if(val ==90){
			dat.push(["<%=bean.getPmName()%>", <%=bean.getDays_90_count()%>,<%=bean.getDays_90_count_all_jobs()%>]);
		}
		<%}
		}%>
		
		var data = google.visualization.arrayToDataTable(dat);

		var titleTxt = 'PVS Vs Total Jobs By Sr.PM, '+val +' days';
		if(val==1){
			titleTxt = 'PVS Vs Total Jobs By Sr.PM, Year to Date';	
		}
		
		var options = {
				title: titleTxt,
				vAxis: {title: 'Sr.PM',  titleTextStyle: {color: 'Green'}}
		};
		var chart = new google.visualization.BarChart(document.getElementById('PVS_Total_Job_chart_div'));
		chart.draw(data, options);       
	}
</script> 

</head>
<%
	String[] count = (String[]) request.getAttribute("count");

	//Set data list for forms
	String grouping_menu = "select distinct ap_tl_ot_name+'|'+ap_tl_pr_title+'|'+convert(varchar(10),ap_tl_pr_id) from ap_top_level_summary order by ap_tl_ot_name+'|'+ap_tl_pr_title+'|'+convert(varchar(10),ap_tl_pr_id)";
	String bdm_list = "select distinct convert(varchar(12),lo_pc_id)+'|'+lo_pc_first_name+' '+lo_pc_last_name, lo_pc_last_name from lo_poc join lo_poc_rol on lo_pc_id = lo_pr_pc_id and lo_pr_ro_id in (1,2,3,4) where lo_pc_id in (select distinct lx_pr_cns_poc from lx_appendix_main where lx_pr_cns_poc not in (7,32)) order by lo_pc_last_name";
	String pm_list = "select distinct convert(varchar(12),lo_pc_id)+'|'+lo_pc_first_name+' '+lo_pc_last_name, lo_pc_last_name from lo_poc join lo_poc_rol on lo_pc_id = lo_pr_pc_id and lo_pr_ro_id in (5,7,8,12,16,19,20) where lo_pc_id in (select distinct lm_ap_pc_id from lm_appendix_poc where lm_ap_pc_id not in (7,32)) order by lo_pc_last_name";
	String jo_list = "select  distinct convert(varchar(12),lo_pc_id)+'|'+lo_pc_first_name+' '+lo_pc_last_name, lo_pc_last_name from dbo.ap_job_audit_analysis join lo_poc on ap_aa_owner_id = lo_pc_id where (lo_pc_active_user = 1 or lo_pc_termination_date > dateadd(dd, -45, getdate())) and lo_pc_role in (3, 5, 6, 10) order by lo_pc_last_name";

	// Get page type display parameters
	String view_title = (String) request.getAttribute("view_title");

	// Build link and hidden values based on input parameters
	String view_link = "";
	
	// Build view links to include current filter values
    Vector requestParameters = (Vector)request.getAttribute("requestParameters");
    String delimiter = "&";
    String parameter = "";
    String value = "";
    if (requestParameters != null) {
    	for (int x=0; x<requestParameters.size(); x++) {
    		parameter = (String)requestParameters.get(x);
    		if (!parameter.equals("view")) {
        		value = (request.getAttribute(parameter) != null ? (String)request.getAttribute(parameter) : "");
        		if (!value.equals("")) {
        		  view_link += delimiter+parameter+"="+value;
        		}	
    		}
    	}
    }
    
    
	String lx_pr_id = (String)request.getAttribute("lx_pr_id");
	if (lx_pr_id == null) {
		lx_pr_id = "";
	} 

	String bdm = (String) request.getAttribute("bdm");
	if (bdm == null) {
		bdm = "";
	} 

	String pm = (String) request.getAttribute("pm");
	if (pm == null) {
		pm = "";
	}

	String jo = (String) request.getAttribute("jo");
	if (jo == null) {
		jo = "";
	}

	String from_date = (String) request.getAttribute("from_date");
	if (from_date == null) {
		from_date = "";
	} 

	String to_date = (String) request.getAttribute("to_date");
	if (to_date == null) {
		to_date = "";
	} 
	
	String Subtitle = (String) request.getAttribute("Subtitle");
	if (Subtitle == null) {
		Subtitle = "";
	} 
	
%>

<body>
<script type="text/javascript">

function setSubtitle(title) {
  var subtitle01 = document.getElementById("subtitle");
  //subtitle01.innerHTML = title;
  return;
}

</script>
<div class="rptBody01">
<table border="0" cellpadding="0" cellspacing="0">
<form name="form1" action="pvsUsageAction.do" method="post">
<input type="hidden" name="form_action", value="pvsSummary">
<input type="hidden" name="projectStatus", value="<%=request.getParameter("projectStatus")%>">

<tr>
 <td>
 <table width="859" border="0" cellspacing="0" cellpadding="0">
 
 	<%-- <tr>
   <td colspan="2" align="right" class="label1">PVS Justification:&nbsp;&nbsp;<el:element type="menu2" request="<%=request%>" name="bdm" source="database" data ="<%=bdm_list%>" js=""/></td>
   </tr> --%>
   <tr>
   <td rowspan="4" class="titleLine">PVS Usage</td> 
   
   <td align="right" class="label1">Project:&nbsp;&nbsp;<el:element type="groupingmenu1" request="<%=request%>" name="lx_pr_id" source="database" data ="<%=grouping_menu%>" js=""/></td>
   </tr>
   
   <tr>
   <td align="right" class="label1">Business Development Manager:&nbsp;&nbsp;<el:element type="menu2" request="<%=request%>" name="bdm" source="database" data ="<%=bdm_list%>" js=""/></td>
   </tr>
   
   <tr>
   <td align="right" class="label1">Project Manager:&nbsp;&nbsp;<el:element type="menu2" request="<%=request%>" name="pm" source="database" data ="<%=pm_list%>" js=""/></td>
   </tr>

   <tr>
   <td align="right" class="label1">Job Owner:&nbsp;&nbsp;<el:element type="menu2" request="<%=request%>" name="jo" source="database" data ="<%=jo_list%>" js=""/></td>
   </tr>
   
   <tr>
   <td class="subTitleLine"></td><%--Active Jobs: <%=count[18]%> --%> 
   <td align="right" class="label1">
   From:&nbsp;&nbsp;<el:element type="date1" request="<%=request%>" name="from_date" source="" js="" data=""/>
   &nbsp;&nbsp;&nbsp;To:&nbsp;&nbsp;&nbsp;
   <el:element type="date1" request="<%=request%>" name="to_date" source="" js="" data=""/>&nbsp;&nbsp;
   <input type="submit" value="Go" class=date1></td>
   </tr>
 </table> </td>
</tr>
</form>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
  <td><span class="subTitleLine" style="text-align: left;float: left;">
  <%
  String proStatus = request.getParameter("projectStatus");
  if(proStatus.equals("in_work")){
	  proStatus = "In work";
	}else if(proStatus.equals("scheduled")){
		proStatus = "Scheduled";
	}else{
		proStatus = "Complete, Closed";
	}
  %>
  <%= proStatus %> 
  jobs using PVS</span>
  <span style="text-align: left;float: right;">
  <a href="pvsUsageAction.do?form_action=pvsSummary&projectStatus=scheduled" style="color: #0F55CA;">Scheduled</a>
  &nbsp;&nbsp;&nbsp;
  <a href="pvsUsageAction.do?form_action=pvsSummary&projectStatus=in_work" style="color: #0F55CA;">In Work</a>
  &nbsp;&nbsp;&nbsp;
  <a href="pvsUsageAction.do?form_action=pvsSummary&projectStatus=c_closed" style="color: #0F55CA;">Completed & Closed</a>
  </span>
  </td>
  
</tr>

<tr>
  <td><table border="0" cellpadding="0" cellspacing="1" width="859">
    <tr>
	 <td width="30%" class="columnHeader1">Justifications</td>	    
      <td width="12%" class="columnHeader1"><a href="#" onclick="drawChartByVal(7);">Trailling 7</a></td>
      <td width="12%" class="columnHeader1"><a href="#" onclick="drawChartByVal(30);">30 days</a></td>
      <td width="12%" class="columnHeader1"><a href="#" onclick="drawChartByVal(60);">60 days</a></td>
      <td width="12%" class="columnHeader1"><a href="#" onclick="drawChartByVal(90);">90 days</a></td>
      <td width="20%" class="columnHeader1"><a href="#" onclick="drawChartByVal(1);">Year to date</a></td>
    </tr>
    <%    
    for(int i=0; i< justificationCoutnList.size(); i++){
    	PVSCoutnBean bean = justificationCoutnList.get(i);
    if(bean.getJustifcationName() != null){
    %>
    <tr>
      <td class="inbounds" style="text-align: left;" id='<%="jst"+i%>'><%=bean.getJustifcationName()%></td>
      <td class="inbounds">
	      <a id='<%="day7"+i%>' href="pvsUsageAction.do?form_action=inline_audit_view&days=7&projectStatus=<%=request.getParameter("projectStatus")%>&justificationId=<%=bean.getJustifcationId()%>&justificationName=<%=bean.getJustifcationName()%><%=view_link%>" target="i1" onclick="setSubtitle('To Be Scheduled OK');document.getElementById('id1').style.display = 'inline';">
	      	<%=bean.getDays_7_count()%>
	      </a>
      </td>
      <td class="inbounds">
	      <a id='<%="day30"+i%>' href="pvsUsageAction.do?form_action=inline_audit_view&days=30&projectStatus=<%=request.getParameter("projectStatus")%>&justificationId=<%=bean.getJustifcationId()%>&justificationName=<%=bean.getJustifcationName()%><%=view_link%>" target="i1" onclick="setSubtitle('To Be Scheduled Over-1');document.getElementById('id1').style.display = 'inline'">
	      	<%=bean.getDays_30_count()%>
	      </a>
      </td>
      <td class="inbounds">
	      <a id='<%="day60"+i%>' href="pvsUsageAction.do?form_action=inline_audit_view&days=60&projectStatus=<%=request.getParameter("projectStatus")%>&justificationId=<%=bean.getJustifcationId()%>&justificationName=<%=bean.getJustifcationName()%><%=view_link%>" target="i1" onclick="setSubtitle('Scheduled OK');document.getElementById('id1').style.display = 'inline'">
	  	    <%=bean.getDays_60_count()%>
	      </a>
      </td>
      <td class="inbounds">
	      <a id='<%="day90"+i%>' href="pvsUsageAction.do?form_action=inline_audit_view&days=90&projectStatus=<%=request.getParameter("projectStatus")%>&justificationId=<%=bean.getJustifcationId()%>&justificationName=<%=bean.getJustifcationName()%><%=view_link%>" target="i1" onclick="setSubtitle('Scheduled Over-1');document.getElementById('id1').style.display = 'inline'">
	     	 <%=bean.getDays_90_count()%>
	      </a>
      </td>
      <td class="inbounds">
	      <a id='<%="day1"+i%>' href="pvsUsageAction.do?form_action=inline_audit_view&days=365&projectStatus=<%=request.getParameter("projectStatus")%>&justificationId=<%=bean.getJustifcationId()%>&justificationName=<%=bean.getJustifcationName()%><%=view_link%>" target="i1" onclick="setSubtitle('Confirmed');document.getElementById('id1').style.display = 'inline'">
	     	 <%=bean.getYearToDate_count()%>
	      </a>
      </td>
    </tr>
    <% } }%>    
  </table></td>
</tr>

<tr  style="display: none;">
  <td><table border="0" cellpadding="0" cellspacing="1" width="859">
    
    <%    
    for(int i=0; i< pmCoutnList.size(); i++){
    	PVSCoutnBean bean = pmCoutnList.get(i);
    if(bean.getPmName() != null){
    %>
    <tr>
      <td class="inbounds" style="text-align: left;" id='<%="pm"+i%>'><%=bean.getPmName()%></td>
      <td class="inbounds">
	      <a id='<%="pmday7"+i%>' href="#">
	      	<%=bean.getDays_7_count()%>
	      </a>
	      <a id='<%="allJobDay7"+i%>' href="#">
	      	<%=bean.getDays_7_count_all_jobs()%>
	      </a>
      </td>
      <td class="inbounds">
	      <a id='<%="pmday30"+i%>' href="#">
	      	<%=bean.getDays_30_count()%>
	      </a>
	      <a id='<%="allJobDay30"+i%>' href="#">
	      	<%=bean.getDays_30_count_all_jobs()%>
	      </a>
      </td>
      <td class="inbounds">
	      <a id='<%="pmday60"+i%>' href="#">
	  	    <%=bean.getDays_60_count()%>
	      </a>
	      <a id='<%="allJobDay60"+i%>' href="#">
	  	    <%=bean.getDays_60_count_all_jobs()%>
	      </a>
      </td>
      <td class="inbounds">
	      <a id='<%="pmday90"+i%>' href="#">
	     	 <%=bean.getDays_90_count()%>
	      </a>
	      <a id='<%="allJobDay90"+i%>' href="#">
	     	 <%=bean.getDays_90_count_all_jobs()%>
	      </a>
      </td>
      <td class="inbounds">
	      <a id='<%="pmday1"+i%>' href="#">
	     	 <%=bean.getYearToDate_count()%>
	      </a>
	      <a id='<%="allJobDay1"+i%>' href="#">
	     	 <%=bean.getYearToDate_count_all_jobs()%>
	      </a>
      </td>
    </tr>
    <% } }%>    
  </table></td>
</tr>

<tr>
 <td>&nbsp;</td>
</tr>

<tr>
 <td><%-- <div id="subtitle" class="detailTitleLine"></div>--%>&nbsp;</td>
</tr>

<tr>
	<td colspan="6">
 		<div id="PM_chart_div" style="width: 420px; height: 190px;float:left;"></div> 
		<div id="justification_chart_div" style="width: 440px; height: 190px;float:left;"></div>	 
	</td>
</tr>
<tr>
	<td>
		<div id="PVS_Total_Job_chart_div" style="width: 850px; height: 400px;"></div>
	</td>
</tr>


<tr>
  <td><div id="id1" style="display:none">
  <iframe class="	" name="i1" src="#"  width="859" height="450" scrolling="auto" frameborder="0">
  </iframe></div></td>
</tr>
</table>
</div> 
</body>
</html>
