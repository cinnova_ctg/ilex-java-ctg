<%@ page language="java" contentType="text/html; charset=iso-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>
<link href="<%=request.getContextPath()%>/css/tableSorter.css"
	rel="stylesheet" type="text/css" />
<%@ page import="java.text.NumberFormat,java.util.Locale"%>
<%@ page
	import="java.text.NumberFormat,java.util.Locale,java.util.Calendar"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page
	import="com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@ page import="java.util.Map"%>
<%@ page import="com.ilex.report.formbean.helpdesk.FullResolutionBean"%>
<html>
<head>
<title>Down Time</title>

<style>
.titleLine {
	font-size: 12pt;
	font-weight: bold;
	padding: 0 0 6px;
	text-align: left;
	vertical-align: middle;
	width: 200px;
}

.outlineYellow3 {
	background: none repeat scroll 0 0 #FFFFFF;
	border: 2px solid #FAF8CC;
	margin: 4px 4px 4px 0;
	padding: 0;
}

body {
	cursor: auto;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	margin: 15px;
}

.columnHeader2 {
	background-color: #DBE4F1;
	font-size: 9pt;
	font-weight: bold;
	padding: 3px 8px;
	text-align: center;
	vertical-align: middle;
}

.cDataLeft0 {
	text-align: left;
	background-color: #FFFFFF;
	font-size: 9pt;
	font-weight: normal;
	padding: 1px 3px;
	text-align: left;
	vertical-align: middle;
}

.cDataLeft1 {
	text-align: left;
	background-color: #ECECEC;
	font-size: 9pt;
	font-weight: normal;
	padding: 1px 3px;
	text-align: left;
	vertical-align: middle;
}

.FMCNT002 {
	font-size: 16px;
	font-weight: bold;
	padding: 4px 5px 6px;
	text-align: left;
}

.PCLNK001 {
	background-color: #E1E1E1;
	border-color: #878787;
	border-style: solid;
	border-width: 1px;
	padding: 2px 5px 5px;
}

.PCLNK002 {
	padding: 5px;
}
</style>




</head>
<body>

	<table>
		<tbody>
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tbody>
							<tr>
								<td class="titleLine">Down Time Per Carrier</td>
							</tr>
						</tbody>
					</table>

				</td>
			</tr>

			<tr>
				<td>
					<table border="0" cellspacing="1" cellpadding="0"
						class="outlineYellow3">
						<tbody>
							<tr>
								<td class="columnHeader2">Carrier</td>
								<td class="columnHeader2">Total Down Time</td>
								<td class="columnHeader2">Total Devices</td>
								<td class="columnHeader2">Average Down Time/Device</td>
								<td class="columnHeader2">Average Down Time/Device/Day</td>

							</tr>
						</tbody>
						<%
							if (request.getAttribute("DownTimeDataList") == null) {
						%>
						<tr>
							<td colspan="5">Connection issue, Data not found.</td>
						</tr>
						<%
							} else {

							}
							List<String[]> lst = (ArrayList<String[]>) request
									.getAttribute("DownTimeDataList");

							String className = "cDataLeft0";

							for (int i = 0; i < lst.size(); i++) {
								String[] arr = lst.get(i);
								if (i % 2 == 0) {
									className = "cDataLeft0";
								} else {
									className = "cDataLeft1";
								}
						%>
						<tr>
							<td class="<%=className%>"><%=arr[0]%></td>
							<td class="<%=className%>"><%=arr[1]%></td>
							<td class="<%=className%>"><%=arr[2]%></td>
							<td class="<%=className%>"><%=arr[3]%></td>
							<td class="<%=className%>"><%=arr[4]%></td>
						</tr>
						<%
							}
						%>

					</table>
				</td>
			</tr>

		</tbody>
	</table>





</body>

</html>
