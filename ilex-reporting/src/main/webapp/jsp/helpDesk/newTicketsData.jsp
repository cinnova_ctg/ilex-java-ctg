<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<link href="<%=request.getContextPath() %>/css/tableSorter.css" rel="stylesheet" type="text/css" />
<%



ArrayList<String[]> data = (ArrayList<String[]>)request.getAttribute("listdata");

%>



<!DOCTYPE table PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
 
	<style>
	span {font-size:12px; color:grey;}
	</style>

  
  </head>
<body >
<center>
<table class="tablesorter" style="width:80%">
	<tr>
		<th><b>This Week</b></th>
	</tr>
	<tr>
		<td><b><%= request.getAttribute("newTicketsThisWeek") %></b> <br/><span > New Tickets</span></td>
		<td><b><%= request.getAttribute("resolveTicketsThisWeek") %></b> <br/> <span >Tickets Resolved</span></td>
		<td> <b><%= request.getAttribute("percentageThisWeek")%> % </b><br> <span > % Solved</span></td>
	</tr>
	<tr>
		<th><b>Last Week</b></th>
	</tr>
	<tr>
		<td><b><%= request.getAttribute("newTicketsLastWeek") %> </b><br><span > New Tickets</td>
		<td><b><%= request.getAttribute("resolveTicketsLastWeek") %> </b><br/> <span >Tickets Resolved</span></td>
		<td> <b><%= request.getAttribute("percentageLastWeek")%> % </b><br> <span > % Solved</span></td>
	</tr>
	<tr> 
	   <td colspan='3'>
		<div id="chart_div" ></div>
	
	  <script >
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Days', 'New-Tickets', 'Resolved-Tickets']
          <% for(int i=0;i<data.size();i++){
				 String[] st = data.get(i);
				%>
				,['<%= st[0]%>', <%= st[1]%>, <%= st[2]%> ]
				<%
				 
				 
			 }%>
         
        ]);

        var options = {
          title: 'Tickets Creation/Resolution Trend'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  	</td>
	</tr>
</table>
</center>
</body>
</html>
