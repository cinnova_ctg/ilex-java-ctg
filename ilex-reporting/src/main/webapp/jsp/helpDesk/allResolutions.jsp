<%@ page language="java" contentType="text/html; charset=iso-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<link href="<%=request.getContextPath() %>/css/tableSorter.css" rel="stylesheet" type="text/css" />
<%@ page  import="java.text.NumberFormat,java.util.Locale" %>
<%@ page  import="java.text.NumberFormat,java.util.Locale,java.util.Calendar" %>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ page import = "java.util.Map" %>
<%@ page import ="com.ilex.report.formbean.helpdesk.FullResolutionBean" %>
<html>
<head>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<title>Insert title here</title>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	color: #000000;
}
td {
   vertical-align:text-top;
}
.sectionHeader {
  font-size: 16px; font-weight: bold; color: #2E3D80;
  padding: 10px 10px 5px 5px;
}
.subSectionHeader {
  font-size: 12px; font-weight: bold; color: #2E3D80;
  padding: 10px 10px 5px 5px;
}
.FMLBL001 {
  font-size: 11px; 
  font-weight: bold; 
  padding: 2px 5px 2px 5px;
  text-align:left;
}
.FMCNT001 {
  font-size: 10px; 
  font-weight: normal; 
  padding: 2px 5px 2px 5px;
  text-align:left;
}
.FMCNT002 {
  font-size: 16px; 
  font-weight: bold; 
  padding: 4px 5px 6px 5px;
  text-align:left;
}
.TBHDR001 {
  font-size: 10px; font-weight: bold; 
  padding: 2px 5px 2px 5px;
  text-align:center;
}

.TBHDR0012 {
  font-size: 10px; font-weight: bold; 
  padding: 2px 5px 2px 5px;
  text-align:center;
}

.TBCNT001, .TBCNT001R, .TBCNT001C, .TBCNT001RBENCHMARK {
  font-size: 12px;
  padding: 2px 5px 2px 8px;
}
.TBCNT001R {
  text-align:right;
}

.TBCNT002{
font-size: 12px;
  padding: 2px 5px 2px 0px;
}

.TBCNT0012 
{font-size: 12px;
  padding: 2px 5px 2px 8px;
 text-align:Center;
}
.TBCNT001R0 {
 font-size: 12px;
  padding: 2px 5px 2px 8px;
  text-align:center;
}

.TBCNT001C {
  text-align:center;
}
.TBCNT001RBENCHMARK {
  text-align:right;
  padding: 2px 15px 2px 8px;
}

.TBCNT002, .TBCNT002R, .TBCNT002C, .TBCNT002RBENCHMARK {
  font-size: 10px;
  padding: 2px 5px 2px 5px;
  white-space: nowrap;
}
.TBCNT002R {
  text-align:right;
}
.TBCNT002R {
  text-align:right;
}
.TBCNT002C {
  text-align:center;
}
.TBCNT002RBENCHMARK {
  text-align:right;
  padding: 2px 15px 2px 5px;
}
.TBCNT003, .TBCNT003R {
  font-size: 10px;
  text-indent:20px;
  padding: 2px 0px 2px 0px;
}
.TBCNT003R {
  text-align:right;
}
.TBCNT004, .TBCNT004R {
  font-size: 10px;
  padding: 2px 0px 2px 5px;
  white-space: nowrap;
}
.TBCNT004R {
  text-align:right;
}
.TBCNT005, .TBCNT005R, .TBCNT005RBENCHMARK  {
  font-size: 12px;
  padding: 5px 5px 3px 5px;
  text-decoration: underline;
}
.TBCNT005R {
  text-align:right;
}
.TBCNT005RBENCHMARK {
  text-align:right;
  padding: 5px 15px 3px 5px;
}
.TBDIV001 {
  padding: 10px 10px 5px 5px;
}
.TBDIV002 {
  padding: 5px 0px 10px 10px;
}
.PCLNK001 {
  padding: 2px 5px 5px 5px;
  background-color:#E1E1E1;
  border-style:solid; border-width:1px; border-color: #878787;
 }
.PCLNK002 {
  padding: 5px 5px 5px 5px;
}
.TBP {
  
  background-color:#ECEEF0 ;
  
}
.menu {
  font-size: 10px; font-weight: normal; 
  padding: 2px 0px 2px 0px;
  style="border-style:solid; border-width:1px; border-color: #ABABAB;
}
a[title="Raw Data"] {
        color: black;
}

.titleLine {
	font-size: 12pt;
	font-weight: bold;
	padding: 10 10 6px;
	text-align: left;
	vertical-align: middle;
	width: 200px;
}

.FMCNT002 {
	font-size: 16px;
	font-weight: bold;
	padding: 10px 5px 6px;
	text-align: left;
}

.PCLNK001 {
	background-color: #E1E1E1;
	border-color: #878787;
	border-style: solid;
	border-width: 1px;
	padding: 2px 5px 5px;
}

.PCLNK002 {
	padding: 5px;
}

</style>

<style>
#mask {
  position:absolute;
  left:0;
  top:0;
  z-index:9000;
  background-color:#C6D9FD;
  display:none;
}


.PUWIN001 {
  margin : 5px;
  border:3px solid #B2B2B2;
  width : 500px;
  height : 300px;
  position: absolute;
  left: 0px;
  top: 0px;
  display:none;
  z-index:9999;
}

.PUTTL001 {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px; font-weight: bold; color: #2E3D80;
  background-color : #FFFFC4;
  border-top : 1px solid #B2B2B2;
  border-bottom : 1px solid #B2B2B2;
  padding : 4px;
}

.PUCNT001 {
  margin : 5px;
  background-color : #ffffff;
  width : 100%;
  height : 100%;
}

.PUCNTHDR01 {
  font-family: Arial, Helvetica, sans-serif;
  text-align: center;
  font-size: 12px; font-weight: bold; color: #000000;
  background-color : #ECEEF0;
  padding : 4px;
}

</style>




<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript"></script>
<script src="http://jquery-ui.googlecode.com/svn/tags/latest/external/jquery.bgiframe-2.1.2.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/i18n/jquery-ui-i18n.min.js" type="text/javascript"></script>

<script language="JavaScript" src="vgp/FusionCharts.js"></script>
  
</head>
<body>
<html:form action="HDTicketData.do">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<!--  lINKS ADD -->
<%
								String day = "30";
							if(request.getAttribute("days")!=null && !request.getAttribute("days").toString().equals(""))
								{day = (String) request.getAttribute("days");}
								
								%>
	<td class="titleLine">All Resolution Report</td>
							<td class="FMCNT002"><span><a
									class="<% if(day.equals("30")){out.print("PCLNK001");}else{out.print("PCLNK002");} %>"
									href="HDTicketData.do?function=getAllResolutiontData&days=30">30</a></span> <span><a
									class="<% if(day.equals("60")){out.print("PCLNK001");}else{out.print("PCLNK002");} %>"
									href="HDTicketData.do?function=getAllResolutiontData&days=60">60</a></span> <span><a
									class="<% if(day.equals("90")){out.print("PCLNK001");}else{out.print("PCLNK002");} %>"
									href="HDTicketData.do?function=getAllResolutiontData&days=90">90</a></span>
								
									
								</td>





<td colspan="7">
<div class="TBDIV002"><table border="0" cellpadding="0" cellspacing="0" id="c12">
<tr>

<!-- <td class="FMCNT002">&nbsp;&nbsp;<a href="#" class="PCLNK002" onclick(highlightPeriod(30)) tf="30">30</a><a href="#" class="PCLNK002" onclick(highlightPeriod(60) tf="60">60</a><a href="#" class="PCLNK002" onclick(highlightPeriod(90) tf="90">90</a><a href="#" class="PCLNK002" onclick(highlightPeriod(180) tf="180">180</a><a href="#" class="PCLNK002" onclick(highlightPeriod(360) tf="360">360</a></td> -->
</tr>
</table></div>
</td></tr></table>
<table border="0" cellpadding="0" cellspacing="0" style="border-style:solid; border-width:2px; border-color: #ABABAB; padding:10px; margin: 0px 10px 10px 10px;"><tr><td><table border="0" cellpadding="0" cellspacing="0">
<!-- 4 Columns for primary content -->
<tr><td><table>

<!-- Column 1 -->
<tr><td><table width="100%"  border="0" cellpadding="0" cellspacing="0" id="c11"><tr><td class="sectionHeader">Managed Nodes</td><td class="sectionHeader" align="right"></td></tr></table></td></tr>
<tr><td>
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c11" style="width:100%;">
<tr>
 <td class="TBCNT001" nowrap>SNMP Monitors</td>
  <td class="TBCNT001R" width="90" ><%= request.getAttribute("SNMP_Monitors") %></td>
</tr>

<tr>
 <td class="TBCNT001">ICMP/Sub Monitors</td>
  <td class="TBCNT001R" width="90"><%= request.getAttribute("ICMP_Sub_Monitors") %></td>
 
</tr>


<tr>
 <td class="TBCNT001" nowrap>Total Managed Nodes</td>
  <td class="TBCNT001R" width="90"><%= request.getAttribute("Total_Managed_Nodes") %></td>
  
</tr>

</table></div></td></tr>
<tr><td><table width="100%"  border="0" cellpadding="0" cellspacing="0" id="c11"><tr><td class="sectionHeader">Level 1 to Level 2 to Level 3 Tracking</td><td class="sectionHeader" align="right"></td></tr></table></td></tr>
<tr><td>
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c11"  style="width:100%;">
<tr>
 <!--  <td class="TBHDR001" width="100">&nbsp;</td> -->
  <!-- <td class="TBHDR001" width="90">Total</td> -->

</tr>
<tr>
 <td class="TBCNT001" nowrap>Trending # of tickets assigned from Level 1 to Level 2</td>
  <td class="TBCNT001R" width="90" ><%= request.getAttribute("TrendLevel_1_2") %></td>
</tr>

<tr>
 <td class="TBCNT001">Trending # of tickets assigned from Level 2 to Level 3</td>
  <td class="TBCNT001R" width="90"><%= request.getAttribute("TrendLevel_2_3") %></td>
 
</tr>


<tr>
 <td class="TBCNT001" nowrap>Duration of hours tickets assigned from Level 1 to Level 2</td>
  <td class="TBCNT001R" width="90"><%= request.getAttribute("DurationLevel_1_2") %></td>
  
</tr>

<tr>
 <td class="TBCNT001" nowrap>Duration of hours tickets assigned from Level 2 to Level 3 </td>
  <td class="TBCNT001R" width="90"><%= request.getAttribute("DurationLevel_2_3") %></td>

</tr>
</table></div></td></tr>
 
<tr><td><table width="100%" border="0" cellpadding="0" cellspacing="0" id="c11"><tr><td class="sectionHeader"> # of Install Tickets</td></tr></table></td></tr>
<tr><td>
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c12"  style="width:100%;">
<tr><td class="TBCNT001">Average onsite time</td><td class="TBCNT001R" nowrap width="90"><%= request.getAttribute("averageOnSiteTime") %>H</td></tr>
<tr><td class="TBCNT001" class="TBCNT001">First time install completion stats vs revisit install completion stats</td><td class="TBCNT001R" width="90"><%= request.getAttribute("firstTimeInstall") %>/<%= request.getAttribute("revisitInstall") %></td></tr>
</table></div>
</td></tr>
 <tr><td class="sectionHeader">Calls & Tickets Trending Based on Time</td></tr> 
<tr>

<td>
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c13" >
  <tr>
  	<tr>
  
 <tr>
<td class="TBCNT001"># of calls during PPS hours</td>
<td class="TBCNT001R"  width="290"><%= request.getAttribute("callsPps") %></td>
</tr>


<tr>
<td class="TBCNT001" nowrap># of calls during weekdays 6pm-2am </td>
<td class="TBCNT001R"  width="290" > <%= request.getAttribute("callsWeekDays_6pm_2am") %> </td>
</tr>


<tr><td class="TBCNT001" nowrap># of calls during weekdays 2am-8am</td>
<td class="TBCNT001R"  width="290"> <%= request.getAttribute("callsWeekDays_2am_8am") %></td>
</tr>


<tr>
<td class="TBCNT001" nowrap># of tickets opened during PPS hours </td>
<td class="TBCNT001R"  width="290"><%= request.getAttribute("ticketsPps") %></td>
</tr>

<tr>
<td class="TBCNT001" nowrap># of tickets opened weekdays 6pm-2am</td>
<td class="TBCNT001R"  width="290"><%= request.getAttribute("ticketsWeekDays_6pm_2am") %></td>

</tr>


<tr>
<td class="TBCNT001" nowrap># of tickets opened weekdays 2am-8am  </td>
<td class="TBCNT001R"  width="290"><%= request.getAttribute("ticketsWeekDays_2am_8am") %></td>

</tr>


<tr>
<td class="TBCNT001" nowrap># of calls  on weekends </td>
<td class="TBCNT001R"  width="290"><%= request.getAttribute("callweekenddays") %> </td>

</tr>


<tr>
<td class="TBCNT001" nowrap># of Tickets on weekends</td>
<td class="TBCNT001R"  width="290"><%= request.getAttribute("ticketweekenddays") %></td>
</tr> 	
  
<!--   third row -->
<%-- <table border="0" cellpadding="0" cellspacing="0" id="c21">

 <td class="TBHDR001" width="90" ></td>
<tr>

 <td class="TBCNT001" nowrap >First Resolution</td>
  <td class="TBHDR001"></td>
 <td class="TBHDR001"></td>
 <td class="TBHDR001">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

 <td class="TBCNT001R" width="700"  ><span><%= request.getAttribute("firstTimeResolutionThisWeek") %>H</span></td>
<td class="TBCNT001R0"><%= request.getAttribute("firstTimeResolutionThisWeek") %>H</td>
</tr>
<tr>
<td class="TBCNT001" nowrap >Full Resolution</td>
 <td class="TBHDR001"></td>
<td class="TBHDR001"></td>
<td class="TBHDR001"></td>
 
<td class="TBCNT001R" width="700" ><span ><%= request.getAttribute("fullTimeResolutionThisWeek") %>H</span></td>
<td class="TBCNT001R0" ><%= request.getAttribute("fullTimeResolutionThisWeek") %>H</td>
</tr>

</table> --%></td></tr>
<tr>
<!--  strat of squence -->

<tr><td id="c32">
<table border="0" cellpadding="0" cellspacing="0" id="c31">
<tr>
  <td class="sectionHeader" colspan="6">Average Tickets Hours</td>
</tr>

<%
ArrayList<FullResolutionBean> dataAck = (ArrayList<FullResolutionBean>)request.getAttribute("listDataAck");
%>

<tr>
<td colspan='3'>
<div id="chart_divby" ></div>
 <script>
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChartby);
      function drawChartby() {
        var dataweb = google.visualization.arrayToDataTable([
          ['Days', 'Average Tickets Hours']
          <% 
          for (FullResolutionBean Strsplit: dataAck){
     //     for(int i=0;i<dataAck.size();i++){
        	   String total1 = Strsplit.getAveragediff();
				 String total2=Strsplit.getDays();
        //	  	String[] st = dataAck.get(i)[0];
 		//		st[1] = dataAck.get(i)[1];
				// int result=Integer.parseInt(st[1]);
				 %>	
				,['<%= total2  %>', <%=total1%>] 
 				<%
				 
				 
			 }%> 
         
        ]);

        var options = {
                title: 'Average Hours Trend',
                hAxis:{"title":"Days"},
           
                vAxis:{"title":"Hours",baseline:2,baselineColor:'red'},
                
                legend: { position: 'bottom' },height: 500
                ,width: 500
              };

        var chart = new google.visualization.LineChart(document.getElementById('chart_divby'));
        chart.draw(dataweb, options);
      }
          
    </script>
    </td>
</tr>


</table>
</div>
</td></tr>



































<!--  End of squence  -->




<%-- <td colspan='3'>
		<div id="chart_divby" ></div>
	
	  <script >
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChartby);
      function drawChartby() {
        var data = google.visualization.arrayToDataTable([
          ['Days', 'Average Hours']
          <% 
          for (FullResolutionBean Strsplit: dataAck){
     //     for(int i=0;i<dataAck.size();i++){
        	   String total1 = Strsplit.getAveragediff();
				 String total2=Strsplit.getDays();
        //	  	String[] st = dataAck.get(i)[0];
 		//		st[1] = dataAck.get(i)[1];
				// int result=Integer.parseInt(st[1]);
				 %>	
				,['<%= total1  %>', <%=total2%> ] 
 				<%
				 
				 
			 }%> 
					
]);

        var options = {
          title: 'Average Hours Trend',
          legend: { position: 'bottom' },height: 500
          ,width: 500
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_divby'));
        chart.draw(data, options);
      }
    </script>
 </td> --%> 

</tr>



</table></div>


<!-- <tr>
<td>

</td>
</tr> -->
<%-- <tr>
<td colspan='0' class="TBHDR001" nowrap> <span style="float:left;font-size:14px;" >First Resolution & Full Resolution By Year</span>
	  </td>
		<th   align="right"  Style="background-color:#D9DFEF;"><!-- <span style="float:left;font-size:14px;" >First Resolution & Full Resolution By Year</span> -->
  							<span style="float:left; padding-left:13px; padding-top:4px;" ></span Style="float:right;">
  							<p>Year:&nbsp;
<!--   							<SELECT NAME="list"  onchange="submitAll();"> -->
  							
<html:select   styleClass="select" styleId="test"  name="FullResolutionForm" property="year" onchange="submitAll();">
  							</p>	
										  			<% int year=calendar.get(Calendar.YEAR);
										  			String currentyear=Integer.toString(calendar.get(Calendar.YEAR));
										  			
										  			%>
										  			
										  			<%for(int i=year-9;i<=year;++i){
										  			String val=i+"";
										  	         %>
										  			<option  value='<%=val%>'  <% if(val.equals(retyear)) {%> selected='selected' <% }  %>><%=i %></option>
										  			<%} %>
<!-- 	</SELECT> -->
</html:select>
	</th>		
		
	</tr> --%>
   
 <%-- <tr> 
	  
		<div id="chart_divbyyear" ></div>
	
	  <script >
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChartbyyear);
      function drawChartbyyear() {
        var datayear = google.visualization.arrayToDataTable([
          ['Months', 'First Resolution Time', 'Full Resolution Time']
          <% for(int i=0;i<datayear.size();i++){
				 String[] styear = datayear.get(i);
				%>
				,['<%= styear[0]%>', <%= styear[1]%>, <%= styear[2]%> ]
				<%
				 
				 
			 }%>
         
        ]);

        var options = {
          title: 'Tickets First-time/Full Resolution Trend',
          
          height: 500
          ,width: 500,
        	  legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_divbyyear'));
        chart.draw(datayear, options);
      }
    </script>
  	</td>
	</tr> --%>
	
<%-- <tr>
   	<td class="TBCNT001" nowrap>Percentage of first call completion tickets </td>
   	<td class="TBCNT001" > </td>
   	<td class="TBCNT001R" width="230"><span style="float:left; padding-left:13px; padding-top:4px;" ></span Style="float:right;"> <%= request.getAttribute("firstCallcompletionPercentage") %>  </td>
  </tr>
  <tr>	
		<td class="TBCNT001" nowrap>Percentage of first call completion tickets based on PPS </td>
			<td class="TBCNT001" > </td>
		<td class="TBCNT001R"><span style="float:left; padding-left:13px; padding-top:4px;" ></span Style="float:right;"><%= request.getAttribute("firstCallcompletionPps") %> </td>
  </tr>
  <tr>
		<td class="TBCNT001" nowrap>Percentage of first call completion tickets based on Non-PPS </td>
			<td class="TBCNT001" > </td>
		<td class="TBCNT001R"><span style="float:left; padding-left:13px; padding-top:4px;" ></span Style="float:right;"><%= request.getAttribute("firstCallcompletionNonPps") %> </td>
   </tr>
   <tr>
		<td class="TBCNT001" nowrap>Percentage of first call completion tickets based on weekends</td>
			<td class="TBCNT001" > </td>
		<td class="TBCNT001R"><span style="float:left; padding-left:13px; padding-top:4px;" ></span Style="float:right;"><%= request.getAttribute("firstCallWeekendPercentage") %> </td>
   </tr>	 --%>
	    
</table></div>
</td></tr>
</table></td>
<td width="1" bgcolor="#858585"></td>
<td><table>
<!-- Column 2 -->
<tr><td>
<div class="TBDIV001"><table  border="0" cellpadding="0" cellspacing="0" id="c21">
<tr>
<td class="sectionHeader" colspan="2">First & Full Resolution</td>
</tr><!-- </table></td></tr> -->
<!-- <tr>

 <td class="TBHDR001" Nowrap>First Resolution </td>
 <td class="TBHDR001" Nowrap>Full Resolution </td>
 
</tr> -->

<tr>
 <td class="TBHDR001" ></td>
  <td class="TBHDR0012" >First Resolution</td>
 <td class="TBHDR0012">Full Resolution </td>
 
</tr>

<tr>
<td class="TBCNT001">This Week</td>
 <%-- <td class="TBCNT001R" width="90" ><%= request.getAttribute("TrendLevel_1_2") %></td> --%>
<td class="TBCNT0012" ><a href="#" onClick="window.location.href ='HDTicketData.do?function=getThisWeekFirstTimeResRawData&action=thisWeekFirstRes';" title="Raw Data"  ><%= request.getAttribute("firstTimeResolutionThisWeek") %></a></td>
<td class="TBCNT0012" ><a href="#" onClick="window.location.href ='HDTicketData.do?function=getThisWeekFirstTimeResRawData&action=thisWeekFullRes';" title="Raw Data"  ><%= request.getAttribute("fullTimeResolutionThisWeek") %></a></td>
</tr>


<tr>

<td class="TBCNT001">Last Week</td>
<td class="TBCNT0012" ><a href="#" onClick="window.location.href ='HDTicketData.do?function=getThisWeekFirstTimeResRawData&action=lastWeekFirstRes';" title="Raw Data"  ><%= request.getAttribute("firstTimeResolutionLastWeek") %></a></td>
<td class="TBCNT0012" ><a href="#" onClick="window.location.href ='HDTicketData.do?function=getThisWeekFirstTimeResRawData&action=lastWeekFullRes';" title="Raw Data"  ><%= request.getAttribute("fullTimeResolutionLastWeek") %></a></td>

</tr>
<!-- <tr><td class="sectionHeader" nowrap>First & Full Resolution By Current Week   </td></tr> -->
<tr>
<td>
 <div class="TBDIV001"><%-- <table border="0" cellpadding="0" cellspacing="0" id="c21">

 <td class="TBHDR001" width="90" ></td>
<tr>

 <td class="TBCNT001" nowrap >First Resolution</td>
  <td class="TBHDR001"></td>
 <td class="TBHDR001"></td>
 <td class="TBHDR001">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

 <td class="TBCNT001R" width="700"  ><span><%= request.getAttribute("firstTimeResolutionThisWeek") %>H</span></td>
<td class="TBCNT001R0"><%= request.getAttribute("firstTimeResolutionThisWeek") %>H</td>
</tr>
<tr>
<td class="TBCNT001" nowrap >Full Resolution</td>
 <td class="TBHDR001"></td>
<td class="TBHDR001"></td>
<td class="TBHDR001"></td>
 
<td class="TBCNT001R" width="700" ><span ><%= request.getAttribute("fullTimeResolutionThisWeek") %>H</span></td>
<td class="TBCNT001R0" ><%= request.getAttribute("fullTimeResolutionThisWeek") %>H</td>
</tr>

</table> --%></td></tr>
<tr>

<%
ArrayList<String[]> datares = (ArrayList<String[]>)request.getAttribute("listDatares");
%>


<td colspan='3'>
		<div id="chart_div" ></div>
	
	  <script >
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Days', 'First Resolution', 'Full Resolution']
          <% for(int i=0;i<datares.size();i++){
				 String[] st = datares.get(i);
				%>
				,['<%= st[0]%>', <%= st[1]%>, <%= st[2]%> ]
				<%
				 
				 
			 }%>
         
        ]);

        var options = {
          title: 'Tickets First-time/Full Resolution Trend',
          hAxis:{"title":"Days"},
          vAxis:{"title":"Hours"},
          legend: { position: 'bottom' },height: 500
          ,width: 500
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
 </td> 

</tr>



</table></div>
</td></tr>

<tr><td>

<div class="TBDIV001">
<%-- <table width="100%"  border="0" cellpadding="0" cellspacing="0" id="c11">
  <tr>
   <tr>
    <td class="sectionHeader" nowrap>MTTR: Mean Time To Repair</td> <!-- <span class="TBCNT003">(Trailing 30 Days)</span> -->
   <!--  <td class="sectionHeader" align="right">&nbsp;</td> -->
    
  </tr>
    <td class="sectionHeader" align="right">&nbsp;</td>
  </tr>
  
 <tr>
 <td class="TBCNT001" nowrap>Trailing 30 Days</td>
 <td class="TBCNT001" align="right">&nbsp;&nbsp;&nbsp;&nbsp;</td>
 <td class="TBCNT001R" width="410"><%= request.getAttribute("meanTimeLastThrityDays")%></td>

 
</tr> 
  
</table> --%>
<%
String string="0,0,0";
if (!"".equals(request.getAttribute("param") )  &&  request.getAttribute("param")!=null)
 string = (String )request.getAttribute("param");


String[] parts = string.split(",");
 
%>
</td></tr>
<tr><td id="c32">
<table border="0" cellpadding="0" cellspacing="0" id="c31">
<tr>
  <td class="sectionHeader" colspan="6">New Ticket/Resolved Tickets</td>
</tr>

<tr>
 <td class="TBHDR001" ></td>
  <td class="TBHDR001" >New Ticket</td>
 <td class="TBHDR001">Resolved Tickets</td>
 <td class="TBHDR001" nowrap>% Solved </td>
</tr>

<tr>
<td class="TBCNT001" nowrap padding-right>This Week</td>
  	<td class="TBCNT001R0" ><span ><a href="#" onClick="window.location.href ='HDTicketData.do?function=getRawData&action=thisWeekNewTicket';" title="Raw Data"  ><%= request.getAttribute("newTicketsThisWeek") %></a></span></td>
    <td class="TBCNT001R0" ><span><a href="#" onClick="window.location.href ='HDTicketData.do?function=getRawData&action=thisWeekTicketRes';"  title="Raw Data" ><%= request.getAttribute("resolveTicketsThisWeek") %></a></span></td>
    <td class="TBCNT001R0" ><%= request.getAttribute("percentageThisWeek")%>%<br></td>
</tr>

<tr>
 <td class="TBCNT001">Last Week</td>
  	<td class="TBCNT001R0"  ><span ><a href="#" onClick="window.location.href ='HDTicketData.do?function=getRawData&action=lastWeekNewTicket';" title="Raw Data"  ><%= request.getAttribute("newTicketsLastWeek") %></a></span></td>
 	<td class="TBCNT001R0" ><a href="#" onClick="window.location.href ='HDTicketData.do?function=getRawData&action=lastWeekTicketRes';"  title="Raw Data"  ><%= request.getAttribute("resolveTicketsLastWeek") %></a></span></td>
     <td class="TBCNT001R0" ><%= request.getAttribute("percentageLastWeek")%>%</td>
</tr>

<%
ArrayList<String[]> data = (ArrayList<String[]>)request.getAttribute("listdata");

%>

<tr>
<td colspan='3'>
<div id="chart_divbyday" ></div>
 <script>
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChartbyday);
      function drawChartbyday() {
        var data = google.visualization.arrayToDataTable([
          ['Days', 'New-Tickets', 'Resolved-Tickets']
          <% for(int i=0;i<data.size();i++){
				 String[] st = data.get(i);
				%>
				,['<%= st[0]%>', <%= st[1]%>, <%= st[2]%> ]
				<%
				 
				 
			 }%>
         
        ]);

        var options = {
          title: 'Tickets Creation/Resolution Trend',
          hAxis:{"title":"Days"},
          vAxis:{"title":"Tickets"},
          height: 500
          ,width: 500
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_divbyday'));
        chart.draw(data, options);
      }
          
    </script>
    </td>
</tr>


</table>
</div>
</td></tr>

<tr><td id="c32">
<table border="0" cellpadding="0" cellspacing="0" id="c31">

<tr>
  <td class="subSectionHeader" colspan="6"></td>
</tr>





<tr>
 
  
</tr>
</table>
</div>
</td></tr>

<tr><td id="c32">
<table border="0" cellpadding="0" cellspacing="0" id="c31">

<tr>
  
</tr>

<tr>
 
</tr>








</table>
</div>
</td></tr>

</table></td>

<td width="1" bgcolor="#858585"></td>
<td><table>
<!-- Column 3 -->
<tr><td class="sectionHeader" nowrap>MTTR: Mean Time To Repair  By Client</td></tr>
<tr><td>
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c31">

 <%
  ArrayList<String[]> datamttr = (ArrayList<String[]>)request.getAttribute("mttrByClient");
  for(int i=0;i<datamttr.size();i++){
	  String[] st = datamttr.get(i);
  %>
<tr>
  	<td class="TBCNT001" nowrap><%= st[0] %></td>
  	<td class="TBCNT001R" width="90" nowrap><%= st[1] %></td>
  </tr>
 
 <%} %>


<table width="100%"  border="0" cellpadding="0" cellspacing="0" id="c11">
  <tr>
   <tr>
    <td class="sectionHeader" nowrap>MTTR: Mean Time To Repair</td> <!-- <span class="TBCNT003">(Trailing 30 Days)</span> -->
   <!--  <td class="sectionHeader" align="right">&nbsp;</td> -->
    
  <!-- </tr>
    <td class="sectionHeader" align="right">&nbsp;</td>
  </tr> -->
  
 <tr>
 <td class="TBCNT001" nowrap>Trailing 30 Days</td>
 <td class="TBCNT001" align="right">&nbsp;&nbsp;&nbsp;&nbsp;</td>
 <td class="TBCNT001R" width="410"><%= request.getAttribute("meanTimeLastThrityDays")%></td>
</tr> 

<table width="100%"  border="0" cellpadding="0" cellspacing="0" id="c11">
  <tr>
   <tr>
    <td class="sectionHeader" nowrap>Tickets Details</td> <!-- <span class="TBCNT003">(Trailing 30 Days)</span> -->
   <!--  <td class="sectionHeader" align="right">&nbsp;</td> -->
    
  <!-- </tr>
    <td class="sectionHeader" align="right">&nbsp;</td>
  </tr> -->
  
 <tr>
 <td class="TBCNT001" nowrap>Total Closed Tickets</td>
 <td class="TBCNT001" align="right">&nbsp;&nbsp;&nbsp;&nbsp;</td>
 <td class="TBCNT001R" width="410"><%= parts[0] %></td>
 <tr>
	<td class="TBCNT001" nowrap>Total Worked Tickets</td>
 <td class="TBCNT001" align="right">&nbsp;&nbsp;&nbsp;&nbsp;</td>
 <td class="TBCNT001R" width="410"><%= parts[1] %></td>
 </tr>
 <tr>
 <td class="TBCNT001" nowrap>Total Install/Onsite Dispatch Tickets</td>
 <td class="TBCNT001" align="right">&nbsp;&nbsp;&nbsp;&nbsp;</td>
 <td class="TBCNT001R" width="410"><%= parts[2] %></td>
 </tr>
</tr> 
  
</table>

  
</table>










</table></div>  
</td></tr>


<tr><td>



</td></tr>
<tr><td id="c32">


</td></tr>

<!-- Hide old MM/SP/Standard
<tr><td>
<div class="TBDIV001">
<table border="0" cellpadding="0" cellspacing="0" id="c31">
<tr><td class="TBHDR001" width="142">&nbsp;</td><td class="TBHDR001">Selected</td><td class="TBHDR001">Average</td></tr>
<tr><td class="TBCNT001">Minuteman</td><td class="TBCNT001R"><%-- <%=request.getAttribute("minuteman")%></td><td class="TBCNT001R"><%=request.getAttribute("avg_minuteman")%> --%></td></tr>
<tr><td class="TBCNT001">Speedpay</td><td class="TBCNT001R"><%-- <%=request.getAttribute("speedpay")%></td><td class="TBCNT001R"><%=request.getAttribute("avg_speedpay")%> --%></td></tr>
<tr><td class="TBCNT001">Standard</td><td class="TBCNT001R"><%-- <%=request.getAttribute("standard")%></td><td class="TBCNT001R"><%=request.getAttribute("avg_standard")%> --%></td></tr>
</table>
</div>
</td></tr>
-->


<tr><td id="c32">


</td></tr>


</table></td>

<!-- <td width="1" bgcolor="#858585"></td> -->

<td><%-- <table>
<!-- Column 4 -->
<tr><td class="sectionHeader">Top&nbsp;15&nbsp;Most&nbsp;Active</td></tr>
<tr><td id="c41">
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c31">
<tr><td colspan="2" class="TBCNT001">Revenue (000)</td></tr>
<%=request.getAttribute("revenue10")%>
</table></div>
</td></tr>
<tr><td id="c42">
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c32">
<tr><td colspan="2" class="TBCNT001">Jobs</td></tr>
<%=request.getAttribute("count10")%>
</table></div>
</td></tr>
</table> --%></td>
</tr></table></td></tr></table>
<%-- <form method="post" id="form1">
<input type="hidden" name="ac" value="db_summary"/>
<input type="hidden" id="bdm1" name="bdm" value="<%=request.getAttribute("bdm")%>"/>
<input type="hidden" id="spm1" name="spm" value="<%=request.getAttribute("spm")%>"/>
<input type="hidden" id="period1" name="period" value="<%=request.getAttribute("period")%>"/>
</form> --%>
<%-- <form method="post" id="form2">
<input type="hidden" id="spm1" name="spm" value="<%=request.getAttribute("spm")%>"/>
<input type="hidden" id="period1" name="period" value="<%=request.getAttribute("period")%>"/>
<input type="hidden" name="tick" value="<"/>
</form> --%>

<!-- <div class="PUWIN001" id="phonepopup" style="background-color : #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Phone Definitions</td>
 <td class="PUTTL001" align="right"><img src="vgp/delete.gif" class="close-chart" win="phonepopup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001" id="phonepopupdata"></div>
 </td>
</tr>
</table>
</div> -->


<!-- <div class="PUWIN001" id="networkpopup" style="background-color : #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Network Definitions</td>
 <td class="PUTTL001" align="right"><img src="vgp/delete.gif" class="close-chart" win="networkpopup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001" id="networkpopupdata"></div>
 </td>
</tr>
</table>
</div> -->


<!-- 
<div class="PUWIN001" id="installspopup" style="background-color : #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Install Definitions</td>
 <td class="PUTTL001" align="right"><img src="vgp/delete.gif" class="close-chart" win="installspopup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001" id="installspopupdata"></div>
 </td>
</tr>
</table>
</div> -->


<!-- 
<div class="PUWIN001" id="chart001popup" style="background-color : #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Profitability</td>
 <td class="PUTTL001" align="right"><img src="vgp/delete.gif" class="close-chart" win="chart001popup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001" id="chart001popupdata"></div>
 </td>
</tr>
</table>
</div> -->


<!-- <div class="PUWIN001" id="chart002popup" style="background-color : #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Revenue</td>
 <td class="PUTTL001" align="right"><img src="vgp/delete.gif" class="close-chart" win="chart002popup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001"  id="chart002popupdata"></div>
 </td>
</tr>
</table>
</div> -->

<!-- <div class="PUWIN001" id="chart003popup" style="background-color : #ffffff; width:380px;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Minuteman, Speedpay, and PVS Usage</td>
 <td class="PUTTL001" align="center"><img src="vgp/delete.gif" class="close-chart" win="chart003popup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001" id="chart003popupdata"></div>
 </td>
</tr>
</table>
</div> -->

<!-- keyname 
<div>
<iframe src="http://www.contingent.net/admin/forecast_crm_reports.php?data=master_qmt_report&rotation=disabled&slide=5&&width=1300&height=800&key_name=" width="1400" height="900" frameborder="0" />
</div>
-->
</html:form>
</body>

</html>
