<%@page import="java.util.List"%>
<%@page import="com.ilex.reports.bean.helpDesk.HDBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<html>
<head>
<style>
.titleLine {
	font-size: 12pt;
	font-weight: bold;
	padding: 0 0 6px;
	text-align: left;
	vertical-align: middle;
	width: 200px;
}

.outlineYellow3 {
	background: none repeat scroll 0 0 #FFFFFF;
	border: 2px solid #FAF8CC;
	margin: 4px 4px 4px 0;
	padding: 0;
}

body {
	cursor: auto;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	margin: 15px;
}

.columnHeader2 {
	background-color: #DBE4F1;
	font-size: 9pt;
	font-weight: bold;
	padding: 3px 8px;
	text-align: center;
	vertical-align: middle;
}

.cDataLeft0 {
	text-align: left;
	background-color: #FFFFFF;
	font-size: 9pt;
	font-weight: normal;
	padding: 1px 3px;
	text-align: left;
	vertical-align: middle;
}

.cDataLeft1 {
	text-align: left;
	background-color: #ECECEC;
	font-size: 9pt;
	font-weight: normal;
	padding: 1px 3px;
	text-align: left;
	vertical-align: middle;
}

.FMCNT002 {
	font-size: 16px;
	font-weight: bold;
	padding: 4px 5px 6px;
	text-align: left;
}

.PCLNK001 {
	background-color: #E1E1E1;
	border-color: #878787;
	border-style: solid;
	border-width: 1px;
	padding: 2px 5px 5px;
}

.PCLNK002 {
	padding: 5px;
}
</style>
<script type="text/javascript">

// function exportData(){
// 	alert("Called");
// 	window.open("/Reporting/HDTicketData.do?function=getTicketData&days=30"+"&genExport=Export", "_self");
// }


</script>

</head>
<body>


 <%--  <%
        String exportToExcel = request.getParameter("exportToExcel");
        if (exportToExcel != null
                && exportToExcel.toString().equalsIgnoreCase("Export")) {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "inline; filename="
                    + "excel.xls");
 
        }
    %>  --%>


	<table>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tbody>
						<tr>
							<%
								String day = "30";
							if(request.getAttribute("days")!=null && !request.getAttribute("days").toString().equals(""))
								{day = (String) request.getAttribute("days");}
								
								%>
							<td class="titleLine">NOC Action Agent Report</td>
							
							<td class="FMCNT002"><span><a
									class="<% if(day.equals("30")){out.print("PCLNK001");}else{out.print("PCLNK002");} %>"
									href="NocAgentAction.do?function=getAgentData&days=30">30</a></span> <span><a
									class="<% if(day.equals("60")){out.print("PCLNK001");}else{out.print("PCLNK002");} %>"
									href="NocAgentAction.do?function=getAgentData&days=60">60</a></span> <span><a
									class="<% if(day.equals("90")){out.print("PCLNK001");}else{out.print("PCLNK002");} %>"
									href="NocAgentAction.do?function=getAgentData&days=90">90</a></span>
									<!--  hyper link for exporting data in excel -->
									
									<span><a href="NocAgentAction.do?function=getAgentData&genExport=Export&days=<%= day%>">Export</a></span></td>					
									
						</tr>
					</tbody>
				</table>

			</td>
		</tr>

		<tr>
			<td>
				<table border="0" cellspacing="1" cellpadding="0"
					class="outlineYellow3">
					<tbody>
						<tr>
						<!--	<td class="columnHeader2">Client Name</td>-->
							<td class="columnHeader2">User Name</td>
							<td class="columnHeader2">Action Count</td>
							
						<!-- <td class="columnHeader2">Tickets</td> -->
<!-- 							<td class="columnHeader2">Next Action</td> -->
							<td class="columnHeader2">Root Cause</td>
							<td class="columnHeader2">Configuration Items</td>
							<td class="columnHeader2">Category</td>
							<td class="columnHeader2">Urgency</td>
						</tr>


						<%
							List<HDBean> hdBeanList = (List<HDBean>) request
									.getAttribute("HDBeanList");

							for (int i = 0; i < hdBeanList.size(); i++) {
								HDBean bean = hdBeanList.get(i);
								if (i % 2 == 0) {
						%>
						<tr>
							<td class="cDataLeft0"><%=bean.getName()%></td>
							<td class="cDataLeft0"><%=bean.getUpdatecount()%></td>
							<%-- <td class="cDataLeft0" style="text-align: center;"><%=bean.getTicketid()%></td> --%>
<%-- 							<td class="cDataLeft0"><%=bean.getNextAction()%></td> --%>
					
							<td class="cDataLeft0"><%=bean.getRootCause()%></td>
							<td class="cDataLeft0"><%=bean.getConfigItem()%></td>
							<td class="cDataLeft0"><%=bean.getCategory()%></td>
							<td class="cDataLeft0"><%=bean.getUrgency()%></td>
						</tr>
						<%
							} else {
						%>

						<tr>
							<td class="cDataLeft1"><%=bean.getName()%></td>
							<td class="cDataLeft1"><%=bean.getUpdatecount()%></td>
						<%-- 	<td class="cDataLeft1" style="text-align: center;"><%=bean.getTicketid()%></td> --%>
<%-- 							<td class="cDataLeft1"><%=bean.getNextAction()%></td> --%>
							
							<td class="cDataLeft1"><%=bean.getRootCause()%></td>
							<td class="cDataLeft1"><%=bean.getConfigItem()%></td>
							<td class="cDataLeft1"><%=bean.getCategory()%></td>
							<td class="cDataLeft1"><%=bean.getUrgency()%></td>
						</tr>


						<%
							}
							}
						%>
					</tbody>
				</table>
			</td>
		</tr>

	</table>

</body>
</html>