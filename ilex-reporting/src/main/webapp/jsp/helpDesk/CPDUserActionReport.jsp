<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<link href="<%=request.getContextPath() %>/css/tableSorter.css" rel="stylesheet" type="text/css" />

<!DOCTYPE table PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
 <style>
.titleLine {
	font-size: 12pt;
	font-weight: bold;
	padding: 0 0 6px;
	text-align: left;
	vertical-align: middle;
	width: 200px;
}

.outlineYellow3 {
	background: none repeat scroll 0 0 #FFFFFF;
	border: 2px solid #FAF8CC;
	margin: 4px 4px 4px 0;
	padding: 0;
}

body {
	cursor: auto;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	margin: 15px;
}

.columnHeader2 {
	background-color: #DBE4F1;
	font-size: 9pt;
	font-weight: bold;
	padding: 3px 8px;
	text-align: center;
	vertical-align: middle;
}

.cDataLeft0 {
	text-align: left;
	background-color: #FFFFFF;
	font-size: 9pt;
	font-weight: normal;
	padding: 1px 3px;
	text-align: left;
	vertical-align: middle;
}

.cDataLeft1 {
	text-align: left;
	background-color: #ECECEC;
	font-size: 9pt;
	font-weight: normal;
	padding: 1px 3px;
	text-align: left;
	vertical-align: middle;
}

.FMCNT002 {
	font-size: 16px;
	font-weight: bold;
	padding: 4px 5px 6px;
	text-align: left;
}

.PCLNK001 {
	background-color: #E1E1E1;
	border-color: #878787;
	border-style: solid;
	border-width: 1px;
	padding: 2px 5px 5px;
}

.PCLNK002 {
	padding: 5px;
}
</style>
 
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
 
 	<style>
	span {font-size:12px; color:grey;}
	</style>
  
  </head>
<body >

<!-- <table class="tablesorter" style="width:60%;margin-left:110px;"> -->

<!--   <tr> -->
<!-- 	<th><b>PVS Actions Taken</b></th> -->
<!--   </tr> -->
<table  border="0" cellspacing="0" cellpadding="2">
<tbody>  
  <tr>
							<%
								String day = "30";
							if(request.getAttribute("days")!=null && !request.getAttribute("days").toString().equals(""))
								{day = (String) request.getAttribute("days");}
								
								%>
							
								
							<td class="titleLine"><b>CPD User Action Report</b></td>
							
							<td class="FMCNT002"><span><a
									class="<% if(day.equals("30")){out.print("PCLNK002");}else{out.print("PCLNK002");} %>"
									href="CPDUserAction.do?function=getAllData&days=30">30</a></span> <span><a
									class="<% if(day.equals("60")){out.print("PCLNK001");}else{out.print("PCLNK002");} %>"
									href="CPDUserAction.do?function=getAllData&days=60">60</a></span> <span><a
									class="<% if(day.equals("90")){out.print("PCLNK001");}else{out.print("PCLNK002");} %>"
									href="CPDUserAction.do?function=getAllData&days=90">90</a></span>
						   </td>					
									
						</tr>
 </tbody>
 </table>
 <table class="tablesorter" padding="0px" style="margin:6px 0px 0px 0px;padding:0px"  > 
  
  <tr> 
 	<th><b>PVS Actions Taken</b></th> 
  </tr>
  
  <tr>
   	<td>Category</td>
  <%
  ArrayList<String[]> data = (ArrayList<String[]>)request.getAttribute("cpduserpartner");
  for(int i=0;i<data.size();i++){
	  String[] st = data.get(i);
  %>
 		
 	<td><%= st[0] %></td>
  
 
  
  <%} %> 
   	
   	

   	
  <!--  	<td>Anna Reeves</td>
   	<td>Janessa Sambola</td>
   	<td>Zulma Gunther</td> -->
  </tr>
  
  
  
  <tr>
   	<td>Partner Profile Updates</td>
   <%
  ArrayList<String> profiledata = (ArrayList)request.getAttribute("noc_lists");
  
   
 
 	
     
  for(int i=0;i<profiledata.size();i++){
	  
 	  String stprofile = profiledata.get(i).split(",")[0];
	  
  %>
  	
  	
  	<td><%= stprofile %></td>

  <%
  } 
  
  %>
    
  </tr>
  <tr>
   	<td>Documents Approved</td>
   	<%
  ArrayList<String> countfileappr = (ArrayList)request.getAttribute("fileapproved");
  
   
 
 	
     
  for(int i=0;i<countfileappr.size();i++){
	  
 	  String stapproved = countfileappr.get(i).split(",")[0];	
	  
  %>
  	
  	
  	<td><%= stapproved %></td>

  <%
  } 
  
  %>
   
  </tr>
  <tr>
   	<td>ID Badges Approved</td>
   	 <%
  ArrayList<String> countbadgedata = (ArrayList)request.getAttribute("countbadgeIdList");
  
   	 for(int i=0;i<countbadgedata.size();i++){
// 	  String webdata;
// 	  webdata=countbadgedata.get(i).split(",")[0];
	  
	  
	  String stcount = countbadgedata.get(i).split(",")[0];
  %>
 
  	<td><%= stcount %></td>
  
  <%} %> 
   	
 
   
  </tr>	
  <tr>
   	<td>Incident Report Filed </td>
   <%-- 	<td> <%= request.getAttribute("DurationLevel_2_3") %>  </td> --%>
   	<%
  ArrayList<String> incidentreport = (ArrayList)request.getAttribute("incident_lists");
  for(int i=0;i<incidentreport.size();i++){
	  String stcountreport = incidentreport.get(i).split(",")[0];
  %>
  
  	<td><%= stcountreport %></td>
 
  <%} %>
   
   
  </tr>
  
  <tr>
   	<td>Partner Status Changed</td>
   <%
  ArrayList<String> countstatus = (ArrayList)request.getAttribute("Statuschange");
  
   
 
 	
     
  for(int i=0;i<countstatus.size();i++){
	  
 	  String ststatus = countstatus.get(i).split(",")[0];
	  
  %>
  	
  	
  	<td><%= ststatus %></td>

  <%
  } 
  
  %>
   	
  </tr>
  
  
  <tr>
  	<th>&nbsp;</th>
  <tr>
  <tr>
	<th><b>Recruiting</b></th>
  </tr>	
  
  <tr>
   	<td>Category</td>
   	<td>Domestic</td>
   	<td>International</td>
   
  </tr>
  
  
  
  <tr>
   	<td>Minuteman </td>
   	<%
  String domesticminute = (String)request.getAttribute("Domestic");
     %>
  	 
  	
  	<td><%= domesticminute %></td>
  
<%--   <%} %> --%>
  
  	<%
  String Internationalpartner = (String)request.getAttribute("International");
  	
   %>
	  
 
  
  	<td><%= Internationalpartner %></td>
  
 

  
  
  </tr>  
   		
<%--    	<td> <%= request.getAttribute("TrendLevel_1_2") %>  </td> --%>
   
 
 <tr>
   	<td>Standard Partner </td>
<%--    	<td> <%= request.getAttribute("TrendLevel_1_2") %>  </td> --%>

		<%
  String domesticminuteM = (String)request.getAttribute("DomesticM");
  
	 
  %>
  	
  	<td><%= domesticminuteM %></td>
 

  <%
  String InternationalpartnerM = (String)request.getAttribute("InternationalM");
  
	
  %>
  
  	<td><%= InternationalpartnerM %></td>
  
  
  
  
  
  
  
</tr> 

  <tr>
  	<th>&nbsp;</th>
  <tr>
   <tr>
	<th><b> Workload </b></th>
  </tr>
  <tr>
   	<td>Category</td>
  
   <%
  ArrayList<String[]> datapartner = (ArrayList<String[]>)request.getAttribute("cpduserpartner");
  for(int i=0;i<datapartner.size();i++){
	  String[] st = datapartner.get(i);
  %>
 
  	<td><%= st[0] %></td>
  
  <%} %> 
   	
  
  
  
  
  
  
  
  
  
  
  
  
  
  </tr>
  
  
  
  
  
  
  
  
  
  
  
  
  	
  <tr>
   	<td>W-9s Added</td>
  
   <%
  ArrayList<String> countw9s = (ArrayList)request.getAttribute("StatusWork");
  
   
 
 	
     
  for(int i=0;i<countw9s.size();i++){
	  
 	  String ststatusw9s = countw9s.get(i).split(",")[0];
	  
  %>
  	
  	
  	<td><%= ststatusw9s %></td>

  <%
  } 
  
  %>
   	
  </tr>
  
   	

  </tr>
   <tr>
   	<td>Insurance Forms Added</td>
   	  <%
  ArrayList<String> countInsurance = (ArrayList)request.getAttribute("StatusWorkinurance");
  
   
 
 	
     
  for(int i=0;i<countInsurance.size();i++){
	  
 	  String ststatusinsurance = countInsurance.get(i).split(",")[0];
	  
  %>
  	
  	
  	<td><%= ststatusinsurance %></td>

  <%
  } 
  
  %>
   	
   	
  </tr>

   <tr>
   	<td>Resumes Added</td>
   
     	  <%
  ArrayList<String> countresume = (ArrayList)request.getAttribute("StatusWorkresume");
  
   
 
 	
     
  for(int i=0;i<countresume.size();i++){
	  
 	  String ststatusresume = countresume.get(i).split(",")[0];
	  
  %>
  	
  	
  	<td><%= ststatusresume %></td>

  <%
  } 
  
  %>
   	
 
   
  </tr>

	<tr>
   	<td>ID Badges Added</td>
   	<%
  ArrayList<String> countbadgedatabadge = (ArrayList)request.getAttribute("countbadgeIdList");
  
   	 for(int i=0;i<countbadgedatabadge.size();i++){
// 	  String webdata;
// 	  webdata=countbadgedata.get(i).split(",")[0];
	  
	  
	  String stcountbadge = countbadgedatabadge.get(i).split(",")[0];
  %>
 
  	<td><%= stcountbadge %></td>
  
  <%} %> 
  </tr>
  
<!--  New Section added  -->
<th>&nbsp;</th>
  <tr> 	 
 <tr>
	<th><b>Special Recruiting Efforts </b></th>
  </tr> 
  <tr>
  	<td>Category</td>
   	

 	<td>Zulma Gunther</td>
  	<td>Janessa Sambola</td>
 	<td>Anna Reeves</td>
  

   	
  </tr>
<!--  First Record column  -->	
	<%
   List<String[]> recruitedata =(ArrayList<String[]>)request.getAttribute("recruitedataarr");
   for(int i=0;i<recruitedata.size();i++){
	  
      String[] st = (String[])recruitedata.get(i);
      %>
      <tr>
      	<td><%= st[0] %></td>
      	<td><%= st[3] %></td>
      	<td><%= st[2] %></td>
      	<td><%= st[1] %></td>
      	
      	
      </tr>
      <% 
      
   }
      %>

    
  <tr>
  	<th>&nbsp;</th>
  <tr>
<tr>
	<th><b> Job Advertisement </b></th>
  </tr>
 
  	<td>Category</td>
	<td>Zulma Gunther</td>
  	<td>Janessa Sambola</td>
 	<td>Anna Reeves</td>



<!--  start of second row  -->
<%
   List<String[]> recruitedatasec =(ArrayList<String[]>)request.getAttribute("recruitedataarrsec");
   for(int i=0;i<recruitedatasec.size();i++){
	  
      String[] stg = (String[])recruitedatasec.get(i);
      %>
      <tr>
      	<td><%= stg[0] %></td>
      	<td><%= stg[3] %></td>
      	<td><%= stg[2] %></td>
      	<td><%= stg[1] %></td>
      	
      	
      </tr>
      <% 
      
   }
      %>  

<!--  Start of third row  -->
  
 
  <tr>
  	<th>&nbsp;</th>
  <tr>
   <tr>
	<th><b> Phone Log  </b></th>
  </tr>
  <tr>
   	<td>Category</td>
   	
   	 <%
  ArrayList<String[]> datacpd = (ArrayList<String[]>)request.getAttribute("cpduserpartner");
  for(int i=0;i<datacpd.size();i++){
	  String[] stdatacpd = datacpd.get(i);
  %>
  
  	<td><%= stdatacpd[0] %></td>
  
  <%} %> 
   	

  </tr>
  
  
  	
  <tr>
   	<td>Outbound Calls Made</td>

 <%
  ArrayList<String> dataout = (ArrayList)request.getAttribute("outgoing_lists");
  for(int i=0;i<dataout.size();i++){
	  String stdataout = dataout.get(i).split(",")[0];
  %>
  
  	<td><%= stdataout %></td>
  
  <%} %>


  </tr>
   <tr>
   	<td>Inbound Calls Received</td>

 <%
  ArrayList<String> datain = (ArrayList)request.getAttribute("incoming_lists");
  for(int i=0;i<datain.size();i++){
	  String stdatain = datain.get(i).split(",")[0];
  %>
  
  	<td><%= stdatain %></td>
  
  <%
  } 
  
  %>
  </tr>
   <%-- <tr>
   	<td># of calls during weekdays 2am-8am  </td>
   	<td> <%= request.getAttribute("callsWeekDays_2am_8am") %>  </td>
  </tr> --%>
   <%-- <tr>
   	<td># of tickets opened during PPS hours </td>
   	<td> <%= request.getAttribute("ticketsPps") %>  </td>
  </tr> --%>
  <%-- <tr>
   	<td># of tickets opened Weekdays 6pm-2am </td>
   	<td> <%= request.getAttribute("ticketsWeekDays_6pm_2am") %>  </td>
  </tr> --%>
  <%-- <tr>
   	<td># of tickets opened weekdays 2am-8am  </td>
   	<td> <%= request.getAttribute("ticketsWeekDays_2am_8am") %>  </td>
  </tr> --%>
  <%-- <tr>  	
  
   <tr>
	<th><b> MTTR: Mean Time To repair by trailing 30 days  </b></th>
  </tr>		
  <tr>
   	<td>Total by trailing 30 days  </td>
   	<td> <%= request.getAttribute("meanTimeLastThrityDays") %>  </td>
  </tr>
  <tr>
  	<th><b>MTTR: Mean Time To repair  by Client</b></th>
  	
  </tr>
  <%
  ArrayList<String[]> data = (ArrayList<String[]>)request.getAttribute("mttrByClient");
  for(int i=0;i<data.size();i++){
	  String[] st = data.get(i);
  %>
  <tr>
  	<td><%= st[0] %></td>
  	<td><%= st[1] %></td>
  </tr>
  <%} %> --%>
</table>	 

</body>
</html>
