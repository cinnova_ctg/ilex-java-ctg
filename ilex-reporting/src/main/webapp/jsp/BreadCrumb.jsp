<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>

<%
	String section = "";
	String sectionReport = "";
	String reportTitle = "";

	section = request.getParameter("section");
	sectionReport = request.getParameter("sectionReport");
	reportTitle = request.getParameter("reportTitle");
%>	

<div class="BreadCrumbText1">
	<a>Reporting</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<a 
		><%=section %></a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<a 
		><%=sectionReport %></a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<span id="lcBreadCrumbText"><%=reportTitle %></span>
</div>