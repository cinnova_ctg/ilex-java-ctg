<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<bean:define id="reportTitle" name = "MinutemanSpeedpayForm" property="reportSubTitle" />
<tiles:insert page="../reportLayout.jsp" flush="true">
  	<tiles:put name="menu" value="minutmanSpeedpayReport/reportNavigator.jsp" />
  	<tiles:put name="title" value="<%=reportTitle %>" />
 	<tiles:put name="filter" value="minutmanSpeedpayReport/minutemanSpeedpayUsageByCost/UsageByCostFilter.jsp" />
    <tiles:put name="factBoard" value="demo/factBoard.jsp" />
    <tiles:put name="content" value="minutmanSpeedpayReport/minutemanSpeedpayUsageByCost/UsageByCostContent.jsp" />
</tiles:insert>