<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
	<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
		  <tr>
		  	<td colspan="3" valign="top" width="100%"><tiles:insert attribute="menu"/></td>
		  </tr>
	      <tr>
  			<td id="contentTitle">
  				<table id="contentTitleTable" border="0" cellpadding="0" cellspacing="0" width="100%">
  					<tr>
  						<td><tiles:getAsString name="title" /></td>
  						<td style="text-align: right;"><tiles:insert attribute="filter" ignore="true"/></td>
  					</tr>
  				</table>
  			</td>
  			<td></td>
  			<td>&nbsp;</td>
	      </tr>
		  <tr>
		  	<td id="contentTD" valign="top"><tiles:insert attribute="content"/></td>
		  	<td width="100%"></td>
		  	<td id="factBoardTD" class="factBoard" valign="top">
		  		<div class="factBoardDiv"><tiles:insert attribute="factBoard"/></div>
		  	</td>
		  </tr>
	</table>
	
