<%@ page import="java.text.NumberFormat,java.util.Locale"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<link href="/Reporting/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Reporting/js/include.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath() %>/js/jquery.tablesorter.js"></script>
<link href="<%=request.getContextPath() %>/css/tableSorter.css"
	rel="stylesheet" type="text/css" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<Style>
.cDataLeft0 {
	text-align: left;
	background-color: #FFFFFF;
	font-size: 9pt;
	font-weight: normal;
	padding: 1px 3px;
	text-align: left;
	vertical-align: middle;
}
</Style>
<bean:define id="graphXML" name="TicketCountForm" property="graphXML" />
<script>
var chartWidth = "3000";
var graphXML = '<%=graphXML%>';
</script>
</head>
<%
String string="0,0,0";
if (!"".equals(request.getAttribute("param") )  &&  request.getAttribute("param")!=null)
 string = (String )request.getAttribute("param");


String[] parts = string.split(",");
 
%>
<table id="contentTable" border="0" width="3000px" cellspacing="0"
	cellpadding="0" style="padding: 0px; height: 328px;">
	<tr><td class="cDataLeft0" colspan="2">Total Closed tickets: <%= parts[0] %></td></tr>
	<tr><td class="cDataLeft0" colspan="2">Total Worked tickets: <%= parts[1] %></td></tr>
	<tr><td class="cDataLeft0" colspan="2">Total Install/Onsite Dispatch Tickets: <%= parts[2] %></td></tr>
	<tr style="padding: 0px;">
		<td valign="top" class="pageContentPadding">
			<table width="3000px" border="0" cellspacing="0" cellpadding="0">
				<tr style="padding: 0px;">
				
					<td valign="top">
						<table width="3000px" border="0" cellpadding="0" cellspacing="0"
							class="outlineYellow3">
							<tr class="dataRow1">
							
								<td class="dataRow1rA">
									<div id="chartdiv">This text is replaced by the
										chart.</div> <script>javascript:renderAxisChartForTicketCount();</script> <%=graphXML %>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</html>