<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;padding-left:10px;" >

	<tr>
		<td id="emptyTD" width="100%">&nbsp;</td>
	</tr>
	<tr>
		<td id="contentTD" valign="top"><tiles:insert attribute="content" /></td>
		<td id="emptyTD" >&nbsp;</td>
		<td id="factBoardTD" class="factBoard" valign="top">
			<div class="factBoardDiv">
				<tiles:insert attribute="factBoard" />
			</div>
		</td>
	</tr>
</table>

<script>
var contentTDSize = 0;

function getFrameSize(frameID) {
    var result = {height:0, width:0};
    if (document.getElementById) {
        var frame = parent.document.getElementById(frameID);
        if (frame.scrollWidth) {
            result.height = frame.scrollHeight;
            result.width = frame.scrollWidth;
        }
    }
    return result;
}

function modifyTD() {
	var widthDiff = 200;
	var frameSize = getFrameSize("footerFrame");
	if(contentTDSize != frameSize.width - widthDiff)
	{
		contentTDSize = frameSize.width - widthDiff;
		$("#contentTD").attr("width", contentTDSize);
	}
}

$(window).bind('resize', function() {
	//modifyTD();
	$("#contentTitleTable").width($("#contentTable").width());
	});

$(document).ready(function() {
	//modifyTD();
	$("#contentTitleTable").width($("#contentTable").width());
});
</script>
