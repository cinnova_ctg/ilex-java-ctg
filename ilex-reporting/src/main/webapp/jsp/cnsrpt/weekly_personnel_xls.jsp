<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
response.setContentType("application/vnd.ms-excel");   
response.setHeader("Content-Disposition","inline; filename = weekly_personnel_current.xlsx");
%>
<html>
	<head>
		<script src="js/FusionCharts.js"></script>
    </head>
	<body style="margin: 15px;">
		<table>
			<tr style="font-weight: bold;">
				<td>First name</td>
				<td>Last Name </td>
				<td>Title</td>
				<td>Rank</td>
				<td>Role</td>
			</tr>
			<%=request.getAttribute("xls_data") %>			
		</table>
	</body>
</html>