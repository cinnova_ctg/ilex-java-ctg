<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
	<head>
	<style type="text/css">
		
		
		.stylishButtons {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #8C9CBF;
    background-image: -moz-linear-gradient(center top , #8C9CBF 0%, #546A9E 50%, #36518F 50%, #3D5691 100%);
    
    background-image: -ms-linear-gradient(rgb(140, 156, 191) 0%, rgb(84, 106, 158) 50%, rgb(54, 81, 143) 50%, rgb(61, 86, 145) 100%);
    border-color: #172D6E #172D6E #0E1D45;
    border-image: none;
    border-radius: 5px;
    border-style: solid;
    border-width: 1px;
    box-shadow: 0 1px 0 0 #B1B9CB inset;
    color: #FFFFFF;
    font: bold 12px/1 "helvetica neue",helvetica,arial,sans-serif;
    padding: 8px 0 8px;
    text-align: center;
    text-decoration: none;
    text-shadow: 0 -1px 1px #000F4D;
    width: 100px;
}
.stylishButtons:hover {
    background-color: #7F8DAD;
    background-image: -moz-linear-gradient(center top , #7F8DAD 0%, #4A5E8C 50%, #2F477D 50%, #364C80 100%);
    background-image: -ms-linear-gradient(rgb(127, 141, 173) 0%, rgb(74, 94, 140) 50%, rgb(47, 71, 125) 50%, rgb(54, 76, 128) 100%);
    
    cursor: pointer;
}

.stylishButtons:active {
    box-shadow: 0 0 20px 0 #1D2845 inset, 0 1px 0 #FFFFFF;
}
	
	</style>
		<script src="js/FusionCharts.js"></script>
		<script type="text/javascript">
			chart_data = "<chart caption='Personnel Head Count by Title' xAxisName='Week' yAxisName='Amount' labelDisplay='Rotate' yAxisMinValue='0' yAxisMaxValue='50' minimiseWrappingInLegend='1'>"+
             "<%=request.getAttribute("chart_data") %>"+
             "</chart>";

             var counter = 1;
         
             if(window.location.href.indexOf("&counter") > -1)
            	 {
            	 counter = window.location.href.split("&counter=")[1]*1;
            	 }
             else
            	 {
            	 counter = 1;
            	 }
            
             
             var contextPath = "<%=request.getContextPath() %>";
             
             function next()
             {
            	 if(counter == 1)
            		 {
            		 	counter = 1;
            		 	alert("You are already on latest page.");
            		 	return false;
            		 }
            	 else
            		 {
            		 counter = counter - 1;
            		 }
            	 goLink();
             }
             
             function previous()
             {
            	 counter = counter + 1;
            	 goLink();
             }
             
             function reset()
             {
            	 counter = 1;
            	 goLink();
             }
             
             function goLink()
             {
            	
            	 var link = contextPath + "/CNSRPT.do?ac=weekly_personnel&counter=" + counter;
            	// alert(link);
            	 window.open(link,"_self");
            	 
             }
             
             function openSaveExcelFile(value){
            	if(value!='0'){
            		window.open("?ac=weekly_personnel_xls&paramDate="+value,"_self");	
            	}
             }
		</script>
    </head>
	<body style="margin: 15px;">
		<div id="chartdiv" align="center" style="margin: 10px;">The chart will appear within this DIV. This text will be replaced by the chart.</div>
		<script type="text/javascript">
			var myChart = new FusionCharts("fc/MSLine4.swf", "myChartId", "1200", "800", "0", "0");
	        myChart.setDataXML(chart_data);
	      	myChart.render("chartdiv");
	    </script>
	    <div style="text-align: center;margin: 10px;">
	    	<div style="background: #E5E5E5; border: 1px solid #B4B3B3; font-family: Verdana, Geneva, sans-serif; font-size: 12px; padding: 10px; width: 1150;">
	    		<div style="border: 0px; border-right: 1px solid #B4B3B3; float: left; height: 20px;width:150px;">
	    		<select onchange="openSaveExcelFile(this.value);">
	    		<option value="0">Excel Spreadsheet</option>
	    		<%
	    			String[] catDates = (String[])request.getAttribute("catDates");
	    			for(int i=0;i<catDates.length;i++){
	    				out.write("<option value="+catDates[i]+">"+catDates[i]+"</option>");
	    			}
	    		%>
	    		
	    		</select></div>
	    		<div style="border: 0px; height: 20px;">Role: <a href="?ac=weekly_personnel_details&in_department=Accounting">Accounting</a> <a href="?ac=weekly_personnel_details&in_department=CPD">CPD</a> <a href="?ac=weekly_personnel_details&in_department=Engineering">Engineering</a> <a href="?ac=weekly_personnel_details&in_department=Field%20Services">Field Services</a> <a href="?ac=weekly_personnel_details&in_department=Help%20Desk">Help Desk</a> <a href="?ac=weekly_personnel_details&in_department=Logistics">Logistics</a> <a href="?ac=weekly_personnel_details&in_department=Management">Management</a>  <a href="?ac=weekly_personnel_details&in_department=MIS">MIS</a> <a href="?ac=weekly_personnel_details&in_department=MSP%20Billing">MSP Billing</a> <a href="?ac=weekly_personnel_details&in_department=Projects">Projects</a> <a href="?ac=weekly_personnel_details&in_department=Quality%20Assurance">Quality Assurance</a> <a href="?ac=weekly_personnel_details&in_department=Sales">Sales</a> <a href="?ac=weekly_personnel_details&in_department=Software">Software</a></div>
                        <div style="border: 0px; height: 20px;">Title: <a href="?ac=weekly_personnel_title&in_department=Accounting">Accounting</a> <a href="?ac=weekly_personnel_title&in_department=CPD">CPD</a> <a href="?ac=weekly_personnel_title&in_department=Engineering">Engineering</a> <a href="?ac=weekly_personnel_title&in_department=Field%20Services">Field Services</a> <a href="?ac=weekly_personnel_title&in_department=Help%20Desk">Help Desk</a> <a href="?ac=weekly_personnel_title&in_department=Logistics">Logistics</a> <a href="?ac=weekly_personnel_title&in_department=Management">Management</a>  <a href="?ac=weekly_personnel_title&in_department=MIS">MIS</a> <a href="?ac=weekly_personnel_title&in_department=MSP%20Billing">MSP Billing</a> <a href="?ac=weekly_personnel_title&in_department=Projects">Projects</a> <a href="?ac=weekly_personnel_title&in_department=Quality%20Assurance">Quality Assurance</a> <a href="?ac=weekly_personnel_title&in_department=Sales">Sales</a> <a href="?ac=weekly_personnel_title&in_department=Software">Software</a></div>
	    	</div>
		</div>
	</body>
</html>