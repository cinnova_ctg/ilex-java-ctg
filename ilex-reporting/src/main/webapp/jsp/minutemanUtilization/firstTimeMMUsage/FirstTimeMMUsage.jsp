<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>


<tiles:insert page="../reportLayout.jsp" flush="true">
  	<tiles:put name="menu" value="minutemanUtilization/reportNavigator.jsp" />
 	<tiles:put name="title" value="" />
 	<tiles:put name="factBoard" value="demo/factBoard.jsp" />
    <tiles:put name="content" value="minutemanUtilization/firstTimeMMUsage/FirstTimeMMUsageContent.jsp" /> 
</tiles:insert>
         