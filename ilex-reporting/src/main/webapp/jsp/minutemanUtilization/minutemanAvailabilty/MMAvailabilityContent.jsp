<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<bean:define id="graphXML" name="MinutemanUtilizationForm" property="graphXML" />
<link href="<%=request.getContextPath() %>/css/style_common.css" rel="stylesheet" type="text/css" />
<script>
var chartWidth = "800";
var graphXML = '<%=graphXML%>';

$(document).ready(function() {
	$("#lcBreadCrumbText").html("Availability/Recruiting");
});
</script>
</head>
<body>
<table id="contentTable">
	<tr>
		<td valign="top" class="pageContentPadding">
		<table>
		<tr>
			<TD class="PCSSH0001">
				Minuteman Availability/Usage - Past 24 Months
			</TD>
		</tr>
		</table>
		<table>	
			<tr>
	  				<td  class="PCSTD0001">Total Minuteman</td>
	  				<td  class="PCSTD0002">
	  				<bean:write name="MinutemanUtilizationForm" property="totalMM"/></td>
	  			</tr>
	  			
	  			<tr>
	  				<td  class="PCSTD0001"  >Used Minuteman</td>
	  				<td  class="PCSTD0002">
	  				<bean:write name="MinutemanUtilizationForm" property="usedMM"/></td>
	  			</tr>
	  			
	  			<tr>
	  				<td  class="PCSTD0001" >Unused Minuteman</td>
	  				<td  class="PCSTD0002">
	  			<bean:write name="MinutemanUtilizationForm" property="unUsedMM"/></td>
	  			</tr>
	  			
	  			
	</table>
	<table class="SSHSTD002">
		<tr>
	  		<td class="PCSSH0001"  >Minuteman Recruiting - TTM</td>
	  	</tr>
	 </table>
	<tr>
	  			 <td class="dataRow1rA">
				              <div id="chartMultiAxisdiv">This text is replaced by the chart.</div>
				              	<script>javascript:renderStrakedChart();</script>
				              </td>
	  			</tr>
	  			
	  			
	  			
	  			<tr>
							<td id="PageContentDate">Content Date:&nbsp;&nbsp;<span id="currDate"></span></td>
						  </tr>
</table>
</body>
</html>