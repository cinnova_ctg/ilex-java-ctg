<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Minuteman Utilization Navigator</title>
<script>
var contextPath = "<%=request.getContextPath() %>";
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/include.js"></script>
</head>

<%
	StringBuffer url = new StringBuffer();
	url = request.getRequestURL();
	 
	 String phpBaseURL = "http://ilex-qa.contingent.local/";
	 
	 if (url.toString().contains(
				"http://ilex.contingent.local/Reporting/")
				|| url.toString().contains(
						"http://ilex-prod2.contingent.local/Reporting/")) {
		 
		 phpBaseURL = "http://ilex-php.contingent.local/";
	 }
%>


<body>
<table width="100%" border="0" style="background: url(<%=request.getContextPath() %>/images/bggrey.jpg) repeat-x; margin: 0;" cellpadding="0" cellspacing="0"><tr><td>
	<ul class="dropdown">
	    <li><a href="#">MinutemanUtilization&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/white-arrow.gif" border="0"></img></a>
	   <ul class="sub_menu">
	            <li><a href="<%=phpBaseURL %>content/ilex/#/report/first_time_usage" target="mainBodyFrame">First Time MM Usage</a></li>
	            <li><a href="<%=request.getContextPath() %>/MinutemanUtilization.do?ref=minutemanAvailablity">MM Availability</a></li>
	            <li><a href="<%=request.getContextPath() %>/MinutemanUtilization.do?ref=minutemanUsageByJobOwner">MM Usage by Job Owner</a></li>
	        </ul>
	    </li>
	</ul>
</td></tr>
	<tr>
		<td id="PageContentBreadCrumb">
			<div class="BreadCrumbText1"><a href="#SSSSSS">Reporting</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<a 
			>Operations</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<a 
			>Minuteman Utilization</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/><span id="lcBreadCrumbText"></span></div>
		</td>
	</tr>
</table>
<script>
activateDDMenu();
</script>
</body>
</html>