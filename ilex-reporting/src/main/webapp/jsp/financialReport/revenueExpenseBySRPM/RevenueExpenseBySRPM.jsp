<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%
String secondLevelReportType1="SPM(TTM)Summary";
String secondLevelReportType2="SPMAccountSummary(TTM)";
String secondLevelReportType3="SPMAccountSummary(All)";
String secondLevelReportType4="spmRevenueChart";
%>
<tiles:insert page="../reportLayout.jsp" flush="true">
  	<tiles:put name="menu" value="financialReport/reportNavigator.jsp" />
  	<tiles:put name="title" value="" />
    <tiles:put name="factBoard" value="demo/factBoard.jsp" />
    <%if(secondLevelReportType1.equals(request.getAttribute("secondLevelReportName"))){ %>
    <tiles:put name="content" value="financialReport/revenueExpenseBySRPM/SPM(TTM)Summary.jsp" />
    <%}else if(secondLevelReportType2.equals(request.getAttribute("secondLevelReportName"))) {%>
    <tiles:put name="content" value="financialReport/revenueExpenseBySRPM/SPMAccountSummary(TTM).jsp" />
    <%}else if(secondLevelReportType3.equals(request.getAttribute("secondLevelReportName"))) {%>
    <tiles:put name="content" value="financialReport/revenueExpenseBySRPM/SPMAccountSummary(All).jsp" />
    <%}else if(secondLevelReportType4.equals(request.getAttribute("chartReportName"))){ %>
    <tiles:put name="content" value="financialReport/revenueExpenseBySRPM/RevenueExpenseSRPMChart.jsp" />
  	<%}else{ %>
    <tiles:put name="content" value="financialReport/revenueExpenseBySRPM/RevenueExpenseBySRPMContent.jsp" />
<%} %>
</tiles:insert>