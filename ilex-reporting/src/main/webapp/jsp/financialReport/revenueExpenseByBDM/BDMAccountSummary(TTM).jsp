<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ page  import="java.text.NumberFormat,java.util.Locale" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<link href="<%=request.getContextPath() %>/css/tableSorter.css" rel="stylesheet" type="text/css" />
<link href="/Reporting/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Reporting/js/include.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.tablesorter.js"></script>
<%
NumberFormat Formatter=NumberFormat.getCurrencyInstance(new Locale("en_US"));
Double currency,amount;
%>
<bean:define id="bdmName" name="RevenueExpenseForm" property="bdmName" />
<bean:define id="bdmID" name="RevenueExpenseForm" property="bdmID" />
<script>
var bdmName = '<%=bdmName%>';
var bdmID='<%=bdmID%>';
$(document).ready(function() {
	//$("#emptyTD").removeAttr('width');
	var url="RevenueExpenseAction.do?ref=revenueExpenseByBDM&parm=firstTime";
	$("#lcBreadCrumbText").html("<a href="+url+">Revenue/Expense by BDM</a>&nbsp;&nbsp;"+"<img src=images/historyBullets.gif height=5 width=5/>&nbsp;&nbsp;"+bdmName);
	$("#bdmTable_3 tr:even").addClass("even");
	$("#bdmTable_3 tr:odd").addClass("odd");
	
});
function switchingCall()
{
	var report=document.getElementById('test').value;
	var url = "RevenueExpenseAction.do?ref=bdmSecondLevelReports&bdmName="+bdmName+"&bdmID="+bdmID+"&reportName="+report;
	document.forms[0].action = url;
	document.forms[0].submit();	
	}
$(document).ready(function(){
            $(".noborder").each(function(){
                        $(this).css('border-right','none');
                        $(this).css('border-bottom','none');
                        $(this).css('border-top','none');
                        $(this).css('background-color','#FFFFFF');
                        $(this).css('width','32px');
                        
            });
});
</script>
<title>Revenue By BDM</title>
</head>
<body>
	  		<table id="contentTable" border="0"  cellspacing="0" cellpadding="0" style="padding: 0px; height: 328px;">
	  			<tr>
	  				<td>
	  					<table border="0" width="100%">
	  						<tr>
	  							<td align="left" width="99%"><h1 style="font-size: 11px; color:#2e3d80; white-space: nowrap;">&nbsp;&nbsp;Account Summary(TTM)</h1></td>
	  							<td  align="right" style="padding-right: 35px">
		  								<html:select styleClass="select" styleId="test"  name="RevenueExpenseForm" property="selectedSummaryPage" onchange="switchingCall();">	
						  				<html:option  value="BDM(TTM)Summary">TTM Summary</html:option>	
						  				<html:option value="BDMAccountSummary(TTM)" >Account Summary(TTM)</html:option>
						  				<html:option value="BDMAccountSummary(All)">Account Summary(All)</html:option>
		  								</html:select>
		  						</td>
	  						</tr>
	  					</table>
	  				</td>
	  			</tr>
	  					<tr style="padding: 0px;">	
				        	<td >
				        		<table width="100%" border="0" cellpadding="0" cellspacing="1" id="bdmTable_3" class="tablesorter">
		  						<thead>
			  						<tr>
				  						<th rowspan="3">Account</th>
				  						<th colspan="2">Expense</th>
				  						<th rowspan="2">Revenue</th>
				  						<th colspan="3">VGPM</th>
				  						<th rowspan="2">Count</th>
				  						<th rowspan="2" class="noborder"></th>
			  						</tr>
			  						<tr>
			  							<th nowrap="nowrap">Pro Forma</th>
			  							<th>Actual</th>
			  							<th nowrap="nowrap">Pro Forma</th>
			  							<th>Actual</th>
			  							<th>Delta</th>
			  						</tr>
			  				
		  						</thead>
		  			<tbody>
		  			
		  										<logic:present name="RevenueExpenseForm" property="detailedList">
					        			<logic:iterate id="list" name="RevenueExpenseForm" property="detailedList">
						       			<bean:define id="accountName" name="list"  property="accountName" type="java.lang.String" />
						       				<%
						       					if(accountName.contains("'")){
						       						accountName=accountName.replace("'","\\'");
						       					}
						       				%>
						        			<tr>
						        				<td width="40%" align="left" nowrap="nowrap" ><a href="javascript:openThirdLevelReportPopUP('/RevenueExpenseAction.do?ref=bdmThirdLevelReports','<bean:write name="list" property="accountID" />','<bean:write name="RevenueExpenseForm" property="bdmID" />','bdmAccountPopupReport','<bean:write name="RevenueExpenseForm" property="bdmName" />','<%=accountName%>');" >
						        						<bean:write name="list" property="accountName" /></a></td>
						        				<bean:define id="pfExpense" name="list"  property="proFormaExpense" type="java.lang.String" />
						        					<% 	
						        					currency = new Double(pfExpense);
						        					pfExpense=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
													 %>
						        				<td  align="right" nowrap="nowrap"><%=pfExpense %></td>    
						        				<bean:define id="expense" name="list"  property="expense" type="java.lang.String" />    
						        				 <%
						        				 currency = new Double(expense);
						        				 expense=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
												 %>   					       
						        				<td  align="right" nowrap="nowrap"><%=expense%></td>     					       
						        				<bean:define id="revenue" name="list"  property="revenue" type="java.lang.String" />    
						        				 <%
						        				 currency = new Double(revenue);
						        				 revenue=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
												 %>
						        				<td align="right" nowrap="nowrap"><%=revenue%></td>     					       
						        				<bean:define id="proFormaVGPM" name="list"  property="proFormaVGPM" type="java.lang.String" />   
						        				<%
						        				proFormaVGPM=proFormaVGPM.substring(0,proFormaVGPM.length()-2);
						        				%>
						        				<td  align="center" nowrap="nowrap"><%=proFormaVGPM%></td>  
						        				<bean:define id="VGPM" name="list"  property="VGPM" type="java.lang.String" />   
						        				<% 
						        				VGPM=VGPM.substring(0,VGPM.length()-2);
						        				%>   					       
						        				<td  align="center" nowrap="nowrap"><%=VGPM%></td> 
						        				<bean:define id="deltaVGPM" name="list"  property="deltaVGPM" type="java.lang.String" />   
						        				<%deltaVGPM=deltaVGPM.substring(0,deltaVGPM.length()-2); %>
						        				<td align="center" nowrap="nowrap"><%=deltaVGPM%></td> 
						        				<bean:define id="occurrences" name="list"  property="occurrences" type="java.lang.String" />    
						        				 <%
						        				 amount = new Double(occurrences);
						        				 occurrences=Formatter.format(amount).substring(1, Formatter.format(amount).indexOf("."));
												 %>      					       
						        				<td  align="right" nowrap="nowrap"><%=occurrences %></td>     					       
						        		<logic:lessThan name="list" property="deltaVGPM" value=".00">
						        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-red.gif" alt=""/></td>
						        			</logic:lessThan>	
						        			<logic:greaterEqual name="list" property="deltaVGPM" value=".05">
						        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-green.gif" alt=""/></td>
						        			</logic:greaterEqual>
						        		<logic:greaterThan name="list"  property="VGPM" value=".55">
						        					<logic:greaterEqual name="list" property="deltaVGPM" value="0.00">
							        					<logic:lessThan name="list" property="deltaVGPM" value="0.05">
							        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-blue.gif" alt=""/></td>
							        					</logic:lessThan>
						        					</logic:greaterEqual>
						        	   </logic:greaterThan>	   
						        			</tr>
						        			
					        			</logic:iterate>
					        		</logic:present>
		  					</tbody>
		  						</table>
				        	</td>
				        	</tr>
						   <tr height="15px"></tr>
						  <tr>
						  <td>
	  						<%@ include file="../../Legends.jsp" %>
	  					 </td>
	  			</tr>
	  			<tr>
							<td id="PageContentDate">Content Date:&nbsp;&nbsp;<span id="currDate"></span></td>
						  </tr>
	  		</table>
	  		<div id="jobOwnerChartModal" style="display: none; cursor: default; border: 0px; border-color: red;" > 
		        <div  style="border: 0px;">
		        	<table cellspacing="0" cellpadding="0" class="headerpaleyellow" width="100%" border='0px'>
		        		<tr>
		        			<td align="left" id="jobTitle" height="20px"></td>
		        			<td align="right" style="padding-right: 5px;"><img height="15" width="15" alt="Close" src="images/delete.gif" id="close" onclick="closeWindow();"></td>
		        		</tr>
		        	</table>
		        </div>
        		<div id="jobOwnerChartValue" style=" padding-top:4px; overflow: visible; height:400px; overflow-x:auto; overflow-y:auto;"><img src="./images/waiting_tree.gif" /></div>
			</div>
	  	</body>
	  	</html>