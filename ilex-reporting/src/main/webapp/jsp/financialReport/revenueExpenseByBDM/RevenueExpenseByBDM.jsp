<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%
String secondLevelReportType1="BDM(TTM)Summary";
String secondLevelReportType2="BDMAccountSummary(TTM)";
String secondLevelReportType3="BDMAccountSummary(All)";
%>
<tiles:insert page="../reportLayout.jsp" flush="true">
  	<tiles:put name="menu" value="financialReport/reportNavigator.jsp" />
  	<tiles:put name="title" value="" />
    <tiles:put name="factBoard" value="demo/factBoard.jsp" />
    <%if(secondLevelReportType1.equals(request.getAttribute("secondLevelReportName"))){ %>
    <tiles:put name="content" value="financialReport/revenueExpenseByBDM/BDM(TTM)Summary.jsp" />
    <%}else if(secondLevelReportType2.equals(request.getAttribute("secondLevelReportName"))) {%>
    <tiles:put name="content" value="financialReport/revenueExpenseByBDM/BDMAccountSummary(TTM).jsp" />
    <%}else if(secondLevelReportType3.equals(request.getAttribute("secondLevelReportName"))) {%>
    <tiles:put name="content" value="financialReport/revenueExpenseByBDM/BDMAccountSummary(All).jsp" />
  	<%}else{ %>
    <tiles:put name="content" value="financialReport/revenueExpenseByBDM/RevenueExpenseByBDMContent.jsp" />
	<%} %>
</tiles:insert>