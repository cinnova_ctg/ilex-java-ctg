<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link href="<%=request.getContextPath() %>/css/style.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/tableSorter.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.b1{
font-family:Arial, Helvetica, sans-serif;
font-size:12px;
font-weight:bold;
color:#000000; 
text-align:left;
text-transform:uppercase;
padding-left: 15px;
}

</style>

<script>
var contextPath = "<%=request.getContextPath() %>";
var chartWidth = "600";
var graphXML = "";
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/FusionCharts.js" ></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/include.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.dropdownPlain.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/css_browser_selector.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/multiAxisChart.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.tablesorter.js"></script>

<script> 
function display(){
	if(document.getElementById("menuList").style.display == 'none')
	{
		$("#menuList").show();
	}
	else {
		$("#menuList").hide();
	}
}

function formatDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;//January is 0!
	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd}
	if(mm<10){mm='0'+mm}
	return mm+'/'+dd+'/'+yyyy;
}

function openThirdLevelReportPopUP(url, param,Id,reportName,Name,accountName) {
	blockUnblockUI();
	url = encodeURI(url);
	if("jobOwnerMonthPopupReport"==reportName){
		$("#jobOwnerChartValue").load("RevenueExpenseAction.do?ref=jobOwnerThirdLevelReports&jobOwnerID="+Id+"&reportName="+reportName+"&month="+param+"&timeStamp="+(new Date()*1));
		document.getElementById("jobTitle").innerHTML = '<div class="b1">'+Name+'</div>';
	}
	else if("jobOwnerAccountPopupReport"==reportName)
		{
		$("#jobOwnerChartValue").empty();
		$("<iframe id='jobOwnerChartValueIFrame' width=100% height='400px' FRAMEBORDER=0 style='width:100%;height:400px;overflow:scroll;' SCROLLING='auto'></iframe>").appendTo("#jobOwnerChartValue");
		$("#jobOwnerChartValueIFrame").attr("src","RevenueExpenseAction.do?ref=jobOwnerThirdLevelReports&jobOwnerID="+Id+"&reportName="+reportName+"&accountID="+param+"&accountName="+accountName+"&timeStamp="+(new Date()*1));
		document.getElementById("jobTitle").innerHTML = '<div class="b1">'+Name+'</div>';
		}
	else if("projectManagerMonthPopupReport"==reportName){
		$("#jobOwnerChartValue").load("RevenueExpenseAction.do?ref=projectManagerThirdLevelReports&projectManagerID="+Id+"&reportName="+reportName+"&month="+param+"&timeStamp="+(new Date()*1));
		document.getElementById("jobTitle").innerHTML = '<div class="b1">'+Name+'</div>';
	}
	else if("projectManagerAccountPopupReport"==reportName)
		{
		$("#jobOwnerChartValue").empty();
		$("<iframe id='jobOwnerChartValueIFrame' width=100% height='400px' FRAMEBORDER=0 style='width:100%;height:400px;overflow:scroll;' SCROLLING='auto'></iframe>").appendTo("#jobOwnerChartValue");
		$("#jobOwnerChartValueIFrame").attr("src","RevenueExpenseAction.do?ref=projectManagerThirdLevelReports&projectManagerID="+Id+"&reportName="+reportName+"&accountID="+param+"&accountName="+accountName+"&timeStamp="+(new Date()*1));
		document.getElementById("jobTitle").innerHTML = '<div class="b1">'+Name+'</div>';
		}
	else if("bdmMonthPopupReport"==reportName){
		$("#jobOwnerChartValue").load("RevenueExpenseAction.do?ref=bdmThirdLevelReports&bdmID="+Id+"&reportName="+reportName+"&month="+param+"&timeStamp="+(new Date()*1));
		document.getElementById("jobTitle").innerHTML = '<div class="b1">'+Name+'</div>';
	}
	else if("bdmAccountPopupReport"==reportName)
		{
		$("#jobOwnerChartValue").empty();
		$("<iframe id='jobOwnerChartValueIFrame' width=100% height='400px'  FRAMEBORDER=0 style='width:100%;height:400px; vertical-align:top; overflow:scroll; padding:0px;' valign='top' SCROLLING='false'></iframe>").appendTo("#jobOwnerChartValue");
		$("#jobOwnerChartValueIFrame").attr("src","RevenueExpenseAction.do?ref=bdmThirdLevelReports&bdmID="+Id+"&reportName="+reportName+"&accountID="+param+"&accountName="+accountName+"&timeStamp="+(new Date()*1));
		document.getElementById("jobTitle").innerHTML = '<div class="b1">'+Name+'</div>';
		}
	else if("spmMonthPopupReport"==reportName){
		$("#jobOwnerChartValue").load("RevenueExpenseAction.do?ref=spmThirdLevelReports&spmID="+Id+"&reportName="+reportName+"&month="+param+"&timeStamp="+(new Date()*1));
		document.getElementById("jobTitle").innerHTML = '<div class="b1">'+Name+'</div>';
	}
	else if("spmAccountPopupReport"==reportName)
		{
		$("#jobOwnerChartValue").empty();
		$("<iframe id='jobOwnerChartValueIFrame' width=100% height='400px' FRAMEBORDER=0 style='width:100%;height:400px;overflow:scroll;' SCROLLING='auto'></iframe>").appendTo("#jobOwnerChartValue");
		$("#jobOwnerChartValueIFrame").attr("src","RevenueExpenseAction.do?ref=spmThirdLevelReports&spmID="+Id+"&reportName="+reportName+"&accountID="+param+"&accountName="+accountName+"&timeStamp="+(new Date()*1));
		document.getElementById("jobTitle").innerHTML = '<div class="b1">'+Name+'</div>';
		}
	else if("customerAppendixMonthPopupReport"==reportName)
		{
		$("#jobOwnerChartValue").empty();
		$("<iframe id='jobOwnerChartValueIFrame' width=100% height='400px' FRAMEBORDER=0 style='width:100%;height:400px;overflow:scroll;' SCROLLING='auto'></iframe>").appendTo("#jobOwnerChartValue");
		$("#jobOwnerChartValueIFrame").attr("src","RevenueExpenseAction.do?ref=customerThirdLevelReports&appendixID="+param+"&reportName="+reportName+"&appendixName="+accountName+"&timeStamp="+(new Date()*1));
		document.getElementById("jobTitle").innerHTML = '<div class="b1">'+Name+'</div>';
		
		}
	}
	
function blockUnblockUI() {
	$.blockUI(
		{ 
			message: $('#jobOwnerChartModal'),
			css: { 
				width: '790px',
				height:'480px',
				position:'absolute',
				color:'lightblue' 
				},
			overlayCSS:  { 
		        backgroundColor: '#f0f0f0', 
		        opacity:         0.3 
		    },
		    centerX: false,  
		    centerY: false,
		    fadeIn:  0,
		    fadeOut: 0
		}
		);
}
function closeWindow () {
    $.unblockUI(); 
	$("#jobOwnerChartValue").empty().html('<img src="./images/waiting_tree.gif" />');
	document.getElementById("jobTitle").innerHTML = '';	
   // return false; 
}
</script>

<style>
.outlineYellow3 {
	padding: 0px;
	margin:6px 0pt 6px;
	border: 2px solid #FAF8CC;
	background: #ffffff;
}
.dataRow1rA, .dataRow2rA, .dataRow1, .dataRow2 {
	white-space:nowrap;
	font-size: 12px;
	text-align:left;
	padding:  3px 5px 3px 8px;
	color: #000000;
	background: #f9f9f9;
}
.dataRow2rA, .dataRow2 {
	background: #ececec;
}
.dataRow1rA, .dataRow2rA {
	border-right-width: 2px;
	border-right-style: solid;
	border-right-color: #FAF8CC;
}
.dataRow1, .dataRow2 {
	text-align:right;
}
</style>
</head>
<body>
<html:form action="/RevenueExpenseAction">
<jsp:include page="../reportLayoutTemplate.jsp"></jsp:include>
</html:form>
<script>
var d = formatDate();
document.getElementById("currDate").innerHTML = d;
</script>
</body>
</html>