<%@ page  import="java.text.NumberFormat,java.util.Locale" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<link href="/Reporting/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Reporting/js/include.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.tablesorter.js"></script>
<link href="<%=request.getContextPath() %>/css/tableSorter.css" rel="stylesheet" type="text/css" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<bean:define id="graphXML" name="RevenueExpenseForm" property="graphXML" />
<%
NumberFormat Formatter=NumberFormat.getCurrencyInstance(new Locale("en_US"));
%>
<% 
Double currency,amount;
%>
<script>
var chartWidth = "800";
var graphXML = '<%=graphXML%>';

$(document).ready(function() {
	$("#lcBreadCrumbText").html("Revenue/Expense");
});
function getFilteredJobType(jobFilterSelectedValue) {
	if((jobFilterSelectedValue)==""){
		return false;
	}
	$("form[name='RevenueExpenseForm']").attr('action' , 'RevenueExpenseAction.do?ref=revenueExpense&filterType='+jobFilterSelectedValue);
	$("form[name='RevenueExpenseForm']").submit();
}
</script>
<script>

$(document).ready(function() {
	$("#revenueExpenseTable tr:even").addClass("even");
	$("#revenueExpenseTable tr:odd").addClass("odd");
});

</script>
<script>
$(document).ready(function(){
            $(".noborder").each(function(){
                        $(this).css('border-right','none');
                        $(this).css('border-bottom','none');
                        $(this).css('border-top','none');
                        $(this).css('background-color','#FFFFFF');
            });
});
</script>
</head>
	  		<table id="contentTable" border="0" width="100%" cellspacing="0" cellpadding="0"
			style="padding: 0px; height: 328px;">
	  			<tr  style="padding: 0px;">
	  				<td valign="top" class="pageContentPadding">
	  					<table  width="100%" border="0" cellspacing="0" cellpadding="0">
	  						<tr style="padding: 0px;">	
						        <td valign="top">
						          <table width="165" border="0" cellpadding="0" cellspacing="0" class="outlineYellow3">
						            <tr class="dataRow1">
						              <td class="dataRow1rA">
						              <div id="chartMultiAxisdiv">This text is replaced by the chart.</div>
						             	<script>javascript:renderMultiAxisChart();</script>
						             	<%=graphXML %>
						              </td>
						            </tr>
						          </table>
						         </td>
						      </tr>
						      
	  					</table>
	  				</td>
	  			</tr>
	  			<tr>
	  			<td>
	  				<table id="contentTable" border="0" width="100%" cellspacing="0" cellpadding="0"
			style="padding: 0px; height: 328px;">
	  			<tr  style="padding: 0px;">
	  				<td valign="top" class="pageContentPadding" >
	  				<table  width="100%" border="0" cellspacing="0" cellpadding="0">
	  					<tr style="padding: 0px;">	
				        	<td valign="top">
				        		<table width="100%" border="0" cellpadding="0" cellspacing="1" id="revenueExpenseTable" class="tablesorter">
		  						<thead>
			  						<tr>
				  						<th rowspan="2">Month/Year</th>
				  						<td colspan="2">Expense</td>
				  						<th rowspan="2">Revenue</th>
				  						<td colspan="3">VGPM</td>
				  						<th rowspan="2">Count</th>
			  							<th style="background-color: white; border-color: white;border: 0px;  " rowspan="2"></th>
			  						</tr>
			  						<tr>
			  							<th>Pro Forma</th>
			  							<th>Actual</th>
			  							<th>Pro Forma</th>
			  							<th>Actual</th>
			  							<th>Delta</th>
			  						</tr>
			  				
		  						</thead>
		  			<tbody>
		  			
		  										<logic:present name="RevenueExpenseForm" property="revenueExpenceList">
					        			<logic:iterate id="list" name="RevenueExpenseForm" property="revenueExpenceList">
						        
						        			<tr>
						        				<td  align="left" nowrap="nowrap"><bean:write name="list" property="month" /></td>
						        				<bean:define id="pfExpense" name="list"  property="proFormaExpense" type="java.lang.String" />
						        					<% 	
						        					currency = new Double(pfExpense);
						        					pfExpense=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
													 %>
						        				<td  align="right" nowrap="nowrap"><%=pfExpense %></td>   
						        			 <bean:define id="expense" name="list"  property="expense" type="java.lang.String" />    
						        				 <%
						        				 currency = new Double(expense);
						        				 expense=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
												 %>   
						        				<td  align="right" nowrap="nowrap"><%=expense %></td>     					       
						        				<bean:define id="revenue" name="list"  property="revenue" type="java.lang.String" />    
						        				 <%
						        				 currency = new Double(revenue);
						        				 revenue=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
												 %>   
						        				<td  align="right" nowrap="nowrap"><%=revenue %></td>   
						        				<bean:define id="proFormaVGPM" name="list"  property="proFormaVGPM" type="java.lang.String" />   
						        				<%
						        				proFormaVGPM=proFormaVGPM.substring(0,proFormaVGPM.length()-2);
						        				%>  					       
						        				<td  align="right" nowrap="nowrap"><%=proFormaVGPM%></td>     					       
						        				<bean:define id="VGPM" name="list"  property="VGPM" type="java.lang.String" />   
						        				<% 
						        				VGPM=VGPM.substring(0,VGPM.length()-2);
						        				%>
						        				<td  align="right" nowrap="nowrap"><%=VGPM %></td>     					       
						        				<bean:define id="deltaVGPM" name="list"  property="deltaVGPM" type="java.lang.String" />   
						        				<%deltaVGPM=deltaVGPM.substring(0,deltaVGPM.length()-2); %>
						        				<td  align="right" nowrap="nowrap"><%=deltaVGPM %></td>   
						        				  <bean:define id="occurrences" name="list"  property="occurrences" type="java.lang.String" />    
						        				 <%
						        				 amount = new Double(occurrences);
						        				 occurrences=Formatter.format(amount).substring(1, Formatter.format(amount).indexOf("."));
												 %>   					       
						        				<td  align="right" nowrap="nowrap"><%=occurrences %></td>     					       
						        		<logic:greaterThan name="list"  property="VGPM" value=".50">
						        					<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-green.gif" alt=""/></td> 
						        				</logic:greaterThan>
						        				<logic:lessThan name="list" property="deltaVGPM" value=".00">
						        					<logic:lessEqual name="list" property="VGPM" value=".50">
						        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-red.gif" alt=""/></td>
						        					</logic:lessEqual>
						        				</logic:lessThan>	
						        				<logic:lessEqual name="list"  property="VGPM" value=".50">
						        					<logic:greaterEqual name="list" property="deltaVGPM" value=".00">
						        						<td class="noborder" align="left" nowrap="nowrap"></td>
						        					</logic:greaterEqual>
						        				</logic:lessEqual>	  
						        			</tr>
					        			</logic:iterate>
					        		</logic:present>
		  					</tbody>
		  						</table>
				        	</td>
				        	</tr>
				        	<tr>
								<td id="PageContentDate">Content Date:&nbsp;&nbsp;<span id="currDate"></span></td>
							  </tr>
	  					</table>
	  				</td>
	  			</tr>
	  		</table>
	  		</td>
	  		</tr>
	  		</table>

</html>