<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:insert page="../reportLayout.jsp" flush="true">
  	<tiles:put name="menu" value="financialReport/reportNavigator.jsp" />
  	
   	<%
   	String jobFilterType= (String)request.getAttribute("jobFilterType");
   	if(jobFilterType.indexOf(".")>0) {
   	jobFilterType=jobFilterType.substring(0,jobFilterType.indexOf("."));
   %>
  		<tiles:put name="title" value="<%=jobFilterType %>" />
  		
  		
  <% }else{ %>
  	<tiles:put name="title" value='<%=(String)request.getAttribute("jobFilterType") %>' />
  	<%} %>
    <tiles:put name="filter" value="financialReport/revenueExpense/RevenueExpenceReportFilter.jsp" />
    <tiles:put name="factBoard" value="demo/factBoard.jsp" />
    <tiles:put name="content" value="financialReport/revenueExpense/RevenueExpenseContent.jsp" />
</tiles:insert>