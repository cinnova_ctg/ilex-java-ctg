<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ page  import="java.text.NumberFormat,java.util.Locale" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<link href="/Reporting/css/style.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/tableSorter.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.tablesorter.js"></script>
<%
NumberFormat Formatter=NumberFormat.getCurrencyInstance(new Locale("en_US"));
Double currency,amount;
%>
<bean:define id="projectManagerName" name="RevenueExpenseForm" property="projectManagerName" />
<bean:define id="projectManagerID" name="RevenueExpenseForm" property="projectManagerID" />
<script>
var projectManagerName = '<%=projectManagerName%>';
var projectManagerID='<%=projectManagerID%>';
$(document).ready(function() {
	//$("#emptyTD").removeAttr('width');
	var url="RevenueExpenseAction.do?ref=revenueExpenseByProjectManager&parm=firstTime";
	$("#lcBreadCrumbText").html("<a href="+url+">Revenue/Expense by Project Manager</a>&nbsp;&nbsp;"+"<img src=images/historyBullets.gif height=5 width=5/>&nbsp;&nbsp;"+projectManagerName);
	$("#projectManagerTable_1 tr:even").addClass("even");
	$("#projectManagerTable_1 tr:odd").addClass("odd");
	
});
function switchingCall()
{
	var report=document.getElementById('test').value;
	var url = "RevenueExpenseAction.do?ref=projectManagerSecondLevelReports&projectManagerName="+projectManagerName+"&projectManagerID="+projectManagerID+"&reportName="+report;
	document.forms[0].action = url;
	document.forms[0].submit();	
	}
$(document).ready(function(){
            $(".noborder").each(function(){
                        $(this).css('border-right','none');
                        $(this).css('border-bottom','none');
                        $(this).css('border-top','none');
                        $(this).css('background-color','#FFFFFF');
                        $(this).css('width','32px');
            });
});
</script>
<title>Revenue By Job Owner</title>
</head>
<body>
	  		<table id="contentTable" border="0" cellspacing="0" cellpadding="0" style="padding: 0px; height: 328px;">
	  			<tr >
	  				<td>
		  				<table width="100%" border="0px">
			  				<tr>
				  				<td align="left"><h1 style="font-size: 11px; color:#2e3d80; white-space: nowrap;">&nbsp;&nbsp;Account Summary(All)</h1></td>
								<td align="right" style="padding-right:35px;">
										<html:select styleClass="select" styleId="test" name="RevenueExpenseForm" property="selectedSummaryPage" onchange="switchingCall();">
											<html:option value="ProjectManagerTTMSummary">TTM Summary</html:option>
											<html:option value="ProjectManagerAccountSummary(TTM)">Account Summary(TTM)</html:option>
											<html:option value="ProjectManagerAccountSummary(All)">Account Summary(All)</html:option>
										</html:select>
								</td>
							</tr>
		  				</table>
	  				</td>
	  				</tr>
	  					<tr style="padding: 0px;">	
				        	<td valign="top">
				        		<table width="100%" border="0" cellpadding="0" cellspacing="1" id="projectManagerTable_1" class="tablesorter">
		  						<thead>
			  						<tr>
				  						<th rowspan="3">Account</th>
				  						<th colspan="2">Expense</th>
				  						<th rowspan="2">Revenue</th>
				  						<th colspan="3">VGPM</th>
				  						<th rowspan="2">Count</th>
				  						<th rowspan="2" class="noborder"></th>
			  						</tr>
			  						<tr>
			  							<th nowrap="nowrap">Pro Forma</th>
			  							<th>Actual</th>
			  							<th nowrap="nowrap">Pro Forma</th>
			  							<th>Actual</th>
			  							<th>Delta</th>
			  						</tr>
			  				
		  						</thead>
		  			<tbody>
		  			
		  										<logic:present name="RevenueExpenseForm" property="detailedList">
					        			<logic:iterate id="list" name="RevenueExpenseForm" property="detailedList">
						       
						        			<tr>
						        				<td align="left" scope="colgroup" nowrap="nowrap" ><bean:write name="list" property="accountName" /></td>
						        				<bean:define id="pfExpense" name="list"  property="proFormaExpense" type="java.lang.String" />
						        					<% 	
						        					currency = new Double(pfExpense);
						        					pfExpense=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
													 %>
						        				<td  align="right" nowrap="nowrap"><%=pfExpense %></td>    
						        				<bean:define id="expense" name="list"  property="expense" type="java.lang.String" />    
						        				 <%
						        				 currency = new Double(expense);
						        				 expense=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
												 %>   					       
						        				<td  align="right" nowrap="nowrap"><%=expense%></td>     					       
						        				<bean:define id="revenue" name="list"  property="revenue" type="java.lang.String" />    
						        				 <%
						        				 currency = new Double(revenue);
						        				 revenue=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
												 %>
						        				<td align="right" nowrap="nowrap"><%=revenue%></td>     					       
						        				<bean:define id="proFormaVGPM" name="list"  property="proFormaVGPM" type="java.lang.String" />   
						        				<%
						        				proFormaVGPM=proFormaVGPM.substring(0,proFormaVGPM.length()-2);
						        				%>
						        				<td  align="center" nowrap="nowrap"><%=proFormaVGPM%></td>  
						        				<bean:define id="VGPM" name="list"  property="VGPM" type="java.lang.String" />   
						        				<% 
						        				VGPM=VGPM.substring(0,VGPM.length()-2);
						        				%>   					       
						        				<td  align="center" nowrap="nowrap"><%=VGPM%></td> 
						        				<bean:define id="deltaVGPM" name="list"  property="deltaVGPM" type="java.lang.String" />   
						        				<%deltaVGPM=deltaVGPM.substring(0,deltaVGPM.length()-2); %>
						        				<td align="center" nowrap="nowrap"><%=deltaVGPM%></td> 
						        				<bean:define id="occurrences" name="list"  property="occurrences" type="java.lang.String" />    
						        				 <%
						        				 amount = new Double(occurrences);
						        				 occurrences=Formatter.format(amount).substring(1, Formatter.format(amount).indexOf("."));
												 %>      					       
						        				<td  align="right" nowrap="nowrap"><%=occurrences %></td>     					       
						        		<logic:lessThan name="list" property="deltaVGPM" value=".00">
						        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-red.gif" alt=""/></td>
						        			</logic:lessThan>	
						        			<logic:greaterEqual name="list" property="deltaVGPM" value=".05">
						        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-green.gif" alt=""/></td>
						        			</logic:greaterEqual>
						        		<logic:greaterThan name="list"  property="VGPM" value=".55">
						        					<logic:greaterEqual name="list" property="deltaVGPM" value="0.00">
							        					<logic:lessThan name="list" property="deltaVGPM" value="0.05">
							        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-blue.gif" alt=""/></td>
							        					</logic:lessThan>
						        					</logic:greaterEqual>
						        	   </logic:greaterThan>	
						        			</tr>
						        			
					        			</logic:iterate>
					        		</logic:present>
		  					</tbody>
		  						</table>
				        	
	  				</td>
	  			</tr>
						   <tr height="15px"></tr>
						  <tr>
						  <td>
	  						<%@ include file="../../Legends.jsp" %>
	  					 </td>
	  			</tr>
	  			<tr>
							<td id="PageContentDate">Content Date:&nbsp;&nbsp;<span id="currDate"></span></td>
						  </tr>
	  		</table>
	  	</body>
	  	</html>