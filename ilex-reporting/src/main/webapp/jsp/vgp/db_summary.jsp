<%@ page language="java" contentType="text/html; charset=iso-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/Element.tld" prefix="el"%>
<%@ page import = "java.util.Map" %>
<html>
<head>
<%
	String spm_list = "select spm from datawarehouse.dbo.ac_dimension_ops_summary_managers where spm is not null and spm not in ('Shroder, Charles','Supinger, Bob', 'Courtney, Terezia', 'Carlton, Lance')order by spm";

	Map oob = (Map) request.getAttribute("oob");
	Map avg_oob = (Map) request.getAttribute("avg_oob");
	String select_bdm = "'" + (String) request.getAttribute("bdm")
			+ "'";
	if (select_bdm.equals("%")) {
		select_bdm = "";
	}
	String select_spm = "'" + (String) request.getAttribute("spm")
			+ "'";
	String q_spm = "";

	if (select_spm.equals("%")) {
		select_spm = "";
		q_spm = "";
	} else {

		/*
		 String[] q_spm_parts = select_spm.split(",");
		 if (q_spm_parts.length > 1) {
		 q_spm = q_spm_parts[1].trim();
		 }
		 q_spm = (q_spm_parts[1].trim()).substring(1,2)+q_spm_parts[0];
		 */
	}

	String spm = (String) request.getAttribute("spm");
	String style1 = "";
	String style1_restore = "";
	if (!spm.equals("%")) {
		style1 = "style =\"background-color: #FAF8CC\"";
		style1_restore = "style =\"background-color: #FAF8CC\"";
	}

	String style_red = "style =\"background-color: #ff9999\"";
	String style_green = "style =\"background-color: #99ff99\"";
%>
<title>Insert title here</title>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	color: #000000;
}
td {
   vertical-align:text-top;
}
.sectionHeader {
  font-size: 16px; font-weight: bold; color: #2E3D80;
  padding: 10px 10px 5px 5px;
}
.subSectionHeader {
  font-size: 12px; font-weight: bold; color: #2E3D80;
  padding: 10px 10px 5px 5px;
}
.FMLBL001 {
  font-size: 11px; 
  font-weight: bold; 
  padding: 2px 5px 2px 5px;
  text-align:left;
}
.FMCNT001 {
  font-size: 10px; 
  font-weight: normal; 
  padding: 2px 5px 2px 5px;
  text-align:left;
}
.FMCNT002 {
  font-size: 16px; 
  font-weight: bold; 
  padding: 4px 5px 6px 5px;
  text-align:left;
}
.TBHDR001 {
  font-size: 10px; font-weight: bold; 
  padding: 2px 5px 2px 5px;
  text-align:center;
}
.TBCNT001, .TBCNT001R, .TBCNT001C, .TBCNT001RBENCHMARK {
  font-size: 12px;
  padding: 2px 5px 2px 8px;
}
.TBCNT001R {
  text-align:right;
}
.TBCNT001C {
  text-align:center;
}
.TBCNT001RBENCHMARK {
  text-align:right;
  padding: 2px 15px 2px 8px;
}

.TBCNT002, .TBCNT002R, .TBCNT002C, .TBCNT002RBENCHMARK {
  font-size: 10px;
  padding: 2px 5px 2px 5px;
  white-space: nowrap;
}
.TBCNT002R {
  text-align:right;
}
.TBCNT002R {
  text-align:right;
}
.TBCNT002C {
  text-align:center;
}
.TBCNT002RBENCHMARK {
  text-align:right;
  padding: 2px 15px 2px 5px;
}
.TBCNT003, .TBCNT003R {
  font-size: 10px;
  text-indent:20px;
  padding: 2px 0px 2px 0px;
}
.TBCNT003R {
  text-align:right;
}
.TBCNT004, .TBCNT004R {
  font-size: 10px;
  padding: 2px 0px 2px 5px;
  white-space: nowrap;
}
.TBCNT004R {
  text-align:right;
}
.TBCNT005, .TBCNT005R, .TBCNT005RBENCHMARK  {
  font-size: 12px;
  padding: 5px 5px 3px 5px;
  text-decoration: underline;
}
.TBCNT005R {
  text-align:right;
}
.TBCNT005RBENCHMARK {
  text-align:right;
  padding: 5px 15px 3px 5px;
}
.TBDIV001 {
  padding: 10px 10px 5px 5px;
}
.TBDIV002 {
  padding: 5px 0px 10px 10px;
}
.PCLNK001 {
  padding: 2px 5px 5px 5px;
  background-color:#E1E1E1;
  border-style:solid; border-width:1px; border-color: #878787;
 }
.PCLNK002 {
  padding: 5px 5px 5px 5px;
}
.TBP {
  
  background-color:#ECEEF0 ;
  
}
.menu {
  font-size: 10px; font-weight: normal; 
  padding: 2px 0px 2px 0px;
  style="border-style:solid; border-width:1px; border-color: #ABABAB;
}
</style>

<style>
#mask {
  position:absolute;
  left:0;
  top:0;
  z-index:9000;
  background-color:#C6D9FD;
  display:none;
}


.PUWIN001 {
  margin : 5px;
  border:3px solid #B2B2B2;
  width : 500px;
  height : 300px;
  position: absolute;
  left: 0px;
  top: 0px;
  display:none;
  z-index:9999;
}

.PUTTL001 {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px; font-weight: bold; color: #2E3D80;
  background-color : #FFFFC4;
  border-top : 1px solid #B2B2B2;
  border-bottom : 1px solid #B2B2B2;
  padding : 4px;
}

.PUCNT001 {
  margin : 5px;
  background-color : #ffffff;
  width : 100%;
  height : 100%;
}

.PUCNTHDR01 {
  font-family: Arial, Helvetica, sans-serif;
  text-align: center;
  font-size: 12px; font-weight: bold; color: #000000;
  background-color : #ECEEF0;
  padding : 4px;
}

</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript"></script>
<script src="http://jquery-ui.googlecode.com/svn/tags/latest/external/jquery.bgiframe-2.1.2.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/i18n/jquery-ui-i18n.min.js" type="text/javascript"></script>

<script language="JavaScript" src="vgp/FusionCharts.js"></script>
 
<script type="text/javascript">

//alert(<%=q_spm%>)

$(function() {

	// Set form parameters based on request attributes
	$('[tf=<%=request.getAttribute("period")%>]').removeClass('PCLNK002').addClass('PCLNK001');
	$('[name=bdm1]').val(<%=select_bdm%>)
	$('[name=spm1]').val(<%=select_spm%>)	
	
	// Bind events
	$('[tf]').bind('click', handlePeriod);
	$('[name=bdm1]').change(handleDropDown);
	$('[name=spm1]').change(handleDropDown);
	
	// Default formatting
	
	$('[tp=TBP]').addClass('TBP');
});

function handlePeriod() {
	
	// jQ object for event element
	menuItem = $(this);
	$('#period1').val(menuItem.text());
	submitForm();
}

function handleDropDown() {
	
	// jQ object for event element
	menuItem = $(this);
	// Get ID for the respective menu drop down item
	menuValue = '#'+menuItem.attr('name');
	// Set the hidden form value to the selected value
	$(menuValue).val(menuItem.val());
	submitForm();
}

function submitForm () {
	$('#form1').submit();
}
</script>

<script type="text/javascript">

window_x_delta = 540;
winddows_open = 0;

$(document).ready(function() {
	$('#phoneInfo').click(function(evt){
                popup = '#phonepopup';

                //Set the popup window to center
                $(popup).css('top',  evt.pageY-50);
                $(popup).css('left', evt.pageX+30);

                //transition effect
                $(popup).fadeIn();
                $(popup).draggable();
		$("#phonepopupdata").html("<table style='width:100%; padding: 3px;'>"+
"<tr><td style='width:25%; vertical-align:middle;'><b>Live Answer</b>:</td><td>Percentage of calls answered in under 30 seconds.</td></tr>"+
"<tr><td style='width:25%; vertical-align:middle;'><b>Field Queue</b>:</td><td>Average Time in seconds that the Tech Line is answered.</td></tr>"+
"<tr><td style='width:25%; vertical-align:middle;'><b>Client Queue</b>:</td><td>Average Time in seconds that all lines minus the Tech Line are answered.</td></tr>"+
"<tr><td style='width:25%; vertical-align:middle;'><b>On Hold</b>:</td><td>Solid count of calls on hold.</td></tr>"+
"</table>");
	})

        $('#networkInfo').click(function(evt){
                popup = '#networkpopup';

                //Set the popup window to center
                $(popup).css('top',  evt.pageY-50);
                $(popup).css('left', evt.pageX+30);

                //transition effect
                $(popup).fadeIn();
                $(popup).draggable();
                $("#networkpopupdata").html("<table style='width:100%; padding: 3px;'>"+
"<tr><td style='width:25%; vertical-align:middle;'><b>WAN Uptime</b>:</td><td>Uptime percentage of all managed circuits.</td></tr>"+
"<tr><td style='width:25%; vertical-align:middle;'><b>Down</b>:</td><td>Solid count of sites with their primary circuit currently down.</td></tr>"+
"<tr><td style='width:25%; vertical-align:middle;'><b>Down Down</b>:</td><td>Solid count of sites with their primary and backup (EVDO or Other) currently down.</td></tr>"+
"</table>");

        })

        $('#installsInfo').click(function(evt){
                popup = '#installspopup';

                //Set the popup window to center
                $(popup).css('top',  evt.pageY-50);
                $(popup).css('left', evt.pageX+30);

                //transition effect
                $(popup).fadeIn();
                $(popup).draggable();
                $("#installspopupdata").html("<table style='width:100%; padding: 3px;'>"+
"<tr><td style='width:25%; vertical-align:middle;'><b>Scheduled</b>:</td><td>Solid count of installs scheduled for the given period.</td></tr>"+
"<tr><td style='width:25%; vertical-align:middle;'><b>Completed</b>:</td><td>Solid count of completed installs for the given period.</td></tr>"+
"<tr><td style='width:25%; vertical-align:middle;'><b>Failed/Rescheduled</b>:</td><td>Solid count of Failed & Rescheduled installs for the given period.</td></tr>"+
"<tr><td style='width:25%; vertical-align:middle;'><b>Completetion(%)</b>:</td><td>Percent of successful installations.</td></tr>"+
"</table>");

        })

	$('#chart001').click(function(evt) {
				
//		return;
		
		$.ajaxSetup({cache: false});
		
		popup = '#chart001popup';
		
		//Set the popup window to center
		$(popup).css('top',  evt.pageY-50);
		$(popup).css('left', evt.pageX+30);
		
		//transition effect
		$(popup).fadeIn();
		$(popup).draggable();
		
		$.post("TL2CHT.xo?ac=profitability_chart1", $("#form2").serialize(), function(data){
			$("#chart001popupdata").html(data);
		});
	})
	
	$('#chart002').click(function(evt) {
		
//		return;
		
		$.ajaxSetup({cache: false});
		
		popup = '#chart002popup';
		
		//Set the popup window to center
		$(popup).css('top',  evt.pageY+30);
		$(popup).css('left', evt.pageX+40);
		
		//transition effect
		$(popup).fadeIn();
		$(popup).draggable();
		
		$("#chart002popupdata").html('');
		
		$.post("TL2CHT.xo?ac=revenue_chart1", $("#form2").serialize(), function(data){
			$("#chart002popupdata").html(data);
		});
	})
	
	$('#chart003').click(function(evt) {
		
//		return;
		
		$.ajaxSetup({cache: false});
			
		popup = $('#'+$(this).attr('id')+'popup');
			
		//Set the popup window to center
		$(popup).css('top',  evt.pageY-200);
		$(popup).css('left', evt.pageX+40);
		
		//transition effect
		$(popup).fadeIn(); 
		$(popup).draggable();
		$("#chart003popupdata").load("TL2CHT.xo?ac=purchase_chart1", $("#form1").serialize())
	})
	
	$('.close-chart').click(function(evt) {
		win = '#'+$(this).attr('win');
		$(win).fadeOut(); 
	})
	
});


</script>

</head>
<body>

<table border="0" cellpadding="0" cellspacing="0">
<tr><td colspan="7">
<div class="TBDIV002"><table border="0" cellpadding="0" cellspacing="0" id="c12">
<tr>
<td class="FMLBL001" style="vertical-align:bottom;">SPM:&nbsp;<el:element type="menu1" request="<%=request%>" name="spm1" source="database" data ="<%=spm_list%>" js=""/></td>
<td class="FMCNT002">&nbsp;&nbsp;<a href="#" class="PCLNK002" onclick(highlightPeriod(30)) tf="30">30</a><a href="#" class="PCLNK002" onclick(highlightPeriod(60) tf="60">60</a><a href="#" class="PCLNK002" onclick(highlightPeriod(90) tf="90">90</a><a href="#" class="PCLNK002" onclick(highlightPeriod(180) tf="180">180</a><a href="#" class="PCLNK002" onclick(highlightPeriod(360) tf="360">360</a></td>
</tr>
</table></div>
</td></tr></table>
<table border="0" cellpadding="0" cellspacing="0" style="border-style:solid; border-width:2px; border-color: #ABABAB; padding:10px; margin: 0px 10px 10px 10px;"><tr><td><table border="0" cellpadding="0" cellspacing="0">
<!-- 4 Columns for primary content -->
<tr><td><table>
<!-- Column 1 -->
<tr><td><table width="100%"  border="0" cellpadding="0" cellspacing="0" id="c11"><tr><td class="sectionHeader">Profitability</td><td class="sectionHeader" align="right"><img src="vgp/chart.png" id="chart001"></td></tr></table></td></tr>
<tr><td>
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c11">
<tr>
  <td class="TBHDR001" width="100">&nbsp;</td>
  <td class="TBHDR001">Selected</td>
  <td class="TBHDR001">Benchmark</td>
  <td class="TBHDR001">Hi</td>
  <td class="TBHDR001">Low</td>
  <td class="TBHDR001">Overall</td>
</tr>

<%
	// Start of profitability/revenue sections - get main data structure
	Map<String, Map<String, String>> rows = (Map) request.getAttribute("rows");

	Map<String, String> row = rows.get("Projects");
	String selected = "";
	String benchmark = "";
	String hi = "";
	String low = "";
	String total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("m_selected");
		benchmark = row.get("m_benchmark_all");
		hi        = row.get("m_hi");
		low       = row.get("m_low");
		total     = row.get("m_overall");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">Projects&nbsp;VGP</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
	style1 = style1_restore;

    selected  = row.get("p_selected");
    benchmark = row.get("p_benchmark_all");
    hi        = row.get("p_hi");
    low       = row.get("p_low");
    total     = row.get("p_overall");
%>
<tr>
 <td class="TBCNT003">Pro&nbsp;Forma</td>
  <td class="TBCNT002R"<%=style1%>><%=selected%></td>
  <td class="TBCNT002RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT002R"><%=hi%></td>
  <td class="TBCNT002R"><%=low%></td>
  <td class="TBCNT002R"><%=total%></td>
</tr>

<%
	row = rows.get("NetMedX");
	selected = "";
	benchmark = "";
	hi = "";
	low = "";
	total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("m_selected");
		benchmark = row.get("m_benchmark_all");
		hi        = row.get("m_hi");
		low       = row.get("m_low");
		total     = row.get("m_overall");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">NetMedX&nbsp;VGP</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
	style1 = style1_restore;

    selected  = row.get("p_selected");
    benchmark = row.get("p_benchmark_all");
    hi        = row.get("p_hi");
    low       = row.get("p_low");
    total     = row.get("p_overall");
%>
<tr>
 <td class="TBCNT003">Pro&nbsp;Forma</td>
  <td class="TBCNT002R"<%=style1%>><%=selected%></td>
  <td class="TBCNT002RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT002R"><%=hi%></td>
  <td class="TBCNT002R"><%=low%></td>
  <td class="TBCNT002R"><%=total%></td>
</tr>

<%
	row = rows.get("Blended");
	selected = "";
	benchmark = "";
	hi = "";
	low = "";
	total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("m_selected");
		benchmark = row.get("m_benchmark_all");
		hi        = row.get("m_hi");
		low       = row.get("m_low");
		total     = row.get("m_overall");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">Blended&nbsp;VGP</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
	style1 = style1_restore;

    selected  = row.get("p_selected");
    benchmark = row.get("p_benchmark_all");
    hi        = row.get("p_hi");
    low       = row.get("p_low");
    total     = row.get("p_overall");
%>
<tr>
 <td class="TBCNT003">Pro&nbsp;Forma</td>
  <td class="TBCNT002R"<%=style1%>><%=selected%></td>
  <td class="TBCNT002RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT002R"><%=hi%></td>
  <td class="TBCNT002R"><%=low%></td>
  <td class="TBCNT002R"><%=total%></td>
</tr>
<%
	row = rows.get("EverWorX Operations");
	selected = "";
	benchmark = "";
	hi = "";
	low = "";
	total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("m_selected");
		benchmark = row.get("m_benchmark_all");
		hi        = row.get("m_hi");
		low       = row.get("m_low");
		total     = row.get("m_overall");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">Managed&nbsp;VGP</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
	style1 = style1_restore;

    selected  = row.get("p_selected");
    benchmark = row.get("p_benchmark_all");
    hi        = row.get("p_hi");
    low       = row.get("p_low");
    total     = row.get("p_overall");
%>
<tr>
 <td class="TBCNT003">Pro&nbsp;Forma</td>
  <td class="TBCNT002R"<%=style1%>><%=selected%></td>
  <td class="TBCNT002RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT002R"><%=hi%></td>
  <td class="TBCNT002R"><%=low%></td>
  <td class="TBCNT002R"><%=total%></td>
</tr>

<%
	row = rows.get("EverWorX Deployment");
	selected = "";
	benchmark = "";
	hi = "";
	low = "";
	total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("m_selected");
		benchmark = row.get("m_benchmark_all");
		hi        = row.get("m_hi");
		low       = row.get("m_low");
		total     = row.get("m_overall");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">EverWorX&nbsp;Deployment&nbsp;VGP</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
	style1 = style1_restore;

    selected  = row.get("p_selected");
    benchmark = row.get("p_benchmark_all");
    hi        = row.get("p_hi");
    low       = row.get("p_low");
    total     = row.get("p_overall");
%>
<tr>
 <td class="TBCNT003">Pro&nbsp;Forma</td>
  <td class="TBCNT002R"<%=style1%>><%=selected%></td>
  <td class="TBCNT002RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT002R"><%=hi%></td>
  <td class="TBCNT002R"><%=low%></td>
  <td class="TBCNT002R"><%=total%></td>
</tr>

<%
	row = rows.get("Total");
	selected = "";
	benchmark = "";
	hi = "";
	low = "";
	total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("m_selected");
		benchmark = row.get("m_benchmark_all");
		hi        = row.get("m_hi");
		low       = row.get("m_low");
		total     = row.get("m_overall");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">Total&nbsp;VGP</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
	style1 = style1_restore;

    selected  = row.get("p_selected");
    benchmark = row.get("p_benchmark_all");
    hi        = row.get("p_hi");
    low       = row.get("p_low");
    total     = row.get("p_overall");
%>
<tr>
 <td class="TBCNT003">Pro&nbsp;Forma</td>
  <td class="TBCNT002R"<%=style1%>><%=selected%></td>
  <td class="TBCNT002RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT002R"><%=hi%></td>
  <td class="TBCNT002R"><%=low%></td>
  <td class="TBCNT002R"><%=total%></td>
</tr>
 
</table></div></td></tr>
 
<tr><td><table width="100%" border="0" cellpadding="0" cellspacing="0" id="c11"><tr><td class="sectionHeader">Revenue (000)</td><td class="sectionHeader" align="right"><img src="vgp/chart.png"  id="chart002"></td></tr></table></td></tr>
<tr><td>
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c12">
<tr>
  <td class="TBHDR001" width="100">&nbsp;</td>
  <td class="TBHDR001">Selected</td>
  <td class="TBHDR001">Benchmark</td>
  <td class="TBHDR001">Hi</td>
  <td class="TBHDR001">Low</td>
  <td class="TBHDR001">Total</td>
</tr>

<%
    //  Revenue sections
    
	row = rows.get("Projects");
	selected = "";
	benchmark = "";
	hi = "";
	low = "";
	total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("r_selected");
		benchmark = row.get("r_benchmark_all");
		hi        = row.get("r_hi");
		low       = row.get("r_low");
		total     = row.get("r_total");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">Projects</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>

<%	
	row = rows.get("NetMedX");
	selected = "";
	benchmark = "";
	hi = "";
	low = "";
	total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("r_selected");
		benchmark = row.get("r_benchmark_all");
		hi        = row.get("r_hi");
		low       = row.get("r_low");
		total     = row.get("r_total");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">NetMedX</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>

<%	
	row = rows.get("Blended");
	selected = "";
	benchmark = "";
	hi = "";
	low = "";
	total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("r_selected");
		benchmark = row.get("r_benchmark_all");
		hi        = row.get("r_hi");
		low       = row.get("r_low");
		total     = row.get("r_total");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">Blended</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>

<%	
	row = rows.get("EverWorX Operations");
	selected = "";
	benchmark = "";
	hi = "";
	low = "";
	total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("r_selected");
		benchmark = row.get("r_benchmark_all");
		hi        = row.get("r_hi");
		low       = row.get("r_low");
		total     = row.get("r_total");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">Managed</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>

<%	
	row = rows.get("EverWorX Deployment");
	selected = "";
	benchmark = "";
	hi = "";
	low = "";
	total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("r_selected");
		benchmark = row.get("r_benchmark_all");
		hi        = row.get("r_hi");
		low       = row.get("r_low");
		total     = row.get("r_total");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">EverWorX Deployment</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%	
	row = rows.get("Total");
	selected = "";
	benchmark = "";
	hi = "";
	low = "";
	total = "";

	if (row != null & row.size() > 0) {

		selected  = row.get("r_selected");
		benchmark = row.get("r_benchmark_all");
		hi        = row.get("r_hi");
		low       = row.get("r_low");
		total     = row.get("r_total");

		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr>
 <td class="TBCNT001">Total</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
	style1 = style1_restore;
%>

</table></div>
</td></tr>
<tr><td class="sectionHeader">Invoice Delays</td></tr>
<tr><td>
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c13" >
<tr><td class="TBHDR001" width="100">&nbsp;</td><td class="TBHDR001" width="62">Selected</td><td class="TBHDR001" width="62">Average</td></tr>
<tr><td class="TBCNT001">Operations</td><td class="TBCNT001R"><%=request.getAttribute("complete_delay")%></td><td class="TBCNT001R"><%=request.getAttribute("avg_complete_delay")%></td></tr>
<tr><td class="TBCNT001" class="TBCNT001">Accounting</td><td class="TBCNT001R"><%=request.getAttribute("closed_delay")%></td><td class="TBCNT001R"><%=request.getAttribute("avg_closed_delay")%></td></tr>
<tr><td class="TBCNT001" class="TBCNT001">Actual Invoice</td><td class="TBCNT001R"><%=request.getAttribute("invoice_delay")%></td><td class="TBCNT001R"><%=request.getAttribute("avg_invoice_delay")%></td></tr>
</table></div>
</td></tr>
</table></td>
<td width="1" bgcolor="#858585"></td>
<td><table>
<!-- Column 2 -->
<tr><td class="sectionHeader">Out&nbsp;of&nbsp;Bounds</td></tr>
<tr><td>
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c21">
<tr><td class="TBHDR001" width="110">&nbsp;</td>
<td class="TBHDR001">Selected</td>
<td class="TBHDR001">Benchmark</td>
<td class="TBHDR001">Hi</td>
<td class="TBHDR001">Low</td>
<td class="TBHDR001">Total</td>
</tr>

<%
	String condition = "";
	Map s = (Map) request.getAttribute("Selected");
	Map b = (Map) request.getAttribute("Benchmark");
	Map h = (Map) request.getAttribute("Hi");
	Map l = (Map) request.getAttribute("Low");
	Map t = (Map) request.getAttribute("Total");

	/*
	 To Be Scheduled
	 To Be Scheduled Over-1
	 Scheduled
	 Scheduled Over-1
	 Onsite
	 Onsite Over-1
	 Offsite
	 Offsite Over-2
	 Offsite Over/Del-7
	 Confirmed
	 Complete
	 Complete Over-5 
	 */
%>

<%
	condition = "To Be Scheduled Total";

	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			//style1 = style_green;
		} else if (selected.equals(hi)) {
			//style1 = style_red;
		}
	}
%>

<tr>
<td class="TBCNT005">To&nbsp;Be&nbsp;Scheduled</td>
<td class="TBCNT005R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT005RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT005R"></td>
<td class="TBCNT005R"></td>
<td class="TBCNT005R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "To Be Scheduled";

	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			//style1 = style_green;
		} else if (selected.equals(hi)) {
			//style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT003">OK</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "To Be Scheduled Over-1";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			style1 = style_green;
		} else if (selected.equals(hi)) {
			style1 = style_red;
		}
	}
%>
<tr><td class="TBCNT003">Over-1</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Scheduled Total";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			style1 = style_green;
		} else if (selected.equals(hi)) {
			style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT005">Scheduled</td>
<td class="TBCNT005R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT005RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT005R"></td>
<td class="TBCNT005R"></td>
<td class="TBCNT005R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Scheduled";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			//style1 = style_green;
		} else if (selected.equals(hi)) {
			//style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT003">OK</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Scheduled Over-1";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			style1 = style_green;
		} else if (selected.equals(hi)) {
			style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT003">Over-1</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "In Work";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			//style1 = style_green;
		} else if (selected.equals(hi)) {
			//style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT005">In Work</td>
<td class="TBCNT005R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT005RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT005R"></td>
<td class="TBCNT005R"></td>
<td class="TBCNT005R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Confirmed";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			//style1 = style_green;
		} else if (selected.equals(hi)) {
			//style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT003">PO Over-1</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Onsite";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			//style1 = style_green;
		} else if (selected.equals(hi)) {
			//style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT003">OK Onsite</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Onsite Over-1";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			style1 = style_green;
		} else if (selected.equals(hi)) {
			style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT003">Onsite Over-1</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Offsite";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			//style1 = style_green;
		} else if (selected.equals(hi)) {
			//style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT003">OK Offsite</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Offsite Over-2";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			style1 = style_green;
		} else if (selected.equals(hi)) {
			style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT003">Offsite Over-2</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Offsite Over/Del-7";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			style1 = style_green;
		} else if (selected.equals(hi)) {
			style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT003">Offsite Del>7</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Complete Total";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			//style1 = style_green;
		} else if (selected.equals(hi)) {
			//style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT005">Complete</td>
<td class="TBCNT005R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT005RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT005R"></td>
<td class="TBCNT005R"></td>
<td class="TBCNT005R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Complete";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			//style1 = style_green;
		} else if (selected.equals(hi)) {
			//style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT003">OK</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

<%
	condition = "Complete Over-5";
	selected = (String) s.get(condition);
	hi = (String) h.get(condition);
	low = (String) l.get(condition);

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(low)) {
			style1 = style_green;
		} else if (selected.equals(hi)) {
			style1 = style_red;
		}
	}
%>
<tr>
<td class="TBCNT003">Over-5</td>
<td class="TBCNT002R"<%=style1%>><%=s.get(condition)%></td>
<td class="TBCNT002RBENCHMARK"><%=b.get(condition)%></td>
<td class="TBCNT002R"><%=h.get(condition)%></td>
<td class="TBCNT002R"><%=l.get(condition)%></td>
<td class="TBCNT002R"><%=t.get(condition)%></td>
</tr>

</table></div>
</td></tr>

<tr><td>
<%
selected = "";
benchmark = "XX.X";
hi = "XX.X";
low = "XX.X";
total = "XXXXX";
%>
<div class="TBDIV001">
<table width="100%"  border="0" cellpadding="0" cellspacing="0" id="c11">
  <tr>
    <td class="sectionHeader">Quality Management</td>
    <td class="sectionHeader" align="right">&nbsp;</td>
  </tr>
</table>
</td></tr>
<tr><td id="c32">
<table border="0" cellpadding="0" cellspacing="0" id="c31">
<tr>
  <td class="subSectionHeader" colspan="6">Contingent</td>
</tr>
<%
String[] stats;
%>
<tr>
  <td class="TBHDR001" width="96">&nbsp;</td>
  <td class="TBHDR001" nowrap>Selected(%)</td>
  <td class="TBHDR001" nowrap>Benchmark(%)</td>
  <td class="TBHDR001" nowrap>Hi(%)</td>
  <td class="TBHDR001" nowrap>Low(%)</td>
  <td class="TBHDR001" nowrap>Total</td>
</tr>
<%
style1 = "";
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("Poke");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">Professional</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("pOke");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">On Time</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("poKe");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">Knowledgeable</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("pokE");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">Equipped</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
</table>
</div>
</td></tr>

<tr><td id="c32">
<table border="0" cellpadding="0" cellspacing="0" id="c31">

<tr>
  <td class="subSectionHeader" colspan="6">Client HQ</td>
</tr>
<%
selected = "";
benchmark = "XX.X";
hi = "XX.X";
low = "XX.X";
total = "XXXXX";
%>
<tr>
  <td class="TBHDR001" width="100">&nbsp;</td>
  <td class="TBHDR001" nowrap>Selected(%)</td>
  <td class="TBHDR001" nowrap>Benchmark(%)</td>
  <td class="TBHDR001" nowrap>Hi(%)</td>
  <td class="TBHDR001" nowrap>Low(%)</td>
  <td class="TBHDR001" nowrap>Total</td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("QA-H-P");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">Professional</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("QA-H-O");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">On Time</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
</table>
</div>
</td></tr>

<tr><td id="c32">
<table border="0" cellpadding="0" cellspacing="0" id="c31">

<tr>
  <td class="subSectionHeader" colspan="6">Client Site</td>
</tr>
<%
selected = "";
benchmark = "XX.X";
hi = "XX.X";
low = "XX.X";
total = "XXXXX";
%>
<tr>
  <td class="TBHDR001" width="110">&nbsp;</td>
  <td class="TBHDR001">Selected</td>
  <td class="TBHDR001">Benchmark</td>
  <td class="TBHDR001">Hi</td>
  <td class="TBHDR001">Low</td>
  <td class="TBHDR001">Total</td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("QA-S-Overall");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">Overall&nbsp;(0-10)</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("QA-S-PKE");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001" nowrap>Technician-PKE (%)</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("QA-S-O");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">On Time (%)</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("QA-S-Issue");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">Issues (0-10+)</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
</table>
</div>
</td></tr>

</table></td>

<td width="1" bgcolor="#858585"></td>
<td><table>
<!-- Column 3 -->
<tr><td class="sectionHeader">Productivity</td></tr>
<tr><td>
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c31">
<tr>
 <td class="TBHDR001" width="142">&nbsp;</td>
 <td class="TBHDR001">Selected</td><td class="TBHDR001">Benchmark</td>
 <td class="TBHDR001">&nbsp;&nbsp;High&nbsp;&nbsp;</td>
 <td class="TBHDR001">&nbsp;&nbsp;Low&nbsp;&nbsp;</td>
 <td class="TBHDR001">&nbsp;Overall&nbsp;</td>
</tr>

<%
	Map number_employees = (Map) request
			.getAttribute("number_employees");
	Map revenue_employees = (Map) request
			.getAttribute("revenue_employees");
	Map jobs_employees = (Map) request.getAttribute("jobs_employees");
	Map revenue_job = (Map) request.getAttribute("revenue_job");
%>

<%
	selected = (String) revenue_employees.get("selected");
	hi = (String) revenue_employees.get("high");
	low = (String) revenue_employees.get("low");

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr><td class="TBCNT001">Revenue/Employee (000)</td>
<td class="TBCNT001R"<%=style1%>><%=revenue_employees.get("selected")%></td>
<td class="TBCNT001R"><%=revenue_employees.get("benchmark")%></td>
<td class="TBCNT001R"><%=revenue_employees.get("high")%></td>
<td class="TBCNT001R"><%=revenue_employees.get("low")%></td>
<td class="TBCNT001R"><%=revenue_employees.get("total")%></td>
</tr>
<%
	selected = (String) jobs_employees.get("selected");
	hi = (String) jobs_employees.get("high");
	low = (String) jobs_employees.get("low");

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr><td class="TBCNT001">Jobs/Employee</td>
<td class="TBCNT001R"<%=style1%>><%=jobs_employees.get("selected")%></td>
<td class="TBCNT001R"><%=jobs_employees.get("benchmark")%></td>
<td class="TBCNT001R"><%=jobs_employees.get("high")%></td>
<td class="TBCNT001R"><%=jobs_employees.get("low")%></td>
<td class="TBCNT001R"><%=jobs_employees.get("total")%></td>
</tr>
<%
	selected = (String) revenue_job.get("selected");
	hi = (String) revenue_job.get("high");
	low = (String) revenue_job.get("low");

	style1 = style1_restore;
	if (selected != null) {
		if (selected.equals(hi)) {
			style1 = style_green;
		} else if (selected.equals(low)) {
			style1 = style_red;
		}
	}
%>
<tr><td class="TBCNT001">Revenue/Job</td>
<td class="TBCNT001R"<%=style1%>><%=revenue_job.get("selected")%></td>
<td class="TBCNT001R"><%=revenue_job.get("benchmark")%></td>
<td class="TBCNT001R"><%=revenue_job.get("high")%></td>
<td class="TBCNT001R"><%=revenue_job.get("low")%></td>
<td class="TBCNT001R"><%=revenue_job.get("total")%></td>
</tr>
<tr>
 <td class="TBHDR001" width="142">&nbsp;</td>
 <td class="TBHDR001">&nbsp;</td>
 <td class="TBHDR001">&nbsp;</td>
 <td class="TBHDR001">&nbsp;</td>
 <td class="TBHDR001">&nbsp;</td>
 <td class="TBHDR001">Total</td>
</tr>

<tr>
<td class="TBCNT001">Number of Employees</td>
<td class="TBCNT001R"<%=style1%>><%=number_employees.get("selected")%></td>
<td class="TBCNT001R"><%=number_employees.get("benchmark")%></td>
<td class="TBCNT001R"><%=number_employees.get("high")%></td>
<td class="TBCNT001R"><%=number_employees.get("low")%></td>
<td class="TBCNT001R"><%=number_employees.get("total")%></td>
</tr>

<tr>
<td class="TBCNT001">Company Reference</td>
<td class="TBCNT001R" colspan="2">FTEs:&nbsp;<%=request.getAttribute("fte_employee_count")%></td>
<td class="TBCNT001R" colspan="3">Revenue(000)/FTE:&nbsp;<%=request.getAttribute("fte_employee_revenue")%></td>
</tr>

<tr>
<td colspan="6">&nbsp;</td>
</tr>
<tr><td class="TBCNT001">User Actions</td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td></tr>

<tr><td class="TBCNT003">PPS</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td></tr>
<tr><td class="TBCNT003">Non-PPS</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td></tr>

<tr><td class="TBCNT001" class="TBCNT001">User&nbsp;Actions/Employee</td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td></tr>

<tr><td class="TBCNT003">PPS</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT002R" tp="TBP"> </td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td></tr>
<tr><td class="TBCNT003">Non-PPS</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT002R" tp="TBP"> </td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td></tr>

<tr><td class="TBCNT001" class="TBCNT001">Workload</td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td></tr>

<tr><td class="TBCNT003">Client</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td></tr>
<tr><td class="TBCNT003">Appendices</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td></tr>
<tr><td class="TBCNT003">Jobs</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT002R" tp="TBP">&nbsp;</td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td><td class="TBCNT001R" tp="TBP"></td></tr>

</table></div>  
</td></tr>
<%
	String chart_link = "&nbsp;";
	if (!spm.equals("%")) {
		chart_link = "<img src=\"vgp/chart.png\" id=\"chart003\">";
	}
%>

<tr><td>

<div class="TBDIV001">
<table width="100%"  border="0" cellpadding="0" cellspacing="0" id="c11">
  <tr>
    <td class="sectionHeader">Purchasing</td>
    <td class="sectionHeader" align="right"><%=chart_link%></td>
  </tr>
</table>
</td></tr>
<tr><td id="c32">
<table border="0" cellpadding="0" cellspacing="0" id="c31">
<tr>
  <td class="TBHDR001" width="100">&nbsp;</td>
  <td class="TBHDR001">Selected(%)</td>
  <td class="TBHDR001">Benchmark(%)</td>
  <td class="TBHDR001">Hi(%)</td>
  <td class="TBHDR001">Low(%)</td>
  <td class="TBHDR001">Total</td>
</tr>
<%
style1 = "";
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("MM2");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">Minuteman</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("SP2");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">Speedpay</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("PVS2");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">Standard</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("LUA");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">LUA</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
<%
selected = "";
benchmark = "";
hi = "";
low = "";
total = "";
stats = (String[])request.getAttribute("Future");
if (stats != null) {
	selected = stats[0];
	benchmark = stats[1];
	hi = stats[2];
	low = stats[3];
	total = stats[4];
}
%>
<tr>
 <td class="TBCNT001">Forecast</td>
  <td class="TBCNT001R"<%=style1%>><%=selected%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
  <td class="TBCNT001R"><%=hi%></td>
  <td class="TBCNT001R"><%=low%></td>
  <td class="TBCNT001R"><%=total%></td>
</tr>
</table>
</div>
</td></tr>

<!-- Hide old MM/SP/Standard
<tr><td>
<div class="TBDIV001">
<table border="0" cellpadding="0" cellspacing="0" id="c31">
<tr><td class="TBHDR001" width="142">&nbsp;</td><td class="TBHDR001">Selected</td><td class="TBHDR001">Average</td></tr>
<tr><td class="TBCNT001">Minuteman</td><td class="TBCNT001R"><%=request.getAttribute("minuteman")%></td><td class="TBCNT001R"><%=request.getAttribute("avg_minuteman")%></td></tr>
<tr><td class="TBCNT001">Speedpay</td><td class="TBCNT001R"><%=request.getAttribute("speedpay")%></td><td class="TBCNT001R"><%=request.getAttribute("avg_speedpay")%></td></tr>
<tr><td class="TBCNT001">Standard</td><td class="TBCNT001R"><%=request.getAttribute("standard")%></td><td class="TBCNT001R"><%=request.getAttribute("avg_standard")%></td></tr>
</table>
</div>
</td></tr>
-->

<tr><td>

<div class="TBDIV001">
<table width="100%"  border="0" cellpadding="0" cellspacing="0" id="c11">
  <tr>
    <td class="sectionHeader">Network & Help Desk</td>
    <td class="sectionHeader" align="right"><%=chart_link%></td>
  </tr>
</table>
</td></tr>
<tr><td id="c32">
<table border="0" cellpadding="0" cellspacing="0" id="c31">
<tr>
  <td class="subSectionHeader" colspan="1">Phones <img style="float:right;"src="vgp/help.png" id="phoneInfo"></td>
</tr>
<tr>
  <td class="TBHDR001" width="100">&nbsp;</td>
  <td class="TBHDR001">Actual</td>
  <td class="TBHDR001">Goal</td>
</tr>
<%

int ibenchmark = 90;

Map<String, String> network_help_desk = (Map<String, String>)request.getAttribute("network_help_desk");

	String Live_Answer = network_help_desk.get("Live Answer");
	if (Live_Answer==null)Live_Answer="0";
	int iLive_Answer = Integer.parseInt(Live_Answer);
        String style3 = "";
        String style3_restore = "";
        if (iLive_Answer <= ibenchmark) {
                style3 = style_green;
        }
	else {
		style3 = style_red;
	}


%>
<tr>
 <td class="TBCNT001">Live Answer</td>
  <td class="TBCNT001R"<%=style3%>><%=Live_Answer%>%</td>
  <td class="TBCNT001RBENCHMARK"><%=ibenchmark%>%</td>
</tr>

<%
	ibenchmark = 30;
        String Field_Queue = network_help_desk.get("Field Queue");
	if (Field_Queue==null)Field_Queue="0";
	int iField_Queue = Integer.parseInt(Field_Queue);
        if (iField_Queue <= ibenchmark) {
                style3 = style_green;
        }
        else {
                style3 = style_red;
        }


%>

<tr>
 <td class="TBCNT001">Field Queue</td>
  <td class="TBCNT001R"<%=style3%>><%=Field_Queue%>s</td>
  <td class="TBCNT001RBENCHMARK"><%=ibenchmark%>s</td>
</tr>

<%
        ibenchmark = 30;
        String Client_Queue = network_help_desk.get("Client Queue");
	if (Client_Queue==null)Client_Queue="0";
	int iClient_Queue = Integer.parseInt(Client_Queue);
        if (iClient_Queue <= ibenchmark) {
                style3 = style_green;
                style3_restore = "style =\"background-color: #FAF8CC\"";
        }
        else {
                style3 = style_red;
        }


%>


<tr>
 <td class="TBCNT001">Client Queue</td>
  <td class="TBCNT001R"<%=style3%>><%=Client_Queue%>s</td>
  <td class="TBCNT001RBENCHMARK"><%=ibenchmark%>s</td>
</tr>
<%
        ibenchmark = 0;
        String o_h_n = network_help_desk.get("On Hold Now");
	if (o_h_n==null)o_h_n="0";
	int io_h_n = Integer.parseInt(o_h_n);
        if (io_h_n <= ibenchmark) {
                style3 = style_green;
                style3_restore = "style =\"background-color: #FAF8CC\"";
        }
        else {
                style3 = style_red;
        }


%>
<tr>
 <td class="TBCNT001">On Hold Now</td>
  <td class="TBCNT001R"<%=style3%>><%=o_h_n%></td>
  <td class="TBCNT001RBENCHMARK"><%=ibenchmark%></td>
</tr>

<tr>
  <td class="subSectionHeader" colspan="1">Network<img style="float:right;" src="vgp/help.png" id="networkInfo"></td>
</tr>
<tr>
  <td class="TBHDR001" width="100">&nbsp;</td>
  <td class="TBHDR001">Actual</td>
  <td class="TBHDR001">Goal</td>
</tr>
<%
        ibenchmark = 99;
        String w_u_t = network_help_desk.get("WAN Uptime");
	if (w_u_t==null)w_u_t="0";
	int iw_u_t = Integer.parseInt(w_u_t);
        if (iw_u_t >= ibenchmark) {
                style3 = style_green;
                style3_restore = "style =\"background-color: #FAF8CC\"";
        }
        else {
                style3 = style_red;
        }


%>
<tr>
 <td class="TBCNT001">WAN Uptime</td>
  <td class="TBCNT001R"<%=style3%>><%=w_u_t%>%</td>
  <td class="TBCNT001RBENCHMARK"><%=ibenchmark%>%</td>
</tr>

<%
        ibenchmark = 100;
        String d_n = network_help_desk.get("Down Now");
	if (d_n==null)d_n="0";
        int id_n = Integer.parseInt(d_n);
        if (id_n <= ibenchmark) {
                style3 = style_green;
                style3_restore = "style =\"background-color: #FAF8CC\"";
        }
        else {
                style3 = style_red;
        }


%>

<tr>
 <td class="TBCNT001">Down Now</td>
  <td class="TBCNT001R"<%=style3%>><%=d_n%></td>
  <td class="TBCNT001RBENCHMARK"><%=ibenchmark%></td>
</tr>
<%
        ibenchmark = 0;
        String d_d_n = network_help_desk.get("Down Down Now");
	if (d_d_n==null)d_d_n="0";
	int id_d_n = Integer.parseInt(d_d_n);
        if (id_d_n <= ibenchmark) {
                style3 = style_green;
                style3_restore = "style =\"background-color: #FAF8CC\"";
        }
        else {
                style3 = style_red;
        }


%>

<tr>
 <td class="TBCNT001">Down Down Now</td>
  <td class="TBCNT001R"<%=style3%>><%=d_d_n%></td>
  <td class="TBCNT001RBENCHMARK"><%=ibenchmark%></td>
</tr>

<tr>
  <td class="subSectionHeader" colspan="1">Installs<img style="float:right;" src="vgp/help.png" id="installsInfo"></td>
</tr>
<tr>
  <td class="TBHDR001" width="100">&nbsp;</td>
  <td class="TBHDR001">Actual</td>
  <td class="TBHDR001">Goal</td>
</tr>
<%
String Scheduled_ = network_help_desk.get("Scheduled");
if(Scheduled_==null)Scheduled_="0";

String Completed_ = network_help_desk.get("Completed");
if(Completed_==null)Completed_="0";

String FailedRescheduled_ = network_help_desk.get("Failed/Rescheduled");
if(FailedRescheduled_==null)FailedRescheduled_="0";

String CompletionRate_ = network_help_desk.get("Completion Rate");
if(CompletionRate_==null)CompletionRate_="0";

%>
<tr>
 <td class="TBCNT001">Scheduled</td>
  <td class="TBCNT001R"<%=style1%>><%=Scheduled_%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
</tr>

<tr>
 <td class="TBCNT001">Completed</td>
  <td class="TBCNT001R"<%=style1%>><%=Completed_%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
</tr>

<tr>
 <td class="TBCNT001">Failed/Rescheduled</td>
  <td class="TBCNT001R"<%=style1%>><%=FailedRescheduled_%></td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
</tr>

<tr>
 <td class="TBCNT001">Completion(%)</td>
  <td class="TBCNT001R"<%=style1%>><%=CompletionRate_%>%</td>
  <td class="TBCNT001RBENCHMARK"><%=benchmark%></td>
</tr>


</table>
</div>
</td></tr>


</table></td>

<td width="1" bgcolor="#858585"></td>

<td><table>
<!-- Column 4 -->
<tr><td class="sectionHeader">Top&nbsp;15&nbsp;Most&nbsp;Active</td></tr>
<tr><td id="c41">
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c31">
<tr><td colspan="2" class="TBCNT001">Revenue (000)</td></tr>
<%=request.getAttribute("revenue10")%>
</table></div>
</td></tr>
<tr><td id="c42">
<div class="TBDIV001"><table border="0" cellpadding="0" cellspacing="0" id="c32">
<tr><td colspan="2" class="TBCNT001">Jobs</td></tr>
<%=request.getAttribute("count10")%>
</table></div>
</td></tr>
</table></td>
</tr></table></td></tr></table>
<form method="post" id="form1">
<input type="hidden" name="ac" value="db_summary"/>
<input type="hidden" id="bdm1" name="bdm" value="<%=request.getAttribute("bdm")%>"/>
<input type="hidden" id="spm1" name="spm" value="<%=request.getAttribute("spm")%>"/>
<input type="hidden" id="period1" name="period" value="<%=request.getAttribute("period")%>"/>
</form>
<form method="post" id="form2">
<input type="hidden" id="spm1" name="spm" value="<%=request.getAttribute("spm")%>"/>
<input type="hidden" id="period1" name="period" value="<%=request.getAttribute("period")%>"/>
<input type="hidden" name="tick" value="<"/>
</form>

<div class="PUWIN001" id="phonepopup" style="background-color : #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Phone Definitions</td>
 <td class="PUTTL001" align="right"><img src="vgp/delete.gif" class="close-chart" win="phonepopup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001" id="phonepopupdata"></div>
 </td>
</tr>
</table>
</div>


<div class="PUWIN001" id="networkpopup" style="background-color : #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Network Definitions</td>
 <td class="PUTTL001" align="right"><img src="vgp/delete.gif" class="close-chart" win="networkpopup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001" id="networkpopupdata"></div>
 </td>
</tr>
</table>
</div>



<div class="PUWIN001" id="installspopup" style="background-color : #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Install Definitions</td>
 <td class="PUTTL001" align="right"><img src="vgp/delete.gif" class="close-chart" win="installspopup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001" id="installspopupdata"></div>
 </td>
</tr>
</table>
</div>



<div class="PUWIN001" id="chart001popup" style="background-color : #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Profitability</td>
 <td class="PUTTL001" align="right"><img src="vgp/delete.gif" class="close-chart" win="chart001popup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001" id="chart001popupdata"></div>
 </td>
</tr>
</table>
</div>


<div class="PUWIN001" id="chart002popup" style="background-color : #ffffff;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Revenue</td>
 <td class="PUTTL001" align="right"><img src="vgp/delete.gif" class="close-chart" win="chart002popup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001"  id="chart002popupdata"></div>
 </td>
</tr>
</table>
</div>

<div class="PUWIN001" id="chart003popup" style="background-color : #ffffff; width:380px;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
 <td class="PUTTL001" id="chart-title">Minuteman, Speedpay, and PVS Usage</td>
 <td class="PUTTL001" align="center"><img src="vgp/delete.gif" class="close-chart" win="chart003popup"></td></tr>
<tr>
 <td colspan="2" id="chart_target">
 <div class="PUCNT001" id="chart003popupdata"></div>
 </td>
</tr>
</table>
</div>

<!-- keyname 
<div>
<iframe src="http://www.contingent.net/admin/forecast_crm_reports.php?data=master_qmt_report&rotation=disabled&slide=5&&width=1300&height=800&key_name=" width="1400" height="900" frameborder="0" />
</div>
-->
</body>

</html>
