<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix="logic" %>


<%
int listSize = 0;
int count = 0;

if(request.getAttribute("siteDetailListSize")!=null) {
	listSize = Integer.parseInt(request.getAttribute("siteDetailListSize").toString());
//if(request.getAttribute("count")!=null)
	// count = Integer.parseInt(request.getAttribute("count").toString());
}
%>


<html:html>
<head>

<link rel = "stylesheet" href = "styles/style.css" type = "text/css">
<link rel = "stylesheet" href="styles/summary.css" type="text/css"> 
<!--
<link rel = "stylesheet" href = "styles/content.css" type = "text/css">
 <script language = "JavaScript" src = "javascript/JLibrary.js"></script>
 <script language="JavaScript" src="javascript/popcalendar.js"></script> 
 <script language = "JavaScript" src = "javascript/ilexGUI.js"></script> -->


<script language="javascript">

function moveToCurrentPage()
{
	document.forms[0].next.value= "";
	document.forms[0].last.value= "";
	document.forms[0].first.value= "";
	document.forms[0].previous.value= "";
	
	
	
	if(document.forms[0].current_page_no.value!="")
		{
			
			var re2digit=/^\d+$/ 
			if(document.forms[0].current_page_no.value.search(re2digit)==-1) 
			{
			alert("Only positive numbers are allowed.");
			document.forms[0].current_page_no.value = "";
			document.forms[0].current_page_no.focus();
			return false;
			}
			
			document.forms[0].current_page_no.value = eval(document.forms[0].current_page_no.value);
			
			
			if(eval(document.forms[0].current_page_no.value) == 0 )
			{
				if(eval(document.forms[0].current_page_no.value) == eval(document.forms[0].total_page_size.value))
				return false;
			}
			
			if(eval(document.forms[0].current_page_no.value) > eval(document.forms[0].total_page_size.value))
			{
			alert("Entered page cannot be greater than total page size.");
			document.forms[0].current_page_no.value = "";
			document.forms[0].current_page_no.focus();
			return false; 
			}
			if(eval(document.forms[0].current_page_no.value=="1"))
			{
	
				return false;
			}
		}
		
	document.forms[0].action="wipDtlAction.do?countRow="+document.forms[0].countRow.value+"&status=<bean:write name="SiteManagementForm" property="jobStatus"/>";
	document.forms[0].submit();
	return true;	
}

function first_record()
{
	
	document.forms[0].first.value= "Yes";
	document.forms[0].last.value= "";	
	document.forms[0].next.value= "";
	document.forms[0].previous.value = "" ;
	if(eval(document.forms[0].current_page_no.value=="1"))
	{
		return false;
	}
	if(eval(document.forms[0].current_page_no.value=="0"))
	{
		return false;
	}
	document.forms[0].action="wipDtlAction.do?countRow="+document.forms[0].countRow.value+"&status=<bean:write name="SiteManagementForm" property="jobStatus"/>";
	document.forms[0].submit();
	return true;	
}
function last_record()
{
	
	document.forms[0].last.value= "Yes";
	document.forms[0].first.value= "";
	document.forms[0].next.value= "";
	document.forms[0].previous.value= "";
	
	if(eval(document.forms[0].current_page_no.value)== eval(document.forms[0].total_page_size.value))
	{
	
		return false;
	}
	document.forms[0].action="wipDtlAction.do?countRow="+document.forms[0].countRow.value+"&status=<bean:write name="SiteManagementForm" property="jobStatus"/>";
	document.forms[0].submit();
	return true;	
}
function next_record()
{
	
	
	document.forms[0].next.value= "Yes";
	document.forms[0].last.value= "";
	document.forms[0].first.value= "";
	document.forms[0].previous.value= "";
	
	
	if(eval(document.forms[0].current_page_no.value)== eval(document.forms[0].total_page_size.value))
	{

		return false;
	}
	
	
	if (eval(document.forms[0].current_page_no.value) >= eval(document.forms[0].total_page_size.value))
		return false; 
		
	document.forms[0].action="wipDtlAction.do?countRow="+document.forms[0].countRow.value+"&status=<bean:write name="SiteManagementForm" property="jobStatus"/>";
	document.forms[0].submit();
	return true;	
}
function previous_record()
{
	
	document.forms[0].previous.value= "Yes";
	document.forms[0].last.value= "";
	document.forms[0].next.value= "";
	document.forms[0].first.value= "";
	
	
	if(eval(document.forms[0].current_page_no.value=="1"))
	{
	
		return false;
	}
	
	if (document.forms[0].current_page_no.value < 2)
		return false; 
		
	document.forms[0].action="wipDtlAction.do?countRow="+document.forms[0].countRow.value+"&status=<bean:write name="SiteManagementForm" property="jobStatus"/>";
	document.forms[0].submit();
	return true;	
}

function sortwindow()
{
	
	//str = 'SortAction.do?appendixid=<bean:write name="SiteManagementForm" property="appendixid"/>&countRow=<bean:write name = "SiteManagementForm" property = "countRow"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&org_lines_per_page=<bean:write name = "SiteManagementForm" property = "org_lines_per_page"/>&lines_per_page=<bean:write name = "SiteManagementForm" property = "lines_per_page"/>&siteNameForColumn=<bean:write name = "SiteManagementForm" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "SiteManagementForm" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "SiteManagementForm" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "SiteManagementForm" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "SiteManagementForm" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "SiteManagementForm" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "SiteManagementForm" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "SiteManagementForm" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "SiteManagementForm" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "SiteManagementForm" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "SiteManagementForm" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "SiteManagementForm" property = "siteSearchGroup"/>&siteSearchEndCustomer=<bean:write name = "SiteManagementForm" property = "siteSearchEndCustomer"/>&pageType=<bean:write name = "SiteManagementForm" property = "pageType"/>&site_msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>&siteSearchStatus=<bean:write name = "SiteManagementForm" property = "siteSearchStatus"/>&Type=siteManage';
	str = 'SortAction.do?appendixid=<bean:write name="SiteManagementForm" property="appendixid"/>&countRow=<bean:write name = "SiteManagementForm" property = "countRow"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&org_lines_per_page=<bean:write name = "SiteManagementForm" property = "org_lines_per_page"/>&lines_per_page=<bean:write name = "SiteManagementForm" property = "lines_per_page"/>&siteNameForColumn=<bean:write name = "SiteManagementForm" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "SiteManagementForm" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "SiteManagementForm" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "SiteManagementForm" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "SiteManagementForm" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "SiteManagementForm" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "SiteManagementForm" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "SiteManagementForm" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "SiteManagementForm" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "SiteManagementForm" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "SiteManagementForm" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "SiteManagementForm" property = "siteSearchGroup"/>&siteSearchEndCustomer=<bean:write name = "SiteManagementForm" property = "siteSearchEndCustomer"/>&pageType=<bean:write name = "SiteManagementForm" property = "pageType"/>&site_msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>&siteSearchStatus=<bean:write name = "SiteManagementForm" property = "siteSearchStatus"/>&Type=siteManage';
	p = showModelessDialog( str , window, 'status:false;dialogWidth:400px; dialogHeight:350px' ); 
	return true;
}
function del(siteId) 
{	
		var ans = confirm( "Are you sure you want to delete the site?" );	
		if( ans )
		{
			document.forms[0].action = "SiteManage.do?countRow="+document.forms[0].countRow.value+"&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&siteID="+siteId+"&method=Delete";
			document.forms[0].submit();
			return true;	
		}
}

function AddSite()
{
document.forms[0].action = "SiteManage.do?countRow="+document.forms[0].countRow.value+"&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>&siteSearchName=<bean:write name = "SiteManagementForm" property = "siteSearchName"/>&siteSearchNumber=<bean:write name = "SiteManagementForm" property = "siteSearchNumber"/>&msaid=<bean:write name = "SiteManagementForm" property = "msaid"/>&method=Add";
document.forms[0].submit();
return true;
}

function toggleMenuSection(unique) {
	
	var divid="div_" + unique;
	thisDiv = document.getElementById(divid);
	try{
	if (thisDiv==null) { return ; }
	}catch(e){
	}
	try{
	if (thisDiv==null) { return ; }
	}catch(e){ }
	
	
	action = "toggleType = toggleDiv('div_" + unique + "');";
	eval(action);
	
	action = "thisImage = document.getElementById('img_" + unique + "');";
	eval(action);
	if (document.getElementById('div_' + unique).offsetHeight > 0) 	thisImage.src = "images/Collapse.gif";    
	else thisImage.src = "images/Expand.gif";
}
function toggleDiv(divName) {
	 var tcookie = document.cookie;
     var st = tcookie.indexOf ("A1Db");
     var cookievalue ="";
	  if(st!= -1) {
	      st = st+3;
	      st = tcookie.indexOf ("=", st);
	      en = tcookie.indexOf (";", st);
		  if (en > st) cookievalue = tcookie.substring(st+1, en); //div's that is reopen
	 }
	var id = document.getElementById(divName);
	if( id.style.display == "none" ) {
			id.style.display = "block";
			//add it to the existing cookie opendiv paraneter
			if (cookievalue.length>0)	cookievalue = cookievalue+','+divName;
			else	cookievalue = divName;
			if(cookievalue!="") { // Save as cookie
				document.cookie = "ADb="+ cookievalue+" ; ";
          } }
	else {
			id.style.display = "none";	
			var newcookievalue='';
			//remove this div from the opendiv cookie
			if (cookievalue.length>0) {
				var array = cookievalue.split(',');
				if (array.length>1) {
					for(i=0;i<array.length;i++) {
						if(array[i]==divName) { }
						else {
							if(newcookievalue.length > 0 )
								newcookievalue = newcookievalue+','+array[i];
							else
								newcookievalue = array[i];	
						}
					}
				}
				if(newcookievalue!="") { // Save as cookie
			 		document.cookie = "ADb="+ newcookievalue+" ; ";
			 	}
			}
		}
	if( divName == 'div_1')
	{
		id = document.getElementById('div_13');
		if( id.style.display == "none" ) id.style.display = "block";
		else id.style.display = "none";	
		id = document.getElementById('div_11');
		if( id.style.display == "none" ) id.style.display = "block";
		else id.style.display = "none";	
	}
}

</script>


</head>
<BODY onLoad="MM_preloadImages('images/Expand.gif','images/Collapse.gif');leftAdjLayers();" >


<html:form action="wipDtlAction"> 
<%-- 


<html:hidden property ="ownerId"/>
<html:hidden property ="org_page_no"/>
<html:hidden property = "row_size"/>
<html:hidden property = "pageType"/>
<html:hidden property = "specialEndCustomer"/>
<html:hidden property="jobOwnerOtherCheck"/>
 --%>
<html:hidden property="appendixid"/>
<html:hidden property="appendixname"/>
<html:hidden property="msaname"/>
<html:hidden property ="org_lines_per_page"/>
<html:hidden property="countRow"/>
<html:hidden property ="first" />
<html:hidden property ="last" />
<html:hidden property ="previous" />
<html:hidden property ="next" />
 



<table  id="myTable" name="myTable" border = "0" width = "1200" cellspacing = "0" cellpadding = "1">

		<tr>
		    <td width="22" ></td> 
		    <td colspan ="10" class="dblabel" align="left"> 
		     	<IMG id = img_page  src = "images/old-first.gif" border = 0 onclick="first_record();">
			<IMG id = img_page src = "images/old-prev.gif" border = 0 onclick="previous_record();"> 
			<bean:message bundle = "pm" key = "site.page"/>
			<html:text property = "current_page_no" styleClass = "textbox" size="2" onblur="moveToCurrentPage();"/> 
			<bean:message bundle = "pm" key = "site.of"/>
		 	<html:text property = "total_page_size" styleClass = "textboxnumberreadonlymiddle" size="2" readonly = "true"/>   
			<IMG id = img_page src = "images/old-next.gif" border = 0 onclick="next_record();">
			<IMG id = img_page src = "images/old-last.gif" border = 0 onclick="last_record();">
			</td>
		</tr>
		<tr>
			<td width="22" ></td>
			<td colspan="10">
				<span style="width: 30px;">
					<a href='wipDtlAction.do?status=<%= request.getAttribute("status")%>&search=go&PocReferenceType=<%= request.getAttribute("PocReferenceType")%>&PocReference=<%= request.getAttribute("PocReference")%>&daysCount=0'>All</a>
				</span>
				<span style="width: 55px;">
					<a href='wipDtlAction.do?status=<%= request.getAttribute("status")%>&search=go&PocReferenceType=<%= request.getAttribute("PocReferenceType")%>&PocReference=<%= request.getAttribute("PocReference")%>&daysCount=7'>7 Days</a>
				</span>
				<span style="width: 60px;">
					<a href='wipDtlAction.do?status=<%= request.getAttribute("status")%>&search=go&PocReferenceType=<%= request.getAttribute("PocReferenceType")%>&PocReference=<%= request.getAttribute("PocReference")%>&daysCount=30'>30 Days</a>
				</span>
				<span style="width: 60px;">
					<a href='wipDtlAction.do?status=<%= request.getAttribute("status")%>&search=go&PocReferenceType=<%= request.getAttribute("PocReferenceType")%>&PocReference=<%= request.getAttribute("PocReference")%>&daysCount=60'>60 Days</a>
				</span>
				<span style="width: 60px;">
					<a href='wipDtlAction.do?status=<%= request.getAttribute("status")%>&search=go&PocReferenceType=<%= request.getAttribute("PocReferenceType")%>&PocReference=<%= request.getAttribute("PocReference")%>&daysCount=90'>90 Days</a>
				</span>		
			</td>
		</tr>
		

		<tr>
				<td width="2"></td>
				<logic:equal name = "SiteManagementForm" property = "pageType" value ="jobsites">
					<td class = "buttonrow" styleClass="button" colspan = "<%=count+4%>">
				</logic:equal>
				
				<logic:notEqual name = "SiteManagementForm" property = "pageType" value ="jobsites">
					<td class = "buttonrow" styleClass="button" colspan = "<%=count+3%>">
				</logic:notEqual>
					<%	if(listSize>0){ %>
<%-- 					<html:button property="Sort"  styleClass="button" onclick = "javascript:return sortwindow();"><bean:message bundle = "pm" key = "site.sort"/></html:button> --%>
					<%}else{ %>
					<%}%>	
						<logic:equal name = "SiteManagementForm" property = "pageType" value ="jobsites">
						</logic:equal>
						<logic:equal name = "SiteManagementForm" property = "pageType" value ="mastersites">
						<html:button property="New"  styleClass="button" onclick="return AddSite();"><bean:message bundle = "pm" key = "site.new"/></html:button>
						</logic:equal>	
						
							
					</td>
				
				</tr>



		<tr> 
            	<td width="2"></td>
            	
            		<TD width="152"  class = "tryb" > 
				    	<div align = "center"><bean:message bundle = "pm" key = "appendix.title"/></div>
				    </td>
            	     <TD width="152"  class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.number"/></div>
				    </td>
				
				      <TD width="141" class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.name"/></div>
				    </td>
				    
				   <TD width="236"  class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.address"/></div>
				    </td>
				    
				  <TD width="94" class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.city"/></div>
				    </td>
				    
				    <TD width="33"  class = "tryb"  > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.state"/></div>
				    </td>
				   
				  <TD width="73"  class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.zipcode"/></div>
				    </td>
				    
				   <TD width="32" class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.country"/></div>
				    </td>
			     
			   <TD width="180" class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.group"/></div>
				 </td>
				<TD width="180"  class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.end.customer"/></div>
				 </td>
				<TD width="180" class = "tryb" > 
				    <div align = "center"><bean:message bundle = "pm" key = "site.status"/></div>
				 </td>
				 
				<td class = "tryb">
					<div align = "center">Job Name</div>
				</td>
				
            </tr>    

<logic:present name="siteList">
	
	<%
					String temp_str = "";
					String temp_str1 = "";
					String temp_str2 = "";
					int i = 1;
              		int j = 1;

					String bgcolor = "";
					String bgcolorno = ""; 
					String bgcolortext1 = ""; 
					String tempCategory = "";
					String bgcolordata = "";
					String divbgcolortext1="";
	%>
	
	<logic:iterate id="list" name="siteList" >
	<%
					temp_str = "";
					if( ( i%2 )== 0 ) {
						bgcolor = "dbvalueeven";
						bgcolorno="numbereven";
						bgcolortext1="textlefteven";
						divbgcolortext1="textlefteventop";
						bgcolordata ="textlefteven1";
					} 
					else
					{
						bgcolor = "dbvalueodd";
						bgcolorno = "numberodd";
						bgcolortext1 = "textleftodd";
						divbgcolortext1="textleftoddtop";
						bgcolordata ="textleftodd1";
					}
					
	%>
	
			<bean:define id = "siteID" name = "list" property = "siteID" type = "java.lang.String"/>
					<bean:define id = "siteJobId" name = "list" property = "siteJobId" />
			<tr>
				<TD width="2">
				<A href = "javascript:toggleMenuSection( 'j<%= i %>' );">
						<IMG id = img_j<%= i %> alt=+ src = "images/Expand.gif" border = 0></A> 
				</TD>
					
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
					<bean:write name="list" property="appendixname"/></td>			
					
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
				<A id='one<%= siteID%>' href ="/Ilex/SiteManage.do?method=Modify&countRow=<bean:write name= "SiteManagementForm" property= "countRow"/>&siteID=<bean:write name = "list" property = "siteID"/>&appendixid=<bean:write name = "list" property = "appendixid"/>&siteSearchName=<bean:write name = "list" property = "siteSearchName"/>&siteSearchNumber=<bean:write name = "list" property = "siteSearchNumber"/>&lines_per_page=<bean:write name = "SiteManagementForm" property = "lines_per_page"/>&current_page_no=<bean:write name = "SiteManagementForm" property = "current_page_no"/>&org_page_no=<bean:write name = "SiteManagementForm" property = "org_page_no"/>&org_lines_per_page=<bean:write name = "SiteManagementForm" property = "org_lines_per_page"/>&total_page_size=<bean:write name = "SiteManagementForm" property = "total_page_size"/>&ownerId=<bean:write name = "list" property = "ownerId"/>&siteNameForColumn=<bean:write name = "list" property = "siteNameForColumn"/>&siteNumberForColumn=<bean:write name = "list" property = "siteNumberForColumn"/>&siteAddressForColumn=<bean:write name = "list" property = "siteAddressForColumn"/>&siteCityForColumn=<bean:write name = "list" property = "siteCityForColumn"/>&siteStateForColumn=<bean:write name = "list" property = "siteStateForColumn"/>&siteZipForColumn=<bean:write name = "list" property = "siteZipForColumn"/>&siteCountryForColumn=<bean:write name = "list" property = "siteCountryForColumn"/>&siteSearchCity=<bean:write name = "list" property = "siteSearchCity"/>&siteSearchState=<bean:write name = "list" property = "siteSearchState"/>&siteSearchZip=<bean:write name = "list" property = "siteSearchZip"/>&siteSearchCountry=<bean:write name = "list" property = "siteSearchCountry"/>&siteSearchGroup=<bean:write name = "list" property = "siteSearchGroup"/>&siteSearchEndCustomer=<bean:write name = "list" property = "specialEndCustomer"/>&siteSearchStatus=<bean:write name = "list" property = "siteSearchStatus"/>&pageType=jobsite&msaid=<bean:write name = "list" property = "msaid"/>">
					<bean:write name="list" property="siteNumber"/>
					</A>
					</td>				
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
			
				<bean:write name="list" property="siteName"/></TD>
					
				
					
				<TD  class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
								<bean:write name="list" property="siteAddress"/></td>
				
				
					
				<TD class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="siteCity"/></td>
				
				
					
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="state"/></td>
				
				
						
				<TD   class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="siteZipCode"/></td>
					
				
						
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="country"/></td>	
				
				
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="site_group"/></td>	
				
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="site_end_customer"/></td>	
									
				<TD    class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
									<bean:write name="list" property="siteStatus"/></td>
									
				 
					<TD class = "<%= bgcolortext1 %>" align = "left"  valign = middle>
							<a href="/Ilex/JobDashboardAction.do?&function=view&jobid=<%=siteJobId%>&appendixid=<bean:write name = "list" property = "appendixid"/>"><%=siteJobId%></a> 
					</TD>	
										
				
				
									
			<!--  	<TD class="<%= bgcolortext1 %>" align = "right">
				<A id='one<%= siteID%>' href = "SiteManage.do?method=Modify&countRow=<bean:write name= "SiteManagementForm" property= "countRow"/>&siteID=<bean:write name = "list" property = "siteID"/>&appendixid=<bean:write name = "SiteManagementForm" property = "appendixid"/>">Modify</A>&nbsp;|&nbsp;<A id='two<%= siteID%>' href = "javascript:del(<%=siteID%>)">Delete</A>&nbsp;|&nbsp;  --> 
			</tr>
	                <TR id = div_j<%= i %> style = "DISPLAY: none">
	                <TD>&nbsp;</TD>
	                <TD colSpan=11 width="1200">
	                  <TABLE cellSpacing = 0 cellPadding = 0 width="1200" border =0>
	                    <TBODY>
		                        <TR height =20>
								    <TD  class="<%= divbgcolortext1 %>  colSpan=10 height="60" width ="1200">
								      <table cellpadding=0 cellspacing=0 border="0" width="1200">
								   
								       <tr>
									    	<td  class = "dblabelsmall" width="111"><bean:message bundle = "pm" key = "site.worklocation"/>:</td>
									    	<td  class = "dbvaluesmall" colspan="3" valign="top"><bean:write name = "list" property = "siteWorkLocation" /></td>
									    	<td  class = "dblabelsmall" width="117"><bean:message bundle = "pm" key = "site.siteDirections"/>:</td>
									    	<td  class = "dbvaluesmall" colspan="3" valign="top"><bean:write name = "list" property = "siteDirections" /></td>
								    	</tr>
										<tr>
											<td  class = "dblabelsmall" width="111"><bean:message bundle = "pm" key = "site.pointOfContact"/></td>
											<td  class = "dblabelsmall" width="140"><bean:message bundle = "pm" key = "site.pointOfContactName"/></td>
											<td  class = "dblabelsmall" width="144"><bean:message bundle = "pm" key = "site.pointOfContactPhone"/></td>
											<td  class = "dblabelsmall" width="144"><bean:message bundle = "pm" key = "site.pointOfContactEmail"/></td>
											<td  class = "dblabelsmall" width="117"><bean:message bundle = "pm" key = "site.specialConditions"/>:</td>
											<td  class = "dbvaluesmall" colspan="3" rowspan="3" valign="top"><bean:write name = "list" property = "siteNotes" /></td>
								    	</tr>
								    	<tr>
										    <td  class = "dbvaluesmall" width="111"><bean:message bundle = "pm" key = "site.pointOfContactPrimary"/></td>
										    <td  class = "dbvaluesmall" width="140"><bean:write name = "list" property = "sitePrimaryName" /></td>	    		
										    <td  class = "dbvaluesmall" width="144"><bean:write name = "list" property = "sitePrimaryPhone" /></td>	    		
										    <td  class = "dbvaluesmall" width="144"><bean:write name = "list" property = "sitePrimaryEmail" /></td>	    		
				    					</tr>
							  			<tr>	
						  					<td  class = "dbvaluesmall" width="111"><bean:message bundle = "pm" key = "site.pointOfContactSecondary"/></td>
											<td  class = "dbvaluesmall" width="140"><bean:write name = "list" property = "siteSecondaryName" /></td>	    		
											<td  class = "dbvaluesmall" width="140"><bean:write name = "list" property = "siteSecondayPhone" /></td>	    		
											<td  class = "dbvaluesmall" width="144"><bean:write name = "list" property = "siteSecondaryEmail"/></td>	    		
					  					</tr>
					  					<tr>
					  						<td  class = "dbvaluesmall" width="111"><bean:message bundle = "pm" key = "site.createdby"/></td>
					  						 <TD class=dbvaluesmall width=140><bean:write name = "list" property = "siteCreatedBy"/></TD>
					  						 <TD class=dbvaluesmall width=144><bean:message bundle = "pm" key = "site.createddate"/></TD>
					  						 <TD class=dbvaluesmall width=144><bean:write name = "list" property = "siteCreatedDate"/></TD>
					  						 <TD class=dbvaluesmall width=117><bean:message bundle = "pm" key = "site.changedby"/></TD>
					  						 <TD class=dbvaluesmall width=164><bean:write name = "list" property = "siteChangedBy"/></TD>
					  						 <TD class=dbvaluesmall width=113><bean:message bundle = "pm" key = "site.changeddate"/></TD>
					  						 <TD class=dbvaluesmall width=267><bean:write name = "list" property = "siteChangedDate"/></TD>  
					  					</tr>
								     </table>
								    </TD>
								    
								</TR>
							</TBODY></TABLE></TD>
						</TR>
				<% i++; %>
				</logic:iterate>
				</table>
	
</logic:present>
</html:form>
</BODY>
</html:html>