<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Drag Coefficient</title>
<style>
.titleLine {
	font-size: 12pt;
	font-weight: bold;
	text-align: left;
	vertical-align: Middle;
	padding: 0px 0px 6px 0px;
}
</style>

<%
	List<String[]> netMedXLst = (List<String[]>) request
			.getAttribute("netMedXAllCustomerChartData");
	List<String[]> everWorxLst = (List<String[]>) request
			.getAttribute("everWorxAllCustomerChartData");
	List<String[]> deploymentLst = (List<String[]>) request
			.getAttribute("deploymentAllCustomerChartData");

	List<String[]> customerLst = (List<String[]>) request
			.getAttribute("allCustomers");
	List<String[]> appendixLst = (List<String[]>) request
			.getAttribute("appedixListing");

	String appendix = (String) request.getAttribute("selectedAppendix");
	String customer = (String) request.getAttribute("selectedCustomer");
	String lastCustomer = (String) request
			.getAttribute("lastSelectedCustomer");
	String lastAppendix = (String) request
			.getAttribute("lastSelectedAppendix");
	String lastCustomerName = (String) request
			.getAttribute("lastSelectedCustomerName");
	String currentCustomerName = (String) request
			.getAttribute("currentSelectedCustomerName");

	List<String[]> customerNetMedXChartData = new ArrayList<String[]>();
	List<String[]> customerEverWorxChartData = new ArrayList<String[]>();
	List<String[]> customerDeploymentChartData = new ArrayList<String[]>();
	if (request.getAttribute("netMedXCurrentCustomerChartData") != null) {
		customerNetMedXChartData = (List<String[]>) request
				.getAttribute("netMedXCurrentCustomerChartData");
		customerEverWorxChartData = (List<String[]>) request
				.getAttribute("everWorxCurrentCustomerChartData");
		customerDeploymentChartData = (List<String[]>) request
				.getAttribute("deploymentCurrentCustomerChartData");
	}

	List<String[]> oldCustomerNetMedXChartData = new ArrayList<String[]>();
	List<String[]> oldCustomerEverWorxChartData = new ArrayList<String[]>();
	List<String[]> oldCustomerDeploymentChartData = new ArrayList<String[]>();
	if (request.getAttribute("netMedXOldCustomerChartData") != null) {
		oldCustomerNetMedXChartData = (List<String[]>) request
				.getAttribute("netMedXOldCustomerChartData");
		oldCustomerEverWorxChartData = (List<String[]>) request
				.getAttribute("everWorxOldCustomerChartData");
		oldCustomerDeploymentChartData = (List<String[]>) request
				.getAttribute("deploymentOldCustomerChartData");
	}
%>

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<script type="text/javascript">

$(function () {
    $('#netMedXContainer').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'NetMedX'
        },
        subtitle: {
            text: 'Trailing 12 month'
        },
        xAxis: [{
            categories: [<%for (int i = 0; i < netMedXLst.size(); i++) {
				String[] data = netMedXLst.get(i);
				if (i == (netMedXLst.size() - 1)) {
					out.print("'" + data[1] + "-" + data[2] + "'");
				} else {
					out.print("'" + data[1] + "-" + data[2] + "', ");
				}
			}%>],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            title: {
                text: 'NetMedX',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            opposite: false

        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 80,
            verticalAlign: 'top',
            y: 55,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [ {
            name: 'All',
            type: 'column',
            data: [ <%for (int i = 0; i < netMedXLst.size(); i++) {
				String[] data = netMedXLst.get(i);
				if (i == (netMedXLst.size() - 1)) {
					out.print(data[0]);
				} else {
					out.print(data[0] + ", ");
				}

			}%>],
            tooltip: {
                valueSuffix: ''
            }
        }
        
        <%if (customerNetMedXChartData != null
					&& customerNetMedXChartData.size() > 0) {%>
        	, {
                name: '<%=currentCustomerName%>',
                type: 'spline',
                data: [ <%for (int i = 0; i < customerNetMedXChartData.size(); i++) {
					String[] data = customerNetMedXChartData.get(i);
					if (i == (customerNetMedXChartData.size() - 1)) {
						out.print(data[0]);
					} else {
						out.print(data[0] + ", ");
					}

				}%>],
                tooltip: {
                    valueSuffix: ''
                }
            }
        	<%}%>
        	
        	<%if (oldCustomerNetMedXChartData != null
					&& oldCustomerNetMedXChartData.size() > 0) {%>
            	, {
                    name: '<%=lastCustomerName%>',
                    type: 'spline',
                    data: [ <%for (int i = 0; i < oldCustomerNetMedXChartData.size(); i++) {
					String[] data = oldCustomerNetMedXChartData.get(i);
					if (i == (oldCustomerNetMedXChartData.size() - 1)) {
						out.print(data[0]);
					} else {
						out.print(data[0] + ", ");
					}

				}%>],
        	        dashStyle: 'shortdot',
                    tooltip: {
                        valueSuffix: ''
                    }
                }
            	<%}%>
        ]
    });
});

$(function () {
    $('#deploymentContainer').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Deployment'
        },
        subtitle: {
            text: 'Trailing 12 month'
        },
        xAxis: [{
            categories: [<%for (int i = 0; i < deploymentLst.size(); i++) {
				String[] data = deploymentLst.get(i);
				if (i == (deploymentLst.size() - 1)) {
					out.print("'" + data[1] + "-" + data[2] + "'");
				} else {
					out.print("'" + data[1] + "-" + data[2] + "', ");
				}
			}%>],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            title: {
                text: 'Deployment',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            opposite: false

        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 80,
            verticalAlign: 'top',
            y: 55,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [ {
            name: 'All',
            type: 'column',
            data: [ <%for (int i = 0; i < deploymentLst.size(); i++) {
				String[] data = deploymentLst.get(i);
				if (i == (deploymentLst.size() - 1)) {
					out.print(data[0]);
				} else {
					out.print(data[0] + ", ");
				}

			}%>],
            tooltip: {
                valueSuffix: ''
            }
        }
        <%if (customerDeploymentChartData != null
					&& customerDeploymentChartData.size() > 0) {%>
        	, {
                name: '<%=currentCustomerName%>',
                type: 'spline',
                data: [ <%for (int i = 0; i < customerDeploymentChartData.size(); i++) {
					String[] data = customerDeploymentChartData.get(i);
					if (i == (customerDeploymentChartData.size() - 1)) {
						out.print(data[0]);
					} else {
						out.print(data[0] + ", ");
					}

				}%>],
                tooltip: {
                    valueSuffix: ''
                }
            }
        	<%}%>
        	
        	<%if (oldCustomerDeploymentChartData != null
					&& oldCustomerDeploymentChartData.size() > 0) {%>
            	, {
                    name: '<%=lastCustomerName%>',
                    type: 'spline',
                    data: [ <%for (int i = 0; i < oldCustomerDeploymentChartData.size(); i++) {
					String[] data = oldCustomerDeploymentChartData.get(i);
					if (i == (oldCustomerDeploymentChartData.size() - 1)) {
						out.print(data[0]);
					} else {
						out.print(data[0] + ", ");
					}

				}%>],
        	        dashStyle: 'shortdot',
                    tooltip: {
                        valueSuffix: ''
                    }
                }
            	<%}%>
        ]
    });
});

$(function () {
    $('#everWroxContainer').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'EverWrox'
        },
        subtitle: {
            text: 'Trailing 12 month'
        },
        xAxis: [{
            categories: [<%for (int i = 0; i < everWorxLst.size(); i++) {
				String[] data = everWorxLst.get(i);
				if (i == (everWorxLst.size() - 1)) {
					out.print("'" + data[1] + "-" + data[2] + "'");
				} else {
					out.print("'" + data[1] + "-" + data[2] + "', ");
				}
			}%>],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            title: {
                text: 'EverWrox',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            opposite: false

        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 80,
            verticalAlign: 'top',
            y: 55,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [ {
            name: 'All',
            type: 'column',
            data: [ <%for (int i = 0; i < everWorxLst.size(); i++) {
				String[] data = everWorxLst.get(i);
				if (i == (everWorxLst.size() - 1)) {
					out.print(data[0]);
				} else {
					out.print(data[0] + ", ");
				}

			}%>],
            tooltip: {
                valueSuffix: ''
            }
        }
        
        <%if (customerEverWorxChartData != null
					&& customerEverWorxChartData.size() > 0) {%>
        	, {
                name: '<%=currentCustomerName%>',
                type: 'spline',
                data: [ <%for (int i = 0; i < customerEverWorxChartData.size(); i++) {
					String[] data = customerEverWorxChartData.get(i);
					if (i == (customerEverWorxChartData.size() - 1)) {
						out.print(data[0]);
					} else {
						out.print(data[0] + ", ");
					}

				}%>],
                tooltip: {
                    valueSuffix: ''
                }
            }
        	<%}%>
        	
        	<%if (oldCustomerEverWorxChartData != null
					&& oldCustomerEverWorxChartData.size() > 0) {%>
            	, {
                    name: '<%=lastCustomerName%>',
                    type: 'spline',
                    data: [ <%for (int i = 0; i < oldCustomerEverWorxChartData.size(); i++) {
					String[] data = oldCustomerEverWorxChartData.get(i);
					if (i == (oldCustomerEverWorxChartData.size() - 1)) {
						out.print(data[0]);
					} else {
						out.print(data[0] + ", ");
					}

				}%>],
        	        dashStyle: 'shortdot',
                    tooltip: {
                        valueSuffix: ''
                    }
                }
            	<%}%>
            	
            	
        	]
    });
});

function updateChartForCustomer(mmIdVal){	
    $.ajax({url: "TL2.do?ac=getAppendixByCustomer&mmId="+mmIdVal, success: function(result){
       	document.getElementById("selectedAppendixId").innerHTML = result;        
    }});	
}

</script>
</head>
<body style="padding: 10px;">
	<script src="jsp/vgp/res/highcharts.js"></script>
	<script src="jsp/vgp/res/modules/exporting.js"></script>

	<div style="min-width: 400px; margin: 0px; padding: 0px;">
		<h1 class="titleLine">Drag Coefficient</h1>
	</div>
	<div id="dropdowns"
		style="min-width: 400px; margin: 0px; padding: 0px;">
		<form action="TL2.do" method="post">
			<input type="hidden" name="ac" value="dragCoefficient" /> <input
				type="hidden" name="lastSelectedCustomer" value="<%=lastCustomer%>" />
			<input type="hidden" name="lastSelectedAppendix"
				value="<%=lastAppendix%>" /> Customer: <select
				name="selectedCustomer"
				onchange="updateChartForCustomer(this.value);" style="width: 350px;">
				<option value="0">--Select--</option>
				<%
					for (int i = 0; i < customerLst.size(); i++) {
						String[] custLst = customerLst.get(i);
				%>
				<option value="<%=custLst[1]%>"
					<%if (customer.equals(custLst[1]))
					out.print("selected=\"selected\"");%>><%=custLst[0]%></option>
				<%
					}
				%>
			</select> <br /> <br /> Appendix: <select name="selectedAppendix"
				id="selectedAppendixId" style="width: 350px;">
				<%
					if (appendixLst != null) {
				%>
				<%
					for (int i = 0; i < appendixLst.size(); i++) {
							String[] appLst = appendixLst.get(i);
				%>
				<option value="<%=appLst[1]%>"
					<%if (appendix.equals(appLst[1]))
						out.print("selected=\"selected\"");%>><%=appLst[0]%></option>
				<%
					}
				%>
				<%
					}
				%>
			</select> <input type="submit" value="Populate Chart" name="PopulateChart"
				title="Populate Chart" />


		</form>
	</div>

	<div id="netMedXContainer"
		style="min-width: 400px; height: 400px; margin: 0 auto"></div>

	<div id="deploymentContainer"
		style="min-width: 400px; height: 400px; margin: 0 auto"></div>


	<div id="everWroxContainer"
		style="min-width: 400px; height: 400px; margin: 0 auto"></div>

</body>
</html>
