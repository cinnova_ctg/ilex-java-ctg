<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ page  import="java.text.NumberFormat,java.util.Locale" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%
NumberFormat Formatter=NumberFormat.getCurrencyInstance(new Locale("en_US"));
%>
<% 
Double currency,amount;
%>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.tablesorter.js"></script>
<link href="<%=request.getContextPath() %>/css/tableSorter.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.blockUI.js"></script>
<bean:define id="accountName" name="RevenueExpenseForm" property="accountName" />
<script>

$(document).ready(function() {
	$("#jobOwnerTable_3 tr:even").addClass("even");
	$("#jobOwnerTable_3 tr:odd").addClass("odd");
	
});

</script>
<script>
$(document).ready(function(){
            $(".noborder").each(function(){
                        $(this).css('border-right','none');
                        $(this).css('border-bottom','none');
                        $(this).css('border-top','none');
                        $(this).css('background-color','#FFFFFF');
            });
});
</script>
<title>Revenue By Job Owner</title>
</head>
<body>
<html:form action="RevenueExpenseAction.do">
	  		<table id="contentTable" border="0" width="100%" cellspacing="0" cellpadding="0"
			style="padding: 0px; height: 328px;">
	  			<tr  style="padding: 0px;">
	  				<td valign="top" class="pageContentPadding" >
	  				<table  width="100%" border="0" cellspacing="0" cellpadding="0">
	  				<tr>
				<td align="left" style="padding-left: 5px;"><span style="font-size: 11px; font-weight:bold; font-family:Arial; color:#2e3d80; padding-top:0px; padding-bottom:0px; padding:0px; white-space: nowrap;">Account Summary for <%=accountName %></span></td>
	  				</tr>
	  					<tr style="padding: 0px;">	
				        	<td valign="top">
				        		<table width="100%" border="0" cellpadding="0" cellspacing="1" id="jobOwnerTable_3" class="tablesorter">
		  						<thead>
			  						<tr>
				  						<th rowspan="2">Month</th>
				  						<td colspan="2">Expense</td>
				  						<th rowspan="2">Revenue</th>
				  						<td colspan="3">VGPM</td>
				  						<th rowspan="2">Count</th>
				  						<th rowspan="2" class="noborder"></th>
			  						</tr>
			  						<tr>
			  							<th>Pro Forma</th>
			  							<th>Actual</th>
			  							<th>Pro Forma</th>
			  							<th>Actual</th>
			  							<th>Delta</th>
			  						</tr>
			  				
		  						</thead>
		  			<tbody>
		  			
		  										<logic:present name="RevenueExpenseForm" property="detailedList">
					        			<logic:iterate id="list" name="RevenueExpenseForm" property="detailedList">
						       
						        			<tr>
						        				<td width="10%" align="left"><bean:write name="list" property="month" /></td>
						        				<bean:define id="pfExpense" name="list"  property="proFormaExpense" type="java.lang.String" />
						        					<% 	
						        					currency = new Double(pfExpense);
						        					pfExpense=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
													 %>
						        				<td  align="right" nowrap="nowrap"><%=pfExpense %></td>    
						        				<bean:define id="expense" name="list"  property="expense" type="java.lang.String" />    
						        				 <%
						        				 currency = new Double(expense);
						        				 expense=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
												 %>   					       
						        				<td  align="right" nowrap="nowrap"><%=expense%></td>     					       
						        				<bean:define id="revenue" name="list"  property="revenue" type="java.lang.String" />    
						        				 <%
						        				 currency = new Double(revenue);
						        				 revenue=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
												 %>
						        				<td align="right" nowrap="nowrap"><%=revenue%></td>     					       
						        				<bean:define id="proFormaVGPM" name="list"  property="proFormaVGPM" type="java.lang.String" />   
						        				<%
						        				proFormaVGPM=proFormaVGPM.substring(0,proFormaVGPM.length()-2);
						        				%>
						        				<td  align="center" nowrap="nowrap"><%=proFormaVGPM%></td>  
						        				<bean:define id="VGPM" name="list"  property="VGPM" type="java.lang.String" />   
						        				<% 
						        				VGPM=VGPM.substring(0,VGPM.length()-2);
						        				%>   					       
						        				<td  align="center" nowrap="nowrap"><%=VGPM%></td> 
						        				<bean:define id="deltaVGPM" name="list"  property="deltaVGPM" type="java.lang.String" />   
						        				<%deltaVGPM=deltaVGPM.substring(0,deltaVGPM.length()-2); %>
						        				<td align="center" nowrap="nowrap"><%=deltaVGPM%></td> 
						        				<bean:define id="occurrences" name="list"  property="occurrences" type="java.lang.String" />    
						        				 <%
						        				 amount = new Double(occurrences);
						        				 occurrences=Formatter.format(amount).substring(1, Formatter.format(amount).indexOf("."));
												 %>      					       
						        				<td  align="right" nowrap="nowrap"><%=occurrences %></td>     					       
						        		<logic:lessThan name="list" property="deltaVGPM" value=".00">
						        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-red.gif" alt=""/></td>
						        			</logic:lessThan>	
						        			<logic:greaterEqual name="list" property="deltaVGPM" value=".05">
						        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-green.gif" alt=""/></td>
						        			</logic:greaterEqual>
						        		<logic:greaterThan name="list"  property="VGPM" value=".55">
						        					<logic:greaterEqual name="list" property="deltaVGPM" value="0.00">
							        					<logic:lessThan name="list" property="deltaVGPM" value="0.05">
							        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-blue.gif" alt=""/></td>
							        					</logic:lessThan>
						        					</logic:greaterEqual>
						        	   </logic:greaterThan>	 
						        			</tr>
						        			
					        			</logic:iterate>
					        		</logic:present>
		  					</tbody>
		  						</table>
				        	</td>
				        	</tr>
	  					</table>
	  				</td>
	  			</tr>
	  		</table>
	  	</html:form>
	  	</body>
	  	</html>