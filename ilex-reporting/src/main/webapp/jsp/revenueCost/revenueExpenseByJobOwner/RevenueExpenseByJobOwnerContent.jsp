<%@page import="java.util.ArrayList"%>
<%@ page  import="java.text.NumberFormat,java.util.Locale" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.tablesorter.js"></script>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<link href="<%=request.getContextPath() %>/css/tableSorter.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/rptstyle.css" rel="stylesheet" type="text/css" />
<script language = "JavaScript" src = "js/popcalendar.js"></script>
<script language = "JavaScript" src = "js/JLibrary.js"></script>
<title>Revenue By Job Owner</title>
<%
NumberFormat Formatter=NumberFormat.getCurrencyInstance(new Locale("en_US"));
Double currency,amount;
%>

<script>

$(document).ready(function() {
	//$("#emptyTD").removeAttr('width');
	$("#lcBreadCrumbText").html("Revenue/Expense by Job Owner");
	$("#jobOwnerTable tr:even").addClass("even");
	$("#jobOwnerTable tr:odd").addClass("odd");
	
});
function validateAll(x){ 
	
	// - Fire on click of Go button
	// - For validate and submit form		
		if( document.forms[0].toDate.value  != "" && document.forms[0].fromDate.value  != "" ){ // - when both To Date and From Date is not blank
		
			if ( !compDate_mdy(document.forms[0].fromDate.value, document.forms[0].toDate.value) ){ // - Compare To Date and From Date
			
				alert("To Date Should be greater than From Date");
				return false;
			} 
		}
submitAll();
	}
function submitAll()
{
	var url = "RevenueExpenseAction.do?ref=revenueExpenseByJobOwner&reportType=RevenueCost";
	document.forms[0].action = url;
	document.forms[0].submit();
	
}
function secondLevelCall(jobOwner,OwnerID,report)

{	

var url = "RevenueExpenseAction.do?ref=jobOwnerSecondLevelReports&jobOwnerName="+jobOwner+"&jobOwnerID="+OwnerID+"&reportName="+report+"&reportType=RevenueCost";
document.forms[0].action = url;
document.forms[0].submit();

}
$(document).ready(function(){

            $(".noborder").each(function(){

                        $(this).css('border-right','none');

                        $(this).css('border-bottom','none');

                        $(this).css('border-top','none');

                        $(this).css('background-color','#FFFFFF');
                        $(this).css('width','30px');

            });

});

</script>
<%ArrayList al=new ArrayList();
			al.add("Project");
			al.add("NetMedX");
			al.add("Help Desk");
			al.add("Provisioning/Deployment");
			%>
</head>
	  		<table id="contentTable" border="0" cellspacing="0" cellpadding="0" style="padding: 0px; height: 328px;">
			<tr>
				<td>
					<table border="0"  width="100%" >
						<tr>
							<td width="99%"></td>
							<%for(int i=0;i<al.size()-1;++i){ %>
							<td >
								<html:multibox property="checked" name="RevenueExpenseForm">
								<%=al.get(i) %>
								</html:multibox>
							</td>
							<td <%if(i!=2){ %>style="padding-right: 10px; white-space: nowrap;""<%}else{ %>style="padding-right: 35px; white-space: nowrap;""<%}%>>
								<h3><%=al.get(i)%></h3>
							</td>
							<%}%>
					  </tr>
					  </table>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>
			<tr>
				<td>
					   <table border="0"  width="100%" >
						<tr>
							<td width="99%"></td>
							<td align="right"  >
								<html:multibox property="checked" name="RevenueExpenseForm">
								<%=al.get(al.size()-1) %>
								</html:multibox>
							</td>
							<td align="left" style="padding-right: 35px;white-space: nowrap;" >	
								<h3>&nbsp;<%=al.get(al.size()-1)%>&nbsp;</h3>
							</td>
							
					  </tr>
				 </table>
			</td>
		</tr>
		<tr>
			<td height="3px"></td>
		</tr>
				<tr>
					<td>
						<table border="0" width="100%">
							<tr>
								<td width="99%"></td>
								<td><h3>From:</h3></td>
								<td style="white-space: nowrap;">
									<html:text  name  ="RevenueExpenseForm" styleClass = "textbox" size = "10" property = "fromDate" readonly = "true"/>
										<img src = "images/calendar.gif" style="vertical-align: top; padding-left: 2px;padding-right: 2px" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].fromDate , document.forms[0].fromDate , 'mm/dd/yyyy' ),blankQuarter();"  onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
								</td>
								<td><h3>To:</h3></td>
								<td style="white-space: nowrap;" >
									<html:text  name  ="RevenueExpenseForm" styleClass = "textbox"  size = "10" property = "toDate" readonly = "true"/>
										<img src = "images/calendar.gif" style="vertical-align: top;padding-right: 0px" width = 19 height = 17 border = 0 align = "center" onclick = "return popUpCalendar( document.forms[0].toDate , document.forms[0].toDate , 'mm/dd/yyyy' ),blankQuarter();" onmouseover = "window.status = 'Date Picker';return true;" onmouseout = "window.status = '';return true;"></img>
									</td>
								<td style="padding-right: 35px"><html:button property="go" styleClass="Rbutton" onclick="return validateAll(document.forms[0]);">Go</html:button></td>
				 			</tr>
						</table>
							</td>
		  				</tr>
			<tr  style="padding: 0px;">
	  				<td valign="top" class="pageContentPadding">
	  				<table  width="100%" border="0" cellspacing="0" cellpadding="0">
	  					<tr style="padding: 0px;">	
				        	<td valign="top">
				        		<table width="100%" border="0" cellpadding="0" cellspacing="1" id="jobOwnerTable" class="tablesorter">
		  						<thead>
			  						<tr>
				  						<th rowspan="2">Job Owner</th>
				  						<th colspan="2">Expense</th>
				  						<th rowspan="2">Revenue</th>
				  						<th colspan="3">VGPM</th>
				  						<th rowspan="2">Count</th>
				  						<th rowspan="2" class="noborder"></th>
			  						</tr>
			  						<tr>
			  							<th nowrap="nowrap">Pro Forma</th>
			  							<th>Actual</th>
			  							<th nowrap="nowrap">Pro Forma</th>
			  							<th>Actual</th>
			  							<th>Delta</th>
			  						</tr>
			  				
		  						</thead>
		  						<tbody>
		  										<logic:present name="RevenueExpenseForm" property="jobOwnerList">
					        			<logic:iterate id="list" name="RevenueExpenseForm" property="jobOwnerList">
						        			<tr id="trColor">
						        				<td  align="left" nowrap="nowrap">
						        				<a href="#" onclick="secondLevelCall('<bean:write name="list" property="jobOwner"  />','<bean:write name="list" property="jobOwnerId"  />','JobOwnerTTMSummary')">
						        						<bean:write name="list" property="jobOwner"  /></a></td>
						        						 <bean:define id="pfExpense" name="list"  property="proFormaExpense" type="java.lang.String" />
						        					<% 	
						        					currency = new Double(pfExpense);
						        					pfExpense=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
													 %>
						        				<td align="right" nowrap="nowrap"><%=pfExpense%></td>
						        				 <bean:define id="expense" name="list"  property="expence" type="java.lang.String" />    
						        				 <%
						        				 currency = new Double(expense);
						        				 expense=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
												 %>  
						        				<td align="right" nowrap="nowrap"><%=expense %></td>
						        				<bean:define id="revenue" name="list"  property="revenue" type="java.lang.String" />    
						        				 <%
						        				 currency = new Double(revenue);
						        				 revenue=Formatter.format(currency).substring(1, Formatter.format(currency).indexOf("."));
												 %>
						        				<td align="right" nowrap="nowrap"><%=revenue%></td>
						        				<bean:define id="proFormaVGPM" name="list"  property="proFormaVGPM" type="java.lang.String" />   
						        				<%
						        				proFormaVGPM=proFormaVGPM.substring(0,proFormaVGPM.length()-2);
						        				%> 
						        				<td  align="center" nowrap="nowrap"><%=proFormaVGPM%></td>
						        				<bean:define id="VGPM" name="list"  property="VGPM" type="java.lang.String" />   
						        				<% 
						        				VGPM=VGPM.substring(0,VGPM.length()-2);
						        				%>
						        				<td  align="center" nowrap="nowrap"><%=VGPM %></td>
						        				<bean:define id="deltaVGPM" name="list"  property="deltaVGPM" type="java.lang.String" />   
						        				<%deltaVGPM=deltaVGPM.substring(0,deltaVGPM.length()-2); %>
						        				<td align="center" nowrap="nowrap"><%=deltaVGPM %></td>
						        				<bean:define id="occurrences" name="list"  property="occurrences" type="java.lang.String" />    
						        				 <%
						        				 amount = new Double(occurrences);
						        				 occurrences=Formatter.format(amount).substring(1, Formatter.format(amount).indexOf("."));
												 %>  
						        				<td  align="right" nowrap="nowrap"><%=occurrences %></td>
						      				<logic:lessThan name="list" property="deltaVGPM" value=".00">
						        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-red.gif" alt=""/></td>
						        			</logic:lessThan>	
						        			<logic:greaterEqual name="list" property="deltaVGPM" value=".05">
						        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-green.gif" alt=""/></td>
						        			</logic:greaterEqual>
						        		<logic:greaterThan name="list"  property="VGPM" value=".55">
						        					<logic:greaterEqual name="list" property="deltaVGPM" value="0.00">
							        					<logic:lessThan name="list" property="deltaVGPM" value="0.05">
							        						<td class="noborder" align="left" nowrap="nowrap"><img src="images/cau-blue.gif" alt=""/></td>
							        					</logic:lessThan>
						        					</logic:greaterEqual>
						        	   </logic:greaterThan>	 
						        			</tr>
					        			</logic:iterate>
					        		</logic:present>
		  						</tbody>
		  						</table>
				        	</td>
				        	</tr>
	  						
						   <tr height="15px"></tr>
						  <tr>
						  <td>
	  						<%@ include file="../../Legends.jsp" %>
	  					 </td>
	  			</tr>
	  			<tr>
							<td id="PageContentDate">Content Date:&nbsp;&nbsp;<span id="currDate"></span></td>
						  </tr>
	  					</table>
	  				</td>
	  			</tr>
	  		</table>
	  		

</html>