<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.mind.fw.util.PropertyFileUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<form>
<strong style="color: #000000;">Job Type:</strong>&nbsp;
<%if(null!=request.getAttribute("jobFilterType")){%>
<select  id="selectedJob" name="JobType" onchange="getJobFilteredType(document.getElementById('selectedJob').value);"   >
		
		<%if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("All.type1"))){ %>
			<option   style="text-align: right; font-size: small;"  selected="selected" value="<bean:message bundle="JobFilterType" key="All.type1"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="All.type1"/></option>
		<%}else{ %>
			<option  style="text-align: right; font-size: small;" value="<bean:message bundle="JobFilterType" key="All.type1"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="All.type1"/></option>
			<%} %>
	<option   style="color: highlight;" value="">Project</option>

		<%if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("Project.type1"))){ %>
			<option   style="text-align: right; font-size: small;"  selected="selected" value="<bean:message bundle="JobFilterType" key="Project.type1"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="Project.type1"/></option>
		<%}else{ %>
			<option  style="text-align: right; font-size: small;" value="<bean:message bundle="JobFilterType" key="Project.type1"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="Project.type1"/></option>
			<%} %>
		<%if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("Project.type2"))){ %>
			<option selected="selected" value="<bean:message bundle="JobFilterType" key="Project.type2"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="Project.type2"/></option>
		<%}else{ %>
			<option value="<bean:message bundle="JobFilterType" key="Project.type2"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="Project.type2"/></option>
			<%} %>
		<%if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("Project.type3"))){ %>
			<option selected="selected" value="<bean:message bundle="JobFilterType" key="Project.type3"/>" >&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="Project.type3"/></option>
		<%}else{ %>
			<option value="<bean:message bundle="JobFilterType" key="Project.type3"/>" >&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="Project.type3"/></option>
			
			<%} %>
	<option value="" style="color: highlight;" ><b>Help Desk</b></option>
	
		<%if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("HelpDesk.type1"))) {%>
			<option selected="selected" value="<bean:message bundle="JobFilterType" key="HelpDesk.type1"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="HelpDesk.type1"/></option>
		<% }else{ %>
			<option value="<bean:message bundle="JobFilterType" key="HelpDesk.type1"/>" >&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="HelpDesk.type1"/></option>
		<%}if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("HelpDesk.type2"))){ %>					
			<option selected="selected" value="<bean:message bundle="JobFilterType" key="HelpDesk.type2"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="HelpDesk.type2"/></option>
		<%}else{ %>
			<option value="<bean:message bundle="JobFilterType" key="HelpDesk.type2"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="HelpDesk.type2"/></option>
		<%}if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("HelpDesk.type3"))){ %>
			<option selected="selected" value="<bean:message bundle="JobFilterType" key="HelpDesk.type3"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="HelpDesk.type3"/> </option>
		<%}else {%>
			<option value="<bean:message bundle="JobFilterType" key="HelpDesk.type3"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="HelpDesk.type3"/></option>
		<%}if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("HelpDesk.type4.identifier"))) {%>
			<option selected="selected"  value="HelpDesk.type4.identifier">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="HelpDesk.type4"/></option>
		<%}else{ %>
			<option value="<bean:message bundle="JobFilterType" key="HelpDesk.type4.identifier"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="HelpDesk.type4"/></option>
		<%} %>
	<option value="" style="color: highlight;" >NetMedX</option>
		
		<%if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("NetMedX.type1"))){ %>
			<option selected="selected" value="<bean:message bundle="JobFilterType" key="NetMedX.type1"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type1"/></option>
		<%}else{%>
			<option value="<bean:message bundle="JobFilterType" key="NetMedX.type1"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type1"/></option>
		<%}if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("NetMedX.type2.identifier"))) {%>	
			<option selected="selected" value="<bean:message bundle="JobFilterType" key="NetMedX.type2.identifier"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type2"/></option>
		<%}else{ %>
			<option value="<bean:message bundle="JobFilterType" key="NetMedX.type2.identifier"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type2"/></option>
		<%}if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("NetMedX.type3"))) {%>
			<option selected="selected" value="<bean:message bundle="JobFilterType" key="NetMedX.type3"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type3"/></option>
		<%}else{ %>
			<option value="<bean:message bundle="JobFilterType" key="NetMedX.type3"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type3"/></option>
		<%}if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("NetMedX.type4"))) {%>
			<option selected="selected" value="<bean:message bundle="JobFilterType" key="NetMedX.type4"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type4"/></option>
		<%}else {%>
			<option value="<bean:message bundle="JobFilterType" key="NetMedX.type4"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type4"/></option>
		<%}if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("NetMedX.type5"))){ %>
			<option selected="selected" value="<bean:message bundle="JobFilterType" key="NetMedX.type5"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type5"/></option>
		<%}else{ %>
			<option value="<bean:message bundle="JobFilterType" key="NetMedX.type5"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type5"/></option>
		<%}if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("NetMedX.type6"))){ %>
			<option selected="selected"  value="<bean:message bundle="JobFilterType" key="NetMedX.type6"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type6"/></option>
		<%}else{ %>
		<option value="<bean:message bundle="JobFilterType" key="NetMedX.type6"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="NetMedX.type6"/></option>
		<%} %>
	<option value="" style="color: highlight;">MSP</option>
	
		<%if(request.getAttribute("jobFilterType").equals(PropertyFileUtil.getAppProperty("MSP.type1"))) {%>
			<option selected="selected" value="<bean:message bundle="JobFilterType" key="MSP.type1"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="MSP.type1"/></option>
		<%}else{ %>
			<option value="<bean:message bundle="JobFilterType" key="MSP.type1"/>">&nbsp;&nbsp;&nbsp;<bean:message bundle="JobFilterType" key="MSP.type1"/></option>
		<%} %>
</select>
<%}%>
</form>
</html>