<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Revenue/Cost Navigator</title>
<script>
var contextPath = "<%=request.getContextPath() %>";
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/include.js"></script>
</head>
<body>
<table width="100%" border="0" style="background: url(<%=request.getContextPath() %>/images/bggrey.jpg) repeat-x; margin: 0;" cellpadding="0" cellspacing="0"><tr><td>
	<ul class="dropdown">
	     <li><a href="#">Revenue/Expense&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/white-arrow.gif" border="0"></img></a>
			    <ul class="sub_menu">
			        <li><a href="<%=request.getContextPath() %>/RevenueExpenseAction.do?ref=revenueExpense&reportType=RevenueCost&filterType=All Jobs">Revenue/Expense</a></li>
			       	<li><a href="<%=request.getContextPath() %>/RevenueExpenseAction.do?ref=revenueExpenseByBDM&reportType=RevenueCost&parm=firstTime">Revenue/Expense by BDM</a></li>
			        <li><a href="<%=request.getContextPath() %>/RevenueExpenseAction.do?ref=revenueExpenseBySRPM&reportType=RevenueCost&parm=firstTime">Revenue/Expense by SPM</a></li>
			        <li><a href="<%=request.getContextPath() %>/RevenueExpenseAction.do?ref=revenueExpenseBySRPMChart&reportType=RevenueCost&parameter=spmRevenueChart">SPM Revenue Chart</a></li>
			        <li><a href="<%=request.getContextPath() %>/RevenueExpenseAction.do?ref=revenueExpenseByProjectManager&reportType=RevenueCost&parm=firstTime">Revenue/Expense by Project Manager</a></li>    
			        <li><a href="<%=request.getContextPath() %>/RevenueExpenseAction.do?ref=revenueExpenseByJobOwner&reportType=RevenueCost&parm=firstTime">Revenue/Expense by Job Owner</a></li>
			  		<li><a href="<%=request.getContextPath() %>/RevenueExpenseAction.do?ref=revenueExpenseByCustomer&reportType=RevenueCost&parm=firstTime">Revenue/Expense by Customer</a></li>
			   </ul>
		</li>
	</ul>
</td></tr>
	<tr>
		<td id="PageContentBreadCrumb">
			<div class="BreadCrumbText1"><a href="#">Reporting</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<a 
			>Operations</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<a 
			>Revenue/Expense</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<span id="lcBreadCrumbText"></span></div>
		</td>
	</tr>
</table>
<script>
activateDDMenu();
</script>
</body>
</html>