<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<bean:define id="graphXML" name="RevenueExpenseForm" property="graphXML" />
 
<script>
var chartWidth = "800";
var graphXML = '<%=graphXML%>';

$(document).ready(function() {
	$("#lcBreadCrumbText").html("Revenue/Expense by SPM");
	$("#spmTable tr:even").addClass("even");
	$("#spmTable tr:odd").addClass("odd");
});
</script>

</head>
	  		<table id="contentTable" border="0" width="100%" cellspacing="0" cellpadding="0"
			style="padding: 0px; height: 328px;">
	  			<tr  style="padding: 0px;">
	  				<td valign="top" class="pageContentPadding">
	  					<table  width="100%" border="0" cellspacing="0" cellpadding="0">
	  						<tr style="padding: 0px;">	
						        <td valign="top">
						          <table width="165" border="0" cellpadding="0" cellspacing="0" class="outlineYellow3">
						            <tr class="dataRow1">
						              <td class="dataRow1rA">
						              <div id="chartMultiAxisdiv">This text is replaced by the chart.</div>
						             	<script>javascript:renderMultiAxisChart();</script>
						              </td>
						            </tr>
						          </table>
						         </td>
						      </tr>
						     
	  					</table>
	  				</td>
	  			</tr>
	  			<tr>
							<td id="PageContentDate">Content Date:&nbsp;&nbsp;<span id="currDate"></span></td>
						  </tr>
	  		</table>
	  		<div id="jobOwnerChartModal" style="display: none; cursor: default; border: 0px; border-color: red;" > 
		        <div  style="border: 0px;">
		        	<table border='0px' cellspacing="0" cellpadding="0" class="headerpaleyellow" width="100%">
		        		<tr>
		        			<td align="left" id="jobTitle" height="20px"></td>
		        			<td align="right"><img height="15" width="15" alt="Close" src="images/delete.gif" id="close" onclick="closeWindow();"></td>
		        		</tr>
		        	</table>
		        </div>
        		<div id="jobOwnerChartValue" style=" padding-top:4px; overflow: visible; height:450px; overflow-x:auto; overflow-y:auto;"></div>
			</div>

</html>