<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Labor Cost Navigator</title>
<script>
var contextPath = "<%=request.getContextPath() %>";
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/include.js"></script>
</head>
<body>
<table width="100%" border="0" style="background: url(<%=request.getContextPath() %>/images/bggrey.jpg) repeat-x; margin: 0;" cellpadding="0" cellspacing="0"><tr><td>
	<ul class="dropdown">
	    <li><a href="#">Labor Costs&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/white-arrow.gif" border="0"></img></a>
	        <ul class="sub_menu">
	            <li><a href="<%=request.getContextPath() %>/LaborCostAction.do?ref=topLevelLaborCost">Top Level Labor Cost</a></li>
	            <li><a href="<%=request.getContextPath() %>/LaborCostAction.do?ref=technicianLaborCost">Technician Labor Cost</a></li>
				<li><a href="<%=request.getContextPath() %>/LaborCostAction.do?ref=srPMLaborCost">Labor Cost by Sr.PM</a></li>
				<li><a href="<%=request.getContextPath() %>/LaborCostByLocationAction.do?ref=laborCostBylocation">Labor Cost by Location</a></li>
				<li><a href="<%=request.getContextPath() %>/LaborCostAction.do?ref=laborCostByCriticality">Labor Cost by Criticality</a></li>
	        </ul>
	    </li>
	</ul>
</td></tr>
	<tr>
		<td id="PageContentBreadCrumb">
			<div class="BreadCrumbText1"><a href="<%=request.getContextPath() %>/LaborCostAction.do?ref=topLevelLaborCost">Reporting</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<a 
			>Contingent</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<a 
			>Labor Costs</a>&nbsp;&nbsp;<img src="<%=request.getContextPath() %>/images/historyBullets.gif" height="5" width="5"/>&nbsp;&nbsp;<span id="lcBreadCrumbText"></span></div>
		</td>
	</tr>
</table>
<script>
activateDDMenu();
</script>
</body>
</html>