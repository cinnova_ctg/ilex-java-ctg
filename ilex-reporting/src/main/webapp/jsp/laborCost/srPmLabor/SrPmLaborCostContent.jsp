<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<bean:define id="graphXML" name="LaborCostForm" property="graphXML" />
 
<script>
var chartWidth = "800";
var graphXML = '<%=graphXML%>';

$(document).ready(function() {
	$("#lcBreadCrumbText").html("Labor Cost by Senior Project Manager for Technicians");
});

</script>
</head>
	  		<table id="contentTable" border="0" width="100%" cellspacing="0" cellpadding="0"
			style="padding: 0px; height: 328px;">
	  			<tr  style="padding: 0px;">
	  				<td valign="top" class="pageContentPadding">
	  					<table  width="100%" border="0" cellspacing="0" cellpadding="0">
	  						<tr style="padding: 0px;">	
						        <td valign="top">
						          <table width="165" border="0" cellpadding="0" cellspacing="0" class="outlineYellow3">
						            <tr class="dataRow1">
						              <td class="dataRow1rA">
						              <div id="chartMultiAxisdiv">This text is replaced by the chart.</div>
						             	<script>javascript:renderMultiAxisChart();</script>
						              </td>
						            </tr>
						          </table>
						         </td>
						      </tr>
						      <tr>
								<td id="PageContentDate">Content Date:&nbsp;&nbsp;<span id="currDate"></span></td>
							  </tr>
	  					</table>
	  				</td>
	  			</tr>
	  		</table>

</html>