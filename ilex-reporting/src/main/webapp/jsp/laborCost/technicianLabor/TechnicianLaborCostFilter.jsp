<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<strong style="color: #000000;">Labor Category:</strong>&nbsp;
		<html:select name="LaborCostForm" property="laborCategoryFilter" styleClass="select" onchange="getFilteredTechlaborCost();">
   				<html:optionsCollection property="laborCategoryList" value = "value" label = "label" />
  			</html:select>
