<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@ page
	import="java.text.NumberFormat,java.util.Locale,java.util.Calendar"%>
<%
Calendar calendar = Calendar.getInstance();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="<%=request.getContextPath() %>/js/jquery.tablesorter.js"></script>
<link href="<%=request.getContextPath() %>/css/tableSorter.css"
	rel="stylesheet" type="text/css" />
<title>Labor Cost by Location for PVS Labor</title>

<script>
$(document).ready(function() {
	$("#lcBreadCrumbText").html("Labor Cost by Location for PVS Labor");

	 $("#sortTable").tablesorter({
		 headers: {
		 1: {sorter:'digit'},
		 2: {sorter:'digit'},
		 3: {sorter: false}
	 } ,
	 sortList: [[0,0]],
	 stripeRowsOnStartUp: true,
	 widgets: ['zebra'],
	 sortInitialOrder: "desc"
	}); 
});




function viewLocation() {
	if(document.forms[0].startMonth.value=='')
		{
		alert("Please select start Month");
		return false;
		};
		if(document.forms[0].startYear.value=='')
		{
		alert("Please select start Year");
		return false;
		};
		if(document.forms[0].endMonth.value=='')
		{
		alert("Please select end Month");
		return false;
		};
		if(document.forms[0].endYear.value=='')
		{
		alert("Please select end Year");
		return false;
		};
		if(document.forms[0].startYear.value>document.forms[0].endYear.value)
			{
			alert("Start year should not be greater than end year");
			return false;
			}
		if(document.forms[0].startYear.value==document.forms[0].endYear.value)
		{ 
			var status=0;
			if(document.forms[0].startMonth.value>document.forms[0].endMonth.value)
				{
				alert("Start month should not be greater than end month");
				status=1;
				return false;
				}
			if(status==1)
			  return false;
		}
		
	$("form[name='LaborCostByLocationForm']").attr('action' , 'LaborCostByLocationAction.do?ref=laborCostBylocation');
	$("form[name='LaborCostByLocationForm']").submit();
}
</script>
</head>
<div style="text-align: right;padding-right:28px;">
	<strong style="color: #000000; font-size: 12px;">Start Date:</strong>&nbsp;
	<html:select styleClass="select" property="startMonth">
	<html:option value="">Month</html:option>
	<html:optionsCollection property="monthList" value = "value" label = "label" />
      </html:select>
	<html:select styleClass="select" property="startYear">
		<%   int year=calendar.get(Calendar.YEAR); %>
		<html:option value="">Year</html:option>
		<%for(int i=year-9;i<=year;++i){
										  			String val=i+"";
										  			%>
		<html:option value='<%=val%>'><%=i %></html:option>
		<%} %>
	</html:select>
	
	&nbsp;&nbsp; <strong style="color: #000000; font-size: 12px;">End
		Date:</strong>&nbsp;
	
	<html:select styleClass="select" property="endMonth">
	<html:option value="">Month</html:option>
     <html:optionsCollection property="monthList" value = "value" label = "label" />
	</html:select>
	<html:select styleClass="select" property="endYear">
		<% int year=calendar.get(Calendar.YEAR);%>
		<html:option value="">Year</html:option>
		<%for(int i=year-9;i<=year;++i){
										  			String val=i+"";
										  			%>
		<html:option value='<%=val%>'><%=i %></html:option>
		<%} %>
	</html:select>
</div>
<table id="contentTable" border="0" cellspacing="0" cellpadding="0"
	style="padding: 0px; height: 328px; width: 600px;">
	<tr style="padding: 0px;">
		<td valign="top" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr style="padding: 0px;">
					<td valign="top">
						<table width="100%" border="0" cellpadding="0" cellspacing="1"
							id="sortTable" class="tablesorter">
							<thead>
								<tr>
									<th rowspan="2" width="55%">Location</th>
									<th rowspan="2" width="15%">Labor Rate</th>
									<td colspan="2">Occurrences</td>
								</tr>
								<tr>
									<th width="15%">Number</th>
									<th width="15%">Last Date</th>
								</tr>
							</thead>
							<tbody>
								<logic:present name="LaborCostByLocationForm"
									property="lcByLocationList">
									<logic:iterate id="list" name="LaborCostByLocationForm"
										property="lcByLocationList">
										<tr id="trColor">
											<td class="Irowfont"><bean:write name="list"
													property="location" /></td>
											<td class="Irowfont" align="right"><bean:write
													name="list" property="laborRate" /></td>
											<td class="Irowfont" align="right"><bean:write
													name="list" property="numberCount" /></td>
											<td class="Irowfont" align="center"><bean:write
													name="list" property="lastDate" /></td>
										</tr>
									</logic:iterate>
								</logic:present>
							</tbody>

						</table>
					</td>
				</tr>
				<tr>
					<td id="PageContentDate">Content Date:&nbsp;&nbsp;<span
						id="currDate"></span></td>
				</tr>
			</table>
		</td>
	</tr>
</table>


</html>