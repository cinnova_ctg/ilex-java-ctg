<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script>
var contextPath = "<%=request.getContextPath()%>";
</script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/include.js"></script>


<script type="text/javascript">
			$(function(){

				// Accordion
				
			});
</script>
<style type="text/css">
			/*demo page css*/
			body{font: 62.5% "Trebuchet MS", sans-serif;margin:0px; padding:0px; width: 100%; height: 100%; vertical-align: top;}
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
</style>
</head>
<%



	StringBuffer url = new StringBuffer();
	url = request.getRequestURL();
	 System.out.println(url);
	 String phpBaseURL = "http://ilex-qa.contingent.local/";
	 String dbBaseURL = "http://wc-ilexdbstage";//For Staging reporting.
	 
	 if (url.toString().contains(
				"http://ilex.contingent.local/Reporting/")
				|| url.toString().contains(
						"http://ilex-prod2.contingent.local/Reporting/")) {
		 
		 phpBaseURL = "http://ilex-php.contingent.local/";
		 dbBaseURL = "http://db-prod2";//For Production.
	 }
	 
	
%>
<body background="/images/sidebg.jpg">
<table id="visibleFrame" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" valign="top">
  <tr>
    <td>
		<div id="accordion">
			<div>
				<h3><a href="#">Contingent</a></h3>
				<ul>
				<li><a href="<%=request.getContextPath()%>/DB1.do?action=topLevelSummary" target="mainBodyFrame">Top Level Summary</a></li>
				<li><a href="<%=request.getContextPath()%>/RevenueExpenseAction.do?ref=revenueExpense&filterType=All Jobs" target="mainBodyFrame">Financials</a></li>
				<li><a href="<%=request.getContextPath()%>/LaborCostAction.do?ref=topLevelLaborCost" target="mainBodyFrame">Labor Costs</a></li>
                            <li><a href="http://ilex.contingent.local/rpt/LaborUsageAnalysis.xo?ac=lua&rpt=7" target="mainBodyFrame">Labor Utilization Analysis</a></li>
                            <li><a href="http://ilex.contingent.local/rpt/Troll.xo?ac=com_new_data" target="mainBodyFrame">Commissions</a></li>
                            <li><a href="<%=request.getContextPath() %>content/reports/ops_summary/" target="mainBodyFrame">Operations Summary</a></li>
                            <li><a href="<%=request.getContextPath()%>/TL2.do?ac=dragCoefficient" target="mainBodyFrame">Drag Coefficient</a></li>
                             <li><a href="<%=request.getContextPath()%>/CNSRPT.do?ac=weekly_personnel" target="mainBodyFrame">Personnel Head Count</a></li>
				</ul>
			</div>
			
			<div>
				<h3><a href="#">Sales</a></h3>
				<ul>
					<li><a href="http://ilex.contingent.local/rpt/Troll.xo?ac=com_new_data" target="mainBodyFrame">Commissions</a></li>
				</ul>
			</div>

			<div>
				<h3><a href="#">Operations</a></h3>
				<ul>
				<li><a href="<%=request.getContextPath()%>/RevenueExpenseAction.do?ref=revenueExpenseBySRPM&reportType=RevenueCost&filterType=All Jobs&parm=firstTime" target="mainBodyFrame">Revenue/Expense</a></li>
				<li><a href="<%=phpBaseURL %>content/ilex/#/report/first_time_usage" target="mainBodyFrame">Minuteman Utilization</a></li>
				<li style="padding-top: 0px; padding-bottom: 0px;"><a href="<%=request.getContextPath()%>/MinutemanSpeedpayAction.do?ref=usageByOccurence" target="mainBodyFrame">MM/SP Company</a></li>

<!--                             <li><a href="http://ilex.contingent.local/rpt/MSG11.xo?ac=mmsp_goals1" target="mainBodyFrame">MM/SP Goals</a></li> -->

                            <li><a href="http://ilex.contingent.local/rpt/POA1.xo?ac=report1&report=0" target="mainBodyFrame">MM/SP Sr. PM</a></li>
                            <li><a href="http://ilex.contingent.local/rpt/POA1.xo?ac=report1&report=4" target="mainBodyFrame">MM/SP Job Owner</a></li>



				<li><a href="<%=request.getContextPath()%>/MinutemanSpeedpayAction.do?ref=outOfBound" target="mainBodyFrame">Out-of-Bounds</a></li>
				<li><a href="<%=request.getContextPath()%>/MinutemanSpeedpayAction.do?ref=jobStatusSummary" target="mainBodyFrame">Job Status Summary</a></li>
				<li><a href="<%=request.getContextPath()%>/MinutemanSpeedpayAction.do?ref=dailyJobReport" target="mainBodyFrame">Daily Job Summary</a></li>
                <li><a href="http://ilex.contingent.local/rpt/WK.xo?ac=workload1&job_type=All" target="mainBodyFrame">Workload/User Actions</a></li>
                <li><a href="<%=request.getContextPath()%>/MinutemanSpeedpayAction.do?ref=pvsUsage" target="mainBodyFrame">PVS Usage</a></li>
				</ul>
			</div>

			<div>
				<h3><a href="#">Managed Services</a></h3>
				<ul>
					 <li><a href="/rpt/MSRPT.xo?ac=monthly_by_user" target="mainBodyFrame">Personnel vs Workload</a></li>
					 <li><a href="/rpt/MSRPT.xo?ac=weekly_ave_outage" target="mainBodyFrame">Personnel vs Average Outage</a></li>
					 <li><a href="/rpt/MSRPT.xo?ac=weekly_outage_percent" target="mainBodyFrame">Personnel vs Outage Percent</a></li>
					 <li><a href="http://192.168.32.56/rpt/msrpt/impaired_circuit.jsp" target="mainBodyFrame">Daily Impaired Circuit</a></li>
<!--                                     <li><a href="http://ilex.contingent.local/rpt/NetFlow.xo?ac=usage2" target="mainBodyFrame">NetFlow Verification Rpt</a></li> -->
<!--                                     <li><a href="http://ilex.contingent.local/rpt/NetFlow.xo?ac=usage3" target="mainBodyFrame">CFA Distribution</a></li> -->
				</ul>
			</div>

			<div>
				<h3><a href="#">Accounting</a></h3>
				<ul>
                                   <li><a href="http://ilex.contingent.local/rpt/ACCTEXP.xo?ac=ttm" target="mainBodyFrame"">Expense Breakout</a></li>
					<li><a href="<%=request.getContextPath()%>/MinutemanSpeedpayAction.do?ref=OhioSalesTaxImpactReport" target="mainBodyFrame">Ohio Sales Tax Impact</a></li>
                                   <li><a href="http://ilex.contingent.local/rpt/ACCTEXP.xo?ac=revenue_allocation" target="mainBodyFrame"">Revenue Allocation</a></li>

				
				</ul>
			</div>

			<div>
				<h3><a href="#">Partners</a></h3>
				<ul>
					<li><a href="#">Partners</a></li>
					<li><a href="<%=request.getContextPath()%>/CPDUserAction.do?function=getAllData&days=30" target="mainBodyFrame">CPD User Action Report</a></li>
				</ul>
			</div>
			<div>
				<h3><a href="#">Help Desk</a></h3>
				<ul>
					<li><a href="<%=request.getContextPath()%>/HDTicketData.do?function=getTicketData&days=30" target="mainBodyFrame">NOC Ticket Data</a></li>
					<li><a href="<%=request.getContextPath()%>/ManagedServiceAction.do?ref=ticketCount" target="mainBodyFrame">Worked / Closed Ticket Count</a></li>
					<li><a href="<%=request.getContextPath()%>/NocAgentAction.do?function=getAgentData&days=30" target="mainBodyFrame">User Actions by NOC Agent</a></li>
					<li><a href="<%=request.getContextPath()%>/NocAgentTicket.do?function=getAgentTicketData&days=30" target="mainBodyFrame">Total Tickets by NOC Agent</a></li>
					<%-- <li><a href="<%=request.getContextPath() %>/HDTicketData.do?function=getNewTicketData" target="mainBodyFrame">New/Resolved Tickets</a></li> --%>
					<li><a href="<%=request.getContextPath()%>/HDTicketData.do?function=getTickeResolutiontData" target="mainBodyFrame">First time/Full Resolution</a></li>
					<li><a href="<%=request.getContextPath()%>/HDTicketData.do?function=getAllResolutiontData" target="mainBodyFrame">All Resolutions</a></li>
					<li><a href="<%=request.getContextPath()%>/HDTicketData.do?function=getDownTimeData" target="mainBodyFrame">Downtime Per Carrier</a></li>
					
					<li><a href="<%=dbBaseURL%>/reportserver/?%2fHelpDesk%2fTicketTimeToAcceptByClient&rs:Command=Render" target="mainBodyFrame">Ticket Accept Time-Client</a></li>
					<li><a href="<%=dbBaseURL%>/reportserver/?%2fHelpDesk%2fTicketTimeToAcceptByAgent&rs:Command=Render" target="mainBodyFrame">Ticket Accept Time-Agent</a></li>
					
			<%-- <!--  		<li><a href="<%=request.getContextPath() %>/HDTicketData.do?function=getTicketData&days=30" target="mainBodyFrame">Inbound calls by NOC Agent</a></li>
					<li><a href="<%=request.getContextPath() %>/HDTicketData.do?function=getTicketData&days=30" target="mainBodyFrame">Outbound Calls by NOC Agent</a></li>--> --%>
				</ul>
			</div>
			
		</div>
	</td>
  </tr>
</table>		
</body>
</html>
