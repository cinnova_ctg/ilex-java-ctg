<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

</head>
<%
	StringBuffer url = new StringBuffer();
	url = request.getRequestURL();
	 
	 String phpBaseURL = "http://ilex-qa.contingent.local/";
	 
	 if (url.toString().contains(
				"http://ilex.contingent.local/Reporting/")
				|| url.toString().contains(
						"http://ilex-prod2.contingent.local/Reporting/")) {
		 
		 phpBaseURL = "http://ilex-php.contingent.local/";
	 }
%>
<table border="0" width="400" align="left" bgcolor="#ffffff" style="border: solid; border-color: #0f55ca;border-width: 1px;">
	<tr>
		<td style="padding-left: 10px;padding-top: 10px;padding-bottom: 10px;" valign="top">
			<div class="menuList">Contingent
				<ul>
					<li style="height: 5px;">&nbsp;</li>	
					<li><a href="<%=request.getContextPath() %>/jsp/topLevelSummary/topLevelReport.jsp" target="mainBodyFrame">Top Level Summary</a></li>
					<li><a href="<%=request.getContextPath() %>/RevenueExpenseAction.do?ref=revenueExpense&filterType=All Jobs" target="mainBodyFrame">Financials</a></li>
					<li><a href="<%=request.getContextPath() %>/LaborCostAction.do?ref=topLevelLaborCost" target="mainBodyFrame">Labor Costs</a></li>
					</ul>
			</div>
		</td>
		<td style="padding-top: 10px;padding-bottom: 10px;">
			<table border="0" style="height: 220px;width: 1px;" cellspacing="0" cellpadding="0" bgcolor="#bbbbbb">
				<tr>
					<td width="1"></td>
				</tr>
			</table>
		</td>
		<td style="padding-left: 10px;padding-top: 10px;padding-bottom: 10px;" valign="top">
			<div class="menuList">Sales
				<ul>
					<li style="height: 5px;">&nbsp;</li>
					<li><a href="#">Commissions</a></li>
				</ul>
			<br/>
			</div>
			<div class="menuList">Operations
				<ul>
					<li style="height: 5px;">&nbsp;</li>
					<li><a href="<%=request.getContextPath() %>/RevenueExpenseAction.do?ref=revenueExpenseBySRPM&reportType=RevenueCost&filterType=All Jobs&parm=firstTime" target="mainBodyFrame">Revenue/Expense</a></li>
					<li><a href="<%=phpBaseURL %>content/reports/ops_summary/" target="mainBodyFrame">Minuteman Utilization</a></li>
					<li><a href="<%=request.getContextPath() %>/MinutemanSpeedpayAction.do?ref=usageByOccurence" target="mainBodyFrame">Minuteman/Speedpay</a></li>
					<li><a href="<%=request.getContextPath() %>/jsp/exceptionAnalysis/exceptionAnalysisReport.jsp" target="mainBodyFrame">Out-of-Bounds</a></li>
					<li><a href="<%=request.getContextPath() %>/jsp/JobStatusSummary/JobStatusSummaryReport.jsp" target="mainBodyFrame">Job Status Summary</a></li>
					<li><a href="<%=request.getContextPath() %>/jsp/DailyJobSummary/DailyJobSummaryReport.jsp" target="mainBodyFrame">Daily Job Summary</a></li>
					
				</ul>
			</div>
		</td>
		<td style="padding-top: 10px;padding-bottom: 10px;">
			<table border="0" style="height: 220px;width: 1px;" cellspacing="0" cellpadding="0" bgcolor="#bbbbbb">
				<tr>
					<td width="1"></td>
				</tr>
			</table>
		</td>
		<td style="padding-left: 10px;padding-top: 10px;padding-bottom: 10px;" valign="top">
			<div class="menuList">Managed Services
				<ul>
					<li style="height: 5px;">&nbsp;</li>
					<li><a href="#">Help Desk</a></li>
				
				</ul>
			</div>
			<br/>
			<div class="menuList">Accounting
				<ul>
					<li style="height: 5px;">&nbsp;</li>
					<li><a href="#">Accounting</a></li>
					<li><a href="<%=request.getContextPath() %>/jsp/OhioSalesTaxImpact/OhioSalesTaxImpactReport.jsp" target="mainBodyFrame">Ohio Sales Tax Impact</a></li>
				</ul>
			</div>
			<br/>
			<div class="menuList">Partners
				<ul>
					<li style="height: 5px;">&nbsp;</li>
					<li><a href="#">Partners</a></li>
				</ul>
			</div>
		</td>
	</tr>
</table>
