<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<body bgcolor="#FFFFFF">
<table id="contentTable" width="100%" border="0" align="center" cellpadding="0"
	cellspacing="0" style="padding-left: 0px; padding-right: 0px;">
	<tr>
		<td height="328">
		<table border="0" width="600" cellspacing="0" cellpadding="0"
			style="padding: 0px; height: 328px;">
			<tr style="padding: 0px;">
				<td valign="top" class="pageContentPadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr style="padding: 0px;">
							<td valign="top" id="PageContentBg2">
							<div id="pane1" style="background-color: black; width: 100%">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" >
								  <tr>
								    <td>&nbsp;</td>
								  </tr>
								  <tr>
								    <td valign="top">&nbsp;</td>
								  </tr>
								  <tr>
								    <td valign="top">&nbsp;</td>
								  </tr>
								  <tr>
								    <td valign="top"><table width="50%" border="0" cellspacing="5" cellpadding="5">
								      <tr class="BigBoard">
								        <td>nr</td>
								        <td>-30</td>
								        <td>cur</td>
								        <td>30+</td>
								        <td>60+</td>
								        <td>ttma</td>
								      </tr>
								      <tr class="BigBoard">
								        <td>&nbsp;</td>
								        <td class="Green">1.21</td>
								        <td class="Red">0.88</td>
								        <td class="Red">0.67</td>
								        <td class="Red">0.03</td>
								        <td class="Blue">1.24</td>
								      </tr>
								    </table>
								    <br />
								    <br />
								    <table width="50%" border="0" cellspacing="5" cellpadding="5">
								      <tr class="BigBoard">
								        <td>gp</td>
								        <td>-60</td>
								        <td>-30</td>
								        <td>CUR</td>
								        <td>ttma</td>
								        <td>ltma</td>
								      </tr>
								      <tr class="BigBoard">
								        <td>&nbsp;</td>
								        <td class="Green">51%</td>
								        <td class="Red">45%</td>
								        <td class="Red"><span class="Green">52%</span></td>
								        <td class="Red"><span class="Blue">51%</span></td>
								        <td class="Blue">49%</td>
								      </tr>
								      <tr class="BigBoard">
								        <td>dk</td>
								        <td class="Green">51%</td>
								        <td class="Red">45%</td>
								        <td class="Red"><span class="Green">52%</span></td>
								        <td class="Red"><span class="Blue">51%</span></td>
								        <td class="Blue">49%</td>
								        </tr>
								      <tr class="BigBoard">
								        <td>lc</td>
								        <td class="Green">51%</td>
								        <td class="Red">45%</td>
								        <td class="Red"><span class="Green">52%</span></td>
								        <td class="Red"><span class="Blue">51%</span></td>
								        <td class="Blue">49%</td>
								        </tr>
								      <tr class="BigBoard">
								        <td>hz</td>
								        <td class="Green">51%</td>
								        <td class="Red">45%</td>
								        <td class="Red"><span class="Green">52%</span></td>
								        <td class="Red"><span class="Blue">51%</span></td>
								        <td class="Blue">49%</td>
								        </tr>
								    </table>
								    <br />
								    <br />
								    <table width="40%" border="0" cellspacing="5" cellpadding="5">
								      <tr class="BigBoard">
								        <td>hc</td>
								        <td>-60</td>
								        <td>-30</td>
								        <td>cur</td>
								        <td>ttma</td>
								        </tr>
								      <tr class="BigBoard">
								        <td>&nbsp;</td>
								        <td class="Green">65</td>
								        <td class="Red"><span class="Green">68</span></td>
								        <td class="Red"><span class="Green">70</span></td>
								        <td class="Red"><span class="Blue">56</span></td>
								        </tr>
								    </table>
								    <br />
								    <br />
								    <table width="40%" border="0" cellspacing="5" cellpadding="5">
								      <tr class="BigBoard">
								        <td>pr</td>
								        <td>-60</td>
								        <td>-30</td>
								        <td>cur</td>
								        <td>ttma</td>
								      </tr>
								      <tr class="BigBoard">
								        <td>&nbsp;</td>
								        <td class="Green">.028</td>
								        <td class="Red"><span class="Red">.015</span></td>
								        <td class="Red"><span class="Green">.029</span></td>
								        <td class="Red"><span class="Blue">.021</span></td>
								      </tr>
								    </table>
								    <br />
								    <br />
								    <br />
								    <br /></td>
								  </tr>
								</table>
							</div>
							</td>
						</tr>
						<tr>
						    <td id="PageContentDate">Content Date:&nbsp;&nbsp;<span id="currDate"></span></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</body>
</html>
