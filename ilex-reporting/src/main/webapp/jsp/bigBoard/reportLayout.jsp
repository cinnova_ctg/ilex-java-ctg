<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-logic.tld" prefix = "logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script>
var contextPath = "<%=request.getContextPath() %>";
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/FusionCharts.js" ></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/include.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.dropdownPlain.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/css_browser_selector.js"></script>
<link href="<%=request.getContextPath() %>/css/style.css" rel="stylesheet" type="text/css" />

<script>
function display(){
	if(document.getElementById("menuList").style.display == 'none')
	{
		$("#menuList").show();
	}
	else {
		$("#menuList").hide();
	}
}

function formatDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;//January is 0!
	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd}
	if(mm<10){mm='0'+mm}
	return mm+'/'+dd+'/'+yyyy;
}
</script>


</head>
<body>
	<jsp:include page="../reportLayoutTemplate.jsp"></jsp:include>
<script>
var d = formatDate();
document.getElementById("currDate").innerHTML = d;
</script>
</body>
</html>