// REGULAR EXPRESSION DECLARATIONS
// Notes which apply to all the regexps below:
// (1) We want to only match strings exactly. In other words,
//     we only want to return true if the string being tested
//     matches the regular expression with no leading or trailing
//     unmatched characters. So, we begin each regexp with
//     the special character ^ (which matches beginning of input)
//     and end each regexp with the special character $ (which
//     matches end of input).
// (2) In the below comments we use these abbreviations:
//     BOI = Beginning Of Input
//     EOI = End Of Input
// (3) For explanations of the regexp special characters such as
//     ^ $ \s + [] \d * ! ? \ .
//     see http://developer.netscape.com/library/documentation/communicator/jsguide/regexp.htm

//FUNCTIONS IN THIS JS FILE

// isUSPhoneNumber (s [,eok])  True if string s is a valid U.S. Phone Number. 
// isEmail (s [,eok])                   True if string s is a valid email address.
// isFloat (s [,eok])                    True if string s is an unsigned floating point (real) number. (Integers also OK.)
// ShowMsg(str)                        alert 
// ValidateDate(objField)             true if valid date else false
// invalidChar(str)                       checks for invalid charachter in str (can be modified to put additional chars)
// invalidChar(str,label)                       checks for invalid charachter in str and label of the field (can be modified to put additional chars)
// trim(inputString)                      trims inputstring from left and right
// isBlank (s)                             Returns true if string s is empty or  whitespace characters only.
// ValidateNum(objField,negFlag,minVal,maxVal) 
//                                             validates a number in objfield between minval and maxval,negFlage can be true/false     
// isInteger (s)                            whether string s is integer
// isStateCode (s [,eok])            True if string s is a valid U.S. Postal Code
// stripCharsInBag (s, bag)          Removes all characters in string bag from string s.
// getRadioButtonValue (radio)    Get checked value from radio button.
// isAlphanumeric (s [,eok])        True if string s is English letters and numbers only.
// isZIPCode (s [,eok])                True if string s is a valid U.S. ZIP code.
// isIndianZIPCode(s [,eok])           True if string s is a valid Indian ZIP code.
// isAlphabetic (s [,eok])             True if string s is English letters 
// ALERT FUNCTIONS
// prompt (s)                             Display prompt string s in status bar.
// promptEntry (s)                     Display data entry prompt string s in status bar.
// warnEmpty (theField, s)     The alert saying field s is to be entered is displayed, the focus return to theField 
//                                        the alert is of Prefix + s + Suffix format where Prefix and suffix are predefined in this file 
//warnInvalid (theField, s)      Notify user that contents of field theField are invalid.
//                                       String s describes expected contents of theField.value.
//                                       Put select theField, pu focus in it, and return false.


// VARIABLE DECLARATION

var defaultEmptyOK = true;
var mPrefix = "You did not enter a value into the ";
var mSuffix = " field. This is a required field. Please enter it now.";
var pEntryPrompt = "Please enter  ";
var reFloat = /^((\d+(\.\d*)?)|((\d*\.)?\d+))$/;
var reWhitespace = /^\s+$/;
var reInteger = /^\d+$/;
var sEmail = "Email"
//var reSignedInteger = /^(+|-)?\d+$/;

// BOI, followed by an optional + or -, followed by one or more digits, 
// followed by EOI.
//var reSignedInteger
//var reSignedInteger = /^(+|-)?\d+$/


// BOI, followed by one or more characters, followed by @,
// followed by one or more characters, followed by ., 
// followed by one or more characters, followed by EOI.
var reEmail = /^.+\@.+\..+$/;

// BOI, followed by one or more lower or uppercase English letters
// or digits, followed by EOI.
var reAlphanumeric = /^[a-zA-Z0-9]+$/;

// Valid U.S. Postal Codes for states, territories, armed forces, etc.
// See http://www.usps.gov/ncsc/lookups/abbr_state.txt.

var USStateCodeDelimiter = "|";
var USStateCodes = "AL|AK|AS|AZ|AR|CA|CO|CT|DE|DC|FM|FL|GA|GU|HI|ID|IL|IN|IA|KS|KY|LA|ME|MH|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|MP|OH|OK|OR|PW|PA|PR|RI|SC|SD|TN|TX|UT|VT|VI|VA|WA|WV|WI|WY|AE|AA|AE|AE|AP"
var digits = "0123456789";
//var reNumeric = /^-*[0-9]+.{0,1}[0-9]+$/;
var reNumeric = /^(\d|-)?(\d|,)*\.?\d*$/;


// BOI, followed by one lower or uppercase English letter, followed by EOI.
var reLetter = /^[a-zA-Z]$/;


// BOI, followed by one or more lower or uppercase English letters, 
// followed by EOI.
var reAlphabetic = /^[a-zA-Z]+$/;

var check=/^[A-Z]\d[A-Z](\s){1}\d[A-Z]\d$/
function checkCanadianZipcode(obj) {
//var check=/^(a-z\d){3}$/i
//var check=/^\s*[a-ceghj-npr-tvxy]\d[A-Z](\s){1}\d[A-Z]\d\s*$/i
//var check=/^[A-Z]\d[A-Z](\s){1}\d[A-Z]\d$/
//var check=^\d{5}-\d{4}|\d{5}|[A-Z]\d[A-Z][\s ]\d[A-Z]\d$;
//alert(obj.value);
 
 //alert(check.test(obj.value));
 return check.test(obj.value);
 
	
}







function ShowMsg(str)
{
	alert(str);
}


function ValidateDate(objField)
{
	var str;
	var strDate;
	var strDateArray;
	var strDay;
	var strMonth;
	var strYear;
	var intday;
	var intMonth;
	var intYear;
	var booFound = false;
	var datefield = objField;
	var strSeparatorArray = new Array("-"," ","/",".");
	var i;
	var Err = 0;

	strDate = datefield.value;

	if (strDate.length >= 1)
	{
		for (i = 0; i < strSeparatorArray.length; i++) 
		{
			if (strDate.indexOf(strSeparatorArray[i]) != -1) 
			{
				strDateArray = strDate.split(strSeparatorArray[i]);
				if (strDateArray.length != 3) 
				{
					Err = 1;
					str = "Invalid date size\r";
				}
				else 
				{
					strDay = strDateArray[0];
					strMonth = strDateArray[1];
					strYear = strDateArray[2];
                  if (strDay==null || strMonth==null || strYear==null  ){
                     str ="Enter date in dd/mm/yyyy format\r"
                     Err=1;
                   } 

                   if (Err == 0 &&( isNaN(strDay) || isNaN(strMonth) || isNaN(strYear) )) {
                     str ="Enter date in dd/mm/yyyy format\r"
                     Err=1;
                     } 

                   if (Err == 0 && (eval(strDay)<0 || eval(strMonth)<0 || eval(strYear) <0)) {
                     str ="Enter date in dd/mm/yyyy format\r"
                     Err=1;
                     } 



				}
				booFound = true;
			}
		}
		
		if (booFound == false) 
		{
			if (strDate.length > 5) 
			{
				strDay = strDate.substr(0, 2);
				strMonth = strDate.substr(2, 2);
				strYear = strDate.substr(4);
			}
			else
			{
				Err = 1;
				str = "Invalid date size\r";
			}
		}

		if (Err == 0)
		{

			if (strYear.length == 2) 
			{
				strYear = '20' + strYear;
			}
			else
			{
				if ( strYear.length > 4)
				{
					Err = 2;
					str = "Invalid Year\r";
				}
			}
		}			

		if (Err == 0)
		{
			
			intday = parseInt(strDay, 10);
			if (isNaN(intday)) 
			{
				Err = 3;
				str = "Invalid day\r";
			}
		}

		if (Err == 0)
		{
			intMonth = parseInt(strMonth, 10);
                                             
				if (isNaN(intMonth)) 
				{
					Err = 3;
					str = "Invalid Month\r";
				}
		}

		if (Err == 0)
		{
			intYear = parseInt(strYear, 10);

			if (isNaN(intYear)) 
			{
				Err = 4;
				str = "Invalid Year\r";
			}
			else
			{
				if (intYear < 1900 || intYear > 3000)
				{
					Err = 4;
					str = "Invalid Year: should be > 1900 or < 3000\r";
				}
			}
		}

		if (Err == 0)
		{
			if (intMonth>12 || intMonth<1) 
			{
				Err = 5;
				str = "Invalid Month\r";
			}
		}

		if (Err == 0)
		{
			if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1)) 
			{
				Err = 6;
				str = "Invalid number of days\r";
			}
		}
			
		if (Err == 0)
		{
			if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1)) 
			{
				Err = 7;
				str = "Invalid number of days\r";
			}
		}

		if (Err == 0)
		{
			if (intMonth == 2) 
			{
				if (intday < 1) 
				{
					Err = 8;
					str = "Invalid number of days\r";
				}
				
				if (LeapYear(intYear) == true) 
				{
					if (intday > 29) 
					{
						Err = 9;
						str = "Invalid number of days(cannot be greater than 29)\r";
					}
				}
				else 
				{
					if (intday > 28) 
					{
						Err = 10;
						str = "Invalid number of days(cannot be greater than 28)\r";
					}
				}
			}
		}
			

		if (Err == 0)
		{
			month = intMonth;
			if (intMonth < 10)
				month = "0" + intMonth;

			day = intday;
			if (intday < 10)
				day = "0" + intday;

			datefield.value = day + "/" + month + "/" + intYear;

		}
	}

	
	if (Err > 0)
	{
		var str1 = str;

		str = "";

		if (Err > 1)
		{
			if ( strMonth.length > 0)
				str = str + strDay;
			str = str + "/";

			if ( strDay.length > 0)
				str = str + strMonth;
			str = str + "/";

			if ( strYear.length > 0)
				str = str + strYear;
		}
		else
		{
			str = str + strDate;
		}

		datefield.value = str;
		str = "The Date [" + str + "]\r\r" + str1;

		alert(str);
                            return false;    
	}

       return true;
}

function LeapYear(intYear) 
{

	if (intYear % 100 == 0) 
	{
		if (intYear % 400 == 0) { return true; }
	}
	else 
	{
		if ((intYear % 4) == 0) { return true; }
	}
	return false;
}



function invalidChar(str)
{

  	if((str.indexOf("'")>=0)||(str.indexOf("\"")>=0))
	{
			
  		 alert('Invalid Character not allowed ');
   		return true;
  }
  for(ii=0;ii<=str.length;ii++){
   if (str.charCodeAt(ii)==34)
   {
    alert('Invalid Character not allowed');
    return true;
   }
  }
  return false;
}

function invalidChar(str,label)
{

  	if((str.indexOf("'")>=0)||(str.indexOf("\"")>=0))
	{
			
  		 alert('Invalid Character not allowed in '+label);
   		return true;
  	}
  /* for(ii=0;ii<=str.length;ii++){
   if (str.charCodeAt(ii)==34)
   {
    alert('Invalid Character not allowed');
    return true;
   }*/
  
  return false;
}


function trim(inputString){
var str = inputString;
for(var i=0;str.indexOf(" ")!=-1 && str.indexOf(" ") ==0 ;i++)
{
  str=str.substring(1);
}
for(var i=0;str.lastIndexOf(" ")!=-1 && str.lastIndexOf(" ")==str.length-1;i++)
{
  str=str.substring(0,str.lastIndexOf(" "));
}
 return str;
}

function blankAlert(fieldname){
 alert(fieldname+ ' needs to be entered');
}

// U.S. phone numbers have 10 digits.
// They are formatted as 123 456 7890 or (123) 456-7890.
var digitsInUSPhoneNumber = 10;

function emailCheck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		var temp=str.substring(lat,lstr)
		
		var temp1=temp.indexOf(dot)
		
		var temp2=temp.substring(temp1,temp1+2)
		
		if (str.indexOf(at)==-1){
		   alert("Invalid E-mail ID")
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Invalid E-mail ID")
		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("Invalid E-mail ID")
		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Invalid E-mail ID")
		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Invalid E-mail ID")
		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Invalid E-mail ID")
		    return false
		 }
		
		 if(temp2=='..')
	     {
			alert("Invalid EmailID. There must be a string between two .'s after '@'")
			return false
		 }
		 if (str.indexOf(" ")!=-1){
		    alert("Invalid E-mail ID")
		    return false
		 }
         
		 return true					
	}



function isUSPhoneNumber (s)
{   if (isEmpty(s)) 
       if (isUSPhoneNumber.arguments.length == 1) return defaultEmptyOK;
       else return (isUSPhoneNumber.arguments[1] == true);
    return (isInteger(s) && s.length == digitsInUSPhoneNumber)
}

// Check whether string s is empty.
function isEmpty(s)
{   return ((s == null) || (s.length == 0))
}

// Returns true if string s is empty or 
// whitespace characters only.
function isBlank (s)
{   // Is s empty?
   return (isEmpty(s) || reWhitespace.test(s));
}

function isNumeric(s) {
	return reNumeric.test(s);
}


function isInteger (s)

{   var i;

    if (isEmpty(s)) 
       if (isInteger.arguments.length == 1) return defaultEmptyOK;
       else return (isInteger.arguments[1] == true);

    return reInteger.test(s)
}

// FUNCTIONS TO PROMPT USER:
//
// prompt (s)                             Display prompt string s in status bar.
// promptEntry (s)                     Display data entry prompt string s in status bar.
// warnEmpty (theField, s)         Notify user that required field theField is empty.
// warnInvalid (theField, s)          Notify user that contents of field theField are invalid.


// FUNCTIONS TO INTERACTIVELY CHECK FIELD CONTENTS:
//
// checkString (theField, s [,eok])    Check that theField.value is not empty or all whitespace.




/* FUNCTIONS TO INTERACTIVELY CHECK VARIOUS FIELDS. */

// checkString (TEXTFIELD theField, STRING s, [, BOOLEAN emptyOK==false])
//
// Check that string theField.value is not all whitespace.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function checkString (theField, s, emptyOK)
{   // Next line is needed on NN3 to avoid "undefined is not a number" error
    // in equality comparison below.
    if (checkString.arguments.length == 2) 
    emptyOK = defaultEmptyOK;
    
   if ((emptyOK == true) && (isEmpty(theField.value))) 
      return true;
      
    if (isWhitespace(theField.value)) 
       return warnEmpty (theField, s);
    else 
    return true;
}



/* FUNCTIONS TO NOTIFY USER OF INPUT REQUIREMENTS OR MISTAKES. */


// Display prompt string s in status bar.
function prompt (s)
{   window.status = s
}

// Display data entry prompt string s in status bar.
function promptEntry (s)
{   window.status = pEntryPrompt + s
}


// Notify user that required field theField is empty.
// String s describes expected contents of theField.value.
// Put focus in theField and return false.
function warnEmpty (theField, s)
{	

	alert(mPrefix + s + mSuffix);
	theField.focus();
	return false;
}

function warningEmpty (theField,s, errormsg)
{	

	alert(errormsg+" "+s);
	theField.focus();
	return false;
}
// Notify user that contents of field theField are invalid.
// String s describes expected contents of theField.value.
// Put select theField, pu focus in it, and return false.

function warnInvalid (theField, s)
{	alert(s);
	theField.focus();
	//theField.select();
	return false;
}



// Check whether string s is empty.
function isEmpty(s)
{   return ((s == null) || (s.length == 0))
}


// Returns true if string s is empty or 
// whitespace characters only.
function isWhitespace (s)
{   
 //alert('inwhitespace '+reWhitespace.exec(s));
 
    if (s.indexOf(' ')!=-1){
		   return true;
		//alert(reWhitespace.test(s));
		//return (isEmpty(s) || reWhitespace.test(s));
	}
	return false;
}


// isFloat (STRING s [, BOOLEAN emptyOK])
// 
// True if string s is an unsigned floating point (real) number. 
//
// Also returns true for unsigned integers. If you wish
// to distinguish between integers and floating point numbers,
// first call isInteger, then call isFloat.
//
// Does not accept exponential notation.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function isFloat (s)

{   if (isEmpty(s)) 
       if (isFloat.arguments.length == 1) return defaultEmptyOK;
       else return (isFloat.arguments[1] == true);

    return reFloat.test(s)
}



function ValidateNum(objField,negFlag,minVal,maxVal)
{
	var str;

	if (isNaN(objField.value)) 
	{ 
		str = "Invalid Number format\r";
	}
	else
	{
		if (negFlag == false && parseInt(objField.value, 10) < 0)
			str = "Value cannot be Negative\r";
		else
		{
			if (parseInt(objField.value, 10) < minVal)
				str = "Value cannot be less than " + minVal + "\r";
			else
			{
				if (parseInt(objField.value, 10) > maxVal)
					str = "Value cannot be greater than " + maxVal + "\r";
			}			
		}			
	}
	 
	if (str) 
	{
		alert(str);
                            return false;
             }
    return true;            
}



/*function isValidStringLength(s, maxVal)
{
	var str;
	var length;
	length=s.length;
	
	if(length > maxVal)
	{
		return false;
	}
	else
	{
		return true;
	}
    str = "Value length " + length + "\r"; 
    
    return false;           
}*/



// isEmail (STRING s [, BOOLEAN emptyOK])
// 
// Email address must be of form a@b.c -- in other words:
// * there must be at least one character before the @
// * there must be at least one character before and after the .
// * the characters @ and . are both required
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function isEmail(s)
{  
 if (isEmpty(s)) 
       if (isEmail.arguments.length == 1) return defaultEmptyOK;
       else return (isEmail.arguments[1] == true);
    
    else 
       return reEmail.test(s)
  
}

// isStateCode (STRING s [, BOOLEAN emptyOK])
// 
// Return true if s is a valid U.S. Postal Code 
// (abbreviation for state).
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function isStateCode(s)
{   if (isEmpty(s)) 
       if (isStateCode.arguments.length == 1) return defaultEmptyOK;
       else return (isStateCode.arguments[1] == true);
    return ( (USStateCodes.indexOf(s) != -1) &&
             (s.indexOf(USStateCodeDelimiter) == -1) )
}

// Removes all characters which appear in regexp bag from string s.
// NOTES:
// 1) bag must be a regexp which matches single characters in isolation,
//    i.e. A or B or C or D or 1 or 2 ...
//    e.g. /\d/g  or /[a-zA-Z]/g
// 2) make sure to append the 'g' modifier (for global search & replace)
//    at the end of the regexp
//    e.g. /\d/g  or /[a-zA-Z]/g
//example :stripCharsInBag(form1.t1.value, '/[#!%]/g');

// Removes all characters which appear in string bag from string s.

function stripCharsInBag (s, bag)

{   var i;
    var returnString = "";

    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.

    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

// Get checked value from radio button.

function getRadioButtonValue (radio)
{   
  for (var i = 0; i < radio.length; i++)
    {
    
      if (radio[i].checked) 
        { break }
    }
    return radio[i].value
}


function isRadioButtonChecked (radio)
{   
  var flag = false;
  if( !(radio.length >=1))
  {
     if (radio.checked) 
         flag = true  ;
  }
  for (var i = 0; i < radio.length; i++)
    {
      if (radio[i].checked) 
        { flag = true  ;
          break ;
       }
    }
    return flag;
}



// isAlphanumeric (STRING s [, BOOLEAN emptyOK])
// 
// Returns true if string s is English letters 
// (A .. Z, a..z) and numbers only.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.
//
// NOTE: Need i18n version to support European characters.
// This could be tricky due to different character
// sets and orderings for various languages and platforms.

function isAlphanumeric (s)

{   var i;

    if (isEmpty(s)) 
       if (isAlphanumeric.arguments.length == 1) return defaultEmptyOK;
       else return (isAlphanumeric.arguments[1] == true);

    else {
       return reAlphanumeric.test(s)
    }
}


// U.S. phone numbers have 10 digits.
// They are formatted as 123 456 7890 or (123) 456-7890.
var digitsInUSPhoneNumber = 10;


// non-digit characters which are allowed in ZIP Codes
var ZIPCodeDelimiters = "-";


// our preferred delimiter for reformatting ZIP Codes
var ZIPCodeDelimeter = "-"


// characters which are allowed in Social Security Numbers
var validZIPCodeChars = digits + ZIPCodeDelimiters


// U.S. ZIP codes have 5 or 9 digits.
// They are formatted as 12345 or 12345-6789.
var digitsInZIPCode1 = 5
var digitsInZIPCode2 = 9
var digitsInZIPCode3 = 6



// isZIPCode (STRING s [, BOOLEAN emptyOK])
// 
// isZIPCode returns true if string s is a valid 
// U.S. ZIP code.  Must be 5 or 9 digits only.
//
// NOTE: Strip out any delimiters (spaces, hyphens, etc.)
// from string s before calling this function.  
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function isZIPCode (s)
{  if (isEmpty(s)) 
       if (isZIPCode.arguments.length == 1) return defaultEmptyOK;
       else return (isZIPCode.arguments[1] == true);
   return (isInteger(s) && 
            ((s.length == digitsInZIPCode1) ||
             (s.length == digitsInZIPCode2)))
}

// isIndianZIPCode (STRING s [, BOOLEAN emptyOK])
// 
// isIndianZIPCode returns true if string s is a valid 
// INDIAN ZIP code.  Must be 6 digits only.
//
// NOTE: Strip out any delimiters (spaces, hyphens, etc.)
// from string s before calling this function.  
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.

function isIndianZIPCode (s)
{  if (isEmpty(s)) 
       if (isIndianZIPCode.arguments.length == 1) return defaultEmptyOK;
       else return (isIndianZIPCode.arguments[1] == true);
	     return (isInteger(s) && 
            (s.length == digitsInZIPCode3))
}


// isAlphabetic (STRING s [, BOOLEAN emptyOK])
// 
// Returns true if string s is English letters 
// (A .. Z, a..z) only.
//
// For explanation of optional argument emptyOK,
// see comments of function isInteger.
//
// NOTE: Need i18n version to support European characters.
// This could be tricky due to different character
// sets and orderings for various languages and platforms.

function isAlphabetic (s)
{   var i;

    if (isEmpty(s)) 
       if (isAlphabetic.arguments.length == 1) return defaultEmptyOK;
       else return (isAlphabetic.arguments[1] == true);

    else {
       return reAlphabetic.test(s)
    }
}
function trimLeadTrail(str)
{
var len = str.length;
var c=0;
 for (i=len-1;i>=0;i--)
 {
   if (str.substring(i,i+1)==" ")
   {
   c=c+1;
   }
   else
   {
   break;
   }
 } 
str = str.substring(0,len-c)

len = str.length
c=0;
 for (i=0;i<len;i++)
 {
   if (str.substring(i,i+1)==" ")
   {
   c=c+1;
   }
   else
   {
   break;
   }
 } 
str = str.substring(c,len)
return str;
}

function helpPop( jspName)
{
window.open(jspName,"", "height=400, width=400,scrollbars=yes,toolbar=false,status=false");
}

function ValidateNumber(objField, negFlag, gtZeroFlag)
{
	var str = null;
    objField.value = trimLeadTrail(objField.value);
    if (isEmpty(objField.value))
    {
       objField.value = "0.0";
       return true;
    }
	if (isNaN(objField.value)) 
	{ 
		str = "Invalid Number entered\r";
	}
	else
	{
		if (negFlag == false && parseFloat(objField.value) < 0)
			str = "Value cannot be Negative\r";
		else
		{
			if (gtZeroFlag != false && parseFloat(objField.value) == 0)
				str = "Value must be greater than zero\r";
		}			
	}
	 
	if (str != null) 
	{
		alert(str);
        return false;
    }
    return true;            
}
function ValidateInteger(objField, negFlag, gtZeroFlag)
{
	var str = null;

    objField.value = trimLeadTrail(objField.value);
    if (isEmpty(objField.value))
    {
       objField.value = "0";
    } else
	if (isNaN(objField.value)) 
	{ 
		str = "Invalid integer entered\r";
	} else
    if (parseInt(objField.value) != parseFloat(objField.value))
    {
		str = "Enter without decimals\r";
    }
	else
	{
		if (negFlag == false && parseInt(objField.value) < 0)
			str = "Value cannot be Negative\r";
		else
		{
			if (gtZeroFlag != false && parseInt(objField.value) == 0)
				str = "Value must be greater than zero\r";
		}			
	}
	 
	if (str != null) 
	{
		alert(str);
        return false;
    }
    return true;            
}
function makeProperHeaderMenu(strValue)
{
	parent.headerinfo.MakeProperMenu(strValue);
}
function mainURL(strValue)
{
    parent.main.location.href=strValue;
}

// functions for history page

function prevPage(pPage)

{

  document.forms[0].pageNum.value=eval(pPage)+eval('-1');

  document.forms[0].buttonVal.value='next';

  //document.forms[0].addPrevFunc();
 addPrevFunc();

  document.forms[0].submit();

}


// functions for history page
function NextPage(pPage)

{

document.forms[0].pageNum.value=eval(pPage)+eval('1');

document.forms[0].buttonVal.value='previous';

addNextFunc();

document.forms[0].submit();

}
// functions for history page

function gotoPage(pPage)

{

document.forms[0].pageNum.value=eval(pPage);

addGotoFunc();

document.forms[0].submit();

}


// functions for history page

function sortColumn(psortFieldMethod,pSortOrder)

{
document.forms[0].sortFieldMethod.value=psortFieldMethod;
document.forms[0].sortOrder.value=pSortOrder;

document.forms[0].submit();

}
// by udai for date in mm/dd/yyyy format to Oracle date in dd-mon-yyyy format
  function convertStringToOracleDate(pdate)  // String in format 'mm/dd/yyyy'
   {
      l_sOracleDate = null;
         l_sDay = pdate.substring(3,5);
         l_sMth = pdate.substring(0,2);
         l_sYear = pdate.substring(6);
         m_iMth = parseInt(l_sMth);
         l_sOracleDate = l_sDay;
         switch (m_iMth)
         {
            case 1:
            l_sOracleDate += "-jan-";
            break;
            case 2:
            l_sOracleDate += "-feb-";
            break;
            case 3:
            l_sOracleDate += "-mar-";
            break;
            case 4:
            l_sOracleDate += "-apr-";
            break;
            case 5:
            l_sOracleDate += "-may-";
            break;
            case 6:
            l_sOracleDate += "-jun-";
            break;
            case 7:
            l_sOracleDate += "-jul-";
            break;
            case 8:
            l_sOracleDate += "-aug-";
            break;
            case 9:
            l_sOracleDate += "-sep-";
            break;
            case 10:
            l_sOracleDate += "-oct-";
            break;
            case 11:
            l_sOracleDate += "-nov-";
            break;
            case 12:
            l_sOracleDate += "-dec-";
            break;
         }
           l_sOracleDate += l_sYear;
           return "'"+l_sOracleDate+"'";
   }       


  function checkIllegalChar(obj)
    {
    object =  obj.split("");
 		for(i=0;i<object.length;++i)
  	{
		 	if(object[i]=='\\' || object[i]=='<' || object[i]=='>' || object[i]=='#' ||  object[i]=='"' || object[i]==';' || object[i]=='=' || object[i]=='&')
		 	{
		 	alert('Invalid Characters not allowed\n  \\ < > # & " ; = ');
		 	return false;
		 	}
  	}
return true;
  }

//for setting element help

function fillElemHelp(elemStr,document){

    elemStr=elemStr.substring(elemStr.indexOf("|")+1);
    while(elemStr.length>0){
    elemName=elemStr.substring(0,elemStr.indexOf("|"));
    elemStr=elemStr.substring(elemStr.indexOf("|")+1);
    if(elemStr.indexOf("|")!=-1){
    elemText=elemStr.substring(0,elemStr.indexOf("|"));
    elemStr=elemStr.substring(elemStr.indexOf("|")+1);
      }
    else {
    elemText=elemStr;
    elemStr="";
    }


    if(document.all.item(elemName)!=null){
    document.all.item(elemName).title=elemText;
      }
    
    }
}



//SRFQ


function checkDate(datestr) {
		if (datestr.match(/^\d{2}\/\d{2}\/\d{4}$/) == null) {
			return false;
		}
		var vDay   = datestr.substr(0, 2) - 0;
		var vMonth = datestr.substr(3, 2) - 1; 
		var vYear  = datestr.substr(6, 4) - 0;

		if ( isNaN(vDay) )   return false;
		if ( isNaN(vMonth) ) return false;
		if ( isNaN(vYear) )  return false;

		if (vMonth >= 0 && vMonth <= 11 && vDay >= 1 && vDay <= 31) {
			var vDt = new Date(vYear, vMonth, vDay);

			if (isNaN(vDt) == true) {
				return false;
			} else if (vDt.getFullYear() == vYear && vDt.getMonth() == vMonth && vDt.getDate() == vDay){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
}

function checkTime(txt) {
   if (txt == "") return true;
   tmp = new Date();
   p1 = txt.indexOf(":");
   if (p1<0) return false;
   hstr = txt.substring(0, p1);
   mstr = txt.substring(p1+1);
   tmp.setHours(parseInt(hstr, 10));
   tmp.setMinutes(parseInt(mstr, 10));
   if (tmp.getHours() != hstr) return false;
   if (tmp.getMinutes() != mstr) return false;
   return true;
}


function compDate_dmy(from, to){
    tmp1 = new Date();
    tmp2 = new Date();

    p1 = from.indexOf("/");
    dstr = from.substring(0, p1);
    p2 = from.indexOf("/", p1+1);
    mstr = from.substring(p1+1, p2);
    ystr = from.substring(p2+1);
    tmp1.setYear(parseInt(ystr, 10));
    tmp1.setMonth(parseInt(mstr, 10) - 1);
    tmp1.setDate(parseInt(dstr, 10));

    p1 = to.indexOf("/");
    dstr = to.substring(0, p1);
    p2 = to.indexOf("/", p1+1);
    mstr = to.substring(p1+1, p2);
    ystr = to.substring(p2+1);
    tmp2.setYear(parseInt(ystr, 10));
    tmp2.setMonth(parseInt(mstr, 10) - 1);
    tmp2.setDate(parseInt(dstr, 10));

    if (tmp1 <= tmp2) {
        return true;
    } else {
        return false;
    }
}


function compDate_mdy(from, to){
    tmp1 = new Date();
    tmp2 = new Date();

    p1 = from.indexOf("/");
    mstr = from.substring(0, p1);
    p2 = from.indexOf("/", p1+1);
    dstr = from.substring(p1+1, p2);
    ystr = from.substring(p2+1);
    tmp1.setYear(parseInt(ystr, 10));
    tmp1.setMonth(parseInt(mstr, 10) - 1);
    tmp1.setDate(parseInt(dstr, 10));

    p1 = to.indexOf("/");
    mstr = to.substring(0, p1);
    p2 = to.indexOf("/", p1+1);
    dstr = to.substring(p1+1, p2);
    ystr = to.substring(p2+1);
    tmp2.setYear(parseInt(ystr, 10));
    tmp2.setMonth(parseInt(mstr, 10) - 1);
    tmp2.setDate(parseInt(dstr, 10));

    if (tmp1 <= tmp2) {
        return true;
    } else {
        return false;
    }
}


function checkMailId(str){
	if( (str.indexOf("@")==-1) || (str.indexOf("@")==0) ){	
		return false;
	}else{
		var n = str.indexOf("@",0);
		var str2 = str.substring(n+1)

		if(str2.indexOf("@",0)>=0){
			return false;
		}
	} 
	if(str.indexOf(".")==-1){
		return false;
	}else{
		var m = str.indexOf(".",0);
		var str3 = str.substring(m+1);
		if((str3.indexOf(".",0)==0)||(str3.indexOf("@",0)==0)){
		    return false;
		}
		if(str3==""){
			return false;
		}
 	}
	return true;
}

function checkNumber(number,precision,scale) {
    var num1 = number;
    if(isNaN(num1))  return true;
    if(number.indexOf("-")==0){
        num1 = number.substring(1,number.length);
    }
    p1=num1.indexOf(".");
    if(p1==-1){
        if(num1.length>(precision-scale)) return true;
    } else {
        num2 = num1.substring(0,p1);
        if(num2.length>(precision-scale)) return true;
        num3 = num1.substring(p1+1,num1.length);
        if(num3.length>scale) return true;
    }
    return false;
}

function checkInteger(num){
    if ( (num % 1) != 0 ){
        return true;
    }
    return false;
}

function openWin(p, width, height, name) {
        //newwin = window.open("", name, "outerWidth=0,outerHeight=0");
        //newwin.close();
        newwin = window.open(p, name, "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,outerWidth=" + width + ",outerHeight=" + height);
        x = (screen.width - width) / 2;
        y = (screen.height - height) / 2;
        newwin.moveTo(x, y);
}

function openWin1(p, width, height, name) {
      
	newwin = window.open(p, name, "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=" + width + ",height=" + height);
      
        x = (screen.width - width) / 4;
        y = (screen.height - height) / 2;
        newwin.moveTo(x, y);
}

function checkAll2(fm,cbx1,cbx2){
	if(cbx1.checked){
		if(isNaN(cbx2.length)){
			cbx2.checked = true ;		
		}else{
			for (i = 0; i < cbx2.length; i++){
				cbx2[i].checked = true ;
			}
		}
	}else{
		if(isNaN(cbx2.length)){
			cbx2.checked = false ;		
		}else{
			for (i = 0; i < cbx2.length; i++){
				cbx2[i].checked = false ;
			}
		}
	}
}

//------------------------------------------------- Added by NK
// check all fm
function allCheck(fm)
{
for (i = 0; i < fm.length; i++)
	fm[i].checked = true ;
}
// check uncheck  fm is selected
function uncheckAll(fm)
{
for (i = 0; i < fm.length; i++)
	fm[i].checked = false ;
}
// check if any fm is for check box
function checkAll(fm,str)
{
	if(str.checked)
	{
		
		for (i = 0; i < fm.length; i++)
			if (fm[i].type == 'checkbox')
		{
		fm[i].checked = true ;
		}
	}
	else
	{
for (i = 0; i < fm.length; i++)		
	fm[i].checked = false ;
	}
}
// to check un check all field
function CheckUncheckAll(fm,str)
{
	if(str.checked==true)
	{  
		for (i = 0; i < fm.length; i++)
			if (fm[i].type == 'checkbox')
		{
		fm[i].checked = true ;
		}
	}
	else
	{
for (i = 0; i < fm.length; i++)		
	fm[i].checked = false ;
	}
}
// to to see if any feild is selected
function check(fm,msg)
{


for (i = 0; i < fm.length; i++)
	{
	if (fm[i].type == 'checkbox')
		{
			if (fm[i].checked == true)
			return true;
		}
	}
alert(msg);
return false; 
}

// to to see if any field is selected
function checkSingle(fm,msg1,msg2)
{
var cnt=0;
for (i = 0; i < fm.length; i++)
 {
 if (fm[i].type == 'checkbox')
  {
   if (fm[i].checked == true)
   cnt++;
  }
 }
if (cnt>0)
{
 if(cnt==1)
  return true;
 else
  alert(msg2);
 return false; 
}
else
 alert(msg1);
return false; 
}

function selectField(value,name,msg)
{

if(value=='')
	{
		alert(msg);
		name.focus();
		return false;
	}//end of if
			else
			{
			return true;
			}

}//end of function
// to check blank fm
function trimLeadTrail(str)
{
var len = str.length;
var c=0;
 for (i=len-1;i>=0;i--)
 {
   if (str.substring(i,i+1)==" ")
   {
   c=c+1;
   }
   else
   {
   break;
   }
 } 
str = str.substring(0,len-c)

len = str.length
c=0;
 for (i=0;i<len;i++)
 {
   if (str.substring(i,i+1)==" ")
   {
   c=c+1;
   }
   else
   {
   break;
   }
 } 
str = str.substring(c,len)
return str;
}
function blank_fields(str,str1,msg)
{
	var version=str;
	if(version=='')
	{
		alert(msg);
		str1.focus();
		return false;
	}//end of if
			else
			{
			return true;
			}

}//end of function
// to check special character
function check_spl_character(str,str1,msg)
{


var valid = "???????abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-  "
var ok = "yes";
var temp;
for (var i=0; i<str.length; i++) {
temp = "" + str.substring(i, i+1);
if (valid.indexOf(temp) == "-1") ok = "no";
}
if (ok == "no") {
alert(msg);
str1.focus();
str1.select();
	return false;

   }
   else
	return true;
}


// to check special character
function check_Phone(str,str1,msg)
{

var valid = "0123456789- "
var ok = "yes";
var temp;
for (var i=0; i<str.length; i++) {
temp = "" + str.substring(i, i+1);
if (valid.indexOf(temp) == "-1") ok = "no";
}
if (ok == "no") {
alert(msg);
str1.focus();
str1.select();
	return false;

   }
   else
	return true;
}
function trimFields() 
{
	var field = document.forms[0];
	
	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='text') 
		{ 
			var temp1=field[i].value;
         
var temp2="";
var flag=0;
if(temp1.indexOf(" ",temp1.length-1)>=0) {
for(var j=temp1.length; j>0; j--){
if((temp1.substring(j-1,j)==" ")&&(flag==0)){
temp2=temp1.substring(0,j-1);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

temp1=temp2;
temp2="";
flag=0;
if(temp1.indexOf(" ")==0) { 
for(var j=0; j<temp1.length; j++){
if((temp1.substring(j,j+1)==" ")&&(flag==0)){
temp2=temp1.substring(j+1,temp1.length);
} else {flag=1; break;}
}//for
} else { temp2=temp1; }

field[i].value=temp2;
		}
	}
return true;
}

function trimTextAreaFields() 
{
	var field = document.forms[0];
	
	for(var i=0; i<field.length; i++)
	{
		if(field[i].type=='textarea') 
		{ 
			field[i].value=field[i].value.replace(/\s+$/,"")                //remove trailing white space 
			field[i].value=field[i].value.replace(/^\s+/,"")                //remove leading white space
			field[i].value=field[i].value.replace(/\t/g," ")               	//change tabs to spaces 
			var temp1=field[i].value;
         }
     }
}         

function setBlankFocus(obj){
	document.forms[0].obj.value =="";
	document.forms[0].obj.focus();
}
