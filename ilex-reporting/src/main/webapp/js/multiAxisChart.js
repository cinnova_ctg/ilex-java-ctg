function getFrameSize(frameID) {
    var result = {height:0, width:0};
    if (document.getElementById) {
        var frame = parent.document.getElementById(frameID);
        if (frame.scrollWidth) {
            result.height = frame.scrollHeight;
            result.width = frame.scrollWidth;
        }
    }
    return result;
}

function multiAxisChartRender() {
	var widthDiff = 220;
	var frameSize = getFrameSize("footerFrame");
	if(frameSize.width > 884)
	{
		widthDiff = 284;
	}
	if(chartWidth != (frameSize.width - widthDiff))
	{
		chartWidth=frameSize.width - widthDiff;
		renderChart(chartWidth);
	}
}

function renderMultiAxisChart() {
	var chart3 = new FusionCharts(contextPath + "/fc/MultiAxisLine.swf", "chart3Id", "950", "400", "0", "1");
//	alert(graphXML);
	chart3.setDataXML(graphXML);
     chart3.render("chartMultiAxisdiv");
}
function renderStrakedChart() {
	var chart3 = new FusionCharts(contextPath + "/fc/StackedColumn2D.swf", "chart3Id", chartWidth, "400", "0", "1");
	//var graphXML="<graph bgColor='F1f1f1' showSum='1' formatNumberScale='0' caption='Minuteman Speedpay Usage -Total Cost' numberPrefix='$'  xaxisname='Trailing 12 Months'useRoundEdges='1'  animation='1'numdivlines='3' divLineColor='333333' decimalPrecision='0'  showColumnShadow='1' yAxisMaxValue='100000'><categories font='Arial' fontSize='10' fontColor='000000'><category label='08/09' hoverText=''/><category label='09/09' hoverText=''/><category label='10/09' /><category label='11/09' /><category label='12/09' /><category label='01/10' hoverText=''/><category label='02/10' hoverText=''/><category label='03/10' /><category label='04/10' /><category label='05/10' /><category label='06/10' /><category label='07/10' /><category label='08/10' /><category label='09/10' /></categories><dataset seriesname='Minuteman' color='AFD8F8' showValues='0' alpha='100'><set value='17858' /><set value='24999' /><set value='26045' /><set value='24184' /><set value='24418' /><set value='17453' /><set value='24999' /><set value='26045' /><set value='24184' /><set value='24418' /><set value='24184' /><set value='24418' /></dataset><dataset seriesname='SpeedPay' color='F6BD0F' showValues='0' alpha='100'><set value='41345' /><set value='17858' /><set value='24999' /><set value='26045' /><set value='24184' /><set value='24418' /><set value='17453' /><set value='24999' /><set value='26045' /><set value='24184' /><set value='24418' /><set value='24184' /><set value='24418' /></dataset><dataset seriesname='PVS' color='8BBA00' showValues='0' alpha='100'><set value='17858' /><set value='24999' /><set value='26045' /><set value='24184' /><set value='24418' /><set value='17453' /><set value='24999' /><set value='26045' /><set value='24184' /><set value='24418' /><set value='24184' /><set value='24418' /></dataset></graph>";
	//alert(graphXML);
	chart3.setDataXML(graphXML);
     chart3.render("chartMultiAxisdiv");
}
function renderAxisChartForTicketCount() {
	var myChart = new FusionCharts(contextPath + "/fc/MSColumn2D.swf", "myChartId", "3000px", "600", "0", "0");
    myChart.setDataXML(graphXML); 
  	myChart.render("chartdiv");
}