/*JS files*/
/*Refer http://rafael.adm.br/css_browser_selector/ for browser codes used by below js file*/
document.write('<script type="text/javascript" src="'+contextPath+'/js/css_browser_selector.js"></script>');
document.write('<script type="text/javascript" src="'+contextPath+'/js/jquery-1.3.2.min.js"></script>');
document.write('<script type="text/javascript" src="'+contextPath+'/js/jquery-ui-1.7.2.custom.min.js"></script>');
document.write('<script type="text/javascript" src="'+contextPath+'/js/treeview.js"></script>');
document.write('<script type="text/javascript" src="'+contextPath+'/js/navigation.js"></script>');
document.write('<script type="text/javascript" src="'+contextPath+'/js/jquery.cookie.js"></script>');
document.write('<script type="text/javascript" src="'+contextPath+'/js/jquery.dropdownPlain.js"></script>');
document.write('<script type="text/javascript" src ="'+contextPath+'/js/jquery.blockUI.js"></script>');

/*CSS files*/
document.write('<link type="text/css" href="'+contextPath+'/css/smoothness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />');
document.write('<link href="'+contextPath+'/css/style.css" rel="stylesheet" type="text/css" />');
document.write('<link href="'+contextPath+'/css/treeview.css" rel="stylesheet" type="text/css" />');
document.write('<link href="'+contextPath+'/css/dropdownStyle.css" rel="stylesheet" type="text/css" />');

