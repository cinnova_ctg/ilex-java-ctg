$(document).ready(function(){
		
	$("#accordion").accordion({ header: "h3",  autoHeight: false, collapsible: true });
	$("#accordion div ul li ul.treeNode").each(function(i){
		var cookieId = "ilexReportingTree_"+i;
		jQuery(this).treeview({
			collapsed: true,
			unique: false,
			persist: "cookie",
			cookieId: cookieId
		});
	});
});