function activateDDMenu(){

    $("ul.dropdown li").hover(function(){
        $(this).addClass("hover");
        $(this).children('ul').children('li').addClass("hoverOfParent");
        $('ul:first',this).css('visibility', 'visible');
        $('a', this).children('img').attr('src',contextPath+"/images/white-arrow.gif");
    
    }, function(){
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
        $(this).children('ul').children('li').removeClass("hoverOfParent");
        $('a', this).children('img').attr('src',contextPath+"/images/white-arrow.gif");
    
    });
    
    $("ul.dropdown li ul li").hover(function(){
        $(this).parent().parent().children('a').children('img').attr('src',contextPath+"/images/white-arrow.gif");
    
    }, function(){
    	$(this).parent().parent().children('a').children('img').attr('src',contextPath+"/images/white-arrow.gif");
    
    });
    
    $("ul.dropdown li ul li:has(ul)").find("a:first").append(" &raquo; ");

}