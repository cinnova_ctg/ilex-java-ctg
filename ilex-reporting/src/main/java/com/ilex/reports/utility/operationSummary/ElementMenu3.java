package com.ilex.reports.utility.operationSummary;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/*************************************************************************/

	/* Title: 
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 */

public class ElementMenu3 extends Element{

	ElementMenu3 (Map init, HttpServletRequest request) {

		super (init, request);
		createElement();

	}

/*************************************************************************/
	
	/* Title: 
	 * 
	 * Description:
	 * 
	 *  
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 *  
	 */

	void createElement () {
	 
     String[] values = data.split("~");
     String selected = "";
     String use_value = "";

     if (currentValue != null) {
    	 use_value = currentValue;
     } 

     elementHTML = "\t<option value=\"\"> -- Select Option -- </option>\n";
     if (use_value.equals("All")) {
    	 selected = " SELECTED";
     }

     elementHTML = elementHTML.concat("\t<option value=\"All\""+selected+">All</option>\n");
     elementHTML = elementHTML.concat("\t<option value=\"Active\""+selected+">Active</option>\n");
     
     selected = "";
     
     String cr = "\n";
     for (int i=0; i<values.length; i++)
     {
    	if (i == values.length-1) {
    		cr = "";
    	}
    	if (use_value.equals(values[i])) {
    		selected = " SELECTED";
    	}
    	elementHTML = elementHTML.concat("\t<option value=\""+values[i]+"\""+selected+">"+values[i]+"</option>"+cr);
  	    selected = "";
     }
      
     elementHTML = "<select name=\""+name+"\" size=\"1\" class=\"menu\" "+javascript+">\n"+elementHTML+"</select>";
     
	 
	}

}

