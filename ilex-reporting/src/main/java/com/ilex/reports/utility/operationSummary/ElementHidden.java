package com.ilex.reports.utility.operationSummary;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/*************************************************************************/

	/* Title: 
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 */

public class ElementHidden extends Element{

	ElementHidden (Map init, HttpServletRequest request) {

		super (init, request);
		createElement();

	}

/*************************************************************************/
	
	/* Title: 
	 * 
	 * Description:
	 * 
	 *  
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 *  
	 */

	void createElement () {
	 
     String use_value = "";
     
     if (currentValue != null) {
     	use_value = currentValue;
     } 
      
     elementHTML = "<input type=\"hidden\" name=\""+name+"\"  value=\""+use_value+"\">";
    
	}

}

