package com.ilex.reports.utility.operationSummary;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/*************************************************************************/

	/* Title: 
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 */

public class ElementMenu2 extends Element{

	ElementMenu2 (Map init, HttpServletRequest request) {

		super (init, request);
		createElement();

	}

/*************************************************************************/
	
	/* Title: 
	 * 
	 * Description:
	 * 
	 *  
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 *  
	 */

	void createElement () {
	 
     String[] values = data.split("~");
     String selected = "";
     String use_value = "";
     String[] option_date;
     
     if (currentValue != null) {
     	use_value = currentValue;
     } 

     elementHTML = "\t<option value=\"\"> -- Select Option -- </option>\n";
     String cr = "\n";
     
     for (int i=0; i<values.length; i++)
     {
    	if (i == values.length-1) {
    		cr = "";
    	}
    	
    	option_date = values[i].split("\\|");
    	
    	if (use_value.equals(option_date[0])) {
    		selected = " SELECTED";
    	}
    	elementHTML = elementHTML.concat("\t<option value=\""+option_date[0]+"\""+selected+">"+option_date[1]+"</option>"+cr);
  	    selected = "";
     }
      
     elementHTML = "<select name=\""+name+"\" size=\"1\" class=\"menu\" "+javascript+">\n"+elementHTML+"</select>";
     
	 
	}

}

