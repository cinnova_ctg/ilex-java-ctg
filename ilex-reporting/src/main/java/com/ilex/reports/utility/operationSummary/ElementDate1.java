package com.ilex.reports.utility.operationSummary;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/*************************************************************************/

/*
 * Title:
 * 
 * Description:
 * 
 * 
 * Inputs:
 * 
 * Returns: Not Applicable
 */

public class ElementDate1 extends Element {

	ElementDate1(Map init, HttpServletRequest request) {

		super(init, request);
		createElement(request);

	}

	/*************************************************************************/

	/*
	 * Title:
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 */

	void createElement(HttpServletRequest request) {

		String image = "";
		String use_value = "";

		if (currentValue != null) {
			use_value = currentValue;
		} else if (use_value.equals("Today")) {
			Date timeDate = new Date(System.currentTimeMillis());
			DateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
			use_value = fmt.format(timeDate);
		}
		image = "&nbsp;<img src=\""
				+ request.getContextPath()
				+ "/images/calendar1.jpg\" width=\"14\" height=\"14\" alt=\"\" border=\"0\" onclick = \"return popUpCalendar(document.forms[0]."
				+ name
				+ ", document.forms[0]."
				+ name
				+ ", 'mm/dd/yyyy')\" onmouseover = \"window.status = 'Date Picker';return true;\" onmouseout = \"window.status = '';return true;\">";

		elementHTML = "<input name=\"" + name
				+ "\" class=\"date1\" type=\"text\" size=\"9\" value=\""
				+ use_value + "\" readonly=\"readonly\">" + image;
	}
}
