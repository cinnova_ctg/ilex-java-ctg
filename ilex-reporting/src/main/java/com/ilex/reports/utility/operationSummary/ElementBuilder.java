package com.ilex.reports.utility.operationSummary;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class ElementBuilder  extends TagSupport {

	String type;
	String name;
	String source;
	String data;
	String javascript;
	String checkbox;
	Map elementData = new HashMap();
	
	HttpServletRequest request;
	
	/*****************************************************************************/	  
	  /* Title: doStartTag
	   * 
	   * Description: Control is passed here for tag processing.
	   *  
	   * Inputs: Per tag
	   * 
	   * Returns:
	   * 
	   *  true/false 
	   *  
	   * Version: 1.0
	   *   Version History
	   *   1.0 Initial release (1/8/07)  
	   */
		   
	  public int doStartTag() throws JspException
	  {
		  Element e = new Element ();
		  
		  try 
		  { 
			  Element el = e.ElementFactory(elementData, request);
			  pageContext.getOut().print(el.elementHTML);
		  }
		  catch (Exception ioException)
		  {
			  System.out.println("generateFieldElement Error: "+ioException);
		  } 
		  
		  return 0;
	  }

	  /*****************************************************************************/

	  /* Title: Tag Set Functions
	   * 
	   * Description: The following functions correspond to the tag elements.  Functions are
	   * called automatically by container.  In general, the functions only 
	   * set the corresponding class variable based on tag values.  Some set 
	   * functions perform limited initialization.
	   * 
	   * Inputs: 
	   * 
	   * Returns:
	   * 
	   * Version: 1.0
	   *   Version History
	   *   1.0 Initial release (1/8/07)
	   */

	  public void setType (String ty)
	  {
	   type = ty;
	   elementData.put("type", ty);
	  }
	 
	  public void setRequest (HttpServletRequest rq)
	  {
	   request = rq;
	  }
	  
	  public void setName (String nm)
	  {
	   name = nm;
	   elementData.put("name", nm);
	  }

	  public void setSource (String sr)
	  {
	   source = sr;
	   elementData.put("source", sr);
	  }

	  public void setData (String da)
	  {
	   data = da;
	   elementData.put("data", da);
	  }
	  
	  public void setJs (String js)
	  {
	   javascript = js;
	   elementData.put("javascript", js);
	  }
	  
	  public void setCvalue (String value)
	  {
	   checkbox = value;
	   elementData.put("checkbox", checkbox);
	  }
}
