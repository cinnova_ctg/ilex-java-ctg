package com.ilex.reports.utility.operationSummary;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/*************************************************************************/

	/* Title: 
	 * 
	 * Description:
	 * 
	 * 
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 */

public class ElementGroupingMenu1 extends Element{

	ElementGroupingMenu1 (Map init, HttpServletRequest request) {

		super (init, request);
		createElement();

	}

/*************************************************************************/
	
	/* Title: 
	 * 
	 * Description:
	 * 
	 *  
	 * Inputs:
	 * 
	 * Returns: Not Applicable
	 *  
	 */

	void createElement () {
	 
     String[] values = data.split("~");
     String[] option_values;
     
     String selected = "";
     String use_value = "";
     
     String current_group = "";
     String new_group = null;
     
     if (currentValue != null) {
     	use_value = currentValue;
     } 

     elementHTML = "\t<option value=\"\"> -- Select Option -- </option>\n";
     
     String cr = "\n";
     for (int i=0; i<values.length; i++)
     {
  
    	if (i == values.length-1) {
    		cr = "";
    	}
    	
    	// Breakout the individual values
    	
    	option_values = values[i].split("\\|");
    	
    	if (use_value.equals(option_values[2])) {
    		selected = " SELECTED";
    	}
    	
    	new_group = option_values[0];
    	if (!new_group.equals(current_group)) {
    		current_group = new_group;
    		elementHTML = elementHTML.concat("  <optgroup label=\""+new_group+"\">"+cr);
    	}
    	
     	elementHTML = elementHTML.concat("\t<option value=\""+option_values[2]+"\""+selected+">"+option_values[1]+"</option>"+cr);
  	    selected = "";
     }
      
     elementHTML = "<select name=\""+name+"\" size=\"1\" class=\"menu\" "+javascript+">\n"+elementHTML+"</select>";
     
	}

}

