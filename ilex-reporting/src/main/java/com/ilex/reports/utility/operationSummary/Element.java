package com.ilex.reports.utility.operationSummary;



import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ilex.reports.action.operationSummary.DataAccess;

public class Element  {
	
	String type;
	String name;
	String currentValue;
	String source;
	String data;
	String javascript;
	String checkbox;
	
	String elementHTML;
	
	HttpServletRequest request;
	
	DataAccess da = new DataAccess();
	
	/*****************************************************************/
	
	static String html;
	

	/*****************************************************************/
	
	Element () {
		
		
	}

	/*****************************************************************/

	Element (Map tp, HttpServletRequest request) {

		type = (String)tp.get("type");
		name = (String)tp.get("name");
		source = (String)tp.get("source");
		data = (String)tp.get("data");
		javascript = (String)tp.get("javascript");
		checkbox = (String)tp.get("checkbox");
		
		if (source.equals("database")) {
			data = da.getValues1 (data);
		}

		currentValue = getParameter(name, request);
	}
	
	/*****************************************************************/
	
	static Element ElementFactory (Map init, HttpServletRequest request) {

		Element newElement = null;

		String element_type = (String)init.get("type");
		
		if (element_type.equals("hidden")) {
			newElement = new ElementHidden(init, request);

		} else if (element_type.equals("menu1")) {
			newElement = new ElementMenu1(init, request);

		} else if (element_type.equals("menu2")) {
			newElement = new ElementMenu2(init, request);

		} else if (element_type.equals("menu3")) {
			newElement = new ElementMenu3(init, request);

		} else if (element_type.equals("text1")) {
			newElement = new ElementText1(init, request);

		} else if (element_type.equals("date1")) {
			newElement = new ElementDate1(init, request);

		} else if (element_type.equals("textarea1")) {
			newElement = new ElementTextArea1(init, request);

		} else if (element_type.equals("groupingmenu1")) {
			newElement = new ElementGroupingMenu1(init, request);
			
		} else if (element_type.equals("checkbox1")) {
			newElement = new ElementCheckBox1(init, request);
		}
		
		return newElement;
	}
	
	/*************************************************************************/
	
	/* Title: getParameter
	 * 
	 * Description:
	 * 
	 * Inputs: 
	 * 
	 * 
	 * Returns:
	 * 
	 */
	
	String getParameter (String parameter, HttpServletRequest request) {
		
		String parameter_value = null;
		
		if (request != null) {
		
			parameter_value = (String)request.getParameter(parameter);
			
			if (parameter_value == null) {
				
				parameter_value = (String)request.getAttribute(parameter);
				
				if (parameter_value != null) {
					parameter_value = parameter_value.trim();
				}
			}
		}
		return parameter_value;
	}	
}
