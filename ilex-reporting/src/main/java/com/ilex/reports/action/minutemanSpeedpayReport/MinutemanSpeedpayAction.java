package com.ilex.reports.action.minutemanSpeedpayReport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ilex.reports.bean.minutemanSpeedpayReport.MinutemanSpeedpayCostBean;
import com.ilex.reports.formbean.minutemanSpeedpayReport.MinutemanSpeedpayForm;
import com.ilex.reports.logic.minutemanSpeedpayReport.MinutemanSpeedpayOrganiser;
import com.ilex.reports.logic.minutemanSpeedpayReport.MinutemanSpeedpayReportType;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.ExceptionHelper;
import com.mind.fw.lang.SysException;

// TODO: Auto-generated Javadoc
/**
 * The Class MinutemanSpeedpayAction.
 */
public class MinutemanSpeedpayAction extends DispatchAction {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = Logger
			.getLogger(MinutemanSpeedpayAction.class);
	
	
	
	
	public ActionForward outOfBound(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}
		
		
		return mapping.findForward("outOfBound");
	}
	
	
	public ActionForward jobStatusSummary(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}
		
		
		return mapping.findForward("jobStatusSummary");
	}
	
	
	public ActionForward dailyJobReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}
		
		
		return mapping.findForward("dailyJobReport");
	}
	
	public ActionForward pvsUsage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}
		
		
		return mapping.findForward("pvsUsage");
	}
	
	
	
	
	public ActionForward OhioSalesTaxImpactReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}
		
		
		return mapping.findForward("OhioSalesTaxImpactReport");
	}
	/**
	 * Method for Usage by cost Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward usageByCost(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		ActionErrors errors = new ActionErrors();

		MinutemanSpeedpayForm minutemanCostForm = (MinutemanSpeedpayForm) form;
		MinutemanSpeedpayCostBean minutemanCostBean = new MinutemanSpeedpayCostBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		try {
			MinutemanSpeedpayOrganiser.setBeanFromMinutemanForm(
					minutemanCostBean, minutemanCostForm);
			MinutemanSpeedpayOrganiser.populateProjectCategory(
					minutemanCostBean,
					MinutemanSpeedpayReportType.USAGE_BY_COST.getReportName());
			MinutemanSpeedpayOrganiser.setMinutemanSpeedpayGraphDetails(
					minutemanCostBean,
					MinutemanSpeedpayReportType.USAGE_BY_COST.getReportName());
			MinutemanSpeedpayOrganiser.setFormFromMinutemanCostBean(
					minutemanCostForm, minutemanCostBean);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}
			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}

		return mapping.findForward("usageByCost");

	}

	/**
	 * Method for Usage by occurence Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward usageByOccurence(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		ActionErrors errors = new ActionErrors();

		MinutemanSpeedpayForm minutemanCostForm = (MinutemanSpeedpayForm) form;
		MinutemanSpeedpayCostBean minutemanCostBean = new MinutemanSpeedpayCostBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		try {
			MinutemanSpeedpayOrganiser.setBeanFromMinutemanForm(
					minutemanCostBean, minutemanCostForm);
			MinutemanSpeedpayOrganiser.populateProjectCategory(
					minutemanCostBean,
					MinutemanSpeedpayReportType.USAGE_BY_OCCURENCES
							.getReportName());
			MinutemanSpeedpayOrganiser.setMinutemanSpeedpayGraphDetails(
					minutemanCostBean,
					MinutemanSpeedpayReportType.USAGE_BY_OCCURENCES
							.getReportName());
			MinutemanSpeedpayOrganiser.setFormFromMinutemanCostBean(
					minutemanCostForm, minutemanCostBean);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}
			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}

		return mapping.findForward("usageByOccurrences");

	}
}
