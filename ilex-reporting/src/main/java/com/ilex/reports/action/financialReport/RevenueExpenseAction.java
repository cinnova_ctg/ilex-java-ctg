package com.ilex.reports.action.financialReport;

import java.net.URLDecoder;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ilex.reports.action.laborCost.LaborCostAction;
import com.ilex.reports.bean.financialReport.RevenueExpenseBean;
import com.ilex.reports.formbean.financialReport.RevenueExpenseForm;
import com.ilex.reports.logic.financialReport.FinancialReportType;
import com.ilex.reports.logic.financialReport.RevenueExpenseOrganizer;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.ExceptionHelper;
import com.mind.fw.lang.SysException;

/**
 * The Class RevenueExpenseAction will use to handle the all request which is
 * generated from Financial Report Section.
 */
public class RevenueExpenseAction extends DispatchAction {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = Logger.getLogger(LaborCostAction.class);

	/** The calendar. */
	Calendar calendar = Calendar.getInstance();
	public static final String REVENUE_COST = "RevenueCost";

	/**
	 * Method for Revenue expense Report .
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the action forward
	 */
	public ActionForward revenueExpense(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		String jobFilterType = (String) request.getParameter("filterType");
		request.setAttribute("jobFilterType", jobFilterType);
		String reportType = (String) request.getParameter("reportType");
		try {
			revenueExpenseBean = revenueExpenseOrganizer
					.getFinancialMultiAxisChartDetails(
							FinancialReportType.REVENUE_FINANCIAL_REPORT
									.getReportName(), jobFilterType, null, null);

			revenueExpenseOrganizer.setFormFromRevenueExpenseBean(
					revenueExpenseForm, revenueExpenseBean);
		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping.findForward("revenueCostRevenueExpenseReport");
		else
			return mapping.findForward("financialRevenueExpenseReport");
	}

	/**
	 * Method for First level Revenue expense by srpm Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the action forward
	 */
	public ActionForward revenueExpenseBySRPM(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		String reportType = (String) request.getParameter("reportType");
		String param = (String) request.getParameter("parm");
		String param1 = "firstTime";
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		try {

			if (param1.equals(param))
				revenueExpenseBean = revenueExpenseOrganizer
						.setSeniorProjectManagerDetails(param,
								revenueExpenseForm.getFromDate(),
								revenueExpenseForm.getToDate(), new String[] {
										"Project", "NetMedX" },
								revenueExpenseForm);
			else
				revenueExpenseBean = revenueExpenseOrganizer
						.setSeniorProjectManagerDetails(param,
								revenueExpenseForm.getFromDate(),
								revenueExpenseForm.getToDate(),
								revenueExpenseForm.getChecked(),
								revenueExpenseForm);

			revenueExpenseOrganizer.setFormFromRevenueExpenseBean(
					revenueExpenseForm, revenueExpenseBean);
			if (param1.equals(param)) {
				revenueExpenseForm.setChecked(new String[] { "Project",
						"NetMedX" });
			}
		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping.findForward("revenueCostRevenueExpenseBySRPM");
		else
			return mapping.findForward("financialRevenueExpenseBySRPM");
	}

	/**
	 * Method for Revenue expense by srpm chart.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward revenueExpenseBySRPMChart(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		String reportType = (String) request.getParameter("reportType");
		String param = (String) request.getParameter("parameter");
		request.setAttribute("chartReportName", param);
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		try {
			revenueExpenseBean = revenueExpenseOrganizer
					.getFinancialMultiAxisChartDetails(
							FinancialReportType.REVENUE_FINANCIAL_REPORT_BySRPM
									.getReportName(), null, null, null);

			revenueExpenseOrganizer.setFormFromRevenueExpenseBean(
					revenueExpenseForm, revenueExpenseBean);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping.findForward("revenueCostRevenueExpenseBySRPM");
		else
			return mapping.findForward("financialRevenueExpenseBySRPM");
	}

	/**
	 * Mehood for First Level Revenue expense by bdm Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward revenueExpenseByBDM(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String param = (String) request.getParameter("parm");
		String param1 = "firstTime";
		try {

			if (param1.equals(param))
				revenueExpenseBean = revenueExpenseOrganizer.setBDMDetails(
						param, revenueExpenseForm.getFromDate(),
						revenueExpenseForm.getToDate(), new String[] {
								"Project", "NetMedX" }, revenueExpenseForm);
			else

				revenueExpenseBean = revenueExpenseOrganizer.setBDMDetails(
						param, revenueExpenseForm.getFromDate(),
						revenueExpenseForm.getToDate(),
						revenueExpenseForm.getChecked(), revenueExpenseForm);

			revenueExpenseOrganizer.setFormFromRevenueExpenseBean(
					revenueExpenseForm, revenueExpenseBean);

			if (param1.equals(param)) {
				revenueExpenseForm.setChecked(new String[] { "Project",
						"NetMedX" });
			}

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping.findForward("revenueCostRevenueExpenseByBDM");
		else
			return mapping.findForward("financialRevenueExpenseByBDM");
	}

	/**
	 * Report for First Level Revenue expense by job owner Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * 
	 * @return the action forward
	 */
	public ActionForward revenueExpenseByJobOwner(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {

		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String param = (String) request.getParameter("parm");
		String param1 = "firstTime";
		try {
			if (param1.equals(param))
				revenueExpenseBean = revenueExpenseOrganizer
						.setJobOwnersDetails(param,
								revenueExpenseForm.getFromDate(),
								revenueExpenseForm.getToDate(), new String[] {
										"Project", "NetMedX" });
			else
				revenueExpenseBean = revenueExpenseOrganizer
						.setJobOwnersDetails(param,
								revenueExpenseForm.getFromDate(),
								revenueExpenseForm.getToDate(),
								revenueExpenseForm.getChecked());

			revenueExpenseOrganizer.setFormFromRevenueExpenseBean(
					revenueExpenseForm, revenueExpenseBean);
			if (param1.equals(param)) {
				revenueExpenseForm.setChecked(new String[] { "Project",
						"NetMedX" });
				revenueExpenseForm
						.setFromDate((calendar.get(Calendar.MONTH) + 1)
								+ "/01/" + (calendar.get(Calendar.YEAR)));
			}

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping.findForward("revenueCostRevenueExpenseByJobOwner");
		else
			return mapping.findForward("financialRevenueExpenseByJobOwner");
	}

	/**
	 * Report for First Level Revenue expense by Project Manager Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * 
	 * @return the action forward
	 */
	public ActionForward revenueExpenseByProjectManager(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {

		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String param = (String) request.getParameter("parm");
		String param1 = "firstTime";
		try {
			if (param1.equals(param))
				revenueExpenseBean = revenueExpenseOrganizer
						.setProjectManagerDetails(param,
								revenueExpenseForm.getFromDate(),
								revenueExpenseForm.getToDate(), new String[] {
										"Project", "NetMedX" });
			else
				revenueExpenseBean = revenueExpenseOrganizer
						.setProjectManagerDetails(param,
								revenueExpenseForm.getFromDate(),
								revenueExpenseForm.getToDate(),
								revenueExpenseForm.getChecked());

			revenueExpenseOrganizer.setFormFromRevenueExpenseBean(
					revenueExpenseForm, revenueExpenseBean);
			if (param1.equals(param)) {
				revenueExpenseForm.setChecked(new String[] { "Project",
						"NetMedX" });
				revenueExpenseForm
						.setFromDate((calendar.get(Calendar.MONTH) + 1)
								+ "/01/" + (calendar.get(Calendar.YEAR)));
			}

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping
					.findForward("revenueCostRevenueExpenseByProjectManager");
		else
			return mapping
					.findForward("financialRevenueExpenseByProjectManager");
	}

	/**
	 * Revenue expense by customer.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
	public ActionForward revenueExpenseByCustomer(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {

		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String param = (String) request.getParameter("parm");
		String param1 = "firstTime";
		try {
			if (param1.equals(param))
				revenueExpenseBean = revenueExpenseOrganizer
						.setCustomerDetails(param,
								revenueExpenseForm.getFromDate(),
								revenueExpenseForm.getToDate(), new String[] {
										"Project", "NetMedX" });
			else
				revenueExpenseBean = revenueExpenseOrganizer
						.setCustomerDetails(param,
								revenueExpenseForm.getFromDate(),
								revenueExpenseForm.getToDate(),
								revenueExpenseForm.getChecked());

			revenueExpenseOrganizer.setFormFromRevenueExpenseBean(
					revenueExpenseForm, revenueExpenseBean);
			if (param1.equals(param)) {
				revenueExpenseForm.setChecked(new String[] { "Project",
						"NetMedX" });
				revenueExpenseForm
						.setFromDate((calendar.get(Calendar.MONTH) + 1)
								+ "/01/" + (calendar.get(Calendar.YEAR)));
			}

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		
		revenueExpenseForm.setToDate(revenueExpenseForm.getToDate());
		revenueExpenseForm.setFromDate(revenueExpenseForm.getFromDate());
		
		if (REVENUE_COST.equals(reportType))		
			return mapping.findForward("revenueCostRevenueExpenseByCustomer");
		else
			return mapping.findForward("financialRevenueExpenseByCustomer");
	}

	/**
	 * Method for Revenue expense chart by job owner Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * 
	 * @return the action forward
	 */
	public ActionForward revenueExpenseChartByJobOwner(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {
		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		try {
			String jobOwnerName = URLDecoder.decode(
					request.getParameter("jobOwnerName"), "UTF8");
			String jobOwnerID = URLDecoder.decode(
					request.getParameter("jobOwnerId"), "UTF8");
			revenueExpenseBean = revenueExpenseOrganizer
					.getFinancialMultiAxisChartDetails(
							FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
									.getReportName(), null, jobOwnerName,
							jobOwnerID);
			revenueExpenseOrganizer.setFormFromRevenueExpenseBean(
					revenueExpenseForm, revenueExpenseBean);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (Exception e) {
			LOGGER.error(ExceptionHelper.getStackTrace(e));
			ActionError error = new ActionError("reports.sysException",
					e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping
					.findForward("revenueCostRevenueExpenseByJobOwnerChart");
		else
			return mapping
					.findForward("financialRevenueExpenseByJobOwnerChart");

	}

	/**
	 * Mehod for Job owner third level reports.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
	public ActionForward jobOwnerThirdLevelReports(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {
		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenceForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenceBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String month = null;
		String accountID = null;
		String reportName = null;
		String jobOwnerID = null;
		String reportNam = "jobOwnerMonthPopupReport";
		try {
			jobOwnerID = URLDecoder.decode(request.getParameter("jobOwnerID"),
					"UTF8");
			reportName = URLDecoder.decode(request.getParameter("reportName"),
					"UTF8");
			if (reportNam.equals(reportName)) {
				month = URLDecoder
						.decode(request.getParameter("month"), "UTF8");
				revenueExpenceBean = revenueExpenseOrganizer
						.setRevenueJobOwnerDetails(jobOwnerID, reportName,
								month);
			} else {
				accountID = URLDecoder.decode(
						request.getParameter("accountID"), "UTF8");
				revenueExpenceBean = revenueExpenseOrganizer
						.setRevenueJobOwnerDetails(jobOwnerID, reportName,
								accountID);
			}
			revenueExpenseOrganizer.setFormFromJobOwnerBean(revenueExpenceForm,
					revenueExpenceBean, request, responce);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (Exception e) {
			LOGGER.error(ExceptionHelper.getStackTrace(e));
			ActionError error = new ActionError("reports.sysException",
					e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType)) {
			if (reportNam.equals(reportName))
				return mapping
						.findForward("revenueCostRevenueExpenseTableForMonth");
			else
				return mapping
						.findForward("revenueCostRevenueExpenseTableForAccount");
		} else {
			if (reportNam.equals(reportName))
				return mapping
						.findForward("financialRevenueExpenseTableForMonth");
			else
				return mapping
						.findForward("financialRevenueExpenseTableForAccount");
		}

	}

	/**
	 * Mehod for project manager third level reports.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
	public ActionForward projectManagerThirdLevelReports(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {
		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenceForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenceBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String month = null;
		String accountID = null;
		String reportName = null;
		String projectManagerID = null;
		String reportNam = "projectManagerMonthPopupReport";
		try {
			projectManagerID = URLDecoder.decode(
					request.getParameter("projectManagerID"), "UTF8");
			reportName = URLDecoder.decode(request.getParameter("reportName"),
					"UTF8");
			if (reportNam.equalsIgnoreCase(reportName)) {
				month = URLDecoder
						.decode(request.getParameter("month"), "UTF8");
				revenueExpenceBean = revenueExpenseOrganizer
						.setRevenueProjectManagerDetails(projectManagerID,
								reportName, month);
			} else {
				accountID = URLDecoder.decode(
						request.getParameter("accountID"), "UTF8");
				revenueExpenceBean = revenueExpenseOrganizer
						.setRevenueProjectManagerDetails(projectManagerID,
								reportName, accountID);
			}
			revenueExpenseOrganizer.setFormFromProjectManagerBean(
					revenueExpenceForm, revenueExpenceBean, request, responce);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (Exception e) {
			LOGGER.error(ExceptionHelper.getStackTrace(e));
			ActionError error = new ActionError("reports.sysException",
					e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equalsIgnoreCase(reportType)) {
			if (reportNam.equalsIgnoreCase(reportName))
				return mapping
						.findForward("revenueCostRevenueExpenseProjectManagerTableForMonth");
			else
				return mapping
						.findForward("revenueCostRevenueExpenseTableProjectManagerForAccount");
		} else {
			if (reportNam.equalsIgnoreCase(reportName))
				return mapping
						.findForward("financialRevenueExpenseProjectManagerTableForMonth");
			else
				return mapping
						.findForward("financialRevenueExpenseProjectManagerTableForAccount");
		}

	}

	/**
	 * Customer third level reports.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
	public ActionForward customerThirdLevelReports(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {
		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenceForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenceBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String appendixID = null;
		String reportName = null;
		String customerID = null;
		try {
			reportName = URLDecoder.decode(request.getParameter("reportName"),
					"UTF8");
			appendixID = URLDecoder.decode(request.getParameter("appendixID"),
					"UTF8");
			revenueExpenceBean = revenueExpenseOrganizer
					.setRevenueCustomerDetails(customerID, reportName,
							appendixID, "", "");
			revenueExpenseOrganizer.setFormFromCustomerBean(revenueExpenceForm,
					revenueExpenceBean, request, responce);
		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (Exception e) {
			LOGGER.error(ExceptionHelper.getStackTrace(e));
			ActionError error = new ActionError("reports.sysException",
					e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType)) {
			return mapping
					.findForward("revenueCostRevenueExpenseCustomerTableForAccount");
		} else {
			return mapping
					.findForward("financialRevenueExpenseCustomerTableForAccount");
		}

	}

	/**
	 * Method for Bdm third level reports.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
	public ActionForward bdmThirdLevelReports(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {
		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenceForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenceBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String month = null;
		String accountID = null;
		String reportName = null;
		String bdmID = null;
		String reportNam = "bdmMonthPopupReport";
		try {
			bdmID = URLDecoder.decode(request.getParameter("bdmID"), "UTF8");
			reportName = URLDecoder.decode(request.getParameter("reportName"),
					"UTF8");
			if (reportNam.equals(reportName)) {
				month = URLDecoder
						.decode(request.getParameter("month"), "UTF8");
				revenueExpenceBean = revenueExpenseOrganizer
						.setRevenueBDMDetails(bdmID, reportName, month);
			} else {
				accountID = URLDecoder.decode(
						request.getParameter("accountID"), "UTF8");
				revenueExpenceBean = revenueExpenseOrganizer
						.setRevenueBDMDetails(bdmID, reportName, accountID);
			}
			revenueExpenseOrganizer.setFormFromBDMBean(revenueExpenceForm,
					revenueExpenceBean, request, responce);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (Exception e) {
			LOGGER.error(ExceptionHelper.getStackTrace(e));
			ActionError error = new ActionError("reports.sysException",
					e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType)) {
			if (reportNam.equals(reportName))
				return mapping
						.findForward("revenueCostRevenueExpenseBDMTableForMonth");
			else
				return mapping
						.findForward("revenueCostRevenueExpenseBDMTableForAccount");
		} else {
			if (reportNam.equals(reportName))
				return mapping
						.findForward("financialRevenueExpenseBDMTableForMonth");
			else
				return mapping
						.findForward("fianancialRevenueExpenseBDMTableForAccount");
		}

	}

	/**
	 * Method for Spm third level reports.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
	public ActionForward spmThirdLevelReports(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {
		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenceForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenceBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String month = null;
		String accountID = null;
		String reportName = null;
		String spmID = null;
		String reportNam = "spmMonthPopupReport";
		try {
			spmID = URLDecoder.decode(request.getParameter("spmID"), "UTF8");
			reportName = URLDecoder.decode(request.getParameter("reportName"),
					"UTF8");
			if (reportNam.equals(reportName)) {
				month = URLDecoder
						.decode(request.getParameter("month"), "UTF8");
				revenueExpenceBean = revenueExpenseOrganizer
						.setRevenueSPMDetails(spmID, reportName, month,
								revenueExpenceBean);
			} else {
				accountID = URLDecoder.decode(
						request.getParameter("accountID"), "UTF8");
				revenueExpenceBean = revenueExpenseOrganizer
						.setRevenueSPMDetails(spmID, reportName, accountID,
								revenueExpenceBean);
			}
			revenueExpenseOrganizer.setFormFromSPMBean(revenueExpenceForm,
					revenueExpenceBean, request, responce);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (Exception e) {
			LOGGER.error(ExceptionHelper.getStackTrace(e));
			ActionError error = new ActionError("reports.sysException",
					e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType)) {
			if (reportNam.equals(reportName))
				return mapping
						.findForward("revenueCostRevenueExpenseSPMTableForMonth");
			else
				return mapping
						.findForward("revenueCostRevenueExpenseSPMTableForAccount");
		} else {
			if (reportNam.equals(reportName))
				return mapping
						.findForward("financialRevenueExpenseSPMTableForMonth");
			else
				return mapping
						.findForward("fianancialRevenueExpenseSPMTableForAccount");
		}

	}

	/**
	 * Method for Job owner second level reports.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
	public ActionForward jobOwnerSecondLevelReports(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {
		ActionErrors errors = new ActionErrors();
		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String secondLevelReportName = request.getParameter("reportName");
		request.setAttribute("secondLevelReportName", secondLevelReportName);
		String jobOwnerId = request.getParameter("jobOwnerID");
		try {
			revenueExpenseBean = revenueExpenseOrganizer
					.setRevenueJobOwnerDetails(jobOwnerId,
							secondLevelReportName, null);
			revenueExpenseOrganizer.setFormFromJobOwnerBean(revenueExpenseForm,
					revenueExpenseBean, request, responce);
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (Exception e) {
			LOGGER.error(ExceptionHelper.getStackTrace(e));
			ActionError error = new ActionError("reports.sysException",
					e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping.findForward("revenueCostRevenueExpenseByJobOwner");
		else
			return mapping.findForward("financialRevenueExpenseByJobOwner");

	}

	/**
	 * Method for project manager second level reports.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
	public ActionForward projectManagerSecondLevelReports(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {
		ActionErrors errors = new ActionErrors();
		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String secondLevelReportName = request.getParameter("reportName");
		request.setAttribute("secondLevelReportName", secondLevelReportName);
		String projectManagerId = request.getParameter("projectManagerID");
		try {
			revenueExpenseBean = revenueExpenseOrganizer
					.setRevenueProjectManagerDetails(projectManagerId,
							secondLevelReportName, null);
			revenueExpenseOrganizer.setFormFromProjectManagerBean(
					revenueExpenseForm, revenueExpenseBean, request, responce);
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (Exception e) {
			LOGGER.error(ExceptionHelper.getStackTrace(e));
			ActionError error = new ActionError("reports.sysException",
					e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping
					.findForward("revenueCostRevenueExpenseByProjectManager");
		else
			return mapping
					.findForward("financialRevenueExpenseByProjectManager");

	}

	/**
	 * Customer second level reports.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
	public ActionForward customerSecondLevelReports(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {
		ActionErrors errors = new ActionErrors();
		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String secondLevelReportName = request.getParameter("reportName");
		request.setAttribute("secondLevelReportName", secondLevelReportName);
		String customerId = request.getParameter("customerID");
		try {
			revenueExpenseBean = revenueExpenseOrganizer
					.setRevenueCustomerDetails(customerId,
							secondLevelReportName, null, revenueExpenseForm.getToDate(), revenueExpenseForm.getFromDate());
			revenueExpenseOrganizer.setFormFromCustomerBean(revenueExpenseForm,
					revenueExpenseBean, request, responce);
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (Exception e) {
			LOGGER.error(ExceptionHelper.getStackTrace(e));
			ActionError error = new ActionError("reports.sysException",
					e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping.findForward("revenueCostRevenueExpenseByCustomer");
		else
			return mapping.findForward("financialRevenueExpenseByCustomer");

	}

	/**
	 * Method for Bdm second level reports.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
	public ActionForward bdmSecondLevelReports(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {
		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String secondLevelReportName = request.getParameter("reportName");
		request.setAttribute("secondLevelReportName", secondLevelReportName);
		String bdmID = request.getParameter("bdmID");
		try {
			revenueExpenseBean = revenueExpenseOrganizer.setRevenueBDMDetails(
					bdmID, secondLevelReportName, null);
			revenueExpenseOrganizer.setFormFromBDMBean(revenueExpenseForm,
					revenueExpenseBean, request, responce);
		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (Exception e) {
			LOGGER.error(ExceptionHelper.getStackTrace(e));
			ActionError error = new ActionError("reports.sysException",
					e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping.findForward("revenueCostRevenueExpenseByBDM");
		else
			return mapping.findForward("financialRevenueExpenseByBDM");

	}

	/**
	 * Method for Spm second level reports.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
	public ActionForward spmSecondLevelReports(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse responce) {
		ActionErrors errors = new ActionErrors();

		RevenueExpenseForm revenueExpenseForm = (RevenueExpenseForm) form;
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		RevenueExpenseOrganizer revenueExpenseOrganizer = new RevenueExpenseOrganizer();
		String reportType = (String) request.getParameter("reportType");
		String secondLevelReportName = request.getParameter("reportName");
		request.setAttribute("secondLevelReportName", secondLevelReportName);
		String spmID = request.getParameter("spmID");
		try {

			revenueExpenseBean = revenueExpenseOrganizer
					.getFinancialMultiAxisChartDetails(
							FinancialReportType.REVENUE_FINANCIAL_REPORT_BySRPM
									.getReportName(), null, null, null);

			revenueExpenseBean = revenueExpenseOrganizer.setRevenueSPMDetails(
					spmID, secondLevelReportName, null, revenueExpenseBean);
			revenueExpenseOrganizer.setFormFromSPMBean(revenueExpenseForm,
					revenueExpenseBean, request, responce);
		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (Exception e) {
			LOGGER.error(ExceptionHelper.getStackTrace(e));
			ActionError error = new ActionError("reports.sysException",
					e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		if (REVENUE_COST.equals(reportType))
			return mapping.findForward("revenueCostRevenueExpenseBySRPM");
		else
			return mapping.findForward("financialRevenueExpenseBySRPM");

	}

	/**
	 * Method for Revenue expense table(Poppup Window) by sr manager report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @return the action forward
	 */
}
