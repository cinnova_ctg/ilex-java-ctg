package com.ilex.reports.action.manageService;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ilex.reports.bean.managedServices.TicketCountBean;
import com.ilex.reports.dao.managedServices.TicketCountDAO;
import com.ilex.reports.formbean.managedServices.TicketCountForm;
import com.ilex.reports.logic.managedServices.TicketCountOrganizer;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.ExceptionHelper;
import com.mind.fw.lang.SysException;

public class ManagedServiceAction extends DispatchAction {

	public static final Logger LOGGER = Logger
			.getLogger(ManagedServiceAction.class);

	/**
	 * Call for Worked / Closed Ticket Count
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward ticketCount(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		ActionErrors errors = new ActionErrors();

		TicketCountForm ticketCountForm = (TicketCountForm) form;
		TicketCountBean ticketCountBean=new TicketCountBean();
		TicketCountOrganizer ticketCountOrganizer=new TicketCountOrganizer();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		try {
			ticketCountBean = ticketCountOrganizer
					.getChartDetails();
			
			String param = TicketCountDAO.getCountDetails("60");
			request.setAttribute("param", param);
			try {
				BeanUtils.copyProperties(ticketCountForm, ticketCountBean);
			} catch (IllegalAccessException e) {
				throw new SysException(e);
			} catch (InvocationTargetException e) {
				throw new SysException(e);
			}
		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
			return mapping.findForward("workedClosed");
	}
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
}
