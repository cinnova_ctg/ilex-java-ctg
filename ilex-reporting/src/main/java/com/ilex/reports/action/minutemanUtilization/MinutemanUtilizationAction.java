package com.ilex.reports.action.minutemanUtilization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ilex.reports.bean.helpDesk.HDBean;
import com.ilex.reports.bean.minutemanUtilization.MinutemanUtilizationBean;
import com.ilex.reports.formbean.minutemanUtilization.MinutemanUtilizationForm;
import com.ilex.reports.logic.minutemanUtilization.MinutemanUtilizationOrganiser;
import com.ilex.reports.logic.minutemanUtilization.MinutemanUtilizationResultSetBean;
import com.ilex.reports.logic.minutemanUtilization.MinutmanUtilizationReportType;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.ExceptionHelper;
import com.mind.fw.lang.SysException;

public class MinutemanUtilizationAction extends DispatchAction {

	public static final Logger LOGGER = Logger
			.getLogger(MinutemanUtilizationAction.class);

	/**
	 * First time mm usage Report method .
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception 
	 */
	public ActionForward firstTimeMMUsage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		ActionErrors errors = new ActionErrors();
		MinutemanUtilizationBean minutemanUtilizationBean = new MinutemanUtilizationBean();
		MinutemanUtilizationForm minutemanUtlizationForm = (MinutemanUtilizationForm) form;
		MinutemanUtilizationOrganiser minutemanUtilizationOrganiser = new MinutemanUtilizationOrganiser();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		String filter = request.getParameter("Filter");
		String value = minutemanUtlizationForm.getYear();
		if(value==null){
			
			value = request.getParameter("retyear");
			
		}
		String lastTwo = null;
		if(filter==null){
			
			filter="Job Owner";
			
		}
		
		
		if (value != null && value.length() >= 2) {  
		    lastTwo = value.substring(Math.max(value.length() - 2, 0));
		}
		try {
			minutemanUtilizationBean = minutemanUtilizationOrganiser
					.setFirstTimeMinutemanUsage(filter,lastTwo);
			MinutemanUtilizationOrganiser.setFormFromMinutemanUtilization(
					minutemanUtlizationForm, minutemanUtilizationBean);
		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		
		String generateExport = request.getParameter("genExport");

		if (generateExport != null && generateExport.equals("Export")) {
					byte[] buffer = export(minutemanUtilizationBean);
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=FirstTimeUsage.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = null;
					try {
						outStream = response.getOutputStream();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						outStream.write(buffer);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						outStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		request.setAttribute("year",minutemanUtlizationForm.getYear());
		return mapping.findForward("firstTimeMMUsage");

	}

	private byte[] export(MinutemanUtilizationBean minutemanUtilizationBean) throws Exception {
			
		MinutemanUtilizationResultSetBean minutemanUtilizationResultSetBean = null;
		List<MinutemanUtilizationResultSetBean> hdBeanList1 = new ArrayList<MinutemanUtilizationResultSetBean>();
		
		hdBeanList1=minutemanUtilizationBean.getFirstUsageList();
		
		
	
		
		
		
		
		
		
		
		
		
		
		
		
		File f = new File(System.getProperty( "catalina.base" )+"\\temp\\"+System.currentTimeMillis()+"data.csv");
		if (!f.exists()) {
			f.createNewFile();
		}
		FileWriter writer = new FileWriter(f);

		writer.append("Name");
		writer.append(",");
		writer.append("Jan");
		writer.append(",");
		writer.append("Feb");
		writer.append(",");
		writer.append("March");
		writer.append(",");
		writer.append("April");
		writer.append(",");
		writer.append("May");
		writer.append(",");
		writer.append("June");
		writer.append(",");
		writer.append("july");
		writer.append(",");
		writer.append("August");
		writer.append(",");
		writer.append("September");
		writer.append(",");
		writer.append("October");
		writer.append(",");
		writer.append("November");
		writer.append(",");
		writer.append("Decemeber");
		writer.append(",");
		writer.append("Total");
		writer.append('\n');
	//	writer.append('\n');
	

		String repeatedName = "";
		String Totalcol="";
		int prevalue=0;
		
		String  jansum="";
		int prejan=0;
		String febSum="";
		int prejan02=0;
		String marSum="";
		int prejan03=0;
		String aprSum="";
		int prejan04=0;
		String maySum="";
		int prejan05=0;
		String juneSum="";
		int prejan06=0;
		String julySum="";
		int prejan07=0;
		String augSum="";
		int prejan08=0;
		String  sepSum="";
		int prejan09=0;
		String  octSum="";
		int prejan10=0;
		String  novSum="";
		int prejan11=0;
		String  decSum="";
		int prejan12=0;
		int flagcount=2;
		int totalsum=0;
		String TotSum="";
		for (int i = 0; i <  hdBeanList1.size(); i++) {
		MinutemanUtilizationResultSetBean  hdBean = hdBeanList1.get(i);
			// System.out.println(s);
//		if(!repeatedName.equals("") && !repeatedName.equals(hdBean.getName())){
//		
//		/*prevalue=prevalue+hdBean.getOccurance();
//		Totalcol=String.valueOf(prevalue);*/
//		}
		
		prevalue=prevalue+hdBean.getOccurance();
		Totalcol=String.valueOf(prevalue);
	//	if(i==1535)
	//		System.out.println("");
		try {
				
				if(repeatedName.equals(hdBean.getName())){
					writer.append(",");
					writer.append("\"" + hdBean.getOccurance()+"\"" );
					//flagcount=2;
					if(flagcount==2){
						
						prejan02=prejan02+hdBean.getOccurance();
						febSum=String.valueOf(prejan02);	
					
					}
				if(flagcount==3){
						
						prejan03=prejan03+hdBean.getOccurance();
						marSum=String.valueOf(prejan03);	
				
				}
				
				if(flagcount==4){
					
					prejan04=prejan04+hdBean.getOccurance();
					aprSum=String.valueOf(prejan04);	
				
			}
				if(flagcount==5){
					
					prejan05=prejan05+hdBean.getOccurance();
					maySum=String.valueOf(prejan05);	
			
			}
				if(flagcount==6){
					
					prejan06=prejan06+hdBean.getOccurance();
					juneSum=String.valueOf(prejan06);	
				
			}
				if(flagcount==7){
					
					prejan07=prejan07+hdBean.getOccurance();
					julySum=String.valueOf(prejan07);	
				
			}
				if(flagcount==8){
					
					prejan08=prejan08+hdBean.getOccurance();
					augSum=String.valueOf(prejan08);	
				
			}
			if(flagcount==9){
					
					prejan09=prejan09+hdBean.getOccurance();
					sepSum=String.valueOf(prejan09);	
					
			}	
				
			if(flagcount==10){
				
				prejan10=prejan10+hdBean.getOccurance();
				octSum=String.valueOf(prejan10);	
			
		}	
			
			if(flagcount==11){
				
				prejan11=prejan11+hdBean.getOccurance();
				novSum=String.valueOf(prejan11);	
			
		}		
				
			if(flagcount==12){
				
				prejan12=prejan12+hdBean.getOccurance();
				decSum=String.valueOf(prejan12);	
				flagcount=1;
		}				
				
			flagcount++;		
				
				
		if((i+1)==hdBeanList1.size()){
						writer.append(",");
						writer.append("\"" + Totalcol+"\"" );	
					}
			//		prevalue=prevalue+hdBean.getOccurance();
			//		Totalcol=String.valueOf(prevalue);
					
					/*if(i==12){
						
						 writer.append('\n');	
						
					}*/
				}
				else{
				if(i!=0)
				{
					
					/*prevalue=prevalue+hdBean.getOccurance();
					Totalcol=String.valueOf(prevalue);*/
					
					writer.append(",");
					writer.append("\"" + Totalcol+"\"" );	
					prevalue=0;
					writer.append('\n');
				}
					
				
			    writer.append("\"" +hdBean.getName()+"\"");
				//writer.append(",");
			   
			    writer.append(",");
				writer.append("\"" + hdBean.getOccurance()+"\"" );
				prejan=prejan+hdBean.getOccurance();
				jansum=String.valueOf(prejan);
/*				writer.append(",");
			    writer.append("\""  +hdBean.getOccurance()+"\"" );
				writer.append(",");
				writer.append("\"" + hdBean.getOccurance()+"\"" );
				writer.append(",");
				writer.append("\"" +hdBean.getOccurance()+"\"" );
				writer.append(",");
				writer.append("\"" +hdBean.getOccurance()+"\"" );
				writer.append(",");
				writer.append("\"" +hdBean.getOccurance()+ "\"" );*/
               // writer.append('\n');
				}	
				repeatedName=hdBean.getName();
			//	prevalue=prevalue+hdBean.getOccurance();
			//	Totalcol=String.valueOf(prevalue);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		
	//	writer.append("\"" + Totalcol+"\"" );

		}
		writer.append('\n');
		writer.append("Total");
		writer.append(",");
		writer.append("\"" + jansum + "\"");
		writer.append(",");
		writer.append("\"" + febSum + "\"");
		writer.append(",");
		writer.append("\"" + marSum + "\"");
		writer.append(",");
		writer.append("\"" + aprSum + "\"");
		writer.append(",");
		writer.append("\"" + maySum + "\"");
		writer.append(",");
		writer.append("\"" + juneSum + "\"");
		writer.append(",");
		writer.append("\"" + julySum + "\"");
		writer.append(",");
		writer.append("\"" + augSum + "\"");
		writer.append(",");
		writer.append("\"" + sepSum + "\"");
		writer.append(",");
		writer.append("\"" + octSum + "\"");
		writer.append(",");
		writer.append("\"" + novSum + "\"");
		writer.append(",");
		writer.append("\"" + decSum + "\"");
		totalsum=prejan+prejan02+prejan03+prejan04+prejan05+prejan06+prejan07+prejan08+prejan09+prejan10+prejan11+prejan12;
		TotSum=String.valueOf(totalsum);
		writer.append(",");
		writer.append("\"" + TotSum + "\"");
				writer.flush();
				writer.close();

				String csvfilepath = f.getAbsolutePath();

				String filedata = null;
				File file = new File(csvfilepath);

				// FileReader filereader = new FileReader(file);

				FileInputStream fileinputstream = new FileInputStream(file);

				byte[] buffer = new byte[fileinputstream.available()];

				if (fileinputstream.read(buffer) != -1);
				return buffer;
		
		}
	


	/**
	 * minuteman usage by job owner Report method .
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws Exception 
	 */
	public ActionForward minutemanUsageByJobOwner(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		ActionErrors errors = new ActionErrors();
		MinutemanUtilizationBean minutemanUtilizationBean = new MinutemanUtilizationBean();
		MinutemanUtilizationForm minutemanUtlizationForm = (MinutemanUtilizationForm) form;
		
		MinutemanUtilizationOrganiser minutemanUtilizationOrganiser = new MinutemanUtilizationOrganiser();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		String value = minutemanUtlizationForm.getYear();
		Calendar calendar = Calendar.getInstance();
		if(value==null && request.getParameter("genExport")==null){
			
			int year=calendar.get(Calendar.YEAR);
			value=String.valueOf(calendar.get(Calendar.YEAR));
			
		}
		
		if(request.getParameter("genExport")!=null || request.getParameter("retyear")!=null)
		{
			
			value = request.getParameter("retyear");
			
			
		}
		
		/*String lastTwo = null;
		if (value != null && value.length() >= 2) {  
		    lastTwo = value.substring(Math.max(value.length() - 2, 0));
		}*/
		try {
			minutemanUtilizationBean = minutemanUtilizationOrganiser
					.setMinutemanUsageByJobOwner(value);
			MinutemanUtilizationOrganiser.setFormFromMinutemanUtilization(
					minutemanUtlizationForm, minutemanUtilizationBean);
		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		
		
		
		String generateExport = request.getParameter("genExport");

		if (generateExport != null && generateExport.equals("Export")) {
					byte[] buffer = export(minutemanUtilizationBean);
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=minutemanUsageByJobOwner.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = null;
					try {
						outStream = response.getOutputStream();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						outStream.write(buffer);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						outStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		request.setAttribute("year",minutemanUtlizationForm.getYear());
		return mapping.findForward("minutemanUsageByJobOwner");

	}

	/**
	 * Minuteman availablity Report Method .
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward minutemanAvailablity(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		ActionErrors errors = new ActionErrors();
		MinutemanUtilizationForm availablityMMForm = (MinutemanUtilizationForm) form;
		MinutemanUtilizationBean availablityMMBean = new MinutemanUtilizationBean();
		MinutemanUtilizationOrganiser availablityLogic = new MinutemanUtilizationOrganiser();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		try {
			MinutemanUtilizationOrganiser.setMinutemanUtilizationGraphDetails(
					availablityMMBean,
					MinutmanUtilizationReportType.AVAILABILITY_RECRUITING
							.getReportName());
			availablityMMBean = availablityLogic
					.setMMAvailablity(availablityMMBean);
			MinutemanUtilizationOrganiser.setFormFromMinutemanUtilization(
					availablityMMForm, availablityMMBean);
		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}
		

		
		return mapping.findForward("MMAvailability");
	}

	

	
	
}
