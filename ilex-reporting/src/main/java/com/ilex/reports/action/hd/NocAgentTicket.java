package com.ilex.reports.action.hd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ilex.reports.action.operationSummary.DataAccess;
import com.ilex.reports.bean.helpDesk.HDBean;

public class NocAgentTicket extends DispatchAction {

	DataAccess da = new DataAccess();
	SimpleDateFormat MMM_YY = new SimpleDateFormat("MMM y");
	List noc_lists ;

	public ActionForward getAgentTicketData(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String forward_key = "ticketDataTicket";

		String days = "30";
		
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		
		if(request.getParameter("days") !=null && !request.getParameter("days").equals("")){
			days = request.getParameter("days").toString();
		}else if(request.getAttribute("days") !=null && !request.getAttribute("days").toString().equals("")){
			days = request.getAttribute("days").toString();
		}
		
		request.setAttribute("days", days);
		

		String query = "SELECT isnull(lo_pc_first_name +', '+lo_pc_last_name,'')as name, count(*) tickets, "
				+ " lm_na_next_action, isnull(lm_hd_root_cause, 0) lm_hd_root_cause,  "
				+ " lm_ci_config_item, lm_ct_category,  "
				+ " isnull(lm_hd_urgency, 0) lm_hd_urgency   "
				+ " FROM lm_help_desk_ticket_details "
			//	+ " LEFT JOIN lm_help_desk_create_ticket_customer_list_view ON lp_mm_id = lm_hd_lp_mm_id "
				+ " LEFT JOIN lm_help_desk_ticket_next_actions ON lm_na_id = lm_hd_next_action "
				+ " LEFT JOIN lm_help_desk_ticket_config_items ON lm_ci_id = lm_hd_ci "
				+ " LEFT JOIN lm_help_desk_ticket_categories ON lm_ct_id = lm_hd_category "
			//	+ " LEFT JOIN lm_help_desk_next_action_history ON  lm_na_tc_id=lm_hd_tc_id"
				+ " LEFT JOIN lo_poc ON lo_pc_id = lm_hd_assigned_to "
				+ " where lm_hd_opened> dateadd(dd,-"+days+",getdate())  "
			//	+ " and lo_ot_name is not null  "
				+ " GROUP BY lo_pc_first_name+', '+ lo_pc_last_name,  lm_na_next_action, "
				+ " lm_hd_root_cause, lm_ci_config_item,   "
				+ " lm_ct_category, lm_hd_urgency  " 
				+ " HAVING count(*) > 0"
				+ " ORDER BY lo_pc_first_name+', '+ lo_pc_last_name";
				


		ResultSet rs = this.da.getResultSet(query);
		HDBean hdBean = null;

		List<HDBean> hdBeanList = new ArrayList<HDBean>();

		while (rs.next()) {
			hdBean = new HDBean();

		/*	hdBean.setUpdatecount(rs.getString("count"));
			hdBean.setTicketid(rs.getString("lm_hd_tc_id"));*/
			hdBean.setName(rs.getString("name"));
			hdBean.setTicketCount(rs.getString("tickets"));
			hdBean.setNextAction(rs.getString("lm_na_next_action"));
			hdBean.setRootCause(rs.getString("lm_hd_root_cause"));
			hdBean.setConfigItem(rs.getString("lm_ci_config_item"));
			hdBean.setCategory(rs.getString("lm_ct_category"));
			hdBean.setUrgency(rs.getString("lm_hd_urgency"));
			
			
			
			if (hdBean.getUrgency().equals("1")) {
				hdBean.setUrgency("High");
			} else if (hdBean.getUrgency().equals("2")) {
				hdBean.setUrgency("Medium");
			} else if (hdBean.getUrgency().equals("3")) {
				hdBean.setUrgency("Low");
			} else {
				hdBean.setUrgency("N/A");
			}

			if (hdBean.getRootCause().equals("1")) {
				hdBean.setRootCause("General connection error");
			} else if (hdBean.getRootCause().equals("2")) {
				hdBean.setRootCause("Configuration error");
			} else if (hdBean.getRootCause().equals("3")) {
				hdBean.setRootCause("Customer caused outage");
			} else if (hdBean.getRootCause().equals("4")) {
				hdBean.setRootCause("Power outage");
			} else if (hdBean.getRootCause().equals("5")) {
				hdBean.setRootCause("Hardware failure");
			} else if (hdBean.getRootCause().equals("6")) {
				hdBean.setRootCause("Provider Outage");
			} else if (hdBean.getRootCause().equals("7")) {
				hdBean.setRootCause("DynDNS");
			} else {
				hdBean.setRootCause("N/A");
			}

			hdBeanList.add(hdBean);
		}
		// get listing from jsp page  and show in excel 
		
		
		String generateExport = request.getParameter("genExport");

		if (generateExport != null && generateExport.equals("Export")) {
					byte[] buffer = export(hdBeanList);
					response.setContentType("application/x-msdownload");
					response.setHeader("Content-Disposition",
							"attachment;filename=NocTicketExport.csv;size="
									+ buffer.length + "");
					ServletOutputStream outStream = response.getOutputStream();
					outStream.write(buffer);
					outStream.close();
		}
		
	request.setAttribute("HDBeanList", hdBeanList);
					
	return mapping.findForward(forward_key);
		

}	
	
	
	public byte[] export(List<HDBean> hdBeanList) throws Exception {

		List<HDBean> hdBeanList1 = new ArrayList<HDBean>(); 
		hdBeanList1=hdBeanList;
		
		File f = new File(System.getProperty( "catalina.base" )+"\\temp\\"+System.currentTimeMillis()+"dataAgentTicket.csv");
		if (!f.exists()) {
			f.createNewFile();
		}
		FileWriter writer = new FileWriter(f);

		/*writer.append("Count");
		writer.append(",");
		writer.append("lm_hd_tc_id");
		writer.append(",");*/
		writer.append("name");
		writer.append(",");
		writer.append("Number of Ticket");
		writer.append(",");
		writer.append("Root Cause");
		writer.append(",");
		writer.append("Configuration items");
		writer.append(",");
		writer.append("Category");
		writer.append(",");
		writer.append("Urgency");
		writer.append('\n');
		writer.append('\n');
	

		
		for (int i = 0; i <  hdBeanList1.size(); i++) {
		HDBean  hdBean = hdBeanList1.get(i);
			// System.out.println(s);
		try {
				
		/*		
			writer.append("\"" + hdBean.getUpdatecount()+"\"");
			writer.append(",");
			writer.append("\"" + hdBean.getTicketid()+"\"");
			writer.append(",");*/
			writer.append("\"" +hdBean.getName()+"\"" );
			writer.append(",");
			writer.append("\"" +hdBean.getTicketCount()+"\"" );
			writer.append(",");
//			writer.append("\"" +hdBean.getNextAction()+"\"" );
//			writer.append(",");
			writer.append("\"" +hdBean.getRootCause()+"\"" );
			writer.append(",");
			writer.append("\"" +hdBean.getConfigItem()+"\"" );
			writer.append(",");
			writer.append("\"" +hdBean.getCategory()+"\"" );
			writer.append(",");
			writer.append("\"" +hdBean.getUrgency()+ "\"" );
            writer.append('\n');
				
					
			
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		
		writer.flush();
		writer.close();

		String csvfilepath = f.getAbsolutePath();

		String filedata = null;
		File file = new File(csvfilepath);

		// FileReader filereader = new FileReader(file);

		FileInputStream fileinputstream = new FileInputStream(file);

		byte[] buffer = new byte[fileinputstream.available()];

		if (fileinputstream.read(buffer) != -1);
		return buffer;

	}
	
}	
	
	
	

