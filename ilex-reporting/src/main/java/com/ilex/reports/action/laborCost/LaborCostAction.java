package com.ilex.reports.action.laborCost;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ilex.reports.bean.laborCost.LaborCostBean;
import com.ilex.reports.bean.laborCost.LaborCostByLocationBean;
import com.ilex.reports.common.LabelValue;
import com.ilex.reports.formbean.laborCost.LaborCostByLocationForm;
import com.ilex.reports.formbean.laborCost.LaborCostForm;
import com.ilex.reports.logic.laborCost.LaborCostOrganizer;
import com.ilex.reports.logic.laborCost.LaborCostReportType;
import com.ilex.reports.logic.laborCost.locationWise.LocationWiseOrganizer;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.ExceptionHelper;
import com.mind.fw.lang.SysException;

public class LaborCostAction extends DispatchAction {

	public static final Logger LOGGER = Logger.getLogger(LaborCostAction.class);

	/**
	 * Call for Top Level Labor Cost Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward topLevelLaborCost(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		ActionErrors errors = new ActionErrors();

		LaborCostForm laborCostForm = (LaborCostForm) form;
		LaborCostBean laborCostBean = new LaborCostBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		try {
			LaborCostOrganizer.setLaborCostChartDetails(laborCostBean,
					LaborCostReportType.TOP_LEVEL_LABOR_COST.getReportName());

			LaborCostOrganizer.setFormFromLaborCostBean(laborCostForm,
					laborCostBean);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}

		return mapping.findForward("topLevelLabor");

	}

	/**
	 * Call for Technician Labor Cost Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward technicianLaborCost(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		ActionErrors errors = new ActionErrors();

		LaborCostForm laborCostForm = (LaborCostForm) form;
		LaborCostBean laborCostBean = new LaborCostBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		try {
			LaborCostOrganizer.setBeanFromLaborCostForm(laborCostBean,
					laborCostForm);

			LaborCostOrganizer.populateLaborCategory(laborCostBean,
					LaborCostReportType.TECHNICIAN_LABOR_COST.getReportName());

			LaborCostOrganizer.setLaborCostChartDetails(laborCostBean,
					LaborCostReportType.TECHNICIAN_LABOR_COST.getReportName());

			LaborCostOrganizer.setFormFromLaborCostBean(laborCostForm,
					laborCostBean);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}
			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}

		return mapping.findForward("technicianLabor");

	}

	/**
	 * Call for Sr PM Labor Cost Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward srPMLaborCost(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		ActionErrors errors = new ActionErrors();

		LaborCostForm laborCostForm = (LaborCostForm) form;
		LaborCostBean laborCostSrPmBean = new LaborCostBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		try {
			LaborCostOrganizer.setLaborCostChartDetails(laborCostSrPmBean,
					LaborCostReportType.SR_PM_LABOR_COST.getReportName());

			LaborCostOrganizer.setFormFromSrPmLaborCostBean(laborCostForm,
					laborCostSrPmBean);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}
			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}

		return mapping.findForward("srPmLabor");
	}

	/**
	 * Call for Labor Cost By Location For PVS Labor Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward laborCostBylocation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		ActionErrors errors = new ActionErrors();

		LaborCostByLocationForm LaborCostByLocForm = (LaborCostByLocationForm) form;
		LaborCostByLocationBean LaborCostByLocBean = new LaborCostByLocationBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		try {
			LocationWiseOrganizer.setBeanFromForm(LaborCostByLocBean,
					LaborCostByLocForm);

			LocationWiseOrganizer
					.setLaborCostByLocationBean(LaborCostByLocBean);

			LocationWiseOrganizer.setFormFromBean(LaborCostByLocForm,
					LaborCostByLocBean);
			 ArrayList<LabelValue> monthList=LocationWiseOrganizer.getMonthsList();
			 LaborCostByLocForm.setMonthList(monthList);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}
			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}

		return mapping.findForward("laborCostByLocation");
	}

	/**
	 * Labor cost by criticality Report.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward laborCostByCriticality(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		ActionErrors errors = new ActionErrors();

		LaborCostForm laborCostForm = (LaborCostForm) form;
		LaborCostBean laborCostBean = new LaborCostBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		try {
			LaborCostOrganizer.setBeanFromLaborCostForm(laborCostBean,
					laborCostForm);
			LaborCostOrganizer.populateLaborType(laborCostBean);

			LaborCostOrganizer.setLaborByCategoryDetail(laborCostBean,
					LaborCostReportType.CATEGORY_LABOR.getReportName());

			LaborCostOrganizer.setFormFromLaborCostBean(laborCostForm,
					laborCostBean);

		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}
			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		}

		return mapping.findForward("categoryLabor");

	}
}
