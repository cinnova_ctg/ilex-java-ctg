package com.ilex.reports.action.hd;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.log4j.Logger;

import com.ilex.report.formbean.helpdesk.FullResolutionBean;
import com.ilex.reports.dao.AbstractDAO;
import com.ilex.reports.dao.financialReport.FinancialReportDAO;
import com.ilex.reports.logic.financialReport.RevenueExpenceTableResultSetBean;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;

public class HDTicketDataDao extends  AbstractDAO {

	
	public static final Logger LOGGER = Logger
			.getLogger(FinancialReportDAO.class);

	
	public List<FullResolutionBean> getAckTicketData(
			String days) throws SysException,
			AppException {

		
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<FullResolutionBean> beanList=new ArrayList<FullResolutionBean>();
		try {

			initIlexDS();
			conn = ilexMainDS.getConnection();
			cstmt = conn.prepareCall("{?=call ack_ticket_data_analysis(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, days);
		    rs = cstmt.executeQuery();
		    FullResolutionBean rSBean = new FullResolutionBean();
		    while(rs.next()){
		    	rSBean=new FullResolutionBean();
		    	rSBean.setAveragediff(rs.getString("Average"));
		    	rSBean.setDays(rs.getString("day"));
		    	beanList.add(rSBean);
		    	
		    }
		    
			/*beanList = (List<FullResolutionBean>) beanProcessor
					.toBeanList(rs, RSBean.getClass());*/
			LOGGER.debug("QUERY-> Executed");
			LOGGER.debug("beanlist populated");
		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, cstmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

		
	
	
	
}
