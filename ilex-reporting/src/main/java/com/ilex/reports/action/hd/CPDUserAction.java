package com.ilex.reports.action.hd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.JSONException;
import org.json.JSONObject;

import com.ilex.reports.action.operationSummary.DataAccess;



public class CPDUserAction extends DispatchAction {

	DataAccess da = new DataAccess();
	SimpleDateFormat MMM_YY = new SimpleDateFormat("MMM y");
	List<String> noc_lists ;
	List<String> incident_lists ;
	List<String> incoming_lists ;
	List<String> outgoing_lists ;
	List<String> fileapproved ;
	List<String> countbadgeIdList;
	List<String> Statuschange;
	List<String> StatusWork;
    List<String>  StatusWorkinurance;
	List<String> StatusWorkresume;

	
	public ActionForward getAllData(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response)
		throws Exception {
	String forward_key = "DataAction";
	HttpSession session = request.getSession(true);
	String loginuserid = (String) session.getAttribute("userid");

	if (loginuserid == null) {
		return (mapping.findForward("SessionExpire")); // Check for session
														// expired
	}

	List<String> listData = new ArrayList<String>();
	
	String days = "30";		
	
	if(request.getParameter("days") !=null && !request.getParameter("days").equals("")){
		days = request.getParameter("days").toString();
	}else if(request.getAttribute("days") !=null && !request.getAttribute("days").toString().equals("")){
		days = request.getAttribute("days").toString();
	}
	
	
	String cpduserquery ="SELECT lo_pc_first_name + '  ' + lo_pc_last_name AS cpdname    " +
			"FROM lo_poc WHERE lo_pc_id IN (SELECT lo_pr_pc_id FROM lo_poc_rol WHERE lo_pr_pc_id IN (7270,35605,35737)) " +
			"AND lo_pc_active_user <> 0  GROUP BY lo_pc_first_name,lo_pc_last_name";
	ResultSet rs11 = this.da.getResultSet(cpduserquery);
	List<String> cpduser = new ArrayList<String>();
	while(rs11.next()){
        cpduser.add(rs11.getString("cpdname"));
		
	}
	
	
	
	String cpduserpartnerquery ="SELECT lo_pc_first_name + ' ' + lo_pc_last_name AS cpdname    " +
			"FROM lo_poc WHERE lo_pc_id IN (SELECT lo_pr_pc_id FROM lo_poc_rol WHERE lo_pr_pc_id IN (7270,35605,35737)) " +
			"AND lo_pc_active_user <> 0  GROUP BY lo_pc_first_name,lo_pc_last_name";
	ResultSet rs22 = this.da.getResultSet(cpduserpartnerquery);
	ArrayList<String[]> cpduserpartner = new ArrayList<String[]>();
	while(rs22.next()){
		String name = rs22.getString("cpdname");
    String[] strarr ={name};
    cpduserpartner.add(strarr);
		
	}

	String[] username  =cpduser.toArray(new String[cpduser.size()]);
	for (int j = 0; j < cpduser.size(); j++) {
	    username[j] = cpduser.get(j).toString();
	}	


	for(String n: username)
	{
    System.out.println(n+" ");
	}

	//  getting partner update profile count ()	
	String partnerupdateprofile="SELECT  Isnull(count(cp_partner_id),'0')as profileupdate,lo_pc_first_name + '  ' + lo_pc_last_name AS cpdname  FROM cp_partner "
			+" INNER JOIN lo_poc ON lo_poc.lo_pc_id = cp_pt_updated_by"+
			" WHERE lo_pc_id IN (SELECT lo_pr_pc_id FROM lo_poc_rol WHERE lo_pr_pc_id IN (7270,35605,35737))" 
			+ "AND lo_pc_active_user <> 0 "
			+ "And cp_pt_updated_date > dateadd(dd,-"+days+",getdate())"
			+ "GROUP BY  lo_pc_first_name,lo_pc_last_name";
	
	ResultSet rs12 = this.da.getResultSet(partnerupdateprofile);
	List<String> cpduserupdateprofile = new ArrayList<String>();
	while(rs12.next()){
		String name = rs12.getString("profileupdate");
	    String firstlastname=rs12.getString("cpdname");
		 cpduserupdateprofile.add(name+","+firstlastname);
		
		 
	}
	
	 noc_lists =compareusername(cpduser,cpduserupdateprofile);
	
	 
	 
	 //  getting  document approved
	 
		String documentapprovedquery ="SELECT count(lx_file_id) as documentupdate,lo_pc_first_name + '  ' + lo_pc_last_name AS cpdname  FROM lx_upload_file" 
			 	+ " INNER JOIN lo_poc  ON lo_pc_id =  lx_file_uploaded_by "
			 	+ " WHERE lo_pc_id IN (SELECT lo_pr_pc_id FROM lo_poc_rol WHERE lo_pr_pc_id IN (7270,35605,35737))" 
				+ " AND lo_pc_active_user <> 0"
		  		+ " And lx_file_uploaded_date > dateadd(dd,-"+days+",getdate())"
				+ " GROUP BY  lo_pc_first_name,lo_pc_last_name";
		 
		ResultSet rs20 = this.da.getResultSet(documentapprovedquery);
		List<String> counterfileapproved = new ArrayList<String>();
		while(rs20.next()){
			String name = rs20.getString("documentupdate");
		    String firstlastname=rs20.getString("cpdname");
		//	String[] strarr ={name + ","+ firstlastname};
		//	datalist = convertToCommaDelimited(strarr);
		//	String[] datalistcpd=compareusername(cpduser,strarr);
			
		
		    counterfileapproved.add(name+","+firstlastname);
			
			 
		}
		 
		 
		fileapproved=compareusername(cpduser,counterfileapproved);
		 
				
		String URL = "https://ccc.contingent.com/index.php?cmp=phone_ws&type=cpd_user_action&period="+days;
		String responsedata = getDataFromWeb(URL, 9999, "\n");
		String[] urldata = null;
		List<String> countbadgeId = new ArrayList<String>();
		
		if (responsedata != null && (!responsedata.equals(""))) {
			urldata = responsedata.split(",");
		}

		if (urldata != null) {
			
			for (int j = 0; j < urldata.length; j++) {
						String keyVal = urldata[j];
						
						if (keyVal != null) {
							keyVal=keyVal.replaceAll("\\{", " ");
							keyVal=keyVal.replaceAll("\"", "");
							keyVal=keyVal.replaceAll("\\}", " ");
							countbadgeId.add(keyVal);
							//break;
						}

					}

			}
		
		

		String cpduserqueryweb ="SELECT lo_pc_first_name + ' ' + lo_pc_last_name AS cpdname    FROM lo_poc " +
				"WHERE lo_pc_id IN (SELECT lo_pr_pc_id FROM lo_poc_rol WHERE lo_pr_pc_id IN (7270,35605,35737)) " +
				"AND lo_pc_active_user <> 0  GROUP BY lo_pc_first_name,lo_pc_last_name";
		ResultSet rs1 = this.da.getResultSet(cpduserqueryweb);
		List<String> cpduserweb = new ArrayList<String>();
		while(rs1.next()){
			cpduserweb.add(rs1.getString("cpdname"));
			
		}	
		

		countbadgeIdList=compareusernameweb(cpduserweb,countbadgeId);
	 
// incident report count filed 	
	String incidentreportfiles=" SELECT  count(cp_ir_id) as incidentreport, lo_pc_first_name + '  ' + lo_pc_last_name AS cpdname "
							+ " FROM  cp_partner " 
                            + " INNER JOIN cp_incident_report ON cp_ir_partner_id = cp_partner.cp_partner_id "
                            + " INNER JOIN lo_poc ON lo_poc.lo_pc_id = cp_ir_created_by "
                            + " WHERE lo_pc_id IN (SELECT lo_pr_pc_id FROM lo_poc_rol WHERE lo_pr_pc_id IN (7270,35605,35737)) " 
                            + " AND lo_pc_active_user <> 0 "
                            + " And  cp_ir_create_date  >  dateadd(dd,-"+days+",getdate()) "
                            + " GROUP BY  lo_pc_first_name,lo_pc_last_name";
	ResultSet rs13 = this.da.getResultSet(incidentreportfiles);
	List<String> incident = new ArrayList<String>();
	
	while(rs13.next()){
		String name = rs13.getString("incidentreport");
		String firstlastname=rs13.getString("cpdname");
		 incident.add(name+","+firstlastname);
		 
	}
	

	incident_lists =compareusername(cpduser,incident);
	
	
	
// count status changed 	
	
	String statuschanged="SELECT  count(cp_partner_status_history.cp_partner_hs_id ) as status_changed,ref.lo_pc_first_name + '  ' + ref.lo_pc_last_name AS cpdname "
					   + "   FROM  lo_poc AS ref "
                       + "   INNER JOIN cp_partner_status_history  ON cp_partner_status_history.cp_partner_hs_updated_by =ref.lo_pc_id "
                       + "  WHERE ref.lo_pc_id IN (SELECT lo_pr_pc_id FROM lo_poc_rol WHERE lo_pr_pc_id IN (7270,35605,35737)) " 
                       + "  AND ref.lo_pc_active_user <> 0 " 
                       + "   And  cp_partner_status_history.cp_partner_hs_updated_date >  dateadd(dd,-"+days+",getdate()) " 
                       + "  GROUP BY  ref.lo_pc_first_name,ref.lo_pc_last_name " ;

	
	ResultSet rs33 = this.da.getResultSet(statuschanged);
	List<String> changestatus = new ArrayList<String>();
	
	
		while(rs33.next()){
		String name = rs33.getString("status_changed");
		String firstlastname=rs33.getString("cpdname");
	
		changestatus.add(name+","+firstlastname);
		 
	}
	
	
	
 
	Statuschange =compareusername(cpduser,changestatus);
	
	
	//cp_pd_w9w9_uploaded
	String statusworkuploaded= " SELECT count(lx_upload_file_history.lx_fh_id) as countstatus,"
			 + " ref.lo_pc_first_name + '  ' + ref.lo_pc_last_name AS cpdname "
			 + " FROM lo_poc AS ref" 
			 + " INNER JOIN lx_upload_file_history ON lx_upload_file_history.lx_fh_uploaded_by = ref.lo_pc_id "
			 + " JOIN cp_partner_details ON cp_partner_details.cp_pd_partner_id = lx_upload_file_history.lx_fh_used_in "
			 + " WHERE ref.lo_pc_id IN (SELECT lo_pr_pc_id FROM lo_poc_rol WHERE lo_pr_pc_id IN (7270,35605,35737)) "
			 + " AND ref.lo_pc_active_user <> 0 "
			 + " AND (cp_partner_details.cp_pd_w9w9_uploaded='Y' OR cp_partner_details.cp_pd_w9w9='Y')"
			 + " And lx_upload_file_history.lx_fh_uploaded_date > dateadd(dd,-"+days+",getdate()) "
			 + " GROUP BY ref.lo_pc_first_name,ref.lo_pc_last_name ";
	
	ResultSet rs34 = this.da.getResultSet(statusworkuploaded);
	List<String> changestatuswork = new ArrayList<String>();
	while(rs34.next()){
		String name = rs34.getString("countstatus");
		String firstlastname=rs34.getString("cpdname");
	
		changestatuswork.add(name+","+firstlastname);
		 
	}
	
	StatusWork =compareusername(cpduser,changestatuswork);
	
	//getting value of 
	
	
	String statusworkinsurance = " SELECT count(lx_upload_file_history.lx_fh_id) as countstatus,"
			 + " ref.lo_pc_first_name + '  ' + ref.lo_pc_last_name AS cpdname "
			 + " FROM lo_poc AS ref" 
			 + " INNER JOIN lx_upload_file_history ON lx_upload_file_history.lx_fh_uploaded_by = ref.lo_pc_id "
			 + " JOIN cp_partner_details ON cp_partner_details.cp_pd_partner_id = lx_upload_file_history.lx_fh_used_in "
			 + " WHERE ref.lo_pc_id IN (SELECT lo_pr_pc_id FROM lo_poc_rol WHERE lo_pr_pc_id IN (7270,35605,35737)) "
			 + " AND ref.lo_pc_active_user <> 0 "
			 + " AND (cp_partner_details.cp_pd_insurance_uploaded='Y' OR cp_partner_details.cp_pd_insurance='Y')"
			 + " And lx_upload_file_history.lx_fh_uploaded_date > dateadd(dd,-"+days+",getdate()) "
			 + " GROUP BY ref.lo_pc_first_name,ref.lo_pc_last_name ";
	
	ResultSet rs35 = this.da.getResultSet(statusworkinsurance);
	List<String> changestatusworkinsurance = new ArrayList<String>();
	while(rs35.next()){
		String name = rs35.getString("countstatus");
		String firstlastname=rs35.getString("cpdname");
	
		changestatusworkinsurance.add(name+","+firstlastname);
		 
	}

	StatusWorkinurance=compareusername(cpduser,changestatusworkinsurance);
	
	//getting value of 
	
	//cp_pd_resume_uploaded
	String statusworkresume = " SELECT count(lx_upload_file_history.lx_fh_id) as countstatus,"
			 + " ref.lo_pc_first_name + '  ' + ref.lo_pc_last_name AS cpdname "
			 + " FROM lo_poc AS ref" 
			 + " INNER JOIN lx_upload_file_history ON lx_upload_file_history.lx_fh_uploaded_by = ref.lo_pc_id "
			 + " JOIN cp_partner_details ON cp_partner_details.cp_pd_partner_id = lx_upload_file_history.lx_fh_used_in "
			 + " WHERE ref.lo_pc_id IN (SELECT lo_pr_pc_id FROM lo_poc_rol WHERE lo_pr_pc_id IN (7270,35605,35737)) "
			 + " AND ref.lo_pc_active_user <> 0 "
			 + " AND (cp_partner_details.cp_pd_resume_uploaded='Y' OR cp_partner_details.cp_pd_resume='Y')"
			 + " And lx_upload_file_history.lx_fh_uploaded_date > dateadd(dd,-"+days+",getdate()) "
			 + " GROUP BY ref.lo_pc_first_name,ref.lo_pc_last_name ";

	
	ResultSet rs36 = this.da.getResultSet(statusworkresume);
	List<String> changestatusworkresume = new ArrayList<String>();
	while(rs36.next()){
		String name = rs36.getString("countstatus");
		String firstlastname=rs36.getString("cpdname");
	
		changestatusworkresume.add(name+","+firstlastname);
		 
	}
	
	StatusWorkresume = compareusername(cpduser,changestatusworkresume);
	
// count domestic us partner	
	
	 String recruitingDomestic="SELECT count(cp_partner_id) As Domestic"
			+" FROM  cp_partner" 
			+" left outer join  lo_organization_main ON lo_om_id = cp_partner_om_id"  
			+" left outer join lo_address on lo_ad_id = lo_om_ad_id1"
			+" WHERE lo_ad_country = 'US' " +
			"  AND cp_pt_partner_type IS NOT NULL"
			+ " AND cp_pt_created_date > dateadd(dd,-"+days+",getdate()) "
			+" GROUP BY cp_pt_partner_type"
			+" HAVING cp_pt_partner_type='M' ";
	ResultSet rs14 = this.da.getResultSet(recruitingDomestic);
	String DomesticCount = "0";
	
	while(rs14.next()){
		String name = rs14.getString("Domestic");
		String[] strarr ={name};
		DomesticCount=name;
		
	}
	
	String recruitingInt="SELECT count(cp_partner_id) as Int"
			+" FROM cp_partner" 
			+" left outer join  lo_organization_main ON lo_om_id = cp_partner_om_id"  
			+" left outer join lo_address on lo_ad_id = lo_om_ad_id1"
			+" WHERE lo_ad_country <> 'US' " +
			" AND cp_pt_partner_type IS NOT NULL"
			
			+ " AND cp_pt_created_date > dateadd(dd,-"+days+",getdate())"
			+" GROUP BY cp_pt_partner_type"
			+" HAVING cp_pt_partner_type='M'";
	ResultSet rs15 = this.da.getResultSet(recruitingInt);
	String  International = "0";
		while(rs15.next()){
		String name = rs15.getString("Int");
		String[] strarr ={name};
		International=name;
		
	}
	
	String recruitingDomesticM="SELECT count(cp_partner_id) As Domestic"
			+" FROM  cp_partner" 
			+" left outer join  lo_organization_main ON lo_om_id = cp_partner_om_id"  
			+" left outer join lo_address on lo_ad_id = lo_om_ad_id1"
			+" WHERE lo_ad_country = 'US' " +
			"  AND cp_pt_partner_type IS NOT NULL"
			+ " AND cp_partner.cp_pt_created_date > dateadd(dd,-"+days+",getdate()) "
			+" GROUP BY cp_pt_partner_type"
			+" HAVING cp_pt_partner_type='S' ";
	ResultSet rs18 = this.da.getResultSet(recruitingDomesticM);
	String DomesticCountM = "0";
	
		while(rs18.next()){
		String name = rs18.getString("Domestic");
		String[] strarr ={name};
		DomesticCountM=name;
		
	}
	
	String recruitingIntM="SELECT count(cp_partner_id) as Int"
			+" FROM cp_partner " 
			+" left outer join  lo_organization_main ON lo_om_id = cp_partner_om_id"  
			+" left outer join lo_address on lo_ad_id = lo_om_ad_id1"
			+" WHERE lo_ad_country <> 'US' " +
			"  AND cp_pt_partner_type IS NOT NULL"
			+ " AND cp_pt_created_date > dateadd(dd,-"+days+",getdate()) "
			+ " GROUP BY cp_pt_partner_type"
			+" HAVING cp_pt_partner_type='S' ";
	ResultSet rs19 = this.da.getResultSet(recruitingIntM);
	String InternationalM ="0";
	
		while(rs19.next()){
		String name = rs19.getString("Int");
		String[] strarr ={name};
		InternationalM=name;
		
	}
	
	// getting two section reports 
	
		// getting json  return from url 
		
		String s = "https://ccc.contingent.com/index.php?cmp=phone_ws&type=special_recruiting_efforts&period="+days;
	
		URL url = new URL(s);

			Scanner scan = new Scanner(url.openStream());
			String str = new String();
			while (scan.hasNext())
				str += scan.nextLine();
			scan.close();

			JSONObject object = new JSONObject(str);
			
			String[] jsonobj=new String[6];
			
			jsonobj[0]="Job Advertisement Activated";
			jsonobj[1]="Fail";
			jsonobj[2]="Final Contact";
			jsonobj[3]="First Contact";
			jsonobj[4]="Second Contact";
			jsonobj[5]="Success";
			
			 ArrayList<String[]> recruitedataarr = new ArrayList<String[]>();
			 String[] values= null;
			 String[] valuest= null;
			for(int i=0; i<jsonobj.length;i++){
		    JSONObject info = object.getJSONObject(jsonobj[i]);

		    Map<String,String> out = new HashMap<String, String>();
			
		    parse(info,out);
		    
		    values = new String[4];
			values[0] = jsonobj[i];
			values[1] = "0";
			values[2] = "0";
			values[3] = "0";
		    for ( String key : out.keySet() ) {
		    	
		    	
		    	if(key.equals("Anna Reeves")){
		    		values[1] = out.get(key);
		    	}else if(key.equals("Janessa Sambola")){
		    		values[2] = out.get(key);
		    	}else if(key.equals("Zulma Gunther")){
		    		values[3] = out.get(key);
		    	}
		    	
		    }
		    recruitedataarr.add(values);
		   
		    
	}	  
	
			 

	//		  get Second Section  of report
			
			
			String[] jsonobjsec=new String[5];
			jsonobjsec[0]="Backpage";
			jsonobjsec[1]="LinkedIn";
			jsonobjsec[2]="ZipRecruiter";
			jsonobjsec[3]="Craigslist";
			jsonobjsec[4]="Other";
			  ArrayList<String[]> recruitedataarrsec = new ArrayList<String[]>();
			  Map<String,String> out = new HashMap<String, String>();
			  boolean falg=false;
			for(int i=0; i<jsonobjsec.length;i++){
				JSONObject info = null  ;
				
				if(object.has(jsonobjsec[i]) && !object.isNull(jsonobjsec[i])){
					
				  info = object.getJSONObject(jsonobjsec[i]);	
				  parse(info,out);
				}
				
				
				
			// Array list  
			    valuest = new String[4];
			    valuest[0] = jsonobjsec[i];
			    valuest[1] = "0";
			    valuest[2] = "0";
			    valuest[3] = "0";
			 if(object.has(jsonobjsec[i]) && !object.isNull(jsonobjsec[i])){    
			    for ( String key : out.keySet() ) {
			    	if(key.equals("Anna Reeves")){
			    		valuest[1] = out.get(key);
			    	}else if(key.equals("Janessa Sambola")){
			    		valuest[2] = out.get(key);
			    	}else if(key.equals("Zulma Gunther")){
			    		valuest[3] = out.get(key);
			    	}
				   
			    	
			    }
			 }   
			    recruitedataarrsec.add(valuest);
     }		
	
	request.setAttribute("recruitedataarr", recruitedataarr);
	request.setAttribute("recruitedataarrsec", recruitedataarrsec);
		
	request.setAttribute("cpduserpartner",cpduserpartner);
	request.setAttribute("cpduser",cpduser);
	
	request.setAttribute("noc_lists", noc_lists);
	request.setAttribute("incident_lists", incident_lists);
	request.setAttribute("Domestic", DomesticCount);
	request.setAttribute("International", International);
	request.setAttribute("DomesticM", DomesticCountM);
	request.setAttribute("InternationalM", InternationalM);
	request.setAttribute("fileapproved", fileapproved);
	request.setAttribute("countbadgeIdList", countbadgeIdList); 
	request.setAttribute("Statuschange", Statuschange);
	request.setAttribute("StatusWork", StatusWork);
	request.setAttribute("StatusWorkinurance",StatusWorkinurance);
	request.setAttribute("StatusWorkresume",StatusWorkresume);

	String callsQueryIncoming 	= "SELECT count(*) as incoming,lo_pc_first_name + '  ' + lo_pc_last_name as cpdname "   
			+ " FROM  CiscoPhones.dbo.ContactCallDetail  "
			+ " INNER JOIN  CiscoPhones.dbo.virtual_sub_agentconnectiondetail vsa "
			+ " ON vsa.sessionid= CiscoPhones.dbo.ContactCallDetail.sessionid"
			+ " INNER JOIN CiscoPhones.dbo.resource res ON res.resourceid = vsa.resourceid "
			+ " JOIN ilex.dbo.lo_poc ON lo_pc_first_name = resourcefirstname "
			+ " AND lo_pc_last_name = resourcelastname"
			+ " JOIN ilex.dbo.lo_poc_rol ON lo_pr_pc_id = lo_pc_id"
			+ " AND contacttype=1 " 
			+ " AND lo_pr_pc_id IN (7270,35605,35737)" 
			+ " AND lo_pc_active_user <> 0" 
			+ " And enddatetime > dateadd(dd,-"+days+",getdate())"
			+ " GROUP BY lo_pc_first_name,lo_pc_last_name" ;
												
	
	
	String callsQueryoutgoing 					= "SELECT count(*) as outgoing,lo_pc_first_name + '  ' + lo_pc_last_name as cpdname "   
			+ " FROM  CiscoPhones.dbo.ContactCallDetail  "
			+ " INNER JOIN  CiscoPhones.dbo.virtual_sub_agentconnectiondetail vsa "
			+ " ON vsa.sessionid= CiscoPhones.dbo.ContactCallDetail.sessionid"
			+ " INNER JOIN CiscoPhones.dbo.resource res ON res.resourceid = vsa.resourceid "
			+ " JOIN ilex.dbo.lo_poc ON lo_pc_first_name = resourcefirstname "
			+ " AND lo_pc_last_name = resourcelastname"
			+ " JOIN ilex.dbo.lo_poc_rol ON lo_pr_pc_id = lo_pc_id"
			+ " AND contacttype=1 " 
			+ " AND lo_pr_pc_id IN (7270,35605,35737)" 
			+ " AND lo_pc_active_user <> 0 " 
			+ " AND  enddatetime > dateadd(dd,-"+days+",getdate())"
			+ " GROUP BY lo_pc_first_name,lo_pc_last_name" ;
	
	ResultSet rs8c = this.da.getResultSet(callsQueryIncoming);
	List<String> incoming = new ArrayList<String>();
	if(rs8c.next()){
	String pps = rs8c.getString("Incoming");
	String firstlastname=rs8c.getString("cpdname");
	String[] strarr ={pps+ ","+ firstlastname};
	incoming.add(pps+ ","+ firstlastname);
		
	}

	incoming_lists=compareusername(cpduser,incoming);
	
	request.setAttribute("incoming_lists",incoming_lists);
	
	ResultSet rs9c = this.da.getResultSet(callsQueryoutgoing);
	List<String> outgoing = new ArrayList<String>();
	if(rs9c.next()){
	String pps = rs9c.getString("outgoing");

	String firstlastname=rs9c.getString("cpdname");
	String[] strarr ={pps+ ","+ firstlastname};
	outgoing.add(pps+ ","+ firstlastname);
	
	}
	
	outgoing_lists=compareusername(cpduser,outgoing);
	
	request.setAttribute("outgoing_lists",outgoing_lists);

	return mapping.findForward(forward_key);
}
	
	public String getDayName(String dayNumber){
		Map<String,String> map = new HashMap<String,String>();
		map.put("1", "Sun");
		map.put("2", "Mon");
		map.put("3", "Tue");
		map.put("4", "Wed");
		map.put("5", "Thu");
		map.put("6", "Fri");
		map.put("7", "Sat");
		
		return map.get(dayNumber);
	}
	
	
//getting time in hours/minute/days
	public String calcHMS(int timeInSeconds) {
	      int hours, minutes;
	      hours = timeInSeconds / 3600;
	      timeInSeconds = timeInSeconds - (hours * 3600);
	      minutes = timeInSeconds / 60;
	      int day = (int)TimeUnit.SECONDS.toDays(timeInSeconds); 
	      if((day==0) &&  (hours==0)){
	    	return (minutes + " minute(s)"); 
	      }else if((day==0) &&  hours > 0){
	    	  return (hours + " hour(s) " + " " +  minutes + " minute(s)");  
	      }
	      else{ 
	      return (day + "day(s)" + " " + hours + " hour(s) " + " " +  minutes + " minute(s)");
	      }
	   }
	
	 public static String convertToCommaDelimited(String[] list) {
	        StringBuffer ret = new StringBuffer("");
	        for (int i = 0; list != null && i < list.length; i++) {
	            ret.append(list[i]);
	            if (i < list.length - 1) {
	            	
	                ret.append(',');
	            }
	        }
	        return ret.toString();
	    }
	
	public static List   compareusername(List<String> arrayA,List<String> arrayB)
	{
	//populate the arrays

	//loop through the contents of arrayA
	//and comparing each element to each element in arrayB
	//switch variable used to indicate whether a match was found
	String result = new String();
	List<String> functionret = new ArrayList<String>();
	boolean foundSwitch = false;
	String userinfo;
	String userinfo1;
	//outer loop for all the elements in arrayA[i]
	for(int i = 0; i < arrayA.size(); i++)
	{
	//inner loop for all the elements in arrayB[j]
	for (int j = 0; j < arrayB.size();j++)
	{
	//compare arrayA to arrayB and output results
		
	if(arrayA.get(i).contains(arrayB.get(j).split(",")[1]))
	{
		userinfo	=arrayB.get(j).split(",")[0];
		userinfo1=arrayB.get(j).split(",")[1];
		result=userinfo +","+ userinfo1;
		foundSwitch = true;
		break;
	}
	else{
		userinfo="0";
		userinfo1=arrayA.get(i);
		result=userinfo+","+ userinfo1;
		
	}
	}
	if (foundSwitch == false)
	{	
		userinfo="0";
		userinfo1=arrayA.get(i);
		result=userinfo+","+ userinfo1;
		
	}
	
	functionret.add(result);
	//set foundSwitch bool back to false
	}
	
	return functionret;
	}
	
	
	// getting values of 
	public static List   compareusernameweb(List<String> arrayA,List<String> arrayB)
	{
	//populate the arrays

	//loop through the contents of arrayA
	//and comparing each element to each element in arrayB
	//switch variable used to indicate whether a match was found
	//arrayA.set(index, element)	
	//arrayA.add("Janessa Sambola:1");
	String result = new String();
	List<String> functionret = new ArrayList<String>();
	boolean foundSwitch = false;
	String userinfo;
	String userinfo1;
	//outer loop for all the elements in arrayA[i]
	for(int i = 0; i < arrayA.size(); i++)
	{
	//inner loop for all the elements in arrayB[j]
	for (int j = 0; j < arrayB.size();j++)
	{
	//compare arrayA to arrayB and output results
	if(arrayA.get(i).contains(arrayB.get(j).split(":")[0].trim()))
	{
		userinfo	=arrayB.get(j).split(":")[0];
		userinfo1=arrayB.get(j).split(":")[1];
		if(userinfo1.contains("\n")){
			
			userinfo1 = userinfo1.replaceAll("\\r\\n", "");
		}
		result=userinfo1 +","+ userinfo;
		foundSwitch = true;
		break;
	}
	else{
		userinfo=arrayA.get(i);
		userinfo1="0";
		result =userinfo1+","+userinfo;
		
	}
	}
	if (foundSwitch == false)
	{	
		userinfo="0";
		userinfo1=arrayA.get(i);
		result= userinfo+","+userinfo1;
		
		
	}
	
	functionret.add(result);
	//set foundSwitch bool back to false
	}
	
	return functionret;
	}
	
	//getting data from web 
	public static String getDataFromWeb(String url, int timeout,
			String lineBreak) {
		try {
			URL u = new URL(url);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(timeout);
			c.setReadTimeout(timeout);
			c.connect();
			int status = c.getResponseCode();
			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(
						c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + lineBreak);
				}
				br.close();
				return sb.toString();
			}
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
// read map value 
	public static Map<String,String> parse(JSONObject json , Map<String,String> out) throws JSONException{
	    Iterator<String> keys = json.keys();
	    while(keys.hasNext()){
	        String key = keys.next();
	        String val = null;
	        try{
	             JSONObject value = json.getJSONObject(key);
	             parse(value,out);
	        }catch(Exception e){
	            val = json.getString(key);
	        }

	        if(val != null){
	            out.put(key,val);
	        }
	    }
	    return out;
	}
	
}