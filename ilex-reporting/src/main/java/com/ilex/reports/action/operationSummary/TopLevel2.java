package com.ilex.reports.action.operationSummary;

import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class TopLevel2 extends DispatchAction {

    int BDM = 0;
    int SPM = 1;
    int PERIOD = 2;

    int HI = 0;
    int LOW = 1;
    int BENCHMARK = 2;
    int SELECTED = 3;
    int TOTAL = 4;

    String[] DEFAULT_PARAMETERS = {"%", "%", ""};

    String SQL = "use datawarehouse; select sum(ac_os_pf_expense), sum(ac_os_expense), convert(int, sum(ac_os_revenue))/1000 as revenue, sum(ac_os_count), convert(decimal(3,2), case when (sum(ac_os_revenue) > 0.0) then 1-(sum(ac_os_pf_expense)/sum(ac_os_revenue)) else 0.0 end) as pf_vgpm, convert(decimal(3,2), case when (sum(ac_os_revenue) > 0.0) then 1-(sum(ac_os_expense)/sum(ac_os_revenue)) else 0.0 end) as vgpm from dbo.ac_fact2_ops_summary_main ";

    String WHERE = "where ac_os_day_index = 'period' and ac_os_bdm = '-bdm-' and ac_os_spm = '-spm-'";

    String SQL_INVOICE_DELAY = "select ac_os_bdm, ac_os_spm, ac_id_complete_delay, ac_id_closed_delay, ac_id_real_invoice_delay  from datawarehouse.dbo.ac_fact2_ops_summary_invoice_delays ";

    String SQL_OOB = "select ac_ob_oob_condition, ac_ob_count from datawarehouse.dbo.ac_fact2_ops_summary_oob ";
    String WHERE_OOB = "where ac_os_bdm = '-bdm-' and ac_os_spm = '-spm-'";

    String SQL_PO_TYPES = "select ac_op_type, ac_op_percentage from datawarehouse.dbo.ac_fact2_ops_summary_po ";

    DataAccess da = new DataAccess();

    NumberFormat currency = new DecimalFormat("#,###");
    NumberFormat percentage = NumberFormat.getPercentInstance();
    NumberFormat decimal_format = new DecimalFormat("#,###.0");
    NumberFormat vgpm = new DecimalFormat("0.00");

    private void actionsList(HttpServletRequest request, String selectDataFor,
            String mmId, String appendixId) {

        List<String[]> netMedXList = fillListWithMonths(
                new ArrayList<String[]>(), "NetMedX");
        List<String[]> everWorXList = fillListWithMonths(
                new ArrayList<String[]>(), "EverWorX");
        List<String[]> deploymentList = fillListWithMonths(
                new ArrayList<String[]>(), "Deployment");

        if (mmId.equals("") || mmId.equals("0")) {
            mmId = "%";
        }
        if (appendixId.equals("") || appendixId.equals("0")) {
            appendixId = "%";
        }

        String sql = "SELECT count(*) actions, count(distinct lm_js_id) totalJobs, CEILING(cast(count(*) as float)/count(distinct lm_js_id)) avg, MONTH(pr_ja_date_time_stamp)  month ,"
                + " DateName( month , DateAdd( month , MONTH(pr_ja_date_time_stamp) , -1 ) )  monthName , YEAR(pr_ja_date_time_stamp) Year, CASE "
                + " WHEN lx_pr_title LIKE '%NetMedX%' THEN 'NetMedX' "
                + " WHEN lx_pr_title LIKE '%EverWorX%' THEN 'EverWorX' "
                + " ELSE 'Deployment' END title   "
                + " FROM pr_job_actions_minute_rollup "
                + " LEFT JOIN lm_job ON lm_js_id = pr_ja_lm_id "
                + " LEFT JOIN lx_appendix_main ON lx_pr_id = lm_js_pr_id "
                + " WHERE pr_ja_date_time_stamp >= DateAdd(month, -11, getdate())  AND lx_pr_mm_id LIKE '"
                + mmId
                + "' AND lx_pr_id LIKE '"
                + appendixId
                + "' "
                + " GROUP BY CASE "
                + " WHEN lx_pr_title LIKE '%NetMedX%' THEN 'NetMedX' "
                + " WHEN lx_pr_title LIKE '%EverWorX%' THEN 'EverWorX'  "
                + " ELSE 'Deployment' END, MONTH(pr_ja_date_time_stamp),YEAR(pr_ja_date_time_stamp) "
                + " ORDER BY YEAR(pr_ja_date_time_stamp), MONTH(pr_ja_date_time_stamp)";

        ResultSet rs = this.da.getResultSet(sql);

        try {

            while (rs.next()) {
                if (rs.getString("title").equals("NetMedX")) {
                    updateStringArrayInList(netMedXList, rs.getString("avg"),
                            rs.getString("monthName"));
                } else if (rs.getString("title").equals("EverWorX")) {
                    updateStringArrayInList(everWorXList, rs.getString("avg"),
                            rs.getString("monthName"));
                } else if (rs.getString("title").equals("Deployment")) {
                    updateStringArrayInList(deploymentList,
                            rs.getString("avg"), rs.getString("monthName"));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + " --------------- " + sql);
        }

        request.setAttribute("netMedX" + selectDataFor + "ChartData",
                netMedXList);
        request.setAttribute("everWorx" + selectDataFor + "ChartData",
                everWorXList);
        request.setAttribute("deployment" + selectDataFor + "ChartData",
                deploymentList);

    }

    private void updateStringArrayInList(List<String[]> list, String avg,
            String monthName) {
        for (int i = 0; i < list.size(); i++) {
            String[] arr = list.get(i);
            if (arr[1].equals(monthName)) {
                arr[0] = avg;
                break;
            }
        }
    }

    private List<String[]> fillListWithMonths(List<String[]> list,
            String appendixType) {

        DateFormatSymbols dfs = new DateFormatSymbols();
        /*
         * Getting all 12 months, PLEASE NOTE: Month count is 0-11
         */
        String[] months = dfs.getMonths();

        Calendar cal = Calendar.getInstance();

        int currentYear = cal.get(cal.YEAR);
        /*
         * This also return months form 0-11
         */
        int currentMonth = cal.get(cal.MONTH);

        int oldMonthCount = 0;
        /* getting trailing 12 Months, */
        for (int i = 0; i < 12; i++) {
            /*
             * Adding currentMonth value to i to get last years month first to
             * maintain the order of the list.
             */
            if ((i + currentMonth) < 11) {
                /*
                 * months[i+currentMonth+1] adding 1 in the index to get next
                 * month of last year as per current month of this year. e-g if
                 * this is Year 2015 and Month is June then this formula will
                 * bring next month of last year i-e July 2014 and in this way
                 * calendar will end with June 2015
                 */
                list.add(new String[]{"0", months[i + currentMonth + 1],
                    currentYear - 1 + "", appendixType});
                /*
                 * Maintaining this count to start remaining year with the 1st
                 * month of current year.
                 */
                oldMonthCount++;
            } else {
                list.add(new String[]{"0", months[i - oldMonthCount],
                    currentYear + "", appendixType});
            }

        }

        return list;
    }

    public ActionForward getAppendixByCustomer(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        response.setHeader("Cache-control", "no-cache, no-store");

        String customer = "";

        if (request.getAttribute("mmId") != null) {
            customer = (String) request.getAttribute("mmId");
        } else if (request.getParameter("mmId") != null) {
            customer = (String) request.getParameter("mmId");
        }

        String sql = "SELECT DISTINCT lx_pr_id, lx_pr_title "
                + " FROM lx_appendix_main  "
                + " WHERE lx_pr_end_customer IS NOT NULL AND lx_pr_mm_id = '"
                + customer + "' " + " ORDER BY lx_pr_title  ";
        String optionList = "<option value=\"0\">--Select--</option>";
        ResultSet rs = this.da.getResultSet(sql);
        //
        try {

            while (rs.next()) {
                optionList += "<option value=\"" + rs.getString("lx_pr_id")
                        + "\">" + rs.getString("lx_pr_title") + "</option>";

            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + " --------------- ");
        }

        out.write(optionList);
        // out.print("This is call return...");

        return null;
    }

    public ActionForward dragCoefficient(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");

        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
            // expired
        }

        String forward_key = "dragCoefficientFwdkey";

        // getting all customers for drop down.
        String sql = "SELECT DISTINCT lx_pr_end_customer, lx_pr_mm_id "
                + " FROM lx_appendix_main  "
                + " WHERE lx_pr_end_customer IS NOT NULL ORDER  BY "
                + " lx_pr_end_customer ";
        List<String[]> customerList = new ArrayList<String[]>();

        ResultSet rs = this.da.getResultSet(sql);

        try {

            while (rs.next()) {
                customerList.add(new String[]{
                    rs.getString("lx_pr_end_customer"),
                    rs.getString("lx_pr_mm_id")});
            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + " --------------- ");
        }

        request.setAttribute("allCustomers", customerList);

        // Getting all appendixes of given customer.
        String customer = "";
        String appendix = "";

        String lastSelectedCustomer = "";
        String lastSelectedAppendix = "";

        // getting/setting value of current selected customer
        if (request.getAttribute("selectedCustomer") != null) {
            customer = (String) request.getAttribute("selectedCustomer");
        } else if (request.getParameter("selectedCustomer") != null) {
            customer = (String) request.getParameter("selectedCustomer");
        }
        request.setAttribute("selectedCustomer", customer);
        // getting/setting value of current selected appendix
        if (request.getAttribute("selectedAppendix") != null) {
            appendix = (String) request.getAttribute("selectedAppendix");
        } else if (request.getParameter("selectedAppendix") != null) {
            appendix = (String) request.getParameter("selectedAppendix");
        }
        request.setAttribute("selectedAppendix", appendix);

        // getting value of lastly selected customer
        if (request.getAttribute("lastSelectedCustomer") != null) {
            lastSelectedCustomer = (String) request
                    .getAttribute("lastSelectedCustomer");
        } else if (request.getParameter("lastSelectedCustomer") != null) {
            lastSelectedCustomer = (String) request
                    .getParameter("lastSelectedCustomer");
        }
        // getting value of lastly selected appendix
        if (request.getAttribute("lastSelectedAppendix") != null) {
            lastSelectedAppendix = (String) request
                    .getAttribute("lastSelectedAppendix");
        } else if (request.getParameter("lastSelectedAppendix") != null) {
            lastSelectedAppendix = (String) request
                    .getParameter("lastSelectedAppendix");
        }

        // getting value of lastly selected customer
        if (request.getAttribute("lastSelectedCustomer") != null) {
            lastSelectedCustomer = (String) request
                    .getAttribute("lastSelectedCustomer");
        } else if (request.getParameter("lastSelectedCustomer") != null) {
            lastSelectedCustomer = (String) request
                    .getParameter("lastSelectedCustomer");
        }
        // getting value of lastly selected appendix
        if (request.getAttribute("lastSelectedAppendix") != null) {
            lastSelectedAppendix = (String) request
                    .getAttribute("lastSelectedAppendix");
        } else if (request.getParameter("lastSelectedAppendix") != null) {
            lastSelectedAppendix = (String) request
                    .getParameter("lastSelectedAppendix");
        }

        // getting and setting values for customer name both current and last.
        String currentCustomerName = getCutomerNameById(customer);
        request.setAttribute("currentSelectedCustomerName", currentCustomerName);
        String lastCustomerName = getCutomerNameById(lastSelectedCustomer);
        request.setAttribute("lastSelectedCustomerName", lastCustomerName);

        List<String[]> appedixList = new ArrayList<String[]>();
        appedixList.add(new String[]{"--Select--", "0"});
        if (!"".equals(customer)) {
            // if customer is not empty then find the appendix.
            sql = "SELECT DISTINCT lx_pr_id, lx_pr_title "
                    + " FROM lx_appendix_main  "
                    + " WHERE lx_pr_end_customer IS NOT NULL AND lx_pr_mm_id = '"
                    + customer + "' " + " ORDER BY lx_pr_title  ";

            rs = this.da.getResultSet(sql);
            try {

                while (rs.next()) {
                    appedixList.add(new String[]{rs.getString("lx_pr_title"),
                        rs.getString("lx_pr_id")});

                }
            } catch (Exception e) {
                System.out.println(e.getMessage() + " --------------- ");
            }
        }
        request.setAttribute("appedixListing", appedixList);

        // getting listing for all customers
        actionsList(request, "AllCustomer", "%", "%");

        // previous customer setup ... for comparison between the customers.
        if (!"".equals(lastSelectedCustomer)
                && !"0".equals(lastSelectedCustomer)
                && !customer.equals(lastSelectedCustomer)) {
            actionsList(request, "OldCustomer", lastSelectedCustomer,
                    lastSelectedAppendix);

        }

        lastSelectedCustomer = customer;
        request.setAttribute("lastSelectedCustomer", lastSelectedCustomer);
        lastSelectedAppendix = appendix;
        request.setAttribute("lastSelectedAppendix", lastSelectedAppendix);

        // getting listing for given customer/appendix
        if (!"0".equals(customer) && !customer.equals("")) {

            actionsList(request, "CurrentCustomer", customer, appendix);
        }
        return mapping.findForward(forward_key);
    }

    private String getCutomerNameById(String mmId) {

        String sql = "SELECT DISTINCT isnull(lx_pr_end_customer, '') lx_pr_end_customer "
                + " FROM lx_appendix_main  "
                + " WHERE lx_pr_mm_id = '" + mmId + "' AND  lx_pr_end_customer IS NOT NULL  ";
        String customerName = "";

        ResultSet rs = this.da.getResultSet(sql);
        //
        try {

            if (rs.next()) {
                customerName = rs.getString("lx_pr_end_customer");

            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + " --------------- ");
        }
        return customerName;
    }

    public ActionForward db_summary(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String forward_key = "db_summary";

        String[] parameters = getParameters(request);

        getOpsSummaryMain2(request, parameters);

        getOpsSummaryMainTop10Revenue(request, parameters);
        getOpsSummaryMainTop10Count(request, parameters);

        getOpsSummaryInvoiceDelays(request, parameters);
        getOpsSummaryInvoiceDelaysAvg(request, parameters[this.PERIOD]);

        getOpsSummaryPOType(request, parameters);
        getOpsSummaryPOTypeAvg(request, parameters[this.PERIOD]);

        buildProductivityValues(request, parameters);

        oob2(request, parameters[this.SPM]);

        return mapping.findForward(forward_key);
    }

    String[] getParameters(HttpServletRequest request) {
        String[] p = new String[3];

        String bdm = request.getParameter("bdm");
        if ((bdm == null) || (bdm.equals(""))) {
            bdm = "%";
        }
        String spm = request.getParameter("spm");
        if ((spm == null) || (spm.equals(""))) {
            spm = "%";
        }
        String period = request.getParameter("period");
        if ((period == null) || (period.equals(""))) {
            period = "30";
        }

        p[this.BDM] = bdm;
        p[this.SPM] = spm;
        p[this.PERIOD] = period;

        request.setAttribute("bdm", bdm);
        request.setAttribute("spm", spm);
        request.setAttribute("period", period);

        return p;
    }

    void getOpsSummaryMain2(HttpServletRequest request, String[] p) {
        Map revenue = new HashMap();
        Map pf_cost = new HashMap();
        Map cost = new HashMap();
        Map<String, String> spm_map = new HashMap<String, String>();
        Vector spms = new Vector();

        Double[] legacy_vgp = {Double.valueOf(-1.0D),
            Double.valueOf(9999999.0D), Double.valueOf(0.0D),
            Double.valueOf(0.0D), Double.valueOf(0.0D)};
        Double[] legacy_pf_vgp = {Double.valueOf(-1.0D),
            Double.valueOf(9999999.0D), Double.valueOf(0.0D),
            Double.valueOf(0.0D), Double.valueOf(0.0D)};
        Double[] legacy_revenue = {Double.valueOf(-1.0D),
            Double.valueOf(9999999.0D), Double.valueOf(0.0D),
            Double.valueOf(0.0D), Double.valueOf(0.0D)};

        String sql = "select ac_os_spm, 'Legacy',        sum(ac_os_revenue) as revenue,        sum(ac_os_pf_expense) pf,        sum(ac_os_expense) actual   from datawarehouse.dbo.ac_fact2_ops_summary_main  where ac_os_day_index = 'period' and ac_os_bdm = '%' and ac_os_spm != '%'        and ac_os_spm not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave')  group by ac_os_spm";

        sql = updateSQL(sql, p[this.PERIOD]);
        System.out.println("getOpsSummaryMain2\n" + sql);

        ResultSet rs = this.da.getResultSet(sql);
        try {
            while (rs.next()) {
                spm_map.put(rs.getString("ac_os_spm"),
                        rs.getString("ac_os_spm"));
                revenue.put(rs.getString("ac_os_spm"),
                        Double.valueOf(rs.getDouble("revenue")));
                pf_cost.put(rs.getString("ac_os_spm"),
                        Double.valueOf(rs.getDouble("pf")));
                cost.put(rs.getString("ac_os_spm"),
                        Double.valueOf(rs.getDouble("Actual")));
            }

            for (Map.Entry spm : spm_map.entrySet()) {
                spms.add((String) spm.getKey());
            }

            rs.close();

            legacy_revenue = computeTotal(spms, p[this.SPM], revenue);

            if (p[this.SPM].equals("%")) {
                request.setAttribute("revenue_selected", "");
            } else {
                request.setAttribute("revenue_selected",
                        this.currency.format(legacy_revenue[this.SELECTED]
                                .doubleValue() / 1000.0D));
            }
            request.setAttribute("revenue_hi", this.currency
                    .format(legacy_revenue[this.HI].doubleValue() / 1000.0D));
            request.setAttribute("revenue_low", this.currency
                    .format(legacy_revenue[this.LOW].doubleValue() / 1000.0D));
            request.setAttribute("revenue_benchmark",
                    this.currency.format(legacy_revenue[this.BENCHMARK]
                            .doubleValue() / 1000.0D));
            request.setAttribute("revenue_total", this.currency
                    .format(legacy_revenue[this.TOTAL].doubleValue() / 1000.0D));

            legacy_pf_vgp = computeAverageVGP(spms, p[this.SPM], revenue,
                    pf_cost);

            if (p[this.SPM].equals("%")) {
                request.setAttribute("pf_vgpm_selected", "");
            } else {
                request.setAttribute("pf_vgpm_selected",
                        this.vgpm.format(legacy_pf_vgp[this.SELECTED]));
            }
            request.setAttribute("pf_vgpm_hi",
                    this.vgpm.format(legacy_pf_vgp[this.HI]));
            request.setAttribute("pf_vgpm_low",
                    this.vgpm.format(legacy_pf_vgp[this.LOW]));
            request.setAttribute("pf_vgpm_benchmark",
                    this.vgpm.format(legacy_pf_vgp[this.BENCHMARK]));
            request.setAttribute("pf_vgpm_total",
                    this.vgpm.format(legacy_pf_vgp[this.TOTAL]));

            legacy_vgp = computeAverageVGP(spms, p[this.SPM], revenue, cost);

            if (p[this.SPM].equals("%")) {
                request.setAttribute("vgpm_selected", "");
            } else {
                request.setAttribute("vgpm_selected",
                        this.vgpm.format(legacy_vgp[this.SELECTED]));
            }
            request.setAttribute("vgpm_hi",
                    this.vgpm.format(legacy_vgp[this.HI]));
            request.setAttribute("vgpm_low",
                    this.vgpm.format(legacy_vgp[this.LOW]));
            request.setAttribute("vgpm_benchmark",
                    this.vgpm.format(legacy_vgp[this.BENCHMARK]));
            request.setAttribute("vgpm_total",
                    this.vgpm.format(legacy_vgp[this.TOTAL]));

            getOpsSummaryMain2Temp(request);
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException1) {
            }
        }
        getOpsSummaryMain2Managed(request, p, legacy_revenue, revenue, pf_cost,
                cost, legacy_vgp);
    }

    void getOpsSummaryMain2Managed(HttpServletRequest request, String[] p,
            Double[] dv_legacy, Map<String, Double> revenue_legacy,
            Map<String, Double> pfc_legacy, Map<String, Double> ac_legacy,
            Double[] legacy_vgp) {
        Vector spms = new Vector();

        String sql = "select ac_os_day_index,        sum(ac_os_revenue) as revenue,        sum(ac_os_pf_expense) pf,        sum(ac_os_expense) actual   from datawarehouse.dbo.ac_fact2_ops_summary_main_managed  where ac_os_day_index = 'period' and ac_os_bdm = '%' and ac_os_spm != '%'  group by ac_os_day_index";

        sql = updateSQL(sql, p[this.PERIOD]);

        ResultSet rs = this.da.getResultSet(sql);
        try {
            if (rs.next()) {
                Double revenue = Double.valueOf(rs.getDouble("revenue"));
                Double pf_cost = Double.valueOf(rs.getDouble("pf"));
                Double cost = Double.valueOf(rs.getDouble("actual"));
                Double vgp;
                if (revenue.doubleValue() > 0.0D) {
                    vgp = Double.valueOf(1.0D - cost.doubleValue()
                            / revenue.doubleValue());
                } else {
                    vgp = Double.valueOf(0.0D);
                }

                request.setAttribute("mrevenue_selected", "");
                request.setAttribute("mrevenue_hi", "");
                request.setAttribute("mrevenue_low", "");
                request.setAttribute("mrevenue_benchmark", "");
                request.setAttribute("mrevenue_total",
                        this.currency.format(revenue.doubleValue() / 1000.0D));
                int tmp205_202 = this.TOTAL;
                Double[] tmp205_200 = dv_legacy;
                tmp205_200[tmp205_202] = Double.valueOf(tmp205_200[tmp205_202]
                        .doubleValue() + revenue.doubleValue());

                request.setAttribute("trevenue_selected", "");
                request.setAttribute("trevenue_hi", "");
                request.setAttribute("trevenue_low", "");
                request.setAttribute("trevenue_benchmark", "");
                request.setAttribute("trevenue_total", this.currency
                        .format(dv_legacy[this.TOTAL].doubleValue() / 1000.0D));

                request.setAttribute("mvgpm_selected", "");
                request.setAttribute("mvgpm_hi", "");
                request.setAttribute("mvgpm_low", "");
                request.setAttribute("mvgpm_benchmark", "");
                request.setAttribute("mvgpm_total", this.vgpm.format(vgp));

                Double total_ac_costs = Double.valueOf(0.0D);

                for (Map.Entry costs : ac_legacy.entrySet()) {
                    total_ac_costs = Double.valueOf(total_ac_costs
                            .doubleValue()
                            + ((Double) costs.getValue()).doubleValue());
                }
                total_ac_costs = Double.valueOf(total_ac_costs.doubleValue()
                        + cost.doubleValue());

                System.out.println("Cost " + total_ac_costs + " "
                        + dv_legacy[this.TOTAL]);
                if (dv_legacy[this.TOTAL].doubleValue() > 0.0D) {
                    vgp = Double.valueOf(1.0D - total_ac_costs.doubleValue()
                            / dv_legacy[this.TOTAL].doubleValue());
                } else {
                    vgp = Double.valueOf(0.0D);
                }

                request.setAttribute("Amvgpm_selected", "");
                request.setAttribute("Amvgpm_hi", "");
                request.setAttribute("Amvgpm_low", "");
                request.setAttribute("Amvgpm_benchmark", "");
                request.setAttribute("Amvgpm_total", this.vgpm.format(vgp));
            } else {
                if (p[this.SPM].equals("%")) {
                    request.setAttribute("mrevenue_selected", "");
                } else {
                    request.setAttribute("mrevenue_selected", "");
                }
                request.setAttribute("mrevenue_hi", "");
                request.setAttribute("mrevenue_low", "");
                request.setAttribute("mrevenue_benchmark", "");
                request.setAttribute("mrevenue_total", "");

                if (p[this.SPM].equals("%")) {
                    request.setAttribute("trevenue_selected", "");
                } else {
                    request.setAttribute("trevenue_selected",
                            this.currency.format(dv_legacy[this.SELECTED]
                                    .doubleValue() / 1000.0D));
                }
                request.setAttribute("trevenue_hi", this.currency
                        .format(dv_legacy[this.HI].doubleValue() / 1000.0D));
                request.setAttribute("trevenue_low", this.currency
                        .format(dv_legacy[this.LOW].doubleValue() / 1000.0D));
                request.setAttribute("trevenue_benchmark",
                        this.currency.format(dv_legacy[this.BENCHMARK]
                                .doubleValue() / 1000.0D));
                request.setAttribute("trevenue_total", this.currency
                        .format(dv_legacy[this.TOTAL].doubleValue() / 1000.0D));

                if (p[this.SPM].equals("%")) {
                    request.setAttribute("mvgpm_selected", "");
                } else {
                    request.setAttribute("mvgpm_selected", "");
                }
                request.setAttribute("mvgpm_hi", "");
                request.setAttribute("mvgpm_low", "");
                request.setAttribute("mvgpm_benchmark", "");
                request.setAttribute("mvgpm_total", "");

                if (p[this.SPM].equals("%")) {
                    request.setAttribute("Amvgpm_selected", "");
                } else {
                    request.setAttribute("Amvgpm_selected",
                            this.vgpm.format(legacy_vgp[this.SELECTED]));
                }
                request.setAttribute("Amvgpm_hi",
                        this.vgpm.format(legacy_vgp[this.HI]));
                request.setAttribute("Amvgpm_low",
                        this.vgpm.format(legacy_vgp[this.LOW]));
                request.setAttribute("Amvgpm_benchmark",
                        this.vgpm.format(legacy_vgp[this.BENCHMARK]));
                request.setAttribute("Amvgpm_total",
                        this.vgpm.format(legacy_vgp[this.TOTAL]));
            }

            rs.close();
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
    }

    void getOpsSummaryMain2Temp(HttpServletRequest request) {
        request.setAttribute("all_pf_vgpm_selected", "");
        request.setAttribute("all_pf_vgpm_hi", "");
        request.setAttribute("all_pf_vgpm_low", "");
        request.setAttribute("all_pf_vgpm_benchmark", "");
        request.setAttribute("all_pf_vgpm_total", "");

        request.setAttribute("all_vgpm_selected", "");
        request.setAttribute("all_vgpm_hi", "");
        request.setAttribute("all_vgpm_low", "");
        request.setAttribute("all_vgpm_benchmark", "");
        request.setAttribute("all_vgpm_total", "");

        request.setAttribute("msp_pf_vgpm_selected", "");
        request.setAttribute("msp_pf_vgpm_hi", "");
        request.setAttribute("msp_pf_vgpm_low", "");
        request.setAttribute("msp_pf_vgpm_benchmark", "");
        request.setAttribute("msp_pf_vgpm_total", "");

        request.setAttribute("msp_vgpm_selected", "");
        request.setAttribute("msp_vgpm_hi", "");
        request.setAttribute("msp_vgpm_low", "");
        request.setAttribute("msp_vgpm_benchmark", "");
        request.setAttribute("msp_vgpm_total", "");
    }

    Double[] computeAverageVGP(Vector<String> list, String exclude,
            Map<String, Double> revenue, Map<String, Double> cost) {
        Double[] display_values = {Double.valueOf(-1.0D),
            Double.valueOf(9999999.0D), Double.valueOf(0.0D),
            Double.valueOf(0.0D), Double.valueOf(0.0D)};

        Double x = Double.valueOf(0.0D);
        Double c = Double.valueOf(0.0D);
        Double r = Double.valueOf(0.0D);
        Double c_sum = Double.valueOf(0.0D);
        Double r_sum = Double.valueOf(0.0D);

        Double c_sum_total = Double.valueOf(0.0D);
        Double r_sum_total = Double.valueOf(0.0D);

        for (int i = 0; i < list.size(); i++) {
            c_sum_total = Double.valueOf(c_sum_total.doubleValue()
                    + ((Double) cost.get(list.get(i))).doubleValue());
            r_sum_total = Double.valueOf(r_sum_total.doubleValue()
                    + ((Double) revenue.get(list.get(i))).doubleValue());

            c = (Double) cost.get(list.get(i));
            r = (Double) revenue.get(list.get(i));

            if (!((String) list.get(i)).equals(exclude)) {
                c_sum = Double.valueOf(c_sum.doubleValue() + c.doubleValue());
                r_sum = Double.valueOf(r_sum.doubleValue() + r.doubleValue());
            }

            x = Double.valueOf(0.0D);

            if (r.doubleValue() > 0.0D) {
                x = Double.valueOf(1.0D - c.doubleValue() / r.doubleValue());
            }

            if (x.doubleValue() > display_values[this.HI].doubleValue()) {
                display_values[this.HI] = x;
            }

            if (x.doubleValue() < display_values[this.LOW].doubleValue()) {
                display_values[this.LOW] = x;
            }
        }

        if (r_sum.doubleValue() > 0.0D) {
            int tmp353_350 = this.BENCHMARK;
            Double[] tmp353_347 = display_values;
            tmp353_347[tmp353_350] = Double.valueOf(tmp353_347[tmp353_350]
                    .doubleValue()
                    + (1.0D - c_sum.doubleValue() / r_sum.doubleValue()));
        } else {
            display_values[this.BENCHMARK] = Double.valueOf(0.0D);
        }

        if (r_sum_total.doubleValue() > 0.0D) {
            int tmp406_403 = this.TOTAL;
            Double[] tmp406_400 = display_values;
            tmp406_400[tmp406_403] = Double.valueOf(tmp406_400[tmp406_403]
                    .doubleValue()
                    + (1.0D - c_sum_total.doubleValue()
                    / r_sum_total.doubleValue()));
        } else {
            display_values[this.TOTAL] = Double.valueOf(0.0D);
        }

        if (!exclude.equals("%")) {
            c = (Double) cost.get(exclude);
            r = (Double) revenue.get(exclude);

            if (r.doubleValue() > 0.0D) {
                display_values[this.SELECTED] = Double.valueOf(1.0D
                        - c.doubleValue() / r.doubleValue());
            }
        } else {
            display_values[this.SELECTED] = Double.valueOf(-1.0D);
        }

        return display_values;
    }

    Double[] computeAverage(Vector<String> list, String exclude,
            Map<String, Double> value1) {
        Double[] display_values = {Double.valueOf(-1.0D),
            Double.valueOf(9999999.0D), Double.valueOf(0.0D),
            Double.valueOf(0.0D), Double.valueOf(0.0D)};

        Double cnt = Double.valueOf(0.0D);

        Double x = Double.valueOf(0.0D);

        for (int i = 0; i < list.size(); i++) {
            x = (Double) value1.get(list.get(i));
            int tmp86_83 = this.TOTAL;
            Double[] tmp86_80 = display_values;
            tmp86_80[tmp86_83] = Double.valueOf(tmp86_80[tmp86_83]
                    .doubleValue()
                    + ((Double) value1.get(list.get(i))).doubleValue());

            if (!((String) list.get(i)).equals(exclude)) {
                int tmp136_133 = this.BENCHMARK;
                Double[] tmp136_130 = display_values;
                tmp136_130[tmp136_133] = Double.valueOf(tmp136_130[tmp136_133]
                        .doubleValue() + x.doubleValue());
                cnt = Double.valueOf(cnt.doubleValue() + 1.0D);
            }
        }

        if (x.doubleValue() > display_values[this.HI].doubleValue()) {
            display_values[this.HI] = x;
        }

        if (x.doubleValue() < display_values[this.LOW].doubleValue()) {
            display_values[this.LOW] = x;
        }

        display_values[this.BENCHMARK] = Double
                .valueOf(display_values[this.BENCHMARK].doubleValue()
                        / cnt.doubleValue());

        display_values[this.TOTAL] = Double.valueOf(display_values[this.TOTAL]
                .doubleValue() / list.size());

        if (!exclude.equals("%")) {
            display_values[this.SELECTED] = ((Double) value1.get(exclude));
        } else {
            display_values[this.SELECTED] = Double.valueOf(-1.0D);
        }

        return display_values;
    }

    Double[] computeTotal(Vector<String> list, String exclude,
            Map<String, Double> value1) {
        Double[] display_values = {Double.valueOf(-1.0D),
            Double.valueOf(9999999.0D), Double.valueOf(0.0D),
            Double.valueOf(0.0D), Double.valueOf(0.0D)};

        Double cnt = Double.valueOf(0.0D);

        Double x = Double.valueOf(0.0D);

        for (int i = 0; i < list.size(); i++) {
            x = (Double) value1.get(list.get(i));
            int tmp86_83 = this.TOTAL;
            Double[] tmp86_80 = display_values;
            tmp86_80[tmp86_83] = Double.valueOf(tmp86_80[tmp86_83]
                    .doubleValue() + x.doubleValue());

            if (!((String) list.get(i)).equals(exclude)) {
                int tmp123_120 = this.BENCHMARK;
                Double[] tmp123_117 = display_values;
                tmp123_117[tmp123_120] = Double.valueOf(tmp123_117[tmp123_120]
                        .doubleValue() + x.doubleValue());

                cnt = Double.valueOf(cnt.doubleValue() + 1.0D);
            }

            if (x.doubleValue() > display_values[this.HI].doubleValue()) {
                display_values[this.HI] = x;
            }

            if (x.doubleValue() < display_values[this.LOW].doubleValue()) {
                display_values[this.LOW] = x;
            }

        }

        if (!exclude.equals("%")) {
            display_values[this.SELECTED] = ((Double) value1.get(exclude));
        } else {
            display_values[this.SELECTED] = Double.valueOf(-1.0D);
        }

        display_values[this.BENCHMARK] = Double
                .valueOf(display_values[this.BENCHMARK].doubleValue()
                        / cnt.doubleValue());

        return display_values;
    }

    void getOpsSummaryMain(HttpServletRequest request, String[] p) {
        String sql = this.SQL + updateSQL(p);

        ResultSet rs = this.da.getResultSet(sql);
        try {
            if (rs.next()) {
                request.setAttribute("vgpm", getColumnValue(rs, "vgpm"));
                request.setAttribute("revenue",
                        getColumnValueCurrency(rs, "revenue"));
                request.setAttribute("pf_vgpm", getColumnValue(rs, "pf_vgpm"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
    }

    String getColumnValue(ResultSet rs, String column) {
        String column_value = "";
        try {
            column_value = rs.getString(column);
            if (rs.wasNull()) {
                column_value = "N/A";
            }
        } catch (SQLException e) {
            column_value = "E1";
        }

        return column_value;
    }

    String getColumnValueCurrency(ResultSet rs, String column) {
        String column_value = "";
        try {
            column_value = "$" + this.currency.format(rs.getInt(column));
            if (rs.wasNull()) {
                column_value = "N/A";
            }
        } catch (SQLException e) {
            column_value = "E1";
        }

        return column_value;
    }

    void getOpsSummaryMainAvg(HttpServletRequest request, String period) {
        String[] p = {"%", "%", ""};
        p[this.PERIOD] = period;

        String sql = this.SQL + updateSQL(p);

        ResultSet rs = this.da.getResultSet(sql);
        try {
            if (rs.next()) {
                request.setAttribute("avg_vgpm", rs.getString("vgpm"));
                request.setAttribute("avg_revenue", this.currency
                        .format(Integer.parseInt(rs.getString("revenue"))));
                request.setAttribute("avg_pf_vgpm", rs.getString("pf_vgpm"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
    }

    void getOpsSummaryMainTop10Revenue(HttpServletRequest request, String[] p) {
        String sql = "select top 10 left(ac_os_customer,25), convert(int, sum(ac_os_revenue)/1000)  from datawarehouse.dbo.ac_fact2_ops_summary_main ";

        sql = sql + updateSQL(p);

        sql = sql
                + " group by ac_os_customer  order by sum(ac_os_revenue) desc";

        String t = "";

        String row = "<td class=\"TBCNT004\" width=\"160\" style=\"text-indent:20px;\">-name-</td><td class=\"TBCNT004R\" width=\"75\">$-value-</td></tr>";
        String trow = "";
        String rows = "";

        ResultSet rs = this.da.getResultSet(sql);
        try {
            while (rs.next()) {
                t = rs.getString(1).replace(" ", "&nbsp;");
                t = t.replace("-", "&ndash;");
                trow = row.replace("-name-", t);
                trow = trow
                        .replace("-value-", this.currency.format(Integer
                                        .parseInt(rs.getString(2))));
                rows = rows + trow;
            }
            rs.close();

            request.setAttribute("revenue10", rows);
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
    }

    void getOpsSummaryMainTop10Count(HttpServletRequest request, String[] p) {
        String sql = "select top 10 left(ac_os_customer,25), convert(int, sum(ac_os_count))  from datawarehouse.dbo.ac_fact2_ops_summary_main ";

        sql = sql + updateSQL(p);

        sql = sql + " group by ac_os_customer  order by sum(ac_os_count) desc";

        String t = "";

        String row = "<td class=\"TBCNT004\" width=\"160\" style=\"text-indent:20px;\">-name-</td><td class=\"TBCNT004R\" width=\"75\">-value-</td></tr>";
        String trow = "";
        String rows = "";

        ResultSet rs = this.da.getResultSet(sql);
        try {
            while (rs.next()) {
                t = rs.getString(1).replace(" ", "&nbsp;");
                t = t.replace("-", "&ndash;");
                trow = row.replace("-name-", t);
                trow = trow
                        .replace("-value-", this.currency.format(Integer
                                        .parseInt(rs.getString(2))));
                rows = rows + trow;
            }
            rs.close();

            request.setAttribute("count10", rows);
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
    }

    void getOpsSummaryInvoiceDelays(HttpServletRequest request, String[] p) {
        String sql = this.SQL_INVOICE_DELAY + updateSQL(p);

        System.out.println(sql);

        ResultSet rs = this.da.getResultSet(sql);
        try {
            if (rs.next()) {
                request.setAttribute("complete_delay",
                        getColumnValue(rs, "ac_id_complete_delay"));
                request.setAttribute("closed_delay",
                        getColumnValue(rs, "ac_id_closed_delay"));
                request.setAttribute("invoice_delay",
                        getColumnValue(rs, "ac_id_real_invoice_delay"));
            } else {
                request.setAttribute("complete_delay", "N/A");
                request.setAttribute("closed_delay", "N/A");
                request.setAttribute("invoice_delay", "N/A");
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
    }

    void getOpsSummaryInvoiceDelaysAvg(HttpServletRequest request, String period) {
        String[] p = {"%", "%", ""};
        p[this.PERIOD] = period;

        String sql = this.SQL_INVOICE_DELAY + updateSQL(p);

        ResultSet rs = this.da.getResultSet(sql);
        try {
            if (rs.next()) {
                request.setAttribute("avg_complete_delay",
                        getColumnValue(rs, "ac_id_complete_delay"));
                request.setAttribute("avg_closed_delay",
                        getColumnValue(rs, "ac_id_closed_delay"));
                request.setAttribute("avg_invoice_delay",
                        getColumnValue(rs, "ac_id_real_invoice_delay"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
    }

    void getOpsSummaryPOType(HttpServletRequest request, String[] p) {
        String sql = this.SQL_PO_TYPES + updateSQL(p);

        ResultSet rs = this.da.getResultSet(sql);

        Map po_types = new HashMap();

        po_types.put("M", "0.0%");
        po_types.put("S", "0.0%");
        po_types.put("P", "0.0%");
        try {
            while (rs.next()) {
                po_types.put(rs.getString("ac_op_type"), this.percentage
                        .format(rs.getDouble("ac_op_percentage")));
            }

            rs.close();
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
        request.setAttribute("minuteman", po_types.get("M"));
        request.setAttribute("speedpay", po_types.get("S"));
        request.setAttribute("standard", po_types.get("P"));
    }

    void getOpsSummaryPOTypeAvg(HttpServletRequest request, String period) {
        String[] p = {"%", "%", ""};
        p[this.PERIOD] = period;

        String sql = this.SQL_PO_TYPES + updateSQL(p);

        ResultSet rs = this.da.getResultSet(sql);

        Map po_types = new HashMap();

        po_types.put("M", "0.0%");
        po_types.put("S", "0.0%");
        po_types.put("P", "0.0%");
        try {
            while (rs.next()) {
                po_types.put(rs.getString("ac_op_type"), this.percentage
                        .format(rs.getDouble("ac_op_percentage")));
            }

            rs.close();
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
        request.setAttribute("avg_minuteman", po_types.get("M"));
        request.setAttribute("avg_speedpay", po_types.get("S"));
        request.setAttribute("avg_standard", po_types.get("P"));
    }

    void buildProductivityValues(HttpServletRequest request, String[] p) {
        Map number_employees = new HashMap();
        Map revenue_employees = new HashMap();
        Map jobs_employees = new HashMap();
        Map revenue_job = new HashMap();

        String spm = "";

        String sql = "select ac_os_spm, ac_pm_head_count,                   ac_pm_job_count/ac_pm_head_count as ac_pm_job_count,                   ac_pm_productivity/1000.0 as ac_pm_productivity,                   ac_pm_revenue/ac_pm_job_count as rev_per_job from datawarehouse.dbo.ac_fact2_ops_summary_productivity_main  where ac_os_spm not in ('Shroder, Charles','Supinger, Bob')       and ac_os_day_index = "
                + p[this.PERIOD];

        ResultSet rs = this.da.getResultSet(sql);
        try {
            while (rs.next()) {
                spm = rs.getString("ac_os_spm");
                number_employees.put(spm,
                        Double.valueOf(rs.getDouble("ac_pm_head_count")));
                revenue_employees.put(spm,
                        Double.valueOf(rs.getDouble("ac_pm_productivity")));
                jobs_employees.put(spm,
                        Double.valueOf(rs.getDouble("ac_pm_job_count")));
                revenue_job.put(spm,
                        Double.valueOf(rs.getDouble("rev_per_job")));
            }

            rs.close();

            request.setAttribute("number_employees",
                    analyzeProductivity(number_employees, p[this.SPM], "Total"));
            request.setAttribute(
                    "revenue_employees",
                    analyzeProductivity(revenue_employees, p[this.SPM],
                            "Overall"));
            request.setAttribute("jobs_employees",
                    analyzeProductivity(jobs_employees, p[this.SPM], "Overall"));
            request.setAttribute("revenue_job",
                    analyzeProductivity(revenue_job, p[this.SPM], "Overall"));
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
    }

    Map<String, String> analyzeProductivity(Map<String, Double> values,
            String selected, String type) {
        Map display_mapping = new HashMap();

        int size = 0;

        Double high = Double.valueOf(0.0D);
        Double low = Double.valueOf(0.0D);
        Double total = Double.valueOf(0.0D);
        Double benchmark = Double.valueOf(0.0D);

        Double next_value = Double.valueOf(0.0D);

        total = (Double) values.get("%");
        values.remove("%");
        size = values.size();

        high = Double.valueOf(0.0D);
        low = total;

        for (Map.Entry value : values.entrySet()) {
            next_value = (Double) value.getValue();
            if (!selected.equals(value.getKey())) {
                benchmark = Double.valueOf(benchmark.doubleValue()
                        + next_value.doubleValue());
            }
            if (next_value.doubleValue() > high.doubleValue()) {
                high = next_value;
            }
            if (next_value.doubleValue() >= low.doubleValue()) {
                continue;
            }
            low = next_value;
        }

        if (!selected.equals("%")) {
            benchmark = Double.valueOf(benchmark.doubleValue() / (size - 1));
            display_mapping.put("selected",
                    this.decimal_format.format(values.get(selected)));
        } else {
            benchmark = Double.valueOf(benchmark.doubleValue() / size);
            display_mapping.put("selected", "");
        }

        display_mapping.put("high", this.decimal_format.format(high));
        display_mapping.put("low", this.decimal_format.format(low));
        display_mapping.put("total", this.decimal_format.format(total));

        if (selected.equals("%")) {
            if (type.equals("Total")) {
                display_mapping.put("benchmark",
                        this.decimal_format.format(benchmark));
            } else {
                display_mapping.put("benchmark",
                        this.decimal_format.format(total));
            }
        } else {
            display_mapping.put("benchmark",
                    this.decimal_format.format(benchmark));
        }

        return display_mapping;
    }

    Map getOpsSummaryOOB(HttpServletRequest request, String[] p) {
        Map oob = initializeOOB();

        String sql = this.SQL_OOB + updateSQL(this.WHERE_OOB, p);

        ResultSet rs = this.da.getResultSet(sql);
        try {
            while (rs.next()) {
                oob.put(rs.getString(1), rs.getString(2));
            }

            rs.close();
            oob = totalOOBConditions(oob);
        } catch (Exception e) {
            System.out.println(e + " " + sql);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
        return oob;
    }

    Map<String, String> totalOOBConditions(Map<String, String> oob) {
        int s1 = 0;

        s1 = Integer.parseInt((String) oob.get("To Be Scheduled"))
                + Integer.parseInt((String) oob.get("To Be Scheduled Over-1"));
        oob.put("To Be Scheduled Total", String.valueOf(s1));

        s1 = Integer.parseInt((String) oob.get("Scheduled"))
                + Integer.parseInt((String) oob.get("Scheduled Over-1"));
        oob.put("Scheduled Total", String.valueOf(s1));

        s1 = Integer.parseInt((String) oob.get("Confirmed"))
                + Integer.parseInt((String) oob.get("Onsite"))
                + Integer.parseInt((String) oob.get("Onsite Over-1"))
                + Integer.parseInt((String) oob.get("Offsite"))
                + Integer.parseInt((String) oob.get("Offsite Over-2"))
                + Integer.parseInt((String) oob.get("Offsite Over/Del-7"));
        oob.put("In Work", String.valueOf(s1));

        s1 = Integer.parseInt((String) oob.get("Complete"))
                + Integer.parseInt((String) oob.get("Complete Over-5"));
        oob.put("Complete Total", String.valueOf(s1));

        for (Map.Entry c : oob.entrySet()) {
            if (((String) c.getValue()).length() > 3) {
                s1 = Integer.parseInt((String) c.getValue());
                c.setValue(this.currency.format(s1));
            }
        }

        return oob;
    }

    Map<String, String> initializeOOB() {
        Map oob = new HashMap();

        ResultSet rs = this.da
                .getResultSet("select * from datawarehouse.dbo.ac_dimension_ops_summary_oob_status");
        try {
            while (rs.next()) {
                oob.put(rs.getString(1), "0");
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
        return oob;
    }

    String updateSQL(String[] p) {
        String sql = this.WHERE;

        sql = sql.replace("-bdm-", p[this.BDM]);
        sql = sql.replace("-spm-", p[this.SPM]);
        sql = sql.replace("period", p[this.PERIOD]);

        return sql;
    }

    String updateSQL(String where, String[] p) {
        String sql = where;

        sql = sql.replace("-bdm-", p[this.BDM]);
        sql = sql.replace("-spm-", p[this.SPM]);

        return sql;
    }

    String updateSQL(String sql, String p) {
        sql = sql.replace("period", p);

        return sql;
    }

    void oob2(HttpServletRequest request, String spm) {
        Vector oob_condition_list = initializeOOB2();

        Vector oob_spm_list = initializeSPM2();

        Map oob_list = initializeMapForOOB(oob_condition_list, oob_spm_list);

        oob_list = getActualOOBs(oob_list);

        computeStatusForOOB(request, oob_list, spm, oob_condition_list,
                oob_spm_list);
    }

    Vector<String> initializeOOB2() {
        Vector oob = new Vector();

        ResultSet rs = this.da
                .getResultSet("select * from datawarehouse.dbo.ac_dimension_ops_summary_oob_status");
        try {
            while (rs.next()) {
                oob.add(rs.getString(1));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
        return oob;
    }

    Vector<String> initializeSPM2() {
        Vector oob = new Vector();

        ResultSet rs = this.da
                .getResultSet("select distinct ac_os_spm from datawarehouse.dbo.ac_fact2_ops_summary_oob where ac_os_bdm = '%' and ac_os_spm != '%' and ac_os_spm not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave')");
        try {
            while (rs.next()) {
                oob.add(rs.getString(1));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
        return oob;
    }

    Map<String, Map<String, Integer>> initializeMapForOOB(
            Vector<String> condition_list, Vector<String> spm_list) {
        Map oob = new HashMap();

        Integer initial_condition = Integer.valueOf(0);

        for (int i = 0; i < condition_list.size(); i++) {
            Map counts = new HashMap();

            for (int j = 0; j < spm_list.size(); j++) {
                counts.put((String) spm_list.get(j), initial_condition);
            }

            oob.put((String) condition_list.get(i), counts);
        }

        return oob;
    }

    Map<String, Map<String, Integer>> getActualOOBs(
            Map<String, Map<String, Integer>> oob) {
        ResultSet rs = this.da
                .getResultSet("select ac_ob_oob_condition, ac_os_spm, ac_ob_count from datawarehouse.dbo.ac_fact2_ops_summary_oob where ac_os_bdm = '%' and ac_os_spm != '%' and ac_os_spm not in ('Shroder, Charles','Supinger, Bob', 'Epling, Dave')");
        try {
            while (rs.next()) {
                Map temp_count = (Map) oob.get(rs
                        .getString("ac_ob_oob_condition"));
                temp_count.put(rs.getString("ac_os_spm"),
                        Integer.valueOf(rs.getInt("ac_ob_count")));
                oob.put(rs.getString("ac_ob_oob_condition"), temp_count);
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
            try {
                rs.close();
            } catch (SQLException localSQLException) {
            }
        }
        return oob;
    }

    void computeStatusForOOB(HttpServletRequest request,
            Map<String, Map<String, Integer>> oob_list, String spm,
            Vector<String> condition_list, Vector<String> spm_list) {
        String condition = "";
        String target_spm = "";

        Map Benchmark = new HashMap();
        Map Benchmark2 = new HashMap();
        Map Hi = new HashMap();
        Map Low = new HashMap();
        Map Selected = new HashMap();
        Map Total = new HashMap();

        Integer value = Integer.valueOf(0);
        Integer sum1 = Integer.valueOf(0);
        Integer hi = Integer.valueOf(-1);
        Integer low = Integer.valueOf(999999999);
        Integer benchmark = Integer.valueOf(0);

        for (int i = 0; i < condition_list.size(); i++) {
            condition = (String) condition_list.get(i);

            value = Integer.valueOf(0);
            sum1 = Integer.valueOf(0);
            hi = Integer.valueOf(-1);
            low = Integer.valueOf(999999999);
            benchmark = Integer.valueOf(0);

            for (int j = 0; j < spm_list.size(); j++) {
                target_spm = (String) spm_list.get(j);

                value = (Integer) ((Map) oob_list.get(condition))
                        .get(target_spm);

                sum1 = Integer.valueOf(sum1.intValue() + value.intValue());

                if (!target_spm.equals(spm)) {
                    benchmark = Integer.valueOf(benchmark.intValue()
                            + value.intValue());
                }

                if (value.intValue() > hi.intValue()) {
                    hi = value;
                }

                if (value.intValue() < low.intValue()) {
                    low = value;
                }

                if (target_spm.equals(spm)) {
                    Selected.put(condition, String.valueOf(value));
                }
            }
            double d1;
            if (spm.equals("%")) {
                d1 = 1.0D * sum1.intValue() / spm_list.size();
                benchmark = sum1;
                Selected.put(condition, "");
            } else {
                d1 = 1.0D * benchmark.intValue() / (spm_list.size() - 1);
            }

            Benchmark.put(condition, this.decimal_format.format(d1));
            Benchmark2.put(condition, String.valueOf(benchmark));
            Hi.put(condition, String.valueOf(hi));
            Low.put(condition, String.valueOf(low));
            Total.put(condition, String.valueOf(sum1));
        }

        double d1 = Integer
                .parseInt((String) Benchmark2.get("To Be Scheduled"))
                + Integer.parseInt((String) Benchmark2
                        .get("To Be Scheduled Over-1"));
        d1 = spm.equals("%") ? d1 / spm_list.size() : d1
                / (spm_list.size() - 1);
        Benchmark.put("To Be Scheduled Total", this.decimal_format.format(d1));

        d1 = Integer.parseInt((String) Benchmark2.get("Scheduled"))
                + Integer.parseInt((String) Benchmark2.get("Scheduled Over-1"));
        d1 = spm.equals("%") ? d1 / spm_list.size() : d1
                / (spm_list.size() - 1);
        Benchmark.put("Scheduled Total", this.decimal_format.format(d1));

        d1 = Integer.parseInt((String) Benchmark2.get("Confirmed"))
                + Integer.parseInt((String) Benchmark2.get("Onsite"))
                + Integer.parseInt((String) Benchmark2.get("Onsite Over-1"))
                + Integer.parseInt((String) Benchmark2.get("Offsite"))
                + Integer.parseInt((String) Benchmark2.get("Offsite Over-2"))
                + Integer.parseInt((String) Benchmark2
                        .get("Offsite Over/Del-7"));
        d1 = spm.equals("%") ? d1 / spm_list.size() : d1
                / (spm_list.size() - 1);
        Benchmark.put("In Work", this.decimal_format.format(d1));

        d1 = Integer.parseInt((String) Benchmark2.get("Complete"))
                + Integer.parseInt((String) Benchmark2.get("Complete Over-5"));
        d1 = spm.equals("%") ? d1 / spm_list.size() : d1
                / (spm_list.size() - 1);
        Benchmark.put("Complete Total", this.decimal_format.format(d1));

        int s1 = Integer.parseInt((String) Total.get("To Be Scheduled"))
                + Integer
                .parseInt((String) Total.get("To Be Scheduled Over-1"));
        Total.put("To Be Scheduled Total", this.currency.format(s1));

        s1 = Integer.parseInt((String) Total.get("Scheduled"))
                + Integer.parseInt((String) Total.get("Scheduled Over-1"));
        Total.put("Scheduled Total", this.currency.format(s1));

        s1 = Integer.parseInt((String) Total.get("Confirmed"))
                + Integer.parseInt((String) Total.get("Onsite"))
                + Integer.parseInt((String) Total.get("Onsite Over-1"))
                + Integer.parseInt((String) Total.get("Offsite"))
                + Integer.parseInt((String) Total.get("Offsite Over-2"))
                + Integer.parseInt((String) Total.get("Offsite Over/Del-7"));
        Total.put("In Work", this.currency.format(s1));

        s1 = Integer.parseInt((String) Total.get("Complete"))
                + Integer.parseInt((String) Total.get("Complete Over-5"));
        Total.put("Complete Total", this.currency.format(s1));

        if (spm.equals("%")) {
            Selected.put("To Be Scheduled Total", "");
            Selected.put("Scheduled Total", "");
            Selected.put("In Work", "");
            Selected.put("Complete Total", "");
        } else {
            s1 = Integer.parseInt((String) Selected.get("To Be Scheduled"))
                    + Integer.parseInt((String) Selected
                            .get("To Be Scheduled Over-1"));
            Selected.put("To Be Scheduled Total", this.currency.format(s1));

            s1 = Integer.parseInt((String) Selected.get("Scheduled"))
                    + Integer.parseInt((String) Selected
                            .get("Scheduled Over-1"));
            Selected.put("Scheduled Total", this.currency.format(s1));

            s1 = Integer.parseInt((String) Selected.get("Confirmed"))
                    + Integer.parseInt((String) Selected.get("Onsite"))
                    + Integer.parseInt((String) Selected.get("Onsite Over-1"))
                    + Integer.parseInt((String) Selected.get("Offsite"))
                    + Integer.parseInt((String) Selected.get("Offsite Over-2"))
                    + Integer.parseInt((String) Selected
                            .get("Offsite Over/Del-7"));
            Selected.put("In Work", this.currency.format(s1));

            s1 = Integer.parseInt((String) Selected.get("Complete"))
                    + Integer
                    .parseInt((String) Selected.get("Complete Over-5"));
            Selected.put("Complete Total", this.currency.format(s1));
        }

        request.setAttribute("Selected", Selected);
        request.setAttribute("Benchmark", Benchmark);
        request.setAttribute("Hi", Hi);
        request.setAttribute("Low", Low);
        request.setAttribute("Total", Total);
    }
}
