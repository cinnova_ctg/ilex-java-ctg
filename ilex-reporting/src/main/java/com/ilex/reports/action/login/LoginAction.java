package com.ilex.reports.action.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Cookie;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.mind.masterSchedulerTask.MasterSchedulerTaskUtil;

public class LoginAction extends DispatchAction {

    public ActionForward doLoginAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession(true);

        String userId = request.getParameter("userid");
        String token = request.getParameter("token");
        if (userId != null) {
            session.setAttribute("userid", userId);
        }

        if (token != null) {
            session.setAttribute("token", token);
        }

        if (userId == null || userId == "" || token == null || token == "") {
            response.sendRedirect(request.getScheme() + "://"
                    + request.getServerName() + ":"
                    + request.getServerPort() + "/Ilex");
            return (mapping.findForward("SessionExpire")); // Check for session
        }

        /**
         * Code for Re-Authentication
         */
        String jsonString = MasterSchedulerTaskUtil.sendAPIRequestForLogin(userId, token);
        if (jsonString == "") {
            response.sendRedirect(request.getScheme() + "://"
                    + request.getServerName() + ":"
                    + request.getServerPort() + "/Ilex");
            return (mapping.findForward("SessionExpire")); // Check for session
        }

        return mapping.findForward("success");
    }

}
