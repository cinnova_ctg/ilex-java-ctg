package com.ilex.reports.action.rpt;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ilex.reports.action.operationSummary.DataAccess;

public class DashBoard1 extends DispatchAction {
	DataAccess da = new DataAccess();

	static String ChartImageTag1 = "<img src=\"http://chart.apis.google.com/chart?";
	static String[] Months = { "", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	static String[] MonthsReversed = { "", "Dec", "Nov", "Oct", "Sep", "Aug",
			"Jul", "Jun", "May", "Apr", "Mar", "Feb", "Jan" };

	String XAxisMonths = "";
	String XAxisYears = "";
	String XAxisWeeks = "";
	int[] MonthOrder = new int[12];
	int iFirstMonth;
	int iLastMonth;
	String sFirstMonth;
	String sLastMonth;
	int iFirstYear;
	int iLastYear;
	int iFirstWeek;
	int iLastWeek;
	Map Years = new HashMap();
	String[] YearList = new String[2];

	int[] PrimaryMonthOrder = new int[13];
	int[] PrimaryQtrOrder = new int[5];

	Vector InvoiceDelayClosed = new Vector();
	Vector InvoiceDelayComplete = new Vector();

	Vector MthRevenue = new Vector();
	Vector MthMargin = new Vector();
	Vector MthProForma = new Vector();
	Vector MthDelta = new Vector();
	Vector MthJobs = new Vector();

	String reference_type = "";
	String project_type = "";
	String reference = "";
	String reference_name = "";
	String where = "";
	boolean title_note = false;
	String JobOwner_SPM_reference = "";

	Double MaxRevenue = Double.valueOf(0.0D);
	HttpServletRequest Request;

	public ActionForward topLevelSummary(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}
		
		
		return mapping.findForward("success");
	}
	
	
	public ActionForward dash_board1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "db1";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		this.Request = request;

		buildTimeConstants();

		processViewRequest(this.Request);

		buildTitle();

		revertInputParameters();

		getBuildDropDowns();

		if (this.reference_type.equals("JobOwner")) {
			buildQuarterlyFinancials();
			buildWeeklyFinancials();
		} else {
			buildInvoicingDelays();
			buildQuarterlyFinancials();
			buildProductivity();
			buildWIP();
			getCurrentemployees();
			buildMMMSPUsage();
			buildQualityPartnerRatings();
			buildWeeklyFinancials();
		}

		return mapping.findForward(forward_key);
	}

	void buildTitle() {
		String title = "";

		if (this.reference_type.equals("All")) {
			if (this.project_type.equals("Project"))
				title = "Projects";
			else if (this.project_type.equals("Dispatch"))
				title = "Dispatch";
			else if (this.project_type.equals("MSP")) {
				title = "Not Available";
			}

		} else if (this.reference_type.equals("SPM"))
			title = "SPM";
		else if (this.reference_type.equals("BDM")) {
			title = "BDM";
		}

		this.Request.setAttribute("title", title);
	}

	void revertInputParameters() {
		String view = "";

		if (this.reference_type.equals("ALL")) {
			if (this.project_type.equals("Project"))
				view = "Project";
			else if (this.project_type.equals("Dispatch"))
				view = "Dispatch";
			else if (this.project_type.equals("MSP"))
				view = "MSP";
			else {
				view = "All";
			}

		} else if (this.reference_type.equals("SPM")) {
			this.Request.setAttribute("spm", "1");
			view = "0";
		} else if (this.reference_type.equals("BDM")) {
			this.Request.setAttribute("bdm", "1");
			view = "0";
		} else if (this.reference_type.equals("JobOwner")) {
			this.Request.setAttribute("jobowner", "1");
			view = "0";
		} else {
			view = "All";
		}

		this.Request.setAttribute("view", view);
	}

	void processViewRequest(HttpServletRequest request) {
		boolean view_ok = false;

		String view = request.getParameter("view");
		String spm = request.getParameter("spm");
		String bdm = request.getParameter("bdm");
		String jobowner = request.getParameter("jobowner");

		System.out.println(view + " | " + spm + " | " + bdm + " | " + jobowner);

		String w2 = "";

		this.title_note = false;

		if ((view != null) && (!view.equals("0"))) {
			this.project_type = view;
			this.reference_type = "ALL";
			this.reference = "0";
			this.JobOwner_SPM_reference = "0";
			this.title_note = true;

			w2 = "PocReferenceType = 'ALL' and ProjectType = '" + view + "'";
			this.where = (" where " + w2);
			view_ok = true;
		} else {
			if ((bdm != null) && (!bdm.equals("0"))) {
				this.reference_type = "BDM";
				this.reference = bdm;
				this.project_type = "";
				this.JobOwner_SPM_reference = "0";
				this.title_note = true;
				view_ok = true;
			} else if ((jobowner != null) && (!jobowner.equals("0"))) {
				this.reference_type = "JobOwner";
				this.reference = jobowner;
				this.project_type = "";
				this.JobOwner_SPM_reference = jobowner;
				this.title_note = true;
				view_ok = true;
			} else if ((spm != null) && (!spm.equals("0"))) {
				this.reference_type = "SPM";
				this.reference = spm;
				this.project_type = "";
				this.JobOwner_SPM_reference = "0";
				this.title_note = true;
				view_ok = true;
			}

			w2 = "PocReferenceType = '" + this.reference_type
					+ "' and PocReference = " + this.reference;
			this.where = (" where " + w2);
		}

		System.out.println(this.where);

		if (!view_ok) {
			this.reference_type = "All";
			this.project_type = "ALL";
			this.reference = "0";
			this.JobOwner_SPM_reference = "0";
			this.title_note = true;
			this.where = (" where PocReferenceType = 'All' and ProjectType = '"
					+ this.project_type + "'");
		}
	}

	private void buildInvoicingDelays() {
		String mmm = "";
		String complete = "";
		String closed = "";
		Double dcomplete = Double.valueOf(0.0D);
		Double dclosed = Double.valueOf(0.0D);
		int icomplete = 0;
		int iclosed = 0;
		String sComplete = "";
		String sClosed = "";
		String tablerows = "";
		String chart = "";
		String delimiter = ",";

		getInvoicingDelays();

		for (int i = 0; i < 12; i++) {
			if (i == 11) {
				delimiter = "";
			}

			mmm = Months[this.MonthOrder[i]];

			if (i < this.InvoiceDelayComplete.size()) {
				complete = ((Double) this.InvoiceDelayComplete
						.get(this.MonthOrder[i] - 1)).toString();
				if (complete.equals("-1.0")) {
					complete = "N/A";
				}
				dcomplete = Double.valueOf(((Double) this.InvoiceDelayComplete
						.get(this.MonthOrder[i] - 1)).doubleValue() * 10.0D);
				icomplete = dcomplete.intValue();
				sComplete = sComplete + String.valueOf(icomplete) + delimiter;
			} else {
				complete = "N/A";
				sComplete = sComplete + "-1,";
			}

			if (i < this.InvoiceDelayClosed.size()) {
				closed = ((Double) this.InvoiceDelayClosed
						.get(this.MonthOrder[i] - 1)).toString();
				if (closed.equals("-1.0")) {
					closed = "N/A";
				}
				dclosed = Double.valueOf(((Double) this.InvoiceDelayClosed
						.get(this.MonthOrder[i] - 1)).doubleValue() * 10.0D);
				iclosed = dclosed.intValue();
				sClosed = sClosed + String.valueOf(iclosed) + delimiter;
			} else {
				closed = "N/A";
				sClosed = sClosed + "-1,";
			}

			if (i == 0)
				tablerows = "<tr><td class=\"cccb\" style=\"padding-left:12px\">"
						+ mmm
						+ "</td><td class=\"cccb\">"
						+ complete
						+ "</td><td class=\"cccb\">"
						+ closed
						+ "</td></tr>\n"
						+ tablerows;
			else if (i == 11)
				tablerows = "<tr><td class=\"ccbb\" style=\"padding-left:12px;color: #5a5a5a;\">"
						+ mmm
						+ "</td><td class=\"ccbb\" style=\"color: #5a5a5a;\">"
						+ complete
						+ "</td><td class=\"ccbb\" style=\"color: #5a5a5a;\">"
						+ closed + "</td></tr>\n" + tablerows;
			else {
				tablerows = "<tr><td class=\"ccab\" style=\"padding-left:12px\">"
						+ mmm
						+ "</td><td class=\"ccab\">"
						+ complete
						+ "</td><td class=\"ccab\">"
						+ closed
						+ "</td></tr>\n"
						+ tablerows;
			}

		}

		this.Request.setAttribute("InvoiceDelaysTable", tablerows);

		chart = "<img src=\"http://chart.apis.google.com/chart?cht=lc&amp;chs=420x175&amp;chd=t:"
				+ sComplete
				+ "|"
				+ sClosed
				+ "|25,25&chds=0,100"
				+ "&amp;chxt=x,x,y&amp;chxl=0:|"
				+ this.XAxisMonths
				+ "|1:|"
				+ this.XAxisYears
				+ "|2:|0|1|2|3|4|5|6|7|8|9|10&amp;chco=5a5a5a,a9a8ad,f5533e&amp;chdl=Complete|Closed|Target(2.5)\"/>";

		this.Request.setAttribute("InvoiceDelaysChart", chart);
	}

	private void getInvoicingDelays() {
		String sql = "select YYYYMMKey, YYYYKey, MMKey-1 as MMKey, InvoiceDelayType, convert(decimal(6,1),InvoiceDelayDays) as InvoiceDelayDays from datawarehouse.dbo.DashBoardInvoicingDelays "
				+ this.where + " " + "order by InvoiceDelayType, YYYYMMKey";

		String InvoiceDelayType = "";

		this.InvoiceDelayClosed.clear();
		this.InvoiceDelayComplete.clear();

		for (int j = 0; j < 12; j++) {
			this.InvoiceDelayClosed.add(j, Double.valueOf(-1.0D));
			this.InvoiceDelayComplete.add(j, Double.valueOf(-1.0D));
		}

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				int mm_key = rs.getInt("MMKey");

				InvoiceDelayType = rs.getString("InvoiceDelayType");

				if ((InvoiceDelayType != null)
						&& (InvoiceDelayType.equals("Close")))
					this.InvoiceDelayClosed.set(mm_key,
							Double.valueOf(rs.getDouble("InvoiceDelayDays")));
				else if ((InvoiceDelayType != null)
						&& (InvoiceDelayType.equals("Complete"))) {
					this.InvoiceDelayComplete.set(mm_key,
							Double.valueOf(rs.getDouble("InvoiceDelayDays")));
				}
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	private void buildQuarterlyFinancials() {
		Vector data_set = new Vector(10);

		Double revenue = Double.valueOf(0.0D);
		Double margin = Double.valueOf(0.0D);
		Double proforma_margin = Double.valueOf(0.0D);

		String x_axis_month_list = "";

		String chart_revenue = "";
		String chart_margin = "";
		String chart_proforma = "";
		String axis_delimiter = "";

		String delimiter = "";

		Double max_revenue = Double.valueOf(0.0D);

		Integer itemp = Integer.valueOf(0);

		String QtrChartImage = "";

		NumberFormat nf1 = NumberFormat.getIntegerInstance();
		NumberFormat nf2 = new DecimalFormat("0.00");
		NumberFormat nf3 = new DecimalFormat("#0.00");

		max_revenue = getQuarterlyFinancials1(data_set);

		if (data_set.size() > 0) {
			for (int i = 0; i < data_set.size() - 1; i++) {
				Vector row = (Vector) data_set.get(i);

				revenue = (Double) row.get(0);
				margin = (Double) row.get(5);
				proforma_margin = (Double) row.get(4);
				int month_index = ((Integer) row.get(7)).intValue();

				margin = Double.valueOf(margin.doubleValue() * 100.0D);
				if (margin.compareTo(Double.valueOf(20.0D)) < 0)
					margin = Double.valueOf(-1.0D);
				else if (margin.compareTo(Double.valueOf(90.0D)) > 0) {
					margin = Double.valueOf(-1.0D);
				}
				itemp = Integer.valueOf(margin.intValue());
				chart_margin = itemp.toString() + delimiter + chart_margin;

				proforma_margin = Double
						.valueOf(proforma_margin.doubleValue() * 100.0D);
				if (proforma_margin.compareTo(Double.valueOf(20.0D)) < 0)
					proforma_margin = Double.valueOf(-1.0D);
				else if (proforma_margin.compareTo(Double.valueOf(90.0D)) > 0) {
					proforma_margin = Double.valueOf(-1.0D);
				}
				itemp = Integer.valueOf(proforma_margin.intValue());
				chart_proforma = itemp.toString() + delimiter + chart_proforma;

				revenue = Double.valueOf(revenue.doubleValue()
						/ max_revenue.doubleValue() * 100.0D);
				itemp = Integer.valueOf(revenue.intValue());
				chart_revenue = itemp.toString() + delimiter + chart_revenue;

				x_axis_month_list = Months[month_index] + axis_delimiter
						+ x_axis_month_list;
				delimiter = ",";
				axis_delimiter = "|";
			}

			Vector row = (Vector) data_set.lastElement();
			this.XAxisWeeks = ((String) row.lastElement());

			QtrChartImage = ChartImageTag1
					+ "cht=lc&amp;chs=420x200&amp;chd=t:"
					+ chart_margin
					+ "|"
					+ chart_proforma
					+ "|55,55,55,55&chds=20,80&amp;chxt=x,y&amp;"
					+ "chxl=0:|"
					+ x_axis_month_list
					+ "|1:|0.2|0.3|0.4|0.5|0.6|0.7|0.8&amp;"
					+ "chco=5a5a5a,a9a8ad,f5533e&amp;chdl=Actual|Pro%20Forma|Target"
					+ "\" />";

			this.Request.setAttribute("VGPMChart", QtrChartImage);

			delimiter = "";
			int incr_delta = 6;
			double incr = max_revenue.doubleValue() / incr_delta;
			String yaxis = "";
			Double converaion_factor = Double.valueOf(1000000.0D);
			String cf_abbreviation = "M";

			if (max_revenue.doubleValue() < 1000000.0D) {
				converaion_factor = Double.valueOf(1000.0D);
				cf_abbreviation = "K";
			} else if (max_revenue.doubleValue() < 10000.0D) {
				converaion_factor = Double.valueOf(1.0D);
				cf_abbreviation = "";
			}

			for (int k = 1; k < incr_delta; k++) {
				yaxis = yaxis
						+ delimiter
						+ nf3.format(incr * k / converaion_factor.doubleValue())
						+ cf_abbreviation;
				delimiter = "|";
			}

			yaxis = "0"
					+ cf_abbreviation
					+ delimiter
					+ yaxis
					+ delimiter
					+ nf3.format(max_revenue.doubleValue()
							/ converaion_factor.doubleValue())
					+ cf_abbreviation;

			QtrChartImage = ChartImageTag1
					+ "cht=lc&amp;chs=400x150&amp;chd=t:" + chart_revenue
					+ "&amp;chxt=x,y&amp;" + "chxl=0:|" + x_axis_month_list
					+ "|1:|" + yaxis + "&amp;" + "chco=5a5a5a" + "\" />";
			this.Request.setAttribute("RevenueChart", QtrChartImage);

			String r = "";
			String m = "";
			String p = "";
			String d = "";
			String j = "";
			String q = "";
			String tablerows = "";

			row = (Vector) data_set.get(0);

			long test_revenue = ((Double) row.get(0)).longValue();
			Double test_margin = (Double) row.get(5);

			q = Months[((Integer) row.get(7)).intValue()];

			if ((test_margin.compareTo(Double.valueOf(0.1D)) < 0)
					|| (test_revenue < 1L)) {
				r = "0";
				m = "N/A";
				p = "N/A";
				d = "N/A";
				j = nf1.format(((Integer) row.get(3)).longValue());
			} else {
				r = nf1.format(((Double) row.get(0)).longValue());
				m = nf2.format((Double) row.get(5));
				p = nf2.format((Double) row.get(4));
				d = nf2.format((Double) row.get(6));
				j = nf1.format(((Integer) row.get(3)).longValue());
			}

			tablerows = "<tr><td class=\"ccaa\" style=\"padding-left:12px;border-top: 1px solid #BFBEC4;color: #5a5a5a;\">"
					+ q
					+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">$"
					+ r
					+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
					+ m
					+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
					+ p
					+ "&nbsp;</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
					+ d
					+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
					+ j + "</td></tr>\n";

			for (int i = 1; i < data_set.size() - 1; i++) {
				row = (Vector) data_set.get(i);

				q = Months[((Integer) row.get(7)).intValue()];
				r = nf1.format(((Double) row.get(0)).longValue());
				m = nf2.format((Double) row.get(5));
				p = nf2.format((Double) row.get(4));
				d = nf2.format((Double) row.get(6));
				j = nf1.format(((Integer) row.get(3)).longValue());

				tablerows = tablerows
						+ "<tr><td class=\"ccaa\" style=\"padding-left:12px\">"
						+ q + "</td><td class=\"ccac\">$" + r
						+ "</td><td class=\"ccac\">" + m
						+ "</td><td class=\"ccac\">" + p
						+ "&nbsp;</td><td class=\"ccac\">" + d
						+ "</td><td class=\"ccac\">" + j + "</td></tr>\n";
			}

			row = (Vector) data_set.get(data_set.size() - 1);

			test_revenue = ((Double) row.get(0)).longValue();
			test_margin = (Double) row.get(5);

			if ((test_margin.compareTo(Double.valueOf(0.1D)) < 0)
					|| (test_revenue < 1L)) {
				r = "0";
				m = "N/A";
				p = "N/A";
				d = "N/A";
				j = nf1.format(((Integer) row.get(3)).longValue());
			} else {
				r = nf1.format(((Double) row.get(0)).longValue());
				m = nf2.format((Double) row.get(5));
				p = nf2.format((Double) row.get(4));
				d = nf2.format((Double) row.get(6));
				j = nf1.format(((Integer) row.get(3)).longValue());
			}

			tablerows = tablerows
					+ "<tr><td class=\"rh1a\" style=\"padding-left:12px;border-bottom: 1px solid #BFBEC4; color: #5a5a5a;\">Total</td><td class=\"cccc\" style=\"color: #5a5a5a;\">$"
					+ r
					+ "</td><td class=\"cccc\" style=\"color: #5a5a5a;\">"
					+ m
					+ "</td><td class=\"cccc\" style=\"color: #5a5a5a;\">"
					+ p
					+ "&nbsp;</td><td class=\"cccc\" style=\"color: #5a5a5a;\">"
					+ d + "</td><td class=\"cccc\" style=\"color: #5a5a5a;\">"
					+ j + "</td></tr>\n";
			this.Request.setAttribute("VGPMTable", tablerows);
		}
	}

	private void buildQuarterlyFinancials1() {
		String revenue = "";
		String revenue_chart = "";

		String margin = "";
		String proforma = "";
		String delimiter = "";
		String tablerows = "";

		String QtrChartImage = "";

		NumberFormat nf1 = NumberFormat.getIntegerInstance();
		NumberFormat nf2 = new DecimalFormat("0.00");
		NumberFormat nf3 = new DecimalFormat("#0.00");

		getQuarterlyFinancials();

		for (int i = 11; i >= 0; i--) {
			Double dQtrMargin = Double.valueOf(((Double) this.MthMargin.get(i))
					.doubleValue() * 100.0D);
			if (dQtrMargin.compareTo(Double.valueOf(20.0D)) < 0) {
				dQtrMargin = Double.valueOf(-1.0D);
			}
			Integer iQtrMargin = Integer.valueOf(dQtrMargin.intValue());
			margin = margin + delimiter + iQtrMargin.toString();

			dQtrMargin = Double.valueOf(((Double) this.MthProForma.get(i))
					.doubleValue() * 100.0D);
			iQtrMargin = Integer.valueOf(dQtrMargin.intValue());
			proforma = proforma + delimiter + iQtrMargin.toString();

			Double dMthRevenue = Double.valueOf(((Double) this.MthRevenue
					.get(i)).doubleValue()
					/ this.MaxRevenue.doubleValue()
					* 100.0D);
			Integer iMthRevenue = Integer.valueOf(dMthRevenue.intValue());
			revenue = revenue + delimiter + iMthRevenue.toString();

			delimiter = ",";
		}

		QtrChartImage = ChartImageTag1
				+ "cht=lc&amp;chs=420x200&amp;chd=t:"
				+ margin
				+ "|"
				+ proforma
				+ "|55,55,55,55&chds=20,80&amp;chxt=x,y,x&amp;"
				+ "chxl=0:|"
				+ this.XAxisMonths
				+ "|1:|0.2|0.3|0.4|0.5|0.6|0.7|0.8|2:|"
				+ this.XAxisYears
				+ "&amp;"
				+ "chco=5a5a5a,a9a8ad,f5533e&amp;chdl=Actual|Pro%20Forma|Target"
				+ "\" />";

		this.Request.setAttribute("VGPMChart", QtrChartImage);

		delimiter = "";
		int incr_delta = 6;
		double incr = this.MaxRevenue.doubleValue() / incr_delta;
		String yaxis = "";

		for (int k = 1; k < incr_delta; k++) {
			yaxis = yaxis + delimiter + nf3.format(incr * k / 1000000.0D) + "M";
			delimiter = "|";
		}

		double high_rang = this.MaxRevenue.doubleValue() / 1000000.0D;
		yaxis = "0K" + delimiter + yaxis + delimiter + nf3.format(high_rang)
				+ "M";

		revenue_chart = ChartImageTag1 + "cht=lc&amp;chs=420x150&amp;chd=t:"
				+ revenue + "&amp;chxt=x,y,x&amp;" + "chxl=0:|"
				+ this.XAxisMonths + "|1:|" + yaxis + "|2:|" + this.XAxisYears
				+ "&amp;" + "chco=5a5a5a&amp;chdl=Monthly%20Revenue" + "\" />";
		this.Request.setAttribute("RevenueChart", revenue_chart);

		String r = "";
		String m = "";
		String p = "";
		String d = "";
		String j = "";

		long test_revenue = ((Double) this.MthRevenue.get(0)).longValue();
		Double test_margin = (Double) this.MthMargin.get(0);

		String q = MonthsReversed[this.MonthOrder[0]];

		if ((test_margin.compareTo(Double.valueOf(0.1D)) < 0)
				|| (test_revenue < 1L)) {
			r = "0";
			m = "N/A";
			p = "N/A";
			d = "N/A";
			j = nf1.format(((Integer) this.MthJobs.get(0)).longValue());
		} else {
			r = nf1.format(((Double) this.MthRevenue.get(0)).longValue());
			m = nf2.format((Double) this.MthMargin.get(0));
			p = nf2.format((Double) this.MthProForma.get(0));
			d = nf2.format(((Double) this.MthMargin.get(0)).doubleValue()
					- ((Double) this.MthProForma.get(0)).doubleValue());
			j = nf1.format(((Integer) this.MthJobs.get(0)).longValue());
		}

		tablerows = "<tr><td class=\"ccaa\" style=\"padding-left:12px;border-top: 1px solid #BFBEC4;color: #5a5a5a;\">"
				+ q
				+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">$"
				+ r
				+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
				+ m
				+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
				+ p
				+ "&nbsp;</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
				+ d
				+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
				+ j
				+ "</td></tr>\n";

		for (int i = 1; i < 12; i++) {
			q = MonthsReversed[this.MonthOrder[i]];
			r = nf1.format(((Double) this.MthRevenue.get(i)).longValue());
			m = nf2.format((Double) this.MthMargin.get(i));
			p = nf2.format((Double) this.MthProForma.get(i));
			d = nf2.format(((Double) this.MthMargin.get(i)).doubleValue()
					- ((Double) this.MthProForma.get(i)).doubleValue());
			j = nf1.format(((Integer) this.MthJobs.get(i)).longValue());

			tablerows = tablerows
					+ "<tr><td class=\"ccaa\" style=\"padding-left:12px\">" + q
					+ "</td><td class=\"ccac\">$" + r
					+ "</td><td class=\"ccac\">" + m
					+ "</td><td class=\"ccac\">" + p
					+ "&nbsp;</td><td class=\"ccac\">" + d
					+ "</td><td class=\"ccac\">" + j + "</td></tr>\n";
		}

		test_margin = (Double) this.MthMargin.get(12);
		if (test_margin.compareTo(Double.valueOf(0.2D)) < 0)
			m = "Invalid";
		else {
			m = nf2.format((Double) this.MthMargin.get(12));
		}
		r = nf1.format(((Double) this.MthRevenue.get(12)).longValue());

		p = nf2.format((Double) this.MthProForma.get(12));
		d = nf2.format(((Double) this.MthMargin.get(12)).doubleValue()
				- ((Double) this.MthProForma.get(12)).doubleValue());
		j = nf1.format(((Integer) this.MthJobs.get(12)).longValue());

		tablerows = tablerows
				+ "<tr><td class=\"rh1a\" style=\"padding-left:12px;border-bottom: 1px solid #BFBEC4; color: #5a5a5a;\">Total</td><td class=\"cccc\" style=\"color: #5a5a5a;\">$"
				+ r + "</td><td class=\"cccc\" style=\"color: #5a5a5a;\">" + m
				+ "</td><td class=\"cccc\" style=\"color: #5a5a5a;\">" + p
				+ "&nbsp;</td><td class=\"cccc\" style=\"color: #5a5a5a;\">"
				+ d + "</td><td class=\"cccc\" style=\"color: #5a5a5a;\">" + j
				+ "</td></tr>\n";
		this.Request.setAttribute("VGPMTable", tablerows);
	}

	private void getQuarterlyFinancials() {
		String sql = "select YYYY, MMKey, EstimatedCost, ActualCost, Revenue, TotalJobs from datawarehouse.dbo.DashboardQuarterlyFinancialSummary "
				+ this.where + " " + "order by YYYY desc, MMKey desc";

		Double tRevenue = Double.valueOf(0.0D);
		Double tEC = Double.valueOf(0.0D);
		Double tAC = Double.valueOf(0.0D);
		Integer tJobs = Integer.valueOf(0);

		this.MthRevenue.clear();
		this.MthMargin.clear();
		this.MthProForma.clear();
		this.MthDelta.clear();
		this.MthJobs.clear();

		for (int i = 0; i < 13; i++) {
			this.MthRevenue.add(i, Double.valueOf(0.0D));
			this.MthMargin.add(i, Double.valueOf(0.0D));
			this.MthProForma.add(i, Double.valueOf(0.0D));
			this.MthDelta.add(i, Double.valueOf(0.0D));
			this.MthJobs.add(i, Integer.valueOf(0));
		}

		this.MaxRevenue = Double.valueOf(0.0D);

		int i = 0;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Double dRevenue = Double.valueOf(rs.getDouble("Revenue"));
				Double dEC = Double.valueOf(rs.getDouble("EstimatedCost"));
				Double dAC = Double.valueOf(rs.getDouble("ActualCost"));
				Integer iJobs = Integer.valueOf(rs.getInt("TotalJobs"));

				if (dRevenue.compareTo(this.MaxRevenue) > 0) {
					this.MaxRevenue = dRevenue;
				}

				Double dProForma = Double.valueOf(1.0D - dEC.doubleValue()
						/ dRevenue.doubleValue());
				Double dMargin = Double.valueOf(1.0D - dAC.doubleValue()
						/ dRevenue.doubleValue());
				Double dDelta = Double.valueOf(dMargin.doubleValue()
						- dProForma.doubleValue());

				tRevenue = Double.valueOf(tRevenue.doubleValue()
						+ dRevenue.doubleValue());
				tEC = Double.valueOf(tEC.doubleValue() + dEC.doubleValue());
				tAC = Double.valueOf(tAC.doubleValue() + dAC.doubleValue());
				tJobs = Integer.valueOf(tJobs.intValue() + iJobs.intValue());

				this.MthRevenue.remove(i);
				this.MthMargin.remove(i);
				this.MthProForma.remove(i);
				this.MthDelta.remove(i);
				this.MthJobs.remove(i);

				this.MthRevenue.add(i, dRevenue);
				this.MthMargin.add(i, dMargin);
				this.MthProForma.add(i, dProForma);
				this.MthDelta.add(i, dDelta);
				this.MthJobs.add(i, iJobs);

				i++;
			}
			Double dDelta;
			Double dProForma;
			Double dMargin;
			if (tRevenue.equals(Double.valueOf(0.0D))) {
				dProForma = Double.valueOf(0.0D);
				dMargin = Double.valueOf(0.0D);
				dDelta = Double.valueOf(0.0D);
			} else {
				dProForma = Double.valueOf(1.0D - tEC.doubleValue()
						/ tRevenue.doubleValue());
				dMargin = Double.valueOf(1.0D - tAC.doubleValue()
						/ tRevenue.doubleValue());
				dDelta = Double.valueOf(dMargin.doubleValue()
						- dProForma.doubleValue());
			}

			i = 12;

			this.MthRevenue.remove(i);
			this.MthMargin.remove(i);
			this.MthProForma.remove(i);
			this.MthDelta.remove(i);
			this.MthJobs.remove(i);

			this.MthRevenue.add(i, tRevenue);
			this.MthMargin.add(i, dMargin);
			this.MthProForma.add(i, dProForma);
			this.MthDelta.add(i, dDelta);
			this.MthJobs.add(i, tJobs);

			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException1) {
			}
		}
	}

	private Double getQuarterlyFinancials1(Vector data_set) {
		String sql = "";

		if (this.reference_type.equals("JobOwner"))
			sql = "select YYYY, MMKey,      isnull(EstimatedCost, 0.0) as EstimatedCost,      isnull(ActualCost, 0.0) as ActualCost,      isnull(Revenue, 0.0) as Revenue,      isnull(TotalJobs, 0) as TotalJobs from datawarehouse.dbo.DashboardQuarterlyFinancialSummary      join  datawarehouse.dbo.DashboardSPMtoJobOwner on PocReference = JobOwner and SPMReference = SPM  and YYYYMMKey = yyyymmdd "
					+ this.where
					+ " and SPMReference = "
					+ this.JobOwner_SPM_reference
					+ " "
					+ "order by YYYY desc, MMKey desc";
		else {
			sql = "select YYYY, MMKey,      isnull(EstimatedCost, 0.0) as EstimatedCost,      isnull(ActualCost, 0.0) as ActualCost,      isnull(Revenue, 0.0) as Revenue,      isnull(TotalJobs, 0) as TotalJobs  from datawarehouse.dbo.DashboardQuarterlyFinancialSummary "
					+ this.where + " " + " order by YYYY desc, MMKey desc";
		}

		Double revenue = Double.valueOf(0.0D);
		Double estimated_cost = Double.valueOf(0.0D);
		Double proforma_margin = Double.valueOf(0.0D);
		Double margin = Double.valueOf(0.0D);
		Double actual_cost = Double.valueOf(0.0D);
		Double delta_margin = Double.valueOf(0.0D);

		Double total_revenue = Double.valueOf(0.0D);
		Double total_estimated_cost = Double.valueOf(0.0D);
		Double total_actual_cost = Double.valueOf(0.0D);
		Double max_revenue = Double.valueOf(0.0D);

		Integer total_job_count = Integer.valueOf(0);

		String year_index = "";
		String delimiter = "";

		int i = 0;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Vector row = new Vector(12);

				revenue = Double.valueOf(rs.getDouble("Revenue"));
				if (revenue.compareTo(max_revenue) > 0) {
					max_revenue = revenue;
				}
				estimated_cost = Double.valueOf(rs.getDouble("EstimatedCost"));
				actual_cost = Double.valueOf(rs.getDouble("ActualCost"));
				Integer job_count = Integer.valueOf(rs.getInt("TotalJobs"));

				total_revenue = Double.valueOf(total_revenue.doubleValue()
						+ revenue.doubleValue());
				total_estimated_cost = Double.valueOf(total_estimated_cost
						.doubleValue() + estimated_cost.doubleValue());
				total_actual_cost = Double.valueOf(total_actual_cost
						.doubleValue() + actual_cost.doubleValue());
				total_job_count = Integer.valueOf(total_job_count.intValue()
						+ job_count.intValue());

				if (revenue.doubleValue() > 0.0D) {
					proforma_margin = Double.valueOf(1.0D
							- estimated_cost.doubleValue()
							/ revenue.doubleValue());
					margin = Double.valueOf(1.0D - actual_cost.doubleValue()
							/ revenue.doubleValue());
					delta_margin = Double.valueOf(margin.doubleValue()
							- proforma_margin.doubleValue());
				} else {
					proforma_margin = Double.valueOf(0.0D);
					margin = Double.valueOf(0.0D);
					delta_margin = Double.valueOf(0.0D);
				}

				if (year_index.equals(rs.getString("YYYY"))) {
					year_index = year_index + delimiter + rs.getString("YYYY");
					delimiter = "|";
				}

				row.add(0, revenue);
				row.add(1, estimated_cost);
				row.add(2, actual_cost);
				row.add(3, job_count);
				row.add(4, proforma_margin);
				row.add(5, margin);
				row.add(6, delta_margin);
				row.add(7, Integer.valueOf(rs.getInt("MMKey")));

				data_set.add(row);
			}

			if (data_set.size() > 0) {
				revenue = total_revenue;
				estimated_cost = total_estimated_cost;
				actual_cost = total_actual_cost;
				Integer job_count = total_job_count;

				if (revenue.doubleValue() > 0.0D) {
					proforma_margin = Double.valueOf(1.0D
							- estimated_cost.doubleValue()
							/ revenue.doubleValue());
					margin = Double.valueOf(1.0D - actual_cost.doubleValue()
							/ revenue.doubleValue());
				} else {
					proforma_margin = Double.valueOf(0.0D);
					margin = Double.valueOf(0.0D);
				}

				Vector row = new Vector(12);

				row.add(0, revenue);
				row.add(1, estimated_cost);
				row.add(2, actual_cost);
				row.add(3, job_count);
				row.add(4, proforma_margin);
				row.add(5, margin);
				row.add(6, delta_margin);
				row.add(7, year_index);

				data_set.add(row);
			}

			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException1) {
			}
		}
		return max_revenue;
	}

	String buildXAxis2() {
		String xaxis = "";
		String delimiter = "";
		for (int i = 12; i > 0; i--) {
			xaxis = xaxis + delimiter + Months[this.PrimaryMonthOrder[i]];
			delimiter = "|";
		}

		return xaxis;
	}

	void buildProductivity() {
		getProductivity();
	}

	void getProductivity() {
		String sql = "select  YYYYKey, MMKey, Department, EmployeeCount, Productivity from datawarehouse.dbo.DashboardProductivity order by YYYYMMKey desc, Department";

		String dept = "";
		String[][] productivity = new String[13][9];

		String[] total_counts = new String[4];
		boolean[] first_pass = { true, true, true, true };

		String[] data = new String[4];

		String xlabel1 = "";
		String xlabel2 = "";
		String delimiter = "";
		Double max = Double.valueOf(0.0D);
		Double min = Double.valueOf(9999999.0D);

		Vector All = new Vector(12);
		Vector Admin = new Vector(12);
		Vector Ops = new Vector(12);
		Vector Sales = new Vector(12);

		int[] YYYYMM = new int[12];
		Vector spm_prod = new Vector(12);
		Vector spm_employee_count = new Vector(12);

		int i = 1;
		int x = 0;
		int x1 = 0;

		String tablerows = "";

		int[] order = new int[14];
		order[0] = -1;

		NumberFormat nf = new DecimalFormat("##0.0");
		NumberFormat nf2 = new DecimalFormat("###");

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Double prod = Double.valueOf(rs.getDouble("Productivity"));
				Double prodK = Double.valueOf(prod.doubleValue() / 1000.0D);
				Double count = Double.valueOf(rs.getDouble("EmployeeCount"));

				if (prod.compareTo(max) > 0) {
					max = prod;
				}
				if (prod.compareTo(min) < 0) {
					min = prod;
				}

				dept = rs.getString("Department");
				x = rs.getInt("MMKey");

				if (dept.equals("All")) {
					All.add(prod);
					productivity[x][0] = Months[x];
					productivity[x][1] = nf.format(count);
					productivity[x][2] = nf.format(prodK);

					xlabel1 = Months[x] + "|" + xlabel1;

					if ((i == 1) || (i == 12)) {
						xlabel2 = rs.getString("YYYYKey") + "|" + xlabel2;
					}

					order[i] = x;
					i++;

					if (first_pass[0] != false) {
						total_counts[0] = nf.format(count);
						first_pass[0] = false;
					}
				} else if (dept.equals("Admin")) {
					Admin.add(prod);
					productivity[x][3] = nf.format(count);
					productivity[x][4] = nf.format(prodK);
					if (first_pass[2] != false) {
						total_counts[2] = nf.format(count);
						first_pass[2] = false;
					}
				} else if (dept.equals("Ops")) {
					Ops.add(prod);
					productivity[x][5] = nf.format(count);
					productivity[x][6] = nf.format(prodK);
					if (first_pass[2] != false) {
						total_counts[2] = nf.format(count);
						first_pass[2] = false;
					}

				} else if (dept.equals("Sales")) {
					Sales.add(prod);
					productivity[x][7] = nf.format(count);
					productivity[x][8] = nf.format(prodK);
					if (first_pass[3] != false) {
						total_counts[3] = nf.format(count);
						first_pass[3] = false;
					}
				}
			}

			rs.close();

			for (int x2 = 1; x2 <= 12; x2++) {
				x1 = order[x2];
				if (x2 == 12)
					tablerows = tablerows
							+ "<tr><td class=\"cccb\" style=\"padding-left:12px\">"
							+ productivity[x1][0]
							+ "</td>"
							+ "<td class=\"cccb\">"
							+ productivity[x1][5]
							+ "</td><td class=\"cccb\" style=\"padding-right:10px\">"
							+ productivity[x1][6]
							+ "</td>"
							+ "<td class=\"cccb\">"
							+ productivity[x1][7]
							+ "</td><td class=\"cccb\" style=\"padding-right:10px\">"
							+ productivity[x1][8]
							+ "</td>"
							+ "<td class=\"cccb\">"
							+ productivity[x1][3]
							+ "</td><td class=\"cccb\" style=\"padding-right:10px\">"
							+ productivity[x1][4]
							+ "</td>"
							+ "<td class=\"cccb\">"
							+ productivity[x1][1]
							+ "</td><td class=\"cccb\" style=\"padding-right:10px\">"
							+ productivity[x1][2] + "</td></tr>\n";
				else if (x2 == 1)
					tablerows = tablerows
							+ "<tr><td class=\"ccbb\" style=\"padding-left:12px;color: #5a5a5a;\">"
							+ productivity[x1][0]
							+ "</td>"
							+ "<td class=\"ccbb\" style=\"color: #5a5a5a;\">"
							+ productivity[x1][5]
							+ "</td><td class=\"ccbb\" style=\"padding-right:10px;color: #5a5a5a;\">"
							+ productivity[x1][6]
							+ "</td>"
							+ "<td class=\"ccbb\" style=\"color: #5a5a5a;\">"
							+ productivity[x1][7]
							+ "</td><td class=\"ccbb\" style=\"padding-right:10px;color: #5a5a5a;\">"
							+ productivity[x1][8]
							+ "</td>"
							+ "<td class=\"ccbb\" style=\"color: #5a5a5a;\">"
							+ productivity[x1][3]
							+ "</td><td class=\"ccbb\" style=\"padding-right:10px;color: #5a5a5a;\">"
							+ productivity[x1][4]
							+ "</td>"
							+ "<td class=\"ccbb\" style=\"color: #5a5a5a;\">"
							+ productivity[x1][1]
							+ "</td><td class=\"ccbb\" style=\"padding-right:10px;color: #5a5a5a;\">"
							+ productivity[x1][2] + "</td></tr>\n";
				else {
					tablerows = tablerows
							+ "<tr><td class=\"ccab\" style=\"padding-left:12px\">"
							+ productivity[x1][0]
							+ "</td>"
							+ "<td class=\"ccab\">"
							+ productivity[x1][5]
							+ "</td><td class=\"ccab\" style=\"padding-right:10px\">"
							+ productivity[x1][6]
							+ "</td>"
							+ "<td class=\"ccab\">"
							+ productivity[x1][7]
							+ "</td><td class=\"ccab\" style=\"padding-right:10px\">"
							+ productivity[x1][8]
							+ "</td>"
							+ "<td class=\"ccab\">"
							+ productivity[x1][3]
							+ "</td><td class=\"ccab\" style=\"padding-right:10px\">"
							+ productivity[x1][4]
							+ "</td>"
							+ "<td class=\"ccab\">"
							+ productivity[x1][1]
							+ "</td><td class=\"ccab\" style=\"padding-right:10px\">"
							+ productivity[x1][2] + "</td></tr>\n";
				}
			}

			this.Request.setAttribute("ProductivityTable", tablerows);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}

		data[0] = "";
		data[1] = "";
		data[2] = "";
		data[3] = "";
		String xaxis = "";
		String yaxis = "";
		delimiter = "";

		min = Double.valueOf(0.0D);

		Double min_max_diff = Double.valueOf(max.doubleValue()
				- min.doubleValue());
		
		int size = Sales.size();
		if(Ops.size()>size){
			size = Ops.size();
		}if(Admin.size()>size){
			size = Admin.size();
		}if(All.size()>size){
			size = All.size();
		}
		

		for (int j = 0; j < size; j++) {
			Double prod = null;
			if(Sales.size()>j){
				prod = (Double) Sales.get(j);
			data[0] = (nf.format((prod.doubleValue() - min.doubleValue())
					/ min_max_diff.doubleValue() * 100.0D)
					+ delimiter + data[0]);
			}else{
				data[0] = "0";
			}
			
			if(Ops.size()>j){
				prod = (Double) Ops.get(j);
				data[1] = (nf.format((prod.doubleValue() - min.doubleValue())
						/ min_max_diff.doubleValue() * 100.0D)
						+ delimiter + data[1]);
			}else{
				data[1] = "0";
			}

			if(Admin.size()>j){
				prod = (Double) Admin.get(j);
				data[2] = (nf.format((prod.doubleValue() - min.doubleValue())
						/ min_max_diff.doubleValue() * 100.0D)
						+ delimiter + data[2]);
			}else{
				data[2] = "0";
			}
			
			if(All.size()>j){
			prod = (Double) All.get(j);
			data[3] = (nf.format((prod.doubleValue() - min.doubleValue())
					/ min_max_diff.doubleValue() * 100.0D)
					+ delimiter + data[3]);
			}else{
				data[3] = "0";
			}
			delimiter = ",";
		}

		String ops_target = nf.format((60000.0D - min.doubleValue())
				/ min_max_diff.doubleValue() * 100.0D);
		String ops_target1 = nf.format((35000.0D - min.doubleValue())
				/ min_max_diff.doubleValue() * 100.0D);
		String sales_target = nf.format((150000.0D - min.doubleValue())
				/ min_max_diff.doubleValue() * 100.0D);

		xaxis = data[0] + "|" + data[2] + "|" + data[1] + "|" + data[3] + "|"
				+ sales_target + "," + sales_target + "|" + ops_target + ","
				+ ops_target + "|" + ops_target1 + "," + ops_target1;

		delimiter = "";
		int incr_delta = 10;
		long incr = min_max_diff.longValue() / incr_delta;

		for (int k = 1; k < incr_delta; k++) {
			yaxis = yaxis + delimiter + String.valueOf(incr * k / 1000L) + "K";
			delimiter = "|";
		}

		yaxis = nf2.format(min.doubleValue() / 1000.0D) + "K" + delimiter
				+ yaxis + delimiter + nf2.format(max.doubleValue() / 1000.0D)
				+ "K";

		String chart = "<img src=\"http://chart.apis.google.com/chart?cht=lc&amp;chs=420x240&amp;chd=t:"
				+ xaxis
				+ "&amp;chxt=x,x,y&amp;chxl=0:|"
				+ xlabel1
				+ "1:|"
				+ xlabel2
				+ "2:|"
				+ yaxis
				+ "&amp;chco=cad384,a9a8ad,7f99b0,5a5a5a,78AA5A,f5533e,F8A186&amp;chdl=Sales|Support|Ops|All|Sales%20Target|Ops%20Target|Ops%2035K%20Target\"/>";
		this.Request.setAttribute("ProductivityChart", chart);

		if (this.reference_type.equals("SPM")) {
			String spm_table = "";

			getSPMProductivity(this.reference, YYYYMM, spm_prod,
					spm_employee_count);

			Vector max1 = new Vector();
			Vector min1 = new Vector();
			if (spm_prod.size() == 0) {
				max1.add(0.0D);
			} else {
				max1.add(Collections.max(spm_prod));
			}

			max1.add(Collections.max(Ops));
			max1.add(Collections.max(All));
			if (spm_prod.size() == 0) {
				min1.add(0.0D);
			} else {
				min1.add(Collections.max(spm_prod));
			}
			min1.add(Collections.min(Ops));
			min1.add(Collections.min(All));

			max = (Double) Collections.max(max1);
			min = Double.valueOf(0.0D);

			min_max_diff = Double
					.valueOf(max.doubleValue() - min.doubleValue());

			data[0] = "";
			data[1] = "";
			data[2] = "";

			delimiter = "";

			spm_table = "<tr><td class=\"subsectiontitle\" style=\"padding-top:16px\">"
					+ this.reference_name
					+ " Productivity Summary</td>"
					+ "</tr>"
					+ "<tr>"
					+ "<td>"
					+ "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"
					+ "<tr>"
					+ "<td class=\"ch1b\">Month</td>"
					+ "<td class=\"ch1b\">Productivity</td>"
					+ "<td class=\"ch1b\">Employees</td>" + "</tr>";

			String r1 = "";
			String r2 = "";
			String r3 = "";

			for (int j = 0; j < spm_prod.size(); j++) {
				Double prod = (Double) spm_prod.get(j);
				data[0] = (nf.format((prod.doubleValue() - min.doubleValue())
						/ min_max_diff.doubleValue() * 100.0D)
						+ delimiter + data[0]);

				r1 = r1 + "<tr><td class=\"ccac\">" + Months[YYYYMM[j]]
						+ "</td>" + "<td class=\"ccac\">"
						+ nf.format(prod.doubleValue() / 1000.0D) + "K</td>"
						+ "<td class=\"ccac\">"
						+ nf.format((Double) spm_employee_count.get(j))
						+ "</td></tr>";

				prod = (Double) Ops.get(j);
				data[1] = (nf.format((prod.doubleValue() - min.doubleValue())
						/ min_max_diff.doubleValue() * 100.0D)
						+ delimiter + data[1]);

				prod = (Double) All.get(j);
				data[2] = (nf.format((prod.doubleValue() - min.doubleValue())
						/ min_max_diff.doubleValue() * 100.0D)
						+ delimiter + data[2]);

				delimiter = ",";
			}

			spm_table = spm_table + r1 + "</table>" + "</td>" + "</tr>";

			this.Request.setAttribute("SPM_ProductivityTable", spm_table);

			ops_target = nf.format((60000.0D - min.doubleValue())
					/ min_max_diff.doubleValue() * 100.0D);
			ops_target1 = nf.format((35000.0D - min.doubleValue())
					/ min_max_diff.doubleValue() * 100.0D);

			xaxis = data[0] + "|" + data[1] + "|" + data[2] + "|" + ops_target
					+ "," + ops_target + "|" + ops_target1 + "," + ops_target1;

			delimiter = "";
			incr_delta = 10;
			yaxis = "";
			incr = min_max_diff.longValue() / incr_delta;

			for (int k = 1; k < incr_delta; k++) {
				yaxis = yaxis + delimiter + String.valueOf(incr * k / 1000L)
						+ "K";
				delimiter = "|";
			}

			yaxis = nf2.format(min.doubleValue() / 1000.0D) + "K" + delimiter
					+ yaxis + delimiter
					+ nf2.format(max.doubleValue() / 1000.0D) + "K";

			chart = "<img src=\"http://chart.apis.google.com/chart?cht=lc&amp;chs=420x240&amp;chd=t:"
					+ xaxis
					+ "&amp;chxt=x,x,y&amp;chxl=0:|"
					+ xlabel1
					+ "1:|"
					+ xlabel2
					+ "2:|"
					+ yaxis
					+ "&amp;chco=cad384,7f99b0,5a5a5a,f5533e,F8A186&amp;chdl="
					+ this.reference_name
					+ "|Ops|All|Ops%20Target|Ops%2035K%20Target\"/>";

			this.Request.setAttribute("ProductivityChart", chart);
		}
	}

	void getSPMProductivity(String reference, int[] YYYYMM, Vector spm_prod,
			Vector spm_employee_count) {
		String sql = "select MMKey, EmployeeCount, Productivity, lo_pc_last_name  from datawarehouse.dbo.DashboardProductivityBreakdown join lo_poc on PocReference = lo_pc_id  where PocReference = "
				+ reference + " order by YYYYMMKey desc";

		int i = 0;
		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				if (i == 0)
					this.reference_name = rs.getString("lo_pc_last_name");
				YYYYMM[(i++)] = rs.getInt("MMKey");
				spm_employee_count.add(Double.valueOf(rs
						.getDouble("EmployeeCount")));
				Double prod = Double.valueOf(rs.getDouble("Productivity"));
				spm_prod.add(prod);
			}

			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
	}

	void buildWIP() {
		String sql = "select PocReference, PocReferenceType, ProjectType, JobDisplayStatus, JobDisplayStatusCount, JobRevenue from datawarehouse.dbo.DashBoardInworkStatus "
				+ this.where;

		String[] job_status = { "Not&nbsp;Scheduled", "Scheduled", "Confirmed",
				"Onsite", "Offsite", "Complete", "Hold" };
		Double[] job_revenue = { Double.valueOf(0.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D), Double.valueOf(0.0D),
				Double.valueOf(0.0D) };
		int[] job_count = new int[7];

		Double total_revenue = Double.valueOf(0.0D);
		int total_count = 0;

		Map job_status_order = new HashMap();
		job_status_order.put("Not Scheduled", Integer.valueOf(0));
		job_status_order.put("Scheduled", Integer.valueOf(1));
		job_status_order.put("Confirmed", Integer.valueOf(2));
		job_status_order.put("Onsite", Integer.valueOf(3));
		job_status_order.put("Offsite", Integer.valueOf(4));
		job_status_order.put("Complete", Integer.valueOf(5));
		job_status_order.put("Hold", Integer.valueOf(6));

		NumberFormat nf1 = NumberFormat.getIntegerInstance();

		String delimiter = "";
		Double max = Double.valueOf(0.0D);
		Double min = Double.valueOf(0.0D);
		Double prod = Double.valueOf(0.0D);

		NumberFormat nf = new DecimalFormat("##0.0");
		NumberFormat nf2 = new DecimalFormat("###");

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String status = rs.getString("JobDisplayStatus");
				int count = rs.getInt("JobDisplayStatusCount");
				Double revenue = Double.valueOf(rs.getDouble("JobRevenue"));
				total_revenue = Double.valueOf(total_revenue.doubleValue()
						+ revenue.doubleValue());
				total_count += count;

				if (job_status_order.get(status) != null) {
					int current_status = ((Integer) job_status_order
							.get(status)).intValue();

					if ((current_status >= 0) && (current_status < 7)) {
						job_revenue[current_status] = revenue;
						job_count[current_status] = count;
					}

				}

				if (revenue.compareTo(max) > 0) {
					max = revenue;
				}
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}

		String[] data = new String[4];
		data[0] = "";
		data[1] = "";
		data[2] = "";
		data[3] = "";
		String xaxis = "";
		String yaxis = "";
		delimiter = "";
		Double min_max_diff = Double.valueOf(max.doubleValue()
				- min.doubleValue());
		String tablerows = "";

		for (int j = 0; j < 7; j++) {
			prod = job_revenue[j];

			data[0] = (data[0] + delimiter + nf
					.format((prod.doubleValue() - min.doubleValue())
							/ min_max_diff.doubleValue() * 100.0D));

			if (j == 0){
				tablerows = tablerows
						+ "<tr><td class=\"ccaa\" style=\"border-top: 1px solid #BFBEC4\">"
						+ job_status[j] + "</td><td class=\"ccbc\"><a href='wipDtlAction.do?status="+job_status[j].replace("&nbsp;", " ")
						+ "&search=go&PocReferenceType="+this.project_type+"&PocReference="+this.reference+"' style='color:#0F55CA;text-decoration:none;'>"
						+ nf1.format(job_count[j])
						+ "</a></td><td class=\"ccbc\">$"
						+ nf1.format(job_revenue[j]) + "</td></tr>\n";
			}
			else {
				tablerows = tablerows 
						+ "<tr><td class=\"ccaa\">"
						+ job_status[j] + "</td><td class=\"ccac\"><a href='wipDtlAction.do?status="+job_status[j].replace("&nbsp;", " ")
						+"&search=go&PocReferenceType="+this.project_type+"&PocReference="+this.reference+"' style='color:#0F55CA;text-decoration:none;'>"
						+ nf1.format(job_count[j])
						+ "</a></td><td class=\"ccac\">$"
						+ nf1.format(job_revenue[j]) + "</td></tr>\n";
			}

			delimiter = "|";
		}

		tablerows = tablerows
				+ "<tr><td class=\"rh1a\" style=\"border-bottom: 1px solid #BFBEC4;color: #5a5a5a;\">&nbsp;Total</td><td class=\"cccc\" style=\"color: #5a5a5a;\">"
				+ nf1.format(total_count)
				+ "</td><td class=\"cccc\" style=\"color: #5a5a5a;\">$"
				+ nf1.format(total_revenue) + "</td></tr>\n";
		this.Request.setAttribute("WIPTable", tablerows);

		xaxis = data[0];
		delimiter = "";
		long incr = min_max_diff.longValue() / 6L;

		for (int k = 1; k < 6; k++) {
			yaxis = yaxis + delimiter + String.valueOf(incr * k / 1000L) + "K";
			delimiter = "|";
		}
		yaxis = nf2.format(min.doubleValue() / 1000.0D) + "K" + delimiter
				+ yaxis + delimiter + nf2.format(max.doubleValue() / 1000.0D)
				+ "K";

		String chart = "<img src=\"http://chart.apis.google.com/chart?cht=bhg&amp;chs=230x128&amp;chbh=11,2,2&amp;chd=t:"
				+ xaxis
				+ "&amp;chxt=x&amp;chxl=0:|"
				+ yaxis
				+ "&amp;chco=DCDCB4,CCBF95,CCBF95,7f99b0,7f99b0,a9a8ad,F8A186\"/>";
		this.Request.setAttribute("WIPChart", chart);
	}

	private void getCurrentemployees() {
		String sql = "select Department, EmployeeCount from datawarehouse.dbo.DashboardEmployeeCounts";

		String Department = "";
		String prefix = "employee_count_";

		NumberFormat nf = new DecimalFormat("##0.00");

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Department = rs.getString("Department");

				if (!rs.wasNull()) {
					this.Request.setAttribute(prefix + Department,
							nf.format(rs.getDouble("EmployeeCount")));
				}
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	void buildMMMSPUsage() {
		int[] MM = new int[13];
		int[] SP = new int[13];
		int[] P = new int[13];

		String m_chart = "";
		String s_chart = "";
		String p_chart = "";
		String t_chart = "";
		String tablerows = "";

		NumberFormat nf = new DecimalFormat("##0.0");
		new DecimalFormat();
		NumberFormat nf1 = DecimalFormat.getPercentInstance();
		NumberFormat nf2 = NumberFormat.getIntegerInstance();

		Double m_p = Double.valueOf(0.0D);
		Double s_p = Double.valueOf(0.0D);
		Double p_p = Double.valueOf(0.0D);

		Double total = Double.valueOf(0.0D);

		int max = getMMSPUsage(MM, SP, P);
		Double dmax = Double.valueOf(max / 1.0D);

		m_chart = "";
		s_chart = "";
		p_chart = "";
		t_chart = "";

		m_chart = nf.format(MM[(this.MonthOrder[0] - 1)] / 1.0D
				/ dmax.doubleValue() * 100.0D);
		s_chart = nf.format(SP[(this.MonthOrder[0] - 1)] / 1.0D
				/ dmax.doubleValue() * 100.0D);
		p_chart = nf.format(P[(this.MonthOrder[0] - 1)] / 1.0D
				/ dmax.doubleValue() * 100.0D);

		total = Double
				.valueOf((MM[(this.MonthOrder[0] - 1)]
						+ SP[(this.MonthOrder[0] - 1)] + P[(this.MonthOrder[0] - 1)]) * 1.0D);
		m_p = Double
				.valueOf(MM[(this.MonthOrder[0] - 1)] / total.doubleValue());
		s_p = Double
				.valueOf(SP[(this.MonthOrder[0] - 1)] / total.doubleValue());
		p_p = Double.valueOf(P[(this.MonthOrder[0] - 1)] / total.doubleValue());
		t_chart = nf.format(total.doubleValue() * 0.5D / dmax.doubleValue()
				* 100.0D);

		String mmm = Months[this.MonthOrder[0]];

		tablerows = "<tr><td class=\"cccb\" style=\"padding-left:12px\">" + mmm
				+ "</td>" + "<td class=\"cccc\">"
				+ MM[(this.MonthOrder[0] - 1)]
				+ "</td><td class=\"cccc\" style=\"padding-right:10px\">"
				+ nf1.format(m_p) + "</td>" + "<td class=\"cccc\">"
				+ SP[(this.MonthOrder[0] - 1)]
				+ "</td><td class=\"cccc\" style=\"padding-right:10px\">"
				+ nf1.format(s_p) + "</td>" + "<td class=\"cccc\">"
				+ nf2.format(P[(this.MonthOrder[0] - 1)])
				+ "</td><td class=\"cccc\" style=\"padding-right:10px\">"
				+ nf1.format(p_p) + "</td>" + "<td class=\"cccc\">"
				+ nf2.format(total.intValue()) + "</td></tr>\n";

		for (int i = 1; i < 12; i++) {
			m_chart = m_chart
					+ ","
					+ nf.format(MM[(this.MonthOrder[i] - 1)] / 1.0D
							/ dmax.doubleValue() * 100.0D);
			s_chart = s_chart
					+ ","
					+ nf.format(SP[(this.MonthOrder[i] - 1)] / 1.0D
							/ dmax.doubleValue() * 100.0D);
			p_chart = p_chart
					+ ","
					+ nf.format(P[(this.MonthOrder[i] - 1)] / 1.0D
							/ dmax.doubleValue() * 100.0D);

			total = Double
					.valueOf((MM[(this.MonthOrder[i] - 1)]
							+ SP[(this.MonthOrder[i] - 1)] + P[(this.MonthOrder[i] - 1)]) * 1.0D);
			m_p = Double.valueOf(MM[(this.MonthOrder[i] - 1)]
					/ total.doubleValue());
			s_p = Double.valueOf(SP[(this.MonthOrder[i] - 1)]
					/ total.doubleValue());
			p_p = Double.valueOf(P[(this.MonthOrder[i] - 1)]
					/ total.doubleValue());
			t_chart = t_chart
					+ ","
					+ nf.format(total.doubleValue() * 0.5D / dmax.doubleValue()
							* 100.0D);

			mmm = Months[this.MonthOrder[i]];

			if (i == 11)
				tablerows = "<tr><td class=\"ccbb\" style=\"padding-left:12px\">"
						+ mmm
						+ "</td>"
						+ "<td class=\"ccbc\">"
						+ MM[(this.MonthOrder[i] - 1)]
						+ "</td><td class=\"ccbc\" style=\"padding-right:10px\">"
						+ nf1.format(m_p)
						+ "</td>"
						+ "<td class=\"ccbc\">"
						+ SP[(this.MonthOrder[i] - 1)]
						+ "</td><td class=\"ccbc\" style=\"padding-right:10px\">"
						+ nf1.format(s_p)
						+ "</td>"
						+ "<td class=\"ccbc\">"
						+ nf2.format(P[(this.MonthOrder[i] - 1)])
						+ "</td><td class=\"ccbc\" style=\"padding-right:10px\">"
						+ nf1.format(p_p)
						+ "</td>"
						+ "<td class=\"ccbc\">"
						+ nf2.format(total.intValue())
						+ "</td></tr>\n"
						+ tablerows;
			else {
				tablerows = "<tr><td class=\"ccab\" style=\"padding-left:12px\">"
						+ mmm
						+ "</td>"
						+ "<td class=\"ccac\">"
						+ MM[(this.MonthOrder[i] - 1)]
						+ "</td><td class=\"ccac\" style=\"padding-right:10px\">"
						+ nf1.format(m_p)
						+ "</td>"
						+ "<td class=\"ccac\">"
						+ SP[(this.MonthOrder[i] - 1)]
						+ "</td><td class=\"ccac\" style=\"padding-right:10px\">"
						+ nf1.format(s_p)
						+ "</td>"
						+ "<td class=\"ccac\">"
						+ nf2.format(P[(this.MonthOrder[i] - 1)])
						+ "</td><td class=\"ccac\" style=\"padding-right:10px\">"
						+ nf1.format(p_p)
						+ "</td>"
						+ "<td class=\"ccac\">"
						+ nf2.format(total.intValue())
						+ "</td></tr>\n"
						+ tablerows;
			}

		}

		this.Request.setAttribute("MMSPTable", tablerows);

		String chart = ChartImageTag1
				+ "cht=lc&amp;chs=380x200&amp;chd=t:"
				+ m_chart
				+ "|"
				+ s_chart
				+ "|"
				+ p_chart
				+ "|"
				+ t_chart
				+ "&amp;chxt=x,x,y&amp;chxl=0:|"
				+ this.XAxisMonths
				+ "|1:|"
				+ this.XAxisYears
				+ "|2:|"
				+ buildYAxis(0, max, 6, 0, 1, "")
				+ "&amp;chco=cad384,7f99b0,5a5a5a,f5533e&amp;chdl=Minuteman|Speedpay|Standard|Target(50%)&amp;chdlp=b\"/>";

		this.Request.setAttribute("MMSPChart", chart);
	}

	int getMMSPUsage(int[] m, int[] s, int[] p) {
		String sql = "select YYYYMMKey, YYYYKey, MMKey-1 as MMKey,       PO_Type, Usage from datawarehouse.dbo.DashBoardMMSP "
				+ this.where
				+ " and YYYYMMKey > datepart(yy, dateadd(mm, -12, getdate()))*100+datepart(mm,dateadd(mm, -12, getdate())) "
				+ "order by YYYYMMKey";

		int mm_key = 0;
		String type = "";
		int i = 0;
		int usage = 0;
		int max = 0;

		for (int j = 0; j < 13; j++) {
			m[j] = 0;
			s[j] = 0;
			p[j] = 0;
		}

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				if (i++ >= 36)
					continue;
				mm_key = rs.getInt("MMKey");

				if (rs.wasNull())
					continue;
				type = rs.getString("PO_Type");
				usage = rs.getInt("Usage");

				if (usage > max) {
					max = usage;
				}

				if (type.equals("M")) {
					m[mm_key] = usage;
					m[12] += usage;
				} else if (type.equals("S")) {
					s[mm_key] = usage;
					s[12] += usage;
				} else {
					p[mm_key] = usage;
					p[12] += usage;
				}

			}

			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return max;
	}

	int getOOB(int[] m, int[] s, int[] p) {
		String sql = "select YYYYMMKey, YYYYKey, MMKey-1 as MMKey,       PO_Type, Usage from datawarehouse.dbo.DashBoardMMSP "
				+ this.where
				+ " and YYYYMMKey > datepart(yy, dateadd(mm, -12, getdate()))*100+datepart(mm,dateadd(mm, -12, getdate())) "
				+ "order by YYYYMMKey";

		int mm_key = 0;
		String type = "";
		int i = 0;
		int usage = 0;
		int max = 0;

		for (int j = 0; j < 12; j++) {
			m[j] = -1;
			s[j] = -1;
			p[j] = -1;
		}

		m[12] = 0;
		s[12] = 0;
		p[12] = 0;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				if (i++ >= 36)
					continue;
				mm_key = rs.getInt("MMKey");

				if (rs.wasNull())
					continue;
				type = rs.getString("PO_Type");
				usage = rs.getInt("Usage");

				if (usage > max) {
					max = usage;
				}

				if (type.equals("M")) {
					m[mm_key] = usage;
					m[12] += usage;
				} else if (type.equals("S")) {
					s[mm_key] = usage;
					s[12] += usage;
				} else {
					p[mm_key] = usage;
					p[12] += usage;
				}

			}

			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
		return max;
	}

	private void buildQualityPartnerRatings() {
		Double[] rating = new Double[12];
		int[] rating_count = new int[12];
		getQualityPartnerRatings(rating, rating_count);

		String mmm = "";
		Double d_rating = Double.valueOf(0.0D);
		String s_rating = "";
		String tablerows = "";
		String chart = "";
		String delimiter = ",";

		for (int i = 0; i < 12; i++) {
			if (i == 11) {
				delimiter = "";
			}

			mmm = Months[this.MonthOrder[i]];

			d_rating = rating[i];
			s_rating = s_rating + String.valueOf(d_rating) + delimiter;

			if (i == 0)
				tablerows = "<tr><td class=\"cccb\" style=\"padding-left:12px\">"
						+ mmm
						+ "</td><td class=\"cccb\">"
						+ rating[i]
						+ "</td><td class=\"cccc\">"
						+ rating_count[i]
						+ "</td></tr>\n" + tablerows;
			else if (i == 11)
				tablerows = "<tr><td class=\"ccbb\" style=\"padding-left:12px;color: #5a5a5a;\">"
						+ mmm
						+ "</td><td class=\"ccbb\" style=\"color: #5a5a5a;\">"
						+ rating[i]
						+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
						+ rating_count[i] + "</td></tr>\n" + tablerows;
			else {
				tablerows = "<tr><td class=\"ccab\" style=\"padding-left:12px\">"
						+ mmm
						+ "</td><td class=\"ccab\">"
						+ rating[i]
						+ "</td><td class=\"ccac\">"
						+ rating_count[i]
						+ "</td></tr>\n" + tablerows;
			}

		}

		this.Request.setAttribute("PartnerRatingTable", tablerows);

		chart = ChartImageTag1
				+ "cht=lc&amp;chs=360x175&amp;chd=t:"
				+ s_rating
				+ "&chds=1,5"
				+ "&amp;chxt=x,x,y&amp;chxl=0:|"
				+ this.XAxisMonths
				+ "|1:|"
				+ this.XAxisYears
				+ "|2:|1.0|1.5|2.0|2.5|3.0|3.5|4.0|4.5|5.0&amp;chco=7f99b0&amp;chdl=Partner%20Rating&amp;chdlp=b\"/>";

		this.Request.setAttribute("PartnerRatingChart", chart);
	}

	private void getQualityPartnerRatings(Double[] r, int[] c) {
		String sql = "select top 12 YYYYMMKey, YYYYKey, MMKey-1 as MMKey, convert(decimal(6,2),Rating) as Rating, RatingCount from datawarehouse.dbo.DashBoardPartnerRating "
				+ this.where + " " + "order by YYYYMMKey";

		for (int j = 0; j < 12; j++) {
			r[j] = Double.valueOf(-1.0D);
			c[j] = -1;
		}

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				int mm_key = rs.getInt("MMKey");
				r[mm_key] = Double.valueOf(rs.getDouble("Rating"));
				c[mm_key] = rs.getInt("RatingCount");
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	void buildTimeConstants() {
		SimpleDateFormat sfMonth = new SimpleDateFormat("MMM");
		SimpleDateFormat sfYear = new SimpleDateFormat("yyyy");

		GregorianCalendar StartPoint = new GregorianCalendar();
		StartPoint.add(2, -11);

		this.XAxisYears = sfYear.format(StartPoint.getTime());
		this.XAxisMonths = sfMonth.format(StartPoint.getTime());

		this.iFirstMonth = (StartPoint.get(2) + 1);
		this.MonthOrder[0] = this.iFirstMonth;
		this.sFirstMonth = sfMonth.format(StartPoint.getTime());
		this.iFirstYear = StartPoint.get(1);

		for (int i = 1; i < 12; i++) {
			StartPoint.add(2, 1);
			this.XAxisMonths = (this.XAxisMonths + "|" + sfMonth
					.format(StartPoint.getTime()));
			this.MonthOrder[i] = (StartPoint.get(2) + 1);
		}

		this.iLastMonth = (StartPoint.get(2) + 1);
		this.sLastMonth = sfMonth.format(StartPoint.getTime());
		this.iLastYear = StartPoint.get(1);

		if (!this.XAxisYears.equals(sfYear.format(StartPoint.getTime()))) {
			this.XAxisYears = (this.XAxisYears + "|" + sfYear.format(StartPoint
					.getTime()));
		}

		StartPoint = new GregorianCalendar();
		this.XAxisWeeks = String.valueOf(StartPoint.get(3));

		for (int i = 5; i > 0; i--) {
			StartPoint.add(3, -1);
			this.XAxisWeeks = (String.valueOf(StartPoint.get(3)) + "|" + this.XAxisWeeks);
		}
	}

	String buildXAxis1() {
		String xaxis = "0:";

		for (int i = 1; i < 13; i++) {
			xaxis = xaxis + "|" + Months[this.PrimaryMonthOrder[i]];
		}

		xaxis = xaxis + "|1:|" + this.YearList[0] + "|" + this.YearList[1];

		return xaxis;
	}

	String buildYAxis(int min, int max, int incr, int start, int base,
			String base_unit) {
		String yaxis = "";
		int incr_delta = (max - min) / incr;

		for (int k = 1; k < incr; k++) {
			yaxis = yaxis + "|" + String.valueOf(incr_delta * k / base)
					+ base_unit;
		}
		yaxis = yaxis + "|";

		yaxis = String.valueOf(min / base) + base_unit + yaxis
				+ String.valueOf(max / base) + base_unit;

		return yaxis;
	}

	void getBuildDropDowns() {
		Vector x = new Vector();
		ArrayList spm = new ArrayList(8);
		ArrayList bdm = new ArrayList(8);
		ArrayList spm_owner = new ArrayList(16);

		getSPM_BDM(spm, bdm);

		if ((this.reference_type.equals("SPM"))
				|| (this.reference_type.equals("JobOwner"))) {
			getJobOwners(this.reference, spm_owner);
		}
		if (this.reference_type.equals("JobOwner")) {
			getJobOwners(this.JobOwner_SPM_reference, spm_owner);
		}
		this.Request.setAttribute("SPM_Dropdown", buildOptions("SPM", spm));
		this.Request.setAttribute("BDM_Dropdown", buildOptions("BDM", bdm));
		this.Request.setAttribute("JobOwner_Dropdown",
				buildOptions("JobOwner", spm_owner));
	}

	void getSPM_BDM(ArrayList<String[]> spm, ArrayList<String[]> bdm) {
		String sql = "select EmployeeID,  left(EmployeeFirstName, 1)+' '+EmployeeLastName as name, EmployeeDepartment   from datawarehouse.dbo.DimEmployee left join ilex.dbo.lo_poc on EmployeeID = lo_pc_id  where EmployeeTransactionType != 'Termination' and lo_pc_title in ('Business Development Manager', 'Sr. Project Manager', 'VP Corporate Development', 'Managing Partner')        and EffectiveEndDate > getdate()order by EmployeeLastName";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String department = rs.getString("EmployeeDepartment");
				if (!rs.wasNull()) {
					String[] p = new String[3];
					p[0] = rs.getString("EmployeeID");
					p[1] = rs.getString("name");
					p[2] = "0";
					if (department.equals("Sales"))
						bdm.add(p);
					else {
						spm.add(p);
					}
				}
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	void getJobOwners(String spm, ArrayList<String[]> owners) {
		String sql = "select JobOwner, EmployeeLastName+' '+left(EmployeeFirstName, 1) as name, convert (decimal(4,1), avg (OwnerWeight)*100) as weight  from datawarehouse.dbo.DashboardSPMtoJobOwner join datawarehouse.dbo.DimEmployee on JobOwner = EmployeeID  where SPM = "
				+ spm
				+ " and EmployeeTransactionType != 'Termination'  and EffectiveEndDate > getdate() "
				+ "       and yyyymmdd > 200810"
				+ " group by JobOwner, EmployeeLastName+' '+left(EmployeeFirstName, 1) "
				+ " order by EmployeeLastName+' '+left(EmployeeFirstName, 1)";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				String jo = rs.getString("JobOwner");
				if (!rs.wasNull()) {
					String[] p = new String[3];
					p[0] = jo;
					p[1] = (rs.getString("name") + " ("
							+ rs.getString("weight") + "%)");
					p[2] = spm;
					owners.add(p);
				}
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException) {
			}
		}
	}

	String buildOptions(String type, ArrayList<String[]> list) {
		String options = "";

		for (int i = 0; i < list.size(); i++) {
			String[] p = (String[]) list.get(i);

			if (type.equals("JobOwner")) {
				if (p[0].equals(this.reference))
					options = options + "<option value=\"" + p[0]
							+ "\" selected>" + p[1] + "</option>";
				else {
					options = options + "<option value=\"" + p[0] + "\">"
							+ p[1] + "</option>";
				}

			} else if ((p[0].equals(this.reference))
					|| (p[0].equals(this.JobOwner_SPM_reference)))
				options = options + "<option value=\"" + p[0] + "\" selected>"
						+ p[1] + "</option>";
			else {
				options = options + "<option value=\"" + p[0] + "\">" + p[1]
						+ "</option>";
			}

		}

		return options;
	}

	private void buildWeeklyFinancials() {
		Vector data_set = new Vector(10);

		Double revenue = Double.valueOf(0.0D);
		Double margin = Double.valueOf(0.0D);
		Double proforma_margin = Double.valueOf(0.0D);

		String chart_revenue = "";
		String chart_margin = "";
		String chart_proforma = "";
		String chart_delimiter = "";

		String delimiter = "";

		Double max_revenue = Double.valueOf(0.0D);

		Integer itemp = Integer.valueOf(0);

		String QtrChartImage = "";

		NumberFormat nf1 = NumberFormat.getIntegerInstance();
		NumberFormat nf2 = new DecimalFormat("0.00");
		NumberFormat nf3 = new DecimalFormat("#0.00");

		max_revenue = getWeeklyFinancials1(data_set);

		for (int i = 0; i < data_set.size() - 1; i++) {
			Vector row = (Vector) data_set.get(i);

			revenue = (Double) row.get(0);
			margin = (Double) row.get(5);
			proforma_margin = (Double) row.get(4);

			margin = Double.valueOf(margin.doubleValue() * 100.0D);
			if (margin.compareTo(Double.valueOf(10.0D)) < 0)
				margin = Double.valueOf(-1.0D);
			else if (margin.compareTo(Double.valueOf(90.0D)) > 0) {
				margin = Double.valueOf(-1.0D);
			}
			itemp = Integer.valueOf(margin.intValue());
			chart_margin = itemp.toString() + delimiter + chart_margin;

			proforma_margin = Double
					.valueOf(proforma_margin.doubleValue() * 100.0D);
			if (proforma_margin.compareTo(Double.valueOf(10.0D)) < 0)
				proforma_margin = Double.valueOf(-1.0D);
			else if (proforma_margin.compareTo(Double.valueOf(90.0D)) > 0) {
				proforma_margin = Double.valueOf(-1.0D);
			}
			itemp = Integer.valueOf(proforma_margin.intValue());
			chart_proforma = itemp.toString() + delimiter + chart_proforma;

			revenue = Double.valueOf(revenue.doubleValue()
					/ max_revenue.doubleValue() * 100.0D);
			itemp = Integer.valueOf(revenue.intValue());
			chart_revenue = itemp.toString() + delimiter + chart_revenue;

			delimiter = ",";
		}

		Vector row = (Vector) data_set.lastElement();
		this.XAxisWeeks = ((String) row.lastElement());

		QtrChartImage = ChartImageTag1
				+ "cht=lc&amp;chs=420x200&amp;chd=t:"
				+ chart_margin
				+ "|"
				+ chart_proforma
				+ "|55,55,55,55&chds=10,80&amp;chxt=x,y&amp;"
				+ "chxl=0:|"
				+ this.XAxisWeeks
				+ "|1:|0.1|0.2|0.3|0.4|0.5|0.6|0.7|0.8&amp;"
				+ "chco=5a5a5a,a9a8ad,f5533e&amp;chdl=Actual|Pro%20Forma|Target"
				+ "\" />";

		this.Request.setAttribute("WVGPMChart", QtrChartImage);

		delimiter = "";
		int incr_delta = 6;
		double incr = max_revenue.doubleValue() / incr_delta;
		String yaxis = "";
		Double converaion_factor = Double.valueOf(1000000.0D);
		String cf_abbreviation = "M";

		if (max_revenue.doubleValue() < 1000000.0D) {
			converaion_factor = Double.valueOf(1000.0D);
			cf_abbreviation = "K";
		} else if (max_revenue.doubleValue() < 10000.0D) {
			converaion_factor = Double.valueOf(1.0D);
			cf_abbreviation = "";
		}

		for (int k = 1; k < incr_delta; k++) {
			yaxis = yaxis + delimiter
					+ nf3.format(incr * k / converaion_factor.doubleValue())
					+ cf_abbreviation;
			delimiter = "|";
		}

		yaxis = "0"
				+ cf_abbreviation
				+ delimiter
				+ yaxis
				+ delimiter
				+ nf3.format(max_revenue.doubleValue()
						/ converaion_factor.doubleValue()) + cf_abbreviation;

		QtrChartImage = ChartImageTag1 + "cht=lc&amp;chs=400x150&amp;chd=t:"
				+ chart_revenue + "&amp;chxt=x,y&amp;" + "chxl=0:|"
				+ this.XAxisWeeks + "|1:|" + yaxis + "&amp;" + "chco=5a5a5a"
				+ "\" />";
		this.Request.setAttribute("WRevenueChart", QtrChartImage);

		String r = "";
		String m = "";
		String p = "";
		String d = "";
		String j = "";
		String q = "";
		String tablerows = "";

		row = (Vector) data_set.get(0);

		long test_revenue = ((Double) row.get(0)).longValue();
		Double test_margin = (Double) row.get(5);

		q = (String) row.get(7);

		if ((test_margin.compareTo(Double.valueOf(0.1D)) < 0)
				|| (test_revenue < 1L)) {
			r = "0";
			m = "N/A";
			p = "N/A";
			d = "N/A";
			j = nf1.format(((Integer) row.get(3)).longValue());
		} else {
			r = nf1.format(((Double) row.get(0)).longValue());
			m = nf2.format((Double) row.get(5));
			p = nf2.format((Double) row.get(4));
			d = nf2.format((Double) row.get(6));
			j = nf1.format(((Integer) row.get(3)).longValue());
		}

		tablerows = "<tr><td class=\"ccaa\" style=\"padding-left:12px;border-top: 1px solid #BFBEC4;color: #5a5a5a;\">"
				+ q
				+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">$"
				+ r
				+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
				+ m
				+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
				+ p
				+ "&nbsp;</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
				+ d
				+ "</td><td class=\"ccbc\" style=\"color: #5a5a5a;\">"
				+ j
				+ "</td></tr>\n";

		for (int i = 1; i < data_set.size() - 1; i++) {
			row = (Vector) data_set.get(i);

			q = (String) row.get(7);
			r = nf1.format(((Double) row.get(0)).longValue());
			m = nf2.format((Double) row.get(5));
			p = nf2.format((Double) row.get(4));
			d = nf2.format((Double) row.get(6));
			j = nf1.format(((Integer) row.get(3)).longValue());

			tablerows = tablerows
					+ "<tr><td class=\"ccaa\" style=\"padding-left:12px\">" + q
					+ "</td><td class=\"ccac\">$" + r
					+ "</td><td class=\"ccac\">" + m
					+ "</td><td class=\"ccac\">" + p
					+ "&nbsp;</td><td class=\"ccac\">" + d
					+ "</td><td class=\"ccac\">" + j + "</td></tr>\n";
		}

		row = (Vector) data_set.get(data_set.size() - 1);

		test_revenue = ((Double) row.get(0)).longValue();
		test_margin = (Double) row.get(5);

		if ((test_margin.compareTo(Double.valueOf(0.1D)) < 0)
				|| (test_revenue < 1L)) {
			r = "0";
			m = "N/A";
			p = "N/A";
			d = "N/A";
			j = nf1.format(((Integer) row.get(3)).longValue());
		} else {
			r = nf1.format(((Double) row.get(0)).longValue());
			m = nf2.format((Double) row.get(5));
			p = nf2.format((Double) row.get(4));
			d = nf2.format((Double) row.get(6));
			j = nf1.format(((Integer) row.get(3)).longValue());
		}

		tablerows = tablerows
				+ "<tr><td class=\"rh1a\" style=\"padding-left:12px;border-bottom: 1px solid #BFBEC4; color: #5a5a5a;\">Total</td><td class=\"cccc\" style=\"color: #5a5a5a;\">$"
				+ r + "</td><td class=\"cccc\" style=\"color: #5a5a5a;\">" + m
				+ "</td><td class=\"cccc\" style=\"color: #5a5a5a;\">" + p
				+ "&nbsp;</td><td class=\"cccc\" style=\"color: #5a5a5a;\">"
				+ d + "</td><td class=\"cccc\" style=\"color: #5a5a5a;\">" + j
				+ "</td></tr>\n";
		this.Request.setAttribute("WVGPMTable", tablerows);
	}

	private Double getWeeklyFinancials1(Vector data_set) {
		String sql = "";

		if (this.reference_type.equals("JobOwner"))
			sql = "select WWKKey,      isnull(EstimatedCost, 0.0) as EstimatedCost,      isnull(ActualCost, 0.0) as ActualCost,      isnull(Revenue, 0.0) as Revenue,      isnull(TotalJobs, 0) as TotalJobs from datawarehouse.dbo.Dashboard5WeekFinancialSummary      join  datawarehouse.dbo.DashboardSPMtoJobOwner on PocReference = JobOwner and SPMReference = SPM  and YYYYKey = yyyymmdd"
					+ this.where
					+ " and SPMReference = "
					+ this.JobOwner_SPM_reference
					+ " "
					+ "order by YYYYKey  desc, WWKKey desc";
		else {
			sql = "select WWKKey,      isnull(EstimatedCost, 0.0) as EstimatedCost,      isnull(ActualCost, 0.0) as ActualCost,      isnull(Revenue, 0.0) as Revenue,      isnull(TotalJobs, 0) as TotalJobs from datawarehouse.dbo.Dashboard5WeekFinancialSummary "
					+ this.where + " " + "order by YYYYKey  desc, WWKKey desc";
		}

		Double revenue = Double.valueOf(0.0D);
		Double estimated_cost = Double.valueOf(0.0D);
		Double proforma_margin = Double.valueOf(0.0D);
		Double margin = Double.valueOf(0.0D);
		Double actual_cost = Double.valueOf(0.0D);
		Double delta_margin = Double.valueOf(0.0D);

		Double total_revenue = Double.valueOf(0.0D);
		Double total_estimated_cost = Double.valueOf(0.0D);
		Double total_actual_cost = Double.valueOf(0.0D);
		Double max_revenue = Double.valueOf(0.0D);

		Integer total_job_count = Integer.valueOf(0);

		String xAxisWeeks = "";
		String delimiter = "";

		int i = 0;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Vector row = new Vector(12);

				revenue = Double.valueOf(rs.getDouble("Revenue"));
				if (revenue.compareTo(max_revenue) > 0) {
					max_revenue = revenue;
				}
				estimated_cost = Double.valueOf(rs.getDouble("EstimatedCost"));
				actual_cost = Double.valueOf(rs.getDouble("ActualCost"));
				Integer job_count = Integer.valueOf(rs.getInt("TotalJobs"));

				total_revenue = Double.valueOf(total_revenue.doubleValue()
						+ revenue.doubleValue());
				total_estimated_cost = Double.valueOf(total_estimated_cost
						.doubleValue() + estimated_cost.doubleValue());
				total_actual_cost = Double.valueOf(total_actual_cost
						.doubleValue() + actual_cost.doubleValue());
				total_job_count = Integer.valueOf(total_job_count.intValue()
						+ job_count.intValue());

				if (revenue.doubleValue() > 0.0D) {
					proforma_margin = Double.valueOf(1.0D
							- estimated_cost.doubleValue()
							/ revenue.doubleValue());
					margin = Double.valueOf(1.0D - actual_cost.doubleValue()
							/ revenue.doubleValue());
					delta_margin = Double.valueOf(margin.doubleValue()
							- proforma_margin.doubleValue());
				} else {
					proforma_margin = Double.valueOf(0.0D);
					margin = Double.valueOf(0.0D);
					delta_margin = Double.valueOf(0.0D);
				}

				xAxisWeeks = rs.getString("WWKKey") + delimiter + xAxisWeeks;
				delimiter = "|";

				row.add(0, revenue);
				row.add(1, estimated_cost);
				row.add(2, actual_cost);
				row.add(3, job_count);
				row.add(4, proforma_margin);
				row.add(5, margin);
				row.add(6, delta_margin);
				row.add(7, rs.getString("WWKKey"));

				data_set.add(row);
			}

			revenue = total_revenue;
			estimated_cost = total_estimated_cost;
			actual_cost = total_actual_cost;
			Integer job_count = total_job_count;

			if (revenue.doubleValue() > 0.0D) {
				proforma_margin = Double.valueOf(1.0D
						- estimated_cost.doubleValue() / revenue.doubleValue());
				margin = Double.valueOf(1.0D - actual_cost.doubleValue()
						/ revenue.doubleValue());
			} else {
				proforma_margin = Double.valueOf(0.0D);
				margin = Double.valueOf(0.0D);
			}

			Vector row = new Vector(12);

			row.add(0, revenue);
			row.add(1, estimated_cost);
			row.add(2, actual_cost);
			row.add(3, job_count);
			row.add(4, proforma_margin);
			row.add(5, margin);
			row.add(6, delta_margin);
			row.add(7, xAxisWeeks);

			data_set.add(row);

			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException1) {
			}
		}
		return max_revenue;
	}

	private void getWeeklyFinancials() {
		String sql = "select EstimatedCost, ActualCost, Revenue, TotalJobs from datawarehouse.dbo.Dashboard5WeekFinancialSummary "
				+ this.where + " " + "order by WWKKey desc";

		System.out.println(sql);

		Double tRevenue = Double.valueOf(0.0D);
		Double tEC = Double.valueOf(0.0D);
		Double tAC = Double.valueOf(0.0D);
		Integer tJobs = Integer.valueOf(0);

		this.MthRevenue.clear();
		this.MthMargin.clear();
		this.MthProForma.clear();
		this.MthDelta.clear();
		this.MthJobs.clear();

		String xAxisWeeks = "";
		String delimiter = "";
		for (int i = 0; i < 6; i++) {
			this.MthRevenue.add(i, Double.valueOf(0.0D));
			this.MthMargin.add(i, Double.valueOf(0.0D));
			this.MthProForma.add(i, Double.valueOf(0.0D));
			this.MthDelta.add(i, Double.valueOf(0.0D));
			this.MthJobs.add(i, Integer.valueOf(0));
		}

		this.MaxRevenue = Double.valueOf(0.0D);

		int i = 0;

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				Double dRevenue = Double.valueOf(rs.getDouble("Revenue"));
				Double dEC = Double.valueOf(rs.getDouble("EstimatedCost"));
				Double dAC = Double.valueOf(rs.getDouble("ActualCost"));
				Integer iJobs = Integer.valueOf(rs.getInt("TotalJobs"));

				if (dRevenue.compareTo(this.MaxRevenue) > 0) {
					this.MaxRevenue = dRevenue;
				}

				Double dProForma = Double.valueOf(1.0D - dEC.doubleValue()
						/ dRevenue.doubleValue());
				Double dMargin = Double.valueOf(1.0D - dAC.doubleValue()
						/ dRevenue.doubleValue());
				Double dDelta = Double.valueOf(dMargin.doubleValue()
						- dProForma.doubleValue());

				tRevenue = Double.valueOf(tRevenue.doubleValue()
						+ dRevenue.doubleValue());
				tEC = Double.valueOf(tEC.doubleValue() + dEC.doubleValue());
				tAC = Double.valueOf(tAC.doubleValue() + dAC.doubleValue());
				tJobs = Integer.valueOf(tJobs.intValue() + iJobs.intValue());

				this.MthRevenue.remove(i);
				this.MthMargin.remove(i);
				this.MthProForma.remove(i);
				this.MthDelta.remove(i);
				this.MthJobs.remove(i);

				this.MthRevenue.add(i, dRevenue);
				this.MthMargin.add(i, dMargin);
				this.MthProForma.add(i, dProForma);
				this.MthDelta.add(i, dDelta);
				this.MthJobs.add(i, iJobs);

				i++;
			}
			Double dDelta;
			Double dProForma;
			Double dMargin;
			if (tRevenue.equals(Double.valueOf(0.0D))) {
				dProForma = Double.valueOf(0.0D);
				dMargin = Double.valueOf(0.0D);
				dDelta = Double.valueOf(0.0D);
			} else {
				dProForma = Double.valueOf(1.0D - tEC.doubleValue()
						/ tRevenue.doubleValue());
				dMargin = Double.valueOf(1.0D - tAC.doubleValue()
						/ tRevenue.doubleValue());
				dDelta = Double.valueOf(dMargin.doubleValue()
						- dProForma.doubleValue());
			}

			i = 5;

			this.MthRevenue.remove(i);
			this.MthMargin.remove(i);
			this.MthProForma.remove(i);
			this.MthDelta.remove(i);
			this.MthJobs.remove(i);

			this.MthRevenue.add(i, tRevenue);
			this.MthMargin.add(i, dMargin);
			this.MthProForma.add(i, dProForma);
			this.MthDelta.add(i, dDelta);
			this.MthJobs.add(i, tJobs);

			rs.close();
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
			try {
				rs.close();
			} catch (SQLException localSQLException1) {
			}
		}
	}

	public static void main(String[] args) {
		DashBoard1 x = new DashBoard1();
	}
}
