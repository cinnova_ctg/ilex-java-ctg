package com.ilex.reports.action.pvsUsage;

import java.io.File;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ilex.reports.action.operationSummary.DataAccess;
import com.ilex.reports.bean.pvsUsage.PVSCoutnBean;

public class PvsUsageAction extends DispatchAction {

	DataAccess da = new DataAccess();
	String WhereClause = "";
	private String pm = "PM";
	private String pvs = "PVS";
	private String allJobs = "allJobs";

	public ActionForward pvsSummary(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "SUCCESS";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		managePageRequest1(request);

		return mapping.findForward(forward_key);
	}

	String managePageRequest1(HttpServletRequest request) {
		String x = "";

		Map inputs = getInputParameters(request);

		int view = 0;

		this.WhereClause = "";

		this.WhereClause = buildWhereClause(inputs);

		ArrayList<PVSCoutnBean> justificationCountList = this
				.getPvsCountListByJustification();
		request.setAttribute("PVSJustificationCoutnBeanLst",
				justificationCountList);

		ArrayList<PVSCoutnBean> pmCountList = this.getPvsCountListByPM(request,
				this.WhereClause);
		pmCountList = this.getAllJobsCountListByPM(request, this.WhereClause,
				pmCountList);
		request.setAttribute("PVSCoutnBeanListByPM", pmCountList);

		if (inputs.get("view") != null) {
			view = Integer.parseInt((String) inputs.get("view"));
			request.setAttribute("view", Integer.valueOf(view));
		}

		return x;
	}

	private ArrayList<PVSCoutnBean> getAllJobsCountListByPM(
			HttpServletRequest request, String wc, ArrayList<PVSCoutnBean> lst) {

		ResultSet r1 = null;

		r1 = getPvsCountBy(allJobs, 7, request.getParameter("projectStatus"),
				wc);
		lst = populatePVSBeanByPM(7, r1, lst, allJobs);
		r1 = getPvsCountBy(allJobs, 30, request.getParameter("projectStatus"),
				wc);
		lst = populatePVSBeanByPM(30, r1, lst, allJobs);
		r1 = getPvsCountBy(allJobs, 60, request.getParameter("projectStatus"),
				wc);
		lst = populatePVSBeanByPM(60, r1, lst, allJobs);
		r1 = getPvsCountBy(allJobs, 90, request.getParameter("projectStatus"),
				wc);
		lst = populatePVSBeanByPM(90, r1, lst, allJobs);
		r1 = getPvsCountBy(allJobs, 1, request.getParameter("projectStatus"),
				wc);
		lst = populatePVSBeanByPM(1, r1, lst, allJobs);

		return lst;
	}

	private ArrayList<PVSCoutnBean> getPvsCountListByPM(
			HttpServletRequest request, String wc) {
		ArrayList<PVSCoutnBean> lst = new ArrayList<PVSCoutnBean>();

		ResultSet r1 = null;

		r1 = getPvsCountBy(pm, 7, request.getParameter("projectStatus"), wc);
		lst = populatePVSBeanByPM(7, r1, lst, pvs);
		r1 = getPvsCountBy(pm, 30, request.getParameter("projectStatus"), wc);
		lst = populatePVSBeanByPM(30, r1, lst, pvs);
		r1 = getPvsCountBy(pm, 60, request.getParameter("projectStatus"), wc);
		lst = populatePVSBeanByPM(60, r1, lst, pvs);
		r1 = getPvsCountBy(pm, 90, request.getParameter("projectStatus"), wc);
		lst = populatePVSBeanByPM(90, r1, lst, pvs);
		r1 = getPvsCountBy(pm, 1, request.getParameter("projectStatus"), wc);
		lst = populatePVSBeanByPM(1, r1, lst, pvs);

		return lst;
	}

	private ArrayList<PVSCoutnBean> populatePVSBeanByPM(int days, ResultSet rs,
			ArrayList<PVSCoutnBean> lst, String jobType) {
		try {
			PVSCoutnBean bean = new PVSCoutnBean();
			boolean beanFound = false;
			while (rs.next()) {

				// lst.get(Integer.parseInt(rs.getString("ap_aa_pm_id")== null ?
				// "0" : rs.getString("ap_aa_pm_id")));
				for (int i = 0; i < lst.size(); i++) {
					bean = lst.get(i);
					if (bean.getPmId() == (Integer.parseInt(rs
							.getString("pmId") == null ? "0" : rs
							.getString("pmId")))) {
						beanFound = true;
						break;
					}
				}
				if (!beanFound) {
					bean = new PVSCoutnBean();
				}
				bean.setPmId(Integer.parseInt(rs.getString("pmId") == null ? "0"
						: rs.getString("pmId")));
				bean.setPmName((rs.getString("pmName") == null ? "NA" : rs
						.getString("pmName")));

				if (days == 7 && jobType.equals(allJobs)) {
					bean.setDays_7_count_all_jobs(rs.getInt("col_count"));
				} else if (days == 7 && jobType.equals(pvs)) {
					bean.setDays_7_count(rs.getInt("col_count"));
				} else if (days == 30 && jobType.equals(allJobs)) {
					bean.setDays_30_count_all_jobs(rs.getInt("col_count"));
				} else if (days == 30 && jobType.equals(pvs)) {
					bean.setDays_30_count(rs.getInt("col_count"));
				} else if (days == 60 && jobType.equals(allJobs)) {
					bean.setDays_60_count_all_jobs(rs.getInt("col_count"));
				} else if (days == 60 && jobType.equals(pvs)) {
					bean.setDays_60_count(rs.getInt("col_count"));
				} else if (days == 90 && jobType.equals(allJobs)) {
					bean.setDays_90_count_all_jobs(rs.getInt("col_count"));
				} else if (days == 90 && jobType.equals(pvs)) {
					bean.setDays_90_count(rs.getInt("col_count"));
				} else if (days == 1 && jobType.equals(allJobs)) {
					bean.setYearToDate_count_all_jobs(rs.getInt("col_count"));
				} else if (days == 1 && jobType.equals(pvs)) {
					bean.setYearToDate_count(rs.getInt("col_count"));
				}

				if (!beanFound) { // add only if new bean created otherwise
									// using bean from this list that will be
									// updated automatically

					lst.add(bean);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return lst;
	}

	private ArrayList<PVSCoutnBean> getPvsCountListByJustification() {

		ArrayList<PVSCoutnBean> lst = new ArrayList<PVSCoutnBean>();

		String query = " select  lm_pe_code, c.lm_pe_id , " // -- lm_gc_code,
				+ " SUM(CASE WHEN lm_po_complete_date >= getdate() -7 THEN 1 ELSE 0 END) AS Trailing_7, "
				+ " SUM(CASE WHEN lm_po_complete_date >= getdate() -30 THEN 1 ELSE 0 END) AS Trailing_30, SUM(CASE WHEN lm_po_complete_date >= getdate() -60 THEN 1 ELSE 0 END) AS Trailing_60, "
				+ " SUM(CASE WHEN lm_po_complete_date >= getdate() -90 THEN 1 ELSE 0 END) AS Trailing_90, COUNT(*) AS yearToDate "
				+ " from lm_purchase_order as a "
				+ " left outer join lm_po_geographic_constraint as b on a.lm_po_geographic_constraint = b.lm_gc_id "
				+ " left outer JOIN lm_purchase_order_exception AS c ON a.lm_po_pvs_justification = c.lm_pe_id "
				+ " where datepart(year,lm_po_complete_date) >= datepart(year,getdate()) AND lm_pe_id IS NOT NULL AND lm_pe_id <> 1 AND lm_po_type = 'P' "
				+ " GROUP BY lm_pe_code, c.lm_pe_id  " // --, lm_gc_code
				+ " UNION ALL "
				+ " select  /*lm_pe_code,*/ '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+lm_gc_code , c.lm_pe_id, "//if removed any &nbsp;, then dont forget to change the substring function while retrieving it from web or simply search justificationCode.substring 
				+ " SUM(CASE WHEN lm_po_complete_date >= getdate() -7 THEN 1 ELSE 0 END) AS Trailing_7, "
				+ " SUM(CASE WHEN lm_po_complete_date >= getdate() -30 THEN 1 ELSE 0 END) AS Trailing_30, SUM(CASE WHEN lm_po_complete_date >= getdate() -60 THEN 1 ELSE 0 END) AS Trailing_60, "
				+ " SUM(CASE WHEN lm_po_complete_date >= getdate() -90 THEN 1 ELSE 0 END) AS Trailing_90, COUNT(*) AS yearToDate "
				+ " from lm_purchase_order as a "
				+ " left outer join lm_po_geographic_constraint as b on a.lm_po_geographic_constraint = b.lm_gc_id "
				+ " left outer JOIN lm_purchase_order_exception AS c ON a.lm_po_pvs_justification = c.lm_pe_id "
				+ " where datepart(year,lm_po_complete_date) >= datepart(year,getdate()) AND lm_pe_id IS NOT NULL AND lm_pe_id <> 1 AND lm_po_type = 'P' AND lm_gc_code IS NOT NULL "
				+ " GROUP BY /*lm_pe_code,*/ lm_gc_code, c.lm_pe_id ";

		PVSCoutnBean bean;
		ResultSet rs = this.da.getResultSet(query);
		try {
			while (rs.next()) {
				bean = new PVSCoutnBean();
				bean.setJustifcationName(rs.getString("lm_pe_code"));
				bean.setJustifcationId(rs.getInt("lm_pe_id"));
				bean.setDays_7_count(rs.getInt("Trailing_7"));
				bean.setDays_30_count(rs.getInt("Trailing_30"));
				bean.setDays_60_count(rs.getInt("Trailing_60"));
				bean.setDays_90_count(rs.getInt("Trailing_90"));
				bean.setYearToDate_count(rs.getInt("yearToDate"));
				lst.add(bean);
			}
		} catch (Exception e) {

		}
		return lst;
	}

	private ResultSet getPvsCountBy(String by, int days, String projectstatus,
			String wc) {

		if (projectstatus.equals("in_work")) {
			projectstatus = "'In work'";
		} else if (projectstatus.equals("scheduled")) {
			projectstatus = "'Scheduled'";
		} else {
			projectstatus = "'Closed', 'Complete'";
		}

		String dayOrYear = "DAY";
		if (days == 1) {
			dayOrYear = "YEAR";
		}

		String query = "";

		if (by.equals(pm)) {
			query = "SELECT pmDtl.lo_pc_id AS pmId , (pmDtl.lo_pc_first_name + ' '+ pmDtl.lo_pc_last_name) AS pmName,  count(lm_js_id) col_count "
					+ " FROM lm_job "
					+ " INNER JOIN lm_purchase_order ON lm_js_id = lm_po_js_id "
					+ " LEFT OUTER JOIN ap_job_audit_analysis ON lm_js_id = ap_aa_js_id "
					+ " LEFT OUTER JOIN lm_purchase_order_exception ON lm_pe_id = lm_po_pvs_justification  "
					+ " LEFT OUTER JOIN lx_appendix_main ON lx_pr_id = lm_js_pr_id "
					+ " LEFT OUTER JOIN lm_appendix_poc ON lm_ap_pr_id = lx_pr_id "
					+ " LEFT OUTER JOIN lo_poc pmDtl ON lm_ap_pc_id = pmDtl.lo_pc_id "
					+ " WHERE DATEDIFF("
					+ dayOrYear
					+ ", lm_js_create_date, getdate() ) <= "
					+ days
					+ " AND lm_po_partner_id IS NOT NULL "
					+ " AND lm_js_prj_display_status IN ("
					+ projectstatus
					+ ") "
					+ " AND lm_pe_id IS NOT NULL AND lm_pe_id <> 1 "
					+ // 1 means Not Applicable
					" AND pmDtl.lo_pc_id IS NOT NULL  "
					+ wc
					+ " GROUP BY pmDtl.lo_pc_id,  (pmDtl.lo_pc_first_name + ' '+ pmDtl.lo_pc_last_name) "
					+ " ORDER BY col_count DESC ";
		} else if (by.equals(allJobs)) {
			query = "SELECT pmDtl.lo_pc_id AS pmId , (pmDtl.lo_pc_first_name + ' '+ pmDtl.lo_pc_last_name) AS pmName,  count(lm_js_id) col_count  "
					+ " FROM lm_job   "
					+ " INNER JOIN lm_purchase_order ON lm_js_id = lm_po_js_id  "
					+ " LEFT OUTER JOIN ap_job_audit_analysis ON lm_js_id = ap_aa_js_id  "
					+ " LEFT OUTER JOIN lm_purchase_order_exception ON lm_pe_id = lm_po_pvs_justification  "
					+ " LEFT OUTER JOIN lx_appendix_main ON lx_pr_id = lm_js_pr_id "
					+ " LEFT OUTER JOIN lm_appendix_poc ON lm_ap_pr_id = lx_pr_id "
					+ " LEFT OUTER JOIN lo_poc pmDtl ON lm_ap_pc_id = pmDtl.lo_pc_id  "
					+ " WHERE DATEDIFF("
					+ dayOrYear
					+ ", lm_js_create_date, getdate() ) <= "
					+ days
					+ "  "
					+ " AND lm_js_prj_display_status IN ("
					+ projectstatus
					+ ")   "
					+ " AND lm_pe_id IS NOT NULL AND lm_pe_id <> 1 "
					+ // 1 means Not Applicable
					" AND pmDtl.lo_pc_id IS NOT NULL     "
					+ wc
					+ " GROUP BY pmDtl.lo_pc_id, (pmDtl.lo_pc_first_name + ' '+ pmDtl.lo_pc_last_name)   "
					+ " ORDER BY col_count DESC ";
		}

		return this.da.getResultSet(query);

	}

	public ActionForward inline_audit_view(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "audit_view";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		managePageRequest3(request);

		return mapping.findForward(forward_key);
	}

	String managePageRequest3(HttpServletRequest request) {
		String x = "";

		Map inputs = getInputParameters(request);
		int days = 60;
		try {
			days = Integer.parseInt(request.getParameter("days"));
			// Integer.parseInt((String) inputs.get("days"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.WhereClause = "";

		this.WhereClause = buildWhereClause(inputs);

		getFlatData(request, this.WhereClause, days);

		request.setAttribute("days", Integer.valueOf(days));

		return x;
	}

	void getFlatData(HttpServletRequest request, String wc, int days) {
		String sql;

		String projectstatus = request.getParameter("projectStatus");

		if (projectstatus.equals("in_work")) {
			projectstatus = "'In work'";
		} else if (projectstatus.equals("scheduled")) {
			projectstatus = "'Scheduled'";
		} else {
			projectstatus = "'Closed', 'Complete'";
		}

		String justificationId = " = "
				+ request.getParameter("justificationId");
		if (request.getParameter("justificationId").toString().equals("0")) {
			justificationId = " IS NULL ";
		}

		String justificationCode = "";
		if (request.getParameter("justificationName") != null) {			
			justificationCode = request.getParameter("justificationName").trim();
		}
		
		/*
		 * Encoding to remove extra spaces
		 * */
		try {
			justificationCode = URLEncoder.encode(justificationCode, "ISO-8859-1");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		justificationCode = justificationCode.replace("%A0", "");
		/*
		 * Decoding to after removing extra spaces
		 * */
		try {
			justificationCode = URLDecoder.decode(justificationCode, "ISO-8859-1");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		
		sql = "declare @period int; "
				+ " declare @justification varchar(max); "

				+ " set @period = "
				+ days
				+ " "
				+ " set @justification = '"
				+ justificationCode
				+ "' "

				+ " select  DISTINCT lm_po_id, Customer, Project, Justification, [Job/Ticket], PVS, "
				+ " City, State, Zip, [Date], [Owner], PM, cp_partner_id, "
				+ " siteId, appendixId, ownerId,  " 
				+ " cast( substring((Select ','+cp_corecomp_name  AS [text()] "
				+ " from ( "
				+ " select lm_po_id, lx_pr_end_customer as Customer, lx_pr_title as Project, 'Justification' = (CASE WHEN lm_gc_code IS NOT NULL THEN lm_gc_code ELSE lm_pe_code END),  /*AS Competencies,*/  lm_js_title as [Job/Ticket], k.lo_om_division as PVS, "
				+ " lm_si_city as City, lm_si_state as State, lm_si_zip_code as Zip, lm_js_create_date as [Date], g.lo_pc_first_name+' '+g.lo_pc_last_name as [Owner], i.lo_pc_first_name+' '+i.lo_pc_last_name AS PM, cp_corecomp_name, "
				+ "	cp_partner_id,  d.lm_js_si_id AS siteId, d.lm_js_pr_id AS appendixId, d.lm_js_created_by AS ownerId "
				+ " from lm_purchase_order as a "
				+ " left outer join lm_po_geographic_constraint as b on a.lm_po_geographic_constraint = b.lm_gc_id "
				+ " left outer JOIN lm_purchase_order_exception AS c ON a.lm_po_pvs_justification = c.lm_pe_id "
				+ " join lm_job as d on a.lm_po_js_id = d.lm_js_id "
				+ " join lx_appendix_main as e on d.lm_js_pr_id = e.lx_pr_id "
				+ " join lm_site_detail as f on d.lm_js_si_id = f.lm_si_id "
				+ " join lo_poc as g on d.lm_js_created_by = g.lo_pc_id "
				+ " join lm_appendix_poc as h on d.lm_js_pr_id = h.lm_ap_pr_id "
				+ " join lo_poc as i on h.lm_ap_pc_id = i.lo_pc_id "
				+ " join cp_partner as j on a.lm_po_partner_id = j.cp_partner_id "
				+ " join lo_organization_main as k on j.cp_partner_om_id = k.lo_om_id "
				+ " left outer join lm_gc_core_competency as l on a.lm_po_id = l.lm_gc_po_id "
				+ " left outer join cp_corecompetency as m on l.lm_gc_corecomp_id = m.cp_corecomp_id "
				+ " where lm_po_complete_date >= getdate() - @period AND "
				+ " lm_pe_id IS NOT NULL AND "
				+ " lm_pe_id <> 1 AND "
				+ " lm_po_type = 'P' AND "
				+ " (lm_pe_code = @justification OR lm_gc_code = @justification)) as a "
				+ " WHERE a.lm_po_id = b.lm_po_id "
				+ " For XML PATH ('')),2, 1000) AS varchar (1000)) AS CoreCompetency "
				+ " FROM (select lm_po_id, lx_pr_end_customer as Customer, lx_pr_title as Project, 'Justification' = (CASE WHEN lm_gc_code IS NOT NULL THEN lm_gc_code ELSE lm_pe_code END),  lm_js_title as [Job/Ticket], k.lo_om_division as PVS, "
				+ " lm_si_city as City, lm_si_state as State, lm_si_zip_code as Zip, lm_js_create_date as [Date], g.lo_pc_first_name+' '+g.lo_pc_last_name as [Owner], i.lo_pc_first_name+' '+i.lo_pc_last_name AS PM , cp_partner_id, "
				+ "  lm_js_si_id AS siteId, lm_js_pr_id AS appendixId, lm_js_created_by AS ownerId "
				+ " from lm_purchase_order as a "
				+ " left outer join lm_po_geographic_constraint as b on a.lm_po_geographic_constraint = b.lm_gc_id "
				+ " left outer JOIN lm_purchase_order_exception AS c ON a.lm_po_pvs_justification = c.lm_pe_id "
				+ " join lm_job as d on a.lm_po_js_id = d.lm_js_id "
				+ " join lx_appendix_main as e on d.lm_js_pr_id = e.lx_pr_id "
				+ " join lm_site_detail as f on d.lm_js_si_id = f.lm_si_id "
				+ " join lo_poc as g on d.lm_js_created_by = g.lo_pc_id "
				+ " join lm_appendix_poc as h on d.lm_js_pr_id = h.lm_ap_pr_id "
				+ " join lo_poc as i on h.lm_ap_pc_id = i.lo_pc_id "
				+ " join cp_partner as j on a.lm_po_partner_id = j.cp_partner_id "
				+ " join lo_organization_main as k on j.cp_partner_om_id = k.lo_om_id "
				+ " left outer join lm_gc_core_competency as l on a.lm_po_id = l.lm_gc_po_id "
				+ " left outer join cp_corecompetency as m on l.lm_gc_corecomp_id = m.cp_corecomp_id "
				+ " where lm_po_complete_date >= getdate() - @period AND "
				+ " lm_pe_id IS NOT NULL AND "
				+ " lm_pe_id <> 1 AND "
				+ " lm_po_type = 'P' AND "
				+ " (lm_pe_code = @justification OR lm_gc_code = @justification)) as b ";

		Vector rows = new Vector();
		String[] row = new String[16];

		int i = 0;
		ResultSet rs = this.da.getResultSet(sql);
		try {

			while (rs.next()) {
				row = new String[16];

				row[0] = (rs.getString("Customer") == null ? "" : rs
						.getString("Customer"));
				row[1] = (rs.getString("Project") == null ? "" : rs
						.getString("Project"));
				row[2] = (rs.getString("Justification") == null ? "NA" : rs
						.getString("Justification"));
				row[3] = (rs.getString("Job/Ticket") == null ? "" : rs
						.getString("Job/Ticket"));
				row[4] = (rs.getString("PVS") == null ? "" : rs
						.getString("PVS"));// will show PVS here
				row[5] = (rs.getString("City") == null ? "" : rs
						.getString("City"));

				row[6] = (rs.getString("Date") == null ? "" : rs
						.getString("Date"));
				row[7] = (rs.getString("Owner") == null ? "" : rs
						.getString("Owner"));
				row[8] = (rs.getString("PM") == null ? "" : rs.getString("PM"));

				row[9] = (rs.getString("State") == null ? "" : rs
						.getString("State"));
				row[10] = (rs.getString("Zip") == null ? "" : rs
						.getString("Zip"));

				row[11] = (rs.getString("cp_partner_id") == null ? "" : rs
						.getString("cp_partner_id"));
				row[12] = rs.getString("siteId");
				row[13] = rs.getString("appendixId");
				row[14] = rs.getString("ownerId");
				row[15] = (rs.getString("CoreCompetency") == null ? "" : rs
						.getString("CoreCompetency"));

				rows.add(i++, row);
			}

			rs.close();

			request.setAttribute("rows", rows);
		} catch (SQLException e) {
			System.out.println(e + " " + sql);
		}
	}

	private String buildWhereClause(Map inputs) {
		String wc = "";
		String delimiter = "";

		Vector expressions = new Vector();
		int i = 0;

		boolean lx_pr_id = inputs.get("lx_pr_id") != null;
		boolean pm = inputs.get("pm") != null;
		boolean bdm = inputs.get("bdm") != null;
		boolean from_date = (inputs.get("from_date") != null)
				&& (!((String) inputs.get("from_date")).equals(""));
		boolean to_date = (inputs.get("to_date") != null)
				&& (!((String) inputs.get("to_date")).equals(""));
		boolean view = inputs.get("view") != null;
		boolean jo = inputs.get("jo") != null;
		boolean sl = inputs.get("sl") != null;

		if (lx_pr_id) {
			expressions.add(i++,
					"lm_js_pr_id = " + (String) inputs.get("lx_pr_id"));
		}

		if (pm) {
			expressions.add(i++, "ap_aa_pm_id = " + (String) inputs.get("pm"));
		}

		if (bdm) {
			expressions
					.add(i++, "ap_aa_bdm_id = " + (String) inputs.get("bdm"));
		}

		if (from_date) {
			expressions.add(i++,
					"ap_aa_js_date >= '" + (String) inputs.get("from_date")
							+ "'");
		}

		if (to_date) {
			expressions.add(i++, "ap_aa_js_date > dateadd(dd,1,'"
					+ (String) inputs.get("to_date") + "')");
		}

		if (jo) {
			expressions.add(i++,
					"ap_aa_owner_id = " + (String) inputs.get("jo"));
		}

		sl = true;

		wc = " ";
		delimiter = " and ";
		for (int j = 0; j < expressions.size(); j++) {
			wc = wc + delimiter + expressions.get(j);
		}

		return wc;
	}

	Map getInputParameters(HttpServletRequest request) {
		Vector requestParameters = initializeInputParameters();

		Map inputs = new HashMap();

		for (int i = 0; i < requestParameters.size(); i++) {
			String parameter = (String) requestParameters.get(i);
			String value = request.getParameter(parameter);

			if ((value != null) && (value.trim().length() > 0)) {
				inputs.put(parameter, value);
				request.setAttribute(parameter, value);
			}
		}

		request.setAttribute("requestParameters", requestParameters);

		return inputs;
	}

	Vector initializeInputParameters() {
		Vector inputParameters = new Vector();

		inputParameters.clear();
		inputParameters.add(0, "lx_pr_id");
		inputParameters.add(1, "bdm");
		inputParameters.add(2, "pm");
		inputParameters.add(3, "from_date");
		inputParameters.add(4, "to_date");
		inputParameters.add(5, "view");
		inputParameters.add(6, "jo");

		return inputParameters;
	}

	public ActionForward pvsExport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		
		try {

			// String filename =
			// request.getSession().getAttribute("path")+"pvsUsage_"+System.currentTimeMillis()+".csv";
			String path = request.getSession().getAttribute("path").toString();
			String name = "pvsUsage_" + System.currentTimeMillis() + ".csv";
			String fileName = path + name;
			Vector rows = (Vector) request.getSession().getAttribute("rows");
			request.setAttribute("rows", rows);
			File f = new File(fileName);
			if (!f.exists()) {
				f.createNewFile();
			}
			FileWriter fw = new FileWriter(f);

			fw.append("Customer");
			fw.append(',');
			fw.append("Project");
			fw.append(',');
			fw.append("Justification");
			fw.append(',');
			fw.append("Core Competency");
			fw.append(',');
			fw.append("Job/Ticket");
			fw.append(',');
			fw.append("PVS");
			fw.append(',');
			fw.append("City");
			fw.append(',');
			fw.append("State");
			fw.append(',');
			fw.append("Zip");
			fw.append(',');
			fw.append("Date");
			fw.append(',');
			fw.append("Owner");
			fw.append(',');
			fw.append("PM");
			fw.append('\n');

			for (int i = 0; i < rows.size(); i++) {
				String[] row = (String[]) rows.get(i);

				fw.append('"' + row[0] + '"');
				fw.append(',');
				fw.append('"' + row[1] + '"');
				fw.append(',');
				fw.append('"' + row[2] + '"');
				fw.append(',');
				fw.append('"' + row[15] + '"');
				fw.append(',');
				fw.append('"' + row[3] + '"');
				fw.append(',');
				fw.append('"' + row[4] + '"');
				fw.append(',');
				fw.append('"' + row[5] + '"');
				fw.append(',');
				fw.append('"' + row[9] + '"');
				fw.append(',');
				fw.append('"' + row[10] + '"');
				fw.append(',');
				fw.append('"' + row[6] + '"');
				fw.append(',');
				fw.append('"' + row[7] + '"');
				fw.append(',');
				fw.append('"' + row[8] + '"');
				fw.append('\n');

			}

			fw.flush();
			fw.close();
			response.sendRedirect(request.getSession().getAttribute("URL")
					.toString()
					+ name);
			System.out
					.println("Csv Report for PVS Usage created Successfully.");

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return mapping.findForward("audit_view");
	}

}