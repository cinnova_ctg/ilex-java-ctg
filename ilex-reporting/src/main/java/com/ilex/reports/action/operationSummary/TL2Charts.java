package com.ilex.reports.action.operationSummary;

import java.sql.ResultSet;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.mind.fw.core.dao.util.DBUtil;

public class TL2Charts extends DispatchAction {
	String[] Months = { "", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
			"Aug", "Sep", "Oct", "Nov", "Dec" };
	DataAccess da = new DataAccess();

	public ActionForward purchase_chart1(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String contextpath = request.getContextPath();
		String spm = request.getParameter("spm");
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		if ((spm == null) || (spm.equals(""))) {
			spm = "%";
		}
		String period = request.getParameter("period");
		if ((period == null) || (period.equals(""))) {
			period = "30";
		}

		Vector rows = getPurchasingData(period, spm);

		request.setAttribute("rows", rows);

		String chart_xml_data = getCorrespondingVGP(spm);
		String ms_line_chart = "<div id='purchase_chart1' align='center' style='padding:15px;'>Waiting...</div>\n<script type='text/javascript'>\nvar purchase_chart1 = ~data_url~\nvar myChart = new FusionCharts('"
				+ contextpath
				+ "/fc/MSLine.swf', 'purchase_chart1', '500', '300', '0', '0');\nmyChart.setDataXML(purchase_chart1);\nmyChart.setTransparent(true);\nmyChart.render('purchase_chart1');\n</script>";

		ms_line_chart = ms_line_chart.replace("~data_url~", chart_xml_data);
		request.setAttribute("xml_data", ms_line_chart);

		return mapping.findForward("purchase_chart1");
	}

	Vector<String[]> getPurchasingData(String period, String spm) {
		String current_job_owner = "";
		String[] row = { "", "00.0", "00.0", "00.0", "0" };
		Vector rows = new Vector();

		String sql = "select ac_os_job_owner, case when ac_op_type='M' then '1' when ac_op_type='S' then '2' when ac_op_type='P' then '3' end as ac_op_type, ac_op_total, convert(decimal(4,1), 100*ac_op_percentage) as ac_op_percentage from datawarehouse.dbo.ac_fact2_ops_summary_po_spm_job_owner where ac_os_spm = '"
				+ spm
				+ "' and ac_os_day_index = "
				+ period
				+ " and ac_op_total > 2"
				+ "order by ac_os_job_owner, ac_op_type ";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			if (rs.next()) {
				current_job_owner = rs.getString("ac_os_job_owner");
				row[0] = current_job_owner;
				row[rs.getInt("ac_op_type")] = (rs
						.getString("ac_op_percentage") + "%");
				row[4] = rs.getString("ac_op_total");

				while (rs.next()) {
					if (!current_job_owner.equals(rs
							.getString("ac_os_job_owner"))) {
						rows.add(row);
						row = new String[5];
						row[1] = "00.0";
						row[2] = "00.0";
						row[3] = "00.0";
						current_job_owner = rs.getString("ac_os_job_owner");
						row[0] = current_job_owner;
						row[rs.getInt("ac_op_type")] = (rs
								.getString("ac_op_percentage") + "%");
						row[4] = rs.getString("ac_op_total");
					} else {
						row[rs.getInt("ac_op_type")] = (rs
								.getString("ac_op_percentage") + "%");
					}
				}
				if (row.length > 0) {
					rows.add(row);
				}
			}
			rs.close();
		} catch (Exception e) {
			System.out.print(e);
		}

		return rows;
	}

	String getCorrespondingVGP(String spm) {
		String sql = "";

		String spm_where = "and ac_du_name != 'Shroder, Charles'";

		if (!spm.equals("%")) {
			spm_where = "and ac_du_name = '" + spm + "'";
		}

		sql = "use datawarehouse; select datepart(mm, sc_dd_month_mm01yyyy), sc_dd_month_mm01yyyy,                       sum(isnull(ac_ru_pro_forma,0.0)), sum(isnull(ac_ru_expense,0.0)), sum(isnull(ac_ru_revenue,0.0)),                       1-(sum(isnull(ac_ru_pro_forma,0.0))/sum(isnull(ac_ru_revenue,0.0))), 1-(sum(isnull(ac_ru_expense,0.0))/sum(isnull(ac_ru_revenue,0.0))),                       (1-(sum(isnull(ac_ru_expense,0.0))/sum(isnull(ac_ru_revenue,0.0))))-(1-(sum(isnull(ac_ru_pro_forma,0.0))/sum(isnull(ac_ru_revenue,0.0))))  from dbo.ac_dimensions_job join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id join dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id and ac_du_spm = 1 join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy  where ac_cl_minor2 = 'Billable' and ac_jd_date_closed > dateadd(mm, -11, getdate()) "
				+ spm_where
				+ " "
				+ " group by datepart(mm, sc_dd_month_mm01yyyy), sc_dd_month_mm01yyyy having sum(isnull(ac_ru_revenue,0.0)) > 0.0";

		String xml = "";
		String xml_static1 = " \"<chart caption='MM vs VGP Trend' xAxisName='Month' yAxisName='VGPM Delta/MM %' showValues='0' numberPrefix='' borderColor='#ffffff'> \\\n";

		String xml_static2 = "<styles><definition><style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' /></definition><application><apply toObject='Canvas' styles='CanvasAnim' /></application></styles></chart>\";";

		String s1 = "<dataset seriesName='Pro Forma'>";
		String s2 = "<dataset seriesName='Actual'>";
		String s3 = "<dataset seriesName='Delta'>";
		String s4 = "<dataset seriesName='MM Usage'>";
		String s5 = "<dataset seriesName='SP Usage'>";
		String cat = "<categories>";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				cat = cat + "<category label='" + this.Months[rs.getInt(1)]
						+ "' />";
				s1 = s1 + "<set value='" + rs.getString(6) + "' />";
				s2 = s2 + "<set value='" + rs.getString(7) + "' />";
				s3 = s3 + "<set value='" + rs.getString(8) + "' />";
			}

			cat = cat + "</categories> \\\n";
			s1 = s1 + "</dataset> \\\n";
			s2 = s2 + "</dataset> \\\n";
			s3 = s3 + "</dataset> \\\n";
		} catch (Exception localException) {
		}

		sql = "use datawarehouse; select month, mm, sp from ac_fact2_ops_summary_po_by_spm_month order by month";
		rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				s4 = s4 + "<set value='" + rs.getString("mm") + "' />";
				s5 = s5 + "<set value='" + rs.getString("sp") + "' />";
			}

			s4 = s4 + "</dataset> \\\n";
			s5 = s5 + "</dataset> \\\n";
		} catch (Exception localException1) {
		}

		xml = xml_static1 + cat + s1 + s2 + s3 + s4 + s5 + xml_static2;
		return xml;
	}

	public ActionForward profitability_chart1(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String contextpath = request.getContextPath();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		String ms_line_chart = "<div id='profitability_chart1' align='center' style='padding:15px;'>Waiting...</div>\n<script type='text/javascript'>\nvar profitability_chart1 = ~data_url~\nvar myChart = new FusionCharts('"
				+ contextpath
				+ "/fc/MSLine.swf?noCache=~tm~', 'profitability_chart1', '500', '300', '0', '0');\nmyChart.setDataXML(profitability_chart1);\nmyChart.setTransparent(true);\nmyChart.render('profitability_chart1');\n</script>";

		String spm = request.getParameter("spm");
		if ((spm == null) || (spm.equals(""))) {
			spm = "%";
		}

		ms_line_chart = ms_line_chart.replace("~data_url~", getChartXML1(spm));

		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0L);
		response.setContentType("text/html");
		response.getWriter().write(ms_line_chart);
		response.flushBuffer();

		return null;
	}

	String getChartXML1(String spm) {
		String sql = "";

		if (spm.equals("%")) {
			sql = "use datawarehouse; select datepart(mm, sc_dd_month_mm01yyyy), sc_dd_month_mm01yyyy, sum(isnull(ac_ru_pro_forma,0.0)), sum(isnull(ac_ru_expense,0.0)), sum(isnull(ac_ru_revenue,0.0)), 1-(sum(isnull(ac_ru_pro_forma,0.0))/sum(isnull(ac_ru_revenue,0.0))), 1-(sum(isnull(ac_ru_expense,0.0))/sum(isnull(ac_ru_revenue,0.0)))   from dbo.ac_dimensions_job join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id join dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id and ac_du_spm = 1 join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy  where ac_cl_minor2 = 'Billable' and ac_jd_date_closed > dateadd(mm, -11, getdate()) and ac_du_name != 'Shroder, Charles'  group by datepart(mm, sc_dd_month_mm01yyyy), sc_dd_month_mm01yyyy having sum(isnull(ac_ru_revenue,0.0)) > 0.0";
		} else {
			sql = "use datawarehouse; select datepart(mm, sc_dd_month_mm01yyyy), sc_dd_month_mm01yyyy, sum(isnull(ac_ru_pro_forma,0.0)), sum(isnull(ac_ru_expense,0.0)), sum(isnull(ac_ru_revenue,0.0)), 1-(sum(isnull(ac_ru_pro_forma,0.0))/sum(isnull(ac_ru_revenue,0.0))), 1-(sum(isnull(ac_ru_expense,0.0))/sum(isnull(ac_ru_revenue,0.0)))   from dbo.ac_dimensions_job join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id join dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id and ac_du_spm = 1 join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy  where ac_cl_minor2 = 'Billable' and ac_jd_date_closed > dateadd(mm, -11, getdate()) and ac_du_name = '"
					+ spm
					+ "'"
					+ " group by datepart(mm, sc_dd_month_mm01yyyy), sc_dd_month_mm01yyyy having sum(isnull(ac_ru_revenue,0.0)) > 0.0";
		}

		String xml = "";
		String xml_static1 = " \"<chart caption='Profitability' xAxisName='Month' yAxisName='VGPM' showValues='0' numberPrefix='' borderColor='#ffffff'> \\\n";

		String xml_static2 = "<trendlines><line startValue='0.55' color='91C728' displayValue='Target' showOnTop='1'/></trendlines><styles><definition><style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' /></definition><application><apply toObject='Canvas' styles='CanvasAnim' /></application></styles></chart>\";";

		String s1 = "<dataset seriesName='Pro Forma'>";
		String s2 = "<dataset seriesName='Actual'>";
		String cat = "<categories>";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				cat = cat + "<category label='" + this.Months[rs.getInt(1)]
						+ "' />";
				s1 = s1 + "<set value='" + rs.getString(6) + "' />";
				s2 = s2 + "<set value='" + rs.getString(7) + "' />";
			}

			cat = cat + "</categories> \\\n";
			s1 = s1 + "</dataset> \\\n";
			s2 = s2 + "</dataset> \\\n";
		} catch (Exception localException) {
		} finally {
			DBUtil.closeResultSet(rs);
		}

		xml = xml_static1 + cat + s1 + s2 + xml_static2;
		return xml;
	}

	public ActionForward revenue_chart1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String contextpath = request.getContextPath();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		String ms_line_chart = "<div id='revenue_chart1' align='center' style='padding:15px;'>Waiting...</div>\n<script type='text/javascript'>\nvar revenue_chart1 = ~data_url~\nvar myChart = new FusionCharts('"
				+ contextpath
				+ "/fc/MSLine.swf?noCache=~tm~', 'revenue_chart1', '500', '300', '0', '0');\nmyChart.setDataXML(revenue_chart1);\nmyChart.setTransparent(true);\nmyChart.render('revenue_chart1');\n</script>";

		String spm = request.getParameter("spm");
		if ((spm == null) || (spm.equals(""))) {
			spm = "%";
		}

		ms_line_chart = ms_line_chart.replace("~data_url~", getChartXML2(spm));

		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0L);
		response.setContentType("text/html");
		response.getWriter().write(ms_line_chart);
		response.flushBuffer();

		return null;
	}

	String getChartXML2(String spm) {
		String sql = "";

		if (spm.equals("%")) {
			sql = "use datawarehouse; select datepart(mm, sc_dd_month_mm01yyyy), sc_dd_month_mm01yyyy, sum(isnull(ac_ru_pro_forma,0.0)), sum(isnull(ac_ru_expense,0.0)), sum(isnull(ac_ru_revenue,0.0)), 1-(sum(isnull(ac_ru_pro_forma,0.0))/sum(isnull(ac_ru_revenue,0.0))), 1-(sum(isnull(ac_ru_expense,0.0))/sum(isnull(ac_ru_revenue,0.0)))   from dbo.ac_dimensions_job join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id join dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id and ac_du_spm = 1 join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy  where ac_cl_minor2 = 'Billable' and ac_jd_date_closed > dateadd(mm, -11, getdate()) and ac_du_name != 'Shroder, Charles'  group by datepart(mm, sc_dd_month_mm01yyyy), sc_dd_month_mm01yyyy having sum(isnull(ac_ru_revenue,0.0)) > 0.0";
		} else {
			sql = "use datawarehouse; select datepart(mm, sc_dd_month_mm01yyyy), sc_dd_month_mm01yyyy, sum(isnull(ac_ru_pro_forma,0.0)), sum(isnull(ac_ru_expense,0.0)), sum(isnull(ac_ru_revenue,0.0)), 1-(sum(isnull(ac_ru_pro_forma,0.0))/sum(isnull(ac_ru_revenue,0.0))), 1-(sum(isnull(ac_ru_expense,0.0))/sum(isnull(ac_ru_revenue,0.0)))   from dbo.ac_dimensions_job join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id join dbo.ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id and ac_du_spm = 1 join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy  where ac_cl_minor2 = 'Billable' and ac_jd_date_closed > dateadd(mm, -11, getdate()) and ac_du_name = '"
					+ spm
					+ "'"
					+ " group by datepart(mm, sc_dd_month_mm01yyyy), sc_dd_month_mm01yyyy having sum(isnull(ac_ru_revenue,0.0)) > 0.0";
		}

		String xml = "";
		String xml_static1 = " \"<chart caption='Revenue/Expense' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='' borderColor='#ffffff'> \\\n";

		String xml_static2 = "<styles><definition><style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' /></definition><application><apply toObject='Canvas' styles='CanvasAnim' /></application></styles></chart>\";";

		String s1 = "<dataset seriesName='Expense'>";
		String s2 = "<dataset seriesName='Revenue'>";
		String cat = "<categories>";

		ResultSet rs = this.da.getResultSet(sql);
		try {
			while (rs.next()) {
				cat = cat + "<category label='" + this.Months[rs.getInt(1)]
						+ "' />";
				s1 = s1 + "<set value='" + rs.getString(4) + "' />";
				s2 = s2 + "<set value='" + rs.getString(5) + "' />";
			}

			cat = cat + "</categories> \\\n";
			s1 = s1 + "</dataset> \\\n";
			s2 = s2 + "</dataset> \\\n";
		} catch (Exception localException) {
		}

		xml = xml_static1 + cat + s1 + s2 + xml_static2;
		return xml;
	}
}
