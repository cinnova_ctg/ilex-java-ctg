package com.ilex.reports.action.operationSummary;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.rowset.CachedRowSet;

import com.ilex.reports.dao.AbstractDAO;
import com.mind.fw.core.dao.util.DBUtil;
import com.sun.rowset.CachedRowSetImpl;

public class DataAccess extends AbstractDAO {

	private Statement sql;

	/*
	 * Title: getResultSet
	 * 
	 * Description: Generic sql execution
	 * 
	 * Inputs: sql
	 * 
	 * Returns: result set
	 */

	public ResultSet getResultSet(String sql_to_execute) {

		CachedRowSet cachedRowSet = null;
		Connection conn = null;

		try {
			initIlexMainDS();
			cachedRowSet = new CachedRowSetImpl();
			conn = ilexMainDS.getConnection();
			sql = conn.createStatement();
			ResultSet rs = sql.executeQuery(sql_to_execute);
			cachedRowSet.populate(rs);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtil.close(conn);
		}

		return cachedRowSet;
	}

	/*************************************************************************/

	/*
	 * Title: Execute non-Result Set Query
	 * 
	 * Description: Generic sql execution
	 * 
	 * Inputs: sql
	 * 
	 * Returns: result set
	 */

	public boolean executeQuery(String sql_to_execute) {

		boolean executeStatus = false;
		Connection conn = null;

		try {
			initIlexMainDS();
			conn = ilexMainDS.getConnection();
			sql = conn.createStatement();
			sql.execute(sql_to_execute);
			executeStatus = true;
		} catch (Exception e) {
			System.out.println(e + " " + sql_to_execute);
			return executeStatus;
		} finally {
			DBUtil.close(conn);
		}

		return executeStatus;
	}

	/*************************************************************************/

	/*
	 * Title: Get Vales for Menu with ~ delimiter
	 * 
	 * Description: Generic sql execution
	 * 
	 * Inputs: sql
	 * 
	 * Returns: result set
	 */

	public String getValues1(String sql_to_execute) {

		String values = "";
		String delimiter = "";
		Connection conn = null;

		try {
			initIlexMainDS();
			conn = ilexMainDS.getConnection();
			sql = conn.createStatement();
			ResultSet rs = sql.executeQuery(sql_to_execute);
			while (rs.next()) {
				values += delimiter + rs.getString(1);
				delimiter = "~";
			}
		} catch (Exception e) {
			System.out.println(e);
			values = null;
		} finally {
			DBUtil.close(conn);
		}

		return values;
	}

}
