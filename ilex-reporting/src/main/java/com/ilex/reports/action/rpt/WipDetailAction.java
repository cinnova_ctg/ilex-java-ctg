package com.ilex.reports.action.rpt;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ilex.reports.action.operationSummary.DataAccess;
import com.ilex.reports.bean.rpt.SiteManagementBean;
import com.ilex.reports.bean.rpt.SiteManagementForm;

public class WipDetailAction extends DispatchAction {

	private DataAccess da = new DataAccess();

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward_key = "success";	
		String QueryWhere = "";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		SiteManagementForm siteManagementForm = (SiteManagementForm) form;
		
		if (request.getParameter("countRow") != null
				&& !request.getParameter("countRow").equals(""))
			siteManagementForm.setCountRow(request.getParameter("countRow"));
		else
			siteManagementForm.setCountRow("" + 1);
		
		if (request.getParameter("lines_per_page") != null)
			siteManagementForm.setLines_per_page(request
					.getParameter("lines_per_page"));

		if (request.getParameter("current_page_no") != null)
			siteManagementForm.setCurrent_page_no(request
					.getParameter("current_page_no"));

		if (request.getParameter("total_page_size") != null)
			siteManagementForm.setTotal_page_size(request
					.getParameter("total_page_size"));

		if (request.getParameter("org_page_no") != null)
			siteManagementForm.setOrg_page_no(request
					.getParameter("org_page_no"));
		
		if (request.getParameter("org_lines_per_page") != null)
			siteManagementForm.setOrg_lines_per_page(request
					.getParameter("org_lines_per_page"));
		
		if(request.getParameter("status")!=null)
			siteManagementForm.setJobStatus(request.getParameter("status").toString().trim());
		
		if(request.getParameter("PocReference")!=null)
			siteManagementForm.setPocReference(request.getParameter("PocReference"));
		
		if(request.getParameter("PocReferenceType")!=null)
			siteManagementForm.setPocReferenceType(request.getParameter("PocReferenceType"));
		
		if(!(siteManagementForm.getPocReference().equals("0"))){//if not equal to 0 then add this where clause in the existing.
			QueryWhere = " AND lx_pr_cns_poc = '"+siteManagementForm.getPocReference()+"' ";
		}else if(siteManagementForm.getPocReferenceType().equals("Project")){
			QueryWhere = " AND lx_pr_type <> '3' ";
		}else if(siteManagementForm.getPocReferenceType().equals("Dispatch")){
			QueryWhere = " AND lx_pr_type = '3' ";
		}
		
		if(request.getParameter("daysCount") != null && !(request.getParameter("daysCount").equals("0"))){
			String dayCount = "";
			dayCount = request.getParameter("daysCount").toString();
			QueryWhere = " AND DATEDIFF(DAY, lm_si_create_date, getdate() ) <= "+dayCount +" " ;
		}
		
		
		
		String query = " SELECT DISTINCT lm_si_id, lm_js_pr_id, lm_js_id, lm_si_name, lm_si_number, lm_si_locality_factor, "
				+ " lm_si_union_site, lm_si_designator, lm_si_work_location, lm_si_address, lm_si_city, lm_si_state,  lm_si_zip_code, "
				+ " lm_si_sec_phone_no, lm_si_phone_no, lm_si_country, lm_si_directions, lm_si_pri_poc, lm_si_sec_poc, lm_si_pri_email_id, "
				+ " lm_si_sec_email_id, lm_si_notes, lm_si_create_date, lm_si_created_by, lm_si_changed_date, lm_si_changed_by, "
				+ " lm_si_installer_poc, lm_si_site_poc, lm_si_lat_deg, lm_si_lat_min, lm_si_lat_direction, lm_si_lon_deg, lm_si_lon_min, "
				+ " lm_si_lon_direction, lm_si_group, lm_si_end_customer, lm_si_time_zone, cc_tz_name, lm_si_status, cc_status_description, " 
				+ " (user1.lo_pc_first_name + ' ' + user1.lo_pc_last_name) AS created_by, " 
				+ " (user2.lo_pc_first_name + ' ' + user2.lo_pc_last_name) AS changeded_by, "
				+ " lx_pr_title "
				+ " FROM  lm_site_detail "
				+ " LEFT OUTER JOIN lm_job ON lm_js_si_id = lm_si_id "
				+ " LEFT OUTER JOIN cc_time_zone ON lm_si_time_zone = cc_tz_id "
				+ " LEFT OUTER JOIN cc_status_master ON lm_js_status = cc_status_code "
				+ " LEFT OUTER JOIN lo_poc user1 ON user1.lo_pc_id = lm_si_created_by "
				+ " LEFT OUTER JOIN lo_poc user2 ON user2.lo_pc_id =  lm_si_changed_by "
				+ " LEFT OUTER JOIN lx_appendix_main ON lx_pr_id = lm_js_pr_id " // appendixID
				+ " WHERE lm_js_prj_display_status LIKE ('"+siteManagementForm.getJobStatus()+"') "
				+ QueryWhere;
		
		request.setAttribute("status", request.getParameter("status"));
		
		ArrayList<SiteManagementBean> siteList = this.setSiteManagementBean(this.da.getResultSet(query));
				
		List siteDetails = new ArrayList();

		if (siteList != null
				|| (siteList!= null && (siteManagementForm
						.getFirst() != null
						|| siteManagementForm.getLast() != null
						|| siteManagementForm.getPrevious() != null || siteManagementForm
						.getNext() != null))) {
			if (request.getParameter("search") != null
					&& request.getParameter("search").equals("go")) {
				siteManagementForm.setNext(null);
				siteManagementForm.setLast(null);
				siteDetails = this.getSitedisplayDetail(
						siteList,
						siteManagementForm.getLines_per_page(), "0",
						siteManagementForm);
				BeanUtils.copyProperties(siteManagementForm, siteManagementForm);
			} else {
				siteDetails = this.getSitedisplayDetail(
								siteList,
								siteManagementForm.getOrg_lines_per_page(),
								siteManagementForm.getCurrent_page_no(),
								siteManagementForm);
				BeanUtils.copyProperties(siteManagementForm, siteManagementForm);
				request.setAttribute("count", 7 + "");
			}

		} 
		
		
		BeanUtils.copyProperties(siteManagementForm, siteManagementForm);
		
		request.setAttribute("siteDetailListSize", siteList.size());
		request.setAttribute("siteList", siteDetails);
		request.setAttribute("PocReferenceType", siteManagementForm.getPocReferenceType());
		request.setAttribute("PocReference", siteManagementForm.getPocReference());

		return mapping.findForward(forward_key);
	}
	
	private ArrayList<SiteManagementBean> setSiteManagementBean(ResultSet rs){
		ArrayList<SiteManagementBean> displayList = new ArrayList<SiteManagementBean>();

		try{
			while(rs.next()){
				SiteManagementBean siteManagementForm = new SiteManagementBean();

				 siteManagementForm.setAppendixname(rs.getString("lx_pr_title"));
				 siteManagementForm.setAppendixid(rs.getString("lm_js_pr_id"));
				 siteManagementForm.setSiteID(rs.getString("lm_si_id"));
				 siteManagementForm.setSiteName(rs.getString("lm_si_name"));
				 siteManagementForm.setSiteNumber(rs.getString("lm_si_number"));
				 siteManagementForm.setSiteAddress(rs.getString("lm_si_address"));				 
				 siteManagementForm.setSiteCity(rs.getString("lm_si_city"));
				 siteManagementForm.setState(rs.getString("lm_si_state"));
				 siteManagementForm.setSiteZipCode(rs.getString("lm_si_zip_code"));
				 siteManagementForm.setCountry(rs.getString("lm_si_country"));
				 siteManagementForm.setSiteLocalityFactor(rs.getString("lm_si_locality_factor"));	
				 siteManagementForm.setSiteUnion(rs.getString("lm_si_union_site"));
				 siteManagementForm.setSiteDesignator(rs.getString("lm_si_designator"));
				 siteManagementForm.setSiteDirections(rs.getString("lm_si_directions"));
				 siteManagementForm.setSiteWorkLocation(rs.getString("lm_si_work_location"));
				 siteManagementForm.setSiteCreatedDate(rs.getString("lm_si_create_date"));
//				 siteManagementForm.setSiteCreatedBy(rs.getString("lm_si_created_by"));
				 siteManagementForm.setOwnerId(rs.getString("lm_si_created_by"));
				 siteManagementForm.setSiteCreatedBy(rs.getString("created_by"));				 
				 siteManagementForm.setSiteChangedDate(rs.getString("lm_si_changed_date"));
//				 siteManagementForm.setSiteChangedBy(rs.getString("lm_si_changed_by"));
				 siteManagementForm.setSiteChangedBy(rs.getString("changeded_by"));
				 siteManagementForm.setSiteInstallerPoc(rs.getString("lm_si_installer_poc"));
				 siteManagementForm.setSitePoc(rs.getString("lm_si_site_poc"));
				 siteManagementForm.setSiteNotes(rs.getString("lm_si_notes"));
				 siteManagementForm.setSitePrimaryName(rs.getString("lm_si_pri_poc"));
				 siteManagementForm.setSitePrimaryPhone(rs.getString("lm_si_phone_no"));
				 siteManagementForm.setSitePrimaryEmail(rs.getString("lm_si_pri_email_id"));
				 siteManagementForm.setSiteSecondaryName(rs.getString("lm_si_sec_poc"));
				 siteManagementForm.setSiteSecondayPhone(rs.getString("lm_si_sec_phone_no"));
				 siteManagementForm.setSiteSecondaryEmail(rs.getString("lm_si_sec_email_id"));
				 siteManagementForm.setSite_group(rs.getString("lm_si_group"));
				 siteManagementForm.setSite_end_customer(rs.getString("lm_si_end_customer"));
//				 siteManagementForm.setSiteStatus(rs.getString("lm_si_status"));
				 siteManagementForm.setSiteSearchStatus(rs.getString("lm_si_status"));
				 siteManagementForm.setSiteStatus(rs.getString("cc_status_description"));
				 siteManagementForm.setSite_time_zone(rs.getString("cc_tz_name"));
				 siteManagementForm.setSiteJobId(rs.getString("lm_js_id"));
				 siteManagementForm.setMsaid(rs.getString("lm_js_pr_id"));
				 
				 displayList.add(siteManagementForm);
				 
			}
		}catch(Exception e){
			e.printStackTrace();
		}	
		
		return displayList;
	}
	
	private  List getSitedisplayDetail(ArrayList displayList, String lines_page_size, String current_page_no, SiteManagementForm siteMngttForm)
	{
		List valuelist = new ArrayList();
		//ArrayList valuelist = new ArrayList();
		int rowSize=0;
		int total_pages = 0;
		int counter = 0;
		int index1 = 0;
		int rem = 0;
		int index2;
		
			rowSize = displayList.size();
				
				if( current_page_no == null || current_page_no.equals("") || current_page_no.equals("0"))
				{
					current_page_no = "1";
				}
				if(lines_page_size == null || lines_page_size.equals("")|| lines_page_size.equals("0"))
				{
					lines_page_size = "10"; 
				}
		

				total_pages = rowSize / Integer.parseInt(lines_page_size);
				rem = rowSize % Integer.parseInt(lines_page_size);
				
				if (rowSize % Integer.parseInt(lines_page_size) > 0)
					total_pages = total_pages + 1;

				
				
				if (siteMngttForm.getFirst() != null && !siteMngttForm.getFirst().equals(""))
				{
					index1= 0;
					current_page_no = "1";
				}
				
				if (siteMngttForm.getNext() != null && !siteMngttForm.getNext().equals(""))
				{
					current_page_no = (Integer.parseInt(current_page_no) + 1) + "";
				}
			
				if (siteMngttForm.getPrevious() != null && !siteMngttForm.getPrevious().equals(""))
				{
					current_page_no = (Integer.parseInt(current_page_no) - 1) + "";
					if ((Integer.parseInt(current_page_no) == 0 || (Integer.parseInt(current_page_no) == 1)))
					{
						current_page_no = "1";
					}
				}
		
				if (siteMngttForm.getLast()!=null && !siteMngttForm.getLast().equals(""))
				{
					current_page_no = total_pages + "";
				}
		
				if(Integer.parseInt(current_page_no) > total_pages)
					current_page_no = (Integer.parseInt(current_page_no)-1)+"";
		
		switch(Integer.parseInt(current_page_no)){
		case 0: ; 
		case 1: index1 = 0; break;
		default: index1 = ((Integer.parseInt(current_page_no) -1) * Integer.parseInt(lines_page_size) ); 
		}
		
		
		if(rem > 0 && (Integer.parseInt(current_page_no) == total_pages))
			index2 = index1 + rem;
		else
			index2 = index1 + Integer.parseInt(lines_page_size);
		
		if(displayList.size() > 0)
			valuelist = displayList.subList(index1,index2);
		
		siteMngttForm.setTotal_page_size(total_pages + "");
		siteMngttForm.setCurrent_page_no(current_page_no + "");
		siteMngttForm.setLines_per_page(lines_page_size + "");				
		siteMngttForm.setOrg_page_no(current_page_no + "");
		siteMngttForm.setOrg_lines_per_page(lines_page_size + "");
		siteMngttForm.setRow_size(rowSize + "");
		
		
		
		return valuelist;
	}	
	
}
