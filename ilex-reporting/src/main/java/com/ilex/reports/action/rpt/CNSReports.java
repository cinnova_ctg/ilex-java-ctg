package com.ilex.reports.action.rpt;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ilex.reports.action.operationSummary.DataAccess;

public class CNSReports extends DispatchAction {

    DataAccess da = new DataAccess();
    SimpleDateFormat MMM_YY = new SimpleDateFormat("MMM y");

    public ActionForward weekly_personnel(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String forward_key = "weekly_personnel";
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");

        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
            // expired
        }

        int count = 0;
        String dept = "";
        int totals = 0;
        Map<String, Vector<String>> organization = getOrganizationStructure();
        Vector<String> dept_details = new Vector<String>();

        for (Map.Entry<String, Vector<String>> dd : organization.entrySet()) {
            dept_details.add(dd.getKey());
        }

        System.out.println(dept_details);
        Map<String, String> datasets = new HashMap<String, String>();

        for (int i = 0; i < dept_details.size(); i++) {
            dept_details.get(i);
            datasets.put(dept_details.get(i), "<dataset seriesName='"
                    + dept_details.get(i) + "'>");
        }

        // new parameter added into jsp that tells us the shift between dates (weeks chunk) in a counter form
        //String counter = request.getParameter("counter");
        String counter = request.getParameter("counter");

        if (counter == null) {
            counter = "1";
        }
        request.setAttribute("counter", counter);

        String[] category_dates = getCategoryDatesWithLeaps(counter);
        request.setAttribute("catDates", category_dates);
        String categories = "<categories>";
        for (int i = 0; i < category_dates.length; i++) {
            categories = categories + " <category Label='" + category_dates[i]
                    + "'/>";
        }
        categories = categories + "</categories>";
        String company_dataseries = "<dataset seriesName='Contingent'>";

        Map<String, Map<String, Map<String, String>>> reportdata = getMonthDepartmentRankCountWithLeaps(counter);

        for (int i = 0; i < category_dates.length; i++) {
            if (reportdata.containsKey(category_dates[i])) {
                //03/16/2014, 03/23/2014, 03/30/2014, 04/06/2014, 04/13/2014, 04/20/2014
//				if(category_dates[i].equals("03/02/2014") || category_dates[i].equals("02/23/2014"))
//				{
//					System.out.println("ALLAH HO AKBAR");
//				}
                Map<String, Map<String, String>> by_dept = reportdata
                        .get(category_dates[i]);

                totals = 0;

                for (int j = 0; j < dept_details.size(); j++) {
                    count = 0;
                    dept = dept_details.get(j);
//					if(dept.equals("MIS") || dept.equals("MSP BILLING"))
//					{
//						System.out.println("----------------");
//					}
                    if (by_dept.containsKey(dept)) {
                        Map<String, String> dept_ranks_cont = by_dept.get(dept);

                        for (Map.Entry<String, String> d : dept_ranks_cont
                                .entrySet()) {
                            count += Integer.parseInt((String) d.getValue());
                        }

                    }
                    //clear zone end
                    String temp1 = (String) datasets.get(dept);
                    temp1 = temp1
                            + " <set value='"
                            + count
                            + "' link='CNSRPT.do?ac=weekly_personnel_details&in_department="
                            + dept + "' />";

                    datasets.put(dept, temp1);
                    totals += count;
                }
                String date = category_dates[i];
                int lcount = getTotal(date, counter);
                company_dataseries = company_dataseries + "<set index='" + i + "' value='"
                        + lcount + "' />";
            }
        }

        for (int i = 0; i < dept_details.size(); i++) {
            dept = dept_details.get(i);
            datasets.put(dept, (String) datasets.get(dept) + "</dataset>");
        }

        for (int i = 0; i < dept_details.size(); i++) {
            dept = dept_details.get(i);
            categories = categories + (String) datasets.get(dept);
        }

        categories = categories + company_dataseries + "</dataset>";
        System.out.println(categories);
        request.setAttribute("chart_data", categories);
        return mapping.findForward(forward_key);
    }

    public ActionForward weekly_personnel_xls(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String forward_key = "weekly_personnel_xls";
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");

        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
            // expired
        }

        String date = request.getParameter("paramDate");
        String sql = "exec dbo.ac_direct_personnel_xls '" + date + "'";
        String first_name = "";
        String last_name = "";
        String title = "";
        String rank = "";
        String role = "";
        String table_rows = "";
        ResultSet rs = this.da.getResultSet(sql);
        try {
            while (rs.next()) {
                first_name = rs.getString(1);
                last_name = rs.getString(2);
                title = rs.getString(3);
                rank = rs.getString(4);
                role = rs.getString(5);
                table_rows = table_rows + "<tr><td>" + first_name + "</td><td>"
                        + last_name + "</td><td>" + title + "</td><td>" + rank
                        + "</td><td>" + role + "</td></tr>";
            }
            rs.close();
        } catch (Exception localException) {
        }
        request.setAttribute("xls_data", table_rows);
        return mapping.findForward(forward_key);
    }

    public ActionForward weekly_personnel_details(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String forward_key = "weekly_personnel_details";
        String count = "";
        String rank = "";
        HttpSession session = request.getSession(true);
        String loginuserid = (String) session.getAttribute("userid");

        if (loginuserid == null) {
            return (mapping.findForward("SessionExpire")); // Check for session
            // expired
        }

        String in_department = request.getParameter("in_department");
        String chart = "<chart caption='"
                + in_department
                + " Head Count by Rank' xAxisName='Week' yAxisName='Amount' labelDisplay='Rotate'>";
        Map organization = getOrganizationStructure();
        Map datasets = new HashMap();

        Vector dept_ranks = (Vector) organization.get(in_department);
        for (int i = 0; i < dept_ranks.size(); i++) {
            dept_ranks.get(i);
            datasets.put((String) dept_ranks.get(i), "<dataset seriesName='"
                    + (String) dept_ranks.get(i) + "'>");
        }

        String[] category_dates = getCategoryDates();
        String categories = "<categories>";
        for (int i = 0; i < category_dates.length; i++) {
            categories = categories + " <category Label='" + category_dates[i]
                    + "'/>";
        }
        categories = categories + "</categories>";
        Map reportdata = getMonthDepartmentRankCount();
        for (int i = 0; i < category_dates.length; i++) {
            if (reportdata.containsKey(category_dates[i])) {
                Map by_dept = (Map) reportdata.get(category_dates[i]);
                Map by_rank = (Map) by_dept.get(in_department);
                for (int j = 0; j < dept_ranks.size(); j++) {
                    if (by_rank.containsKey(dept_ranks.get(j))) {
                        rank = (String) dept_ranks.get(j);
                        count = (String) by_rank.get(dept_ranks.get(j));
                    } else {
                        rank = (String) dept_ranks.get(j);
                        count = "0";
                    }
                    String temp1 = (String) datasets.get(rank);
                    temp1 = temp1 + " <set value='" + count + "' />";
                    datasets.put(rank, temp1);
                }

            }

        }

        for (int i = 0; i < dept_ranks.size(); i++) {
            rank = (String) dept_ranks.get(i);
            datasets.put(rank, (String) datasets.get(rank) + "</dataset>");
        }
        for (int i = 0; i < dept_ranks.size(); i++) {
            rank = (String) dept_ranks.get(i);
            categories = categories + (String) datasets.get(rank);
        }
        request.setAttribute("chart_data", chart + categories);
        return mapping.findForward(forward_key);
    }

    public ActionForward weekly_personnel_title(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String forward_key = "weekly_personnel_title";
        int count = 0;
        String dept = "";
        int totals = 0;
        String in_department = request.getParameter("in_department");
        Map<String, Vector<String>> organization = getOrganizationStructureTitleDept(in_department);
        Vector<String> dept_details = new Vector<String>();

        for (Map.Entry<String, Vector<String>> dd : organization.entrySet()) {
            dept_details.add(dd.getKey());
        }

        System.out.println(dept_details);
        Map<String, String> datasets = new HashMap<String, String>();

        for (int i = 0; i < dept_details.size(); i++) {
            dept_details.get(i);
            datasets.put(dept_details.get(i), "<dataset seriesName='"
                    + dept_details.get(i) + "'>");
        }

        // new parameter added into jsp that tells us the shift between dates (weeks chunk) in a counter form
        //String counter = request.getParameter("counter");
        String counter = request.getParameter("counter");

        if (counter == null) {
            counter = "1";
        }
        request.setAttribute("counter", counter);

        String[] category_dates = getCategoryDatesWithLeaps(counter);
        request.setAttribute("catDates", category_dates);
        String categories = "<categories>";
        for (int i = 0; i < category_dates.length; i++) {
            categories = categories + " <category Label='" + category_dates[i]
                    + "'/>";
        }
        categories = categories + "</categories>";
        //String company_dataseries = "<dataset seriesName='Contingent'>";

        Map<String, Map<String, String>> reportdata = getMonthDepartmentTitleCount();

        //Retrieve the dataset string for the given department.
        //Loop through each date
        for (int i = 0; i < category_dates.length; i++) {
            String theDate = category_dates[i];

            //Loop through each title.
            for (int k = 0; k < dept_details.size(); k++) {
                count = 0;
                dept = dept_details.get(k);

                if (reportdata.containsKey(theDate)) {
                    Map<String, String> titleMap = reportdata.get(theDate);

                    if (titleMap.containsKey(dept)) {
                        String theCount = titleMap.get(dept);

                        if (theCount != null) {
                            count += Integer.parseInt(theCount);
                        }
                    }
                }

                //Only add the value if it isn't 0. This will hide past data.
                if (count != 0) {
                    String temp1 = (String) datasets.get(dept);
                    temp1 = temp1
                            + " <set value='"
                            + count
                            + "'/>";

                    datasets.put(dept, temp1);

                } else {
                    String temp1 = (String) datasets.get(dept);
                    temp1 = temp1
                            + " <set value='"
                            + "'/>";

                    datasets.put(dept, temp1);

                }

            }
        }

//For each date
//        for (int i = 0; i < category_dates.length; i++) {
//            if (reportdata.containsKey(category_dates[i])) {
//                Map<String, Map<String, String>> by_dept = reportdata
//                        .get(category_dates[i]);
//
//                totals = 0;
//
//                //For each department / title
//                for (int j = 0; j < dept_details.size(); j++) {
//                    count = 0;
//                    //This is the current deparatment / title
//                    dept = dept_details.get(j);
//
//
//
//                    if (by_dept.containsKey(dept)) {
//                        Map<String, String> dept_ranks_cont = by_dept.get(dept);
//
//                        for (Map.Entry<String, String> d : dept_ranks_cont
//                                .entrySet()) {
//                            count += Integer.parseInt((String) d.getValue());
//                        }
//
//                    }
//
//                    //Calculate the count for the specific department and date combination.
//
//
//                    //clear zone end
//                    String temp1 = (String) datasets.get(dept);
//                    temp1 = temp1
//                            + " <set value='"
//                            + count
//                            + "'/>";
//
//                    datasets.put(dept, temp1);
//                    totals += count;
//                //}
//                String date = category_dates[i];
//                int lcount = getTotal(date, counter);
//                company_dataseries = company_dataseries + "<set index='" + i + "' value='"
//                        + lcount + "' />";
//            }
//        }
        for (int i = 0; i < dept_details.size(); i++) {
            dept = dept_details.get(i);
            datasets.put(dept, (String) datasets.get(dept) + "</dataset>");
        }

        for (int i = 0; i < dept_details.size(); i++) {
            dept = dept_details.get(i);
            categories = categories + (String) datasets.get(dept);
        }

        //categories = categories + company_dataseries + "</dataset>";
        System.out.println(categories);
        request.setAttribute("chart_data", categories);
        return mapping.findForward(forward_key);
    }

    String[] getCategoryDatesWithLeaps(String leaps) {
        String[] cat = new String[52];
        int i = 0;
        String sql = "exec dbo.ac_direct_personnel_52trailing_weeks_with_leaps " + leaps;
        ResultSet rs = this.da.getResultSet(sql);
        try {
            while (rs.next()) {
                cat[(i++)] = rs.getString(1);
            }
            rs.close();
        } catch (Exception localException) {
        }
        return cat;
    }

    String[] getCategoryDates() {
        String[] cat = new String[52];
        int i = 0;
        String sql = "exec dbo.ac_direct_personnel_52trailing_weeks";
        ResultSet rs = this.da.getResultSet(sql);
        try {
            while (rs.next()) {
                cat[(i++)] = rs.getString(1);
            }
            rs.close();
        } catch (Exception localException) {
        }
        return cat;
    }

    Map<String, Map<String, Map<String, String>>> getMonthDepartmentRankCountWithLeaps(String counter) {
        Map<String, Map<String, Map<String, String>>> by_month = new HashMap<String, Map<String, Map<String, String>>>();
        Map<String, Map<String, String>> by_department = new HashMap<String, Map<String, String>>();
        Map<String, String> by_rank = new HashMap<String, String>();
        String previous = "";
        String current = "";
        String previous_dept = "";
        String current_dept = "";
        String sql = "exec dbo.ac_direct_personnel_details_with_leaps4 " + counter;
        try {
            ResultSet rs = this.da.getResultSet(sql);

            if (rs.next()) {
                current = rs.getString("window_start");
                previous = rs.getString("window_start");
                previous_dept = rs.getString("department");
                current_dept = rs.getString("department");
                by_rank.put(rs.getString("rank"), rs.getString("count"));
            }
            while (rs.next()) {
                if (rs.getString("decision").equals("1")) {
                    current = rs.getString("window_start");
                    current_dept = rs.getString("department");

                    if (!current_dept.equals(previous_dept)) {
                        by_department.put(previous_dept, by_rank);
                        by_rank = new HashMap<String, String>();
                        previous_dept = current_dept;
                    }

                    if (!current.equals(previous)) {
                        //by_department.put(previous_dept, by_rank);
                        by_month.put(previous, by_department);
                        by_department = new HashMap<String, Map<String, String>>();
                        by_rank = new HashMap<String, String>();
                        previous = current;
                        previous_dept = current_dept;
                    }

                    by_rank.put(rs.getString("rank"), rs.getString("count"));
                }

            }

            // for the entry of last month
            by_month.put(previous, by_department);
            by_department = new HashMap<String, Map<String, String>>();
            by_rank = new HashMap<String, String>();
            previous = current;
            previous_dept = current_dept;

            rs.close();
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return by_month;
    }

    int getTotal(String date, String counter) {
        int rtn = 0;
        String sql = "exec dbo.ac_direct_personnel_details_with_leaps4 " + counter;
        try {
            ResultSet rs = this.da.getResultSet(sql);
            while (rs.next()) {
                if (rs.getString("window_start").equals(date) && (!rs.getString("department").equals("--Select--")) && rs.getString("decision").equals("1")) {
                    rtn += rs.getInt("count");
                }
            }
            rs.close();
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return rtn;
    }

    Map<String, Map<String, Map<String, String>>> getMonthDepartmentRankCount() {
        Map<String, Map<String, Map<String, String>>> by_month = new HashMap<String, Map<String, Map<String, String>>>();
        Map<String, Map<String, String>> by_department = new HashMap<String, Map<String, String>>();
        Map<String, String> by_rank = new HashMap<String, String>();
        String previous = "";
        String current = "";
        String previous_dept = "";
        String current_dept = "";
        String sql = "exec dbo.ac_direct_personnel_details";
        try {
            ResultSet rs = this.da.getResultSet(sql);

            if (rs.next()) {
                current = rs.getString("window_start");
                previous = rs.getString("window_start");
                previous_dept = rs.getString("department");
                current_dept = rs.getString("department");
                by_rank.put(rs.getString("rank"), rs.getString("count"));
            }
            while (rs.next()) {
                current = rs.getString("window_start");
                current_dept = rs.getString("department");
                if (!current_dept.equals(previous_dept)) {
                    by_department.put(previous_dept, by_rank);
                    by_rank = new HashMap<String, String>();
                    previous_dept = current_dept;
                }

                if (!current.equals(previous)) {
//					by_department.put(previous_dept, by_rank);
                    by_month.put(previous, by_department);
                    by_department = new HashMap<String, Map<String, String>>();
                    by_rank = new HashMap<String, String>();
                    previous = current;
                    previous_dept = current_dept;
                }
                by_rank.put(rs.getString("rank"), rs.getString("count"));
            }

            rs.close();
        } catch (Exception localException) {
        }
        return by_month;
    }

    Map<String, Map<String, String>> getMonthDepartmentTitleCount() {
        Map<String, Map<String, String>> returnMe = new HashMap<String, Map<String, String>>();

        String sql = "DECLARE @Date1 DATE, @Date2 DATE "
                + "SET @Date2 = DATEADD(wk, DATEDIFF(wk, 6, CURRENT_TIMESTAMP), 6) "
                + "SET @Date1 = DATEADD(wk, -51, @Date2) "
                + " "
                + "SELECT CONVERT(varchar(10), start_of_week, 101) as start_of_week "
                + "	, CONVERT(varchar(10), DATEADD(wk, DATEDIFF(wk, 6, start_of_week), 6 + 6), 101) as end_of_week "
                + "	, title "
                + "	, ( "
                + "			SELECT COUNT(1) "
                + "			FROM rpt_personal_headcount "
                + "			WHERE rpt_hc_title = b.title "
                + "                             AND start_of_week > '09/26/2015' "
                + "				AND rpt_hc_department_change_date <= DATEADD(wk, DATEDIFF(wk, 6, start_of_week), 6 + 6) "
                + "				AND ((rpt_hc_termination_date <= DATEADD(wk, DATEDIFF(wk, 6, start_of_week), 6 + 6) AND rpt_hc_termination_date >= a.start_of_week)  "
                + "                             OR rpt_hc_termination_date IS NULL) "
                + "		) as cnt "
                + "FROM ( "
                + "		SELECT DISTINCT DATEADD(wk, DATEDIFF(wk, 6, graph_date), 6) as start_of_week "
                + "		FROM ( "
                + "				SELECT DATEADD(DAY, number, @Date1) as graph_date "
                + "				FROM master..spt_values "
                + "				WHERE type = 'P' "
                + "					AND DATEADD(DAY, number, @Date1) <= @Date2 "
                + "			) as a "
                + "	) as a "
                + "CROSS JOIN ( "
                + "		SELECT lo_emp_jt_title as title FROM lo_employee_job_titles "
                + "	) as b";
        try {
            ResultSet rs = this.da.getResultSet(sql);

            String currentDate = "";
            String currentTitle = "";
            String oldDate = "";
            String count = "";
            Map<String, String> thePair = new HashMap<String, String>();
            while (rs.next()) {

                oldDate = currentDate;
                currentDate = rs.getString("start_of_week");
                currentTitle = rs.getString("title");
                count = rs.getString("cnt");

                //Is this a new date?
                if (!currentDate.equals(oldDate)) {
                    returnMe.put(oldDate, thePair);
                    thePair = new HashMap<String, String>();
                }

                thePair.put(currentTitle, count);
            }
            returnMe.put(oldDate, thePair);
        } catch (Exception localException) {
        }
        //The hashmap looks like this: <Date <Title, count>, Date <Title, count>, ... >
        return returnMe;
    }

    Map<String, Vector<String>> getOrganizationStructure() {
        Map<String, Vector<String>> organization = new HashMap<String, Vector<String>>();
        Vector<String> ranks = new Vector<String>();
        String previous = "";
        String current = "";
        String sql = "SELECT lo_cl_role, lo_ra_rank_value FROM dbo.lo_classification JOIN dbo.lo_rank ON lo_cl_id = lo_ra_cl_id ORDER BY lo_cl_role, lo_ra_rank_value";
        try {
            ResultSet rs = this.da.getResultSet(sql);
            if (rs.next()) {
                current = rs.getString("lo_cl_role");
                previous = rs.getString("lo_cl_role");
                ranks.add(rs.getString("lo_ra_rank_value"));
            }
            while (rs.next()) {
                current = rs.getString("lo_cl_role");
                if (!current.equals(previous)) {
                    organization.put(previous, ranks);
                    ranks = new Vector<String>();
                    previous = current;
                }
                ranks.add(rs.getString("lo_ra_rank_value"));
            }

            organization.put(current, ranks);
            rs.close();
        } catch (Exception localException) {
        }
        return organization;
    }

    Map<String, Vector<String>> getOrganizationStructureTitleDept(String in_department) {
        Map<String, Vector<String>> organization = new HashMap<String, Vector<String>>();
        Vector<String> ranks = new Vector<String>();
        String previous = "";
        String current = "";
        String sql = "select distinct rpt_hc_lo_pc_id, rpt_hc_f_name, rpt_hc_l_name, rpt_hc_department, rpt_hc_title from rpt_personal_headcount where rpt_hc_department = '" + in_department + "' AND rpt_hc_title IS NOT NULL";
        try {
            ResultSet rs = this.da.getResultSet(sql);
            while (rs.next()) {
                current = rs.getString("rpt_hc_title");
                organization.put(current, ranks);
            }
            rs.close();
        } catch (Exception localException) {
        }
        return organization;
    }

}
