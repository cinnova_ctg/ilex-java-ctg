package com.ilex.reports.action.hd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.JSONObject;

import com.ilex.report.formbean.helpdesk.FullResolutionBean;
import com.ilex.report.formbean.helpdesk.FullResolutionForm;
import com.ilex.reports.action.operationSummary.DataAccess;
import com.ilex.reports.bean.helpDesk.HDBean;
import com.ilex.reports.dao.managedServices.TicketCountDAO;

public class HDTicketDataAction extends DispatchAction {

	HDTicketDataDao daoobj = new HDTicketDataDao();
	DataAccess da = new DataAccess();
	SimpleDateFormat MMM_YY = new SimpleDateFormat("MMM y");

	public ActionForward getTicketData(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String forward_key = "ticketData";

		String days = "30";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		if (request.getParameter("days") != null
				&& !request.getParameter("days").equals("")) {
			days = request.getParameter("days").toString();
		} else if (request.getAttribute("days") != null
				&& !request.getAttribute("days").toString().equals("")) {
			days = request.getAttribute("days").toString();
		}

		request.setAttribute("days", days);

		String query = "SELECT lo_ot_name,  count(*) tickets,  "
				+ " lm_na_next_action, isnull(lm_hd_root_cause, 0) lm_hd_root_cause,  "
				+ " lm_ci_config_item, lm_ct_category,  "
				+ " isnull(lm_hd_urgency, 0) lm_hd_urgency   "
				+ " FROM lm_help_desk_ticket_details "
				+ " LEFT JOIN lm_help_desk_create_ticket_customer_list_view ON lp_mm_id = lm_hd_lp_mm_id "
				+ " LEFT JOIN lm_help_desk_ticket_next_actions ON lm_na_id = lm_hd_next_action "
				+ " LEFT JOIN lm_help_desk_ticket_config_items ON lm_ci_id = lm_hd_ci "
				+ " LEFT JOIN lm_help_desk_ticket_categories ON lm_ct_id = lm_hd_category "
				+ " where lm_hd_opened> dateadd(dd,-" + days + ",getdate())  "
				+ " and lo_ot_name is not null  "
				+ " GROUP BY lo_ot_name,  lm_na_next_action, "
				+ " lm_hd_root_cause, lm_ci_config_item,   "
				+ " lm_ct_category, lm_hd_urgency  " + " ORDER BY lo_ot_name";

		ResultSet rs = this.da.getResultSet(query);
		HDBean hdBean = null;

		List<HDBean> hdBeanList = new ArrayList<HDBean>();
		int totalTickets = 0;
		while (rs.next()) {
			hdBean = new HDBean();

			hdBean.setClientName(rs.getString("lo_ot_name"));
			hdBean.setTicketCount(rs.getString("tickets"));
			totalTickets = totalTickets
					+ Integer.parseInt(rs.getString("tickets"));
			hdBean.setNextAction(rs.getString("lm_na_next_action"));
			hdBean.setRootCause(rs.getString("lm_hd_root_cause"));
			hdBean.setConfigItem(rs.getString("lm_ci_config_item"));
			hdBean.setCategory(rs.getString("lm_ct_category"));
			hdBean.setUrgency(rs.getString("lm_hd_urgency"));

			if (hdBean.getUrgency().equals("1")) {
				hdBean.setUrgency("High");
			} else if (hdBean.getUrgency().equals("2")) {
				hdBean.setUrgency("Medium");
			} else if (hdBean.getUrgency().equals("3")) {
				hdBean.setUrgency("Low");
			} else {
				hdBean.setUrgency("N/A");
			}

			if (hdBean.getRootCause().equals("1")) {
				hdBean.setRootCause("General connection error");
			} else if (hdBean.getRootCause().equals("2")) {
				hdBean.setRootCause("Configuration error");
			} else if (hdBean.getRootCause().equals("3")) {
				hdBean.setRootCause("Customer caused outage");
			} else if (hdBean.getRootCause().equals("4")) {
				hdBean.setRootCause("Power outage");
			} else if (hdBean.getRootCause().equals("5")) {
				hdBean.setRootCause("Hardware failure");
			} else if (hdBean.getRootCause().equals("6")) {
				hdBean.setRootCause("Provider Outage");
			} else if (hdBean.getRootCause().equals("7")) {
				hdBean.setRootCause("DynDNS");
			} else {
				hdBean.setRootCause("N/A");
			}

			hdBeanList.add(hdBean);
		}
		// get listing from jsp page and show in excel

		String generateExport = request.getParameter("genExport");

		if (generateExport != null && generateExport.equals("Export")) {
			byte[] buffer = export(hdBeanList);
			response.setContentType("application/x-msdownload");
			response.setHeader("Content-Disposition",
					"attachment;filename=NocTicketExport.csv;size="
							+ buffer.length + "");
			ServletOutputStream outStream = response.getOutputStream();
			outStream.write(buffer);
			outStream.close();
		}
		String param = TicketCountDAO.getCountDetails(days);
		request.setAttribute("param", param);
		request.setAttribute("totalTickets", totalTickets);
		request.setAttribute("HDBeanList", hdBeanList);

		return mapping.findForward(forward_key);

	}

	public ActionForward getAllResolutiontData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "allResolutionData";

		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		String responsedata = getDataFromWeb(
				"https://ccc.contingent.com/index.php?cmp=phone_ws&type=managed_nodes",
				9999, "\n");

		try {
			if (responsedata != null) {

				JSONObject object = new JSONObject(responsedata);

				Object[] keys = object.keySet().toArray();
				if (keys.length > 0) {
					Arrays.sort(keys);
					request.setAttribute("SNMP_Monitors",
							object.get(keys[1].toString()));
					request.setAttribute("ICMP_Sub_Monitors",
							object.get(keys[2].toString()));
					request.setAttribute("Total_Managed_Nodes",
							object.get(keys[0].toString()));
				} else {
					request.setAttribute("SNMP_Monitors", "0");
					request.setAttribute("ICMP_Sub_Monitors", "0");
					request.setAttribute("Total_Managed_Nodes", "0");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		ArrayList<String[]> listData = new ArrayList<String[]>();
		DecimalFormat twoDForm = new DecimalFormat("#.##");

		String days = "30";

		if (request.getParameter("days") != null
				&& !request.getParameter("days").equals("")) {
			days = request.getParameter("days").toString();
		} else if (request.getAttribute("days") != null
				&& !request.getAttribute("days").toString().equals("")) {
			days = request.getAttribute("days").toString();
		}

		String TrendLevel_1_2_Query = "SELECT count(*) AS TotalGroups FROM lm_help_desk_audit_trail a, lm_help_desk_audit_trail b " +
				"WHERE a.lm_hd_at_assign_group_id = 1 AND b.lm_hd_at_assign_group_id = 2 AND a.lm_hd_at_ticket_id = b.lm_hd_at_ticket_id " +
				"AND  a.lm_hd_at_update_date >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND a.lm_hd_at_update_date <= GetDate()";
		String TrendLevel_2_3_Query = "SELECT count(*) AS TotalGroups FROM lm_help_desk_audit_trail a, lm_help_desk_audit_trail b " +
				"WHERE a.lm_hd_at_assign_group_id = 2 AND b.lm_hd_at_assign_group_id = 3 AND a.lm_hd_at_ticket_id = b.lm_hd_at_ticket_id " +
				"AND  a.lm_hd_at_update_date >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND a.lm_hd_at_update_date <= GetDate()";

		int TrendLevel_1_2 = 0;
		int TrendLevel_2_3 = 0;

		ResultSet rs1 = this.da.getResultSet(TrendLevel_1_2_Query);
		ResultSet rs2 = this.da.getResultSet(TrendLevel_2_3_Query);
		if (rs1.next() | rs2.next()) {
			String firstRTW = rs1.getString("TotalGroups");
			TrendLevel_1_2 = Integer.parseInt(firstRTW);

			String fullRTW = rs2.getString("TotalGroups");
			TrendLevel_2_3 = Integer.parseInt(fullRTW);

		}

		String DurationLevel_1_2_Query = "SELECT isnull(AVG(DATEDIFF(mi, a.lm_hd_at_update_date, b.lm_hd_at_update_date)/(60*1.0)),0.0) AS DateDifference "
				+ "FROM lm_help_desk_audit_trail a, lm_help_desk_audit_trail b "
				+ "WHERE a.lm_hd_at_assign_group_id = 1 AND b.lm_hd_at_assign_group_id = 2 "
				+ "AND a.lm_hd_at_ticket_id = b.lm_hd_at_ticket_id "
				+ "AND  a.lm_hd_at_update_date >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND a.lm_hd_at_update_date <= GetDate()";
		String DurationLevel_2_3_Query = "SELECT isnull(AVG(DATEDIFF(mi, b.lm_hd_at_update_date,a.lm_hd_at_update_date)/(60*1.0)),0.0) AS DateDifference "
				+ "FROM lm_help_desk_audit_trail a, lm_help_desk_audit_trail b WHERE a.lm_hd_at_assign_group_id = 2 AND b.lm_hd_at_assign_group_id = 3 "
				+ "AND a.lm_hd_at_ticket_id = b.lm_hd_at_ticket_id "
				+ "AND  a.lm_hd_at_update_date >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND a.lm_hd_at_update_date <= GetDate()";

		double DurationLevel_1_2 = 0.0;
		double DurationLevel_2_3 = 0.0;

		ResultSet rs3 = this.da.getResultSet(DurationLevel_1_2_Query);
		ResultSet rs4 = this.da.getResultSet(DurationLevel_2_3_Query);

		if (rs3.next() | rs4.next()) {
			String firstRTW = rs3.getString("DateDifference");
			DurationLevel_1_2 = Double.parseDouble(firstRTW);
			DurationLevel_1_2 = Double.valueOf(twoDForm
					.format(DurationLevel_1_2));
			String fullRTW = rs4.getString("DateDifference");
			DurationLevel_2_3 = Double.parseDouble(fullRTW);
			DurationLevel_2_3 = Double.valueOf(twoDForm
					.format(DurationLevel_2_3));

		}

		String averageOnSiteTimeQuery = "SELECT isnull(AVG(DATEDIFF(mi, lxschedule.lx_se_start, lxschedule.lx_se_end)/(60*1.0)),0.0) AS DATEDIFFERENCE  "
				+ "FROM lm_help_desk_ticket_details helpdesk"
				+ " INNER JOIN lm_ticket lmtickets ON helpdesk.lm_hd_tc_id= lmtickets.lm_tc_id"
				+ " INNER JOIN lm_job lmjob ON lmjob.lm_js_id = lmtickets.lm_tc_js_id"
				+ " INNER JOIN lx_schedule_element lxschedule ON lxschedule.lx_se_type_id = lmjob.lm_js_id"
				+ " WHERE  helpdesk.lm_hd_category = 1 AND helpdesk.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND helpdesk.lm_hd_opened <= GetDate()";

		double averageOnSiteTime = 0.0;

		ResultSet rs5 = this.da.getResultSet(averageOnSiteTimeQuery);

		if (rs5.next()) {
			String firstRTW = rs5.getString("DATEDIFFERENCE");
			averageOnSiteTime = Double.parseDouble(firstRTW);
			averageOnSiteTime = Double.valueOf(twoDForm
					.format(averageOnSiteTime));

		}

		// First Time Installation vs Revisit Installation
		String firstTimeInstallQuery = "Select Count(*) as total From    (  SELECT lx_se_type_id	"
				+ "FROM lx_schedule_element WHERE  lx_se_start >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ")	AND lx_se_start <= GetDate() GROUP BY lx_se_type_id HAVING  count(*) = 1   ) AS Z";
		String revisitInstallQuery = "Select Count(*) as total From    (  SELECT lx_se_type_id	FROM lx_schedule_element "
				+ "WHERE  lx_se_start >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ")	AND lx_se_start <= GetDate() GROUP BY lx_se_type_id HAVING  count(*) > 2   ) AS Z";

		int firstTimeInstall = 0;
		int revisitInstall = 0;

		ResultSet rs6 = this.da.getResultSet(firstTimeInstallQuery);
		ResultSet rs7 = this.da.getResultSet(revisitInstallQuery);

		if (rs6.next() | rs7.next()) {
			String firstRTW = rs6.getString("total");
			firstTimeInstall = Integer.parseInt(firstRTW);

			String fullRTW = rs7.getString("total");
			revisitInstall = Integer.parseInt(fullRTW);

		}

		// # of tickets opened during PPS hour and weekdays
		String ticketsPpsQuery = "SELECT count(*) as total FROM lm_help_desk_ticket_details "
				+ "WHERE lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lm_hd_opened),'08:00') and lm_hd_opened < DATEADD(dd,DATEDIFF(dd,0,lm_hd_opened),'18:00')"
				+ " AND lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND lm_hd_opened <= GetDate() ";
		String ticketsWeekDays_6pm_2am_Query = "SELECT count(*) as total FROM lm_help_desk_ticket_details "
				+ "WHERE (lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lm_hd_opened),'06:00:00 PM')"
				+ "AND lm_hd_opened < DATEADD(dd,DATEDIFF(dd,0,lm_hd_opened),'12:00:00 AM') )"
				+ "OR  (lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lm_hd_opened),'12:00:00 AM')"
				+ "AND lm_hd_opened < DATEADD(dd,DATEDIFF(dd,0,lm_hd_opened),'02:00:00 AM') )"
				+ "AND lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ")	AND lm_hd_opened <= GetDate()"
				+ "and DATEPART(dw, lm_hd_opened) NOT IN (1, 7)";
		String ticketsWeekDays_2am_8am_Query = "SELECT count(*) as total FROM lm_help_desk_ticket_details "
				+ "WHERE lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lm_hd_opened),'02:00') and lm_hd_opened <= DATEADD(dd,DATEDIFF(dd,0,lm_hd_opened),'08:00')"
				+ " AND lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ")	AND lm_hd_opened <= GetDate() and DATEPART(dw, lm_hd_opened) NOT IN (1, 7)";

		String ticketsWeekendDays_All = "SELECT count(*) as total FROM lm_help_desk_ticket_details "
				+ "WHERE lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lm_hd_opened),'02:00') and lm_hd_opened <= DATEADD(dd,DATEDIFF(dd,0,lm_hd_opened),'08:00')"
				+ " AND lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -30)	AND lm_hd_opened <= GetDate() and DATEPART(dw, lm_hd_opened) IN (1, 7)";
		int ticketsPps = 0;
		int ticketsWeekDays_6pm_2am = 0;
		int ticketsWeekDays_2am_8am = 0;
		int ticketweekenddays = 0;
		ResultSet rs8 = this.da.getResultSet(ticketsPpsQuery);
		ResultSet rs9 = this.da.getResultSet(ticketsWeekDays_6pm_2am_Query);
		ResultSet rs10 = this.da.getResultSet(ticketsWeekDays_2am_8am_Query);
		ResultSet rs11d = this.da.getResultSet(ticketsWeekendDays_All);
		if (rs8.next() | rs9.next() | rs10.next() | rs11d.next()) {
			String pps = rs8.getString("total");
			ticketsPps = Integer.parseInt(pps);

			String weekdays1 = rs9.getString("total");
			ticketsWeekDays_6pm_2am = Integer.parseInt(weekdays1);

			String weekdays2 = rs10.getString("total");
			ticketsWeekDays_2am_8am = Integer.parseInt(weekdays2);
			String ticketweek = rs11d.getString("total");
			ticketweekenddays = Integer.parseInt(ticketweek);

		}

		// Calls Trending
		String callsPpsQuery = "SELECT count(*) as total FROM CiscoPhones.dbo.contactcalldetail"
				+ " WHERE startdatetime >= DATEADD(dd,DATEDIFF(dd,0,startdatetime),'08:00')"
				+ " and startdatetime < DATEADD(dd,DATEDIFF(dd,0,startdatetime),'18:00')"
				+ " AND startdatetime >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND startdatetime <= GetDate() ";

		String callsWeekDays_6pm_2am_Query = "SELECT count(*) as total FROM CiscoPhones.dbo.contactcalldetail"
				+ " WHERE (startdatetime >= DATEADD(dd,DATEDIFF(dd,0,startdatetime),'06:00:00 PM') AND startdatetime < DATEADD(dd,DATEDIFF(dd,0,startdatetime),'12:00:00 AM') )"
				+ " OR  (startdatetime >= DATEADD(dd,DATEDIFF(dd,0,startdatetime),'12:00:00 AM') AND startdatetime < DATEADD(dd,DATEDIFF(dd,0,startdatetime),'02:00:00 AM') )"
				+ " AND startdatetime >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ")	AND startdatetime <= GetDate()"
				+ " and DATEPART(dw, startdatetime) NOT IN (1, 7)";

		String callsWeekDays_2am_8am_Query = "SELECT count(*) as total FROM CiscoPhones.dbo.contactcalldetail "
				+ " WHERE startdatetime >= DATEADD(dd,DATEDIFF(dd,0,startdatetime),'02:00:00 AM') and startdatetime <= DATEADD(dd,DATEDIFF(dd,0,startdatetime),'08:00:00 AM')"
				+ " AND startdatetime >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ")	AND startdatetime <= GetDate() and DATEPART(dw, startdatetime) NOT IN (1, 7)";

		String callweekendsDays_All = " SELECT count(*) as total FROM CiscoPhones.dbo.contactcalldetail "
				+ " WHERE startdatetime >= DATEADD(dd,DATEDIFF(dd,0,startdatetime),'12:00:00 AM') "
				+ " and startdatetime <= DATEADD(dd,DATEDIFF(dd,0,startdatetime),'11:59:00 PM') "
				+ "	AND startdatetime >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") "
				+ " AND startdatetime <= GetDate() and DATEPART(dw, startdatetime) NOT IN (2,3,4,5,6)";
		int callsPps = 0;
		int callsWeekDays_6pm_2am = 0;
		int callsWeekDays_2am_8am = 0;
		int callweekenddays = 0;
		ResultSet rs8c = this.da.getResultSet(callsPpsQuery);
		ResultSet rs9c = this.da.getResultSet(callsWeekDays_6pm_2am_Query);
		ResultSet rs10c = this.da.getResultSet(callsWeekDays_2am_8am_Query);
		ResultSet rs11c = this.da.getResultSet(callweekendsDays_All);
		if (rs8c.next() | rs9c.next() | rs10c.next() | rs11c.next()) {
			String pps = rs8c.getString("total");
			callsPps = Integer.parseInt(pps);

			String weekdays1 = rs9c.getString("total");
			callsWeekDays_6pm_2am = Integer.parseInt(weekdays1);

			String weekdays2 = rs10c.getString("total");
			callsWeekDays_2am_8am = Integer.parseInt(weekdays2);

			String weekend = rs11c.getString("total");
			callweekenddays = Integer.parseInt(weekend);
		}

		// Mean time to repair

		String mttrLastThrityDaysQuery = "SELECT Avg(iw_om_mttr_minutes) as total FROM wug_dw_iw_outage_mttr where iw_om_date >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND iw_om_date <= GetDate()";
		String mttrByClientQuery = "SELECT lx_pr_end_customer  as cname, Avg(mttr.iw_om_mttr_minutes) as ctime "
				+ " FROM wug_dw_iw_outage_mttr mttr "
				+ " JOIN lx_appendix_main ON iw_om_pr_id = lx_pr_id "
				+ " WHERE lx_pr_end_customer IS NOT NULL "
				+ " GROUP BY mttr.iw_om_mm_id, "
				+ " lx_pr_end_customer "
				+ " having Avg(mttr.iw_om_mttr_minutes) > 0 "
				+ " order by  lx_pr_end_customer";

		double mttr = 0.0;
		int mttrtime = 0;
		ResultSet rs11 = this.da.getResultSet(mttrLastThrityDaysQuery);
		ResultSet rs12 = this.da.getResultSet(mttrByClientQuery);
		if (rs11.next()) {
			String mtr = rs11.getString("total");
			if (mtr == null) {
				mtr = "0.00";
			}
			double i = Double.parseDouble(mtr);

			mttrtime = minutesToSeconds(i);
		}
		ArrayList<String[]> mttrByClient = new ArrayList<String[]>();
		while (rs12.next()) {
			String name = rs12.getString("cname");

			String total = rs12.getString("ctime");
			double i = Double.parseDouble(total);
			int time = minutesToSeconds(i);
			String resulttime = calcHMS(time);
			String[] strarr = { name, "" + resulttime };
			mttrByClient.add(strarr);

		}
		String param = TicketCountDAO.getCountDetails(days);
		request.setAttribute("param", param);
		request.setAttribute("TrendLevel_1_2", TrendLevel_1_2);
		request.setAttribute("TrendLevel_2_3", TrendLevel_2_3);
		request.setAttribute("DurationLevel_1_2", DurationLevel_1_2);
		request.setAttribute("DurationLevel_2_3", DurationLevel_2_3);
		request.setAttribute("averageOnSiteTime", averageOnSiteTime);
		request.setAttribute("firstTimeInstall", firstTimeInstall);
		request.setAttribute("revisitInstall", revisitInstall);
		request.setAttribute("callweekenddays", callweekenddays);
		request.setAttribute("ticketsPps", ticketsPps);
		request.setAttribute("ticketsWeekDays_6pm_2am", ticketsWeekDays_6pm_2am);
		request.setAttribute("ticketsWeekDays_2am_8am", ticketsWeekDays_2am_8am);
		request.setAttribute("ticketweekenddays", ticketweekenddays);
		request.setAttribute("callsPps", callsPps);
		request.setAttribute("callsWeekDays_6pm_2am", callsWeekDays_6pm_2am);
		request.setAttribute("callsWeekDays_2am_8am", callsWeekDays_2am_8am);
		new Double(mttr);
		mttrLastThrityDaysQuery = calcHMS(mttrtime);
		request.setAttribute("meanTimeLastThrityDays", mttrLastThrityDaysQuery);
		request.setAttribute("mttrByClient", mttrByClient);

		DecimalFormat twoDFormd = new DecimalFormat("#.##");

		for (int i = -7; i < 0; i++) {
			// current date time
			Calendar cal = Calendar.getInstance();
			Date dt = new Date();
			cal.setTime(dt);
			cal.add(Calendar.DATE, i);

			String strDay = getDayName("" + cal.get(Calendar.DAY_OF_WEEK));

			String newTicketsQuery = "SELECT count(*) as newTotal  FROM lm_help_desk_ticket_details WHERE lm_hd_status=1 AND lm_hd_opened >= DATEADD(day,"
					+ i
					+ ", GETDATE()) and  lm_hd_opened < DATEADD(day,"
					+ (i + 1) + ", GETDATE())";
			String resolveTicketsQuery = "SELECT count(*) AS resolvedTotal  FROM lm_help_desk_ticket_details WHERE lm_hd_status=6 AND lm_hd_opened BETWEEN DATEADD(day,"
					+ i
					+ ", GETDATE()) and  DATEADD(day,"
					+ (i + 1)
					+ ", GETDATE())";

			ResultSet rs21 = this.da.getResultSet(newTicketsQuery);
			ResultSet rs22 = this.da.getResultSet(resolveTicketsQuery);

			if (rs21.next() | rs22.next()) {
				String total1 = rs21.getString("newTotal");
				String total2 = rs22.getString("resolvedTotal");

				String strdata[] = { strDay, total1, total2 };
				listData.add(strdata);
			}
		}

		String newTicketsThisWeekQuery = "SELECT count(*) as newTotal   FROM lm_help_desk_ticket_details "
				+ "WHERE lm_hd_status=1  and 	lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) AND lm_hd_opened <= GetDate()";
		String resolveTicketsThisWeekQuery = "SELECT count(*) AS resolvedTotal  FROM lm_help_desk_ticket_details "
				+ "WHERE lm_hd_status=11 and lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) AND lm_hd_opened <= GetDate()";
		ResultSet rs31 = this.da.getResultSet(newTicketsThisWeekQuery);
		ResultSet rs32 = this.da.getResultSet(resolveTicketsThisWeekQuery);

		int newTicketsThisWeek = 0;
		int resolveTicketsThisWeek = 0;
		double percentageThisWeek = 0.0;
		if (rs31.next() | rs32.next()) {
			String total1 = rs31.getString("newTotal");
			newTicketsThisWeek = Integer.parseInt(total1);

			String total2 = rs32.getString("resolvedTotal");
			resolveTicketsThisWeek = Integer.parseInt(total2);
			if (newTicketsThisWeek > 0) {
				percentageThisWeek = ((resolveTicketsThisWeek * 1.0) / newTicketsThisWeek) * 100.0;
				percentageThisWeek = Double.valueOf(twoDFormd
						.format(percentageThisWeek));
			}
		}

		String newTicketsLastWeekQuery = "SELECT count(*) AS newTotal  FROM lm_help_desk_ticket_details "
				+ "WHERE lm_hd_status=1 AND lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7) and lm_hd_opened <= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -1)";
		String resolveTicketsLastWeekQuery = "SELECT count(*) AS resolvedTotal  FROM lm_help_desk_ticket_details "
				+ "WHERE lm_hd_status=11 AND lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7) and lm_hd_opened <= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -1)";
		ResultSet rs43 = this.da.getResultSet(newTicketsLastWeekQuery);
		ResultSet rs44 = this.da.getResultSet(resolveTicketsLastWeekQuery);

		int newTicketsLastWeek = 0;
		int resolveTicketsLastWeek = 0;
		double percentageLastWeek = 0.0;
		if (rs43.next() | rs44.next()) {
			String total1 = rs43.getString("newTotal");
			newTicketsLastWeek = Integer.parseInt(total1);

			String total2 = rs44.getString("resolvedTotal");
			resolveTicketsLastWeek = Integer.parseInt(total2);
			if (newTicketsLastWeek > 0) {
				percentageLastWeek = ((resolveTicketsLastWeek * 1.0) / newTicketsLastWeek) * 100.0;
				percentageLastWeek = Double.valueOf(twoDFormd
						.format(percentageLastWeek));
			}
		}

		request.setAttribute("newTicketsThisWeek", newTicketsThisWeek);
		request.setAttribute("resolveTicketsThisWeek", resolveTicketsThisWeek);
		request.setAttribute("percentageThisWeek", percentageThisWeek);

		request.setAttribute("newTicketsLastWeek", newTicketsLastWeek);
		request.setAttribute("resolveTicketsLastWeek", resolveTicketsLastWeek);
		request.setAttribute("percentageLastWeek", percentageLastWeek);

		request.setAttribute("listdata", listData);

		rs43.close();
		rs44.close();
		rs31.close();
		rs32.close();

		FullResolutionForm minutemanUtlizationForm = (FullResolutionForm) form;

		ArrayList<String[]> listDatares = new ArrayList<String[]>();
		List<FullResolutionBean> listDataAck = new ArrayList<FullResolutionBean>();

		days = "30";

		if (request.getParameter("days") != null
				&& !request.getParameter("days").equals("")) {
			days = request.getParameter("days").toString();
		} else if (request.getAttribute("days") != null
				&& !request.getAttribute("days").toString().equals("")) {
			days = request.getAttribute("days").toString();
		}

		request.setAttribute("days", days);

		if (request.getParameter("year") != null
				&& !request.getParameter("year").equals("")) {
			days = request.getParameter("days").toString();
		} else if (request.getAttribute("year") != null
				&& !request.getAttribute("year").toString().equals("")) {
			days = request.getAttribute("year").toString();
		}
		String value = minutemanUtlizationForm.getYear();
		String firstTimeResQueryYear;
		String fulltimeResQueryYear;

		for (int i = 1; i <= 12; i++) {

			if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10
					|| i == 12) {

				firstTimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets FROM lm_help_desk_ticket_details lhd  "
						+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
						+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
						+ "WHERE dat.lm_hd_at_incident_status  <> 'REOPENED' AND lm_hd_opened >= '"
						+ i
						+ "/1/"
						+ value
						+ "'"
						+ "AND lm_hd_opened <= '"
						+ i
						+ "/31/" + value + "'";

				fulltimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets FROM lm_help_desk_ticket_details lhd  "
						+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
						+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
						+ "WHERE dat.lm_hd_at_incident_status = 'REOPENED'  AND lm_hd_opened >= '"
						+ i
						+ "/1/"
						+ value
						+ "'"
						+ "AND lm_hd_opened <= '"
						+ i
						+ "/31/" + value + "'";

			} else {

				if (i != 2) {

					firstTimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets FROM lm_help_desk_ticket_details lhd  "
							+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
							+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
							+ "WHERE dat.lm_hd_at_incident_status  <> 'REOPENED' AND lm_hd_opened >= '"
							+ i
							+ "/1/"
							+ value
							+ "'"
							+ "AND lm_hd_opened <= '"
							+ i + "/30/" + value + "'";

					fulltimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets FROM lm_help_desk_ticket_details lhd  "
							+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
							+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
							+ "WHERE dat.lm_hd_at_incident_status = 'REOPENED'   AND lm_hd_opened >= '"
							+ i
							+ "/1/"
							+ value
							+ "'"
							+ "AND lm_hd_opened <= '"
							+ i + "/30/" + value + "'";

				}

			}
		}

		String ticketRawDataNotReOpenedLastWeek = "SELECT DISTINCT *, DATEDIFF(hour, lm_hd_opened, lm_hd_update_date) AS resolveTimeH"
				+ " FROM lm_help_desk_ticket_details lhd "
				+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id"
				+ " WHERE lhd.lm_hd_status IN (6, 11)"
				+ " AND lm_tc_number IN (SELECT lm_hd_at_ticket_id FROM lm_help_desk_audit_trail"
				+ " WHERE lm_hd_at_incident_status <> 'Reopened')"
				+ " AND (lhd.lm_hd_update_date  >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7)"
				+ " AND lhd.lm_hd_update_date  <=  GetDate())";

		String ticketRawDataReOpenedLastWeek = "SELECT DISTINCT *, DATEDIFF(hour, lm_hd_opened, lm_hd_update_date) AS resolveTimeH"
				+ " FROM lm_help_desk_ticket_details lhd "
				+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id"
				+ " WHERE lhd.lm_hd_status IN (6, 11)"
				+ " AND lm_tc_number IN (SELECT lm_hd_at_ticket_id FROM lm_help_desk_audit_trail"
				+ " WHERE lm_hd_at_incident_status = 'Reopened')"
				+ " AND (lhd.lm_hd_update_date  >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7)"
				+ " AND lhd.lm_hd_update_date  <=  GetDate())";
		String ticketRawDataNotReOpenedThisWeek = "SELECT DISTINCT *, DATEDIFF(hour, lm_hd_opened, lm_hd_update_date) AS resolveTimeH"
				+ " FROM lm_help_desk_ticket_details lhd "
				+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id"
				+ " WHERE lhd.lm_hd_status IN (6, 11)"
				+ " AND lm_tc_number IN (SELECT lm_hd_at_ticket_id FROM lm_help_desk_audit_trail"
				+ " WHERE lm_hd_at_incident_status <> 'Reopened')"
				+ " AND (lhd.lm_hd_update_date  >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0)"
				+ " AND lhd.lm_hd_update_date  <=  GetDate())";
		String ticketRawDataReopenedThisWeek = "SELECT DISTINCT *, DATEDIFF(hour, lm_hd_opened, lm_hd_update_date) AS resolveTimeH"
				+ " FROM lm_help_desk_ticket_details lhd "
				+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id"
				+ " WHERE lhd.lm_hd_status IN (6, 11)"
				+ " AND lm_tc_number IN (SELECT lm_hd_at_ticket_id FROM lm_help_desk_audit_trail"
				+ " WHERE lm_hd_at_incident_status = 'Reopened')"
				+ " AND (lhd.lm_hd_update_date  >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0)"
				+ " AND lhd.lm_hd_update_date  <=  GetDate())";

		for (int i = -7; i < 0; i++) {
			// current date time
			Calendar cal = Calendar.getInstance();
			Date dt = new Date();
			cal.setTime(dt);
			cal.add(Calendar.DATE, i);

			String strDay = getDayName("" + cal.get(Calendar.DAY_OF_WEEK));

			String firstTimeResQuery = "SELECT isnull(AVG((DATEDIFF(mi, lm_hd_opened, lm_hd_update_date))/(60*1.0)),0.0) AS DATEDIFFERENCE FROM ( "
					+ getTicketRawDataNotReOpened(i) + " ) AS rawDataTable ";
			String fullTimeResQuery = "SELECT isnull(AVG((DATEDIFF(mi, lm_hd_opened, lm_hd_update_date))/(60*1.0)),0.0) AS DATEDIFFERENCE FROM ( "
					+ getTicketRawDataReopened(i) + " ) AS rawDataTable";
			ResultSet rs1111 = this.da.getResultSet(firstTimeResQuery);
			ResultSet rs2222 = this.da.getResultSet(fullTimeResQuery);

			if (rs1111.next() | rs2222.next()) {
				String total1 = rs1111.getString("DATEDIFFERENCE");
				String total2 = rs2222.getString("DATEDIFFERENCE");

				String strdata[] = { strDay, total1, total2 };
				listDatares.add(strdata);
			}
		}

		String firstTimeResolutionThisWeekQuery = "SELECT isnull(AVG((DATEDIFF(mi, lm_hd_opened, lm_hd_update_date))/(60*1.0)),0.0) AS DATEDIFFERENCE FROM ("
				+ ticketRawDataNotReOpenedThisWeek + " ) AS rawDataTable ";
		String fullTimeResolutionThisWeekQuery = "SELECT isnull(AVG((DATEDIFF(mi, lm_hd_opened, lm_hd_update_date))/(60*1.0)),0.0) AS DATEDIFFERENCE FROM ("
				+ ticketRawDataReopenedThisWeek + " ) AS rawDataTable";
		double firstTimeResolutionThisWeek = 0.0;
		double fullTimeResolutionThisWeek = 0.0;

		ResultSet rs12345 = this.da
				.getResultSet(firstTimeResolutionThisWeekQuery);
		ResultSet rs2345 = this.da
				.getResultSet(fullTimeResolutionThisWeekQuery);
		if (rs12345.next() | rs2345.next()) {
			String firstRTW = rs12345.getString("DATEDIFFERENCE");
			firstTimeResolutionThisWeek = Double.parseDouble(firstRTW);
			firstTimeResolutionThisWeek = Double.valueOf(twoDForm
					.format(firstTimeResolutionThisWeek));
			String fullRTW = rs2345.getString("DATEDIFFERENCE");
			fullTimeResolutionThisWeek = Double.parseDouble(fullRTW);
			fullTimeResolutionThisWeek = Double.valueOf(twoDForm
					.format(fullTimeResolutionThisWeek));

		}

		String firstTimeResolutionLastWeekQuery = "SELECT isnull(AVG((DATEDIFF(hour, lm_hd_opened, lm_hd_update_date))),0.0) AS DATEDIFFERENCE FROM ("
				+ ticketRawDataNotReOpenedLastWeek + " ) AS rawDataTable";

		String fullTimeResolutionLastWeekQuery = "SELECT isnull(AVG((DATEDIFF(hour, lm_hd_opened, lm_hd_update_date))),0.0) AS DATEDIFFERENCE FROM ("
				+ ticketRawDataReOpenedLastWeek + " ) AS rawDataTable";

		double firstTimeResolutionLastWeek = 0.0;
		double fullTimeResolutionLastWeek = 0.0;

		ResultSet rs345 = this.da
				.getResultSet(firstTimeResolutionLastWeekQuery);
		ResultSet rs445 = this.da.getResultSet(fullTimeResolutionLastWeekQuery);
		if (rs345.next() | rs445.next()) {
			String firstRTW = rs345.getString("DATEDIFFERENCE");
			firstTimeResolutionLastWeek = Double.parseDouble(firstRTW);
			firstTimeResolutionLastWeek = Double.valueOf(twoDForm
					.format(firstTimeResolutionLastWeek));

			String fullRTW = rs445.getString("DATEDIFFERENCE");
			fullTimeResolutionLastWeek = Double.parseDouble(fullRTW);
			fullTimeResolutionLastWeek = Double.valueOf(twoDForm
					.format(fullTimeResolutionLastWeek));
		}

		listDataAck = daoobj.getAckTicketData(String.valueOf(days));

		request.setAttribute("firstTimeResolutionThisWeek",
				firstTimeResolutionThisWeek);
		request.setAttribute("fullTimeResolutionThisWeek",
				fullTimeResolutionThisWeek);
		request.setAttribute("firstTimeResolutionLastWeek",
				firstTimeResolutionLastWeek);
		request.setAttribute("fullTimeResolutionLastWeek",
				fullTimeResolutionLastWeek);

		request.setAttribute("listDatares", listDatares);
		request.setAttribute("listDataAck", listDataAck);

		rs1.close();
		rs2.close();
		rs3.close();
		rs4.close();

		return mapping.findForward(forward_key);
	}

	public ActionForward getTickeResolutiontData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "ticketResolutionData";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		FullResolutionForm minutemanUtlizationForm = (FullResolutionForm) form;

		ArrayList<String[]> listData = new ArrayList<String[]>();
		ArrayList<String[]> listDatayear = new ArrayList<String[]>();
		DecimalFormat twoDForm = new DecimalFormat("#.##");

		String days = "30";

		if (request.getParameter("days") != null
				&& !request.getParameter("days").equals("")) {
			days = request.getParameter("days").toString();
		} else if (request.getAttribute("days") != null
				&& !request.getAttribute("days").toString().equals("")) {
			days = request.getAttribute("days").toString();
		}

		request.setAttribute("days", days);

		String value = minutemanUtlizationForm.getYear();
		Calendar calendar = Calendar.getInstance();
		String firstTimeResQueryYear;
		String fulltimeResQueryYear;

		if (value == null) {

			int year = calendar.get(Calendar.YEAR);
			value = String.valueOf(year);

		}

		for (int i = 1; i <= 12; i++) {

			if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10
					|| i == 12) {

				firstTimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets FROM lm_help_desk_ticket_details lhd  "
						+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
						+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
						+ "WHERE dat.lm_hd_at_incident_status  <> 'REOPENED' AND lm_hd_opened >= '"
						+ i
						+ "/1/"
						+ value
						+ "'"
						+ "AND lm_hd_opened <= '"
						+ i
						+ "/31/" + value + "'";

				fulltimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets FROM lm_help_desk_ticket_details lhd  "
						+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
						+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
						+ "WHERE dat.lm_hd_at_incident_status = 'REOPENED'  AND lm_hd_opened >= '"
						+ i
						+ "/1/"
						+ value
						+ "'"
						+ "AND lm_hd_opened <= '"
						+ i
						+ "/31/" + value + "'";

			} else {

				if (i != 2) {

					firstTimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets "
							+ "FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
							+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
							+ "WHERE dat.lm_hd_at_incident_status  <> 'REOPENED' AND lm_hd_opened >= '"
							+ i
							+ "/1/"
							+ value
							+ "'"
							+ "AND lm_hd_opened <= '"
							+ i + "/30/" + value + "'";

					fulltimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets "
							+ "FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
							+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
							+ "WHERE dat.lm_hd_at_incident_status = 'REOPENED'   AND lm_hd_opened >= '"
							+ i
							+ "/1/"
							+ value
							+ "'"
							+ "AND lm_hd_opened <= '"
							+ i + "/30/" + value + "'";

				} else {

					boolean flagleap = isleapyear(value);
					if (flagleap == false) {

						firstTimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets FROM lm_help_desk_ticket_details lhd  "
								+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
								+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
								+ "WHERE dat.lm_hd_at_incident_status  <> 'REOPENED' AND lm_hd_opened >= '"
								+ i
								+ "/1/"
								+ value
								+ "'"
								+ "AND lm_hd_opened <= '"
								+ i
								+ "/28/"
								+ value
								+ "'";

						fulltimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets FROM lm_help_desk_ticket_details lhd  "
								+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
								+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
								+ "WHERE dat.lm_hd_at_incident_status = 'REOPENED'  AND lm_hd_opened >= '"
								+ i
								+ "/1/"
								+ value
								+ "'"
								+ "AND lm_hd_opened <= '"
								+ i
								+ "/28/"
								+ value
								+ "'";

					} else {

						firstTimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets FROM lm_help_desk_ticket_details lhd "
								+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
								+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
								+ "WHERE dat.lm_hd_at_incident_status  <> 'REOPENED' AND lm_hd_opened >= '"
								+ i
								+ "/1/"
								+ value
								+ "'"
								+ "AND lm_hd_opened <= '"
								+ i
								+ "/29/"
								+ value
								+ "'";

						fulltimeResQueryYear = "SELECT count(DISTINCT lm_tc_id) as totaltickets FROM lm_help_desk_ticket_details lhd  "
								+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
								+ " INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
								+ "WHERE dat.lm_hd_at_incident_status = 'REOPENED'   AND lm_hd_opened >= '"
								+ i
								+ "/1/"
								+ value
								+ "'"
								+ "AND lm_hd_opened <= '"
								+ i
								+ "/29/"
								+ value
								+ "'";

					}

				}

			}

			ResultSet rs1 = this.da.getResultSet(firstTimeResQueryYear);
			ResultSet rs2 = this.da.getResultSet(fulltimeResQueryYear);
			String[] months = { "", "Jan", "Feb", "March", "April", "May",
					"June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" };

			if (rs1.next() | rs2.next()) {

				String total1 = rs1.getString("totaltickets");
				String total2 = rs2.getString("totaltickets");

				String strdata[] = { months[i], total1, total2 };
				listDatayear.add(strdata);
			}

		}

		request.setAttribute("listDatayear", listDatayear);
		request.setAttribute("year", value);

		// calculating percentage of first call completion.
		String allCompletionQuery = "SELECT count(DISTINCT lm_tc_id) as totaltickets "
				+ " FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
				+ " INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id"
				+ " WHERE lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND lhd.lm_hd_opened <= GetDate()";

		String firstCallcompletionQuery = "SELECT count(DISTINCT lm_tc_id) as total "
				+ " FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
				+ " INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id"
				+ " WHERE dat.lm_hd_at_incident_status  <> 'REOPENED'	AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND lhd.lm_hd_opened <= GetDate()";
		int firstCallcompletion = 0;
		int allCompletion = 0;

		double firstCallCompletionPercentage = 0.0;

		ResultSet rs5 = this.da.getResultSet(firstCallcompletionQuery);
		ResultSet rs5a = this.da.getResultSet(allCompletionQuery);
		if (rs5.next() | rs5a.next()) {
			String fcCompletion = rs5.getString("total");
			String aCompletion = rs5a.getString("totaltickets");
			firstCallcompletion = Integer.parseInt(fcCompletion);
			allCompletion = Integer.parseInt(aCompletion);

			if (allCompletion > 0) {
				firstCallCompletionPercentage = (firstCallcompletion / (allCompletion * 1.0)) * 100.0;
				firstCallCompletionPercentage = Double.valueOf(twoDForm
						.format(firstCallCompletionPercentage));
			}
		}

		// Calculating percentage of first call completion tickets based on pps
		String firstCallcompletionPpsQuery = "SELECT count(DISTINCT lm_tc_id) as totalTickets FROM lm_help_desk_ticket_details lhd  "
				+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE dat.lm_hd_at_incident_status  not like  'REOPENED' AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'08:00') "
				+ "and lhd.lm_hd_opened < DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'20:00') and DATEDIFF(dw, 0, lhd.lm_hd_opened) % 7 <= 4 ";
		String totalTicketsByPpsQuery = "SELECT count(DISTINCT lm_tc_id) as totalTickets FROM lm_help_desk_ticket_details lhd  "
				+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'08:00') "
				+ "and lhd.lm_hd_opened < DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'20:00') and DATEDIFF(dw, 0, lhd.lm_hd_opened) % 7 <= 4 ";

		int firstCallcompletionPps = 0;
		int totalTicketsByPp = 0;

		ResultSet rs6 = this.da.getResultSet(firstCallcompletionPpsQuery);
		ResultSet rs6a = this.da.getResultSet(totalTicketsByPpsQuery);

		double firstCallCompletionPpsPercentage = 0.0;

		if (rs6.next() | rs6a.next()) {
			String fcCompletion = rs6.getString("totalTickets");
			String aCompletion = rs6a.getString("totalTickets");

			firstCallcompletionPps = Integer.parseInt(fcCompletion);
			totalTicketsByPp = Integer.parseInt(aCompletion);
			System.out.println("Tickets total pps:" + totalTicketsByPp);
			if (totalTicketsByPp > 0) {
				firstCallCompletionPpsPercentage = (firstCallcompletionPps / (totalTicketsByPp * 1.0)) * 100.0;
				firstCallCompletionPpsPercentage = Double.valueOf(twoDForm
						.format(firstCallCompletionPpsPercentage));
			}
		}

		// Calculating percentage of first call completion based on non-pps
		String firstCallcompletionNonPpsQuery = "SELECT count(DISTINCT lm_tc_id) as totalTickets FROM lm_help_desk_ticket_details lhd  "
				+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE dat.lm_hd_at_incident_status  not like  'REOPENED' AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and (lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'20:00')  "
				+ "and lhd.lm_hd_opened <= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'23:59') ) or (lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'23:59') "
				+ " and lhd.lm_hd_opened <= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'08:00') ) and DATEDIFF(dw, 0, lhd.lm_hd_opened) % 7 <= 4 ";

		String totalTicketsByNonPpsQuery = "SELECT count(DISTINCT lm_tc_id) as totalTickets  FROM lm_help_desk_ticket_details lhd  "
				+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and (lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'20:00') "
				+ " and lhd.lm_hd_opened <= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'23:59') ) or (lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'23:59')  "
				+ "and lhd.lm_hd_opened <= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'08:00') ) and DATEDIFF(dw, 0, lhd.lm_hd_opened) % 7 <= 4 ";

		ResultSet rs7 = this.da.getResultSet(firstCallcompletionNonPpsQuery);
		ResultSet rs7a = this.da.getResultSet(totalTicketsByNonPpsQuery);

		int firstCallcompletionNonPps = 0;
		int totalTicketsByNonPps = 0;

		double firstCallCompletionNonPpsPercentage = 0.0;

		if (rs7.next() | rs7a.next()) {
			String fcCompletion = rs7.getString("totalTickets");
			String aCompletion = rs7a.getString("totalTickets");

			firstCallcompletionNonPps = Integer.parseInt(fcCompletion);
			totalTicketsByNonPps = Integer.parseInt(aCompletion);
			System.out.println("Tickets total Non-pps:" + totalTicketsByNonPps);
			if (totalTicketsByNonPps > 0) {
				firstCallCompletionNonPpsPercentage = (firstCallcompletionNonPps / (totalTicketsByNonPps * 1.0)) * 100.0;
				firstCallCompletionNonPpsPercentage = Double.valueOf(twoDForm
						.format(firstCallCompletionNonPpsPercentage));
			}
		}

		// Calculating percentage of first call completion based on weekends
		String firstCallweekendQuery = "SELECT count(DISTINCT lm_tc_id) as totalTickets FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id WHERE dat.lm_hd_at_incident_status  not like  'REOPENED' AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and DATEPART(dw, lhd.lm_hd_opened)  IN (1, 7)";
		String totalTicketsInWeekendQuery = "SELECT count(DISTINCT lm_tc_id) as totalTickets  FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id WHERE lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and DATEPART(dw, lhd.lm_hd_opened)  IN (1, 7)";

		ResultSet rs8 = this.da.getResultSet(firstCallweekendQuery);
		ResultSet rs8a = this.da.getResultSet(totalTicketsInWeekendQuery);

		int firstCallweekend = 0;
		int totalTicketsInWeekend = 0;

		double firstCallWeekendPercentage = 0.0;

		if (rs8.next() | rs8a.next()) {
			String fcCompletion = rs8.getString("totalTickets");
			String aCompletion = rs8a.getString("totalTickets");

			firstCallweekend = Integer.parseInt(fcCompletion);
			totalTicketsInWeekend = Integer.parseInt(aCompletion);
			System.out.println("Tickets total in weekends:"
					+ totalTicketsInWeekend);
			if (totalTicketsInWeekend > 0) {
				firstCallWeekendPercentage = (firstCallweekend / (totalTicketsInWeekend * 1.0)) * 100.0;
				firstCallWeekendPercentage = Double.valueOf(twoDForm
						.format(firstCallWeekendPercentage));
			}
		}

		request.setAttribute("firstCallcompletionPercentage",
				firstCallCompletionPercentage);
		request.setAttribute("firstCallcompletionPps",
				firstCallCompletionPpsPercentage);
		request.setAttribute("firstCallcompletionNonPps",
				firstCallCompletionNonPpsPercentage);
		request.setAttribute("firstCallWeekendPercentage",
				firstCallWeekendPercentage);

		request.setAttribute("listdata", listData);

		rs5.close();
		rs5a.close();
		rs6.close();
		rs6a.close();
		rs7.close();
		rs7a.close();
		rs8.close();
		rs8a.close();
		return mapping.findForward(forward_key);
	}

	private String getTicketRawDataReopened(int i) {
		String ticketRawDataNotReOpened = "SELECT DISTINCT *, DATEDIFF(hour, lm_hd_opened, lm_hd_update_date) AS resolveTimeH"
				+ " FROM lm_help_desk_ticket_details lhd "
				+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id"
				+ " WHERE lhd.lm_hd_status IN (6, 11)"
				+ " AND lm_tc_number IN (SELECT lm_hd_at_ticket_id FROM lm_help_desk_audit_trail"
				+ " WHERE lm_hd_at_incident_status = 'Reopened')"
				+ " AND (lhd.lm_hd_update_date  >= DATEADD(day,"
				+ i
				+ ", GETDATE())"
				+ " AND lhd.lm_hd_update_date  <=   DATEADD(day,"
				+ (i + 1)
				+ ", GETDATE()) )";
		return ticketRawDataNotReOpened;
	}

	private String getTicketRawDataNotReOpened(int i) {

		String ticketRawDataReOpened = "SELECT DISTINCT *, DATEDIFF(hour, lm_hd_opened, lm_hd_update_date) AS resolveTimeH"
				+ " FROM lm_help_desk_ticket_details lhd "
				+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id"
				+ " WHERE lhd.lm_hd_status IN (6, 11)"
				+ " AND lm_tc_number IN (SELECT lm_hd_at_ticket_id FROM lm_help_desk_audit_trail"
				+ " WHERE lm_hd_at_incident_status <> 'Reopened')"
				+ " AND (lhd.lm_hd_update_date  >= DATEADD(day,"
				+ i
				+ ", GETDATE())"
				+ " AND lhd.lm_hd_update_date  <=   DATEADD(day,"
				+ (i + 1)
				+ ", GETDATE()) )";
		return ticketRawDataReOpened;
	}

	public ActionForward getNewTicketData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "newTicketData";

		ArrayList<String[]> listData = new ArrayList<String[]>();
		DecimalFormat twoDForm = new DecimalFormat("#.##");

		for (int i = -7; i < 0; i++) {
			// current date time
			Calendar cal = Calendar.getInstance();
			Date dt = new Date();
			cal.setTime(dt);
			cal.add(Calendar.DATE, i);

			String strDay = getDayName("" + cal.get(Calendar.DAY_OF_WEEK));

			String newTicketsQuery = "SELECT count(*) as newTotal  FROM lm_help_desk_ticket_details WHERE lm_hd_status=1 AND lm_hd_opened >= DATEADD(day,"
					+ i
					+ ", GETDATE()) and  lm_hd_opened < DATEADD(day,"
					+ (i + 1) + ", GETDATE())";
			String resolveTicketsQuery = "SELECT count(*) AS resolvedTotal  FROM lm_help_desk_ticket_details WHERE lm_hd_status=6 AND lm_hd_opened BETWEEN DATEADD(day,"
					+ i
					+ ", GETDATE()) and  DATEADD(day,"
					+ (i + 1)
					+ ", GETDATE())";

			ResultSet rs1 = this.da.getResultSet(newTicketsQuery);
			ResultSet rs2 = this.da.getResultSet(resolveTicketsQuery);

			if (rs1.next() | rs2.next()) {
				String total1 = rs1.getString("newTotal");
				String total2 = rs2.getString("resolvedTotal");

				String strdata[] = { strDay, total1, total2 };
				listData.add(strdata);
			}
		}

		String newTicketsThisWeekQuery = "SELECT count(*) as newTotal   FROM lm_help_desk_ticket_details WHERE lm_hd_status!=6 " +
				"and 	lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) AND lm_hd_opened <= GetDate()";
		String resolveTicketsThisWeekQuery = "SELECT count(*) AS resolvedTotal  FROM lm_help_desk_ticket_details " +
				"WHERE lm_hd_status=6 and lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) AND lm_hd_opened <= GetDate()";
																																																									// lm_hd_status
		ResultSet rs1 = this.da.getResultSet(newTicketsThisWeekQuery);
		ResultSet rs2 = this.da.getResultSet(resolveTicketsThisWeekQuery);

		int newTicketsThisWeek = 0;
		int resolveTicketsThisWeek = 0;
		double percentageThisWeek = 0.0;
		if (rs1.next() | rs2.next()) {
			String total1 = rs1.getString("newTotal");
			newTicketsThisWeek = Integer.parseInt(total1);

			String total2 = rs2.getString("resolvedTotal");
			resolveTicketsThisWeek = Integer.parseInt(total2);
			if (newTicketsThisWeek > 0) {
				percentageThisWeek = ((resolveTicketsThisWeek * 1.0) / newTicketsThisWeek) * 100.0;
				percentageThisWeek = Double.valueOf(twoDForm
						.format(percentageThisWeek));
			}
		}

		String newTicketsLastWeekQuery = "SELECT count(*) AS newTotal  FROM lm_help_desk_ticket_details WHERE lm_hd_status!=6 " +
				"AND lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7) and lm_hd_opened <= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -1)";
		String resolveTicketsLastWeekQuery = "SELECT count(*) AS resolvedTotal  FROM lm_help_desk_ticket_details " +
				"WHERE lm_hd_status=6 AND lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7) and lm_hd_opened <= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -1)";
		ResultSet rs3 = this.da.getResultSet(newTicketsLastWeekQuery);
		ResultSet rs4 = this.da.getResultSet(resolveTicketsLastWeekQuery);

		int newTicketsLastWeek = 0;
		int resolveTicketsLastWeek = 0;
		double percentageLastWeek = 0.0;
		if (rs3.next() | rs4.next()) {
			String total1 = rs3.getString("newTotal");
			newTicketsLastWeek = Integer.parseInt(total1);

			String total2 = rs4.getString("resolvedTotal");
			resolveTicketsLastWeek = Integer.parseInt(total2);
			if (newTicketsLastWeek > 0) {
				percentageLastWeek = ((resolveTicketsLastWeek * 1.0) / newTicketsLastWeek) * 100.0;
				percentageLastWeek = Double.valueOf(twoDForm
						.format(percentageLastWeek));
			}
		}

		request.setAttribute("newTicketsThisWeek", newTicketsThisWeek);
		request.setAttribute("resolveTicketsThisWeek", resolveTicketsThisWeek);
		request.setAttribute("percentageThisWeek", percentageThisWeek);

		request.setAttribute("newTicketsLastWeek", newTicketsLastWeek);
		request.setAttribute("resolveTicketsLastWeek", resolveTicketsLastWeek);
		request.setAttribute("percentageLastWeek", percentageLastWeek);

		request.setAttribute("listdata", listData);

		rs1.close();
		rs2.close();
		rs3.close();
		rs4.close();

		return mapping.findForward(forward_key);
	}

	public String getDayName(String dayNumber) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("1", "Sun");
		map.put("2", "Mon");
		map.put("3", "Tue");
		map.put("4", "Wed");
		map.put("5", "Thu");
		map.put("6", "Fri");
		map.put("7", "Sat");

		return map.get(dayNumber);
	}

	public byte[] export(List<HDBean> hdBeanList) throws Exception {

		List<HDBean> hdBeanList1 = new ArrayList<HDBean>();
		hdBeanList1 = hdBeanList;

		File f = new File(System.getProperty("catalina.base") + "\\temp\\"
				+ System.currentTimeMillis() + "data.csv");
		if (!f.exists()) {
			f.createNewFile();
		}
		FileWriter writer = new FileWriter(f);

		writer.append("Client Name");
		writer.append(",");
		writer.append("Number of Ticket");
		writer.append(",");
		writer.append("Root Cause");
		writer.append(",");
		writer.append("Configuration items");
		writer.append(",");
		writer.append("Category");
		writer.append(",");
		writer.append("Urgency");
		writer.append('\n');
		writer.append('\n');

		for (int i = 0; i < hdBeanList1.size(); i++) {
			HDBean hdBean = hdBeanList1.get(i);
			try {

				writer.append("\"" + hdBean.getClientName() + "\"");
				writer.append(",");
				writer.append("\"" + hdBean.getTicketCount() + "\"");
				writer.append(",");
				writer.append("\"" + hdBean.getRootCause() + "\"");
				writer.append(",");
				writer.append("\"" + hdBean.getConfigItem() + "\"");
				writer.append(",");
				writer.append("\"" + hdBean.getCategory() + "\"");
				writer.append(",");
				writer.append("\"" + hdBean.getUrgency() + "\"");
				writer.append('\n');

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		writer.flush();
		writer.close();

		String csvfilepath = f.getAbsolutePath();

		File file = new File(csvfilepath);

		FileInputStream fileinputstream = new FileInputStream(file);

		byte[] buffer = new byte[fileinputstream.available()];

		if (fileinputstream.read(buffer) != -1) {
		}
		return buffer;

	}

	private static int minutesToSeconds(double minutes) {
		return (int) (minutes * 60);
	}

	// getting time in hours/minute/days
	public String calcHMS(int timeInSeconds) {
		int hours, minutes;
		hours = timeInSeconds / 3600;
		timeInSeconds = timeInSeconds - (hours * 3600);
		minutes = timeInSeconds / 60;
		int day = (int) TimeUnit.SECONDS.toDays(timeInSeconds);
		if ((day == 0) && (hours == 0)) {
			return (minutes + "M");
		} else if ((day == 0) && hours > 0) {
			return (hours + "H" + " " + minutes + "M");
		} else {
			return (day + "D" + hours + "H" + " " + minutes + "M");
		}
	}

	// by year data

	public ActionForward getTickeResolutiontDataByYear(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "ticketResolutionData";
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		ArrayList<String[]> listData = new ArrayList<String[]>();
		DecimalFormat twoDForm = new DecimalFormat("#.##");

		String days = "30";

		if (request.getParameter("days") != null
				&& !request.getParameter("days").equals("")) {
			days = request.getParameter("days").toString();
		} else if (request.getAttribute("days") != null
				&& !request.getAttribute("days").toString().equals("")) {
			days = request.getAttribute("days").toString();
		}

		request.setAttribute("days", days);

		for (int i = -7; i < 0; i++) {
			// current date time
			Calendar cal = Calendar.getInstance();
			Date dt = new Date();
			cal.setTime(dt);
			cal.add(Calendar.DATE, i);

			String strDay = getDayName("" + cal.get(Calendar.DAY_OF_WEEK));

			String firstTimeResQuery = "SELECT isnull(AVG((DATEDIFF(mi, lhd.lm_hd_opened, dat.lm_hd_at_update_date))/(60*1.0)),0.0) AS DATEDIFFERENCE "
					+ "FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
					+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
					+ "WHERE dat.lm_hd_at_incident_status  <> 'REOPENED' AND lhd.lm_hd_opened >= DATEADD(day,"
					+ i
					+ ", GETDATE()) and  lhd.lm_hd_opened < DATEADD(day,"
					+ (i + 1) + ", GETDATE())";
			String fullTimeResQuery = "SELECT isnull(AVG((DATEDIFF(mi, lhd.lm_hd_opened, dat.lm_hd_at_update_date))/(60*1.0)),0.0) AS DATEDIFFERENCE "
					+ "FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
					+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
					+ "WHERE dat.lm_hd_at_incident_status = 'REOPENED' AND lhd.lm_hd_opened >= DATEADD(day,"
					+ i
					+ ", GETDATE()) and  lhd.lm_hd_opened < DATEADD(day,"
					+ (i + 1) + ", GETDATE())";

			ResultSet rs1 = this.da.getResultSet(firstTimeResQuery);
			ResultSet rs2 = this.da.getResultSet(fullTimeResQuery);

			if (rs1.next() | rs2.next()) {
				String total1 = rs1.getString("DATEDIFFERENCE");
				String total2 = rs2.getString("DATEDIFFERENCE");

				String strdata[] = { strDay, total1, total2 };
				listData.add(strdata);
			}
		}

		String firstTimeResolutionThisWeekQuery = "SELECT isnull(AVG((DATEDIFF(mi, lhd.lm_hd_opened, dat.lm_hd_at_update_date))/(60*1.0)),0.0) AS DATEDIFFERENCE "
				+ "FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
				+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE dat.lm_hd_at_incident_status  <> 'REOPENED' AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) AND lhd.lm_hd_opened <= GetDate()";
		String fullTimeResolutionThisWeekQuery = "SELECT isnull(AVG((DATEDIFF(mi, lhd.lm_hd_opened, dat.lm_hd_at_update_date))/(60*1.0)),0.0) AS DATEDIFFERENCE "
				+ "FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
				+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE dat.lm_hd_at_incident_status = 'REOPENED' AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7) AND lhd.lm_hd_opened <= GetDate()";

		double firstTimeResolutionThisWeek = 0.0;
		double fullTimeResolutionThisWeek = 0.0;

		ResultSet rs1 = this.da.getResultSet(firstTimeResolutionThisWeekQuery);
		ResultSet rs2 = this.da.getResultSet(fullTimeResolutionThisWeekQuery);
		if (rs1.next() | rs2.next()) {
			String firstRTW = rs1.getString("DATEDIFFERENCE");
			firstTimeResolutionThisWeek = Double.parseDouble(firstRTW);
			firstTimeResolutionThisWeek = Double.valueOf(twoDForm
					.format(firstTimeResolutionThisWeek));
			String fullRTW = rs2.getString("DATEDIFFERENCE");
			fullTimeResolutionThisWeek = Double.parseDouble(fullRTW);
			fullTimeResolutionThisWeek = Double.valueOf(twoDForm
					.format(fullTimeResolutionThisWeek));

		}

		String firstTimeResolutionLastWeekQuery = "SELECT isnull(AVG((DATEDIFF(mi, lhd.lm_hd_opened, dat.lm_hd_at_update_date))/(60*1.0)),0.0) AS DATEDIFFERENCE "
				+ "FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
				+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE dat.lm_hd_at_incident_status  <> 'REOPENED' AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7) "
				+ "AND lhd.lm_hd_opened <= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -1)";
		String fullTimeResolutionLastWeekQuery = "SELECT isnull(AVG((DATEDIFF(mi, lhd.lm_hd_opened, dat.lm_hd_at_update_date))/(60*1.0)),0.0) AS DATEDIFFERENCE "
				+ "FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id  "
				+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE dat.lm_hd_at_incident_status = 'REOPENED' AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7) "
				+ "AND lhd.lm_hd_opened <= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -1)";

		double firstTimeResolutionLastWeek = 0.0;
		double fullTimeResolutionLastWeek = 0.0;

		ResultSet rs3 = this.da.getResultSet(firstTimeResolutionLastWeekQuery);
		ResultSet rs4 = this.da.getResultSet(fullTimeResolutionLastWeekQuery);
		if (rs3.next() | rs4.next()) {
			String firstRTW = rs3.getString("DATEDIFFERENCE");
			firstTimeResolutionLastWeek = Double.parseDouble(firstRTW);
			firstTimeResolutionLastWeek = Double.valueOf(twoDForm
					.format(firstTimeResolutionLastWeek));

			String fullRTW = rs4.getString("DATEDIFFERENCE");
			fullTimeResolutionLastWeek = Double.parseDouble(fullRTW);
			fullTimeResolutionLastWeek = Double.valueOf(twoDForm
					.format(fullTimeResolutionLastWeek));
		}

		// calculating percentage of first call completion.
		String allCompletionQuery = "SELECT count(*) as totaltickets "
				+ " FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
				+ " INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id"
				+ " WHERE lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND lhd.lm_hd_opened <= GetDate()";

		String firstCallcompletionQuery = "SELECT count(*) as total "
				+ " FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
				+ " INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id"
				+ " WHERE dat.lm_hd_at_incident_status  <> 'REOPENED'	AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ")	AND lhd.lm_hd_opened <= GetDate()";
		int firstCallcompletion = 0;
		int allCompletion = 0;

		double firstCallCompletionPercentage = 0.0;

		ResultSet rs5 = this.da.getResultSet(firstCallcompletionQuery);
		ResultSet rs5a = this.da.getResultSet(allCompletionQuery);
		if (rs5.next() | rs5a.next()) {
			String fcCompletion = rs5.getString("total");
			String aCompletion = rs5a.getString("totaltickets");
			firstCallcompletion = Integer.parseInt(fcCompletion);
			allCompletion = Integer.parseInt(aCompletion);

			if (allCompletion > 0) {
				firstCallCompletionPercentage = (firstCallcompletion / (allCompletion * 1.0)) * 100.0;
				firstCallCompletionPercentage = Double.valueOf(twoDForm
						.format(firstCallCompletionPercentage));
			}
		}

		// Calculating percentage of first call completion tickets based on pps
		String firstCallcompletionPpsQuery = "SELECT count(*) as totalTickets FROM lm_help_desk_ticket_details lhd  "
				+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
				+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE dat.lm_hd_at_incident_status  not like  'REOPENED' AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'08:00') "
				+ "and lhd.lm_hd_opened < DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'20:00') and DATEPART(dw, lhd.lm_hd_opened) NOT IN (1, 7)";
		String totalTicketsByPpsQuery = "SELECT count(*) as totalTickets FROM lm_help_desk_ticket_details lhd  "
				+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
				+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id WHERE lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'08:00') "
				+ "and lhd.lm_hd_opened < DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'20:00') and DATEPART(dw, lhd.lm_hd_opened) NOT IN (1, 7)";

		int firstCallcompletionPps = 0;
		int totalTicketsByPp = 0;

		ResultSet rs6 = this.da.getResultSet(firstCallcompletionPpsQuery);
		ResultSet rs6a = this.da.getResultSet(totalTicketsByPpsQuery);

		double firstCallCompletionPpsPercentage = 0.0;

		if (rs6.next() | rs6a.next()) {
			String fcCompletion = rs6.getString("totalTickets");
			String aCompletion = rs6a.getString("totalTickets");

			firstCallcompletionPps = Integer.parseInt(fcCompletion);
			totalTicketsByPp = Integer.parseInt(aCompletion);
			System.out.println("Tickets total pps:" + totalTicketsByPp);
			if (totalTicketsByPp > 0) {
				firstCallCompletionPpsPercentage = (firstCallcompletionPps / (totalTicketsByPp * 1.0)) * 100.0;
				firstCallCompletionPpsPercentage = Double.valueOf(twoDForm
						.format(firstCallCompletionPpsPercentage));
			}
		}

		// Calculating percentage of first call completion based on non-pps
		String firstCallcompletionNonPpsQuery = "SELECT count(*) as totalTickets FROM lm_help_desk_ticket_details lhd  "
				+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
				+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE dat.lm_hd_at_incident_status  not like  'REOPENED' AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and (lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'20:00')  "
				+ "and lhd.lm_hd_opened <= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'23:59') ) "
				+ "or (lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'23:59')  "
				+ "and lhd.lm_hd_opened <= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'08:00') ) and DATEPART(dw, lhd.lm_hd_opened) NOT IN (1, 7)";
		String totalTicketsByNonPpsQuery = "SELECT count(*) as totalTickets  "
				+ "FROM lm_help_desk_ticket_details lhd  INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
				+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id WHERE lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and (lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'20:00')  "
				+ "and lhd.lm_hd_opened <= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'23:59') ) "
				+ "or (lhd.lm_hd_opened >= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'23:59')  "
				+ "and lhd.lm_hd_opened <= DATEADD(dd,DATEDIFF(dd,0,lhd.lm_hd_opened),'08:00') ) and DATEPART(dw, lhd.lm_hd_opened) NOT IN (1, 7)";

		ResultSet rs7 = this.da.getResultSet(firstCallcompletionNonPpsQuery);
		ResultSet rs7a = this.da.getResultSet(totalTicketsByNonPpsQuery);

		int firstCallcompletionNonPps = 0;
		int totalTicketsByNonPps = 0;

		double firstCallCompletionNonPpsPercentage = 0.0;

		if (rs7.next() | rs7a.next()) {
			String fcCompletion = rs7.getString("totalTickets");
			String aCompletion = rs7a.getString("totalTickets");

			firstCallcompletionNonPps = Integer.parseInt(fcCompletion);
			totalTicketsByNonPps = Integer.parseInt(aCompletion);
			System.out.println("Tickets total Non-pps:" + totalTicketsByNonPps);
			if (totalTicketsByNonPps > 0) {
				firstCallCompletionNonPpsPercentage = (firstCallcompletionNonPps / (totalTicketsByNonPps * 1.0)) * 100.0;
				firstCallCompletionNonPpsPercentage = Double.valueOf(twoDForm
						.format(firstCallCompletionNonPpsPercentage));
			}
		}

		// Calculating percentage of first call completion based on weekends
		String firstCallweekendQuery = "SELECT count(*) as totalTickets FROM lm_help_desk_ticket_details lhd  "
				+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
				+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE dat.lm_hd_at_incident_status  not like  'REOPENED' AND lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and DATEPART(dw, lhd.lm_hd_opened)  IN (1, 7)";
		String totalTicketsInWeekendQuery = "SELECT count(*) as totalTickets  FROM lm_help_desk_ticket_details lhd  "
				+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
				+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id WHERE lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days
				+ ") AND lhd.lm_hd_opened <= GetDate() and DATEPART(dw, lhd.lm_hd_opened)  IN (1, 7)";

		ResultSet rs8 = this.da.getResultSet(firstCallweekendQuery);
		ResultSet rs8a = this.da.getResultSet(totalTicketsInWeekendQuery);

		int firstCallweekend = 0;
		int totalTicketsInWeekend = 0;

		double firstCallWeekendPercentage = 0.0;

		if (rs8.next() | rs8a.next()) {
			String fcCompletion = rs8.getString("totalTickets");
			String aCompletion = rs8a.getString("totalTickets");

			firstCallweekend = Integer.parseInt(fcCompletion);
			totalTicketsInWeekend = Integer.parseInt(aCompletion);
			System.out.println("Tickets total in weekends:"
					+ totalTicketsInWeekend);
			if (totalTicketsInWeekend > 0) {
				firstCallWeekendPercentage = (firstCallweekend / (totalTicketsInWeekend * 1.0)) * 100.0;
				firstCallWeekendPercentage = Double.valueOf(twoDForm
						.format(firstCallWeekendPercentage));
			}
		}

		// all tickets for testing
		String allTickets = "select count(*) as totalTickets  FROM lm_help_desk_ticket_details lhd  "
				+ "INNER JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
				+ "INNER JOIN lm_help_desk_audit_trail dat  on lt.lm_tc_number = dat.lm_hd_at_ticket_id "
				+ "WHERE lhd.lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -"
				+ days + ") AND lhd.lm_hd_opened <= GetDate() ";
		ResultSet rs9 = this.da.getResultSet(allTickets);

		if (rs9.next()) {
			String fcCompletion = rs9.getString("totalTickets");
			System.out.println("Total tickets=" + fcCompletion);
		}

		request.setAttribute("firstTimeResolutionThisWeek",
				firstTimeResolutionThisWeek);
		request.setAttribute("fullTimeResolutionThisWeek",
				fullTimeResolutionThisWeek);
		request.setAttribute("firstTimeResolutionLastWeek",
				firstTimeResolutionLastWeek);
		request.setAttribute("fullTimeResolutionLastWeek",
				fullTimeResolutionLastWeek);

		request.setAttribute("firstCallcompletionPercentage",
				firstCallCompletionPercentage);
		request.setAttribute("firstCallcompletionPps",
				firstCallCompletionPpsPercentage);
		request.setAttribute("firstCallcompletionNonPps",
				firstCallCompletionNonPpsPercentage);
		request.setAttribute("firstCallWeekendPercentage",
				firstCallWeekendPercentage);

		request.setAttribute("listdata", listData);

		rs1.close();
		rs2.close();
		rs3.close();
		rs4.close();

		return mapping.findForward(forward_key);
	}

	public boolean isleapyear(String year) {

		int ischeck = Integer.valueOf(year);
		if (ischeck > 1582) {

			if (ischeck % 4 != 0) {
				return false;
			}
		} else {

			return true;

		}
		return false;

	}

	public void getThisWeekFirstTimeResRawData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String action = request.getParameter("action");
		String query = "";

		if (action.equals("thisWeekFirstRes")) {
			query = "SELECT DISTINCT lm_tc_number,   convert(VARCHAR,lm_hd_opened,109) AS lm_hd_opened,  "
					+ "convert(VARCHAR,lm_hd_update_date,109) AS lm_hd_update_date, lhs.lm_st_status, "
					+ "lpoc.lo_pc_first_name +' '+lpoc.lo_pc_last_name AS full_name,  DATEDIFF(hour, lm_hd_opened, lm_hd_update_date) AS resolveTimeH"
					+ " FROM lm_help_desk_ticket_details lhd  "
					+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "

					+ " INNER JOIN lm_help_desk_ticket_status lhs ON lhs.lm_st_id = lhd.lm_hd_status"
					+ " LEFT JOIN lo_poc lpoc ON lhd.lm_hd_updated_by = lpoc.lo_pc_id "
					+ " WHERE lhd.lm_hd_status IN (6, 11) "
					+ " AND lm_tc_number IN "
					+ " (SELECT lm_hd_at_ticket_id FROM lm_help_desk_audit_trail"
					+ " WHERE lm_hd_at_incident_status <> 'Reopened') "
					+ " AND (lhd.lm_hd_update_date  >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) "
					+ " AND lhd.lm_hd_update_date  <=   GETDATE() )";
		} else if (action.equals("thisWeekFullRes")) {
			query = "SELECT DISTINCT lm_tc_number,   convert(VARCHAR,lm_hd_opened,109) AS lm_hd_opened,  "
					+ "convert(VARCHAR,lm_hd_update_date,109) AS lm_hd_update_date, lhs.lm_st_status, "
					+ "lpoc.lo_pc_first_name +' '+lpoc.lo_pc_last_name AS full_name,  DATEDIFF(hour, lm_hd_opened, lm_hd_update_date) AS resolveTimeH"
					+ " FROM lm_help_desk_ticket_details lhd  "
					+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "

					+ " INNER JOIN lm_help_desk_ticket_status lhs ON lhs.lm_st_id = lhd.lm_hd_status"
					+ " LEFT JOIN lo_poc lpoc ON lhd.lm_hd_updated_by = lpoc.lo_pc_id "
					+ " WHERE lhd.lm_hd_status IN (6, 11) "
					+ " AND lm_tc_number IN "
					+ " (SELECT lm_hd_at_ticket_id FROM lm_help_desk_audit_trail"
					+ " WHERE lm_hd_at_incident_status = 'Reopened') "
					+ " AND (lhd.lm_hd_update_date  >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) "
					+ " AND lhd.lm_hd_update_date  <=   GETDATE() )";

		} else if (action.equals("lastWeekFirstRes")) {
			query = "SELECT DISTINCT lm_tc_number,   convert(VARCHAR,lm_hd_opened,109) AS lm_hd_opened,  "
					+ "convert(VARCHAR,lm_hd_update_date,109) AS lm_hd_update_date, lhs.lm_st_status, "
					+ "lpoc.lo_pc_first_name +' '+lpoc.lo_pc_last_name AS full_name,  DATEDIFF(hour, lm_hd_opened, lm_hd_update_date) AS resolveTimeH"
					+ " FROM lm_help_desk_ticket_details lhd  "
					+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "

					+ " INNER JOIN lm_help_desk_ticket_status lhs ON lhs.lm_st_id = lhd.lm_hd_status"
					+ " LEFT JOIN lo_poc lpoc ON lhd.lm_hd_updated_by = lpoc.lo_pc_id "
					+ " WHERE lhd.lm_hd_status IN (6, 11) "
					+ " AND lm_tc_number IN "
					+ " (SELECT lm_hd_at_ticket_id FROM lm_help_desk_audit_trail"
					+ " WHERE lm_hd_at_incident_status <> 'Reopened') "
					+ " AND (lhd.lm_hd_update_date  >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7) "
					+ " AND lhd.lm_hd_update_date  <=   GETDATE() )";

		} else if (action.equals("lastWeekFullRes")) {
			query = "SELECT DISTINCT lm_tc_number,  convert(VARCHAR,lm_hd_opened,109) AS lm_hd_opened,  "
					+ "convert(VARCHAR,lm_hd_update_date,109) AS lm_hd_update_date, lhs.lm_st_status, "
					+ "lpoc.lo_pc_first_name +' '+lpoc.lo_pc_last_name AS full_name,  DATEDIFF(hour, lm_hd_opened, lm_hd_update_date) AS resolveTimeH"
					+ " FROM lm_help_desk_ticket_details lhd  "
					+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "

					+ " INNER JOIN lm_help_desk_ticket_status lhs ON lhs.lm_st_id = lhd.lm_hd_status"
					+ " LEFT JOIN lo_poc lpoc ON lhd.lm_hd_updated_by = lpoc.lo_pc_id "
					+ " WHERE lhd.lm_hd_status IN (6, 11) "
					+ " AND lm_tc_number IN "
					+ " (SELECT lm_hd_at_ticket_id FROM lm_help_desk_audit_trail"
					+ " WHERE lm_hd_at_incident_status = 'Reopened') "
					+ " AND (lhd.lm_hd_update_date  >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7) "
					+ " AND lhd.lm_hd_update_date  <=   GETDATE() )";

		}

		File csvfile = new File(System.getProperty("catalina.base")
				+ "\\temp\\" + System.currentTimeMillis() + "data.csv");
		try {
			if (!csvfile.exists()) {
				csvfile.createNewFile();
			}
			FileWriter writer = new FileWriter(csvfile);

			writer.append("Ticket Number");
			writer.append(",");
			writer.append("Open Date");
			writer.append(",");
			writer.append("Resolve/Close Date");
			writer.append(",");
			writer.append("Status");
			writer.append(",");
			writer.append("Full Name");
			writer.append(",");
			writer.append("Resolve Time(Hours)");
			writer.append('\n');
			writer.append('\n');

			ResultSet rs1 = this.da.getResultSet(query);

			while (rs1.next()) {

				writer.append("\"" + rs1.getInt("lm_tc_number") + "\"");
				writer.append(",");
				writer.append("\"" + rs1.getString("lm_hd_opened") + "\"");
				writer.append(",");
				writer.append("\"" + rs1.getString("lm_hd_update_date") + "\"");
				writer.append(",");
				writer.append("\"" + rs1.getString("lm_st_status") + "\"");
				writer.append(",");
				writer.append("\"" + rs1.getString("full_name") + "\"");
				writer.append(",");
				writer.append("\"" + rs1.getString("resolveTimeH") + "\"");
				writer.append('\n');

			}

			writer.flush();
			writer.close();
		} catch (Exception ee) {
			ee.printStackTrace();
		}

		File file = new File(csvfile.getAbsolutePath());
		try {
			FileInputStream fileinputstream = new FileInputStream(file);

			byte[] buffer = new byte[fileinputstream.available()];
			fileinputstream.read(buffer);
			response.setContentType("application/x-msdownload");
			response.setHeader("Content-Disposition",
					"attachment;filename=NocTicketExport.csv;size="
							+ buffer.length + "");
			ServletOutputStream outStream = response.getOutputStream();
			outStream.write(buffer);
			outStream.close();

		} catch (Exception ee) {
			ee.printStackTrace();
		}

	}

	// get New Ticket data
	public void getRawData(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String action = request.getParameter("action");
		String query = "";

		if (action.equals("thisWeekNewTicket")) {
			query = "SELECT lm_tc_number,  convert(VARCHAR,lm_hd_opened,109) AS lm_hd_opened,   "
					+ "convert(VARCHAR,lm_hd_update_date,109) AS lm_hd_update_date, lhs.lm_st_status "
					+ " FROM lm_help_desk_ticket_details lhd "
					+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id"
					+ " left JOIN lm_help_desk_ticket_status lhs ON lhs.lm_st_id = lhd.lm_hd_status "
					+ " WHERE lm_hd_status = 1  and 	lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) AND lm_hd_opened <= GetDate()";

		} else if (action.equals("thisWeekTicketRes")) {
			query = " SELECT  lm_tc_number,   convert(VARCHAR,lm_hd_opened,109) AS lm_hd_opened,   "
					+ "convert(VARCHAR,lm_hd_update_date,109) AS lm_hd_update_date, lhs.lm_st_status "
					+ " FROM lm_help_desk_ticket_details lhd  "
					+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
					+ " left JOIN lm_help_desk_ticket_status lhs ON lhs.lm_st_id = lhd.lm_hd_status "
					+ " WHERE lhd.lm_hd_status = 11 and "
					+ " lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0) AND lm_hd_opened <= GetDate()";

		} else if (action.equals("lastWeekNewTicket")) {
			query = " SELECT  lm_tc_number,   convert(VARCHAR,lm_hd_opened,109) AS lm_hd_opened,   "
					+ "convert(VARCHAR,lm_hd_update_date,109) AS lm_hd_update_date, lhs.lm_st_status "
					+ " FROM lm_help_desk_ticket_details lhd "
					+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
					+ " LEFT JOIN lm_help_desk_ticket_status lhs ON lhs.lm_st_id = lhd.lm_hd_status "
					+ " WHERE lm_hd_status = 1 "
					+ " AND lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7) and lm_hd_opened <= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -1)";

		} else if (action.equals("lastWeekTicketRes")) {
			query = "SELECT  lm_tc_number,   convert(VARCHAR,lm_hd_opened,109) AS lm_hd_opened,   "
					+ "convert(VARCHAR,lm_hd_update_date,109) AS lm_hd_update_date, lhs.lm_st_status "
					+ " FROM lm_help_desk_ticket_details lhd "
					+ " LEFT JOIN lm_ticket lt    on lhd.lm_hd_tc_id = lt.lm_tc_id "
					+ " LEFT JOIN lm_help_desk_ticket_status lhs ON lhs.lm_st_id = lhd.lm_hd_status"
					+ " WHERE lm_hd_status = 11 "
					+ " AND lm_hd_opened >= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -7) and lm_hd_opened <= DATEADD(wk, DATEDIFF(wk,0,GETDATE()), -1)";

		}

		File csvfile = new File(System.getProperty("catalina.base")
				+ "\\temp\\" + System.currentTimeMillis() + "data.csv");
		try {
			if (!csvfile.exists()) {
				csvfile.createNewFile();
			}
			FileWriter writer = new FileWriter(csvfile);

			writer.append("Ticket Number");
			writer.append(",");
			writer.append("Open Date");
			writer.append(",");
			writer.append("Resolve/Close Date");
			writer.append(",");
			writer.append("Status");
			writer.append(",");
			writer.append('\n');

			ResultSet rs1 = this.da.getResultSet(query);

			int i = 0;

			while (rs1.next()) {
				i++;
				if (i == 2660) {

					System.out.println(rs1.getString("lm_hd_opened").toString()
							+ rs1.getString("lm_hd_update_date").toString());
				}
				writer.append("\"" + rs1.getInt("lm_tc_number") + "\"");
				writer.append(",");

				String date = rs1.getString("lm_hd_opened");

				writer.append("\"" + date + "\"");
				writer.append(",");
				String dateupdate = rs1.getString("lm_hd_update_date");
				writer.append("\"" + dateupdate + "\"");
				writer.append(",");
				writer.append("\"" + rs1.getString("lm_st_status") + "\"");
				writer.append(",");
				writer.append('\n');

			}

			writer.flush();
			writer.close();
		} catch (Exception ee) {
			ee.printStackTrace();
		}

		File file = new File(csvfile.getAbsolutePath());
		try {
			FileInputStream fileinputstream = new FileInputStream(file);

			byte[] buffer = new byte[fileinputstream.available()];
			fileinputstream.read(buffer);
			response.setContentType("application/x-msdownload");
			response.setHeader("Content-Disposition",
					"attachment;filename=NocTicketExport.csv;size="
							+ buffer.length + "");
			ServletOutputStream outStream = response.getOutputStream();
			outStream.write(buffer);
			outStream.close();

		} catch (Exception ee) {
			ee.printStackTrace();
		}

	}

	public ActionForward getDownTimeData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String forward_key = "DownTimeResp";

		String URL = "https://ccc.contingent.com/index.php?cmp=phone_ws&type=downtime_per_isp";
		String responsedata = getDataFromWeb(URL, 9999, "\n");
		List<String[]> downTimerList = new ArrayList<String[]>();
		try {
			if (responsedata != null) {

				String[] data = null;
				JSONObject object = new JSONObject(responsedata);
				DecimalFormat df = new DecimalFormat("##,##0.00");
				Object[] keys = object.keySet().toArray();

				Arrays.sort(keys);

				for (Object key : keys) {
					Object info = object.getJSONArray(key.toString());
					data = info.toString().replaceAll("[\\[\\]]", "")
							.split(",");

					int totalDownTimeSeconds = Integer.parseInt(data[0]);
					int totalDevices = Integer.parseInt(data[1]);
					float totalTimeInMinutes = totalDownTimeSeconds / 60;
					float downTimeByDevice = totalTimeInMinutes / totalDevices;

					data = new String[5];

					data[0] = key.toString(); // ISP
					data[1] = df.format(totalTimeInMinutes) + " M"; // Total
																	// Down
																	// Time.
					data[2] = String.valueOf(totalDevices); // Total Devicies
					data[3] = String.valueOf(df.format(downTimeByDevice))
							+ " M"; // Down Time per Device
					data[4] = String.valueOf(df.format(downTimeByDevice / 7))
							+ " M"; // Average Down Time per Device perDay

					downTimerList.add(data);

				}

			}
			request.setAttribute("DownTimeDataList", downTimerList);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward(forward_key);
	}

	public static String getDataFromWeb(String url, int timeout,
			String lineBreak) {
		try {
			URL u = new URL(url);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(timeout);
			c.setReadTimeout(timeout);
			c.connect();
			int status = c.getResponseCode();
			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(
						c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + lineBreak);
				}
				br.close();
				return sb.toString();
			}
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
