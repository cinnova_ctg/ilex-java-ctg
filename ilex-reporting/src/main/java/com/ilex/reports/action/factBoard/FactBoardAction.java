package com.ilex.reports.action.factBoard;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ilex.reports.action.laborCost.LaborCostAction;
import com.ilex.reports.bean.factBoard.FactBoardBean;
import com.ilex.reports.formbean.factBoard.FactBoardForm;
import com.ilex.reports.logic.factBoard.FactBoardOrganiser;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.ExceptionHelper;
import com.mind.fw.lang.SysException;

/**
 * The Class FactBoardAction.
 */
public class FactBoardAction extends DispatchAction {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = Logger.getLogger(LaborCostAction.class);

	/**
	 * Method for Fact board Display.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	public ActionForward factBoard(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		ActionErrors errors = new ActionErrors();

		FactBoardForm factBoardForm = (FactBoardForm) form;
		FactBoardBean factBoardBean = new FactBoardBean();
		HttpSession session = request.getSession(true);
		String loginuserid = (String) session.getAttribute("userid");

		if (loginuserid == null) {
			return (mapping.findForward("SessionExpire")); // Check for session
															// expired
		}

		FactBoardOrganiser organiser = new FactBoardOrganiser();
		
		try {
			organiser.setFactBoardValues(factBoardBean);
			organiser.setFormFromBean(factBoardForm, factBoardBean);
		} catch (SysException se) {
			LOGGER.error(ExceptionHelper.getStackTrace(se));
			ActionError error = new ActionError("reports.sysException",
					se.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (AppException ae) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("AppException: " + ae.getMessage());
			}

			ActionError error = new ActionError("reports.appException",
					ae.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, error);
			saveErrors(request, errors);
			return mapping.findForward("errorpage");
		} catch (IllegalAccessException e) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("IllegalAccessException: " + e.getMessage());
			}
		} catch (InvocationTargetException ite) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("InvocationTargetException: " + ite.getMessage());
			}
		}
		return mapping.findForward("success");
	}

}
