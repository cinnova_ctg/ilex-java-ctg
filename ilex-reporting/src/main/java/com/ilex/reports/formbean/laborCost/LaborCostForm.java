package com.ilex.reports.formbean.laborCost;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;

import com.ilex.reports.common.LabelValue;

public class LaborCostForm extends ActionForm implements Serializable {

	private static final long serialVersionUID = 5009847428137423849L;
	private String xCordinateDate = null;
	private String yCordinateLabor = null;
	private String yCordinateTechnician = null;
	private String yCordinateEngineer = null;
	private String graphXML = null;
	private String laborCategoryFilter = null;
	private ArrayList<LabelValue> laborCategoryList = null;
	private String reportSubTitle = null;
	private String laborType = null;
	private ArrayList<LabelValue> laborTypeList = null;

	public String getLaborType() {
		return laborType;
	}

	public void setLaborType(String laborType) {
		this.laborType = laborType;
	}

	public ArrayList<LabelValue> getLaborTypeList() {
		return laborTypeList;
	}

	public void setLaborTypeList(ArrayList<LabelValue> laborTypeList) {
		this.laborTypeList = laborTypeList;
	}

	public String getxCordinateDate() {
		return xCordinateDate;
	}

	public void setxCordinateDate(String xCordinateDate) {
		this.xCordinateDate = xCordinateDate;
	}

	public String getyCordinateLabor() {
		return yCordinateLabor;
	}

	public void setyCordinateLabor(String yCordinateLabor) {
		this.yCordinateLabor = yCordinateLabor;
	}

	public String getyCordinateTechnician() {
		return yCordinateTechnician;
	}

	public void setyCordinateTechnician(String yCordinateTechnician) {
		this.yCordinateTechnician = yCordinateTechnician;
	}

	public String getyCordinateEngineer() {
		return yCordinateEngineer;
	}

	public void setyCordinateEngineer(String yCordinateEngineer) {
		this.yCordinateEngineer = yCordinateEngineer;
	}

	public String getGraphXML() {
		return graphXML;
	}

	public void setGraphXML(String graphXML) {
		this.graphXML = graphXML;
	}

	public String getLaborCategoryFilter() {
		return laborCategoryFilter;
	}

	public void setLaborCategoryFilter(String laborCategoryFilter) {
		this.laborCategoryFilter = laborCategoryFilter;
	}

	public ArrayList<LabelValue> getLaborCategoryList() {
		return laborCategoryList;
	}

	public void setLaborCategoryList(ArrayList<LabelValue> laborCategoryList) {
		this.laborCategoryList = laborCategoryList;
	}

	public String getReportSubTitle() {
		return reportSubTitle;
	}

	public void setReportSubTitle(String reportSubTitle) {
		this.reportSubTitle = reportSubTitle;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
