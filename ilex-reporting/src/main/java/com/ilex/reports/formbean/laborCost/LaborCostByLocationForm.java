package com.ilex.reports.formbean.laborCost;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;

import com.ilex.reports.bean.laborCost.LaborCostByLocationBean;
import com.ilex.reports.common.LabelValue;

public class LaborCostByLocationForm extends ActionForm {

	private static final long serialVersionUID = -9147228575639364355L;

	private String viewFilter = null;
	private ArrayList<LabelValue> viewFilterList = null;
	private ArrayList<LaborCostByLocationBean> lcByLocationList = null;
	private String reportTitle = null;
	private String startMonth;
	private String startYear;
	private String endYear;
	private String endMonth;
	private ArrayList<LabelValue> monthList;

	private String orderBy = null;

	public String getViewFilter() {
		return viewFilter;
	}

	public void setViewFilter(String viewFilter) {
		this.viewFilter = viewFilter;
	}

	public ArrayList<LabelValue> getViewFilterList() {
		return viewFilterList;
	}

	public void setViewFilterList(ArrayList<LabelValue> viewFilterList) {
		this.viewFilterList = viewFilterList;
	}

	public ArrayList<LaborCostByLocationBean> getLcByLocationList() {
		return lcByLocationList;
	}

	public void setLcByLocationList(
			ArrayList<LaborCostByLocationBean> lcByLocationList) {
		this.lcByLocationList = lcByLocationList;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public String getStartYear() {
		return startYear;
	}

	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	public String getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}

	public String getEndYear() {
		return endYear;
	}

	public void setEndYear(String endYear) {
		this.endYear = endYear;
	}

	public ArrayList<LabelValue> getMonthList() {
		return monthList;
	}

	public void setMonthList(ArrayList<LabelValue> monthList) {
		this.monthList = monthList;
	}
}
