package com.ilex.reports.formbean.factBoard;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;

public class FactBoardForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ytdProForma = null;
	private String ytdExpense = null;
	private String ytdRevenue = null;
	private String ytdVGPM = null;
	private String ytdVGPMDelta = null;
	private String cmProForma = null;
	private String cmExpense = null;

	private String pytdProFormaExpense = null;
	private String pytdExpense = null;
	private String pytdRevenue = null;
	private String pytdVGPM = null;
	private String pytdVGPMDelta = null;

	public String getYtdProForma() {
		return ytdProForma;
	}

	public void setYtdProForma(String ytdProForma) {
		this.ytdProForma = ytdProForma;
	}

	public String getYtdExpense() {
		return ytdExpense;
	}

	public void setYtdExpense(String ytdExpense) {
		this.ytdExpense = ytdExpense;
	}

	public String getYtdRevenue() {
		return ytdRevenue;
	}

	public void setYtdRevenue(String ytdRevenue) {
		this.ytdRevenue = ytdRevenue;
	}

	public String getYtdVGPM() {
		return ytdVGPM;
	}

	public void setYtdVGPM(String ytdVGPM) {
		this.ytdVGPM = ytdVGPM;
	}

	public String getYtdVGPMDelta() {
		return ytdVGPMDelta;
	}

	public void setYtdVGPMDelta(String ytdVGPMDelta) {
		this.ytdVGPMDelta = ytdVGPMDelta;
	}

	public String getCmProForma() {
		return cmProForma;
	}

	public void setCmProForma(String cmProForma) {
		this.cmProForma = cmProForma;
	}

	public String getCmExpense() {
		return cmExpense;
	}

	public void setCmExpense(String cmExpense) {
		this.cmExpense = cmExpense;
	}

	public String getCmRevenue() {
		return cmRevenue;
	}

	public void setCmRevenue(String cmRevenue) {
		this.cmRevenue = cmRevenue;
	}

	public String getCmVGPM() {
		return cmVGPM;
	}

	public void setCmVGPM(String cmVGPM) {
		this.cmVGPM = cmVGPM;
	}

	public String getCmVGPMDelta() {
		return cmVGPMDelta;
	}

	public void setCmVGPMDelta(String cmVGPMDelta) {
		this.cmVGPMDelta = cmVGPMDelta;
	}

	private String cmRevenue = null;
	private String cmVGPM = null;
	private String cmVGPMDelta = null;

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

	public String getPytdProFormaExpense() {
		return pytdProFormaExpense;
	}

	public void setPytdProFormaExpense(String pytdProFormaExpense) {
		this.pytdProFormaExpense = pytdProFormaExpense;
	}

	public String getPytdExpense() {
		return pytdExpense;
	}

	public void setPytdExpense(String pytdExpense) {
		this.pytdExpense = pytdExpense;
	}

	public String getPytdRevenue() {
		return pytdRevenue;
	}

	public void setPytdRevenue(String pytdRevenue) {
		this.pytdRevenue = pytdRevenue;
	}

	public String getPytdVGPM() {
		return pytdVGPM;
	}

	public void setPytdVGPM(String pytdVGPM) {
		this.pytdVGPM = pytdVGPM;
	}

	public String getPytdVGPMDelta() {
		return pytdVGPMDelta;
	}

	public void setPytdVGPMDelta(String pytdVGPMDelta) {
		this.pytdVGPMDelta = pytdVGPMDelta;
	}

}
