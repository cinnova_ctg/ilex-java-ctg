package com.ilex.reports.formbean.minutemanSpeedpayReport;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;

import com.ilex.reports.common.LabelValue;

public class MinutemanSpeedpayForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String xCordinateDate = null;
	private String graphXML = null;
	private String projectCategoryFilter = null;
	private ArrayList<LabelValue> projectCategoryList = null;
	private String reportSubTitle = null;
	private String projectType = null;
	private ArrayList<LabelValue> projectTypeList = null;

	public String getxCordinateDate() {
		return xCordinateDate;
	}

	public void setxCordinateDate(String xCordinateDate) {
		this.xCordinateDate = xCordinateDate;
	}

	public String getGraphXML() {
		return graphXML;
	}

	public void setGraphXML(String graphXML) {
		this.graphXML = graphXML;
	}

	public String getProjectCategoryFilter() {
		return projectCategoryFilter;
	}

	public void setProjectCategoryFilter(String projectCategoryFilter) {
		this.projectCategoryFilter = projectCategoryFilter;
	}

	public ArrayList<LabelValue> getProjectCategoryList() {
		return projectCategoryList;
	}

	public void setProjectCategoryList(ArrayList<LabelValue> projectCategoryList) {
		this.projectCategoryList = projectCategoryList;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public ArrayList<LabelValue> getProjectTypeList() {
		return projectTypeList;
	}

	public void setProjectTypeList(ArrayList<LabelValue> projectTypeList) {
		this.projectTypeList = projectTypeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getReportSubTitle() {
		return reportSubTitle;
	}

	public void setReportSubTitle(String reportSubTitle) {
		this.reportSubTitle = reportSubTitle;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
