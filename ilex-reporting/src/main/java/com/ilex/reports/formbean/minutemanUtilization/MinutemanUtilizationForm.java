package com.ilex.reports.formbean.minutemanUtilization;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;

import com.ilex.reports.logic.minutemanUtilization.MinutemanUtilizationResultSetBean;

public class MinutemanUtilizationForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<MinutemanUtilizationResultSetBean> firstUsageList = null;
	private String selectedFilter;
	private String totalMM;
	private String usedMM;
	private String unUsedMM;
	private String xCordinateDate = null;
	private String graphXML = null;
	private String year = null;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getxCordinateDate() {
		return xCordinateDate;
	}

	public void setxCordinateDate(String xCordinateDate) {
		this.xCordinateDate = xCordinateDate;
	}

	public String getGraphXML() {
		return graphXML;
	}

	public void setGraphXML(String graphXML) {
		this.graphXML = graphXML;
	}

	public String getReportSubTitle() {
		return reportSubTitle;
	}

	public void setReportSubTitle(String reportSubTitle) {
		this.reportSubTitle = reportSubTitle;
	}

	private String reportSubTitle = null;

	public String getSelectedFilter() {
		return selectedFilter;
	}

	public String getTotalMM() {
		return totalMM;
	}

	public void setTotalMM(String totalMM) {
		this.totalMM = totalMM;
	}

	public String getUsedMM() {
		return usedMM;
	}

	public void setUsedMM(String usedMM) {
		this.usedMM = usedMM;
	}

	public String getUnUsedMM() {
		return unUsedMM;
	}

	public void setUnUsedMM(String unUsedMM) {
		this.unUsedMM = unUsedMM;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setSelectedFilter(String selectedFilter) {
		this.selectedFilter = selectedFilter;
	}

	public List<MinutemanUtilizationResultSetBean> getFirstUsageList() {
		return firstUsageList;
	}

	public void setFirstUsageList(
			List<MinutemanUtilizationResultSetBean> firstUsageList) {
		this.firstUsageList = firstUsageList;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
