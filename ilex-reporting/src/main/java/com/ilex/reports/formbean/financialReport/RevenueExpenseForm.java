package com.ilex.reports.formbean.financialReport;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;

import com.ilex.reports.logic.financialReport.BussinessDevlopmentManagerResultSetBean;
import com.ilex.reports.logic.financialReport.CustomerResultSetBean;
import com.ilex.reports.logic.financialReport.JobOwnerResultSetBean;
import com.ilex.reports.logic.financialReport.JobOwnerTableResultSetBean;
import com.ilex.reports.logic.financialReport.ProjectManagerResultSetBean;
import com.ilex.reports.logic.financialReport.RevenueExpenceTableResultSetBean;
import com.ilex.reports.logic.financialReport.SeniorProjectManagerResultSetBean;

public class RevenueExpenseForm extends ActionForm {

	private static final long serialVersionUID = 1L;
	private String startDate = null;
	private String endDate = null;
	private String graphXML = null;
	private String jobOwnerName = null;
	private String jobOwnerID = null;
	private String bdmName = null;
	private String bdmID = null;
	private String jobMonth = null;
	private String accountName = null;
	private String spmName = null;
	private String spmID = null;
	private String quarter = null;
	private String year = null;
	private String customerName = null;
	private String customerID = null;
	private String appendixName = null;
	private String projectManagerName = null;
	private String projectManagerID = null;

	public String getAppendixName() {
		return appendixName;
	}

	public void setAppendixName(String appendixName) {
		this.appendixName = appendixName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getSpmName() {
		return spmName;
	}

	public void setSpmName(String spmName) {
		this.spmName = spmName;
	}

	public String getSpmID() {
		return spmID;
	}

	public void setSpmID(String spmID) {
		this.spmID = spmID;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getBdmName() {
		return bdmName;
	}

	public void setBdmName(String bdmName) {
		this.bdmName = bdmName;
	}

	public String getBdmID() {
		return bdmID;
	}

	public void setBdmID(String bdmID) {
		this.bdmID = bdmID;
	}

	public String getJobMonth() {
		return jobMonth;
	}

	public void setJobMonth(String jobMonth) {
		this.jobMonth = jobMonth;
	}

	public String getJobOwnerID() {
		return jobOwnerID;
	}

	public void setJobOwnerID(String jobOwnerID) {
		this.jobOwnerID = jobOwnerID;
	}

	private String selectedSummaryPage = null;

	public String getSelectedSummaryPage() {
		return selectedSummaryPage;
	}

	public void setSelectedSummaryPage(String selectedSummaryPage) {
		this.selectedSummaryPage = selectedSummaryPage;
	}

	public String getJobOwnerName() {
		return jobOwnerName;
	}

	public void setJobOwnerName(String jobOwnerName) {
		this.jobOwnerName = jobOwnerName;
	}

	public String getProjectManagerName() {
		return projectManagerName;
	}

	public void setProjectManagerName(String projectManagerName) {
		this.projectManagerName = projectManagerName;
	}

	public String getProjectManagerID() {
		return projectManagerID;
	}

	public void setProjectManagerID(String projectManagerID) {
		this.projectManagerID = projectManagerID;
	}

	private List<? extends JobOwnerResultSetBean> jobOwnerList = null;
	private List<? extends ProjectManagerResultSetBean> projectManagerList = null;

	public List<? extends ProjectManagerResultSetBean> getProjectManagerList() {
		return projectManagerList;
	}

	public void setProjectManagerList(
			List<? extends ProjectManagerResultSetBean> projectManagerList) {
		this.projectManagerList = projectManagerList;
	}

	private List<SeniorProjectManagerResultSetBean> seniorProjectManagersList = null;

	private List<CustomerResultSetBean> customerList = null;
	private List<BussinessDevlopmentManagerResultSetBean> bdmList = null;

	public List<CustomerResultSetBean> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<CustomerResultSetBean> customerList) {
		this.customerList = customerList;
	}

	private List<JobOwnerTableResultSetBean> detailedList = null;
	private List<RevenueExpenceTableResultSetBean> revenueExpenceList = null;
	private String[] checked = null;
	private String fromDate = null;
	private String toDate = null;

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String[] getChecked() {
		return checked;
	}

	public void setChecked(String[] checked) {
		this.checked = checked;
	}

	public List<BussinessDevlopmentManagerResultSetBean> getBdmList() {
		return bdmList;
	}

	public void setBdmList(List<BussinessDevlopmentManagerResultSetBean> bdmList) {
		this.bdmList = bdmList;
	}

	public List<RevenueExpenceTableResultSetBean> getRevenueExpenceList() {
		return revenueExpenceList;
	}

	public void setRevenueExpenceList(
			List<RevenueExpenceTableResultSetBean> revenueExpenceList) {
		this.revenueExpenceList = revenueExpenceList;
	}

	public List<SeniorProjectManagerResultSetBean> getSeniorProjectManagersList() {
		return seniorProjectManagersList;
	}

	public void setSeniorProjectManagersList(
			List<SeniorProjectManagerResultSetBean> seniorProjectManagersList) {
		this.seniorProjectManagersList = seniorProjectManagersList;
	}

	public List<JobOwnerTableResultSetBean> getDetailedList() {
		return detailedList;
	}

	public void setDetailedList(List<JobOwnerTableResultSetBean> detailedList) {
		this.detailedList = detailedList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getGraphXML() {
		return graphXML;
	}

	public void setGraphXML(String graphXML) {
		this.graphXML = graphXML;
	}

	public List<? extends JobOwnerResultSetBean> getJobOwnerList() {
		return jobOwnerList;
	}

	public void setJobOwnerList(
			List<? extends JobOwnerResultSetBean> jobOwnerList) {
		this.jobOwnerList = jobOwnerList;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
