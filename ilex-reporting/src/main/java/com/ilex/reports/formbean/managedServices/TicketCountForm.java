package com.ilex.reports.formbean.managedServices;

import java.io.Serializable;

import org.apache.struts.action.ActionForm;

public class TicketCountForm extends ActionForm implements Serializable{

	private static final long serialVersionUID = 1L;
	private String graphXML;
	public String getGraphXML() {
		return graphXML;
	}
	public void setGraphXML(String graphXML) {
		this.graphXML = graphXML;
	}

}
