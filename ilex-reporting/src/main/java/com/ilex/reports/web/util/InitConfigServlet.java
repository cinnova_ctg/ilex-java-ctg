package com.ilex.reports.web.util;

import javax.servlet.http.HttpServlet;

import com.mind.fw.log.Log4jUtils;

public class InitConfigServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void init() {
		Log4jUtils.initLog();
	}
}