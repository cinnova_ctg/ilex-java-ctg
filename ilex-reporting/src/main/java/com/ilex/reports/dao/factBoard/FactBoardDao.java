package com.ilex.reports.dao.factBoard;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.ilex.reports.bean.factBoard.FactBoardBean;
import com.ilex.reports.dao.AbstractDAO;
import com.ilex.reports.dao.laborCost.LaborCostDAO;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;

public class FactBoardDao extends AbstractDAO {

	public static final Logger LOGGER = Logger.getLogger(LaborCostDAO.class);

	/**
	 * Sets the fact board values.
	 * 
	 * @param bean
	 *            the bean
	 * @return the fact board bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static FactBoardBean setFactBoardValues(FactBoardBean bean)
			throws SysException, AppException {

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		String query = "SELECT ytd_revenue,ytd_pro_forma_expense,ytd_expense,ytd_vgpm,ytd_vgpm_delta,cm_revenue,cm_pro_forma_expense,cm_expense,cm_vgpm,cm_vgpm_delta,"
				+

				" pytd_pro_forma_expense,pytd_expense,pytd_revenue,pytd_vgpm,pytd_vgpm_delta FROM ac_fact_board";
		try {
			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				bean.setYtdProForma(rs.getString("ytd_pro_forma_expense"));
				bean.setYtdExpense(rs.getString("ytd_expense"));
				bean.setYtdRevenue(rs.getString("ytd_revenue"));
				bean.setYtdVGPM(rs.getString("ytd_vgpm"));
				bean.setYtdVGPMDelta(rs.getString("ytd_vgpm_delta"));
				bean.setCmProForma(rs.getString("cm_pro_forma_expense"));
				bean.setCmExpense(rs.getString("cm_expense"));
				bean.setCmRevenue(rs.getString("cm_revenue"));
				bean.setCmVGPM(rs.getString("cm_vgpm"));
				bean.setCmVGPMDelta(rs.getString("cm_vgpm_delta"));
				bean.setPytdProFormaExpense(rs
						.getString("pytd_pro_forma_expense"));
				bean.setPytdExpense(rs.getString("pytd_expense"));
				bean.setPytdRevenue(rs.getString("pytd_revenue"));
				bean.setPytdVGPM(rs.getString("pytd_vgpm"));
				bean.setPytdVGPMDelta(rs.getString("pytd_vgpm_delta"));
			}

		} catch (SQLException sqlE) {
			LOGGER.error("SiteSearchDetail = " + bean.toString()
					+ "\n setFactBoardValues(FactBoardBean bean)", sqlE);
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}

		return bean;
	}

}
