package com.ilex.reports.dao.financialReport;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.log4j.Logger;

import com.ilex.reports.common.multiAxis.bean.MultiAxisChartBean;
import com.ilex.reports.common.multiAxis.bean.MultiAxisChartResultSetBean;
import com.ilex.reports.dao.AbstractDAO;
import com.ilex.reports.logic.financialReport.BussinessDevlopmentManagerResultSetBean;
import com.ilex.reports.logic.financialReport.CustomerResultSetBean;
import com.ilex.reports.logic.financialReport.FinancialReportType;
import com.ilex.reports.logic.financialReport.JobOwnerResultSetBean;
import com.ilex.reports.logic.financialReport.JobOwnerTableResultSetBean;
import com.ilex.reports.logic.financialReport.ProjectManagerResultSetBean;
import com.ilex.reports.logic.financialReport.RevenueExpenceTableResultSetBean;
import com.ilex.reports.logic.financialReport.SeniorProjectManagerResultSetBean;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;

public class FinancialReportDAO extends AbstractDAO {

	public static final Logger LOGGER = Logger
			.getLogger(FinancialReportDAO.class);

	/**
	 * Gets the date range for RevenueExpense.
	 * 
	 * @param classType
	 *            the class type
	 * @param query
	 *            the query
	 * @param roleName1
	 *            the role name1
	 * @param roleName2
	 *            the role name2
	 * 
	 * @return the date range
	 * 
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public <T extends MultiAxisChartBean> T getDateRangeForRevenueExpence(
			Class<T> classType, String query, String majorClassification,
			String reportName) throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}
		T multiAxisChartBean;
		try {
			multiAxisChartBean = classType.newInstance();
		} catch (InstantiationException e) {
			throw new SysException(e);
		} catch (IllegalAccessException e) {
			throw new SysException(e);
		}
		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {

			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			if (!reportName.equals("REVENUE_FINANCIAL_REPORT_ByJobOwner"))
				pStmt.setString(1, majorClassification);

			rs = pStmt.executeQuery();

			while (rs.next()) {

				multiAxisChartBean
						.setStartDate((rs.getString("mthStart") == null ? ""
								: rs.getString("mthStart")));
				multiAxisChartBean
						.setEndDate((rs.getString("mthEnd") == null ? "" : rs
								.getString("mthEnd")));
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return multiAxisChartBean;
	}

	/**
	 * Gets the date range.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param classType
	 *            the class type
	 * @param query
	 *            the query
	 * @param roleName1
	 *            the role name1
	 * @param roleName2
	 *            the role name2
	 * @return the date range
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public <T extends MultiAxisChartBean> T getDateRange(Class<T> classType,
			String query, String roleName1, String roleName2)
			throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}
		T multiAxisChartBean;
		try {
			multiAxisChartBean = classType.newInstance();
		} catch (InstantiationException e) {
			throw new SysException(e);
		} catch (IllegalAccessException e) {
			throw new SysException(e);
		}
		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {

			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, roleName1);
			pStmt.setString(2, roleName2);
			rs = pStmt.executeQuery();

			while (rs.next()) {

				multiAxisChartBean
						.setStartDate((rs.getString("mthStart") == null ? ""
								: rs.getString("mthStart")));
				multiAxisChartBean
						.setEndDate((rs.getString("mthEnd") == null ? "" : rs
								.getString("mthEnd")));
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return multiAxisChartBean;
	}

	/**
	 * Gets the no of records.
	 * 
	 * @param query
	 *            the query
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param reportName
	 *            the report name
	 * 
	 * @return the no of records
	 * 
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public int getNoOfRecords(String query, String startDate, String endDate,
			String jobOwnerName, String reportName) throws SysException,
			AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int noOfRecords = 0;

		try {
			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			if (reportName
					.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
							.getReportName())) {
				pStmt.setString(1, jobOwnerName);
				pStmt.setString(2, startDate);
				pStmt.setString(3, endDate);
				pStmt.setString(4, jobOwnerName);
				pStmt.setString(5, startDate);
				pStmt.setString(6, endDate);
			} else {
				pStmt.setString(1, startDate);
				pStmt.setString(2, endDate);
				pStmt.setString(3, startDate);
				pStmt.setString(4, endDate);
			}
			rs = pStmt.executeQuery();
			if (rs.next()) {
				noOfRecords = rs.getInt(1);
			}

		} catch (SQLException e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return noOfRecords;
	}

	/**
	 * Gets the no of records for revenue financial report.
	 * 
	 * @param query
	 *            the query
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param majorClassification
	 *            the major classification
	 * @param minorClassification
	 *            the minor classification
	 * @param reportName
	 *            the report name
	 * @param jobOwnerId
	 *            the job owner id
	 * @return the no of records revenue financial report
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public int getNoOfRecordsRevenueFinancialReport(String query,
			String startDate, String endDate, String majorClassification,
			String minorClassification, String reportName, String jobOwnerId)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int noOfRecords = 0;

		try {
			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, endDate);
			if (!reportName.equals("REVENUE_FINANCIAL_REPORT_ByJobOwner")) {
				pStmt.setString(2, majorClassification);
				pStmt.setString(3, minorClassification);
			} else {
				pStmt.setString(2, jobOwnerId);
			}
			rs = pStmt.executeQuery();
			if (rs.next()) {
				noOfRecords = rs.getInt(1);
			}

		} catch (SQLException e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return noOfRecords;
	}

	/**
	 * Gets the Result Set from query.
	 * 
	 * @param query
	 *            the query
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param reportType
	 *            the report type
	 * 
	 * @return the RS from query
	 * 
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<? extends MultiAxisChartResultSetBean> getRSFromQuery(
			String query, String startDate, String endDate,
			String jobOwnerName, String reportName) throws SysException,
			AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + startDate + "," + endDate);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends MultiAxisChartResultSetBean> beanList;
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			if (reportName
					.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
							.getReportName())) {
				pStmt.setString(1, jobOwnerName);
				pStmt.setString(2, startDate);
				pStmt.setString(3, endDate);
				pStmt.setString(4, jobOwnerName);
				pStmt.setString(5, startDate);
				pStmt.setString(6, endDate);
			} else {
				pStmt.setString(1, startDate);
				pStmt.setString(2, endDate);
				pStmt.setString(3, startDate);
				pStmt.setString(4, endDate);
			}

			rs = pStmt.executeQuery();
			MultiAxisChartResultSetBean multiAxisChartResultSetBean = new MultiAxisChartResultSetBean();

			beanList = beanProcessor.toBeanList(rs,
					multiAxisChartResultSetBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	/**
	 * Gets the revenue financial result set from query.
	 * 
	 * @param query
	 *            the query
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param majorClassification
	 *            the major classification
	 * @param minorClassification
	 *            the minor classification
	 * @param reportName
	 *            the report name
	 * @param jobOwnerId
	 *            the job owner id
	 * @return the revenue financial rs from query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<? extends MultiAxisChartResultSetBean> getRevenueFinancialRSFromQuery(
			String query, String startDate, String endDate,
			String majorClassification, String minorClassification,
			String reportName, String jobOwnerId) throws SysException,
			AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + startDate + "," + endDate);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends MultiAxisChartResultSetBean> beanList;
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			if (!reportName.equals("REVENUE_FINANCIAL_REPORT_ByJobOwner")) {
				pStmt.setString(1, endDate);
				pStmt.setString(2, majorClassification);
				pStmt.setString(3, minorClassification);
				pStmt.setString(4, endDate);
				pStmt.setString(5, majorClassification);
				pStmt.setString(6, minorClassification);
				pStmt.setString(7, endDate);
				pStmt.setString(8, majorClassification);
				pStmt.setString(9, minorClassification);
				pStmt.setString(10, endDate);
				pStmt.setString(11, majorClassification);
				pStmt.setString(12, minorClassification);
				pStmt.setString(13, endDate);
				pStmt.setString(14, majorClassification);
				pStmt.setString(15, minorClassification);
				pStmt.setString(16, endDate);
				pStmt.setString(17, majorClassification);
				pStmt.setString(18, minorClassification);
			} else {
				pStmt.setString(1, endDate);
				pStmt.setString(2, jobOwnerId);
				pStmt.setString(3, endDate);
				pStmt.setString(4, jobOwnerId);
				pStmt.setString(5, endDate);
				pStmt.setString(6, jobOwnerId);
				pStmt.setString(7, endDate);
				pStmt.setString(8, jobOwnerId);
				pStmt.setString(9, endDate);
				pStmt.setString(10, jobOwnerId);
				pStmt.setString(11, endDate);
				pStmt.setString(12, jobOwnerId);
				pStmt.setString(13, endDate);
				pStmt.setString(14, jobOwnerId);
			}

			rs = pStmt.executeQuery();
			LOGGER.debug("QUERY-> Executed");
			MultiAxisChartResultSetBean multiAxisChartResultSetBean = new MultiAxisChartResultSetBean();

			beanList = beanProcessor.toBeanList(rs,
					multiAxisChartResultSetBean.getClass());

			LOGGER.debug("beanlist populated");
		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	/**
	 * Gets the revenue financial result set from query1.
	 * 
	 * @param query
	 *            the query
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param majorClassification
	 *            the major classification
	 * @param minorClassification
	 *            the minor classification
	 * @param reportName
	 *            the report name
	 * @param jobOwnerId
	 *            the job owner id
	 * @return the revenue financial rs from query1
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	@SuppressWarnings("unchecked")
	public List<RevenueExpenceTableResultSetBean> getRevenueFinancialRSFromQuery1(
			String query, String startDate, String endDate,
			String majorClassification, String minorClassification,
			String reportName, String jobOwnerId) throws SysException,
			AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + startDate + "," + endDate);
		}
		Connection conn = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<RevenueExpenceTableResultSetBean> beanList;
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			cstmt = conn.prepareCall("{?=call revenueExpense_ResultSet(?,?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, majorClassification);
			cstmt.setString(3, minorClassification);
			rs = cstmt.executeQuery();
			RevenueExpenceTableResultSetBean revenueExpenceRSBean = new RevenueExpenceTableResultSetBean();

			beanList = (List<RevenueExpenceTableResultSetBean>) beanProcessor
					.toBeanList(rs, revenueExpenceRSBean.getClass());
			LOGGER.debug("QUERY-> Executed");
			LOGGER.debug("beanlist populated");
		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, cstmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	/**
	 * Gets the job owner result set from query.
	 * 
	 * @param query
	 *            the query
	 * 
	 * @return the job owner rs from query
	 * 
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<JobOwnerResultSetBean> getJobOwnerRSFromQuery(String query)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends JobOwnerResultSetBean> beanList;

		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			rs = pStmt.executeQuery();
			JobOwnerResultSetBean jobOwnerRSBean = new JobOwnerResultSetBean();

			beanList = beanProcessor.toBeanList(rs, jobOwnerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return (List<JobOwnerResultSetBean>) beanList;
	}

	/**
	 * Gets the project manager result set from query.
	 * 
	 * @param query
	 *            the query
	 * 
	 * @return the project manager rs from query
	 * 
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<ProjectManagerResultSetBean> getProjectManagerRSFromQuery(
			String query) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends ProjectManagerResultSetBean> beanList;

		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			rs = pStmt.executeQuery();
			ProjectManagerResultSetBean projectManagerRSBean = new ProjectManagerResultSetBean();

			beanList = beanProcessor.toBeanList(rs,
					projectManagerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return (List<ProjectManagerResultSetBean>) beanList;
	}

	public List<CustomerResultSetBean> getCustomerRSFromQuery(String query)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends JobOwnerResultSetBean> beanList;

		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			rs = pStmt.executeQuery();
			CustomerResultSetBean jobOwnerRSBean = new CustomerResultSetBean();

			beanList = beanProcessor.toBeanList(rs, jobOwnerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return (List<CustomerResultSetBean>) beanList;
	}

	/**
	 * Gets the job owner second level report query.
	 * 
	 * @param query
	 *            the query
	 * @return the job owner second level report query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<JobOwnerTableResultSetBean> getJobOwnerSecondLevelReportQuery(
			String query) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends JobOwnerTableResultSetBean> beanList;

		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			rs = pStmt.executeQuery();
			JobOwnerTableResultSetBean jobOwnerRSBean = new JobOwnerTableResultSetBean();

			beanList = beanProcessor.toBeanList(rs, jobOwnerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return (List<JobOwnerTableResultSetBean>) beanList;
	}

	/**
	 * Gets the job owner third level report query.
	 * 
	 * @param query
	 *            the query
	 * @return the job owner third level report query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<JobOwnerTableResultSetBean> getJobOwnerThirdLevelReportQuery(
			String query) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends JobOwnerTableResultSetBean> beanList;

		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			rs = pStmt.executeQuery();
			JobOwnerTableResultSetBean jobOwnerRSBean = new JobOwnerTableResultSetBean();

			beanList = beanProcessor.toBeanList(rs, jobOwnerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return (List<JobOwnerTableResultSetBean>) beanList;
	}

	/**
	 * Gets the ProjectManager second level report query.
	 * 
	 * @param query
	 *            the query
	 * @return the Project Manager second level report query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<JobOwnerTableResultSetBean> getProjectManagerSecondLevelReportQuery(
			String query) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends JobOwnerTableResultSetBean> beanList;

		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			rs = pStmt.executeQuery();
			JobOwnerTableResultSetBean projectManagerRSBean = new JobOwnerTableResultSetBean();

			beanList = beanProcessor.toBeanList(rs,
					projectManagerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return (List<JobOwnerTableResultSetBean>) beanList;
	}

	/**
	 * Gets the job owner third level report query.
	 * 
	 * @param query
	 *            the query
	 * @return the job owner third level report query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<JobOwnerTableResultSetBean> getProjectManagerThirdLevelReportQuery(
			String query) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends JobOwnerTableResultSetBean> beanList;

		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			rs = pStmt.executeQuery();
			JobOwnerTableResultSetBean jobOwnerRSBean = new JobOwnerTableResultSetBean();

			beanList = beanProcessor.toBeanList(rs, jobOwnerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return (List<JobOwnerTableResultSetBean>) beanList;
	}

	/**
	 * Gets the bDM result set query.
	 * 
	 * @param query
	 *            the query
	 * @return the bDM result set query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	@SuppressWarnings("unchecked")
	public List<BussinessDevlopmentManagerResultSetBean> getBDMResultSetQuery(
			String query) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		BeanProcessor beanProcessor = new BeanProcessor();
		List<BussinessDevlopmentManagerResultSetBean> beanList;

		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			rs = pStmt.executeQuery();
			BussinessDevlopmentManagerResultSetBean bdmRSBean = new BussinessDevlopmentManagerResultSetBean();

			beanList = (List<BussinessDevlopmentManagerResultSetBean>) beanProcessor
					.toBeanList(rs, bdmRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	/**
	 * Gets the senior project manager result set from query.
	 * 
	 * @param query
	 *            the query
	 * @return the senior project manager rs from query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	@SuppressWarnings("unchecked")
	public List<SeniorProjectManagerResultSetBean> getSeniorProjectManagerRSFromQuery(
			String query) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		BeanProcessor beanProcessor = new BeanProcessor();
		List<SeniorProjectManagerResultSetBean> beanList;

		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			rs = pStmt.executeQuery();
			SeniorProjectManagerResultSetBean seniorPMRSBean = new SeniorProjectManagerResultSetBean();

			beanList = (List<SeniorProjectManagerResultSetBean>) beanProcessor
					.toBeanList(rs, seniorPMRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	/**
	 * Gets the job owner detail query.
	 * 
	 * @param jobOwnerId
	 *            the job owner id
	 * @param reportName
	 *            the report name
	 * @return the job owner detail query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	@SuppressWarnings("unchecked")
	public List<JobOwnerTableResultSetBean> getJobOwnerDetailQuery(
			String jobOwnerId, String reportName, String accountID)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + jobOwnerId);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<JobOwnerTableResultSetBean> beanList;
		String reportType1 = "JobOwnerTTMSummary";
		String reportType2 = "jobOwnerAccountPopupReport";
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			if (reportType1.equals(reportName))
				cstmt = conn
						.prepareCall("{?=call jobOwnerTTMSummary_ResultSet(?)}");
			else if (reportType2.equals(reportName)) {
				cstmt = conn
						.prepareCall("{?=call jobOwnerAccountSummaryTTM_ResultSet(?,?)}");
			}
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, jobOwnerId);
			if (reportType2.equals(reportName)) {
				cstmt.setString(3, accountID);
			}
			rs = cstmt.executeQuery();
			JobOwnerTableResultSetBean jobOwnerRSBean = new JobOwnerTableResultSetBean();

			beanList = (List<JobOwnerTableResultSetBean>) beanProcessor
					.toBeanList(rs, jobOwnerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closeCallableStatement(rs, cstmt);
			DBUtil.closePreparedStatement(pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	/**
	 * Gets the project manager detail query.
	 * 
	 * @param projectmanagerId
	 *            the project manager Id
	 * @param reportName
	 *            the report name
	 * @return the job owner detail query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	@SuppressWarnings("unchecked")
	public List<JobOwnerTableResultSetBean> getProjectManagerDetailQuery(
			String projectmanagerId, String reportName, String accountID)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + projectmanagerId);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<JobOwnerTableResultSetBean> beanList;
		String reportType1 = "projectManagerTTMSummary";
		String reportType2 = "projectManagerAccountPopupReport";
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			if (reportType1.equalsIgnoreCase(reportName))
				cstmt = conn
						.prepareCall("{?=call projectManagerTTMSummary_ResultSet(?)}");
			else if (reportType2.equalsIgnoreCase(reportName)) {
				cstmt = conn
						.prepareCall("{?=call projectManagerAccountSummaryTTM_ResultSet(?,?)}");
			}
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, projectmanagerId);
			if (reportType2.equals(reportName)) {
				cstmt.setString(3, accountID);
			}
			rs = cstmt.executeQuery();
			JobOwnerTableResultSetBean jobOwnerRSBean = new JobOwnerTableResultSetBean();

			beanList = (List<JobOwnerTableResultSetBean>) beanProcessor
					.toBeanList(rs, jobOwnerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closeCallableStatement(rs, cstmt);
			DBUtil.closePreparedStatement(pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	public List<JobOwnerTableResultSetBean> getCustomerDetailQuery(
			String customerId, String reportName, String appendixID)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + customerId);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<JobOwnerTableResultSetBean> beanList;
		try {
			initIlexDS();
			conn = ilexDS.getConnection();
			cstmt = conn
					.prepareCall("{?=call customerTTMSummary_ResultSet(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, appendixID);
			rs = cstmt.executeQuery();
			JobOwnerTableResultSetBean jobOwnerRSBean = new JobOwnerTableResultSetBean();

			beanList = (List<JobOwnerTableResultSetBean>) beanProcessor
					.toBeanList(rs, jobOwnerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closeCallableStatement(rs, cstmt);
			DBUtil.closePreparedStatement(pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	/**
	 * Gets the bDM detail query.
	 * 
	 * @param bdmId
	 *            the bdm id
	 * @param reportName
	 *            the report name
	 * @param accountID
	 *            the account id
	 * @return the bDM detail query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<JobOwnerTableResultSetBean> getBDMDetailQuery(String bdmId,
			String reportName, String accountID) throws SysException,
			AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY bdmId->" + bdmId);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<JobOwnerTableResultSetBean> beanList;
		String reportType1 = "BDM(TTM)Summary";
		String reportType2 = "bdmAccountPopupReport";
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			if (reportType1.equals(reportName)) {
				cstmt = conn
						.prepareCall("{?=call bdm_TTMSummary_ResultSet(?)}");
			} else if (reportType2.equals(reportName)) {
				cstmt = conn
						.prepareCall("{?=call bdmAccountSummaryTTM_ResultSet(?,?)}");
			}
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, bdmId);
			if (reportType2.equals(reportName)) {
				cstmt.setString(3, accountID);
			}
			rs = cstmt.executeQuery();
			JobOwnerTableResultSetBean jobOwnerRSBean = new JobOwnerTableResultSetBean();

			beanList = (List<JobOwnerTableResultSetBean>) beanProcessor
					.toBeanList(rs, jobOwnerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closeCallableStatement(rs, cstmt);
			DBUtil.closePreparedStatement(pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	/**
	 * Gets the sPM detail query.
	 * 
	 * @param spmId
	 *            the spm id
	 * @param reportName
	 *            the report name
	 * @param accountID
	 *            the account id
	 * @return the sPM detail query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<JobOwnerTableResultSetBean> getSPMDetailQuery(String spmId,
			String reportName, String accountID) throws SysException,
			AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY bdmId->" + spmId);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<JobOwnerTableResultSetBean> beanList;
		String reportType1 = "SPM(TTM)Summary";
		String reportType2 = "spmAccountPopupReport";
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			if (reportType1.equals(reportName)) {
				cstmt = conn
						.prepareCall("{?=call spm_TTMSummary_ResultSet(?)}");
			} else if (reportType2.equals(reportName)) {
				cstmt = conn
						.prepareCall("{?=call spmAccountSummaryTTM_ResultSet(?,?)}");
			}
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, spmId);
			if (reportType2.equals(reportName)) {
				cstmt.setString(3, accountID);
			}
			rs = cstmt.executeQuery();
			JobOwnerTableResultSetBean jobOwnerRSBean = new JobOwnerTableResultSetBean();

			beanList = (List<JobOwnerTableResultSetBean>) beanProcessor
					.toBeanList(rs, jobOwnerRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closeCallableStatement(rs, cstmt);
			DBUtil.closePreparedStatement(pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	/**
	 * Gets the max y axis value for finance.
	 * 
	 * @param query
	 *            the query
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * 
	 * @return the max y axis value for finance
	 * 
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public long getMaxYAxisValueForFinance(String query, String startDate,
			String endDate) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + startDate + "," + endDate);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		long maxYAxisValue = 0;

		try {
			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, startDate);
			pStmt.setString(2, endDate);
			pStmt.setString(3, startDate);
			pStmt.setString(4, endDate);

			rs = pStmt.executeQuery();

			while (rs.next()) {
				maxYAxisValue = rs.getLong(1);
			}
		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}

		return maxYAxisValue;

	}

	/**
	 * Gets the max y axis value for revenue finance.
	 * 
	 * @param query
	 *            the query
	 * @param jobOwnerId
	 *            the job owner id
	 * @param endDate
	 *            the end date
	 * @param majorClassification
	 *            the major classification
	 * @param minorClassification
	 *            the minor classification
	 * @param reportName
	 *            the report name
	 * @return the max y axis value for revenue finance
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public long getMaxYAxisValueForRevenueFinance(String query,
			String jobOwnerId, String endDate, String majorClassification,
			String minorClassification, String reportName) throws SysException,
			AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + endDate);
		}

		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		long maxYAxisValue = 0;
		long value[] = new long[3];
		try {
			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, endDate);
			if (!reportName.equals("REVENUE_FINANCIAL_REPORT_ByJobOwner")) {
				pStmt.setString(2, majorClassification);
				pStmt.setString(3, minorClassification);
			} else {
				pStmt.setString(2, jobOwnerId);
			}
			rs = pStmt.executeQuery();

			while (rs.next()) {

				value[0] = rs.getLong(1);
				value[1] = rs.getLong(2);
				value[2] = rs.getLong(3);

			}
			if (value[0] > value[1] && value[0] > value[2]) {
				maxYAxisValue = value[0];
			} else if (value[1] > value[0] && value[1] > value[2]) {
				maxYAxisValue = value[1];
			} else {
				maxYAxisValue = value[2];
			}
		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}

		return maxYAxisValue;

	}

}
