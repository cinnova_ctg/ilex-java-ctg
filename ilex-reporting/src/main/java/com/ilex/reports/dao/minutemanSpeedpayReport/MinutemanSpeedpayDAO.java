package com.ilex.reports.dao.minutemanSpeedpayReport;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.log4j.Logger;

import com.ilex.reports.bean.minutemanSpeedpayReport.MinutemanSpeedpayCostBean;
import com.ilex.reports.common.LabelValue;
import com.ilex.reports.common.multiAxis.bean.Stacked2DChartResultSetBean;
import com.ilex.reports.dao.AbstractDAO;
import com.ilex.reports.logic.minutemanSpeedpayReport.MinutemanSpeedpayTotalValueByMonth;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;

// TODO: Auto-generated Javadoc
/**
 * The Class MinutemanSpeedpayDAO.
 */
public class MinutemanSpeedpayDAO extends AbstractDAO {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = Logger
			.getLogger(MinutemanSpeedpayDAO.class);

	/**
	 * Populate project category.
	 * 
	 * @param query
	 *            the query
	 * @return the array list
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static ArrayList<LabelValue> populateProjectCategory(String query)
			throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		initIlexDS();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<LabelValue> projectCategories = new ArrayList<LabelValue>();
		int projectCategoriesCount = 0;

		try {
			conn = ilexDS.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				LabelValue projectCategory = new LabelValue(
						rs.getString("labelCategory"),
						rs.getString("valueCategory"));
				projectCategories.add(projectCategoriesCount, projectCategory);
				projectCategoriesCount++;
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closeStatement(rs, stmt);
			DBUtil.closeDbConnection(conn);
		}

		return projectCategories;
	}

	/**
	 * Gets the total value by month.
	 * 
	 * @param query
	 *            the query
	 * @param param
	 *            the param
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @return the total value by month
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static ArrayList<MinutemanSpeedpayTotalValueByMonth> getTotalValueByMonth(
			String query, String param, String startDate, String endDate)
			throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		ArrayList<MinutemanSpeedpayTotalValueByMonth> totalCostByMonthList = new ArrayList<MinutemanSpeedpayTotalValueByMonth>();
		try {
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, param);
			pStmt.setString(2, startDate);
			pStmt.setString(3, endDate);
			rs = pStmt.executeQuery();

			while (rs.next()) {
				MinutemanSpeedpayTotalValueByMonth totalCostByMonth = new MinutemanSpeedpayTotalValueByMonth();
				totalCostByMonth
						.setDisplay_month(rs.getString("display_month"));
				totalCostByMonth.setTotalValue(rs.getString("Sum"));
				totalCostByMonthList.add(totalCostByMonth);
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closeStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}

		return totalCostByMonthList;
	}

	/**
	 * Gets the date range.
	 * 
	 * @param minutemanCostBean
	 *            the minuteman cost bean
	 * @param query
	 *            the query
	 * @param param1
	 *            the param1
	 * @return the date range
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static void getDateRange(
			MinutemanSpeedpayCostBean minutemanCostBean, String query,
			String param1) throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {

			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, param1);

			rs = pStmt.executeQuery();

			while (rs.next()) {
				minutemanCostBean
						.setStartDate((rs.getString("mthStart") == null ? ""
								: rs.getString("mthStart")));
				minutemanCostBean
						.setEndDate((rs.getString("mthEnd") == null ? "" : rs
								.getString("mthEnd")));
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
	}

	/**
	 * Gets the no of records.
	 * 
	 * @param query
	 *            the query
	 * @param param1
	 *            the param1
	 * @param param2
	 *            the param2
	 * @param param3
	 *            the param3
	 * @param param4
	 *            the param4
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param reportName
	 *            the report name
	 * @return the no of records
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static int getNoOfRecords(String query, String param1,
			String param2, String param3, String param4, String startDate,
			String endDate, String reportName) throws SysException,
			AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1 + "," + param1);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int noOfRecords = 0;

		try {
			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, param1);
			pStmt.setString(2, param2);
			pStmt.setString(3, param3);
			pStmt.setString(4, param4);
			pStmt.setString(5, startDate);
			pStmt.setString(6, endDate);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				noOfRecords = rs.getInt(1);
			}

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return noOfRecords;
	}

	/**
	 * Gets the max of minuteman cost data.
	 * 
	 * @param query
	 *            the query
	 * @param param1
	 *            the param1
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param reportName
	 *            the report name
	 * @return the max of minuteman cost data
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static Long getMaxOfMinutemanCostData(String query, String param1,
			String startDate, String endDate, String reportName)
			throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1 + "," + startDate + ","
					+ endDate);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		long maxYAxisValue = 0;
		int[] valuelist = new int[36];
		Date month = null;
		int index = 0;
		try {
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, param1);
			pStmt.setString(2, startDate);
			pStmt.setString(3, endDate);

			rs = pStmt.executeQuery();

			while (rs.next()) {
				if (reportName.equals("USAGE_BY_OCCURENCES")) {
					if (rs.getDate("month").equals(month)) {
						valuelist[index] = valuelist[index]
								+ rs.getInt("occurrences");
					} else {
						month = rs.getDate("month");
						++index;
						valuelist[index] = rs.getInt("occurrences");
					}
				} else {
					if (rs.getDate("month").equals(month)) {
						valuelist[index] = valuelist[index] + rs.getInt("cost");
					} else {
						month = rs.getDate("month");
						++index;
						valuelist[index] = rs.getInt("cost");
					}
				}

			}

			maxYAxisValue = valuelist[1];
			for (int r = 2; r <= index; ++r) {
				if (maxYAxisValue < valuelist[r]) {
					maxYAxisValue = valuelist[r];
				}
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}

		return maxYAxisValue;
	}

	/**
	 * Gets the ResultSet from query.
	 * 
	 * @param query
	 *            the query
	 * @param param1
	 *            the param1
	 * @param param2
	 *            the param2
	 * @param param3
	 *            the param3
	 * @param param4
	 *            the param4
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param reportType
	 *            the report type
	 * @return the RS from query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static List<? extends Stacked2DChartResultSetBean> getRSFromQuery(
			String query, String param1, String param2, String param3,
			String param4, String startDate, String endDate, String reportType)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + startDate + "," + endDate);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends Stacked2DChartResultSetBean> beanList;
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			pStmt.setString(1, param1);
			pStmt.setString(2, param2);
			pStmt.setString(3, param3);
			pStmt.setString(4, param4);
			pStmt.setString(5, startDate);
			pStmt.setString(6, endDate);

			rs = pStmt.executeQuery();
			Stacked2DChartResultSetBean minutemanCostRSBean = new Stacked2DChartResultSetBean();

			beanList = beanProcessor.toBeanList(rs,
					minutemanCostRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

}
