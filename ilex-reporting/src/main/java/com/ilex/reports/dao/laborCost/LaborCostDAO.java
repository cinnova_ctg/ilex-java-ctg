package com.ilex.reports.dao.laborCost;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.log4j.Logger;

import com.ilex.reports.bean.laborCost.LaborCostBean;
import com.ilex.reports.bean.laborCost.LaborCostByLocationBean;
import com.ilex.reports.bean.laborCost.LaborCostSrPmBean;
import com.ilex.reports.common.LabelValue;
import com.ilex.reports.common.ReportStaticData;
import com.ilex.reports.common.multiAxis.bean.MultiAxisChartResultSetBean;
import com.ilex.reports.dao.AbstractDAO;
import com.ilex.reports.logic.laborCost.LaborCostReportType;
import com.ilex.reports.util.DateUtils;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;

// TODO: Auto-generated Javadoc
/**
 * The Class LaborCostDAO.
 */
public class LaborCostDAO extends AbstractDAO {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = Logger.getLogger(LaborCostDAO.class);

	/**
	 * Gets the y-coordinate max value in multiAxis chart for Top Labor Cost
	 * reports.
	 *
	 * @param query - String.
	 * @param param1 the param1
	 * @param param2 the param2
	 * @param param3 the param3
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return integer.
	 * @throws SysException - SysException.
	 * @throws AppException - AppException.
	 */
	public static Long getMaxOfLaborCostData(String query, String param1,
			String param2, String param3, String startDate, String endDate)
			throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1 + "," + param2 + ","
					+ param3 + "," + startDate + "," + endDate);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		long maxYAxisValue = 0;

		try {
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, param1);
			pStmt.setString(2, param2);
			pStmt.setString(3, param3);
			pStmt.setString(4, startDate);
			pStmt.setString(5, endDate);

			rs = pStmt.executeQuery();

			while (rs.next()) {
				maxYAxisValue = rs.getInt("col1");
				if (rs.getInt("col2") > maxYAxisValue) {
					maxYAxisValue = rs.getInt("col2");
				}
				if (rs.getInt("col3") > maxYAxisValue) {
					maxYAxisValue = rs.getInt("col3");
				}
			}
		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}

		return maxYAxisValue;
	}

	/**
	 * Get the list of labor categories.
	 * 
	 * @param query
	 *            - String.
	 * @return ArrayList<LabelValue>.
	 * @throws SysException
	 *             - SysException.
	 * @throws AppException
	 *             - AppException.
	 */
	public static ArrayList<LabelValue> populateLaborCategory(String query)
			throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		initIlexDS();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<LabelValue> laborCategories = new ArrayList<LabelValue>();
		int laborCategoryCount = 0;

		try {
			conn = ilexDS.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				LabelValue laborCategory = new LabelValue(
						rs.getString("labelCategory"),
						rs.getString("valueCategory"));
				laborCategories.add(laborCategoryCount, laborCategory);
				laborCategoryCount++;
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closeStatement(rs, stmt);
			DBUtil.closeDbConnection(conn);
		}

		return laborCategories;
	}

	/**
	 * Gets date range for Labor Cost chart.
	 *
	 * @param laborCostBean the labor cost bean
	 * @param query - String.
	 * @param param1 - String parameter for prepared statement.
	 * @param param2 - String parameter for prepared statement.
	 * @param param3 - String parameter for prepared statement.
	 * @return the date range
	 * @throws SysException - SysException.
	 * @throws AppException - AppException.
	 */
	public static void getDateRange(LaborCostBean laborCostBean, String query,
			String param1, String param2, String param3) throws SysException,
			AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1 + "," + param2 + ","
					+ param3);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {

			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, param1);
			pStmt.setString(2, param2);
			pStmt.setString(3, param3);

			rs = pStmt.executeQuery();

			while (rs.next()) {
				laborCostBean
						.setStartDate((rs.getString("mthStart") == null ? ""
								: rs.getString("mthStart")));
				laborCostBean.setEndDate((rs.getString("mthEnd") == null ? ""
						: rs.getString("mthEnd")));
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
	}

	/**
	 * Create bean list with all dates in specified date range.
	 *
	 * @param startDate - String.
	 * @param monthcount the monthcount
	 * @return ArrayList<LaborCostBean>.
	 */
	public static ArrayList<String> setCompleteMonthData(String startDate,
			int monthcount) {

		SimpleDateFormat sdf = new SimpleDateFormat("MM/yy");
		Date dt = DateUtils.convertStringToSqlDate(startDate);
		Calendar cal = new GregorianCalendar();
		cal.setTime(dt);
		String dd = "";

		ArrayList<String> xCoordinatesDateList = new ArrayList<String>();
		for (int cnt = 0; cnt < monthcount; cnt++) {
			dd = sdf.format(cal.getTime());
			xCoordinatesDateList.add(cnt, dd);
			cal.add(Calendar.MONTH, 1);
		}

		return xCoordinatesDateList;
	}

	/**
	 * Gets date range of 13 months for Labor Cost chart.
	 *
	 * @param laborCostSrPmBean - LaborCostSrPmBean.
	 * @param query - String.
	 * @param param1 - String parameter for prepared statement.
	 * @param param2 - String parameter for prepared statement.
	 * @return the date range of thirteen months
	 * @throws SysException - SysException.
	 * @throws AppException - AppException.
	 */
	public static void getDateRangeOfThirteenMonths(
			LaborCostSrPmBean laborCostSrPmBean, String query, String param1,
			String param2) throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1 + "," + param2);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {

			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, param1);
			pStmt.setString(2, param2);

			rs = pStmt.executeQuery();

			while (rs.next()) {
				laborCostSrPmBean
						.setStartDate((rs.getString("mthStart") == null ? ""
								: rs.getString("mthStart")));
				laborCostSrPmBean
						.setEndDate((rs.getString("mthEnd") == null ? "" : rs
								.getString("mthEnd")));
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
	}

	/**
	 * Gets maximum value for y coordinates in Sr PM Labor Cost report.
	 *
	 * @param query - String.
	 * @param param1 - String parameter for prepared statement.
	 * @param param2 - String parameter for prepared statement.
	 * @param startDate - String parameter for prepared statement.
	 * @param endDate - String parameter for prepared statement.
	 * @return the max of sr pm labor cost data
	 * @throws SysException - SysException.
	 * @throws AppException - AppException.
	 */

	public static int getMaxOfSrPmLaborCostData(String query, String param1,
			String param2, String startDate, String endDate)
			throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1 + "," + param2 + ","
					+ startDate + "," + endDate);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int maxYAxisValue = 0;

		try {
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, param1);
			pStmt.setString(2, param1);
			pStmt.setString(3, startDate);
			pStmt.setString(4, endDate);

			rs = pStmt.executeQuery();

			while (rs.next()) {
				maxYAxisValue = rs.getInt("col1");
				if (rs.getInt("col2") > maxYAxisValue) {
					maxYAxisValue = rs.getInt("col2");
				}
			}
		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}

		maxYAxisValue = getMaxOfLaborCostBySPM(maxYAxisValue,
				ReportStaticData.getQueryForMaxOfLaborCostBySPMName(), param2,
				startDate, endDate);

		return maxYAxisValue;
	}

	/**
	 * Set bean list for all dates in given date range for Sr PM Labor Cost
	 * report.
	 *
	 * @param rs - ResultSet.
	 * @param startDate - String.
	 * @param count - integer.
	 * @return ArrayList<LaborCostSrPmBean>
	 */
	public static ArrayList<LaborCostSrPmBean> setSrPmCompleteMonthData(
			ResultSet rs, String startDate, int count) {

		SimpleDateFormat sdf = new SimpleDateFormat("MM/yy");
		Date dt = DateUtils.convertStringToSqlDate(startDate);
		Calendar cal = new GregorianCalendar();
		cal.setTime(dt);

		String dd = "";

		ArrayList<LaborCostSrPmBean> laborCostSrPmBeans = new ArrayList<LaborCostSrPmBean>();

		for (int cnt = 0; cnt < count; cnt++) {

			dd = sdf.format(cal.getTime());

			LaborCostSrPmBean lcSrPmBean = new LaborCostSrPmBean();
			lcSrPmBean.setxCordinateDate(dd);
			lcSrPmBean.setyCordinateLabor("0");
			lcSrPmBean.setyCordinateTechnician("0");

			lcSrPmBean.setyCordinatePM1("0");
			lcSrPmBean.setyCordinatePM2("0");
			lcSrPmBean.setyCordinatePM3("0");
			lcSrPmBean.setyCordinatePM4("0");
			lcSrPmBean.setyCordinatePM5("0");

			lcSrPmBean.setNamePM1("");
			lcSrPmBean.setNamePM2("");
			lcSrPmBean.setNamePM3("");
			lcSrPmBean.setNamePM4("");
			lcSrPmBean.setNamePM5("");

			laborCostSrPmBeans.add(cnt, lcSrPmBean);
			cal.add(Calendar.MONTH, 1);
		}

		return laborCostSrPmBeans;
	}

	/**
	 * Get maximum of average value among all SPM in Sr PM Labor Cost report.
	 * 
	 * @param maxYAxisValue
	 *            - integer.
	 * @param query
	 *            - String.
	 * @param param1
	 *            - String parameter.
	 * @param startDate
	 *            - String.
	 * @param endDate
	 *            - String.
	 * @return integer
	 * @throws SysException
	 *             - SysException.
	 * @throws AppException
	 *             - AppException.
	 */

	public static int getMaxOfLaborCostBySPM(int maxYAxisValue, String query,
			String param1, String startDate, String endDate)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1 + "," + startDate + ","
					+ endDate);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int maxYAxisVal = maxYAxisValue;
		try {
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, param1);
			pStmt.setString(2, startDate);
			pStmt.setString(3, endDate);

			rs = pStmt.executeQuery();

			while (rs.next()) {
				if (rs.getInt("rateAvg") > maxYAxisVal) {
					maxYAxisVal = rs.getInt("rateAvg");
				}
			}
		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}

		return maxYAxisVal;
	}

	/**
	 * Gets the rs from query.
	 * 
	 * @param query
	 *            the query
	 * @param param1
	 *            the param1
	 * @param param2
	 *            the param2
	 * @param param3
	 *            the param3
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param reportType
	 *            the report type
	 * @return the RS from query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static List<? extends MultiAxisChartResultSetBean> getRSFromQuery(
			String query, String param1, String param2, String param3,
			String startDate, String endDate, String reportType)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1 + "," + param2 + ","
					+ param3 + "," + startDate + "," + endDate);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends MultiAxisChartResultSetBean> beanList;
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			pStmt.setString(1, param1);
			pStmt.setString(2, param2);
			pStmt.setString(3, param3);

			pStmt.setString(4, startDate);
			pStmt.setString(5, endDate);
			if (reportType.equals(LaborCostReportType.SR_PM_LABOR_COST
					.getReportName())) {
				pStmt.setString(6, startDate);
				pStmt.setString(7, endDate);
			}

			rs = pStmt.executeQuery();
			MultiAxisChartResultSetBean laborCostRSBean = new MultiAxisChartResultSetBean();

			beanList = beanProcessor.toBeanList(rs, laborCostRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	/**
	 * Gets the result set related to labor Reports from query.
	 * 
	 * @param query
	 *            the query
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param reportType
	 *            the report type
	 * @return the RS from query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static List<? extends MultiAxisChartResultSetBean> getRSFromQuery(
			String query, String startDate, String endDate, String reportType)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + startDate + "," + endDate);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends MultiAxisChartResultSetBean> beanList;
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			pStmt.setString(1, startDate);
			pStmt.setString(2, endDate);
			pStmt.setString(3, startDate);
			pStmt.setString(4, endDate);

			rs = pStmt.executeQuery();
			MultiAxisChartResultSetBean laborCostRSBean = new MultiAxisChartResultSetBean();

			beanList = beanProcessor.toBeanList(rs, laborCostRSBean.getClass());

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}

	/**
	 * Gets the no of records related to labor Report.
	 *
	 * @param query the query
	 * @param param1 the param1
	 * @param param2 the param2
	 * @param param3 the param3
	 * @param startDate the start date
	 * @param endDate the end date
	 * @param reportName the report name
	 * @return the no of records
	 * @throws SysException the sys exception
	 * @throws AppException the app exception
	 */
	public static int getNoOfRecords(String query, String param1,
			String param2, String param3, String startDate, String endDate,
			String reportName) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1 + "," + param2);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int noOfRecords = 0;

		try {
			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, param1);
			pStmt.setString(2, param2);
			pStmt.setString(3, param3);
			pStmt.setString(4, startDate);
			pStmt.setString(5, endDate);
			if (reportName.equals(LaborCostReportType.SR_PM_LABOR_COST
					.getReportName())) {
				pStmt.setString(6, startDate);
				pStmt.setString(7, endDate);
			}
			rs = pStmt.executeQuery();
			if (rs.next()) {
				noOfRecords = rs.getInt(1);
			}

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return noOfRecords;
	}

	/**
	 * Gets the no of records for category related to labor Report.
	 * 
	 * @param query
	 *            the query
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param reportName
	 *            the report name
	 * @return the no of records for category
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static int getNoOfRecordsForCategory(String query, String startDate,
			String endDate, String reportName) throws SysException,
			AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + startDate + "," + endDate + ","
					+ reportName);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int noOfRecords = 0;

		try {
			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, startDate);
			pStmt.setString(2, endDate);
			pStmt.setString(3, startDate);
			pStmt.setString(4, endDate);

			rs = pStmt.executeQuery();
			if (rs.next()) {
				noOfRecords = rs.getInt(1);
			}

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return noOfRecords;
	}

	

	/**
	 * Gets the data for location wise labor cost.
	 *
	 * @param query the query
	 * @param paramType the param type
	 * @param paramCount the param count
	 * @param paramFilter the param filter
	 * @param paramOrderBy the param order by
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the data for location wise labor cost
	 * @throws SysException the sys exception
	 * @throws AppException the app exception
	 */
	public static ArrayList<LaborCostByLocationBean> getDataForLocationWiseLaborCost(
			String query, String paramType, String paramCount,
			String paramFilter, String paramOrderBy,String startDate,String endDate) throws SysException,
			AppException {

		ArrayList<LaborCostByLocationBean> valueBeans = new ArrayList<LaborCostByLocationBean>();

		StringBuilder sb = new StringBuilder(query);
		sb.append(paramOrderBy);
		query = sb.toString();

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + paramType + "," + paramCount
					+ "," + paramFilter + "," + paramOrderBy);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int lcLocationCount = 0;

		try {

			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, paramType);
			pStmt.setString(2, paramCount);
			pStmt.setString(3, paramFilter);
			pStmt.setString(4, startDate);
			pStmt.setString(5, endDate);

			rs = pStmt.executeQuery();

			LaborCostByLocationBean lcLocationBean = null;
			while (rs.next()) {
				lcLocationBean = new LaborCostByLocationBean();
				lcLocationBean.setLocation(rs.getString("location"));
				lcLocationBean.setLaborRate(rs.getString("rate"));
				lcLocationBean.setNumberCount(rs.getString("count"));
				lcLocationBean.setLastDate(rs.getString("last_used"));

				valueBeans.add(lcLocationCount, lcLocationBean);
				lcLocationCount++;
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}

		return valueBeans;
	}

	/**
	 * Gets the date range from query related to labor Report.
	 * 
	 * @param laborCostBean
	 *            the labor cost bean
	 * @param query
	 *            the query
	 * @return the date range from query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static void getDateRangeFromQuery(LaborCostBean laborCostBean,
			String query) throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {

			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			rs = pStmt.executeQuery();

			while (rs.next()) {
				laborCostBean
						.setStartDate((rs.getString("mthStart") == null ? ""
								: rs.getString("mthStart")));
				laborCostBean.setEndDate((rs.getString("mthEnd") == null ? ""
						: rs.getString("mthEnd")));
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
	}

	/**
	 * Gets the max of y-axis for sprm.
	 * 
	 * @param query
	 *            the query
	 * @param param1
	 *            the param1
	 * @param param2
	 *            the param2
	 * @param param3
	 *            the param3
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @return the max ofy for sprm
	 * @throws SysException
	 *             the sys exception
	 */
	public static long getMaxOFYForSPRM(String query, String param1,
			String param2, String param3, String startDate, String endDate)
			throws SysException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1 + "," + param2 + ","
					+ param3 + "," + startDate + "," + endDate);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		long maxYValue = 0;
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			pStmt.setString(1, param1);
			pStmt.setString(2, param2);
			pStmt.setString(3, param3);

			pStmt.setString(4, startDate);
			pStmt.setString(5, endDate);
			pStmt.setString(6, startDate);
			pStmt.setString(7, endDate);

			rs = pStmt.executeQuery();
			if (rs.next()) {
				maxYValue = rs.getLong(1);
			}

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return maxYValue;

	}
}
