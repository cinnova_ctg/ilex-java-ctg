package com.ilex.reports.dao.minutemanUtilization;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.log4j.Logger;

import com.ilex.reports.bean.minutemanUtilization.MinutemanUtilizationBean;
import com.ilex.reports.common.multiAxis.bean.Stacked2DChartResultSetBean;
import com.ilex.reports.dao.AbstractDAO;
import com.ilex.reports.logic.minutemanUtilization.MinutemanUtilizationResultSetBean;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;

public class MinutemanUtilizationDAO extends AbstractDAO {
	public static final Logger LOGGER = Logger
			.getLogger(MinutemanUtilizationDAO.class);

	/**
	 * Gets the first time minuteman usage Data.
	 * 
	 * @param query
	 *            the query
	 * @return the first time minuteman usage
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<MinutemanUtilizationResultSetBean> getFirstTimeMinutemanUsage(
			String query) throws SysException, AppException {
		initIlexDS();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		MinutemanUtilizationResultSetBean minutemanUtilizationResultSetBean = null;
		List<MinutemanUtilizationResultSetBean> resultList = new ArrayList<MinutemanUtilizationResultSetBean>();
		try {
			conn = ilexDS.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				minutemanUtilizationResultSetBean = new MinutemanUtilizationResultSetBean();
				minutemanUtilizationResultSetBean.setName(rs
						.getString("unique_name"));
				minutemanUtilizationResultSetBean.setOccurance(rs
						.getInt("occurance"));
				resultList.add(minutemanUtilizationResultSetBean);
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closeStatement(rs, stmt);
			DBUtil.closeDbConnection(conn);
		}
		return resultList;

	}

	/**
	 * Gets the minuteman usage by Job Owner Data.
	 * 
	 * 
	 * @return the first time minuteman usage list
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<MinutemanUtilizationResultSetBean> getMinutemanUsageByJobOwner(String lasttwo)
			throws SysException, AppException {
		initIlexDS();
		Connection conn = null;

		CallableStatement cstmt = null;
		ResultSet rs = null;
		MinutemanUtilizationResultSetBean minutemanUtilizationResultSetBean = null;
		List<MinutemanUtilizationResultSetBean> resultList = new ArrayList<MinutemanUtilizationResultSetBean>();
		try {
			conn = ilexDS.getConnection();
			cstmt = conn
					.prepareCall("{?=call ac_fact2_dyna_minuteman_monthly_summary_01 ?}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setString(2, lasttwo);
			rs = cstmt.executeQuery();

			while (rs.next()) {
				minutemanUtilizationResultSetBean = new MinutemanUtilizationResultSetBean();
				minutemanUtilizationResultSetBean.setName(rs
						.getString("unique_name"));
				minutemanUtilizationResultSetBean
						.setOccurance(rs.getInt("occ"));
				resultList.add(minutemanUtilizationResultSetBean);
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closeStatement(rs, cstmt);
			DBUtil.closeDbConnection(conn);
		}
		return resultList;

	}

	/**
	 * Gets the Minuteman Availability /Usage Data .
	 * 
	 * @param query
	 *            the query
	 * @param availablityMMBean
	 *            the availablity mm bean
	 * @return the availablity
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public MinutemanUtilizationBean getAvailablity(String query,
			MinutemanUtilizationBean availablityMMBean) throws SysException,
			AppException {
		initIlexDS();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		MinutemanUtilizationBean bean = availablityMMBean;
		try {
			conn = ilexDS.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				bean.setTotalMM(rs.getString("totalMM"));
				bean.setUnUsedMM(rs.getString("unUsedMM"));
				bean.setUsedMM(rs.getString("usedMM"));
			}
		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closeStatement(rs, stmt);
			DBUtil.closeDbConnection(conn);
		}

		return bean;

	}

	/**
	 * Gets the Minuteman Availability /Usage Chart date range.
	 * 
	 * @param minutemanUtilizationBean
	 *            the minuteman utilization bean
	 * @param query
	 *            the query
	 * @param param1
	 *            the param1
	 * @return the date range
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static void getDateRange(
			MinutemanUtilizationBean minutemanUtilizationBean, String query)
			throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {

			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				minutemanUtilizationBean
						.setStartDate((rs.getString("mthStart") == null ? ""
								: rs.getString("mthStart")));
				minutemanUtilizationBean
						.setEndDate((rs.getString("mthEnd") == null ? "" : rs
								.getString("mthEnd")));
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
	}

	/**
	 * Gets the Minuteman Availability /Usage Chart no of records.
	 * 
	 * @param query
	 *            the query
	 * @param param1
	 *            the param1
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param reportName
	 *            the report name
	 * @return the no of records
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static int getNoOfRecords(String query, String param1,
			String startDate, String endDate, String reportName)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + param1 + "," + param1);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int noOfRecords = 0;

		try {
			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, startDate);
			rs = pStmt.executeQuery();
			if (rs.next()) {
				noOfRecords = rs.getInt(1);
			}

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return noOfRecords;
	}

	/**
	 * Gets the max of Minuteman Availability /Usage Chart Y axis value.
	 * 
	 * @param query
	 *            the query
	 * @param param1
	 *            the param1
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param reportName
	 *            the report name
	 * @return the max of mm util avail data
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static int getMaxOfMMUtilAvailData(String query, String startDate,
			String endDate, String reportName) throws SysException,
			AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + startDate + "," + endDate);
		}

		initIlexDS();
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		int maxYAxisValue = 0;
		try {
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);
			pStmt.setString(1, startDate);
			rs = pStmt.executeQuery();
			while (rs.next()) {
				maxYAxisValue = rs.getInt(1);
			}
		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}

		return maxYAxisValue;
	}

	/**
	 * Gets the rs from query for Minuteman Availability /Usage Chart.
	 * 
	 * @param query
	 *            the query
	 * @param param1
	 *            the param1
	 * @param param4
	 *            the param4
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param reportType
	 *            the report type
	 * @return the RS from query
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static List<? extends Stacked2DChartResultSetBean> getRSFromQuery(
			String query, String param4, String startDate, String endDate,
			String reportType) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
			LOGGER.debug("QUERY PARAMETERS->" + startDate + "," + endDate);
		}
		Connection conn = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		BeanProcessor beanProcessor = new BeanProcessor();
		List<? extends Stacked2DChartResultSetBean> beanList;
		try {

			initIlexDS();
			conn = ilexDS.getConnection();
			pStmt = conn.prepareStatement(query);

			pStmt.setString(1, startDate);
			rs = pStmt.executeQuery();
			Stacked2DChartResultSetBean minutemanCostRSBean = new Stacked2DChartResultSetBean();

			beanList = beanProcessor.toBeanList(rs,
					minutemanCostRSBean.getClass());

		} catch (SQLException sqlE) {
			throw new SysException(sqlE);
		} finally {
			DBUtil.closePreparedStatement(rs, pStmt);
			DBUtil.closeDbConnection(conn);
		}
		return beanList;
	}
}
