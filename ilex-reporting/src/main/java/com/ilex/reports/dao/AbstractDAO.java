package com.ilex.reports.dao;

import javax.sql.DataSource;

import com.mind.fw.core.dao.util.DBUtil;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;

/**
 * The Class AbstractDAO.
 */
public abstract class AbstractDAO {

	/** The ilex ds. */
	protected static DataSource ilexDS = null;

	protected static DataSource ilexMainDS = null;

	/**
	 * Inits the ilex ds.
	 * 
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	protected static synchronized void initIlexDS() throws SysException,
			AppException {
		if (ilexDS == null)
			ilexDS = DBUtil.getDataSource("ilex");
	}

	protected static synchronized void initIlexMainDS() throws SysException,
			AppException {
		if (ilexMainDS == null)
			ilexMainDS = DBUtil.getDataSource("ilexMain");
	}

}
