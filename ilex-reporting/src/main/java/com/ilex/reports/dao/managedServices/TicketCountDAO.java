package com.ilex.reports.dao.managedServices;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.ilex.reports.action.operationSummary.DataAccess;
import com.ilex.reports.bean.managedServices.WorkCloseTicketBean;
import com.ilex.reports.dao.AbstractDAO;
import com.mind.fw.core.dao.util.DBUtil;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;

public class TicketCountDAO extends AbstractDAO {

	public static final Logger LOGGER = Logger.getLogger(TicketCountDAO.class);

	/**
	 * Gets the chart details from db.
	 * 
	 * @param query
	 *            the query
	 * @return the chart details
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public List<WorkCloseTicketBean> getChartDetails(String query)
			throws SysException, AppException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("QUERY->" + query);
		}
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<WorkCloseTicketBean> workCloseTicketBeans = new ArrayList<WorkCloseTicketBean>();
		try {

			initIlexMainDS();
			conn = ilexMainDS.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				WorkCloseTicketBean workCloseTicketBean = new WorkCloseTicketBean();
				workCloseTicketBean.setDate(rs.getString("Date"));
				workCloseTicketBean.setInstallationNotes(rs
						.getString("Installation_Notes"));
				workCloseTicketBean.setName(rs.getString("Name"));
				workCloseTicketBean.setTicketClosed(rs
						.getString("Tickets_Closed"));
				workCloseTicketBeans.add(workCloseTicketBean);
			}

		} catch (Exception e) {
			throw new SysException(e);
		} finally {
			DBUtil.closeStatement(rs, stmt);
			DBUtil.closeDbConnection(conn);
		}
		return workCloseTicketBeans;
	}
	
	// get Count details 
		public static  String  getCountDetails( String days) {
			String retparam="";
			String sp = "{ call dbo.sp_worked_closed_count (?)}";
			try {
				
				initIlexMainDS();
				CallableStatement stmt = ilexMainDS.getConnection()
					.prepareCall(sp);
				
				/*stmt.registerOutParameter(1, 12);
				stmt.setInt(2,Integer.parseInt(days));*/
			//	stmt.setInt(1,12);
				stmt.setInt(1,Integer.parseInt(days));
			//	stmt.registerOutParameter("@inparam",java.sql.Types.VARCHAR);
				 

				
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					 {
						 retparam = rs.getString("count");						
					 }
				
				
				}
				
				
				

				stmt.close();
				
			} catch (Exception e) {
				System.out.println(e);
			}
			return retparam;
		}
	
}
