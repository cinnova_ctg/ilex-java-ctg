package com.ilex.reports.logic.laborCost;

public enum LaborCostCategory {

	ALL_LABOR("All Labor"), TECHNICIAN("Technician-All"), ENGINEER(
			"Engineer-All"), CFT2C3("CFT2-C3 (PPS)"), CFT2C4("CFT2-C5 (PPS)"),
	CFT2C5("CFT2-C5 (PPS)"), TECHNICIAN_PVS("Technician-PVS");

	private String text = null;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	private LaborCostCategory(String category) {
		this.text = category;
	}

}
