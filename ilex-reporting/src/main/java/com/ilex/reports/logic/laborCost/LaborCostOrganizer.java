package com.ilex.reports.logic.laborCost;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ilex.reports.bean.laborCost.LaborCostBean;
import com.ilex.reports.common.LabelValue;
import com.ilex.reports.common.ReportStaticData;
import com.ilex.reports.common.multiAxis.bean.ChartXmlAxis;
import com.ilex.reports.common.multiAxis.bean.MultiAxisChartOperation;
import com.ilex.reports.common.multiAxis.bean.MultiAxisChartResultSetBean;
import com.ilex.reports.dao.financialReport.FinancialReportDAO;
import com.ilex.reports.dao.laborCost.LaborCostDAO;
import com.ilex.reports.formbean.laborCost.LaborCostForm;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;
import com.mind.fw.util.PropertyFileUtil;

public class LaborCostOrganizer {

	/** The logger. */
	private static final Logger LOGGER = Logger
			.getLogger(LaborCostOrganizer.class);

	/**
	 * Sets chart details in laborCostBean for Labor Cost Reports(TopLevel and
	 * Technician Labor Cost Reports).
	 * 
	 * @param laborCostBean
	 *            - LaborCostBean.
	 * @param laborCostType
	 *            - String.
	 * @throws SysException
	 *             - SysException.
	 * @throws AppException
	 *             - AppException.
	 */
	public static void setLaborCostChartDetails(LaborCostBean laborCostBean,
			String reportName) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method setLaborCostChartDetails starts for Report Type -."
					+ reportName);
		}

		String param1 = "";
		String param2 = "";
		String param3 = "";
		String queryforNoOfRecords = "";
		String queryForRsBeanList = "";
		String dateRangeMonth = "";
		String noOfXCoordinates = "";
		int noOfRecords = 0;
		Long maxYValue;
		List<? extends MultiAxisChartResultSetBean> rsBeanList = null;

		try {
			noOfXCoordinates = PropertyFileUtil.getAppProperty(reportName
					+ ".prefix")
					+ "daterange.month";

		} catch (Exception e) {
			throw new AppException(e.getMessage());
		}

		if (reportName.equals(LaborCostReportType.SR_PM_LABOR_COST
				.getReportName())) {
			param1 = LaborCostCategory.ALL_LABOR.getText();
			param3 = LaborCostCategory.TECHNICIAN.getText();

			laborCostBean.setLaborCategoryFilter("");
			queryforNoOfRecords = ReportStaticData
					.getBaseQueryForMultiAxisSrPmLaborCost();
			queryForRsBeanList = ReportStaticData
					.getQueryForMultiAxisSrPmLaborCost();

			try {
				dateRangeMonth = ReportStaticData
						.getQueryForSRPMLaborCostDateRangeMonth(PropertyFileUtil
								.getAppProperty(noOfXCoordinates, "11"));
			} catch (Exception e) {
				throw new AppException(e.getMessage());
			}

		} else {
			if (reportName.equals(LaborCostReportType.TOP_LEVEL_LABOR_COST
					.getReportName())) {

				param1 = LaborCostCategory.ALL_LABOR.getText();
				param2 = LaborCostCategory.ENGINEER.getText();
				param3 = LaborCostCategory.TECHNICIAN.getText();

				laborCostBean.setLaborCategoryFilter("");

			} else if (reportName
					.equals(LaborCostReportType.TECHNICIAN_LABOR_COST
							.getReportName())) {

				if (laborCostBean.getLaborCategoryFilter() == null
						|| laborCostBean.getLaborCategoryFilter().equals("")) {
					laborCostBean
							.setLaborCategoryFilter(LaborCostCategory.CFT2C5
									.getText());
				}

				/* Set report section title. */
				if (reportName.equals(LaborCostReportType.TECHNICIAN_LABOR_COST
						.getReportName())) {
					if (laborCostBean.getLaborCategoryFilter().contains("(")) {
						laborCostBean.setReportSubTitle(laborCostBean
								.getLaborCategoryFilter().substring(
										0,
										laborCostBean.getLaborCategoryFilter()
												.indexOf("(")));
					} else {
						laborCostBean.setReportSubTitle(laborCostBean
								.getLaborCategoryFilter());
					}
				}

				param1 = LaborCostCategory.ALL_LABOR.getText();
				param2 = laborCostBean.getLaborCategoryFilter();
				param3 = LaborCostCategory.TECHNICIAN.getText();

			}
			queryforNoOfRecords = ReportStaticData
					.getBaseQueryForMultiAxisLaborCost();
			queryForRsBeanList = ReportStaticData
					.getQueryForMultiAxisLaborCost();
			try {
				dateRangeMonth = ReportStaticData
						.getQueryForLaborCostDateRangeMonth(PropertyFileUtil
								.getAppProperty(noOfXCoordinates, "11"));
			} catch (Exception e) {
				throw new AppException(e.getMessage());
			}
		}

		LaborCostDAO.getDateRange(laborCostBean, dateRangeMonth, param1,
				param2, param3);

		noOfRecords = LaborCostDAO.getNoOfRecords(ReportStaticData
				.getQueryForCountNoOfRecords(queryforNoOfRecords), param1,
				param2, param3, laborCostBean.getStartDate(), laborCostBean
						.getEndDate(), reportName);

		if (reportName.equals(LaborCostReportType.SR_PM_LABOR_COST
				.getReportName())) {
			maxYValue = LaborCostDAO.getMaxOFYForSPRM(ReportStaticData
					.getQueryFormaxYAxisCategory(ReportStaticData
							.getBaseQueryForMultiAxisSrPmLaborCost()), param1,
					param2, param3, laborCostBean.getStartDate(), laborCostBean
							.getEndDate());
		} else {
			maxYValue = LaborCostDAO.getMaxOfLaborCostData(
					ReportStaticData.getQueryForMaxOfLaborCost(), param1,
					param2, param3, laborCostBean.getStartDate(),
					laborCostBean.getEndDate());
		}

		// get result set from desired query.
		rsBeanList = LaborCostDAO.getRSFromQuery(queryForRsBeanList, param1,
				param2, param3, laborCostBean.getStartDate(),
				laborCostBean.getEndDate(), reportName);

		ArrayList<String> coOrdinatesDates = new ArrayList<String>();

		String xCoordinates = "";
		try {
			xCoordinates = PropertyFileUtil.getAppProperty(noOfXCoordinates,
					"11");
		} catch (Exception e) {
			throw new AppException();
		}

		laborCostBean.setxCordinateDates(LaborCostDAO.setCompleteMonthData(
				laborCostBean.getStartDate(),
				Integer.parseInt(xCoordinates) + 1));

		coOrdinatesDates = laborCostBean.getxCordinateDates();

		ArrayList<ChartXmlAxis> chartXmlAxisList;
		try {
			chartXmlAxisList = MultiAxisChartOperation
					.getAxisListForMultiAxisChart(rsBeanList, coOrdinatesDates,
							noOfRecords, Integer.parseInt(xCoordinates) + 1,
							reportName);
		} catch (NumberFormatException e) {
			throw new AppException(e.getMessage());
		} catch (Exception e) {
			throw new AppException(e.getMessage());
		}
		laborCostBean.setAxisList(chartXmlAxisList);

		maxYValue = MultiAxisChartOperation.getClosedIndexValue(maxYValue);

		/*
		 * Create a chart XML and then set it into bean.
		 */
		try {
			laborCostBean.setGraphXML(MultiAxisChartOperation.createXmlChart(
					laborCostBean.getAxisList(),
					laborCostBean.getxCordinateDates(), reportName, maxYValue,
					null));
		} catch (Exception e) {
			throw new AppException();
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method setLaborCostChartDetails ends for Report Type -."
					+ reportName);
		}

	}

	/**
	 * Sets the Labor Cost Form from Labor Cost Bean.
	 * 
	 * @param laborCostForm
	 *            - LaborCostForm.
	 * @param laborCostBean
	 *            - LaborCostBean.
	 * @throws SysException
	 *             the SysException
	 */
	public static void setFormFromLaborCostBean(LaborCostForm laborCostForm,
			LaborCostBean laborCostBean) throws SysException {
		LaborCostConvertor.setFormFromLaborCostBean(laborCostForm,
				laborCostBean);
	}

	/**
	 * Sets the Labor Cost Bean from Labor Cost Form.
	 * 
	 * @param laborCostBean
	 *            - LaborCostBean.
	 * @param laborCostForm
	 *            - laborCostForm.
	 * @throws SysException
	 *             - SysException.
	 */
	public static void setBeanFromLaborCostForm(LaborCostBean laborCostBean,
			LaborCostForm laborCostForm) throws SysException {
		LaborCostConvertor.setBeanFromLaborCostForm(laborCostBean,
				laborCostForm);
	}

	/**
	 * Populate Labor Category filter on Technician Labor Cost Report.
	 * 
	 * @param laborCostBean
	 *            - LaborCostBean.
	 * @param laborCostType
	 *            - String.
	 * @throws SysException
	 *             - SysException.
	 * @throws AppException
	 *             - AppException.
	 */
	public static void populateLaborCategory(LaborCostBean laborCostBean,
			String laborCostType) throws SysException, AppException {
		laborCostBean.setLaborCategoryList(LaborCostDAO
				.populateLaborCategory(ReportStaticData
						.getQueryForLaborCategory()));
	}

	public static void populateLaborType(LaborCostBean laborCostBean)
			throws SysException, AppException {
		ArrayList<LabelValue> laborCategories = new ArrayList<LabelValue>();
		LabelValue laborCategory;
		laborCategory = new LabelValue(LaborType.LABOR_ENGINER.getType(),
				LaborType.LABOR_ENGINER.getType());
		laborCategories.add(0, laborCategory);
		laborCategory = new LabelValue(LaborType.LABOR_TECHNICIAN.getType(),
				LaborType.LABOR_TECHNICIAN.getType());
		laborCategories.add(1, laborCategory);
		laborCostBean.setLaborTypeList(laborCategories);
	}

	/**
	 * Sets the Labor Cost Form from Sr PM Labor Cost Bean.
	 * 
	 * @param laborCostForm
	 *            - LaborCostForm.
	 * @param laborCostSrPmBean
	 *            - LaborCostSrPmBean.
	 * @throws SysException
	 *             the SysException
	 */
	public static void setFormFromSrPmLaborCostBean(
			LaborCostForm laborCostForm, LaborCostBean laborCostSrPmBean)
			throws SysException {
		LaborCostConvertor.setFormFromSrPmLaborCostBean(laborCostForm,
				laborCostSrPmBean);
	}

	public static void setLaborByCategoryDetail(LaborCostBean laborCostBean,
			String reportName) throws AppException, SysException {

		String queryforNoOfRecords = "";
		String queryForRsBeanList = "";
		String dateRangeMonth = "";
		String noOfXCoordinates = "";
		String queryForMAxYaxisValue = "";
		int noOfRecords = 0;
		List<? extends MultiAxisChartResultSetBean> rsBeanList = null;
		FinancialReportDAO financialReportDao = new FinancialReportDAO();

		if (laborCostBean.getLaborType() == null
				|| ("").equals(laborCostBean.getLaborType())) {
			laborCostBean.setLaborType(LaborType.LABOR_TECHNICIAN.getType());
		}
		String paramType = laborCostBean.getLaborType();

		try {
			noOfXCoordinates = PropertyFileUtil.getAppProperty(reportName
					+ ".prefix")
					+ "daterange.month";
			dateRangeMonth = ReportStaticData
					.getQueryForCategoryDateRangeMonth(PropertyFileUtil
							.getAppProperty(noOfXCoordinates, "11"), paramType);

		} catch (Exception e) {
			throw new AppException(e.getMessage());
		}

		/*
		 * Get the filter criteria and set the parameters according to it.
		 */

		queryforNoOfRecords = ReportStaticData
				.getQueryForCountNoOfRecords(ReportStaticData
						.getQueryForCategoryChart(paramType));
		queryForMAxYaxisValue = ReportStaticData
				.getQueryFormaxYAxisCategory(ReportStaticData
						.getQueryForCategoryChart(paramType));

		queryForRsBeanList = ReportStaticData
				.getOrderedQueryForCategoryChart(paramType);

		LaborCostDAO.getDateRangeFromQuery(laborCostBean, dateRangeMonth);

		noOfRecords = LaborCostDAO.getNoOfRecordsForCategory(
				queryforNoOfRecords, laborCostBean.getStartDate(),
				laborCostBean.getEndDate(), paramType);

		// get result set from desired query.
		rsBeanList = LaborCostDAO.getRSFromQuery(queryForRsBeanList,
				laborCostBean.getStartDate(), laborCostBean.getEndDate(),
				paramType);

		ArrayList<String> coOrdinatesDates = new ArrayList<String>();

		String xCoordinates = "";
		try {
			xCoordinates = PropertyFileUtil.getAppProperty(noOfXCoordinates,
					"11");
		} catch (Exception e) {
			throw new AppException();
		}

		laborCostBean.setxCordinateDates(LaborCostDAO.setCompleteMonthData(
				laborCostBean.getStartDate(),
				Integer.parseInt(xCoordinates) + 1));

		coOrdinatesDates = laborCostBean.getxCordinateDates();

		ArrayList<ChartXmlAxis> chartXmlAxisList;
		try {
			chartXmlAxisList = MultiAxisChartOperation
					.getAxisListForMultiAxisChart(rsBeanList, coOrdinatesDates,
							noOfRecords, Integer.parseInt(xCoordinates) + 1,
							reportName);
		} catch (NumberFormatException e) {
			throw new AppException(e.getMessage());
		} catch (Exception e) {
			throw new AppException(e.getMessage());
		}

		laborCostBean.setAxisList(chartXmlAxisList);
		Long maxYValue = financialReportDao.getMaxYAxisValueForFinance(
				queryForMAxYaxisValue, laborCostBean.getStartDate(),
				laborCostBean.getEndDate());

		maxYValue = MultiAxisChartOperation.getClosedIndexValue(maxYValue);

		String displayName = "";
		if (paramType.equals(LaborType.LABOR_ENGINER.getType())) {
			displayName = "Engineer";
		} else {
			displayName = "Technician";
		}
		String caption = "Labor Cost - " + displayName
				+ " Criticality Breakout (PVS)";

		/*
		 * Create a chart XML and then set it into bean.
		 */
		try {
			laborCostBean.setGraphXML(MultiAxisChartOperation.createXmlChart(
					laborCostBean.getAxisList(),
					laborCostBean.getxCordinateDates(), reportName, maxYValue,
					caption));
		} catch (Exception e) {
			throw new AppException();
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method setLaborCostChartDetails ends for Report Type -."
					+ reportName);
		}

	}
}
