package com.ilex.reports.logic.laborCost;

public enum LaborType {
	LABOR_ENGINER("Engineers"), LABOR_TECHNICIAN("Technicians");

	private String type = null;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private LaborType(String type) {
		this.type = type;
	}
}
