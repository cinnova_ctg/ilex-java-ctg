package com.ilex.reports.logic.managedServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.ilex.reports.bean.managedServices.TicketCountBean;
import com.ilex.reports.bean.managedServices.WorkCloseTicketBean;
import com.ilex.reports.common.ReportStaticData;
import com.ilex.reports.common.multiAxis.bean.ChartXmlDataSet;
import com.ilex.reports.common.multiAxis.bean.ChartXmlSetValue;
import com.ilex.reports.common.multiAxis.bean.MultiAxisChartOperation;
import com.ilex.reports.dao.managedServices.TicketCountDAO;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;

public class TicketCountOrganizer {
	public static final Logger LOGGER = Logger
			.getLogger(TicketCountOrganizer.class);

	/**
	 * Gets the chart details.
	 * 
	 * @return the chart details
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public TicketCountBean getChartDetails() throws SysException, AppException {
		TicketCountBean ticketCountBean = new TicketCountBean();
		TicketCountDAO ticketCountDAO = new TicketCountDAO();
		try {

			List<WorkCloseTicketBean> workCloseTicketBeanList = new ArrayList<WorkCloseTicketBean>();
			workCloseTicketBeanList = ticketCountDAO
					.getChartDetails(ReportStaticData.getQueryForTicketCount());

			Map<String, WorkCloseTicketBean> workCloseTicketBeanMap = new HashMap<String, WorkCloseTicketBean>();
			for (WorkCloseTicketBean workCloseTicketBean : workCloseTicketBeanList) {
				String key = workCloseTicketBean.getDate()
						+ workCloseTicketBean.getName();
				workCloseTicketBeanMap.put(key, workCloseTicketBean);
			}

			Set<String> names = getXCordinateValues(workCloseTicketBeanList);

			ticketCountBean.setxCordinates(names);
			Set<String> datesList = getDatesList(workCloseTicketBeanList);
			List<ChartXmlDataSet> chartXmlDataSets = new ArrayList<ChartXmlDataSet>();
			String[] color = { "4F81BE", "7DA9DF", "C1504D", "E07E7B" };
			for (String date : datesList) {
				chartXmlDataSets.add(getWorkedtTicketDataset(date,
						workCloseTicketBeanMap, names));
				chartXmlDataSets.add(getClosedtTicketDataset(date,
						workCloseTicketBeanMap, names));

			}
			int colorCounter = 0;
			for (ChartXmlDataSet chartXmlDataSet : chartXmlDataSets) {
				chartXmlDataSet.setColor(color[colorCounter % 4]);
				colorCounter++;
			}
			ticketCountBean.setChartXmlDataSets(chartXmlDataSets);
			ticketCountBean.setGraphXML(MultiAxisChartOperation
					.createXmlForTicketCount(chartXmlDataSets,
							ticketCountBean.getxCordinates()));

		} catch (Exception e) {
			throw new SysException(e);
		}
		return ticketCountBean;

	}

	/**
	 * Gets the x cordinate values.
	 * 
	 * @param workCloseTicketBeanList
	 *            the work close ticket bean list
	 * @return the x cordinate values
	 */
	public Set<String> getXCordinateValues(
			List<WorkCloseTicketBean> workCloseTicketBeanList) {
		Set<String> xCordinates = new LinkedHashSet<String>();
		for (WorkCloseTicketBean workCloseTicketBean : workCloseTicketBeanList) {
			xCordinates.add(workCloseTicketBean.getName());
		}

		return xCordinates;
	}

	/**
	 * Gets the dates list.
	 * 
	 * @param workCloseTicketBeanList
	 *            the work close ticket bean list
	 * @return the dates list
	 */
	public Set<String> getDatesList(
			List<WorkCloseTicketBean> workCloseTicketBeanList) {
		Set<String> dates = new LinkedHashSet<String>();
		for (WorkCloseTicketBean workCloseTicketBean : workCloseTicketBeanList) {
			dates.add(workCloseTicketBean.getDate());
		}
		return dates;

	}

	/**
	 * Gets the workedt ticket dataset.
	 * 
	 * @param date
	 *            the date
	 * @param workCloseTicketBeanList
	 *            the work close ticket bean list
	 * @return the workedt ticket dataset
	 */
	public ChartXmlDataSet getWorkedtTicketDataset(String date,
			Map<String, WorkCloseTicketBean> workCloseTicketBeanMap,
			Set<String> names) {
		ChartXmlDataSet chartXmlDataSet = new ChartXmlDataSet();
		chartXmlDataSet.setSeriesName(date + " Worked");
		List<ChartXmlSetValue> chartXmlSetValues = new ArrayList<ChartXmlSetValue>();

		for (String name : names) {
			String key = date + name;
			ChartXmlSetValue chartXmlSetValue = new ChartXmlSetValue();
			if (workCloseTicketBeanMap.get(key) != null) {
				WorkCloseTicketBean workCloseTicketBean = workCloseTicketBeanMap
						.get(key);
				chartXmlSetValue.setValue(workCloseTicketBean
						.getInstallationNotes());
			} else {
				chartXmlSetValue.setValue("0");
			}
			chartXmlSetValues.add(chartXmlSetValue);

		}

		chartXmlDataSet.setSet(chartXmlSetValues);
		return chartXmlDataSet;

	}

	/**
	 * Gets the closedt ticket dataset.
	 * 
	 * @param date
	 *            the date
	 * @param workCloseTicketBeanList
	 *            the work close ticket bean list
	 * @return the closedt ticket dataset
	 */
	public ChartXmlDataSet getClosedtTicketDataset(String date,
			Map<String, WorkCloseTicketBean> workCloseTicketBeanMap,
			Set<String> names) {
		ChartXmlDataSet chartXmlDataSet = new ChartXmlDataSet();
		chartXmlDataSet.setSeriesName(date + " Closed");
		List<ChartXmlSetValue> chartXmlSetValues = new ArrayList<ChartXmlSetValue>();

		for (String name : names) {
			String key = date + name;
			ChartXmlSetValue chartXmlSetValue = new ChartXmlSetValue();
			if (workCloseTicketBeanMap.get(key) != null) {
				WorkCloseTicketBean workCloseTicketBean = workCloseTicketBeanMap
						.get(key);
				chartXmlSetValue
						.setValue(workCloseTicketBean.getTicketClosed());
			} else {
				chartXmlSetValue.setValue("0");
			}
			chartXmlSetValues.add(chartXmlSetValue);

		}
		chartXmlDataSet.setSet(chartXmlSetValues);
		return chartXmlDataSet;

	}
}
