package com.ilex.reports.logic.minutemanSpeedpayReport;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public enum MinutemanSpeedpayCatagory {
	ALL("ALL"), PROJECTS("PROJECTS"), NETMEDX("NETMEDX");

	private String text = null;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	private MinutemanSpeedpayCatagory(String category) {
		this.text = category;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
