package com.ilex.reports.logic.financialReport;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class JobOwnerResultSetBean {
	private String jobOwnerId = null;
	private String jobOwner = null;
	private String proFormaExpense = null;
	private String pOExpense = null;
	private String corExpense = null;
	private String totalExpense = null;
	private String expence = null;
	private String revenue = null;
	private String proFormaVGPM = null;
	private String VGPM = null;
	private String deltaVGPM = null;
	private String occurrences = null;

	public String getpOExpense() {
		return pOExpense;
	}

	public void setpOExpense(String pOExpense) {
		this.pOExpense = pOExpense;
	}

	public String getCorExpense() {
		return corExpense;
	}

	public void setCorExpense(String corExpense) {
		this.corExpense = corExpense;
	}

	public String getTotalExpense() {
		return totalExpense;
	}

	public void setTotalExpense(String totalExpense) {
		this.totalExpense = totalExpense;
	}

	public String getDeltaVGPM() {
		return deltaVGPM;
	}

	public void setDeltaVGPM(String deltaVGPM) {
		this.deltaVGPM = deltaVGPM;
	}

	public String getJobOwner() {
		return jobOwner;
	}

	public void setJobOwner(String jobOwner) {
		this.jobOwner = jobOwner;
	}

	public String getJobOwnerId() {
		return jobOwnerId;
	}

	public void setJobOwnerId(String jobOwnerId) {
		this.jobOwnerId = jobOwnerId;
	}

	public String getProFormaExpense() {
		return proFormaExpense;
	}

	public void setProFormaExpense(String proFormaExpense) {
		this.proFormaExpense = proFormaExpense;
	}

	public String getExpence() {
		return expence;
	}

	public void setExpence(String expence) {
		this.expence = expence;
	}

	public String getProFormaVGPM() {
		return proFormaVGPM;
	}

	public void setProFormaVGPM(String proFormaVGPM) {
		this.proFormaVGPM = proFormaVGPM;
	}

	public String getVGPM() {
		return VGPM;
	}

	public void setVGPM(String vGPM) {
		VGPM = vGPM;
	}

	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	public String getOccurrences() {
		return occurrences;
	}

	public void setOccurrences(String occurrences) {
		this.occurrences = occurrences;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
