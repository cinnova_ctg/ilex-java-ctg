package com.ilex.reports.logic.financialReport;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.ilex.reports.bean.financialReport.RevenueExpenseBean;
import com.ilex.reports.formbean.financialReport.RevenueExpenseForm;
import com.mind.fw.lang.SysException;

public class RevenueExpenseConvertor {
	public static void setFormFromRevenueExpenseBean(
			RevenueExpenseForm revenueExpenseForm,
			RevenueExpenseBean revenueExpenseBean) throws SysException {

		try {
			BeanUtils.copyProperties(revenueExpenseForm, revenueExpenseBean);
		} catch (IllegalAccessException e) {
			throw new SysException(e);
		} catch (InvocationTargetException e) {
			throw new SysException(e);
		}
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
