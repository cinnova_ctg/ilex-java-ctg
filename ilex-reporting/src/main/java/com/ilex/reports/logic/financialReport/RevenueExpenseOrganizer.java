package com.ilex.reports.logic.financialReport;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.log4j.Logger;

import com.ilex.reports.bean.financialReport.RevenueExpenseBean;
import com.ilex.reports.common.ReportStaticData;
import com.ilex.reports.common.multiAxis.bean.ChartXmlAxis;
import com.ilex.reports.common.multiAxis.bean.MultiAxisChartOperation;
import com.ilex.reports.common.multiAxis.bean.MultiAxisChartResultSetBean;
import com.ilex.reports.dao.financialReport.FinancialReportDAO;
import com.ilex.reports.dao.laborCost.LaborCostDAO;
import com.ilex.reports.formbean.financialReport.RevenueExpenseForm;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;
import com.mind.fw.util.PropertyFileUtil;

public class RevenueExpenseOrganizer {
	public static final Logger LOGGER = Logger
			.getLogger(RevenueExpenseOrganizer.class);
	Calendar calendar = Calendar.getInstance();

	/**
	 * Gets the financial multi axis chart details.
	 * 
	 * @param reportName
	 *            the report name
	 * @param jobOwnerName
	 *            the job owner name
	 * 
	 * @return the financial multi axis chart details
	 * 
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public RevenueExpenseBean getFinancialMultiAxisChartDetails(
			String reportName, String jobFilterType, String jobOwnerName,
			String jobOwnerId) throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method setLaborCostChartDetails starts for Report Type -."
					+ reportName);
		}
		FinancialReportDAO financialReportDao = new FinancialReportDAO();
		RevenueExpenseBean revenueExpenseBean = null;

		String parameterRole1 = FinancialReportCategory.ALL.getText();
		String parameterRole2 = "";
		String parameterjobOwnerName = "";
		String majorClassification = "";
		String minorClassification = "";
		String queryforNoOfRecords = "";
		String queryForRsBeanList = "";
		String noOfXCoordinates = "";
		Long maxYValue = null;
		int noOfRecords = 0;
		List<? extends MultiAxisChartResultSetBean> rsBeanList = null;

		String dateRangeMonth = "";
		try {
			noOfXCoordinates = PropertyFileUtil.getAppProperty(reportName
					+ ".prefix")
					+ "daterange.month";
			if (!reportName.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT
					.getReportName())) {
				dateRangeMonth = ReportStaticData
						.getQueryForRevenueDateRangeMonth(PropertyFileUtil
								.getAppProperty(noOfXCoordinates, "11"));
			}
		} catch (Exception e) {
			throw new SysException(e);
		}

		if (reportName
				.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_BySRPM
						.getReportName())) {
			parameterRole2 = FinancialReportCategory.SPM.getText();
			queryforNoOfRecords = ReportStaticData
					.getQueryForCountNoOfRecords(ReportStaticData
							.getCountQueryForFinancialReportBySRPM());
			queryForRsBeanList = ReportStaticData
					.getQueryForFinanceRevenueBySRPMBeanList();
		} else if (reportName
				.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT
						.getReportName())) {
			try {
				jobFilterType = jobFilterType.replaceAll(" ", "");
				majorClassification = PropertyFileUtil
						.getAppProperty(jobFilterType + ".major.classification");
				minorClassification = PropertyFileUtil
						.getAppProperty(jobFilterType + ".minor.classification");

				dateRangeMonth = ReportStaticData
						.getQueryForRevenueFinancialDateRangeMonth(
								PropertyFileUtil.getAppProperty(
										noOfXCoordinates, "11"), jobFilterType,
								reportName);
			} catch (Exception e1) {

				throw new SysException(e1);
			}

			queryforNoOfRecords = ReportStaticData
					.getCountQueryForRevenueFinancialReport(jobFilterType,
							reportName);
			queryForRsBeanList = ReportStaticData
					.getQueryForRevenueFinanceReportBeanList(jobFilterType);

		} else if (reportName
				.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
						.getReportName())) {
			parameterjobOwnerName = jobOwnerName;
			dateRangeMonth = ReportStaticData
					.getQueryForRevenueFinancialDateRangeMonth("11",
							jobFilterType, reportName);
			// queryforNoOfRecords = ReportStaticData
			// .getQueryForCountNoOfRecords(ReportStaticData
			// .getCountQueryForFinancialReportByJobOwner());
			queryforNoOfRecords = ReportStaticData
					.getCountQueryForRevenueFinancialReport(jobFilterType,
							reportName);
			// queryForRsBeanList = ReportStaticData
			// .getQueryForFinanceRevenueByJobOwnerBeanList();
			queryForRsBeanList = ReportStaticData
					.getQueryForRevenueExpenseByJobOwnerBeanList();

		}
		if (!reportName.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT
				.getReportName())
				&& !reportName
						.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
								.getReportName())) {
			revenueExpenseBean = financialReportDao.getDateRange(
					RevenueExpenseBean.class, dateRangeMonth, parameterRole1,
					parameterRole2);

			noOfRecords = financialReportDao.getNoOfRecords(
					queryforNoOfRecords, revenueExpenseBean.getStartDate(),
					revenueExpenseBean.getEndDate(), parameterjobOwnerName,
					reportName);

			// get result set from desired query.
			rsBeanList = financialReportDao.getRSFromQuery(queryForRsBeanList,
					revenueExpenseBean.getStartDate(),
					revenueExpenseBean.getEndDate(), parameterjobOwnerName,
					reportName);
		} else if (reportName
				.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
						.getReportName())) {
			revenueExpenseBean = financialReportDao
					.getDateRangeForRevenueExpence(RevenueExpenseBean.class,
							dateRangeMonth, majorClassification, reportName);

			noOfRecords = financialReportDao
					.getNoOfRecordsRevenueFinancialReport(queryforNoOfRecords,
							revenueExpenseBean.getStartDate(),
							revenueExpenseBean.getEndDate(),
							majorClassification, minorClassification,
							reportName, jobOwnerId);

			// get result set from desired query.
			rsBeanList = financialReportDao.getRevenueFinancialRSFromQuery(
					queryForRsBeanList, revenueExpenseBean.getStartDate(),
					revenueExpenseBean.getEndDate(), majorClassification,
					minorClassification, reportName, jobOwnerId);
		} else {
			revenueExpenseBean = financialReportDao
					.getDateRangeForRevenueExpence(RevenueExpenseBean.class,
							dateRangeMonth, majorClassification, reportName);

			noOfRecords = financialReportDao
					.getNoOfRecordsRevenueFinancialReport(queryforNoOfRecords,
							revenueExpenseBean.getStartDate(),
							revenueExpenseBean.getEndDate(),
							majorClassification, minorClassification,
							reportName, jobOwnerId);

			// get result set from desired query.
			rsBeanList = financialReportDao.getRevenueFinancialRSFromQuery(
					queryForRsBeanList, revenueExpenseBean.getStartDate(),
					revenueExpenseBean.getEndDate(), majorClassification,
					minorClassification, reportName, jobOwnerId);
			revenueExpenseBean.setRevenueExpenceList(financialReportDao
					.getRevenueFinancialRSFromQuery1(queryForRsBeanList,
							revenueExpenseBean.getStartDate(),
							revenueExpenseBean.getEndDate(),
							majorClassification, minorClassification,
							reportName, jobOwnerId));
		}
		ArrayList<String> coOrdinatesDates = new ArrayList<String>();

		String xCoordinates = "";
		try {
			xCoordinates = PropertyFileUtil.getAppProperty(noOfXCoordinates,
					"11");
		} catch (Exception e) {
			throw new SysException(e);
		}

		revenueExpenseBean.setxCordinateDates(LaborCostDAO
				.setCompleteMonthData(revenueExpenseBean.getStartDate(),
						Integer.parseInt(xCoordinates) + 1));

		coOrdinatesDates = revenueExpenseBean.getxCordinateDates();

		ArrayList<ChartXmlAxis> chartXmlAxisList;
		try {
			if (!reportName.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT
					.getReportName())) {
				chartXmlAxisList = MultiAxisChartOperation
						.getAxisListForMultiAxisChart(rsBeanList,
								coOrdinatesDates, noOfRecords,
								Integer.parseInt(xCoordinates) + 1, reportName);
			} else {
				chartXmlAxisList = MultiAxisChartOperation
						.getRevenueExpenceAxisListForMultiAxisChart(rsBeanList,
								coOrdinatesDates, noOfRecords,
								Integer.parseInt(xCoordinates) + 1, reportName,
								revenueExpenseBean.getEndDate(),
								majorClassification, minorClassification,
								revenueExpenseBean.getRevenueExpenceList());
			}
		} catch (NumberFormatException ne) {
			throw new SysException(ne);
		} catch (Exception e) {
			throw new SysException(e);
		}
		revenueExpenseBean.setAxisList(chartXmlAxisList);
		if (!reportName.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT
				.getReportName())
				&& !reportName
						.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
								.getReportName())) {
			maxYValue = financialReportDao.getMaxYAxisValueForFinance(
					ReportStaticData.getQueryForMaxYAxisFinance(),
					revenueExpenseBean.getStartDate(),
					revenueExpenseBean.getEndDate());
		} else if (reportName
				.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
						.getReportName())) {
			maxYValue = financialReportDao.getMaxYAxisValueForRevenueFinance(
					ReportStaticData.getQueryForMaxYAxisRevenueFinance(
							jobFilterType, reportName), jobOwnerId,
					revenueExpenseBean.getEndDate(), majorClassification,
					minorClassification, reportName);
		} else {
			maxYValue = financialReportDao.getMaxYAxisValueForRevenueFinance(
					ReportStaticData.getQueryForMaxYAxisRevenueFinance(
							jobFilterType, reportName), jobOwnerId,
					revenueExpenseBean.getEndDate(), majorClassification,
					minorClassification, reportName);
		}
		long closeIndex = 1;
		if (reportName
				.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_BySRPM
						.getReportName())
				|| reportName
						.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT
								.getReportName())
				|| reportName
						.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
								.getReportName())) {
			maxYValue = MultiAxisChartOperation.getClosedIndexValue(maxYValue);
		} else {
			maxYValue = null;
		}
		String caption = "";

		if (jobOwnerName != null && !jobOwnerName.equals("")) {
			caption = FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner_CAPTION
					.getReportName() + " " + jobOwnerName;
		}

		/*
		 * Create a chart XML and then set it into bean.
		 */
		try {
			revenueExpenseBean.setGraphXML(MultiAxisChartOperation
					.createXmlChart(revenueExpenseBean.getAxisList(),
							revenueExpenseBean.getxCordinateDates(),
							reportName, maxYValue, caption));

		} catch (Exception e) {
			throw new SysException(e);
		}
		return revenueExpenseBean;

	}

	/**
	 * Sets the form from revenue expense bean.
	 * 
	 * @param revenueExpenseForm
	 *            the revenue expense form
	 * @param revenueExpenseBean
	 *            the revenue expense bean
	 * @throws SysException
	 *             the sys exception
	 */
	public void setFormFromRevenueExpenseBean(
			RevenueExpenseForm revenueExpenseForm,
			RevenueExpenseBean revenueExpenseBean) throws SysException {
		RevenueExpenseConvertor.setFormFromRevenueExpenseBean(
				revenueExpenseForm, revenueExpenseBean);
	}

	public void setFormFromJobOwnerBean(RevenueExpenseForm jobOwnerForm,
			RevenueExpenseBean jobOwnerBean, HttpServletRequest request,
			HttpServletResponse responce) throws SysException {
		try {
			BeanUtils.copyProperties(jobOwnerForm, jobOwnerBean);
			jobOwnerForm.setJobOwnerName(request.getParameter("jobOwnerName"));
			jobOwnerForm.setJobOwnerID(request.getParameter("jobOwnerID"));
			jobOwnerForm.setAccountName(request.getParameter("accountName"));
			jobOwnerForm.setSelectedSummaryPage((String) request
					.getAttribute("secondLevelReportName"));
			jobOwnerForm.setJobMonth(request.getParameter("month"));

		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setFormFromProjectManagerBean(
			RevenueExpenseForm projectManagerForm,
			RevenueExpenseBean projectManagerBean, HttpServletRequest request,
			HttpServletResponse responce) throws SysException {
		try {
			BeanUtils.copyProperties(projectManagerForm, projectManagerBean);
			projectManagerForm.setJobOwnerName(request
					.getParameter("projectManagerName"));
			projectManagerForm.setJobOwnerID(request
					.getParameter("projectManagerID"));
			projectManagerForm.setAccountName(request
					.getParameter("accountName"));
			projectManagerForm.setSelectedSummaryPage((String) request
					.getAttribute("secondLevelReportName"));
			projectManagerForm.setJobMonth(request.getParameter("month"));

		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setFormFromCustomerBean(RevenueExpenseForm customerForm,
			RevenueExpenseBean customerBean, HttpServletRequest request,
			HttpServletResponse responce) throws SysException {
		try {
			BeanUtils.copyProperties(customerForm, customerBean);
			customerForm.setCustomerName(request.getParameter("customerName"));
			customerForm.setCustomerID(request.getParameter("customerID"));
			customerForm.setAppendixName(request.getParameter("appendixName"));
			customerForm.setSelectedSummaryPage((String) request
					.getAttribute("secondLevelReportName"));
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Sets the form from bdm bean.
	 * 
	 * @param jobOwnerForm
	 *            the job owner form
	 * @param jobOwnerBean
	 *            the job owner bean
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @throws SysException
	 *             the sys exception
	 */
	public void setFormFromBDMBean(RevenueExpenseForm jobOwnerForm,
			RevenueExpenseBean jobOwnerBean, HttpServletRequest request,
			HttpServletResponse responce) throws SysException {
		try {
			BeanUtils.copyProperties(jobOwnerForm, jobOwnerBean);
			jobOwnerForm.setBdmName(request.getParameter("bdmName"));
			jobOwnerForm.setBdmID(request.getParameter("bdmID"));
			jobOwnerForm.setAccountName(request.getParameter("accountName"));
			jobOwnerForm.setSelectedSummaryPage((String) request
					.getAttribute("secondLevelReportName"));
			jobOwnerForm.setJobMonth(request.getParameter("month"));

		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Sets the form from spm bean.
	 * 
	 * @param jobOwnerForm
	 *            the job owner form
	 * @param jobOwnerBean
	 *            the job owner bean
	 * @param request
	 *            the request
	 * @param responce
	 *            the responce
	 * @throws SysException
	 *             the sys exception
	 */
	public void setFormFromSPMBean(RevenueExpenseForm jobOwnerForm,
			RevenueExpenseBean jobOwnerBean, HttpServletRequest request,
			HttpServletResponse responce) throws SysException {
		try {
			BeanUtils.copyProperties(jobOwnerForm, jobOwnerBean);
			jobOwnerForm.setSpmName(request.getParameter("spmName"));
			jobOwnerForm.setSpmID(request.getParameter("spmID"));
			jobOwnerForm.setAccountName(request.getParameter("accountName"));
			jobOwnerForm.setSelectedSummaryPage((String) request
					.getAttribute("secondLevelReportName"));
			jobOwnerForm.setJobMonth(request.getParameter("month"));
			// jobOwnerForm.setGraphXML(graphXML);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Sets the project manager details.
	 * 
	 * @param param
	 *            the param
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param classfication
	 *            the classfication
	 * @return the revenue expense bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public RevenueExpenseBean setProjectManagerDetails(String param,
			String fromDate, String toDate, String[] classfication)
			throws SysException, AppException {
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		FinancialReportDAO financialReportDao = new FinancialReportDAO();
		/*
		 * Get JobOwner List.
		 */
		revenueExpenseBean.setProjectManagerList(financialReportDao
				.getProjectManagerRSFromQuery(ReportStaticData
						.getQueryForRevenueProjectManagerDetails(param,
								fromDate, toDate, classfication)));

		return revenueExpenseBean;
	}

	/**
	 * Sets the job owners details.
	 * 
	 * @param param
	 *            the param
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param classfication
	 *            the classfication
	 * @return the revenue expense bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public RevenueExpenseBean setJobOwnersDetails(String param,
			String fromDate, String toDate, String[] classfication)
			throws SysException, AppException {
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		FinancialReportDAO financialReportDao = new FinancialReportDAO();
		/*
		 * Get JobOwner List.
		 */
		revenueExpenseBean.setJobOwnerList(financialReportDao
				.getJobOwnerRSFromQuery(ReportStaticData
						.getQueryForRevenueJobOwnersDetails(param, fromDate,
								toDate, classfication)));

		return revenueExpenseBean;
	}

	public RevenueExpenseBean setCustomerDetails(String param, String fromDate,
			String toDate, String[] classfication) throws SysException,
			AppException {
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		FinancialReportDAO financialReportDao = new FinancialReportDAO();
		/*
		 * Get Customer List.
		 */
		revenueExpenseBean.setCustomerList(financialReportDao
				.getCustomerRSFromQuery(ReportStaticData
						.getQueryForRevenueCustomerDetails(param, fromDate,
								toDate, classfication)));

		return revenueExpenseBean;
	}

	/**
	 * Sets the senior project manager details.
	 * 
	 * @param param
	 *            the param
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param classfication
	 *            the classfication
	 * @param revenueExpenseForm
	 *            the revenue expense form
	 * @return the revenue expense bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public RevenueExpenseBean setSeniorProjectManagerDetails(String param,
			String fromDate, String toDate, String[] classfication,
			RevenueExpenseForm revenueExpenseForm) throws SysException,
			AppException {
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		FinancialReportDAO financialReportDao = new FinancialReportDAO();
		String quarter;
		String defaultValue = "firstTime";
		/*
		 * Get JobOwner List.
		 */
		if (defaultValue.equalsIgnoreCase(param)) {
			int monthNo = calendar.get(Calendar.MONTH);
			if (monthNo >= 0 && monthNo <= 2) {
				int previousYear = calendar.get(Calendar.YEAR) - 1;
				fromDate = "09/01/" + previousYear;
				toDate = "12/31/" + previousYear;
				revenueExpenseForm.setQuarter("4");
				revenueExpenseForm.setYear("" + previousYear);

			} else if (monthNo >= 3 && monthNo <= 5) {
				fromDate = "01/01/" + calendar.get(Calendar.YEAR);
				toDate = "03/31/" + calendar.get(Calendar.YEAR);
				revenueExpenseForm.setQuarter("1");
				revenueExpenseForm.setYear("" + calendar.get(Calendar.YEAR));
			} else if (monthNo >= 6 && monthNo <= 8) {
				fromDate = "04/01/" + calendar.get(Calendar.YEAR);
				toDate = "06/30/" + calendar.get(Calendar.YEAR);
				revenueExpenseForm.setQuarter("2");
				revenueExpenseForm.setYear("" + calendar.get(Calendar.YEAR));
			} else {
				fromDate = "07/01/" + calendar.get(Calendar.YEAR);
				toDate = "09/30/" + calendar.get(Calendar.YEAR);
				revenueExpenseForm.setQuarter("3");
				revenueExpenseForm.setYear("" + calendar.get(Calendar.YEAR));
			}

		} else {
			if (null != revenueExpenseForm.getYear()
					&& "" != revenueExpenseForm.getYear()) {
				quarter = revenueExpenseForm.getQuarter();
				int quart = Integer.parseInt(quarter);
				if (quart == 1) {
					fromDate = "01/01/" + revenueExpenseForm.getYear();
					toDate = "03/31/" + revenueExpenseForm.getYear();
				} else if (quart == 2) {
					fromDate = "04/01/" + revenueExpenseForm.getYear();
					toDate = "06/30/" + revenueExpenseForm.getYear();
				} else if (quart == 3) {
					fromDate = "07/01/" + revenueExpenseForm.getYear();
					toDate = "09/30/" + revenueExpenseForm.getYear();
				} else {
					fromDate = "10/01/" + revenueExpenseForm.getYear();
					toDate = "12/31/" + revenueExpenseForm.getYear();
				}
			}
		}

		revenueExpenseBean.setSeniorProjectManagersList(financialReportDao
				.getSeniorProjectManagerRSFromQuery(ReportStaticData
						.getQueryForRevenueSPMDetails(param, fromDate, toDate,
								classfication)));

		return revenueExpenseBean;
	}

	/**
	 * Sets the bdm details.
	 * 
	 * @param param
	 *            the param
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param classfication
	 *            the classfication
	 * @param revenueExpenseForm
	 *            the revenue expense form
	 * @return the revenue expense bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public RevenueExpenseBean setBDMDetails(String param, String fromDate,
			String toDate, String[] classfication,
			RevenueExpenseForm revenueExpenseForm) throws SysException,
			AppException {
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		FinancialReportDAO financialReportDao = new FinancialReportDAO();
		String quarter;
		String defaultValue = "firstTime";
		/*
		 * Get JobOwner List.
		 */
		if (defaultValue.equalsIgnoreCase(param)) {
			int monthNo = calendar.get(Calendar.MONTH);
			if (monthNo >= 0 && monthNo <= 2) {
				int previousYear = calendar.get(Calendar.YEAR) - 1;
				fromDate = "09/01/" + previousYear;
				toDate = "12/31/" + previousYear;
				revenueExpenseForm.setQuarter("4");
				revenueExpenseForm.setYear("" + previousYear);

			} else if (monthNo >= 3 && monthNo <= 5) {
				fromDate = "01/01/" + calendar.get(Calendar.YEAR);
				toDate = "03/31/" + calendar.get(Calendar.YEAR);
				revenueExpenseForm.setQuarter("1");
				revenueExpenseForm.setYear("" + calendar.get(Calendar.YEAR));
			} else if (monthNo >= 6 && monthNo <= 8) {
				fromDate = "04/01/" + calendar.get(Calendar.YEAR);
				toDate = "06/30/" + calendar.get(Calendar.YEAR);
				revenueExpenseForm.setQuarter("2");
				revenueExpenseForm.setYear("" + calendar.get(Calendar.YEAR));
			} else {
				fromDate = "07/01/" + calendar.get(Calendar.YEAR);
				toDate = "09/30/" + calendar.get(Calendar.YEAR);
				revenueExpenseForm.setQuarter("3");
				revenueExpenseForm.setYear("" + calendar.get(Calendar.YEAR));
			}

		} else {
			if (null != revenueExpenseForm.getYear()
					&& "" != revenueExpenseForm.getYear()) {
				quarter = revenueExpenseForm.getQuarter();
				int quart = Integer.parseInt(quarter);
				if (quart == 1) {
					fromDate = "01/01/" + revenueExpenseForm.getYear();
					toDate = "03/31/" + revenueExpenseForm.getYear();
				} else if (quart == 2) {
					fromDate = "04/01/" + revenueExpenseForm.getYear();
					toDate = "06/30/" + revenueExpenseForm.getYear();
				} else if (quart == 3) {
					fromDate = "07/01/" + revenueExpenseForm.getYear();
					toDate = "09/30/" + revenueExpenseForm.getYear();
				} else {
					fromDate = "10/01/" + revenueExpenseForm.getYear();
					toDate = "12/31/" + revenueExpenseForm.getYear();
				}
			}
		}

		revenueExpenseBean.setBdmList(financialReportDao
				.getBDMResultSetQuery(ReportStaticData.getQueryForBDMDetails(
						param, fromDate, toDate, classfication)));

		return revenueExpenseBean;
	}

	/**
	 * Sets the revenue job owner details.
	 * 
	 * @param jobOwnerId
	 *            the job owner id
	 * @param reportName
	 *            the report name
	 * @param param
	 *            the param
	 * @return the revenue expense bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public RevenueExpenseBean setRevenueJobOwnerDetails(String jobOwnerId,
			String reportName, String param) throws SysException, AppException {
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		FinancialReportDAO financialReportDao = new FinancialReportDAO();
		String reportType1 = "JobOwnerTTMSummary";
		String reportType2 = "jobOwnerMonthPopupReport";
		String reportType3 = "jobOwnerAccountPopupReport";
		/*
		 * Get JobOwner List.
		 */
		if (reportType1.equals(reportName))
			revenueExpenseBean.setDetailedList(financialReportDao
					.getJobOwnerDetailQuery(jobOwnerId, reportName, null));
		else if (reportType2.equals(reportName)) {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getJobOwnerThirdLevelReportQuery(ReportStaticData
							.getQueryForParticularJobOwnerDetails(jobOwnerId,
									reportName, param)));
		} else if (reportType3.equals(reportName)) {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getJobOwnerDetailQuery(jobOwnerId, reportName, param));
		} else {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getJobOwnerSecondLevelReportQuery(ReportStaticData
							.getQueryForParticularJobOwnerDetails(jobOwnerId,
									reportName, null)));
		}

		return revenueExpenseBean;
	}

	/**
	 * Sets the revenue project manager details.
	 * 
	 * @param jobOwnerId
	 *            the job owner id
	 * @param reportName
	 *            the report name
	 * @param param
	 *            the param
	 * @return the revenue expense bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public RevenueExpenseBean setRevenueProjectManagerDetails(
			String projectManagerId, String reportName, String param)
			throws SysException, AppException {
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		FinancialReportDAO financialReportDao = new FinancialReportDAO();
		String reportType1 = "ProjectManagerTTMSummary";
		String reportType2 = "projectManagerMonthPopupReport";
		String reportType3 = "projectManagerAccountPopupReport";
		/*
		 * Get JobOwner List.
		 */
		if (reportType1.equalsIgnoreCase(reportName))
			revenueExpenseBean.setDetailedList(financialReportDao
					.getProjectManagerDetailQuery(projectManagerId, reportName,
							null));
		else if (reportType2.equalsIgnoreCase(reportName)) {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getProjectManagerThirdLevelReportQuery(ReportStaticData
							.getQueryForParticularProjectManagerDetails(
									projectManagerId, reportName, param)));
		} else if (reportType3.equalsIgnoreCase(reportName)) {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getProjectManagerDetailQuery(projectManagerId, reportName,
							param));
		} else {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getProjectManagerSecondLevelReportQuery(ReportStaticData
							.getQueryForParticularProjectManagerDetails(
									projectManagerId, reportName, null)));
		}

		return revenueExpenseBean;
	}

	public RevenueExpenseBean setRevenueCustomerDetails(String customerId,
			String reportName, String param, String toDate, String fromDate) throws SysException, AppException {
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		FinancialReportDAO financialReportDao = new FinancialReportDAO();
		String reportType1 = "AppendixSummaryTTM";
		String reportType2 = "customerAppendixMonthPopupReport";
		/*
		 * Get JobOwner List.
		 */
		if (reportType1.equals(reportName))
			revenueExpenseBean.setDetailedList(financialReportDao
					.getJobOwnerSecondLevelReportQuery(ReportStaticData
							.getQueryForParticularCustomerDetails(customerId,
									reportName, null, toDate, fromDate)));
		else if (reportType2.equals(reportName)) {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getCustomerDetailQuery(customerId, reportName, param));
		}
		return revenueExpenseBean;
	}

	/**
	 * Sets the revenue bdm details.
	 * 
	 * @param bdmId
	 *            the bdm id
	 * @param reportName
	 *            the report name
	 * @param param
	 *            the param
	 * @return the revenue expense bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public RevenueExpenseBean setRevenueBDMDetails(String bdmId,
			String reportName, String param) throws SysException, AppException {
		RevenueExpenseBean revenueExpenseBean = new RevenueExpenseBean();
		FinancialReportDAO financialReportDao = new FinancialReportDAO();
		String reportType1 = "BDM(TTM)Summary";
		String reportType2 = "bdmMonthPopupReport";
		String reportType3 = "bdmAccountPopupReport";
		/*
		 * Get Bussiness Deveploment Manager List
		 */
		if (reportType1.equals(reportName))
			revenueExpenseBean.setDetailedList(financialReportDao
					.getBDMDetailQuery(bdmId, reportName, null));
		else if (reportType2.equals(reportName)) {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getJobOwnerThirdLevelReportQuery(ReportStaticData
							.getQueryForParticularBDMDetails(bdmId, reportName,
									param)));
		} else if (reportType3.equals(reportName)) {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getBDMDetailQuery(bdmId, reportName, param));
		} else {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getJobOwnerSecondLevelReportQuery(ReportStaticData
							.getQueryForParticularBDMDetails(bdmId, reportName,
									null)));
		}
		return revenueExpenseBean;

	}

	/**
	 * Sets the revenue spm details.
	 * 
	 * @param spmId
	 *            the spm id
	 * @param reportName
	 *            the report name
	 * @param param
	 *            the param
	 * @param revenueExpenseBean
	 *            the revenue expense bean
	 * @return the revenue expense bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public RevenueExpenseBean setRevenueSPMDetails(String spmId,
			String reportName, String param,
			RevenueExpenseBean revenueExpenseBean) throws SysException,
			AppException {
		FinancialReportDAO financialReportDao = new FinancialReportDAO();
		String reportType1 = "SPM(TTM)Summary";
		String reportType2 = "spmMonthPopupReport";
		String reportType3 = "spmAccountPopupReport";
		/*
		 * Get Bussiness Deveploment Manager List
		 */
		if (reportType1.equals(reportName))
			revenueExpenseBean.setDetailedList(financialReportDao
					.getSPMDetailQuery(spmId, reportName, null));
		else if (reportType2.equals(reportName)) {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getJobOwnerThirdLevelReportQuery(ReportStaticData
							.getQueryForParticularSPMDetails(spmId, reportName,
									param)));
		} else if (reportType3.equals(reportName)) {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getSPMDetailQuery(spmId, reportName, param));
		} else {
			revenueExpenseBean.setDetailedList(financialReportDao
					.getJobOwnerSecondLevelReportQuery(ReportStaticData
							.getQueryForParticularSPMDetails(spmId, reportName,
									null)));
		}
		return revenueExpenseBean;

	}

	/**
	 * Sets the revenue srpm details.
	 * 
	 * @param jobOwnerId
	 *            the job owner id
	 * @param reportName
	 *            the report name
	 * @return the revenue expense bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public RevenueExpenseBean setRevenueSRPMDetails(String jobOwnerId,
			String reportName) throws SysException, AppException {
		/*
		 * Get Senior Project Manager List.
		 */
		return setRevenueJobOwnerDetails(jobOwnerId, reportName, null);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
