package com.ilex.reports.logic.laborCost;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

import com.ilex.reports.bean.laborCost.LaborCostBean;
import com.ilex.reports.bean.laborCost.LaborCostByLocationBean;
import com.ilex.reports.formbean.laborCost.LaborCostByLocationForm;
import com.ilex.reports.formbean.laborCost.LaborCostForm;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;

public class LaborCostConvertor {

	/**
	 * Sets the Labor Cost Form from Labor Cost Bean.
	 * 
	 * @param laborCostForm
	 *            - LaborCostForm.
	 * @param laborCostBean
	 *            - LaborCostBean.
	 * @throws SysException
	 *             the SysException
	 */
	public static void setFormFromLaborCostBean(LaborCostForm laborCostForm,
			LaborCostBean laborCostBean) throws SysException {

		try {
			BeanUtils.copyProperties(laborCostForm, laborCostBean);
		} catch (IllegalAccessException e) {
			throw new SysException(e);
		} catch (InvocationTargetException e) {
			throw new SysException(e);
		}
	}

	/**
	 * Sets the Labor Cost Bean from Labor Cost Form.
	 * 
	 * @param laborCostBean
	 *            - LaborCostBean.
	 * @param laborCostForm
	 *            - laborCostForm.
	 * @throws SysException
	 *             - SysException.
	 */
	public static void setBeanFromLaborCostForm(LaborCostBean laborCostBean,
			LaborCostForm laborCostForm) throws SysException {

		laborCostBean.setLaborCategoryFilter(laborCostForm
				.getLaborCategoryFilter());
		laborCostBean.setLaborType(laborCostForm.getLaborType());
	}

	/**
	 * Sets the Labor Cost Form from Labor Cost Senior PM Bean.
	 * 
	 * @param laborCostForm
	 *            - LaborCostForm.
	 * @param laborCostSrPmBean
	 *            - LaborCostSrPmBean.
	 * @throws SysException
	 *             the SysException
	 */
	public static void setFormFromSrPmLaborCostBean(
			LaborCostForm laborCostForm, LaborCostBean laborCostSrPmBean)
			throws SysException {

		try {
			BeanUtils.copyProperties(laborCostForm, laborCostSrPmBean);
		} catch (IllegalAccessException e) {
			throw new SysException(e);
		} catch (InvocationTargetException e) {
			throw new SysException(e);
		}
	}

	/**
	 * Sets the Location wise Labor Cost Bean from Location wise Labor Cost
	 * Form.
	 * 
	 * @param laborCostByLocBean
	 *            - LaborCostByLocationBean.
	 * @param laborCostByLocForm
	 *            - LaborCostByLocationForm.
	 * @throws SysException
	 *             the SysException
	 */
	public static void setBeanFromLocWiseLaborCostForm(
			LaborCostByLocationBean laborCostByLocBean,
			LaborCostByLocationForm laborCostByLocForm) throws SysException,
			AppException {

		laborCostByLocBean.setViewFilter(laborCostByLocForm.getViewFilter());
		laborCostByLocBean.setOrderBy(laborCostByLocForm.getOrderBy());
		laborCostByLocBean.setEndMonth(laborCostByLocForm.getEndMonth());
		laborCostByLocBean.setEndYear(laborCostByLocForm.getEndYear());
		laborCostByLocBean.setStartMonth(laborCostByLocForm.getStartMonth());
		laborCostByLocBean.setStartYear(laborCostByLocForm.getStartYear());
		

	}

	/**
	 * Sets the Location wise Labor Cost Form from Location wise Labor Cost
	 * Bean.
	 * 
	 * @param laborCostByLocForm
	 *            - LaborCostByLocationForm.
	 * @param laborCostByLocBean
	 *            - LaborCostByLocationBean.
	 * @throws SysException
	 *             the SysException
	 */
	public static void setFormFromLocWiseLaborCostBean(
			LaborCostByLocationForm laborCostByLocForm,
			LaborCostByLocationBean laborCostByLocBean) throws SysException,
			AppException {

		laborCostByLocForm.setLcByLocationList(laborCostByLocBean
				.getLcByLocationList());
		laborCostByLocForm.setViewFilterList(laborCostByLocBean
				.getViewFilterList());
		laborCostByLocForm.setReportTitle(laborCostByLocBean.getReportTitle());

	}
}
