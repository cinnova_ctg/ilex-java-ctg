package com.ilex.reports.logic.laborCost;

public enum LaborCostLocation {
	US("US", "0"), INTERNATIONAL("International", "1");

	private String label = null;
	private String value = null;

	private LaborCostLocation(String label, String value) {
		this.label = label;
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static LaborCostLocation[] getAllLocations() {
		return LaborCostLocation.values();
	}

}
