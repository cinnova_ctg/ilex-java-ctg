package com.ilex.reports.logic.minutemanUtilization;

public class MinutemanUtilizationResultSetBean {
	String name;
	Integer occurance;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOccurance() {
		return occurance;
	}

	public void setOccurance(Integer occurance) {
		this.occurance = occurance;
	}

}
