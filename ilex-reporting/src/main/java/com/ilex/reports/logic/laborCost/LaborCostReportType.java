package com.ilex.reports.logic.laborCost;

public enum LaborCostReportType {

	TOP_LEVEL_LABOR_COST("TOP_LEVEL_LABOR_COST"), TECHNICIAN_LABOR_COST(
			"TECHNICIAN_LABOR_COST"), SR_PM_LABOR_COST("SR_PM_LABOR_COST"), CATEGORY_LABOR(
			"CATEGORY_WISE_LABOR");

	private String reportName = null;

	private LaborCostReportType(String reportName) {
		this.reportName = reportName;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

}
