package com.ilex.reports.logic.financialReport;

public class CustomerResultSetBean extends JobOwnerResultSetBean {

	private String customerId = null;
	private String customerName = null;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
}
