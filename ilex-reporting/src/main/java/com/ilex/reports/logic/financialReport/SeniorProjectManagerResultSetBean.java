package com.ilex.reports.logic.financialReport;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class SeniorProjectManagerResultSetBean extends JobOwnerResultSetBean {

	private String spmId = null;
	private String spmName = null;

	public String getSpmId() {
		return spmId;
	}

	public void setSpmId(String spmId) {
		this.spmId = spmId;
	}

	public String getSpmName() {
		return spmName;
	}

	public void setSpmName(String spmName) {
		this.spmName = spmName;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
