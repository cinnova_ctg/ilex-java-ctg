package com.ilex.reports.logic.financialReport;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class BussinessDevlopmentManagerResultSetBean extends
		JobOwnerResultSetBean {
	private String bdmId = null;
	private String bdmName = null;

	public String getBdmId() {
		return bdmId;
	}

	public void setBdmId(String bdmId) {
		this.bdmId = bdmId;
	}

	public String getBdmName() {
		return bdmName;
	}

	public void setBdmName(String bdmName) {
		this.bdmName = bdmName;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
