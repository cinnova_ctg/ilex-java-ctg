package com.ilex.reports.logic.factBoard;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

import com.ilex.reports.bean.factBoard.FactBoardBean;
import com.ilex.reports.dao.factBoard.FactBoardDao;
import com.ilex.reports.formbean.factBoard.FactBoardForm;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;

public class FactBoardOrganiser {

	public void setFactBoardValues(FactBoardBean factBoardBean) throws SysException, AppException
	{
		 FactBoardDao.setFactBoardValues(factBoardBean);
	}
	public void setFormFromBean(FactBoardForm factBoardForm,FactBoardBean factBoardBean) throws IllegalAccessException, InvocationTargetException 
	{
		BeanUtils.copyProperties(factBoardForm, factBoardBean);
	}
	
}
