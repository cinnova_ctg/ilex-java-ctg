package com.ilex.reports.logic.minutemanUtilization;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public enum MinutmanUtilizationReportType {
	AVAILABILITY_RECRUITING("AVAILABILITY_RECRUITING");
	private String reportName = null;

	private MinutmanUtilizationReportType(String reportName) {
		this.reportName = reportName;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
