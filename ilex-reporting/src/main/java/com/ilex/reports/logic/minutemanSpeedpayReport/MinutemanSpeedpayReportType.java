package com.ilex.reports.logic.minutemanSpeedpayReport;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public enum MinutemanSpeedpayReportType {
	USAGE_BY_COST("USAGE_BY_COST"), USAGE_BY_OCCURENCES("USAGE_BY_OCCURENCES");
	private String reportName = null;

	private MinutemanSpeedpayReportType(String reportName) {
		this.reportName = reportName;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
