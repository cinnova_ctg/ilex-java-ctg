package com.ilex.reports.logic.minutemanSpeedpayReport;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.ilex.reports.bean.minutemanSpeedpayReport.MinutemanSpeedpayCostBean;
import com.ilex.reports.formbean.minutemanSpeedpayReport.MinutemanSpeedpayForm;
import com.mind.fw.lang.SysException;

public class MinutemanSpeedpayConvertor {

	/**
	 * Sets the bean from minuteman cost form.
	 * 
	 * @param minutemanCostBean
	 *            the minuteman cost bean
	 * @param minutemanCostForm
	 *            the minuteman cost form
	 * @throws SysException
	 *             the sys exception
	 */
	public static void setBeanFromMinutemanCostForm(
			MinutemanSpeedpayCostBean minutemanCostBean,
			MinutemanSpeedpayForm minutemanCostForm) throws SysException {

		minutemanCostBean.setProjectCategoryFilter(minutemanCostForm
				.getProjectCategoryFilter());
		minutemanCostBean.setProjectType(minutemanCostForm.getProjectType());
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
