package com.ilex.reports.logic.minutemanUtilization;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.ilex.reports.bean.minutemanUtilization.MinutemanUtilizationBean;
import com.ilex.reports.common.ReportStaticData;
import com.ilex.reports.common.multiAxis.bean.GraphXmlDataSet;
import com.ilex.reports.common.multiAxis.bean.Stacked2DChartOperation;
import com.ilex.reports.common.multiAxis.bean.Stacked2DChartResultSetBean;
import com.ilex.reports.dao.laborCost.LaborCostDAO;
import com.ilex.reports.dao.minutemanUtilization.MinutemanUtilizationDAO;
import com.ilex.reports.formbean.minutemanUtilization.MinutemanUtilizationForm;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;
import com.mind.fw.util.PropertyFileUtil;

public class MinutemanUtilizationOrganiser {
	private static final Logger LOGGER = Logger
			.getLogger(MinutemanUtilizationOrganiser.class);

	/**
	 * Sets the first time minuteman usage.
	 * 
	 * @param filter
	 *            the filter
	 * @return the minuteman utilization bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public MinutemanUtilizationBean setFirstTimeMinutemanUsage(String filter,String lastwo)
			throws SysException, AppException {
		MinutemanUtilizationBean minutemanUtilizationBean = new MinutemanUtilizationBean();
		MinutemanUtilizationDAO minutemanUtilizationDAO = new MinutemanUtilizationDAO();
		minutemanUtilizationBean.setFirstUsageList(minutemanUtilizationDAO
				.getFirstTimeMinutemanUsage(ReportStaticData
						.getBaseQueryForMinutemanFirstTimeUsage(filter,lastwo)));
		return minutemanUtilizationBean;

	}
	
	/*
	public MinutemanUtilizationBean setFirstTimeMinutemanUsageFirstTime(String filter,String lastTwo)
			throws SysException, AppException {
		MinutemanUtilizationBean minutemanUtilizationBean = new MinutemanUtilizationBean();
		MinutemanUtilizationDAO minutemanUtilizationDAO = new MinutemanUtilizationDAO();
		if(lastTwo!=null){
			
			minutemanUtilizationBean.setFirstUsageList(minutemanUtilizationDAO
					.getFirstTimeMinutemanUsage(ReportStaticData
							.getBaseQueryForMinutemanFirstTimeUsage(filter,lastTwo)));	
			
			
		}
		else{
		minutemanUtilizationBean.setFirstUsageList(minutemanUtilizationDAO
				.getFirstTimeMinutemanUsage(ReportStaticData.getBaseQueryForMinutemanFirstTimeUsage(filter)));
	 }
		return minutemanUtilizationBean;

	}*/
	
	
	/**
	 * Sets the first time minuteman usage.
	 * 
	 * @param filter
	 *            the filter
	 * @return the minuteman utilization bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public MinutemanUtilizationBean setMinutemanUsageByJobOwner(String lastTwo)
			throws SysException, AppException {
		MinutemanUtilizationBean minutemanUtilizationBean = new MinutemanUtilizationBean();
		MinutemanUtilizationDAO minutemanUtilizationDAO = new MinutemanUtilizationDAO();
		minutemanUtilizationBean.setFirstUsageList(minutemanUtilizationDAO
				.getMinutemanUsageByJobOwner(lastTwo));
		return minutemanUtilizationBean;

	}

	/**
	 * Sets the form from minuteman utilization.
	 * 
	 * @param minutemanUtlizationForm
	 *            the minuteman utlization form
	 * @param minutemanUtilizationBean
	 *            the minuteman utilization bean
	 * @throws SysException
	 *             the sys exception
	 */
	public static void setFormFromMinutemanUtilization(
			MinutemanUtilizationForm minutemanUtlizationForm,
			MinutemanUtilizationBean minutemanUtilizationBean)
			throws SysException {

		try {
			BeanUtils.copyProperties(minutemanUtlizationForm,
					minutemanUtilizationBean);
		} catch (IllegalAccessException e) {
			throw new SysException(e);
		} catch (InvocationTargetException e) {
			throw new SysException(e);
		}
	}

	/**
	 * Sets the minuteman utilization graph details.
	 * 
	 * @param minutemanUtilizationBean
	 *            the minuteman utilization bean
	 * @param reportName
	 *            the report name
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static void setMinutemanUtilizationGraphDetails(
			MinutemanUtilizationBean minutemanUtilizationBean, String reportName)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method setMinutemanUtilizationGraphDetails starts for Report Type -."
					+ reportName);
		}

		String queryforNoOfRecords = "";
		String queryForRsBeanList = "";
		String dateRangeMonth = "";
		String noOfXCoordinates = "";
		int noOfRecords = 0;
		int maxYValue = 0;
		List<? extends Stacked2DChartResultSetBean> rsBeanList = null;
		try {
			noOfXCoordinates = PropertyFileUtil.getAppProperty(reportName
					+ ".prefix")
					+ ".daterange.month";

		} catch (Exception e) {
			throw new AppException(e.getMessage());
		}

		queryforNoOfRecords = ReportStaticData
				.getBaseQueryForMMUtilAvailablity();
		queryForRsBeanList = ReportStaticData.getQueryForMMUtilAvailablity();
		try {
			dateRangeMonth = ReportStaticData
					.getQueryForMMUtilAvailabilityDateRange("11");
		} catch (Exception e) {
			throw new AppException(e.getMessage());
		}
		MinutemanUtilizationDAO.getDateRange(minutemanUtilizationBean,
				dateRangeMonth);
		noOfRecords = MinutemanUtilizationDAO.getNoOfRecords(ReportStaticData
				.getQueryForCountNoOfRecords(queryforNoOfRecords), "M",
				minutemanUtilizationBean.getStartDate(),
				minutemanUtilizationBean.getEndDate(), reportName);
		maxYValue = MinutemanUtilizationDAO.getMaxOfMMUtilAvailData(
				ReportStaticData.getQueryForMaxOfMMUtilAvailValue(),
				minutemanUtilizationBean.getStartDate(),
				minutemanUtilizationBean.getEndDate(), reportName);
		rsBeanList = MinutemanUtilizationDAO.getRSFromQuery(queryForRsBeanList,
				"M", minutemanUtilizationBean.getStartDate(),
				minutemanUtilizationBean.getEndDate(), reportName);

		ArrayList<String> coOrdinatesDates = new ArrayList<String>();

		String xCoordinates = "";
		try {
			xCoordinates = PropertyFileUtil.getAppProperty(noOfXCoordinates,
					"11");
		} catch (Exception e) {
			throw new AppException();
		}

		minutemanUtilizationBean.setxCordinateDates(LaborCostDAO
				.setCompleteMonthData(minutemanUtilizationBean.getStartDate(),
						Integer.parseInt(xCoordinates) + 1));

		coOrdinatesDates = minutemanUtilizationBean.getxCordinateDates();

		ArrayList<GraphXmlDataSet> graphXmlDataList;
		try {
			graphXmlDataList = Stacked2DChartOperation
					.getAxisListForStackedSimpleChart(rsBeanList,
							coOrdinatesDates, noOfRecords,
							Integer.parseInt(xCoordinates) + 1, reportName);
		} catch (NumberFormatException e) {
			throw new AppException(e.getMessage());
		} catch (Exception e) {
			throw new AppException(e.getMessage());
		}
		/*
		 * Create a chart XML and then set it into bean.
		 */
		try {
			minutemanUtilizationBean.setGraphXML(Stacked2DChartOperation
					.createXmlGraph(graphXmlDataList,
							minutemanUtilizationBean.getxCordinateDates(),
							reportName, (long) maxYValue, null));
		} catch (Exception e) {
			throw new AppException();
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method setMinutemanSpeedpayGraphDetails ends for Report Type -."
					+ reportName);
		}

	}

	/**
	 * Sets the mm availablity.
	 * 
	 * @param availablityMMBean
	 *            the availablity mm bean
	 * @return the minuteman utilization bean
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public MinutemanUtilizationBean setMMAvailablity(
			MinutemanUtilizationBean availablityMMBean) throws SysException,
			AppException {
		MinutemanUtilizationDAO availabilityDAO = new MinutemanUtilizationDAO();
		availablityMMBean = availabilityDAO.getAvailablity(
				ReportStaticData.getBaseQueryForMMAavailability(),
				availablityMMBean);
		return availablityMMBean;
	}

}
