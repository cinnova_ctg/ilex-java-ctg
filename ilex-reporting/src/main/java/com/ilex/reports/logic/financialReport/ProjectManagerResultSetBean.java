package com.ilex.reports.logic.financialReport;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class ProjectManagerResultSetBean {
	private String projectManagerId = null;
	private String projectManager = null;
	private String proFormaExpense = null;
	private String expence = null;
	private String revenue = null;
	private String proFormaVGPM = null;
	private String VGPM = null;
	private String deltaVGPM = null;
	private String occurrences = null;

	public String getProjectManagerId() {
		return projectManagerId;
	}

	public void setProjectManagerId(String projectManagerId) {
		this.projectManagerId = projectManagerId;
	}

	public String getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}

	public String getProFormaExpense() {
		return proFormaExpense;
	}

	public void setProFormaExpense(String proFormaExpense) {
		this.proFormaExpense = proFormaExpense;
	}

	public String getExpence() {
		return expence;
	}

	public void setExpence(String expence) {
		this.expence = expence;
	}

	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	public String getProFormaVGPM() {
		return proFormaVGPM;
	}

	public void setProFormaVGPM(String proFormaVGPM) {
		this.proFormaVGPM = proFormaVGPM;
	}

	public String getVGPM() {
		return VGPM;
	}

	public void setVGPM(String vGPM) {
		VGPM = vGPM;
	}

	public String getDeltaVGPM() {
		return deltaVGPM;
	}

	public void setDeltaVGPM(String deltaVGPM) {
		this.deltaVGPM = deltaVGPM;
	}

	public String getOccurrences() {
		return occurrences;
	}

	public void setOccurrences(String occurrences) {
		this.occurrences = occurrences;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
