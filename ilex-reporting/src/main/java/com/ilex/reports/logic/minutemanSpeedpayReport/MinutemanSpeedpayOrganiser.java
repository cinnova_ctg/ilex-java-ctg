package com.ilex.reports.logic.minutemanSpeedpayReport;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.ilex.reports.bean.minutemanSpeedpayReport.MinutemanSpeedpayCostBean;
import com.ilex.reports.common.ReportStaticData;
import com.ilex.reports.common.multiAxis.bean.GraphXmlDataSet;
import com.ilex.reports.common.multiAxis.bean.MultiAxisChartOperation;
import com.ilex.reports.common.multiAxis.bean.Stacked2DChartOperation;
import com.ilex.reports.common.multiAxis.bean.Stacked2DChartResultSetBean;
import com.ilex.reports.dao.laborCost.LaborCostDAO;
import com.ilex.reports.dao.minutemanSpeedpayReport.MinutemanSpeedpayDAO;
import com.ilex.reports.formbean.minutemanSpeedpayReport.MinutemanSpeedpayForm;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;
import com.mind.fw.util.PropertyFileUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class MinutemanSpeedpayOrganiser.
 */
public class MinutemanSpeedpayOrganiser {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger
			.getLogger(MinutemanSpeedpayOrganiser.class);

	/**
	 * Sets the minuteman cost form from minuteman cost bean.
	 * 
	 * @param minutemanCostForm
	 *            the minuteman cost form
	 * @param minutemanCostBean
	 *            the minuteman cost bean
	 * @throws SysException
	 *             the sys exception
	 */
	public static void setFormFromMinutemanCostBean(
			MinutemanSpeedpayForm minutemanCostForm,
			MinutemanSpeedpayCostBean minutemanCostBean) throws SysException {

		try {
			BeanUtils.copyProperties(minutemanCostForm, minutemanCostBean);
		} catch (IllegalAccessException e) {
			throw new SysException(e);
		} catch (InvocationTargetException e) {
			throw new SysException(e);
		}
	}

	/**
	 * Populate minuteman speedpay project category.
	 * 
	 * @param minutemanCostBean
	 *            the minuteman cost bean
	 * @param minutemanCostType
	 *            the minuteman cost type
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static void populateProjectCategory(
			MinutemanSpeedpayCostBean minutemanCostBean,
			String minutemanCostType) throws SysException, AppException {
		minutemanCostBean.setProjectCategoryList(MinutemanSpeedpayDAO
				.populateProjectCategory(ReportStaticData
						.getQueryForProjectCategory()));
	}

	/**
	 * Sets the minuteman speedpay graph details.
	 * 
	 * @param minutemanCostBean
	 *            the minuteman cost bean
	 * @param reportName
	 *            the report name
	 * @throws SysException
	 *             the sys exception
	 * @throws AppException
	 *             the app exception
	 */
	public static void setMinutemanSpeedpayGraphDetails(
			MinutemanSpeedpayCostBean minutemanCostBean, String reportName)
			throws SysException, AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method setMinutemanSpeedpayGraphDetails starts for Report Type -."
					+ reportName);
		}

		String param = "";
		String queryforNoOfRecords = "";
		String queryForRsBeanList = "";
		String queryForTotalCostForEachMonth = "";
		String queryForTotalOccurrencesForEachMonth = "";
		String dateRangeMonth = "";
		String noOfXCoordinates = "";
		int noOfRecords = 0;
		Long maxYValue = (long) 0.0;
		List<? extends Stacked2DChartResultSetBean> rsBeanList = null;
		List<MinutemanSpeedpayTotalValueByMonth> totalValueByMonthList = null;
		try {
			noOfXCoordinates = PropertyFileUtil.getAppProperty(reportName
					+ ".prefix")
					+ ".daterange.month";

		} catch (Exception e) {
			throw new AppException(e.getMessage());
		}

		if (reportName.equals(MinutemanSpeedpayReportType.USAGE_BY_COST
				.getReportName())) {

			queryforNoOfRecords = ReportStaticData
					.getBaseQueryForMinutemanCost();
			queryForRsBeanList = ReportStaticData.getQueryForMinutemanCost();
			queryForTotalCostForEachMonth = ReportStaticData
					.getQueryForTotalValueForEachMonth(reportName);

			if (minutemanCostBean.getProjectCategoryFilter() == null
					|| minutemanCostBean.getProjectCategoryFilter().equals("")) {
				minutemanCostBean
						.setProjectCategoryFilter(MinutemanSpeedpayCatagory.ALL
								.getText());
			}
			minutemanCostBean.setReportSubTitle(minutemanCostBean
					.getProjectCategoryFilter());
			param = minutemanCostBean.getProjectCategoryFilter();

			try {
				dateRangeMonth = ReportStaticData
						.getQueryForMinutemanCostDateRangeMonth(PropertyFileUtil
								.getAppProperty(noOfXCoordinates, "11"));
			} catch (Exception e) {
				throw new AppException(e.getMessage());
			}
			MinutemanSpeedpayDAO.getDateRange(minutemanCostBean,
					dateRangeMonth, param);
			noOfRecords = MinutemanSpeedpayDAO.getNoOfRecords(ReportStaticData
					.getQueryForCountNoOfRecords(queryforNoOfRecords), "M",
					"S", "P", param, minutemanCostBean.getStartDate(),
					minutemanCostBean.getEndDate(), reportName);
			maxYValue = MinutemanSpeedpayDAO.getMaxOfMinutemanCostData(
					ReportStaticData.getQueryForMaxOfMinutemanCost(), param,
					minutemanCostBean.getStartDate(),
					minutemanCostBean.getEndDate(), reportName);
			rsBeanList = MinutemanSpeedpayDAO.getRSFromQuery(
					queryForRsBeanList, "M", "S", "P", param,
					minutemanCostBean.getStartDate(),
					minutemanCostBean.getEndDate(), reportName);

			totalValueByMonthList = MinutemanSpeedpayDAO.getTotalValueByMonth(
					queryForTotalCostForEachMonth, param,
					minutemanCostBean.getStartDate(),
					minutemanCostBean.getEndDate());
		}
		if (reportName.equals(MinutemanSpeedpayReportType.USAGE_BY_OCCURENCES
				.getReportName())) {

			queryforNoOfRecords = ReportStaticData
					.getBaseQueryForMinutemanOccurences();
			queryForRsBeanList = ReportStaticData
					.getQueryForMinutemanOccurences();
			queryForTotalOccurrencesForEachMonth = ReportStaticData
					.getQueryForTotalValueForEachMonth(reportName);

			if (minutemanCostBean.getProjectCategoryFilter() == null
					|| minutemanCostBean.getProjectCategoryFilter().equals("")) {
				minutemanCostBean
						.setProjectCategoryFilter(MinutemanSpeedpayCatagory.ALL
								.getText());
			}
			minutemanCostBean.setReportSubTitle(minutemanCostBean
					.getProjectCategoryFilter());
			param = minutemanCostBean.getProjectCategoryFilter();

			try {
				dateRangeMonth = ReportStaticData
						.getQueryForMinutemanCostDateRangeMonth(PropertyFileUtil
								.getAppProperty(noOfXCoordinates, "11"));
			} catch (Exception e) {
				throw new AppException(e.getMessage());
			}
			MinutemanSpeedpayDAO.getDateRange(minutemanCostBean,
					dateRangeMonth, param);
			noOfRecords = MinutemanSpeedpayDAO.getNoOfRecords(ReportStaticData
					.getQueryForCountNoOfRecords(queryforNoOfRecords), "M",
					"P", "S", param, minutemanCostBean.getStartDate(),
					minutemanCostBean.getEndDate(), reportName);
			maxYValue = MinutemanSpeedpayDAO.getMaxOfMinutemanCostData(
					ReportStaticData.getQueryForMaxOfMinutemanOccurences(),
					param, minutemanCostBean.getStartDate(),
					minutemanCostBean.getEndDate(), reportName);
			rsBeanList = MinutemanSpeedpayDAO.getRSFromQuery(
					queryForRsBeanList, "M", "P", "S", param,
					minutemanCostBean.getStartDate(),
					minutemanCostBean.getEndDate(), reportName);
			totalValueByMonthList = MinutemanSpeedpayDAO.getTotalValueByMonth(
					queryForTotalOccurrencesForEachMonth, param,
					minutemanCostBean.getStartDate(),
					minutemanCostBean.getEndDate());

		}
		ArrayList<String> coOrdinatesDates = new ArrayList<String>();

		String xCoordinates = "";
		try {
			xCoordinates = PropertyFileUtil.getAppProperty(noOfXCoordinates,
					"11");
		} catch (Exception e) {
			throw new AppException();
		}

		minutemanCostBean.setxCordinateDates(LaborCostDAO.setCompleteMonthData(
				minutemanCostBean.getStartDate(),
				Integer.parseInt(xCoordinates) + 1));

		coOrdinatesDates = minutemanCostBean.getxCordinateDates();

		ArrayList<GraphXmlDataSet> graphXmlDataList;
		try {
			graphXmlDataList = Stacked2DChartOperation
					.getAxisListForStackedAxisChart(rsBeanList,
							coOrdinatesDates, totalValueByMonthList,
							noOfRecords, Integer.parseInt(xCoordinates) + 1,
							reportName);
		} catch (NumberFormatException e) {
			throw new AppException(e.getMessage());
		} catch (Exception e) {
			throw new AppException(e.getMessage());
		}
		// minutemanCostBean.setAxisList(chartXmlAxisList);

		maxYValue = MultiAxisChartOperation.getClosedIndexValue(maxYValue);

		/*
		 * Create a chart XML and then set it into bean.
		 */
		try {
			minutemanCostBean.setGraphXML(Stacked2DChartOperation
					.createXmlGraph(graphXmlDataList,
							minutemanCostBean.getxCordinateDates(), reportName,
							maxYValue, null));
		} catch (Exception e) {
			throw new AppException();
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method setMinutemanSpeedpayGraphDetails ends for Report Type -."
					+ reportName);
		}

	}

	/**
	 * Sets the minuteman bean from minuteman form.
	 * 
	 * @param minutemanCostBean
	 *            the minuteman cost bean
	 * @param minutemanCostForm
	 *            the minuteman cost form
	 * @throws SysException
	 *             the sys exception
	 */
	public static void setBeanFromMinutemanForm(
			MinutemanSpeedpayCostBean minutemanCostBean,
			MinutemanSpeedpayForm minutemanCostForm) throws SysException {
		MinutemanSpeedpayConvertor.setBeanFromMinutemanCostForm(
				minutemanCostBean, minutemanCostForm);
	}

}
