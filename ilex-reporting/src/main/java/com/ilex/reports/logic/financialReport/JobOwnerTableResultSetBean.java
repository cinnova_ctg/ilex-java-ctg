package com.ilex.reports.logic.financialReport;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class JobOwnerTableResultSetBean {

	private String month = null;
	private String proFormaExpense = null;
	private String expense = null;
	private String revenue = null;
	private String proFormaVGPM = null;
	private String VGPM = null;
	private String deltaVGPM = null;
	private String occurrences = null;
	private String accountName = null;
	private String accountID = null;

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getProFormaExpense() {
		return proFormaExpense;
	}

	public void setProFormaExpense(String proFormaExpense) {
		this.proFormaExpense = proFormaExpense;
	}

	public String getExpense() {
		return expense;
	}

	public void setExpense(String expense) {
		this.expense = expense;
	}

	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	public String getProFormaVGPM() {
		return proFormaVGPM;
	}

	public void setProFormaVGPM(String proFormaVGPM) {
		this.proFormaVGPM = proFormaVGPM;
	}

	public String getVGPM() {
		return VGPM;
	}

	public void setVGPM(String vGPM) {
		VGPM = vGPM;
	}

	public String getDeltaVGPM() {
		return deltaVGPM;
	}

	public void setDeltaVGPM(String deltaVGPM) {
		this.deltaVGPM = deltaVGPM;
	}

	public String getOccurrences() {
		return occurrences;
	}

	public void setOccurrences(String occurrences) {
		this.occurrences = occurrences;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
