package com.ilex.reports.logic.laborCost.locationWise;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.ilex.reports.bean.laborCost.LaborCostByLocationBean;
import com.ilex.reports.common.LabelValue;
import com.ilex.reports.common.ReportStaticData;
import com.ilex.reports.dao.laborCost.LaborCostDAO;
import com.ilex.reports.formbean.laborCost.LaborCostByLocationForm;
import com.ilex.reports.logic.laborCost.LaborCostCategory;
import com.ilex.reports.logic.laborCost.LaborCostConvertor;
import com.ilex.reports.logic.laborCost.LaborCostLocation;
import com.ilex.reports.util.EnumUtils;
import com.ilex.reports.util.EnumUtils.Month;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;
import com.mind.fw.util.PropertyFileUtil;

public class LocationWiseOrganizer {

	/*
	 * Logger.
	 */
	public static final Logger LOGGER = Logger
			.getLogger(LocationWiseOrganizer.class);

	/**
	 * Sets the Location wise Labor Cost Bean from Location wise Labor Cost
	 * Form.
	 * 
	 * @param laborCostByLocBean
	 *            - LaborCostByLocationBean.
	 * @param laborCostByLocForm
	 *            - LaborCostByLocationForm.
	 * @throws SysException
	 *             the SysException
	 */
	public static void setBeanFromForm(
			LaborCostByLocationBean laborCostByLocBean,
			LaborCostByLocationForm laborCostByLocForm) throws SysException,
			AppException {

		LaborCostConvertor.setBeanFromLocWiseLaborCostForm(laborCostByLocBean,
				laborCostByLocForm);

	}

	/**
	 * Sets the Location wise Labor Cost Form from Location wise Labor Cost
	 * Bean.
	 * 
	 * @param laborCostByLocForm
	 *            - LaborCostByLocationForm.
	 * @param laborCostByLocBean
	 *            - LaborCostByLocationBean.
	 * @throws SysException
	 *             the SysException
	 */
	public static void setFormFromBean(
			LaborCostByLocationForm laborCostByLocForm,
			LaborCostByLocationBean laborCostByLocBean) throws SysException,
			AppException {

		LaborCostConvertor.setFormFromLocWiseLaborCostBean(laborCostByLocForm,
				laborCostByLocBean);

	}

	/**
	 * Sets details in LaborCostByLocationBean for Labor Cost by Location for
	 * PVS Labor report.
	 * 
	 * @param laborCostByLocBean
	 *            - LaborCostByLocationBean.
	 * @throws SysException
	 *             - SysException.
	 * @throws AppException
	 *             - AppException.
	 */
	public static void setLaborCostByLocationBean(
			LaborCostByLocationBean laborCostByLocBean) throws SysException,
			AppException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method setLaborCostByLocationBean starts for Report Type Labor Cost By Location .");
		}

		String countValue = "0";
		String startDate;
		String endDate;
		StringBuilder sqlQuery=new StringBuilder(ReportStaticData.getBaseQueryForLocationWiseLabor());
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");

		/* Set List of entries in viewFilter drop down . */
		ArrayList<LabelValue> lcLocationList = new ArrayList<LabelValue>();
		LaborCostLocation[] lcLocataions = LaborCostLocation.getAllLocations();

		for (LaborCostLocation lcLocation : lcLocataions) {
			lcLocationList.add(new LabelValue(lcLocation.getLabel(), lcLocation
					.getValue()));
		}

		laborCostByLocBean.setViewFilterList(lcLocationList);

		/* Set Default order by on location in ascending order. */
		if (laborCostByLocBean.getOrderBy() == null
				|| laborCostByLocBean.getOrderBy().equals("")) {
			laborCostByLocBean.setOrderBy("order by location asc");
		}

		/* Set Location as US by default. */
		if (laborCostByLocBean.getViewFilter() == null) {
			laborCostByLocBean.setViewFilter(LaborCostLocation.valueOf("US")
					.getValue());
		}

		if (laborCostByLocBean.getViewFilter().equals(
				LaborCostLocation.valueOf("US").getValue())) {
			countValue = "50";
			laborCostByLocBean.setReportTitle(PropertyFileUtil.getProperty(
					"report.laborCost.location.title.us",
					"ApplicationResources.properties"));
		} else {
			laborCostByLocBean.setReportTitle(PropertyFileUtil.getProperty(
					"report.laborCost.location.title.international",
					"ApplicationResources.properties"));
		}
		if(laborCostByLocBean.getStartMonth()!=null  && laborCostByLocBean.getStartYear()!=null){
			Calendar calendar = Calendar.getInstance();
			calendar.set(Integer.valueOf(laborCostByLocBean.getStartYear()),Integer.valueOf( laborCostByLocBean.getStartMonth())-1,01);
	        startDate=dateFormat.format(calendar.getTime());
	        Calendar calendar2 = Calendar.getInstance();
			calendar2.set(Integer.valueOf(laborCostByLocBean.getEndYear()),Integer.valueOf( laborCostByLocBean.getEndMonth())-1,01);
			int maxDay = calendar2.getActualMaximum(Calendar.DAY_OF_MONTH);
			calendar2.set(Integer.valueOf(laborCostByLocBean.getEndYear()),Integer.valueOf( laborCostByLocBean.getEndMonth())-1,maxDay);
	        endDate=dateFormat.format(calendar2.getTime());
			
		/*
		 * Get Data from database based on input filter parameter value('US'/
		 * 'International')
		 */
		ArrayList<LaborCostByLocationBean> lcLocations = LaborCostDAO
				.getDataForLocationWiseLaborCost(
						sqlQuery.toString(),
						LaborCostCategory.TECHNICIAN_PVS.getText(), countValue,
						laborCostByLocBean.getViewFilter(),
						laborCostByLocBean.getOrderBy(),startDate,endDate);

		laborCostByLocBean.setLcByLocationList(lcLocations);
		}

	}
	public static  ArrayList<LabelValue> getMonthsList()
	{
		 ArrayList<LabelValue>monthList=new ArrayList<LabelValue>();
		for (EnumUtils.Month month : Month.values()) {
			monthList.add(new LabelValue(month.toShortString(),String.valueOf(month.getIndex())));
     }

		   return monthList;
		
	}
}
