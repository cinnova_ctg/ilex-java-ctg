package com.ilex.reports.logic.minutemanSpeedpayReport;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class MinutemanSpeedpayTotalValueByMonth {

	private String display_month = null;
	private String totalValue = null;

	public String getDisplay_month() {
		return display_month;
	}

	public void setDisplay_month(String display_month) {
		this.display_month = display_month;
	}

	public String getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
