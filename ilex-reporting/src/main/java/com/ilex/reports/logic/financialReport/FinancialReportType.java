package com.ilex.reports.logic.financialReport;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public enum FinancialReportType {

	REVENUE_FINANCIAL_REPORT("REVENUE_FINANCIAL_REPORT"), REVENUE_FINANCIAL_REPORT_BySRPM(
			"REVENUE_FINANCIAL_REPORT_BySRPM"), REVENUE_FINANCIAL_REPORT_ByJobOwner(
			"REVENUE_FINANCIAL_REPORT_ByJobOwner"), REVENUE_FINANCIAL_REPORT_ByJobOwner_CAPTION(
			"Revenue/Expense for");

	private String reportName = null;

	private FinancialReportType(String reportName) {
		this.reportName = reportName;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
