package com.ilex.reports.common.multiAxis.bean;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Stacked2DChartResultSetBean {

	private String purchase_order_type;
	private String display_month;
	// For MMUtilizationAvailability Report
	private String partner_type;

	public String getPartner_type() {
		return partner_type;
	}

	public void setPartner_type(String partner_type) {
		this.partner_type = partner_type;
	}

	public String getTotalMinuteman() {
		return totalMinuteman;
	}

	public void setTotalMinuteman(String totalMinuteman) {
		this.totalMinuteman = totalMinuteman;
	}

	private String totalMinuteman;

	public String getPurchase_order_type() {
		return purchase_order_type;
	}

	public void setPurchase_order_type(String purchase_order_type) {
		this.purchase_order_type = purchase_order_type;
	}

	public String getDisplay_month() {
		return display_month;
	}

	public void setDisplay_month(String displayMonth) {
		display_month = displayMonth;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	private String cost;
	private String occurences;

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getOccurences() {
		return occurences;
	}

	public void setOccurences(String occurences) {
		this.occurences = occurences;
	}

	private String month;

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
