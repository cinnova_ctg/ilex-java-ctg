package com.ilex.reports.common.multiAxis.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "graph")
@XmlAccessorType(XmlAccessType.FIELD)
public class GraphXmlHeader {

	@XmlElement
	private GraphXmlCatageries categories = null;

	public GraphXmlCatageries getCategory() {
		return categories;
	}

	public void setCategory(GraphXmlCatageries categories) {
		this.categories = categories;
	}

	@XmlElement
	private List<GraphXmlDataSet> dataset = null;
	
	public List<GraphXmlDataSet> getDataset() {
		return dataset;
	}

	public void setDataset(List<GraphXmlDataSet> dataset) {
		this.dataset = dataset;
	}

	@XmlAttribute
	private String caption = null;
	

	@XmlAttribute
	private String formatNumberScale = null;
	
	public String getFormatNumberScale() {
		return formatNumberScale;
	}

	public void setFormatNumberScale(String formatNumberScale) {
		this.formatNumberScale = formatNumberScale;
	}

	@XmlAttribute
	private String showSum = null;

	@XmlAttribute
	private String useRoundEdges = null;
	
	@XmlAttribute
	private String xaxisname = null;

	@XmlAttribute
	private String divLineColor = null;

	@XmlAttribute
	private String yAxisMaxValue = null;

	@XmlAttribute
	private String numdivLines = null;

	@XmlAttribute
	private String decimalPrecision = null;

	@XmlAttribute
	private String numberPrefix = null;
	
	@XmlAttribute
	private String yaxisname = null;

	
	public String getNumberPrefix() {
		return numberPrefix;
	}

	public void setNumberPrefix(String numberPrefix) {
		this.numberPrefix = numberPrefix;
	}

	
	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}
	

	public String getXaxisname() {
		return xaxisname;
	}

	public void setXaxisname(String xaxisname) {
		this.xaxisname = xaxisname;
	}

	public String getDivLineColor() {
		return divLineColor;
	}

	public void setDivLineColor(String divLineColor) {
		this.divLineColor = divLineColor;
	}

	public String getyAxisMaxValue() {
		return yAxisMaxValue;
	}

	public void setyAxisMaxValue(String yAxisMaxValue) {
		this.yAxisMaxValue = yAxisMaxValue;
	}

	public String getNumdivLines() {
		return numdivLines;
	}

	public void setNumdivLines(String numdivLines) {
		this.numdivLines = numdivLines;
	}

	public String getDecimalPrecision() {
		return decimalPrecision;
	}

	public void setDecimalPrecision(String decimalPrecision) {
		this.decimalPrecision = decimalPrecision;
	}


	public String getYaxisname() {
		return yaxisname;
	}

	public void setYaxisname(String yaxisname) {
		this.yaxisname = yaxisname;
	}
	public String getUseRoundEdges() {
		return useRoundEdges;
	}

	public void setUseRoundEdges(String useRoundEdges) {
		this.useRoundEdges = useRoundEdges;
	}
	public String getShowSum() {
		return showSum;
	}

	public void setShowSum(String showSum) {
		this.showSum = showSum;
	}


}
