package com.ilex.reports.common.multiAxis.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "set")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChartXmlSetValue {

	@XmlAttribute
	private String value = null;

	@XmlAttribute
	private String toolText = null;

	public String getToolText() {
		return toolText;
	}

	public void setToolText(String toolText) {
		this.toolText = toolText;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
