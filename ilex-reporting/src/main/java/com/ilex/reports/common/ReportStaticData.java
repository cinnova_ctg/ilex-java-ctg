package com.ilex.reports.common;

import java.util.Calendar;

import com.ilex.reports.logic.financialReport.FinancialReportType;
import com.ilex.reports.logic.laborCost.LaborType;

public class ReportStaticData {

	/**
	 * Method for query common condition part for All Report.
	 * 
	 * @return String
	 */
	public static String getConditionpart() {
		StringBuilder sb = new StringBuilder(" from dbo.ac_dimensions_project ");
		sb.append("join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append("join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append("join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append("join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		sb.append("join ac_dimension_user on ac_jd_job_owner = ac_du_lo_pc_id ");
		sb.append("where ac_jd_date_closed >= dateadd(mm, -11, ?) ");
		sb.append("and ac_cl_major_classification in ('Project','NetMedX') ");
		sb.append("and ac_jd_job_owner = ?  ");
		sb.append("group by sc_dd_month_mm01yyyy ");
		return sb.toString();
	}

	/**
	 * Gets the query common condition part for All Report.
	 * 
	 * @return String
	 */
	public static String getConditionspart() {
		StringBuilder sb = new StringBuilder(
				"join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append("join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append("join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append("join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		sb.append("where ac_jd_date_closed >= dateadd(mm, -11, ?) ");
		return sb.toString();

	}

	/**
	 * Gets the query common part.
	 * 
	 * @return String
	 */
	public static String getCommonPart() {
		StringBuilder sb = new StringBuilder(
				" sum(ac_ru_pro_forma) as proFormaExpense,");
		sb.append(" sum(ac_ru_expense) as expence, ");
		sb.append(" sum(ac_ru_revenue) as revenue, ");
		sb.append(" case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append(" ROUND((1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)),2) else 0.0 end as proFormaVGPM, ");
		sb.append(" case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append(" ROUND((1-sum(ac_ru_expense)/sum(ac_ru_revenue)),2) else 0.0 end as VGPM, ");
		sb.append(" case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append(" ROUND((1-sum(ac_ru_expense)/sum(ac_ru_revenue)),2)-");
		sb.append(" ROUND((1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)),2)else 0.0 end as deltaVGPM, ");
		sb.append(" count(*) as occurrences ");
		sb.append(" from dbo.ac_dimensions_project ");
		sb.append(" join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append(" join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append(" join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append(" join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		return sb.toString();

	}

	/**
	 * Description - Gets the base query for Top Level and Technician Labor Cost
	 * reports.
	 * 
	 * @return String
	 */

	public static String getBaseQuery() {
		StringBuilder sb = new StringBuilder(
				"(select distinct top 12 month from ac_fact2_labor_rate_by_month order by month desc) B ");
		sb.append("left outer join ");
		sb.append("(select top 12 month, isnull(rate_average,0) as col1 from ac_fact2_labor_rate_by_month ");
		sb.append("where type = ? order by month desc) C on B.month = C.month ");
		sb.append("left outer join ");
		sb.append("(select top 12 month, isnull(rate_average,0) as col2 from ac_fact2_labor_rate_by_month ");
		sb.append("where type = ? order by month desc) D on B.month = D.month ");
		sb.append("left outer join ");
		sb.append("(select top 12 month, isnull(rate_average,0) as col3 from ac_fact2_labor_rate_by_month ");
		sb.append("where type = ? order by month desc) E on B.month = E.month ");
		sb.append("where B.month between ? and ? ");
		sb.append("order by B.month");

		return sb.toString();
	}

	/**
	 * Description - Gets final query for Top Level and Technician Labor Cost
	 * reports.
	 * 
	 * @return String
	 */
	public static String getQueryForLaborCostChart() {

		StringBuilder sb = new StringBuilder(
				"select top 12 displayMonth = substring(convert(varchar(8), B.month, 3), 4,8), ");
		sb.append("col1 = convert(varchar(8), isnull(C.col1,0), 128), ");
		sb.append("col2 = convert(varchar(8), isnull(D.col2, 0), 128), ");
		sb.append("col3 = convert(varchar(8), isnull(E.col3, 0), 128) ");
		sb.append("from ");
		sb.append(ReportStaticData.getBaseQuery());

		return sb.toString();
	}

	/**
	 * Description - Gets query to retrieve maximum of labor cost average value
	 * for Top Level and Technician Labor Cost reports.
	 * 
	 * @return String
	 */
	public static String getQueryForMaxOfLaborCost() {

		StringBuilder sb = new StringBuilder(
				"select CEILING(max(col1)) as col1 , CEILING(max(col2)) as col2, "
						+ "CEILING(Max(col3)) as col3 from ( ");
		sb.append(ReportStaticData.getQueryForLaborCostChart());
		sb.append(") Z");

		return sb.toString();
	}

	/**
	 * Gets the query for max of minuteman cost.
	 * 
	 * @return String
	 */
	public static String getQueryForMaxOfMinutemanCost() {

		StringBuilder sb = new StringBuilder(
				"SELECT cost,month FROM  dbo.ac_fact2_purchase_order_type_summary_by_month WHERE project_type in(?) AND month between  ? and ? order by month");
		return sb.toString();
	}

	public static String getQueryForMaxOfMMUtilAvailValue() {

		StringBuilder sb = new StringBuilder(
				"select max(totalMinuteman) as maxValue from (select count(*) as totalMinuteman ");
		sb.append(" from ilex.dbo.cp_partner ");
		sb.append(" join datawarehouse.dbo.ac_dimension_date on convert(varchar(10), cp_pt_created_date,121) = sc_dd_mmddyyyy ");
		sb.append(" where cp_pt_partner_type = 'M' and cp_pt_created_date >= ?  ");
		sb.append(" group by sc_dd_month_mm01yyyy) A");
		return sb.toString();
	}

	/**
	 * Gets the query for max of minuteman occurences.
	 * 
	 * @return String
	 */
	public static String getQueryForMaxOfMinutemanOccurences() {

		StringBuilder sb = new StringBuilder(
				"SELECT occurrences,month FROM  dbo.ac_fact2_purchase_order_type_summary_by_month WHERE project_type in(?) AND month between  ? and ? order by month");
		return sb.toString();
	}

	/**
	 * Gets the query for finding Maximum Senior Project Manager .
	 * 
	 * @return String
	 */
	public static String getQueryForMaxOfSrPM() {
		StringBuilder sb = new StringBuilder(
				"select CEILING(max(col1)) as col1 , CEILING(max(col2)) as col2, "
						+ "CEILING(Max(col3)) as col3 from ( ");
		sb.append(ReportStaticData.getQueryForLaborCostChart());
		sb.append(") Z");

		return sb.toString();

	}

	/**
	 * Description - Gets query for Labor Cost category drop down .
	 * 
	 * @return String
	 */
	public static String getQueryForLaborCategory() {

		StringBuilder sb = new StringBuilder(
				"select labelCategory = type, valueCategory = type ");
		sb.append("from ");
		sb.append("(select distinct type from dbo.ac_fact2_labor_rate_by_month where type like 'CF%' ) A ");
		sb.append("order by type");

		return sb.toString();
	}

	/**
	 * Gets the query for project category.
	 * 
	 * @return String
	 */
	public static String getQueryForProjectCategory() {

		StringBuilder sb = new StringBuilder(
				"select labelCategory = project_type, valueCategory = project_type ");
		sb.append("from ");
		sb.append("(select distinct project_type from ac_fact2_purchase_order_type_summary_by_month ) A ");
		sb.append("order by project_type");

		return sb.toString();
	}

	/**
	 * Gets the base query for date range.
	 * 
	 * @param dateRange
	 *            the date range
	 * @return String
	 */
	public static String getBaseQueryForDateRange(String dateRange) {
		StringBuilder sb = new StringBuilder("select mthEnd , dateadd(mm, -");
		sb.append(dateRange);
		sb.append(", mthEnd) as mthStart ");
		sb.append("from ");
		return sb.toString();
	}

	/**
	 * Description - Gets query to retrieve date range of last 12 months
	 * starting from highest date in the for table for Top Level and Technician
	 * Labor Cost reports.
	 * 
	 * @return String
	 */
	public static String getQueryForLaborCostDateRangeMonth(String dateRange) {
		StringBuilder sb = new StringBuilder(
				getBaseQueryForDateRange(dateRange));

		sb.append("(select max(month) as mthEnd from ac_fact2_labor_rate_by_month where type in (?,?,?)) A");

		return sb.toString();
	}

	/**
	 * Gets the query for minuteman cost date range.
	 * 
	 * @param dateRange
	 *            the date range
	 * @return String
	 */
	public static String getQueryForMinutemanCostDateRangeMonth(String dateRange) {
		StringBuilder sb = new StringBuilder(
				getBaseQueryForDateRange(dateRange));

		sb.append("(select max(month) as mthEnd from ac_fact2_purchase_order_type_summary_by_month where project_type in (?)) A");

		return sb.toString();
	}

	/**
	 * Gets the query for date range of Minuteman Availability /Usage Chart .
	 * 
	 * @param dateRange
	 *            the date range
	 * @return the query for mm util availability date range
	 */
	public static String getQueryForMMUtilAvailabilityDateRange(String dateRange) {
		StringBuilder sb = new StringBuilder(
				"select max(sc_dd_month_mm01yyyy) AS mthEnd,dateadd(mm, -");
		sb.append(dateRange
				+ ", max(sc_dd_month_mm01yyyy)) as mthStart FROM (select sc_dd_month_mm01yyyy");
		sb.append(" from ilex.dbo.cp_partner");
		sb.append(" join datawarehouse.dbo.ac_dimension_date on convert(varchar(10), cp_pt_created_date,121) = sc_dd_mmddyyyy");
		sb.append(" where cp_pt_partner_type = 'M'");
		sb.append(" group by sc_dd_month_mm01yyyy) A");
		return sb.toString();
	}

	/**
	 * Gets the query for srpm labor cost date range month.
	 * 
	 * @param dateRange
	 *            the date range
	 * @return String
	 */
	public static String getQueryForSRPMLaborCostDateRangeMonth(String dateRange) {
		StringBuilder sb = new StringBuilder(
				getBaseQueryForDateRange(dateRange));

		sb.append("(select max(month) as mthEnd from ");
		sb.append(" ( select month,type from ac_fact2_labor_rate_month_spm union select month,type from ac_fact2_labor_rate_by_month) D ");
		sb.append("where D.type in (?,?,?)) A");

		return sb.toString();
	}

	/**
	 * Gets the query for revenue date range month.
	 * 
	 * @param dateRange
	 *            the date range
	 * @return String
	 */
	public static String getQueryForRevenueDateRangeMonth(String dateRange) {
		StringBuilder sb = new StringBuilder(
				getBaseQueryForDateRange(dateRange));

		sb.append("(select max(month) as mthEnd from ac_fact2_revenue_by_role where role_name in (?,?)) A");

		return sb.toString();
	}

	/**
	 * Gets the query for revenue financial date range month.
	 * 
	 * @param dateRange
	 *            the date range
	 * @param jobFilterType
	 *            the job filter type
	 * @param reportName
	 *            the report name
	 * @return String
	 */
	public static String getQueryForRevenueFinancialDateRangeMonth(
			String dateRange, String jobFilterType, String reportName) {
		String filterType = "AllJobs";
		StringBuilder sb = new StringBuilder();
		sb.append("select max(sc_dd_month_mm01yyyy) as mthEnd, dateadd(mm, -");
		sb.append(dateRange);
		sb.append(",max(sc_dd_month_mm01yyyy)) as mthStart from dbo.ac_dimensions_project ");
		sb.append("join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append("join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append("join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append("join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		if (reportName.equals("REVENUE_FINANCIAL_REPORT_ByJobOwner")) {
			sb.append("join ac_dimension_user on ac_jd_job_owner = ac_du_lo_pc_id ");
			sb.append("where ac_cl_major_classification in ('Project','NetMedX')");
		} else {
			if (filterType.equals(jobFilterType))
				sb.append(" where  ac_cl_major_classification like ? ");
			else
				sb.append(" where  ac_cl_major_classification = ? ");
		}
		return sb.toString();

	}

	/**
	 * Gets the query for category date range month.
	 * 
	 * @param dateRange
	 *            the date range
	 * @param laborType
	 *            the labor type
	 * @return String
	 */
	public static String getQueryForCategoryDateRangeMonth(String dateRange,
			String laborType) {
		StringBuilder sb = new StringBuilder(
				getBaseQueryForDateRange(dateRange));
		sb.append("( select max(month) as mthEnd from ac_fact2_labor_rate_by_criticality_by_month ");
		sb.append(getConditionForCategoryDateRange(laborType));
		sb.append(") C");
		return sb.toString();
	}

	/**
	 * Gets the condition for category date range.
	 * 
	 * @param laborType
	 *            the labor type
	 * @return String
	 */
	public static String getConditionForCategoryDateRange(String laborType) {
		StringBuilder sb = new StringBuilder("");
		if (laborType.equals(LaborType.LABOR_ENGINER.getType())) {
			sb.append("where category in ('Engineer-All','Engineer')");
		} else if (laborType.equals(LaborType.LABOR_TECHNICIAN.getType())) {
			sb.append("where category in ('Technician-All','Technician')");
		}
		return sb.toString();
	}

	/**
	 * Description - Gets query for Sr PM Labor Cost report.
	 * 
	 * @return String
	 */
	public static String getQueryForSrPMLaborCost() {
		StringBuilder sb = new StringBuilder(
				"select top 13 displayMonth = substring(convert(varchar(8), B.month, 3), 4,8), ");
		sb.append("col1 = convert(varchar(8), isnull(C.col1,0), 128), ");
		sb.append("col2 = convert(varchar(8), isnull(D.col2, 0), 128) ");
		sb.append("from ");
		sb.append("(select distinct top 13 month from ac_fact2_labor_rate_by_month order by month desc) B ");
		sb.append("left outer join ");
		sb.append("(select top 13 month, isnull(rate_average,0) as col1 from ac_fact2_labor_rate_by_month ");
		sb.append("where type = ? order by month desc) C on B.month = C.month ");
		sb.append("left outer join ");
		sb.append("(select top 13 month, isnull(rate_average,0) as col2 from ac_fact2_labor_rate_by_month ");
		sb.append("where type = ? order by month desc) D on B.month = D.month ");
		sb.append("where B.month between ? and ?  ");
		sb.append("order by B.month");

		return sb.toString();
	}

	/**
	 * Description - Gets query to retrieve date range of last 13 months
	 * starting from highest date in the table for Sr PM Labor Cost report
	 * 
	 * @return String
	 */
	public static String getQueryForRangeOfThirteenMonths() {
		StringBuilder sb = new StringBuilder(
				"select mthEnd , dateadd(mm, -12, mthEnd) as mthStart ");
		sb.append("from ");
		sb.append("(select max(month) as mthEnd from ac_fact2_labor_rate_by_month where type in (?,?)) A");

		return sb.toString();
	}

	/**
	 * Description - Gets query to retrieve maximum of average value for Sr PM
	 * Labor Cost report.
	 * 
	 * @return String
	 */
	public static String getQueryForMaxOfSrPmLaborCost() {

		StringBuilder sb = new StringBuilder(
				"select CEILING(max(col1)) as col1 , CEILING(max(col2)) as col2 ");
		sb.append("from ( ");
		sb.append(ReportStaticData.getQueryForSrPMLaborCost());
		sb.append(") Z");

		return sb.toString();
	}

	/**
	 * Description - Gets query to retrieve Labor Cost values against 13 months
	 * for all SPM stored in the table.
	 * 
	 * @return String
	 */
	public static String getQueryForLaborCostBySPMName() {

		StringBuilder sb = new StringBuilder(
				"select top 100 percent spm, displayMonth = substring(convert(varchar(8), month, 3), 4,8), ");
		sb.append("rateAvg = convert(varchar(8), rate_average, 128) ");
		sb.append("from ac_fact2_labor_rate_month_spm ");
		sb.append("where type = ? and month between ? and ? ");
		sb.append("order by spm, month desc");

		return sb.toString();

	}

	/**
	 * Description - Gets query to retrieve maximum of average value among all
	 * SPM stored in the table for Sr PM Labor Cost report.
	 * 
	 * @return String
	 */
	public static String getQueryForMaxOfLaborCostBySPMName() {
		StringBuilder sb = new StringBuilder(
				"select CEILING(max(rateAvg)) as rateAvg from ( ");
		sb.append(ReportStaticData.getQueryForLaborCostBySPMName());
		sb.append(") Z");
		return sb.toString();
	}

	/**
	 * Description - Gets query for Location Wise Labor Cost report.
	 * 
	 * @return String
	 */
	public static String getBaseQueryForLocationWiseLabor() {
		StringBuilder sb = new StringBuilder(
				"select location, rate, [count], last_used = convert(varchar(8),date_last_used,1) ");
		sb.append("from dbo.ac_fact2_labor_rate_by_location ");
		sb.append("where type = ? and [count] > ? and international = ? ");
		sb.append(" and date_last_used between ? and ? ");
		return sb.toString();
	}

	/**
	 * Gets the base query for multi axis labor cost.
	 * 
	 * @return String
	 */
	public static String getBaseQueryForMultiAxisLaborCost() {
		StringBuilder sb = new StringBuilder(
				"select type, substring(convert(varchar(8), month, 3), 4,8) as display_month, ");
		sb.append("convert(varchar(8), rate_average, 128) as avrg_rate, month,rate_average");
		sb.append(" from ac_fact2_labor_rate_by_month ");
		sb.append(" where type in (?,?,?)");
		sb.append(" and  month between ? and ?");

		return sb.toString();
	}

	/**
	 * Gets the base query for minuteman cost.
	 * 
	 * @return String
	 */
	public static String getBaseQueryForMinutemanCost() {
		StringBuilder sb = new StringBuilder(
				"select purchase_order_type, substring(convert(varchar(8), month, 3), 4,8) as display_month, ");
		sb.append("convert(int,cost) as cost1, month,cost");
		sb.append(" from ac_fact2_purchase_order_type_summary_by_month ");
		sb.append(" where purchase_order_type in (?,?,?)");
		sb.append(" and project_type in (?)");
		sb.append(" and  month between ? and ?");

		return sb.toString();
	}

	/**
	 * Gets the base query for Minuteman Availability /Usage Chart. CURRENT
	 * MONTH IS TO BE INCLUDED SO UPPER LIMIT OF cp_pt_created_date NOT DEFINED
	 * 
	 * @return the base query for mm util availablity
	 */
	public static String getBaseQueryForMMUtilAvailablity() {
		StringBuilder sb = new StringBuilder(
				"select sc_dd_month_mm01yyyy from ilex.dbo.cp_partner");
		sb.append(" join datawarehouse.dbo.ac_dimension_date on convert(varchar(10), cp_pt_created_date,121) = sc_dd_mmddyyyy ");
		sb.append(" where cp_pt_partner_type = 'M' and cp_pt_created_date >= ? ");
		sb.append(" group by sc_dd_month_mm01yyyy");
		return sb.toString();
	}

	/**
	 * Gets the base query for minuteman occurences.
	 * 
	 * @return String
	 */
	public static String getBaseQueryForMinutemanOccurences() {
		StringBuilder sb = new StringBuilder(
				"select purchase_order_type, substring(convert(varchar(8), month, 3), 4,8) as display_month, ");
		sb.append("month,occurrences as occurences");
		sb.append(" from ac_fact2_purchase_order_type_summary_by_month ");
		sb.append(" where purchase_order_type in (?,?,?)");
		sb.append(" and project_type in (?)");
		sb.append(" and  month between ? and ?");

		return sb.toString();
	}

	public static String getBaseQueryForMMAavailability() {
		StringBuilder sb = new StringBuilder(" select ");
		sb.append(" ac_mu_available_in_period as totalMM, ");
		sb.append(" ac_mu_used_in_period as  usedMM, ");
		sb.append(" ac_mu_not_used_in_period as unUsedMM ");
		sb.append(" from ");
		sb.append(" ac_fact2_minuteman_usage ");

		return sb.toString();
	}

	/**
	 * Gets the get Base Query For Minuteman First Time Usage
	 * 
	 * @return String
	 */
	public static String getBaseQueryForMinutemanFirstTimeUsage(String filter) {
		StringBuilder sb = new StringBuilder(
				"with names_cross_months(unique_name,mm_yyyy)");
		sb.append(" as(select distinct ac_mr_user,sc_dd_month_mm01yyyy from ac_fact2_mimuteman_usage_rollup cross join ac_dimension_date_months where sc_dd_month_mm01yyyy in ('1/1/12','2/1/12','3/1/12','4/1/12','5/1/12','6/1/12','7/1/12','8/1/12','9/1/12','10/1/12','11/1/12','12/1/12') ) ");
		sb.append(" select mm_yyyy,unique_name,isnull(ac_mr_count,0)as occurance ");
		sb.append(" from names_cross_months ");
		sb.append(" left join ac_fact2_mimuteman_usage_rollup ");
		sb.append(" on mm_yyyy=ac_mr_dt_stamp ");
		sb.append(" and unique_name =ac_mr_user ");
		sb.append(" and ac_mr_role='" + filter + "' ");
		sb.append(" order by unique_name,mm_yyyy ");
		return sb.toString();
	}

	/**
	 * Gets the base query for multi axis sr pm labor cost.
	 * 
	 * @return String
	 */
	public static String getBaseQueryForMultiAxisSrPmLaborCost() {
		StringBuilder sb = new StringBuilder(
				getBaseQueryForMultiAxisLaborCost());
		sb.append(" union ");
		sb.append("select spm as type, substring(convert(varchar(8), month, 3), 4,8) as display_month, ");
		sb.append("convert(varchar(8), rate_average, 128) as avrg_rate, month,rate_average");
		sb.append(" from ac_fact2_labor_rate_month_spm ");
		sb.append(" where type = 'Technician-All' ");
		sb.append(" and  month between ? and ?");

		return sb.toString();
	}

	/**
	 * Gets the query for multi axis labor cost.
	 * 
	 * @return String
	 */
	public static String getQueryForMultiAxisLaborCost() {
		StringBuilder sb = new StringBuilder(
				getBaseQueryForMultiAxisLaborCost());
		sb.append(" order by type, month");

		return sb.toString();
	}

	/**
	 * Gets the query for minuteman cost.
	 * 
	 * @return String
	 */
	public static String getQueryForMinutemanCost() {
		StringBuilder sb = new StringBuilder(getBaseQueryForMinutemanCost());
		sb.append(" ORDER BY  (CASE purchase_order_type  WHEN 'M' THEN 1 WHEN 'S' THEN 2 WHEN 'P' THEN 3 END),[month]");

		return sb.toString();
	}

	/**
	 * Gets the Result Set query Minuteman Availability /Usage Chart.
	 * 
	 * @return the query for mm util availablity
	 */
	public static String getQueryForMMUtilAvailablity() {
		StringBuilder sb = new StringBuilder(
				"select count(*) AS totalMinuteman ,sc_dd_month_mm01yyyy as month,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month,'M' AS partner_type");
		sb.append(" from ilex.dbo.cp_partner");
		sb.append(" join datawarehouse.dbo.ac_dimension_date on convert(varchar(10), cp_pt_created_date,121) = sc_dd_mmddyyyy");
		sb.append(" where cp_pt_partner_type = 'M' and cp_pt_created_date >= ? ");
		sb.append(" group by sc_dd_month_mm01yyyy");
		sb.append(" order by sc_dd_month_mm01yyyy");
		return sb.toString();
	}

	/**
	 * Gets the query for total value for each month.
	 * 
	 * @param reportName
	 *            the report name
	 * @return String
	 */
	public static String getQueryForTotalValueForEachMonth(String reportName) {
		if (reportName.equals("USAGE_BY_COST")) {
			StringBuilder sb = new StringBuilder(
					"SELECT SUM(cost) as Sum,substring(convert(varchar(8), month, 3), 4,8) as display_month FROM dbo.ac_fact2_purchase_order_type_summary_by_month where project_type= ? AND [month] between ? and ? GROUP BY [MONTH] ");
			return sb.toString();
		} else {
			StringBuilder sb = new StringBuilder(
					"SELECT SUM(occurrences) as Sum,substring(convert(varchar(8), month, 3), 4,8) as display_month FROM dbo.ac_fact2_purchase_order_type_summary_by_month where project_type= ? AND [month] between ? and ? GROUP BY [MONTH] ");
			return sb.toString();
		}

	}

	/**
	 * Gets the query for minuteman occurences.
	 * 
	 * @return String
	 */
	public static String getQueryForMinutemanOccurences() {
		StringBuilder sb = new StringBuilder(
				getBaseQueryForMinutemanOccurences());
		sb.append("ORDER BY  (CASE purchase_order_type  WHEN 'M' THEN 1 WHEN 'S' THEN 2WHEN 'P' THEN 3 END),[month]");

		return sb.toString();
	}

	/**
	 * Gets the query for multi axis sr pm labor cost.
	 * 
	 * @return String
	 */
	public static String getQueryForMultiAxisSrPmLaborCost() {
		StringBuilder sb = new StringBuilder("select * from ( ");
		sb.append(getBaseQueryForMultiAxisSrPmLaborCost());
		sb.append(") C order by type, month");

		return sb.toString();
	}

	/**
	 * Gets the query for count no of records for Minuteman Availability /Usage
	 * Chart.
	 * 
	 * @param query
	 *            the query
	 * @return String
	 */
	public static String getQueryForCountNoOfRecords(String query) {
		StringBuilder sb = new StringBuilder("select count(*) from (");
		sb.append(query);
		sb.append(") A");

		return sb.toString();
	}

	/**
	 * Gets the query for revenue financial count no of records.
	 * 
	 * @param query
	 *            the query
	 * @return String
	 */
	public static String getQueryForRevenueFinancialCountNoOfRecords(
			String query) {
		StringBuilder sb = new StringBuilder("select count(*) from (");
		sb.append(query);
		sb.append(") A");

		return sb.toString();
	}

	/**
	 * Gets the query formax y axis category.
	 * 
	 * @param query
	 *            the query
	 * @return String
	 */
	public static String getQueryFormaxYAxisCategory(String query) {
		StringBuilder sb = new StringBuilder("select max(rate_average) from (");
		sb.append(query);
		sb.append(") A");

		return sb.toString();
	}

	/**
	 * Gets the revenue query for financial report.
	 * 
	 * @param reportType
	 *            the report type
	 * @return String
	 */
	public static String getRevenueQueryForFinancialReport(String reportType) {
		StringBuilder sb = new StringBuilder("select 'Revenue' as type, ");
		sb.append("substring(convert(varchar(8), month, 3), 4,8) as display_month, ");
		sb.append("revenue as avrg_rate,month from ac_fact2_revenue_by_role ");
		sb.append(getConditioinForFinance(reportType));
		sb.append(" and  month between ? and ?");
		return sb.toString();
	}

	/**
	 * Gets the occurences query for financial report.
	 * 
	 * @param reportType
	 *            the report type
	 * @return String
	 */
	public static String getOccurencesQueryForFinancialReport(String reportType) {
		StringBuilder sb = new StringBuilder(" select 'Occurrences' as type, ");
		sb.append("substring(convert(varchar(8), month, 3), 4,8) as display_month, ");
		sb.append("occurrences as avrg_rate,month from ac_fact2_revenue_by_role ");
		sb.append(getConditioinForFinance(reportType));
		sb.append(" and  month between ? and ?");
		return sb.toString();
	}

	/**
	 * Gets the rolle name query for financial report.
	 * 
	 * @param reportType
	 *            the report type
	 * @return String
	 */
	public static String getRolleNameQueryForFinancialReport(String reportType) {
		StringBuilder sb = new StringBuilder(" select rolee_name as type, ");
		sb.append("substring(convert(varchar(8), month, 3), 4,8) as display_month, ");
		sb.append("revenue as avrg_rate,month from ac_fact2_revenue_by_role ");
		sb.append(getConditioinForFinance(reportType));
		sb.append(" and  month between ? and ?");
		return sb.toString();
	}

	/**
	 * Gets the conditioin for finance.
	 * 
	 * @param reportType
	 *            the report type
	 * @return String
	 */
	public static String getConditioinForFinance(String reportType) {
		StringBuilder sb = new StringBuilder("");
		if (reportType.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT
				.getReportName())) {
			sb.append("where role_name = 'All' ");
		} else if (reportType
				.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_BySRPM
						.getReportName())) {
			sb.append("where role_name = 'SPM' and Occurrences > 5 ");
		} else if (reportType
				.equals(FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
						.getReportName())) {
			sb.append("where role_name = 'Job Owner' and rolee_name = ? ");
		}
		return sb.toString();
	}

	/**
	 * Gets the count query for financial report.
	 * 
	 * @return String
	 */
	public static String getCountQueryForFinancialReport() {
		StringBuilder sb = new StringBuilder("");
		sb.append(getRevenueQueryForFinancialReport(FinancialReportType.REVENUE_FINANCIAL_REPORT
				.getReportName()));
		sb.append(" union ");
		sb.append(getOccurencesQueryForFinancialReport(FinancialReportType.REVENUE_FINANCIAL_REPORT
				.getReportName()));
		return sb.toString();
	}

	/**
	 * Gets the count query for revenue financial report.
	 * 
	 * @param jobFilterType
	 *            the job filter type
	 * @param reportName
	 *            the report name
	 * @return String
	 */
	public static String getCountQueryForRevenueFinancialReport(
			String jobFilterType, String reportName) {
		StringBuilder sb = new StringBuilder("");
		sb.append("select  count(pf_exp)+count(exp)+count(rev)+count(pf_vgpm)+count(vgpm)");
		if (reportName.equals("REVENUE_FINANCIAL_REPORT_ByJobOwner"))
			sb.append("+count(delta)");
		sb.append("+count(occ) from (select sc_dd_month_mm01yyyy as mmyy, sum(ac_ru_pro_forma) as pf_exp, sum(ac_ru_expense) as exp, ");
		sb.append("sum(ac_ru_revenue) as rev, ");
		sb.append("case WHEN sum(ac_ru_revenue)>0 then(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))ELSE 0.00 END as pf_vgpm, ");
		sb.append("case WHEN sum(ac_ru_revenue)>0   then(1-sum(ac_ru_expense)/sum(ac_ru_revenue))ELSE 0.00 END as vgpm, ");
		if (reportName.equals("REVENUE_FINANCIAL_REPORT_ByJobOwner"))
			sb.append("case when sum(ac_ru_revenue) > 0.0 then (1-sum(ac_ru_expense)/sum(ac_ru_revenue))-(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))else 0.0 end as delta, ");
		sb.append(" count(*) as occ ");
		sb.append("from dbo.ac_dimensions_project ");
		sb.append("join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append("join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append("join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append("join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		if (reportName.equals("REVENUE_FINANCIAL_REPORT_ByJobOwner")) {
			sb.append("where ac_jd_date_closed >= dateadd(mm, -11, ?)");
			sb.append("and ac_cl_major_classification in ('Project','NetMedX')");
			sb.append("and ac_jd_job_owner = ? ");
			sb.append("group by sc_dd_month_mm01yyyy) A");
		} else {
			sb.append("where ac_jd_date_closed >= dateadd(mm, -11, ? ) ");
			if (jobFilterType.equals("AllJobs"))
				sb.append(" and ac_cl_major_classification like ? ");
			else
				sb.append(" and ac_cl_major_classification = ? ");
			sb.append(" and ac_cl_minor_classification like ? ");
			sb.append("group by sc_dd_month_mm01yyyy) A ");
		}
		return sb.toString();
	}

	/**
	 * Gets the revenue expense tool text value.
	 * 
	 * @param columnName
	 *            the column name
	 * @param displayMonth
	 *            the display month
	 * @param majorClassification
	 *            the major classification
	 * @param minorClassification
	 *            the minor classification
	 * @param endDate
	 *            the end date
	 * @return String
	 */
	public static String getRevenueExpenseToolTextValue(String columnName,
			String displayMonth, String majorClassification,
			String minorClassification, String endDate) {
		StringBuilder sb = new StringBuilder("");
		sb.append("select ");
		sb.append(columnName);
		sb.append(" from(select ");
		sb.append("substring(CONVERT(VARCHAR(8), [sc_dd_month_mm01yyyy], 3), 4,8) as display, ");
		sb.append("sum(ac_ru_expense) as expense, ");
		sb.append("CASE WHEN sum(ac_ru_revenue)>0.0 THEN  (1-sum(ac_ru_expense)/sum(ac_ru_revenue)) ELSE 0.00 END  as vgpm, ");
		sb.append(" CASE WHEN sum(ac_ru_revenue)>0.0 THEN (1-sum(ac_ru_expense)/sum(ac_ru_revenue)) -");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) ELSE 0.00 END  as delta ");
		sb.append("from dbo.ac_dimensions_project ");
		sb.append("join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append("join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append("join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append("join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		sb.append("where ac_jd_date_closed >= dateadd(mm, -11,'" + endDate);
		sb.append("') " + " and ac_cl_major_classification like '");
		sb.append(majorClassification + "' ");
		sb.append(" and ac_cl_minor_classification like '");
		sb.append(minorClassification + "'" + " group by sc_dd_month_mm01yyyy ");
		sb.append(" ) A where display='" + displayMonth);
		sb.append("'  order by display ");

		return sb.toString();
	}

	/**
	 * Gets the count query for financial report by srpm.
	 * 
	 * @return String
	 */
	public static String getCountQueryForFinancialReportBySRPM() {
		StringBuilder sb = new StringBuilder("");
		sb.append(getRevenueQueryForFinancialReport(FinancialReportType.REVENUE_FINANCIAL_REPORT
				.getReportName()));
		sb.append(" union ");
		sb.append(getRolleNameQueryForFinancialReport(FinancialReportType.REVENUE_FINANCIAL_REPORT_BySRPM
				.getReportName()));
		return sb.toString();
	}

	/**
	 * Gets the count query for financial report by job owner.
	 * 
	 * @return String
	 */
	public static String getCountQueryForFinancialReportByJobOwner() {
		StringBuilder sb = new StringBuilder("");
		sb.append(getRevenueQueryForFinancialReport(FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
				.getReportName()));
		sb.append(" union ");
		sb.append(getOccurencesQueryForFinancialReport(FinancialReportType.REVENUE_FINANCIAL_REPORT_ByJobOwner
				.getReportName()));
		return sb.toString();
	}

	/**
	 * Gets the query for finance revenue bean list.
	 * 
	 * @return String
	 */
	public static String getQueryForFinanceRevenueBeanList() {
		StringBuilder sb = new StringBuilder(getCountQueryForFinancialReport());

		sb.append(" order by type, month");
		return sb.toString();
	}

	/**
	 * Gets the query for revenue finance report bean list.
	 * 
	 * @param jobFilterType
	 *            the job filter type
	 * @return String
	 */
	public static String getQueryForRevenueFinanceReportBeanList(
			String jobFilterType) {
		StringBuilder sb = new StringBuilder("");

		sb.append("select * from (select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month, sum(ac_ru_pro_forma) as avrg_rate,'Pro Forma Expense' as type from dbo.ac_dimensions_project ");
		sb.append(getConditionspart());
		if (jobFilterType.equals("AllJobs"))
			sb.append(" and ac_cl_major_classification like ? ");
		else
			sb.append(" and ac_cl_major_classification = ? ");
		sb.append(" and ac_cl_minor_classification like ? ");
		sb.append("group by sc_dd_month_mm01yyyy ");
		sb.append("union all ");
		sb.append("select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month, sum(ac_ru_expense) as exp,'Expense' as type from dbo.ac_dimensions_project ");
		sb.append(getConditionspart());
		if (jobFilterType.equals("AllJobs"))
			sb.append(" and ac_cl_major_classification like ? ");
		else
			sb.append(" and ac_cl_major_classification = ? ");
		sb.append(" and ac_cl_minor_classification like ?");
		sb.append("group by sc_dd_month_mm01yyyy ");
		sb.append("union all ");
		sb.append("select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month,  sum(ac_ru_revenue) as rev,'Revenue' as type from dbo.ac_dimensions_project ");
		sb.append(getConditionspart());
		if (jobFilterType.equals("AllJobs"))
			sb.append(" and ac_cl_major_classification like ? ");
		else
			sb.append(" and ac_cl_major_classification = ? ");
		sb.append(" and ac_cl_minor_classification like ? ");
		sb.append("group by sc_dd_month_mm01yyyy ");
		sb.append("union all ");
		sb.append("select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month, case WHEN sum(ac_ru_revenue)>0 then(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))ELSE 0.00 END as pf_vgpm,'Pro Forma VGPM' as type from dbo.ac_dimensions_project ");
		sb.append(getConditionspart());
		if (jobFilterType.equals("AllJobs"))
			sb.append(" and ac_cl_major_classification like ? ");
		else
			sb.append(" and ac_cl_major_classification = ? ");
		sb.append(" and ac_cl_minor_classification like ? ");
		sb.append("group by sc_dd_month_mm01yyyy ");
		sb.append("union all ");
		sb.append("select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month,case WHEN sum(ac_ru_revenue)>0   then(1-sum(ac_ru_expense)/sum(ac_ru_revenue))ELSE 0.00 END as vgpm,'VGPM' as type from dbo.ac_dimensions_project ");
		sb.append(getConditionspart());
		if (jobFilterType.equals("AllJobs"))
			sb.append(" and ac_cl_major_classification like ? ");
		else
			sb.append(" and ac_cl_major_classification = ? ");
		sb.append(" and ac_cl_minor_classification like ? ");
		sb.append("group by sc_dd_month_mm01yyyy ");
		sb.append("union all ");
		sb.append("select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month,count(*) as occ,'Occurrences' as type from dbo.ac_dimensions_project ");
		sb.append(getConditionspart());
		if (jobFilterType.equals("AllJobs"))
			sb.append(" and ac_cl_major_classification like ? ");
		else
			sb.append(" and ac_cl_major_classification = ? ");
		sb.append(" and ac_cl_minor_classification like ? ");
		sb.append("group by sc_dd_month_mm01yyyy ) A ");
		sb.append("order by (CASE type when 'Pro Forma Expense' then 1 when 'Expense' then 2 when 'Revenue' then 3 when 'Pro Forma VGPM' then 4 when 'VGPM' then 5 when 'Occurrences' then 6 END), mmyy");
		return sb.toString();
	}

	/**
	 * Gets the query for finance revenue by job owner bean list.
	 * 
	 * @return String
	 */
	public static String getQueryForFinanceRevenueByJobOwnerBeanList() {
		StringBuilder sb = new StringBuilder(
				getCountQueryForFinancialReportByJobOwner());

		sb.append(" order by type, month");
		return sb.toString();
	}

	/**
	 * Gets the query for revenue expense by job owner bean list.
	 * 
	 * @return String
	 */
	public static String getQueryForRevenueExpenseByJobOwnerBeanList() {
		StringBuilder sb = new StringBuilder(
				"select * from (select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month, sum(ac_ru_pro_forma) as avrg_rate,'Pro Forma Expense' as type ");
		sb.append(getConditionpart());
		sb.append("union all ");
		sb.append(" select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month, sum(ac_ru_expense) as exp,'Expense' as type ");
		sb.append(getConditionpart());
		sb.append(" union all ");
		sb.append(" select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month,  sum(ac_ru_revenue) as rev,'Revenue' as type from dbo.ac_dimensions_project ");
		sb.append(getConditionpart());
		sb.append("union all ");
		sb.append("select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month, case when sum(ac_ru_revenue)>0 then  (1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.0 end as pf_vgpm,'Pro Forma VGPM' as type from dbo.ac_dimensions_project ");
		sb.append(getConditionpart());
		sb.append("union all ");
		sb.append("select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month,case when sum(ac_ru_revenue)>0 then (1-sum(ac_ru_expense)/sum(ac_ru_revenue)) else 0.0 end  as vgpm,'VGPM' as type ");
		sb.append(getConditionpart());
		sb.append("union all ");
		sb.append("	select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month,case when sum(ac_ru_revenue)>0 then sum(ac_ru_pro_forma)/sum(ac_ru_revenue) else 0.0 end as delta,'Delta VGPM' as type ");
		sb.append(getConditionpart());
		sb.append("	union all ");
		sb.append("	select sc_dd_month_mm01yyyy as mmyy,substring(convert(varchar(8), sc_dd_month_mm01yyyy, 3), 4,8) as display_month,count(*) as occ,'Occurrences' as type ");
		sb.append(getConditionpart());
		sb.append(") A ");
		sb.append("	order by (CASE type when 'Pro Forma Expense' then 1 when 'Expense' then 2 when 'Revenue' then 3 when 'Pro Forma VGPM' then 4 when 'VGPM' then 5 when 'Delta VGPM' then 6 when 'Occurrences' then 7 END), mmyy");

		return sb.toString();
	}

	/**
	 * Gets the query for finance revenue by srpm bean list.
	 * 
	 * @return String
	 */
	public static String getQueryForFinanceRevenueBySRPMBeanList() {
		StringBuilder sb = new StringBuilder(
				getCountQueryForFinancialReportBySRPM());

		sb.append(" order by type, month");
		return sb.toString();
	}

	/**
	 * Gets the query for job owner details.
	 * 
	 * @return String
	 */
	public static String getQueryForJobOwnerDetails() {
		StringBuilder sb = new StringBuilder("select rolee_name as jobOwner,");
		sb.append("convert(varchar(12),revenue, 128) as revenue,");
		sb.append("occurrences ");
		sb.append("from ac_fact2_revenue_by_role ");
		sb.append("where role_name = 'Job Owner All'and occurrences > 10 ");
		sb.append(" order by rolee_name");
		return sb.toString();
	}

	/**
	 * Gets the query for revenue job owners details.
	 * 
	 * @param param
	 *            the param
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param classification
	 *            the classification
	 * @return String
	 */
	@SuppressWarnings("unused")
	public static String getQueryForRevenueJobOwnersDetails(String param,
			String fromDate, String toDate, String classification[]) {
		String defaultValue = "firstTime";
		int length = 0;
		if (null == classification)
			length = 0;
		else
			length = classification.length;
		String empty = "";
		StringBuilder sb = new StringBuilder(
				"select ac_jd_job_owner as jobOwnerId, ");
		sb.append("ac_du_name as jobOwner , ");
		sb.append(getCommonPart());
		sb.append(" join ac_dimension_user on ac_jd_job_owner = ac_du_lo_pc_id ");
		if (defaultValue.equals(param)) {
			sb.append("where ac_cl_major_classification in ('Project','NetMedX') ");
			sb.append(" and ac_jd_date_closed >= CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(getdate())-1),getdate()),101)");
			sb.append("and ac_du_job_owner = 1 ");
		} else {
			sb.append("where ");

			// condition for the condition check on the
			// ac_cl_major_classification
			if (null != classification) { // Blank check for the
											// classification[]
				for (int i = 0; i < classification.length; ++i) {
					if (i == 0) { // To add the first where condition
						if (classification[i].equals("Provisioning/Deployment"))
							sb.append("( ac_cl_major_classification='MSP' and ac_cl_minor_classification!='Operations'");
						else
							sb.append("( ac_cl_major_classification ='"
									+ classification[i] + "'");
					} else {
						if (classification[i].equals("Provisioning/Deployment"))
							sb.append(" or (ac_cl_major_classification='MSP' and ac_cl_minor_classification!='Operations')");
						else
							sb.append(" or ac_cl_major_classification ='"
									+ classification[i] + "'");
					}
				}
			} else {
				sb.append(" ");
			}
			// condition check for the date part of the query
			if ((empty != fromDate) && (empty != toDate)) {
				if (length == 0) { // if classification[] is blank
					sb.append(" ((ac_jd_date_closed>='" + fromDate + "')");
					sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
					sb.append(" and ac_du_job_owner = 1 ");
				} else { // classification[] is not blank
					sb.append(") and ((ac_jd_date_closed>='" + fromDate + "')");
					sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
					sb.append(" and  ac_du_job_owner  = 1 ");
				}
			} else if ((empty == fromDate) && (empty == toDate)) {
				if (length == 0) // if classification[] is blank
					sb.append(" ac_du_job_owner = 1 ");
				else { // classification[] is not blank
					sb.append(") ");
					sb.append("and ac_du_job_owner = 1 ");
				}
			} else if ((empty == fromDate) && (empty != toDate))
				if (length == 0) { // if classification[] is blank
					sb.append("(ac_jd_date_closed<='" + toDate + "')");
					sb.append("and ac_du_job_owner = 1 ");
				} else { // classification[] is not blank
					sb.append(" ) and (ac_jd_date_closed<='" + toDate + "')");
					sb.append("and ac_du_job_owner = 1 ");
				}
			else if (length == 0) { // if classification[] is blank
				sb.append(" (ac_jd_date_closed>='" + fromDate + "')");
				sb.append("and ac_du_job_owner = 1 ");
			} else { // classification[] is not blank
				sb.append(" ) and (ac_jd_date_closed>='" + fromDate + "')");
				sb.append("and ac_du_job_owner = 1 ");
			}
		}
		sb.append("group by ac_jd_job_owner,ac_du_name ");
		sb.append("order by ac_du_name");
		return sb.toString();
	}

	/**
	 * Gets the query for revenue project managers details.
	 * 
	 * @param param
	 *            the param
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param classification
	 *            the classification
	 * @return String
	 */
	@SuppressWarnings("unused")
	public static String getQueryForRevenueProjectManagerDetails(String param,
			String fromDate, String toDate, String classification[]) {
		String defaultValue = "firstTime";
		int length = 0;
		if (null == classification)
			length = 0;
		else
			length = classification.length;
		String empty = "";
		StringBuilder sb = new StringBuilder(
				"select ac_jd_project_manager as projectManagerId, ");
		sb.append("ac_du_name as projectManager , ");
		sb.append(getCommonPart());
		sb.append(" join ac_dimension_user on ac_jd_project_manager = ac_du_lo_pc_id ");
		if (defaultValue.equals(param)) {
			sb.append("where ac_cl_major_classification in ('Project','NetMedX') ");
			sb.append(" and ac_jd_date_closed >= CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(getdate())-1),getdate()),101)");
			sb.append("and ac_du_job_owner = 1 ");
		} else {
			sb.append("where ");

			// condition for the condition check on the
			// ac_cl_major_classification
			if (null != classification) { // Blank check for the
											// classification[]
				for (int i = 0; i < classification.length; ++i) {
					if (i == 0) { // To add the first where condition
						if (classification[i].equals("Provisioning/Deployment"))
							sb.append("( ac_cl_major_classification='MSP' and ac_cl_minor_classification!='Operations'");
						else
							sb.append("( ac_cl_major_classification ='"
									+ classification[i] + "'");
					} else {
						if (classification[i].equals("Provisioning/Deployment"))
							sb.append(" or (ac_cl_major_classification='MSP' and ac_cl_minor_classification!='Operations')");
						else
							sb.append(" or ac_cl_major_classification ='"
									+ classification[i] + "'");
					}
				}
			} else {
				sb.append(" ");
			}
			// condition check for the date part of the query
			if ((empty != fromDate) && (empty != toDate)) {
				if (length == 0) { // if classification[] is blank
					sb.append(" ((ac_jd_date_closed>='" + fromDate + "')");
					sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
					sb.append(" and ac_du_job_owner = 1 ");
				} else { // classification[] is not blank
					sb.append(") and ((ac_jd_date_closed>='" + fromDate + "')");
					sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
					sb.append(" and  ac_du_job_owner  = 1 ");
				}
			} else if ((empty == fromDate) && (empty == toDate)) {
				if (length == 0) // if classification[] is blank
					sb.append(" ac_du_job_owner = 1 ");
				else { // classification[] is not blank
					sb.append(") ");
					sb.append("and ac_du_job_owner = 1 ");
				}
			} else if ((empty == fromDate) && (empty != toDate))
				if (length == 0) { // if classification[] is blank
					sb.append("(ac_jd_date_closed<='" + toDate + "')");
					sb.append("and ac_du_job_owner = 1 ");
				} else { // classification[] is not blank
					sb.append(" ) and (ac_jd_date_closed<='" + toDate + "')");
					sb.append("and ac_du_job_owner = 1 ");
				}
			else if (length == 0) { // if classification[] is blank
				sb.append(" (ac_jd_date_closed>='" + fromDate + "')");
				sb.append("and ac_du_job_owner = 1 ");
			} else { // classification[] is not blank
				sb.append(" ) and (ac_jd_date_closed>='" + fromDate + "')");
				sb.append("and ac_du_job_owner = 1 ");
			}
		}
		sb.append("group by ac_jd_project_manager,ac_du_name ");
		sb.append("order by ac_du_name");
		return sb.toString();
	}

	/**
	 * Gets the query for revenue customer details.
	 * 
	 * @param param
	 *            the param
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param classification
	 *            the classification
	 * @return the query for revenue customer details
	 */
	public static String getQueryForRevenueCustomerDetails(String param,
			String fromDate, String toDate, String classification[]) {
		String defaultValue = "firstTime";
		int length = 0;
		if (null == classification)
			length = 0;
		else
			length = classification.length;
		String empty = "";
		StringBuilder sb = new StringBuilder("select lp_mm_id as customerId, ");
		sb.append("lo_ot_name as customerName , ");
		sb.append(getCommonPart());
//		sb.append(" join ac_dimension_user on ac_jd_job_owner = ac_du_lo_pc_id ");
		sb.append(" join ilex.dbo.ap_appendix_list_01 on ac_pd_pr_id=lx_pr_id ");
		if (defaultValue.equals(param)) {
			sb.append("where ac_cl_major_classification in ('Project','NetMedX') ");
			sb.append(" and ac_jd_date_closed >= CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(getdate())-1),getdate()),101)");
			sb.append("group by lo_ot_name,lp_mm_id ");
			sb.append("order by lo_ot_name,lp_mm_id");
		} else if ((empty == fromDate) && (empty == toDate) && (length == 0)) {
			sb.append("group by lo_ot_name,lp_mm_id ");
			sb.append("order by lo_ot_name,lp_mm_id");
		} else {
			sb.append("where ");
			// condition for the condition check on the
			// ac_cl_major_classification
			if (null != classification) { // Blank check for the
											// classification[]
				for (int i = 0; i < classification.length; ++i) {
					if (i == 0) { // To add the first where condition
						if (classification[i].equals("Provisioning/Deployment"))
							sb.append("( ac_cl_major_classification='MSP' and ac_cl_minor_classification!='Operations'");
						else
							sb.append("( ac_cl_major_classification ='"
									+ classification[i] + "'");
					} else {
						if (classification[i].equals("Provisioning/Deployment"))
							sb.append(" or (ac_cl_major_classification='MSP' and ac_cl_minor_classification!='Operations')");
						else
							sb.append(" or ac_cl_major_classification ='"
									+ classification[i] + "'");
					}
				}
			} else {
				sb.append(" ");
			}
			// condition check for the date part of the query
			if ((empty != fromDate) && (empty != toDate)) {
				if (length == 0) { // if classification[] is blank
					sb.append(" ((ac_jd_date_closed>='" + fromDate + "')");
					sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
				} else { // classification[] is not blank
					sb.append(") and ((ac_jd_date_closed>='" + fromDate + "')");
					sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
				}
			} else if ((empty == fromDate) && (empty == toDate)) {
				if (length == 0) {
				} // if classification[] is blank
				else { // classification[] is not blank
					sb.append(") ");
				}
			} else if ((empty == fromDate) && (empty != toDate))
				if (length == 0) { // if classification[] is blank
					sb.append("(ac_jd_date_closed<='" + toDate + "')");
				} else { // classification[] is not blank
					sb.append(" ) and (ac_jd_date_closed<='" + toDate + "')");
				}
			else if (length == 0) { // if classification[] is blank
				sb.append(" (ac_jd_date_closed>='" + fromDate + "')");
			} else { // classification[] is not blank
				sb.append(" ) and (ac_jd_date_closed>='" + fromDate + "')");
			}
			sb.append("group by lo_ot_name,lp_mm_id ");
			sb.append("order by lo_ot_name,lp_mm_id");
		}

		return sb.toString();
	}

	/**
	 * Gets the query for revenue spm details.
	 * 
	 * @param param
	 *            the param
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param classification
	 *            the classification
	 * @return String
	 */
	public static String getQueryForRevenueSPMDetails(String param,
			String fromDate, String toDate, String classification[]) {
		String defaultValue = "firstTime";
		int length = 0;
		if (null == classification)
			length = 0;
		else
			length = classification.length;
		String empty = "";
		StringBuilder sb = new StringBuilder("select ac_jd_spm as spmId, ");
		sb.append("ac_du_name as spmName , ");
		sb.append(getCommonPart());
		//sb.append(" join ilex.dbo.lm_appendix_poc on lm_ap_pr_id = ac_jd_pd_pr_id ");
		//sb.append(" join ilex.dbo.lo_poc on lm_ap_pc_id = lo_pc_id ");
		sb.append(" join ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id ");
		if (defaultValue.equals(param)) {
			sb.append("where ac_cl_major_classification in ('Project','NetMedX') ");
			sb.append(" and ((ac_jd_date_closed>='" + fromDate + "')");
			sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
//			sb.append(" and ac_du_spm = 1 ");
		} else {
			sb.append("where ");

			// condition for the condition check on the
			// ac_cl_major_classification
			if (null != classification) { // Blank check for the
											// classification[]
				for (int i = 0; i < classification.length; ++i) {
					if (i == 0) { // To add the first where condition
						if (classification[i].equals("Provisioning/Deployment"))
							sb.append("( ac_cl_major_classification='MSP' and ac_cl_minor_classification!='Operations'");
						else
							sb.append("( ac_cl_major_classification ='"
									+ classification[i] + "'");
					} else {
						if (classification[i].equals("Provisioning/Deployment"))
							sb.append(" or (ac_cl_major_classification='MSP' and ac_cl_minor_classification!='Operations')");
						else
							sb.append(" or ac_cl_major_classification ='"
									+ classification[i] + "'");
					}
				}
			} else {
				sb.append(" ");
			}
			// condition check for the date part of the query
			if ((empty != fromDate) && (empty != toDate)) {
				if (length == 0) { // if classification[] is blank
					sb.append(" ((ac_jd_date_closed>='" + fromDate + "')");
					sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
//					sb.append(" and ac_du_spm = 1 ");
				} else { // classification[] is not blank
					sb.append(") and ((ac_jd_date_closed>='" + fromDate + "')");
					sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
//					sb.append(" and ac_du_spm = 1 ");
				}
			} else if ((empty == fromDate) && (empty == toDate)) {
				if (length == 0){} // if classification[] is blank
				//	sb.append(" ac_du_spm = 1 ");
				else { // classification[] is not blank
					sb.append(") ");
//					sb.append("and ac_du_spm = 1 ");
				}
			} else if ((empty == fromDate) && (empty != toDate))
				if (length == 0) { // if classification[] is blank
					sb.append("(ac_jd_date_closed<='" + toDate + "')");
//					sb.append("and ac_du_spm = 1 ");
				} else { // classification[] is not blank
					sb.append(" ) and (ac_jd_date_closed<='" + toDate + "')");
//					sb.append("and ac_du_spm = 1 ");
				}
			else if (length == 0) { // if classification[] is blank
				sb.append(" (ac_jd_date_closed>='" + fromDate + "')");
				//sb.append("and ac_du_spm = 1 ");
			} else { // classification[] is not blank
				sb.append(" ) and (ac_jd_date_closed>='" + fromDate + "')");
				//sb.append("and ac_du_spm = 1 ");
			}
		}
		sb.append(" group by ac_jd_spm, ac_du_name ");
		sb.append(" order by ac_du_name ");
		return sb.toString();
	}

	/**
	 * Gets the query for particular job owner details.
	 * 
	 * @param jobOwnerId
	 *            the job owner id
	 * @param reportName
	 *            the report name
	 * @param date
	 *            the date
	 * @return String
	 */
	public static String getQueryForParticularJobOwnerDetails(
			String jobOwnerId, String reportName, String date) {
		String reportType1 = "JobOwnerAccountSummary(TTM)";
		String reportType2 = "jobOwnerMonthPopupReport";
		StringBuilder sb = new StringBuilder("select lx_pr_id as accountID, ");
		sb.append("lo_ot_name+': '+lx_pr_title as accountName , ");
		sb.append("isnull(sum(ac_ru_pro_forma),'0') as proFormaExpense, ");
		sb.append("isnull(sum(ac_ru_expense),'0') as expense, ");
		sb.append("isnull(sum(ac_ru_revenue),'0') as revenue, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.0 end as proFormaVGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue)) else 0.0 end as VGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue))-");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))else 0.0 end as deltaVGPM, ");
		sb.append("isnull(count(*),'0') as occurrences ");
		sb.append("from dbo.ac_dimensions_project ");
		sb.append("join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append("join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append("join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append("join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		sb.append("join ac_dimension_user on ac_jd_job_owner = ac_du_lo_pc_id ");
		sb.append("join ilex.dbo.ap_appendix_list_01 on ac_pd_pr_id=lx_pr_id ");
		if (reportType1.equals(reportName)) {
			sb.append(" where ac_jd_date_closed >='" + startingMonthDate()
					+ "'");
			sb.append(" and ac_jd_job_owner = " + jobOwnerId);
		} else if (reportType2.equals(reportName)) {
			sb.append(" where RIGHT(CONVERT(VARCHAR(10), sc_dd_month_mm01yyyy , 103), 7)='"
					+ date + "'");
			sb.append(" and ac_jd_job_owner = " + jobOwnerId);
		} else {// for job owner account all pages
			sb.append(" where ac_jd_job_owner = " + jobOwnerId);
		}
		sb.append(" group by lo_ot_name,lx_pr_title,lx_pr_id ");
		sb.append("order by lo_ot_name,lx_pr_title,lx_pr_id");
		return sb.toString();
	}

	/**
	 * Gets the query for particular project manager details.
	 * 
	 * @param projectManagerId
	 *            the project Manager Id
	 * @param reportName
	 *            the report name
	 * @param date
	 *            the date
	 * @return String
	 */
	public static String getQueryForParticularProjectManagerDetails(
			String projectManagerId, String reportName, String date) {
		String reportType1 = "ProjectManagerAccountSummary(TTM)";
		String reportType2 = "ProjectManagerMonthPopupReport";
		StringBuilder sb = new StringBuilder("select lx_pr_id as accountID, ");
		sb.append("lo_ot_name+': '+lx_pr_title as accountName , ");
		sb.append("isnull(sum(ac_ru_pro_forma),'0') as proFormaExpense, ");
		sb.append("isnull(sum(ac_ru_expense),'0') as expense, ");
		sb.append("isnull(sum(ac_ru_revenue),'0') as revenue, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.0 end as proFormaVGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue)) else 0.0 end as VGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue))-");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))else 0.0 end as deltaVGPM, ");
		sb.append("isnull(count(*),'0') as occurrences ");
		sb.append("from dbo.ac_dimensions_project ");
		sb.append("join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append("join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append("join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append("join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		sb.append("join ac_dimension_user on ac_jd_job_owner = ac_du_lo_pc_id ");
		sb.append("join ilex.dbo.ap_appendix_list_01 on ac_pd_pr_id=lx_pr_id ");
		if (reportType1.equalsIgnoreCase(reportName)) {
			sb.append(" where ac_jd_date_closed >='" + startingMonthDate()
					+ "'");
			sb.append(" and ac_jd_project_manager = " + projectManagerId);
		} else if (reportType2.equalsIgnoreCase(reportName)) {
			sb.append(" where RIGHT(CONVERT(VARCHAR(10), sc_dd_month_mm01yyyy , 103), 7)='"
					+ date + "'");
			sb.append(" and ac_jd_project_manager = " + projectManagerId);
		} else {// for job owner account all pages
			sb.append(" where ac_jd_project_manager = " + projectManagerId);
		}
		sb.append(" group by lo_ot_name,lx_pr_title,lx_pr_id ");
		sb.append("order by lo_ot_name,lx_pr_title,lx_pr_id");
		return sb.toString();
	}

	public static String getQueryForParticularCustomerDetails(
			String customerId, String reportName, String date, String toDate, String fromDate) {
		StringBuilder sb = new StringBuilder("select lx_pr_id as accountID, ");
		sb.append("lx_pr_title as accountName , ");
		sb.append("isnull(sum(ac_ru_pro_forma),'0') as proFormaExpense, ");
		sb.append("isnull(sum(ac_ru_expense),'0') as expense, ");
		sb.append("isnull(sum(ac_ru_revenue),'0') as revenue, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.0 end as proFormaVGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue)) else 0.0 end as VGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue))-");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))else 0.0 end as deltaVGPM, ");
		sb.append("isnull(count(*),'0') as occurrences ");
		sb.append("from dbo.ac_dimensions_project ");
		sb.append("join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append("join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append("join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append("join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		sb.append("join ac_dimension_user on ac_jd_job_owner = ac_du_lo_pc_id ");
		sb.append("join ilex.dbo.ap_appendix_list_01 on ac_pd_pr_id=lx_pr_id ");
		sb.append(" where ");
		if(!toDate.equals("") && !fromDate.equals("")){
			sb.append("ac_jd_date_closed between '"+toDate+"' and '"+fromDate+"'");
			sb.append(" and ((ac_cl_major2 = 'Standard' and ac_cl_minor2 = 'Billable') "); 
					sb.append(" or (ac_cl_major2 = 'NetMedX' and ac_cl_minor2 = 'Billable') "); 
							sb.append(" or (ac_cl_major2 = 'MSP' and (ac_cl_minor2 = 'Billable' "); 
									sb.append(" or ac_cl_minor_classification = 'EverWorX Operations'))) ");
		}else{		
			sb.append("ac_jd_date_closed >='" + startingMonthDate() + "'");
		}
		sb.append(" and lp_mm_id = " + customerId);
		sb.append(" group by lx_pr_title,lx_pr_id ");
		sb.append("order by lx_pr_title,lx_pr_id");
		return sb.toString();
	}

	/**
	 * Gets the query for particular bdm details.
	 * 
	 * @param bdmId
	 *            the bdm id
	 * @param reportName
	 *            the report name
	 * @param date
	 *            the date
	 * @return String
	 */
	public static String getQueryForParticularBDMDetails(String bdmId,
			String reportName, String date) {
		String reportType1 = "BDMAccountSummary(TTM)";
		String reportType2 = "bdmMonthPopupReport";
		StringBuilder sb = new StringBuilder("select lx_pr_id as accountID, ");
		sb.append("lo_ot_name+': '+lx_pr_title as accountName , ");
		sb.append("isnull(sum(ac_ru_pro_forma),'0') as proFormaExpense, ");
		sb.append("isnull(sum(ac_ru_expense),'0') as expense, ");
		sb.append("isnull(sum(ac_ru_revenue),'0') as revenue, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.0 end as proFormaVGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue)) else 0.0 end as VGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue))-");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))else 0.0 end as deltaVGPM, ");
		sb.append("isnull(count(*),'0') as occurrences ");
		sb.append("from dbo.ac_dimensions_project ");
		sb.append("join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append("join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append("join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append("join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		sb.append("join ac_dimension_user on ac_jd_bdm = ac_du_lo_pc_id ");
		sb.append("join ilex.dbo.ap_appendix_list_01 on ac_pd_pr_id=lx_pr_id ");
		if (reportType1.equals(reportName)) {
			sb.append(" where ac_jd_date_closed >='" + startingMonthDate()
					+ "'");
			sb.append(" and ac_jd_bdm = " + bdmId);
		} else if (reportType2.equals(reportName)) {
			sb.append(" where RIGHT(CONVERT(VARCHAR(10), sc_dd_month_mm01yyyy , 103), 7)='"
					+ date + "'");
			sb.append(" and ac_jd_bdm = " + bdmId);
		} else {
			sb.append(" where ac_jd_bdm = " + bdmId);
		}
		sb.append(" group by lo_ot_name,lx_pr_title,lx_pr_id ");
		sb.append("order by lo_ot_name,lx_pr_title,lx_pr_id");
		return sb.toString();
	}

	/**
	 * Gets the query for particular spm details.
	 * 
	 * @param spmId
	 *            the spm id
	 * @param reportName
	 *            the report name
	 * @param date
	 *            the date
	 * @return String
	 */
	public static String getQueryForParticularSPMDetails(String spmId,
			String reportName, String date) {
		String reportType1 = "SPMAccountSummary(TTM)";
		String reportType2 = "spmMonthPopupReport";
		StringBuilder sb = new StringBuilder("select lx_pr_id as accountID, ");
		sb.append("lo_ot_name+': '+lx_pr_title as accountName , ");
		sb.append("isnull(sum(ac_ru_pro_forma),'0') as proFormaExpense, ");
		sb.append("isnull(sum(ac_ru_expense),'0') as expense, ");
		sb.append("isnull(sum(ac_ru_revenue),'0') as revenue, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.0 end as proFormaVGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue)) else 0.0 end as VGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue))-");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))else 0.0 end as deltaVGPM, ");
		sb.append("isnull(count(*),'0') as occurrences ");
		sb.append("from dbo.ac_dimensions_project ");
		sb.append("join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append("join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append("join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append("join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		sb.append("join ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id ");
		sb.append(" join ilex.dbo.lm_appendix_poc on lm_ap_pr_id = ac_jd_pd_pr_id     ");
		sb.append("join ilex.dbo.ap_appendix_list_01 on ac_pd_pr_id=lx_pr_id ");
		if (reportType1.equals(reportName)) {
			sb.append(" where ac_jd_date_closed >='" + startingMonthDate()
					+ "'");
			sb.append(" and lm_appendix_poc.lm_ap_pc_id =  " + spmId);
		} else if (reportType2.equals(reportName)) {
			sb.append(" where RIGHT(CONVERT(VARCHAR(10), sc_dd_month_mm01yyyy , 103), 7)='"
					+ date + "'");
			sb.append(" and ac_jd_spm = " + spmId);
		} else {
			sb.append(" where ac_jd_spm = " + spmId);
		}
		sb.append(" group by lo_ot_name,lx_pr_title,lx_pr_id ");
		sb.append("order by lo_ot_name,lx_pr_title,lx_pr_id");
		return sb.toString();
	}

	/**
	 * Gets the query for bdm details.
	 * 
	 * @param param
	 *            the param
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param classification
	 *            the classification
	 * @return String
	 */
	public static String getQueryForBDMDetails_new(String param,
			String fromDate, String toDate, String classification[]) {

		StringBuilder sb = new StringBuilder(" select ac_jd_bdm as bdmId,");
		sb.append(" ac_du_name as bdmName , ");
		sb.append(" sum(ac_ru_pro_forma) as proFormaExpense,");
		sb.append(" sum(isnull(ac_fn_po_expense, 0.0)) as pOExpense,");
		sb.append(" sum(isnull(ac_fn_corrected_expense, 0.0)) as corExpense,");
		sb.append(" sum(ac_ru_expense) as totalExpense,");
		sb.append(" sum(ac_ru_revenue) as revenue,");
		sb.append(" case when sum(ac_ru_revenue) > 0.0 then");
		sb.append(" (1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.0 end as proFormaVGPM,");
		sb.append(" case when sum(ac_ru_revenue) > 0.0 then");
		sb.append(" (1-sum(ac_ru_expense)/sum(ac_ru_revenue)) else 0.0 end as VGPM,");
		sb.append(" case when sum(ac_ru_revenue) > 0.0 then");
		sb.append(" (1-sum(ac_ru_expense)/sum(ac_ru_revenue))-");
		sb.append(" (1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))else 0.0 end as deltaVGPM,");
		sb.append(" count(*) as occurrences");
		sb.append(" from dbo.ac_dimensions_project");
		sb.append(" join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id");
		sb.append(" join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id");
		sb.append(" join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy");
		sb.append(" join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id");
		sb.append(" join ac_dimension_user on ac_jd_bdm = ac_du_lo_pc_id");
		sb.append(" left join dbo.ac_fact_job_expense on ac_jd_js_id = ac_fn_js_id ");
		sb.append(" where ac_jd_date_closed>='" + fromDate
				+ "'and ac_jd_date_closed<='" + toDate + "'  ");
		if ("firstTime".equals(param)) {

		} else {
			if (null != classification) {
				sb.append(" and ");
				for (int i = 0; i < classification.length; i++) {
					if (i == 0) {
						if (classification[i].equals("MSP")) {
							sb.append(" ac_cl_major2='MSP'and ac_cl_minor2='Billable'  ");
							sb.append(" or  ac_cl_major2='MSP'and ac_cl_minor2='Non-Billable'  ");
						} else if (classification[i].equals("MSP~Billable"))
							sb.append(" ac_cl_major2='MSP'and ac_cl_minor2='Billable'  ");
						else if (classification[i].equals("MSP~Non-Billable"))
							sb.append(" ac_cl_major2='MSP'and ac_cl_minor2='Non-Billable'  ");
						else if (classification[i].equals("NetMedX~Billable"))
							sb.append(" ac_cl_major2='NetMedX' and ac_cl_minor2='Billable'   ");
						else if (classification[i]
								.equals("NetMedX~Non-Billable"))
							sb.append(" ac_cl_major2='NetMedX' and ac_cl_minor2='Non-Billable'   ");
						else if (classification[i].equals("Standards~Billable"))
							sb.append(" ac_cl_major2='Standard'and ac_cl_minor2='Billable'   ");
						else if (classification[i]
								.equals("Standards~Non-Billable"))
							sb.append(" ac_cl_major2='Standard'and ac_cl_minor2='Non-Billable'  ");
					} else {
						if (classification[i].equals("MSP~Billable"))
							sb.append(" or ac_cl_major2='MSP'and ac_cl_minor2='Billable'  ");
						else if (classification[i].equals("MSP~Non-Billable"))
							sb.append(" or ac_cl_major2='MSP'and ac_cl_minor2='Non-Billable'  ");
						else if (classification[i].equals("NetMedX~Billable"))
							sb.append(" or ac_cl_major2='NetMedX' and ac_cl_minor2='Billable'   ");
						else if (classification[i]
								.equals("NetMedX~Non-Billable"))
							sb.append(" or ac_cl_major2='NetMedX' and ac_cl_minor2='Non-Billable'   ");
						else if (classification[i].equals("Standards~Billable"))
							sb.append(" or ac_cl_major2='Standard'and ac_cl_minor2='Billable'   ");
						else if (classification[i]
								.equals("Standards~Non-Billable"))
							sb.append(" or ac_cl_major2='Standard'and ac_cl_minor2='Non-Billable'  ");

					}
				}

			}
		}
		sb.append(" group by ac_jd_bdm, ac_du_name");
		sb.append(" order by ac_du_name");

		return sb.toString();
	}

	public static String getQueryForBDMDetails(String param, String fromDate,
			String toDate, String[] classification) {
		String defaultValue = "firstTime";
		int length = 0;
		if (classification == null)
			length = 0;
		else
			length = classification.length;
		String empty = "";
		StringBuilder sb = new StringBuilder("select ac_jd_bdm as bdmId, ");
		sb.append("ac_du_name as bdmName , ");
		sb.append(getCommonPart());
		sb.append("join ac_dimension_user on ac_jd_bdm = ac_du_lo_pc_id ");
		if (defaultValue.equals(param)) {
			sb.append("where ac_cl_major_classification in ('Project','NetMedX') ");
			sb.append(" and ((ac_jd_date_closed>='" + fromDate + "')");
			sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
			sb.append(" and ac_du_bdm = 1 ");
		} else {
			sb.append("where ");

			if (classification != null) {
				for (int i = 0; i < classification.length; i++) {
					if (i == 0) {
						if (classification[i].equals("Provisioning/Deployment"))
							sb.append("( ac_cl_major_classification='MSP' and ac_cl_minor_classification!='Operations'");
						else
							sb.append("( ac_cl_major_classification ='"
									+ classification[i] + "'");
					} else if (classification[i]
							.equals("Provisioning/Deployment"))
						sb.append(" or (ac_cl_major_classification='MSP' and ac_cl_minor_classification!='Operations')");
					else
						sb.append(" or ac_cl_major_classification ='"
								+ classification[i] + "'");
				}
			} else {
				sb.append(" ");
			}

			if ((empty != fromDate) && (empty != toDate)) {
				if (length == 0) {
					sb.append(" ((ac_jd_date_closed>='" + fromDate + "')");
					sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
					sb.append(" and ac_du_bdm = 1 ");
				} else {
					sb.append(") and ((ac_jd_date_closed>='" + fromDate + "')");
					sb.append(" and (ac_jd_date_closed<='" + toDate + "'))");
					sb.append(" and ac_du_bdm = 1 ");
				}
			} else if ((empty == fromDate) && (empty == toDate)) {
				if (length == 0) {
					sb.append(" ac_du_bdm = 1 ");
				} else {
					sb.append(") ");
					sb.append("and ac_du_bdm = 1 ");
				}
			} else if ((empty == fromDate) && (empty != toDate)) {
				if (length == 0) {
					sb.append("(ac_jd_date_closed<='" + toDate + "')");
					sb.append("and ac_du_bdm = 1 ");
				} else {
					sb.append(" ) and (ac_jd_date_closed<='" + toDate + "')");
					sb.append("and ac_du_bdm = 1 ");
				}
			} else if (length == 0) {
				sb.append(" (ac_jd_date_closed>='" + fromDate + "')");
				sb.append("and ac_du_bdm = 1 ");
			} else {
				sb.append(" ) and (ac_jd_date_closed>='" + fromDate + "')");
				sb.append("and ac_du_bdm = 1 ");
			}
		}
		sb.append("group by ac_jd_bdm, ac_du_name ");
		sb.append("order by ac_du_name");
		return sb.toString();
	}

	/**
	 * Gets the query for revenue senior project manager details.
	 * 
	 * @return String
	 */
	public static String getQueryForRevenueSeniorProjectManagerDetails() {
		StringBuilder sb = new StringBuilder("select ac_jd_spm as spmId, ");
		sb.append("ac_du_name as spmName , ");
		sb.append("sum(ac_ru_pro_forma) as proFormaExpense, ");
		sb.append("sum(ac_ru_expense) as expence, ");
		sb.append("sum(ac_ru_revenue) as revenue, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) else 0.0 end as proFormaVGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue)) else 0.0 end as VGPM, ");
		sb.append("case when sum(ac_ru_revenue) > 0.0 then ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue))-");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue))else 0.0 end as deltaVGPM,");
		sb.append("count(*) as occurrences ");
		sb.append("from dbo.ac_dimensions_project ");
		sb.append("join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append("join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append("join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append("join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		sb.append("join ac_dimension_user on ac_jd_spm = ac_du_lo_pc_id ");
		sb.append("where ac_jd_date_closed > dateadd(mm, -13, getdate()) ");
		sb.append(" and(ac_cl_major_classification in ('Project','NetMedX') or (ac_cl_major_classification ='MSP' and ac_cl_minor_classification = 'EverWorX Operations')) ");
		sb.append("  and  ac_du_spm = 1  ");
		sb.append("group by ac_jd_spm, ac_du_name ");
		sb.append("order by ac_du_name");
		return sb.toString();
	}

	/**
	 * Gets the query for category all.
	 * 
	 * @param laborType
	 *            the labor type
	 * @return String
	 */
	public static String getQueryForCategoryAll(String laborType) {
		StringBuilder sb = new StringBuilder(
				"select 'All' as type,substring(convert(varchar(8), month, 3), 4,8) as display_month, ");
		sb.append(" convert(varchar(8), rate_average, 128) as avrg_rate , month,rate_average ");
		sb.append("from dbo.ac_fact2_labor_rate_by_criticality_by_month ");
		sb.append(getConditioinForAllCategory(laborType));

		sb.append(" and  month between ? and ?");
		return sb.toString();
	}

	/**
	 * Gets the query for category.
	 * 
	 * @param laborType
	 *            the labor type
	 * @return String
	 */
	public static String getQueryForCategory(String laborType) {
		StringBuilder sb = new StringBuilder(
				"select cast(Criticality as varchar(10)) as type, ");
		sb.append("substring(convert(varchar(8), month, 3), 4,8) as display_month, ");
		sb.append(" convert(varchar(8), rate_average, 128) as avrg_rate , month,rate_average ");
		sb.append("from dbo.ac_fact2_labor_rate_by_criticality_by_month ");
		sb.append(getConditioinForCategory(laborType));

		sb.append(" and  month between ? and ?");
		return sb.toString();
	}

	/**
	 * Gets the conditioin for all category.
	 * 
	 * @param laborType
	 *            the labor type
	 * @return String
	 */
	public static String getConditioinForAllCategory(String laborType) {
		StringBuilder sb = new StringBuilder("");
		if (laborType.equals(LaborType.LABOR_ENGINER.getType())) {
			sb.append("where category = 'Engineer-All'");
		} else if (laborType.equals(LaborType.LABOR_TECHNICIAN.getType())) {
			sb.append("where category = 'Technician-All'");
		}
		return sb.toString();
	}

	/**
	 * Gets the conditioin for category.
	 * 
	 * @param laborType
	 *            the labor type
	 * @return String
	 */
	public static String getConditioinForCategory(String laborType) {
		StringBuilder sb = new StringBuilder("");
		if (laborType.equals(LaborType.LABOR_ENGINER.getType())) {
			sb.append("where category = 'Engineer'");
		} else if (laborType.equals(LaborType.LABOR_TECHNICIAN.getType())) {
			sb.append("where category = 'Technician'");
		}
		return sb.toString();
	}

	/**
	 * Gets the query for category chart.
	 * 
	 * @param laborType
	 *            the labor type
	 * @return String
	 */
	public static String getQueryForCategoryChart(String laborType) {
		StringBuilder sb = new StringBuilder(getQueryForCategoryAll(laborType));
		sb.append(" union ");
		sb.append(getQueryForCategory(laborType));
		return sb.toString();
	}

	/**
	 * Gets the ordered query for category chart.
	 * 
	 * @param laborType
	 *            the labor type
	 * @return String
	 */
	public static String getOrderedQueryForCategoryChart(String laborType) {
		StringBuilder sb = new StringBuilder(
				getQueryForCategoryChart(laborType));
		sb.append(" order by type desc, month ");
		return sb.toString();
	}

	/**
	 * Gets the query for max y axis finance.
	 * 
	 * @return String
	 */
	public static String getQueryForMaxYAxisFinance() {
		StringBuilder sb = new StringBuilder("select max(revenue) from ( ");
		sb.append(" select revenue from ac_fact2_revenue_by_role ");
		sb.append(" where role_name = 'All' and month between ? and ? ");
		sb.append(" union ");

		sb.append(" select revenue from ac_fact2_revenue_by_role ");
		sb.append(" where role_name = 'SPM' and month between ? and ? and occurrences > 5  ");
		sb.append(" ) c ");
		return sb.toString();
	}

	/**
	 * Gets the query for max y axis revenue finance.
	 * 
	 * @param jobFilterType
	 *            the job filter type
	 * @param reportName
	 *            the report name
	 * @return String
	 */
	public static String getQueryForMaxYAxisRevenueFinance(
			String jobFilterType, String reportName) {
		StringBuilder sb = new StringBuilder(
				"select max(pf_exp),max([exp]),max(rev) from ");
		sb.append("(select sc_dd_month_mm01yyyy as mmyy, sum(ac_ru_pro_forma) as pf_exp, sum(ac_ru_expense) as exp, ");
		sb.append("sum(ac_ru_revenue) as rev, ");
		sb.append("(1-sum(ac_ru_pro_forma)/sum(ac_ru_revenue)) as pf_vgpm, ");
		sb.append("(1-sum(ac_ru_expense)/sum(ac_ru_revenue)) as vgpm, count(*) as occ ");
		sb.append(" from dbo.ac_dimensions_project ");
		sb.append(" join dbo.ac_dimensions_job on ac_pd_pr_id = ac_jd_pd_pr_id ");
		sb.append(" join dbo.ac_dimension_job_classification on ac_jd_js_id = ac_cl_jd_js_id ");
		sb.append(" join dbo.ac_dimension_date on ac_jd_date_closed = sc_dd_mmddyyyy ");
		sb.append(" join dbo.ac_fact_job_rollup on ac_jd_js_id = ac_ru_js_id ");
		if (reportName.equals("REVENUE_FINANCIAL_REPORT_ByJobOwner")) {
			sb.append(" join ac_dimension_user on ac_jd_job_owner = ac_du_lo_pc_id ");
			sb.append(" where ac_jd_date_closed > dateadd(mm, -11, ?) ");
			sb.append("and ac_cl_major_classification in ('Project','NetMedX') ");
			sb.append(" and ac_jd_job_owner = ? ");
			sb.append(" group by sc_dd_month_mm01yyyy) A");
		} else {
			sb.append(" where ac_jd_date_closed > dateadd(mm, -11, ?) ");

			if (jobFilterType.equals("AllJobs"))
				sb.append(" and ac_cl_major_classification like ? ");
			else
				sb.append(" and ac_cl_major_classification = ?");

			sb.append(" and ac_cl_minor_classification like ? ");
			sb.append("group by sc_dd_month_mm01yyyy) A ");

		}
		return sb.toString();
	}

	public static String startingMonthDate() {
		Calendar calendar = Calendar.getInstance();
		int currentMonth;
		String firstMonth;
		String firstDate = "01";
		String lastYear;
		String dateRange;
		currentMonth = calendar.get(Calendar.MONTH);
		// check whether current date is 1st of Current
		// Month
		if (currentMonth > 10) { // if last month
			firstMonth = "01";
			lastYear = calendar.get(Calendar.YEAR) + "";
		} else {
			firstMonth = ((currentMonth % 11) + 2) + "";
			lastYear = (calendar.get(Calendar.YEAR) - 1) + "";
		}
		dateRange = firstMonth + "/" + firstDate + "/" + lastYear;
		return dateRange;
	}

	/**
	 * Gets the query for proj cord list.
	 * 
	 * @return the query for proj cord list
	 */
	public static String getQueryForProjCordList() {
		StringBuilder sb = new StringBuilder(
				" SELECT Employeekey,EmployeeFirstName, EmployeeLastName ");
		sb.append(" FROM datawarehouse.dbo.DimEmployee ");
		sb.append(" WHERE EmployeeTransactionType IN ('Hire', 'Dept Change') ");
		sb.append(" AND EmployeeDepartment IN ('OPS') ");
		sb.append(" AND EffectiveStartDate <= getdate() ");
		sb.append(" AND EffectiveEndDate >= getdate()  ");
		sb.append(" ORDER BY EmployeeLastName ");
		return sb.toString();
	}

	public static String getQueryForTicketCount() {
		StringBuilder sb = new StringBuilder(" ");
		sb.append(" select replace(substring(convert(char(10), pr_ja_date_time_stamp, 103),4,10),'/','-') as Date, lo_pc_last_name as Name, sum(\"Installation Notes\") as   ");
		sb.append(" Installation_Notes, sum(\"Closed Ticket\") as Tickets_Closed ");
		sb.append(" from (select pr_ja_date_time_stamp, substring(lo_pc_first_name,1,1)+' '+ lo_pc_last_name lo_pc_last_name, case when pr_ja_action =2 then 'Installation Notes' else 'Closed Ticket'  ");
		sb.append(" end as pr_ja_action ");
		sb.append("from dbo.pr_job_actions_minute_rollup ");
		sb.append(" join dbo.lo_poc on pr_ja_actionee = lo_pc_id ");
		sb.append(" join lo_classification on lo_pc_role = lo_cl_id and lo_cl_role = 'Help Desk'  ");
		sb.append(" where pr_ja_action in (2,16)  ");
		sb.append(" and datepart(mm,pr_ja_date_time_stamp) in (datepart(mm,getdate()),datepart(mm,dateadd(mm,-1,getdate()))) ");
		sb.append(" and datepart(yy,pr_ja_date_time_stamp) in (datepart(yy,getdate()),datepart(yy,dateadd(mm,-1,getdate()))) ");
		sb.append(" and pr_ja_date_time_stamp between dateadd(mm, -12, getdate()) and getdate()) s ");
		sb.append(" PIVOT (COUNT(pr_ja_action) FOR pr_ja_action IN ([Installation Notes],[Closed Ticket])) p  ");
		sb.append(" group by replace(substring(convert(char(10), pr_ja_date_time_stamp, 103),4,10),'/','-'), lo_pc_last_name ");
		sb.append(" order by replace(substring(convert(char(10), pr_ja_date_time_stamp, 103),4,10),'/','-'), lo_pc_last_name ");
		return sb.toString();
	}
	
	
	/**
	 * Gets the get Base Query For Minuteman First Time Usage
	 * 
	 * @return String
	 */
	public static String getBaseQueryForMinutemanFirstTimeUsage(String filter,String lastwoYear)
	{
		Calendar calendar = Calendar.getInstance();
		//String lastTwo = null; 
		if(lastwoYear==null){
			String year=Integer.toString(calendar.get(Calendar.YEAR));
			
			String Value=year;
			
			if (Value != null && Value.length() >= 2) {  
				lastwoYear =  Value.substring(Math.max(Value.length() - 2, 0));
			}
		}		
		StringBuilder sb = new StringBuilder(
				"with names_cross_months(unique_name,mm_yyyy)");
		sb.append(" as(select distinct ac_mr_user,sc_dd_month_mm01yyyy from ac_fact2_mimuteman_usage_rollup cross join ac_dimension_date_months LEFT JOIN ilex.dbo.lo_poc AS iu ON iu.lo_pc_last_name+', '+iu.lo_pc_first_name = ac_mr_user where sc_dd_month_mm01yyyy in ('1/1/"+lastwoYear+"','2/1/"+lastwoYear+"','3/1/"+lastwoYear+"','4/1/"+lastwoYear+"','5/1/"+lastwoYear+"','6/1/"+lastwoYear+"','7/1/"+lastwoYear+"','8/1/"+lastwoYear+"','9/1/"+lastwoYear+"','10/1/"+lastwoYear+"','11/1/"+lastwoYear+"','12/1/"+lastwoYear+"') AND iu.lo_pc_active_user <> 0 ) ");
		sb.append(" select mm_yyyy,unique_name,isnull(ac_mr_count,0)as occurance ");
		sb.append(" from names_cross_months ");
		sb.append(" left join ac_fact2_mimuteman_usage_rollup ");
		sb.append(" on mm_yyyy=ac_mr_dt_stamp ");
		sb.append(" and unique_name =ac_mr_user ");
		sb.append(" and ac_mr_role='" + filter + "' ");
		sb.append(" order by unique_name,mm_yyyy ");
		return sb.toString();
	}
	
}
