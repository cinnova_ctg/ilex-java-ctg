package com.ilex.reports.common.multiAxis.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "chart")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChartXmlHeaderTicketCount {

	@XmlElementWrapper(name = "categories")
	private List<ChartXmlCategory> category = null;
	@XmlElement
	private List<ChartXmlDataSet> dataSet = null;
	@XmlAttribute
	private String caption = null;

	@XmlAttribute
	private String xAxisName = null;
	@XmlAttribute
	private String yAxisName = null;
	@XmlAttribute
	private String showLabels = null;
	@XmlAttribute
	private String showvalues = null;
	@XmlAttribute
	private String decimals = null;
	@XmlAttribute
	private String placeValuesInside = null;
	@XmlAttribute
	private String rotateValues = null;
	@XmlAttribute
	private String useRoundEdges = null;
	

	public List<ChartXmlDataSet> getDataSet() {
		return dataSet;
	}

	public void setDataSet(List<ChartXmlDataSet> dataSet) {
		this.dataSet = dataSet;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getxAxisName() {
		return xAxisName;
	}

	public void setxAxisName(String xAxisName) {
		this.xAxisName = xAxisName;
	}

	public String getyAxisName() {
		return yAxisName;
	}

	public void setyAxisName(String yAxisName) {
		this.yAxisName = yAxisName;
	}

	public String getShowLabels() {
		return showLabels;
	}

	public void setShowLabels(String showLabels) {
		this.showLabels = showLabels;
	}

	public String getShowvalues() {
		return showvalues;
	}

	public void setShowvalues(String showvalues) {
		this.showvalues = showvalues;
	}

	public String getDecimals() {
		return decimals;
	}

	public void setDecimals(String decimals) {
		this.decimals = decimals;
	}

	public String getPlaceValuesInside() {
		return placeValuesInside;
	}

	public void setPlaceValuesInside(String placeValuesInside) {
		this.placeValuesInside = placeValuesInside;
	}

	public String getRotateValues() {
		return rotateValues;
	}

	public void setRotateValues(String rotateValues) {
		this.rotateValues = rotateValues;
	}

	public String getUseRoundEdges() {
		return useRoundEdges;
	}

	public void setUseRoundEdges(String useRoundEdges) {
		this.useRoundEdges = useRoundEdges;
	}

	public List<ChartXmlCategory> getCategory() {
		return category;
	}

	public void setCategory(List<ChartXmlCategory> category) {
		this.category = category;
	}

}
