package com.ilex.reports.common.multiAxis.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ChartXmlAxisPcComp {

	@XmlAttribute
	private String title = null;
	@XmlAttribute
	private String titlePos = null;
	@XmlAttribute
	private String tickWidth = null;
	@XmlAttribute
	private String divlineisdashed = null;
	@XmlAttribute
	private String numberPrefix = null;
	@XmlAttribute
	private String minvalue = null;
	@XmlAttribute
	private String maxValue = null;
	@XmlAttribute
	private String axisOnLeft = null;
	@XmlAttribute
	private String formatNumberScale = null;
	
	@XmlElement
	private List<ChartXmlDataSet> dataSet = null;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTickWidth() {
		return tickWidth;
	}

	public void setTickWidth(String tickWidth) {
		this.tickWidth = tickWidth;
	}

	public String getDivlineisdashed() {
		return divlineisdashed;
	}

	public void setDivlineisdashed(String divlineisdashed) {
		this.divlineisdashed = divlineisdashed;
	}

	public String getNumberPrefix() {
		return numberPrefix;
	}

	public void setNumberPrefix(String numberPrefix) {
		this.numberPrefix = numberPrefix;
	}

	public String getMinvalue() {
		return minvalue;
	}

	public void setMinvalue(String minvalue) {
		this.minvalue = minvalue;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public String getAxisOnLeft() {
		return axisOnLeft;
	}

	public void setAxisOnLeft(String axisOnLeft) {
		this.axisOnLeft = axisOnLeft;
	}

	public ChartXmlAxisPcComp() {
		super();
	}

	public ChartXmlAxisPcComp(String title,String titlePos, String tickWidth,
			String divlineisdashed, String minvalue, String maxValue,
			String axisOnLeft,String formatNumberScale) {
		String position = "right";

		if (position.equals(titlePos)) {
			this.title = " " + title;
		} else {
			this.title = title;
		}
		this.titlePos = titlePos;
		this.title = title;
		this.tickWidth = tickWidth;
		this.divlineisdashed = divlineisdashed;
		this.minvalue = minvalue;
		this.maxValue = maxValue;
		this.axisOnLeft = axisOnLeft;
		this.formatNumberScale=formatNumberScale;
	}

	public List<ChartXmlDataSet> getDataSet() {
		return dataSet;
	}

	public void setDataSet(List<ChartXmlDataSet> dataSet) {
		this.dataSet = dataSet;
	}

	public String getTitlePos() {
		return titlePos;
	}

	public void setTitlePos(String titlePos) {
		this.titlePos = titlePos;
	}

	public String getFormatNumberScale() {
		return formatNumberScale;
	}

	public void setFormatNumberScale(String formatNumberScale) {
		this.formatNumberScale = formatNumberScale;
	}
}
