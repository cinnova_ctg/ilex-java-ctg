package com.ilex.reports.common.multiAxis.bean;


import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GraphXmlCatageries {

	@XmlAttribute
	private String font="Arial";
	@XmlAttribute
	private String fontSize ="10";
	@XmlAttribute
	private String fontColor="000000";
	@XmlElement
	private List<ChartXmlCategory> category = null;
	public String getFont() {
		return font;
	}
	public void setFont(String font) {
		this.font = font;
	}
	public String getFontSize() {
		return fontSize;
	}
	public void setFontSize(String fontSize) {
		this.fontSize = fontSize;
	}
	public String getFontColor() {
		return fontColor;
	}
	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}
	public List<ChartXmlCategory> getCategory() {
		return category;
	}
	public void setCategory(List<ChartXmlCategory> category) {
		this.category = category;
	}
	
}
