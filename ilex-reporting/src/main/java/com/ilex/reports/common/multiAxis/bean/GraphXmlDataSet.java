package com.ilex.reports.common.multiAxis.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GraphXmlDataSet {

	@XmlAttribute
	private String seriesName = null;
	@XmlAttribute
	private String color = null;
	@XmlAttribute
	private String showValues = null;
	public String getShowValues() {
		return showValues;
	}

	public void setShowValues(String showValues) {
		this.showValues = showValues;
	}

	@XmlAttribute
	private String alpha = null;
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getAlpha() {
		return alpha;
	}

	public void setAlpha(String alpha) {
		this.alpha = alpha;
	}

	@XmlElement
	private List<ChartXmlSetValue> set = null;

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public List<ChartXmlSetValue> getSet() {
		return set;
	}

	public void setSet(List<ChartXmlSetValue> set) {
		this.set = set;
	}

	public GraphXmlDataSet() {
		super();
	}

	public GraphXmlDataSet(String seriesName) {
		this.seriesName = seriesName;
	}
}
