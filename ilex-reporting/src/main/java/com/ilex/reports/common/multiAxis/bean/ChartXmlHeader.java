package com.ilex.reports.common.multiAxis.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "chart")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChartXmlHeader {

	@XmlElementWrapper(name = "categories")
	private List<ChartXmlCategory> category = null;

	@XmlElement
	private List<ChartXmlAxis> axis = null;

	@XmlAttribute
	private String caption = null;

	@XmlAttribute
	private String xAxisName = null;

	@XmlAttribute
	private String showValues = null;

	@XmlAttribute
	private String divLineAlpha = null;

	@XmlAttribute
	private String yaxisminvalue = null;

	@XmlAttribute
	private String numVDivLines = null;

	@XmlAttribute
	private String vDivLineAlpha = null;

	@XmlAttribute
	private String showAlternateVGridColor = null;

	@XmlAttribute
	private String alternateVGridAlpha = null;

	@XmlAttribute
	private String labelDisplay = null;

	@XmlAttribute
	private String slantLabels = null;

	@XmlAttribute
	private String canvasPadding = null;

	public List<ChartXmlCategory> getCategories() {
		return category;
	}

	public void setCategories(List<ChartXmlCategory> category) {
		this.category = category;
	}

	public List<ChartXmlCategory> getCategory() {
		return category;
	}

	public void setCategory(List<ChartXmlCategory> category) {
		this.category = category;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getxAxisName() {
		return xAxisName;
	}

	public void setxAxisName(String xAxisName) {
		this.xAxisName = xAxisName;
	}

	public String getShowValues() {
		return showValues;
	}

	public void setShowValues(String showValues) {
		this.showValues = showValues;
	}

	public String getDivLineAlpha() {
		return divLineAlpha;
	}

	public void setDivLineAlpha(String divLineAlpha) {
		this.divLineAlpha = divLineAlpha;
	}

	public String getYaxisminvalue() {
		return yaxisminvalue;
	}

	public void setYaxisminvalue(String yaxisminvalue) {
		this.yaxisminvalue = yaxisminvalue;
	}

	public String getNumVDivLines() {
		return numVDivLines;
	}

	public void setNumVDivLines(String numVDivLines) {
		this.numVDivLines = numVDivLines;
	}

	public String getvDivLineAlpha() {
		return vDivLineAlpha;
	}

	public void setvDivLineAlpha(String vDivLineAlpha) {
		this.vDivLineAlpha = vDivLineAlpha;
	}

	public String getShowAlternateVGridColor() {
		return showAlternateVGridColor;
	}

	public void setShowAlternateVGridColor(String showAlternateVGridColor) {
		this.showAlternateVGridColor = showAlternateVGridColor;
	}

	public String getAlternateVGridAlpha() {
		return alternateVGridAlpha;
	}

	public void setAlternateVGridAlpha(String alternateVGridAlpha) {
		this.alternateVGridAlpha = alternateVGridAlpha;
	}

	public String getLabelDisplay() {
		return labelDisplay;
	}

	public void setLabelDisplay(String labelDisplay) {
		this.labelDisplay = labelDisplay;
	}

	public String getSlantLabels() {
		return slantLabels;
	}

	public void setSlantLabels(String slantLabels) {
		this.slantLabels = slantLabels;
	}

	public String getCanvasPadding() {
		return canvasPadding;
	}

	public void setCanvasPadding(String canvasPadding) {
		this.canvasPadding = canvasPadding;
	}

	public List<ChartXmlAxis> getAxis() {
		return axis;
	}

	public void setAxis(List<ChartXmlAxis> axis) {
		this.axis = axis;
	}
}
