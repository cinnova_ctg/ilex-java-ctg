package com.ilex.reports.common.multiAxis.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "dataSet")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChartXmlDataSet {

	@XmlAttribute
	private String seriesName = null;
	@XmlAttribute
	private String lineThickness = null;
	@XmlAttribute
	private String color = null;
	@XmlAttribute
	private String includeInLegend = null;
	public String getLineThickness() {
		return lineThickness;
	}

	public void setLineThickness(String lineThickness) {
		this.lineThickness = lineThickness;
	}

	@XmlElement
	private List<ChartXmlSetValue> set = null;

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public List<ChartXmlSetValue> getSet() {
		return set;
	}

	public void setSet(List<ChartXmlSetValue> set) {
		this.set = set;
	}

	public ChartXmlDataSet() {
		super();
	}

	public ChartXmlDataSet(String seriesName) {
		this.seriesName = seriesName;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIncludeInLegend() {
		return includeInLegend;
	}

	public void setIncludeInLegend(String includeInLegend) {
		this.includeInLegend = includeInLegend;
	}
}
