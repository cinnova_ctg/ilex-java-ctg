package com.ilex.reports.common.multiAxis.bean;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class MultiAxisChartBean {

	private ArrayList<ChartXmlAxis> axisList = null;
	private String graphXML = null;
	private ArrayList<String> xCordinateDates = null;
	private String startDate = null;
	private String endDate = null;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public ArrayList<ChartXmlAxis> getAxisList() {
		return axisList;
	}

	public void setAxisList(ArrayList<ChartXmlAxis> axisList) {
		this.axisList = axisList;
	}

	public String getGraphXML() {
		return graphXML;
	}

	public void setGraphXML(String graphXML) {
		this.graphXML = graphXML;
	}

	public ArrayList<String> getxCordinateDates() {
		return xCordinateDates;
	}

	public void setxCordinateDates(ArrayList<String> xCordinateDates) {
		this.xCordinateDates = xCordinateDates;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
