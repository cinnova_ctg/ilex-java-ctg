package com.ilex.reports.common.multiAxis.bean;

import java.io.StringWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;

import com.ilex.reports.logic.financialReport.RevenueExpenceTableResultSetBean;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;
import com.mind.fw.util.PropertyFileUtil;

public class MultiAxisChartOperation {

	public static final Logger LOGGER = Logger
			.getLogger(MultiAxisChartOperation.class);

	/**
	 * Gets the axis list for multi axis chart.
	 * 
	 * @param rsBeanList
	 *            the rs bean list
	 * @param datesQueue
	 *            the dates queue
	 * @param noOfRecords
	 *            the no of records
	 * @param defaultCount
	 *            the default count
	 * @param reportPropertyKey
	 *            the report property key
	 * @return the axis list for multi axis chart
	 * @throws AppException
	 *             the app exception
	 */
	@SuppressWarnings("null")
	public static ArrayList<ChartXmlAxis> getAxisListForMultiAxisChart(
			List<? extends MultiAxisChartResultSetBean> rsBeanList,
			ArrayList<String> datesQueue, int noOfRecords, int defaultCount,
			String reportPropertyKey) throws AppException {

		ArrayList<ChartXmlAxis> chartXmlAxisList = new ArrayList<ChartXmlAxis>();
		ArrayList<String> tempXmlAxisList = new ArrayList<String>();
		try {
			ChartXmlAxis chartXmlAxis = null;
			ChartXmlDataSet chartXmlDataSet = null;
			List<ChartXmlSetValue> dataSetList = null;
			ChartXmlSetValue chartXmlSetValue = null;

			String type = "";
			int minimumCount = 0;
			int recordCount = 1;
			int xmlAxisLength = 0;
			String reportPrefix = PropertyFileUtil
					.getAppProperty(reportPropertyKey + ".prefix");
			for (MultiAxisChartResultSetBean multiAxisChartResultSetBean : rsBeanList) {

				/*
				 * Check the legend value of chart should be equal to previous
				 * legend value and records should not be last.
				 */
				if (type.equals(multiAxisChartResultSetBean.getType())
						&& recordCount != noOfRecords) {

				} else {
					/*
					 * Check is the record is last record?
					 */
					if (recordCount == noOfRecords) {
						xmlAxisLength = tempXmlAxisList.size();
						if (xmlAxisLength > 0) {
							for (int i = 0; i <= xmlAxisLength; i++) {
								minimumCount++;
								chartXmlSetValue = new ChartXmlSetValue();
								if (multiAxisChartResultSetBean
										.getDisplay_month().equals(
												tempXmlAxisList.get(0))) {
									chartXmlSetValue
											.setValue(multiAxisChartResultSetBean
													.getAvrg_rate());
									dataSetList.add(chartXmlSetValue);
									tempXmlAxisList.remove(0);
									break;
								} else {
									chartXmlSetValue.setValue("0");
									dataSetList.add(chartXmlSetValue);
									tempXmlAxisList.remove(0);
									xmlAxisLength = tempXmlAxisList.size();
									i = 0;
								}
							}
						}

					}
					tempXmlAxisList.addAll(datesQueue);
					if (chartXmlAxis != null && chartXmlDataSet != null) {
						if (minimumCount < defaultCount) {
							for (int i = minimumCount; i <= defaultCount; i++) {
								chartXmlSetValue = new ChartXmlSetValue();
								chartXmlSetValue.setValue("0");
								dataSetList.add(chartXmlSetValue);
							}
						}
						chartXmlDataSet.setSet(dataSetList);
						chartXmlAxis.setDataSet(chartXmlDataSet);
						chartXmlAxisList.add(chartXmlAxis);
					}

					chartXmlAxis = new ChartXmlAxis();
					chartXmlDataSet = new ChartXmlDataSet();
					dataSetList = new ArrayList<ChartXmlSetValue>();

					minimumCount = 0;

				}
				/*
				 * Following lines are default operation which would be
				 * performed in every case.
				 */
				xmlAxisLength = tempXmlAxisList.size();
				if (xmlAxisLength > 0) {
					for (int i = 0; i <= xmlAxisLength; i++) {
						minimumCount++;
						chartXmlSetValue = new ChartXmlSetValue();
						if (multiAxisChartResultSetBean.getDisplay_month()
								.equals(tempXmlAxisList.get(0))) {
							chartXmlSetValue
									.setValue(multiAxisChartResultSetBean
											.getAvrg_rate());
							dataSetList.add(chartXmlSetValue);
							tempXmlAxisList.remove(0);
							break;
						} else {
							chartXmlSetValue.setValue("0");
							dataSetList.add(chartXmlSetValue);
							tempXmlAxisList.remove(0);
							xmlAxisLength = tempXmlAxisList.size();
							i = 0;
						}
					}
				}
				chartXmlDataSet.setSeriesName(PropertyFileUtil.getAppProperty(
						reportPrefix + multiAxisChartResultSetBean.getType()
								+ ".displayName",
						multiAxisChartResultSetBean.getType()));

				type = multiAxisChartResultSetBean.getType();
				recordCount++;
			}
		} catch (Exception e) {
			throw new AppException(e.getMessage());
		} finally {
		}

		return chartXmlAxisList;
	}

	/**
	 * Gets the revenue expence axis list for multi axis chart.
	 * 
	 * @param rsBeanList
	 *            the rs bean list
	 * @param datesQueue
	 *            the dates queue
	 * @param noOfRecords
	 *            the no of records
	 * @param defaultCount
	 *            the default count
	 * @param reportPropertyKey
	 *            the report property key
	 * @param endDate
	 *            the end date
	 * @param majorClassification
	 *            the major classification
	 * @param minorClassification
	 *            the minor classification
	 * @param reTableResultbean
	 *            the re table resultbean
	 * @return the revenue expence axis list for multi axis chart
	 * @throws AppException
	 *             the app exception
	 */
	public static ArrayList<ChartXmlAxis> getRevenueExpenceAxisListForMultiAxisChart(
			List<? extends MultiAxisChartResultSetBean> rsBeanList,
			ArrayList<String> datesQueue, int noOfRecords, int defaultCount,
			String reportPropertyKey, String endDate,
			String majorClassification, String minorClassification,
			List<RevenueExpenceTableResultSetBean> reTableResultbean)
			throws AppException {

		ArrayList<ChartXmlAxis> chartXmlAxisList = new ArrayList<ChartXmlAxis>();
		ArrayList<String> tempXmlAxisList = new ArrayList<String>();
		try {
			ChartXmlAxis chartXmlAxis = null;
			ChartXmlDataSet chartXmlDataSet = null;
			List<ChartXmlSetValue> dataSetList = null;
			ChartXmlSetValue chartXmlSetValue = null;
			String pFExp = null;
			String delta = null;
			String expense = null;
			String exp = null;
			String occ = null;
			String revenue = null;
			Double amount;
			int trancate = 0;
			int timeframe = 12;
			int counter = 0;
			Double currency;
			String vgpm = null;
			NumberFormat Formatter = NumberFormat
					.getCurrencyInstance(new Locale("en_US"));
			String type = "";
			int minimumCount = 0;
			int recordCount = 1;
			int xmlAxisLength = 0;
			String reportPrefix = PropertyFileUtil
					.getAppProperty(reportPropertyKey + ".prefix");
			for (MultiAxisChartResultSetBean multiAxisChartResultSetBean : rsBeanList) {

				/*
				 * Check the legend value of chart should be equal to previous
				 * legend value and records should not be last.
				 */
				trancate = (counter % 12);
				if (type.equals(multiAxisChartResultSetBean.getType())
						&& recordCount != noOfRecords) {

				} else {
					/*
					 * Check is the record is last record?
					 */
					if (recordCount == noOfRecords) {
						xmlAxisLength = tempXmlAxisList.size();
						if (xmlAxisLength > 0) {
							for (int i = 0; i <= xmlAxisLength; i++) {
								minimumCount++;
								chartXmlSetValue = new ChartXmlSetValue();
								if (multiAxisChartResultSetBean
										.getDisplay_month().equals(
												tempXmlAxisList.get(0))) {
									chartXmlSetValue
											.setValue(multiAxisChartResultSetBean
													.getAvrg_rate());
									occ = multiAxisChartResultSetBean
											.getAvrg_rate();
									amount = Double.parseDouble(occ);
									occ = Formatter.format(amount).substring(
											1,
											Formatter.format(amount).indexOf(
													"."));
									chartXmlSetValue.setToolText(occ);
									dataSetList.add(chartXmlSetValue);
									tempXmlAxisList.remove(0);
									break;
								} else {
									chartXmlSetValue.setValue("0");
									chartXmlSetValue.setToolText("0");
									dataSetList.add(chartXmlSetValue);
									tempXmlAxisList.remove(0);
									xmlAxisLength = tempXmlAxisList.size();
									i = 0;
								}
							}
						}

					}
					tempXmlAxisList.addAll(datesQueue);
					if (chartXmlAxis != null && chartXmlDataSet != null) {
						if (minimumCount < defaultCount) {
							for (int i = minimumCount; i < defaultCount; i++) {
								chartXmlSetValue = new ChartXmlSetValue();
								chartXmlSetValue.setValue("0");
								if (counter <= timeframe * 1) {
									chartXmlSetValue.setToolText("$0");
								} else if (counter > timeframe * 1
										&& counter <= timeframe * 2) {
									chartXmlSetValue.setToolText("$0");
								} else if (counter > timeframe * 2
										&& counter <= timeframe * 3) {
									chartXmlSetValue.setToolText("Revenue: "
											+ "$0" + "{br}" + " Expense: $0"
											+ "{br}" + " VGPM: 0.00");
								} else if (counter > timeframe * 3
										&& counter <= timeframe * 4) {
									chartXmlSetValue.setToolText("0.00");
								} else if (counter > timeframe * 4
										&& counter <= timeframe * 5) {
									chartXmlSetValue.setToolText("VGPM: "
											+ "{br}" + " Actual: 0.00" + "{br}"
											+ " Delta:+" + "0.00");
								} else {
									chartXmlSetValue.setToolText("0");
								}
								dataSetList.add(chartXmlSetValue);
								tempXmlAxisList.remove(0);
								counter++;
							}
						}
						chartXmlDataSet.setSet(dataSetList);
						chartXmlAxis.setDataSet(chartXmlDataSet);
						chartXmlAxisList.add(chartXmlAxis);
					}

					chartXmlAxis = new ChartXmlAxis();
					chartXmlDataSet = new ChartXmlDataSet();
					dataSetList = new ArrayList<ChartXmlSetValue>();

					minimumCount = 0;

				}
				/*
				 * Following lines are default operation which would be
				 * performed in every case.
				 */
				xmlAxisLength = tempXmlAxisList.size();
				if (xmlAxisLength > 0) {
					for (int i = 0; i <= xmlAxisLength; i++) {
						minimumCount++;
						chartXmlSetValue = new ChartXmlSetValue();
						if (multiAxisChartResultSetBean.getDisplay_month()
								.equals(tempXmlAxisList.get(0))) {
							chartXmlSetValue
									.setValue(multiAxisChartResultSetBean
											.getAvrg_rate());
							if (multiAxisChartResultSetBean.getType().equals(
									"Pro Forma Expense")) {
								pFExp = multiAxisChartResultSetBean
										.getAvrg_rate();
								currency = Double.parseDouble(pFExp);
								pFExp = Formatter.format(currency)
										.substring(
												1,
												Formatter.format(currency)
														.indexOf("."));
								chartXmlSetValue.setToolText("$" + pFExp);
							} else if (multiAxisChartResultSetBean.getType()
									.equals("Expense")) {
								exp = multiAxisChartResultSetBean
										.getAvrg_rate();
								currency = Double.parseDouble(exp);
								exp = Formatter.format(currency)
										.substring(
												1,
												Formatter.format(currency)
														.indexOf("."));
								chartXmlSetValue.setToolText("$" + exp);
							} else if (multiAxisChartResultSetBean.getType()
									.equals("Pro Forma VGPM")) {
								chartXmlSetValue
										.setToolText(multiAxisChartResultSetBean
												.getAvrg_rate().substring(
														0,
														multiAxisChartResultSetBean
																.getAvrg_rate()
																.length() - 2));
							} else if (multiAxisChartResultSetBean.getType()
									.equals("Occurrences")) {
								occ = multiAxisChartResultSetBean
										.getAvrg_rate();
								amount = Double.parseDouble(occ);
								occ = Formatter.format(amount).substring(1,
										Formatter.format(amount).indexOf("."));
								chartXmlSetValue.setToolText(occ);
							} else if (multiAxisChartResultSetBean.getType()
									.equals("Revenue")) {
								trancate = (counter % 12);
								RevenueExpenceTableResultSetBean rt1 = reTableResultbean
										.get(trancate);
								// vgpm=FinancialReportDAO.getToolTextValue(ReportStaticData.getRevenueExpenseToolTextValue("vgpm",multiAxisChartResultSetBean.getDisplay_month(),majorClassification,minorClassification,endDate),endDate);
								vgpm = rt1.getVGPM();
								// expense=FinancialReportDAO.getToolTextValue(ReportStaticData.getRevenueExpenseToolTextValue("expense",multiAxisChartResultSetBean.getDisplay_month(),majorClassification,minorClassification,endDate),endDate);
								expense = rt1.getExpense();
								currency = Double.parseDouble(expense);
								expense = Formatter.format(currency)
										.substring(
												1,
												Formatter.format(currency)
														.indexOf("."));
								revenue = multiAxisChartResultSetBean
										.getAvrg_rate();
								currency = Double.parseDouble(revenue);
								revenue = Formatter.format(currency)
										.substring(
												1,
												Formatter.format(currency)
														.indexOf("."));
								chartXmlSetValue
										.setToolText(multiAxisChartResultSetBean
												.getType()
												+ ": "
												+ "$"
												+ revenue
												+ "{br}"
												+ " Expense: $"
												+ expense
												+ "{br}"
												+ " VGPM: "
												+ vgpm.substring(0,
														vgpm.length() - 2)

										);
							} else if (multiAxisChartResultSetBean.getType()
									.equals("VGPM")) {
								trancate = (counter % 12);
								// String
								// delta=FinancialReportDAO.getToolTextValue(ReportStaticData.getRevenueExpenseToolTextValue("delta",multiAxisChartResultSetBean.getDisplay_month(),majorClassification,minorClassification,endDate),endDate);
								RevenueExpenceTableResultSetBean rt1 = reTableResultbean
										.get(trancate);
								delta = rt1.getDeltaVGPM();
								if (Double.parseDouble(delta) >= 0.0000) {
									delta = "+" + delta;
									chartXmlSetValue
											.setToolText(multiAxisChartResultSetBean
													.getType()
													+ ": "
													+ "{br}"
													+ " Actual:"
													+ multiAxisChartResultSetBean
															.getAvrg_rate()
															.substring(
																	0,
																	multiAxisChartResultSetBean
																			.getAvrg_rate()
																			.length() - 2)
													+ "{br}"
													+ " Delta:"
													+ delta.substring(0,
															delta.length() - 2));
								} else {
									trancate = (counter % 12);
									chartXmlSetValue
											.setToolText(multiAxisChartResultSetBean
													.getType()
													+ ": "
													+ "{br}"
													+ " Actual:"
													+ multiAxisChartResultSetBean
															.getAvrg_rate()
															.substring(
																	0,
																	multiAxisChartResultSetBean
																			.getAvrg_rate()
																			.length() - 2)
													+ "{br}"
													+ " Delta:"
													+ delta.substring(0,
															delta.length() - 2));
								}
							}
							counter++;
							dataSetList.add(chartXmlSetValue);
							tempXmlAxisList.remove(0);
							break;
						} else {
							chartXmlSetValue.setValue("0");
							if (multiAxisChartResultSetBean.getType().equals(
									"Pro Forma Expense")) {
								chartXmlSetValue.setToolText("$0");
							} else if (multiAxisChartResultSetBean.getType()
									.equals("Expense")) {
								chartXmlSetValue.setToolText("$0");
							} else if (multiAxisChartResultSetBean.getType()
									.equals("Pro Forma VGPM")) {
								chartXmlSetValue.setToolText("0.00");
							} else if (multiAxisChartResultSetBean.getType()
									.equals("Occurrences")) {
								chartXmlSetValue.setToolText("0");
							}

							else if (multiAxisChartResultSetBean.getType()
									.equals("Revenue")) {
								chartXmlSetValue
										.setToolText(multiAxisChartResultSetBean
												.getType()
												+ ": "
												+ "$0"
												+ "{br}"
												+ " Expense: $0"
												+ "{br}" + " VGPM: 0.00");

							} else if (multiAxisChartResultSetBean.getType()
									.equals("VGPM")) {
								chartXmlSetValue
										.setToolText(multiAxisChartResultSetBean
												.getType()
												+ ": "
												+ "{br}"
												+ " Actual: 0.00"
												+ "{br}"
												+ " Delta:+" + "0.00");
							}
							counter++;
							dataSetList.add(chartXmlSetValue);
							tempXmlAxisList.remove(0);
							xmlAxisLength = tempXmlAxisList.size();
							i = 0;
						}
					}
				}
				chartXmlDataSet.setSeriesName(PropertyFileUtil.getAppProperty(
						reportPrefix + multiAxisChartResultSetBean.getType()
								+ ".displayName",
						multiAxisChartResultSetBean.getType()));

				type = multiAxisChartResultSetBean.getType();
				recordCount++;
			}
		} catch (Exception e) {
			throw new AppException(e.getMessage());
		} finally {
		}

		return chartXmlAxisList;
	}

	/**
	 * Creates the xml chart.
	 * 
	 * @param chartXmlAxisList
	 *            the chart xml axis list
	 * @param xCoordinates
	 *            the x coordinates
	 * @param laborCostType
	 *            the labor cost type
	 * @param maxYAxisValue
	 *            the max y axis value
	 * @param laborCategory
	 *            the labor category
	 * @return the string
	 * @throws Exception
	 */
	public static String createXmlChart(List<ChartXmlAxis> chartXmlAxisList,
			List<String> xCoordinates, String reportPropertyKey,
			Long maxYAxisValue, String caption) throws Exception {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method createXmlChart for generating xml for MultiAxis Chart starts.");
		}

		String returnGraphXml = "";
		String captions = caption;
		int monthCount = 0;
		String reportPrefix = PropertyFileUtil.getAppProperty(reportPropertyKey
				+ ".prefix");
		if (captions == null || captions.equals("")) {
			captions = PropertyFileUtil.getAppProperty(reportPropertyKey
					+ ".caption");
		}

		/* Create Chart (chart header) node and set its attributes */
		ChartXmlHeader chartHearder = new ChartXmlHeader();

		chartHearder.setCaption(captions);

		chartHearder.setxAxisName(PropertyFileUtil.getAppProperty(reportPrefix
				+ "axisName", "Twelve Trailling Months"));
		chartHearder.setShowValues("0");
		chartHearder.setDivLineAlpha("100");
		chartHearder.setYaxisminvalue("0");
		chartHearder.setNumVDivLines("5");
		chartHearder.setvDivLineAlpha("0");
		chartHearder.setShowAlternateVGridColor("1");
		chartHearder.setAlternateVGridAlpha("7");
		chartHearder.setLabelDisplay("Rotate");
		chartHearder.setSlantLabels("1");
		chartHearder.setCanvasPadding("0");

		/*
		 * Create Categories node and Axis nodes and set its corresponding
		 * attributes and elements
		 */

		List<ChartXmlCategory> categories = new ArrayList<ChartXmlCategory>();
		ChartXmlCategory category = null;

		/* Y Coordinate 1. */
		ChartXmlAxis yAxis = null;
		List<ChartXmlAxis> axisList = new ArrayList<ChartXmlAxis>();
		String seriesName = "";
		for (ChartXmlAxis chartXmlAxis : chartXmlAxisList) {
			seriesName = chartXmlAxis.getDataSet().getSeriesName()
					.replaceAll(" ", "_");
			seriesName = seriesName.replaceAll(":", "");

			try {

				yAxis = new ChartXmlAxis(PropertyFileUtil.getAppProperty(
						reportPrefix + seriesName + ".titleName",
						PropertyFileUtil.getAppProperty(reportPrefix
								+ seriesName + ".displayName", seriesName)),
						PropertyFileUtil.getAppProperty(reportPrefix
								+ seriesName + ".titlePos", "right"),
						PropertyFileUtil.getAppProperty(reportPrefix
								+ seriesName + ".tickWidth", "10"),
						PropertyFileUtil.getAppProperty(reportPrefix
								+ seriesName + ".divlineisdashed", "1"),
						PropertyFileUtil.getAppProperty(reportPrefix
								+ seriesName + ".numberPrefix", "$"),
						PropertyFileUtil.getAppProperty(reportPrefix
								+ seriesName + ".yaxisMinValue", "0.0"),
						PropertyFileUtil.getAppProperty(reportPrefix
								+ seriesName + ".yaxisMaxValue", maxYAxisValue
								+ ""), PropertyFileUtil.getAppProperty(
								reportPrefix + seriesName + ".isAxisOnLeft",
								"0"), PropertyFileUtil.getAppProperty(
								reportPrefix + seriesName
										+ ".formatNumberScale", "1"));
			} catch (Exception e) {
				throw new AppException(e.getMessage());
			}
			yAxis.setDataSet(chartXmlAxis.getDataSet());
			axisList.add(yAxis);
		}
		for (String xCoordinateDate : xCoordinates) {
			category = new ChartXmlCategory();
			if (monthCount == 0) {
				category.setLabel("");
			} else {
				category.setLabel(xCoordinateDate);
			}
			categories.add(category);
			monthCount++;
		}

		chartHearder.setCategories(categories);
		chartHearder.setAxis(axisList);

		/* Create JAXBContext reference. */
		JAXBContext jc;
		StringWriter sw = null;

		try {
			jc = JAXBContext.newInstance(new Class[] { ChartXmlHeader.class,
					ChartXmlCategory.class, java.util.ArrayList.class,
					ChartXmlAxis.class, ChartXmlDataSet.class,
					ChartXmlSetValue.class });

			sw = new StringWriter();

			Marshaller m = jc.createMarshaller();
			m.marshal(chartHearder, sw);

		} catch (JAXBException e) {
			throw new SysException(e.getMessage(), e);
		}

		returnGraphXml = sw.toString();

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method createXmlChart for generating xml for MultiAxis Chart ends.");
		}

		return returnGraphXml;
	}

	/**
	 * Gets the closed index value.
	 * 
	 * @param value
	 *            the value
	 * 
	 * @return the closed index value
	 */
	public static Long getClosedIndexValue(long value) {
		Long closedIndexValue = value;
		long closeIndex = 1;

		if (value < 1000) {
			closeIndex = 5;
		} else if (value < 10000) {
			closeIndex = 100;
		} else if (value > 10000) {
			closeIndex = 1000;
		}
		if (value % closeIndex != 0) {
			closedIndexValue = ((value + closeIndex) / closeIndex) * closeIndex;
		}
		return closedIndexValue;

	}
	
	/**
	 * Creates the xml chart for pc comparison.
	 *
	 * @param chartXmlAxisList the chart xml axis list
	 * @param xCoordinates the x coordinates
	 * @param reportPropertyKey the report property key
	 * @param caption the caption
	 * @return the string
	 * @throws Exception the exception
	 */
	public static String createXmlChartForPcComparison(List<ChartXmlAxisPcComp> chartXmlAxisList,
			List<String> xCoordinates, String reportPropertyKey, String caption) throws Exception {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method createXmlChartForPcComparison for generating xml ");
		}

		String returnGraphXml = "";
		String captions = caption;
		ChartXmlHeaderPcComp chartXmlHeaderPcComp=new ChartXmlHeaderPcComp();

		chartXmlHeaderPcComp.setCaption(captions);
		chartXmlHeaderPcComp.setPalette("2");
		chartXmlHeaderPcComp.setxAxisName("Months");
		chartXmlHeaderPcComp.setShowValues("0");
		chartXmlHeaderPcComp.setDivLineAlpha("100");
		chartXmlHeaderPcComp.setNumVDivLines("4");
		chartXmlHeaderPcComp.setvDivLineAlpha("0");
		chartXmlHeaderPcComp.setShowAlternateVGridColor("1");
		chartXmlHeaderPcComp.setAlternateVGridAlpha("5");
		chartXmlHeaderPcComp.setCanvasPadding("0");
		
		List<ChartXmlCategory> categories = new ArrayList<ChartXmlCategory>();
		ChartXmlCategory category = null;
		for (String xCoordinateDate : xCoordinates) {
			category = new ChartXmlCategory();
				category.setLabel(xCoordinateDate);
			categories.add(category);
		
		}

		chartXmlHeaderPcComp.setCategories(categories);
		chartXmlHeaderPcComp.setAxis(chartXmlAxisList);
		
		//generate xml 
		/* Create JAXBContext reference. */
		JAXBContext jc;
		StringWriter sw = null;

		try {
			jc = JAXBContext.newInstance(new Class[] { ChartXmlHeaderPcComp.class,
					ChartXmlCategory.class, java.util.ArrayList.class,
					ChartXmlAxisPcComp.class, ChartXmlDataSet.class,
					ChartXmlSetValue.class });

			sw = new StringWriter();

			Marshaller m = jc.createMarshaller();
			m.marshal(chartXmlHeaderPcComp, sw);

		} catch (JAXBException e) {
			throw new SysException(e.getMessage(), e);
		}

		returnGraphXml = sw.toString();

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method createXmlChartForPcComparison for generating xml for MultiAxis Chart ends.");
		}

		

		return returnGraphXml;
	}
	
	/**
	 * Creates the xml for ticket count.
	 *
	 * @param chartXmlDataSets the chart xml data sets
	 * @param xCoordinates the x coordinates
	 * @return the string
	 * @throws Exception the exception
	 */
	public static String createXmlForTicketCount(List<ChartXmlDataSet>chartXmlDataSets,
			Set<String> xCoordinates) throws Exception {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method createXmlForTicketCount for generating xml ");
		}

		String returnGraphXml = "";
		ChartXmlHeaderTicketCount chartXmlHeaderTicketCount=new ChartXmlHeaderTicketCount();
		chartXmlHeaderTicketCount.setCaption("Worked / Closed Ticket Count");
		chartXmlHeaderTicketCount.setDecimals("0");
		chartXmlHeaderTicketCount.setPlaceValuesInside("1");
		chartXmlHeaderTicketCount.setRotateValues("1");
		chartXmlHeaderTicketCount.setShowLabels("1");
		chartXmlHeaderTicketCount.setShowvalues("1");
		chartXmlHeaderTicketCount.setUseRoundEdges("1");
		chartXmlHeaderTicketCount.setxAxisName("Employee");
		chartXmlHeaderTicketCount.setyAxisName("Ticket Count");
		
		List<ChartXmlCategory> categories = new ArrayList<ChartXmlCategory>();
		ChartXmlCategory category = null;
		for (String xCoordinateDate : xCoordinates) {
			category = new ChartXmlCategory();
				category.setLabel(xCoordinateDate);
			categories.add(category);
		
		}

		chartXmlHeaderTicketCount.setCategory(categories);
		chartXmlHeaderTicketCount.setDataSet(chartXmlDataSets);
		//generate xml 
		/* Create JAXBContext reference. */
		JAXBContext jc;
		StringWriter sw = null;

		try {
			jc = JAXBContext.newInstance(new Class[] { ChartXmlHeaderTicketCount.class,
					ChartXmlCategory.class, java.util.ArrayList.class,
					ChartXmlDataSet.class,
					ChartXmlSetValue.class });

			sw = new StringWriter();

			Marshaller m = jc.createMarshaller();
			m.marshal(chartXmlHeaderTicketCount, sw);

		} catch (JAXBException e) {
			throw new SysException(e.getMessage(), e);
		}

		returnGraphXml = sw.toString();

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method createXmlForTicketCount for generating xml ends.");
		}

		

		return returnGraphXml;
	}
}
