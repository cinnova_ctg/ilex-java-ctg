package com.ilex.reports.common.multiAxis.bean;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class MultiAxisChartResultSetBean {
	private String type;
	private String display_month;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDisplay_month() {
		return display_month;
	}

	public void setDisplay_month(String displayMonth) {
		display_month = displayMonth;
	}

	public String getAvrg_rate() {
		return avrg_rate;
	}

	public void setAvrg_rate(String avrgRate) {
		avrg_rate = avrgRate;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	private String avrg_rate;
	private String month;

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
