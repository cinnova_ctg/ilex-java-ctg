package com.ilex.reports.common.multiAxis.bean;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;

import com.ilex.reports.logic.minutemanSpeedpayReport.MinutemanSpeedpayTotalValueByMonth;
import com.mind.fw.lang.AppException;
import com.mind.fw.lang.SysException;
import com.mind.fw.util.PropertyFileUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class Stacked2DChartOperation.
 */
public class Stacked2DChartOperation {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = Logger
			.getLogger(Stacked2DChartOperation.class);

	/**
	 * Gets the axis list for stacked axis chart used for displaying the
	 * Minuteman SpeedPay Report.
	 * 
	 * @param rsBeanList
	 *            the rs bean list
	 * @param datesQueue
	 *            the dates queue
	 * @param totalCostByMonth
	 *            the total cost by month
	 * @param noOfRecords
	 *            the no of records
	 * @param defaultCount
	 *            the default count
	 * @param reportPropertyKey
	 *            the report property key
	 * @return the axis list for stacked axis chart
	 * @throws AppException
	 *             the app exception
	 */
	public static ArrayList<GraphXmlDataSet> getAxisListForStackedAxisChart(
			List<? extends Stacked2DChartResultSetBean> rsBeanList,
			ArrayList<String> datesQueue,
			List<MinutemanSpeedpayTotalValueByMonth> totalCostByMonth,
			int noOfRecords, int defaultCount, String reportPropertyKey)
			throws AppException {

		ArrayList<GraphXmlDataSet> graphXmlDataList = new ArrayList<GraphXmlDataSet>();
		ArrayList<String> tempXmlDataList = new ArrayList<String>();
		try {
			GraphXmlDataSet graphXmlDataSet = null;
			List<ChartXmlSetValue> dataSetList = null;
			ChartXmlSetValue chartXmlSetValue = null;

			String type = "";
			int minimumCount = 0;
			int count = 1;
			int xmlAxisLength = 0;
			String reportPrefix = PropertyFileUtil
					.getAppProperty(reportPropertyKey + ".prefix");
			for (Stacked2DChartResultSetBean stacked2DChartResultSetBean : rsBeanList) {

				/*
				 * Check the legend value of chart should be equal to previous
				 * legend value and records should not be last.
				 */
				if (type.equals(stacked2DChartResultSetBean
						.getPurchase_order_type()) && count != noOfRecords) {

				} else {
					/*
					 * Check is the record is last record?
					 */
					if (count == noOfRecords) {
						++minimumCount;
						chartXmlSetValue = new ChartXmlSetValue();
						String per = null;
						String dataSetValue = null;
						if (reportPrefix.equals(PropertyFileUtil
								.getAppProperty("USAGE_BY_COST.prefix"))) {
							for (MinutemanSpeedpayTotalValueByMonth totalCostForMonth : totalCostByMonth) {
								if (totalCostForMonth.getDisplay_month()
										.equals(tempXmlDataList.get(0))) {
									dataSetValue = stacked2DChartResultSetBean
											.getCost();
									Integer dataSetVal = (int) Double
											.parseDouble(stacked2DChartResultSetBean
													.getCost()) / 1000;
									String monthTotal = totalCostForMonth
											.getTotalValue();
									Integer percentage1 = (int) (((Double
											.parseDouble(dataSetValue)) / (Double
											.parseDouble(monthTotal))) * 100);
									dataSetValue = dataSetVal.toString();
									per = Integer.toString(percentage1);
									break;
								}
							}
							chartXmlSetValue
									.setValue(stacked2DChartResultSetBean
											.getCost());
							if (dataSetValue.equals("0"))
								chartXmlSetValue.setToolText("PVS:{br}"
										+ " Total: $"
										+ chartXmlSetValue.getValue() + "{br}"
										+ " Percentage: " + per + "%");
							else
								chartXmlSetValue.setToolText("PVS:{br}"
										+ " Total: $" + dataSetValue + "K{br}"
										+ " Percentage: " + per + "%");
						} else {
							for (MinutemanSpeedpayTotalValueByMonth totalCostForMonth : totalCostByMonth) {
								if (totalCostForMonth.getDisplay_month()
										.equals(tempXmlDataList.get(0))) {
									dataSetValue = stacked2DChartResultSetBean
											.getOccurences();
									String monthTotal = totalCostForMonth
											.getTotalValue();
									Integer percentage1 = (int) (((Double
											.parseDouble(dataSetValue)) / (Double
											.parseDouble(monthTotal))) * 100);
									per = Integer.toString(percentage1);
									break;
								}
							}
							chartXmlSetValue
									.setValue(stacked2DChartResultSetBean
											.getOccurences());
							chartXmlSetValue.setToolText("PVS:{br}"
									+ " Total: " + dataSetValue + "{br}"
									+ " Percentage: " + per + "%");
						}
						dataSetList.add(chartXmlSetValue);

					}
					tempXmlDataList.addAll(datesQueue);
					if (graphXmlDataSet != null) {
						if (minimumCount < defaultCount) {
							for (int i = minimumCount; i <= defaultCount; i++) {
								chartXmlSetValue = new ChartXmlSetValue();
								chartXmlSetValue.setValue("0");
								dataSetList.add(chartXmlSetValue);
							}
						}
						graphXmlDataSet.setSet(dataSetList);
						if (graphXmlDataSet.getSeriesName().equals(
								PropertyFileUtil.getAppProperty(reportPrefix
										+ "." + type + ".displayName"))) {
							graphXmlDataSet.setColor(PropertyFileUtil
									.getAppProperty(reportPrefix + "." + type
											+ ".color"));
						} else if (graphXmlDataSet.getSeriesName().equals(
								PropertyFileUtil.getAppProperty(reportPrefix
										+ "." + type + ".displayName"))) {
							graphXmlDataSet.setColor(PropertyFileUtil
									.getAppProperty(reportPrefix + "." + type
											+ ".color"));
						} else {
							graphXmlDataSet.setColor(PropertyFileUtil
									.getAppProperty(reportPrefix + "." + type
											+ ".color"));

						}
						graphXmlDataSet.setShowValues("0");
						graphXmlDataSet.setAlpha("100");
						graphXmlDataList.add(graphXmlDataSet);

					}

					graphXmlDataSet = new GraphXmlDataSet();
					dataSetList = new ArrayList<ChartXmlSetValue>();

					minimumCount = 0;
				}
				/*
				 * Following lines are default operation which would be
				 * performed in every case.
				 */
				xmlAxisLength = tempXmlDataList.size();
				if (xmlAxisLength > 0) {
					for (int i = 0; i <= xmlAxisLength; i++) {
						minimumCount++;
						chartXmlSetValue = new ChartXmlSetValue();
						String value = null;
						String purchaseType = stacked2DChartResultSetBean
								.getPurchase_order_type();
						String dataSetValue = null;
						String monthTotal = null;
						String percentage = null;
						if (stacked2DChartResultSetBean.getDisplay_month()
								.equals(tempXmlDataList.get(0))) {

							if (reportPrefix.equals(PropertyFileUtil
									.getAppProperty("USAGE_BY_COST.prefix"))) {
								chartXmlSetValue
										.setValue(stacked2DChartResultSetBean
												.getCost());

								for (MinutemanSpeedpayTotalValueByMonth totalCostForMonth : totalCostByMonth) {
									if (totalCostForMonth.getDisplay_month()
											.equals(tempXmlDataList.get(0))) {
										dataSetValue = stacked2DChartResultSetBean
												.getCost();
										Integer dataSetVal = (int) Double
												.parseDouble(stacked2DChartResultSetBean
														.getCost()) / 1000;

										monthTotal = totalCostForMonth
												.getTotalValue();
										Integer percentage1 = (int) (((Double
												.parseDouble(dataSetValue)) / (Double
												.parseDouble(monthTotal))) * 100);
										dataSetValue = dataSetVal.toString();
										percentage = Integer
												.toString(percentage1);
										break;
									}
								}

							} else {
								chartXmlSetValue
										.setValue(stacked2DChartResultSetBean
												.getOccurences());
								for (MinutemanSpeedpayTotalValueByMonth totalCostForMonth : totalCostByMonth) {
									if (totalCostForMonth.getDisplay_month()
											.equals(tempXmlDataList.get(0))) {
										dataSetValue = stacked2DChartResultSetBean
												.getOccurences();
										monthTotal = totalCostForMonth
												.getTotalValue();
										Integer percentage1 = (int) (((Double
												.parseDouble(dataSetValue)) / (Double
												.parseDouble(monthTotal))) * 100);
										percentage = Integer
												.toString(percentage1);
										break;
									}
								}

							}
							value = PropertyFileUtil
									.getAppProperty(reportPrefix + "."
											+ purchaseType + ".displayName");
							if (reportPrefix.equals(PropertyFileUtil
									.getAppProperty("USAGE_BY_COST.prefix"))) {
								if (dataSetValue.equals("0"))
									chartXmlSetValue.setToolText(value
											+ ":{br}" + " Total: $"
											+ chartXmlSetValue.getValue()
											+ "{br}" + " Percentage: "
											+ percentage + "%");
								else
									chartXmlSetValue.setToolText(value
											+ ":{br}" + " Total: $"
											+ dataSetValue + "K{br}"
											+ " Percentage: " + percentage
											+ "%");
							} else
								chartXmlSetValue.setToolText(value + ":{br}"
										+ " Total: " + dataSetValue + "{br}"
										+ " Percentage: " + percentage + "%");
							dataSetList.add(chartXmlSetValue);
							tempXmlDataList.remove(0);
							break;
						} else {

							value = PropertyFileUtil
									.getAppProperty(reportPrefix + "."
											+ purchaseType + ".displayName");

							chartXmlSetValue.setValue("0");
							chartXmlSetValue.setToolText(value + ":{br}"
									+ " Total: $" + chartXmlSetValue.getValue()
									+ "{br}" + " Percentage: 0%");
							dataSetList.add(chartXmlSetValue);
							tempXmlDataList.remove(0);
							xmlAxisLength = tempXmlDataList.size();
							i = 0;
						}
					}
				}

				graphXmlDataSet.setSeriesName(PropertyFileUtil
						.getAppProperty(reportPrefix
								+ "."
								+ stacked2DChartResultSetBean
										.getPurchase_order_type()
								+ ".displayName"));

				type = stacked2DChartResultSetBean.getPurchase_order_type();
				count++;
			}
		} catch (Exception e) {
			throw new AppException(e.getMessage());
		} finally {
		}
		return graphXmlDataList;

	}

	/*
	 * This Method is used to generate the Data for creating the Stack for MM
	 * Utilization AvailabilityReport
	 */
	public static ArrayList<GraphXmlDataSet> getAxisListForStackedSimpleChart(
			List<? extends Stacked2DChartResultSetBean> rsBeanList,
			ArrayList<String> datesQueue, int noOfRecords, int defaultCount,
			String reportPropertyKey) throws AppException {

		ArrayList<GraphXmlDataSet> graphXmlDataList = new ArrayList<GraphXmlDataSet>();
		ArrayList<String> tempXmlDataList = new ArrayList<String>();
		try {
			GraphXmlDataSet graphXmlDataSet = null;
			List<ChartXmlSetValue> dataSetList = null;
			ChartXmlSetValue chartXmlSetValue = null;

			String type = "";
			int minimumCount = 0;
			int count = 1;
			int xmlAxisLength = 0;
			String reportPrefix = PropertyFileUtil
					.getAppProperty(reportPropertyKey + ".prefix");
			for (Stacked2DChartResultSetBean stacked2DChartResultSetBean : rsBeanList) {

				/*
				 * Check the legend value of chart should be equal to previous
				 * legend value and records should not be last.
				 */
				if (type.equals(stacked2DChartResultSetBean.getPartner_type())
						&& count != noOfRecords) {

				} else {
					/*
					 * Check is the record is last record?
					 */
					if (count == noOfRecords) {
						++minimumCount;
						chartXmlSetValue = new ChartXmlSetValue();
						String dataSetValue = null;
						if (stacked2DChartResultSetBean.getDisplay_month()
								.equals(tempXmlDataList.get(0))) {
							dataSetValue = stacked2DChartResultSetBean
									.getTotalMinuteman();
							// break;
						}
						chartXmlSetValue.setValue(stacked2DChartResultSetBean
								.getTotalMinuteman());
						dataSetList.add(chartXmlSetValue);

					}
					tempXmlDataList.addAll(datesQueue);
					if (graphXmlDataSet != null) {
						if (minimumCount < defaultCount) {
							for (int i = minimumCount; i <= defaultCount; i++) {
								chartXmlSetValue = new ChartXmlSetValue();
								chartXmlSetValue.setValue("0");
								dataSetList.add(chartXmlSetValue);
							}
						}
						graphXmlDataSet.setSet(dataSetList);
						if ((PropertyFileUtil.getAppProperty(reportPrefix + "."
								+ type + ".displayName"))
								.equals(graphXmlDataSet.getSeriesName())) {
							graphXmlDataSet.setColor(PropertyFileUtil
									.getAppProperty(reportPrefix + "." + type
											+ ".color"));
						}
						graphXmlDataSet.setShowValues("0");
						graphXmlDataSet.setAlpha("100");
						graphXmlDataList.add(graphXmlDataSet);

					}

					graphXmlDataSet = new GraphXmlDataSet();
					dataSetList = new ArrayList<ChartXmlSetValue>();

					minimumCount = 0;
				}
				/*
				 * Following lines are default operation which would be
				 * performed in every case.
				 */
				xmlAxisLength = tempXmlDataList.size();
				if (xmlAxisLength > 0) {
					for (int i = 0; i <= xmlAxisLength; i++) {
						minimumCount++;
						chartXmlSetValue = new ChartXmlSetValue();
						if (stacked2DChartResultSetBean.getDisplay_month()
								.equals(tempXmlDataList.get(0))) {

							chartXmlSetValue
									.setValue(stacked2DChartResultSetBean
											.getTotalMinuteman());
							dataSetList.add(chartXmlSetValue);
							tempXmlDataList.remove(0);
							break;
						} else {
							chartXmlSetValue.setValue("0");
							dataSetList.add(chartXmlSetValue);
							tempXmlDataList.remove(0);
							xmlAxisLength = tempXmlDataList.size();
							i = 0;
						}
					}
				}
				graphXmlDataSet.setSeriesName(PropertyFileUtil
						.getAppProperty(reportPrefix + "."
								+ stacked2DChartResultSetBean.getPartner_type()
								+ ".displayName"));
				type = stacked2DChartResultSetBean.getPartner_type();
				count++;
			}
		} catch (Exception e) {
			throw new AppException(e.getMessage());
		} finally {
		}
		return graphXmlDataList;

	}

	/**
	 * Creates the xml graph.
	 * 
	 * @param graphXmlDataList
	 *            the graph xml data list
	 * @param xCoordinates
	 *            the x coordinates
	 * @param reportPropertyKey
	 *            the report property key
	 * @param maxYAxisValue
	 *            the max y axis value
	 * @param caption
	 *            the caption
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public static String createXmlGraph(List<GraphXmlDataSet> graphXmlDataList,
			List<String> xCoordinates, String reportPropertyKey,
			Long maxYAxisValue, String caption) throws Exception {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method createXmlChart for generating xml for Stacked2D Chart starts.");
		}
		String reportPrefix = PropertyFileUtil.getAppProperty(reportPropertyKey
				+ ".prefix");
		String returnGraphXml = "";
		int monthCount = 0;
		/* Create Graph (Graph header) node and set its attributes */
		GraphXmlHeader graphHeader = new GraphXmlHeader();
		// Setting the maximum value for the Y axis
		if (PropertyFileUtil.getAppProperty("USAGE_BY_COST.prefix").equals(
				reportPrefix))
			graphHeader.setyAxisMaxValue(maxYAxisValue + 50000 + "");
		else if (PropertyFileUtil.getAppProperty(
				"AVAILABILITY_RECRUITING.prefix").equals(reportPrefix)) {
			graphHeader.setyAxisMaxValue(maxYAxisValue + 50 + "");
		} else
			graphHeader.setyAxisMaxValue(maxYAxisValue + 1050 + "");

		graphHeader.setCaption(PropertyFileUtil.getAppProperty(reportPrefix
				+ ".caption"));
		graphHeader.setNumberPrefix(PropertyFileUtil
				.getAppProperty(reportPrefix + ".numberPrefix"));
		graphHeader.setXaxisname(" Trailing 12 months");
		graphHeader.setYaxisname("");
		graphHeader.setUseRoundEdges("1");
		graphHeader.setNumdivLines("5");
		graphHeader.setDivLineColor("333333");
		graphHeader.setShowSum("1");
		graphHeader.setDecimalPrecision("0");
		graphHeader.setFormatNumberScale("0");
		/* Setting the value for the categories element of the GraphHeader */
		GraphXmlCatageries categories = new GraphXmlCatageries();
		List<ChartXmlCategory> categorylist = new ArrayList<ChartXmlCategory>();

		for (String xCoordinateDate : xCoordinates) {
			ChartXmlCategory category = new ChartXmlCategory();
			category.setLabel(xCoordinateDate);
			categorylist.add(category);
			monthCount++;
		}

		categories.setCategory(categorylist);
		graphHeader.setCategory(categories);
		/* Setting the value for the DataList element of the GraphHeader */
		graphHeader.setDataset(graphXmlDataList);
		/* Create JAXBContext reference. */
		JAXBContext jc;
		StringWriter sw = null;

		try {
			jc = JAXBContext.newInstance(new Class[] { GraphXmlHeader.class,
					GraphXmlCatageries.class, ChartXmlCategory.class,
					java.util.ArrayList.class, GraphXmlDataSet.class,
					ChartXmlSetValue.class });

			sw = new StringWriter();

			Marshaller m = jc.createMarshaller();
			m.marshal(graphHeader, sw);

		} catch (JAXBException e) {
			throw new SysException(e.getMessage(), e);
		}

		returnGraphXml = sw.toString();

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Method createXmlChart for generating xml for Stacked2D Chart ends.");
		}

		return returnGraphXml;
	}
}
