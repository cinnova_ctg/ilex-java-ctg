package com.ilex.reports.util;

import java.sql.Date;

/**
 * The Class DateUtils.
 */
public class DateUtils {

	/**
	 * Convert string to sql date.
	 * 
	 * @param dateStr
	 *            the date string
	 * @return the java.sql.Date
	 */
	public static java.sql.Date convertStringToSqlDate(String dateStr) {
		return Date.valueOf(dateStr.substring(0, 10));
	}

}
