package com.ilex.reports.bean.minutemanSpeedpayReport;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.ilex.reports.common.LabelValue;

public class MinutemanSpeedpayCostBean {
	private ArrayList<String> xCordinateDates = null;
	private String graphXML = null;
	private String projectCategoryFilter = null;
	private ArrayList<LabelValue> projectCategoryList = null;
	private String startDate = null;
	private String endDate = null;
	private String reportSubTitle = "";

	private String laborType = null;
	private ArrayList<LabelValue> laborTypeList = null;

	public String getLaborType() {
		return laborType;
	}

	public void setLaborType(String laborType) {
		this.laborType = laborType;
	}

	public ArrayList<LabelValue> getLaborTypeList() {
		return laborTypeList;
	}

	public void setLaborTypeList(ArrayList<LabelValue> laborTypeList) {
		this.laborTypeList = laborTypeList;
	}

	public ArrayList<String> getxCordinateDates() {
		return xCordinateDates;
	}

	public void setxCordinateDates(ArrayList<String> xCordinateDates) {
		this.xCordinateDates = xCordinateDates;
	}

	public String getGraphXML() {
		return graphXML;
	}

	public void setGraphXML(String graphXML) {
		this.graphXML = graphXML;
	}

	public String getProjectCategoryFilter() {
		return projectCategoryFilter;
	}

	public void setProjectCategoryFilter(String projectCategoryFilter) {
		this.projectCategoryFilter = projectCategoryFilter;
	}

	public ArrayList<LabelValue> getProjectCategoryList() {
		return projectCategoryList;
	}

	public void setProjectCategoryList(ArrayList<LabelValue> projectCategoryList) {
		this.projectCategoryList = projectCategoryList;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getReportSubTitle() {
		return reportSubTitle;
	}

	public void setReportSubTitle(String reportSubTitle) {
		this.reportSubTitle = reportSubTitle;
	}

	public void setProjectType(String projectType) {
		// TODO Auto-generated method stub

	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
