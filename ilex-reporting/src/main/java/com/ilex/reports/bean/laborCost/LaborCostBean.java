package com.ilex.reports.bean.laborCost;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.ilex.reports.common.LabelValue;
import com.ilex.reports.common.multiAxis.bean.ChartXmlAxis;

public class LaborCostBean {
	private ArrayList<String> xCordinateDates = null;
	private String yCordinateLabor = null;
	private String yCordinateTechnician = null;
	private String yCordinateEngineer = null;
	private String graphXML = null;
	private String laborCategoryFilter = null;
	private ArrayList<LabelValue> laborCategoryList = new ArrayList<LabelValue>();

	private String startDate = null;
	private String endDate = null;
	private String reportSubTitle = "";

	private ArrayList<ChartXmlAxis> axisList = null;

	private String laborType = null;
	private ArrayList<LabelValue> laborTypeList = null;

	public String getLaborType() {
		return laborType;
	}

	public void setLaborType(String laborType) {
		this.laborType = laborType;
	}

	public ArrayList<LabelValue> getLaborTypeList() {
		return laborTypeList;
	}

	public void setLaborTypeList(ArrayList<LabelValue> laborTypeList) {
		this.laborTypeList = laborTypeList;
	}

	public ArrayList<String> getxCordinateDates() {
		return xCordinateDates;
	}

	public void setxCordinateDates(ArrayList<String> xCordinateDates) {
		this.xCordinateDates = xCordinateDates;
	}

	public String getyCordinateLabor() {
		return yCordinateLabor;
	}

	public void setyCordinateLabor(String yCordinateLabor) {
		this.yCordinateLabor = yCordinateLabor;
	}

	public String getyCordinateTechnician() {
		return yCordinateTechnician;
	}

	public void setyCordinateTechnician(String yCordinateTechnician) {
		this.yCordinateTechnician = yCordinateTechnician;
	}

	public String getyCordinateEngineer() {
		return yCordinateEngineer;
	}

	public void setyCordinateEngineer(String yCordinateEngineer) {
		this.yCordinateEngineer = yCordinateEngineer;
	}

	public String getGraphXML() {
		return graphXML;
	}

	public void setGraphXML(String graphXML) {
		this.graphXML = graphXML;
	}

	public String getLaborCategoryFilter() {
		return laborCategoryFilter;
	}

	public void setLaborCategoryFilter(String laborCategoryFilter) {
		this.laborCategoryFilter = laborCategoryFilter;
	}

	public ArrayList<LabelValue> getLaborCategoryList() {
		return laborCategoryList;
	}

	public void setLaborCategoryList(ArrayList<LabelValue> laborCategoryList) {
		this.laborCategoryList = laborCategoryList;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public ArrayList<ChartXmlAxis> getAxisList() {
		return axisList;
	}

	public void setAxisList(ArrayList<ChartXmlAxis> axisList) {
		this.axisList = axisList;
	}

	public String getReportSubTitle() {
		return reportSubTitle;
	}

	public void setReportSubTitle(String reportSubTitle) {
		this.reportSubTitle = reportSubTitle;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
