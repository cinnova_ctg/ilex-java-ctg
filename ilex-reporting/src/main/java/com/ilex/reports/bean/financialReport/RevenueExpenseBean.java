package com.ilex.reports.bean.financialReport;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.ilex.reports.common.multiAxis.bean.MultiAxisChartBean;
import com.ilex.reports.logic.financialReport.BussinessDevlopmentManagerResultSetBean;
import com.ilex.reports.logic.financialReport.CustomerResultSetBean;
import com.ilex.reports.logic.financialReport.JobOwnerResultSetBean;
import com.ilex.reports.logic.financialReport.JobOwnerTableResultSetBean;
import com.ilex.reports.logic.financialReport.ProjectManagerResultSetBean;
import com.ilex.reports.logic.financialReport.RevenueExpenceTableResultSetBean;
import com.ilex.reports.logic.financialReport.SeniorProjectManagerResultSetBean;

public class RevenueExpenseBean extends MultiAxisChartBean {
	private List<? extends JobOwnerResultSetBean> jobOwnerList = null;
	private List<? extends ProjectManagerResultSetBean> projectManagerList = null;
	private List<RevenueExpenceTableResultSetBean> revenueExpenceList = null;
	private List<SeniorProjectManagerResultSetBean> seniorProjectManagersList = null;
	private List<BussinessDevlopmentManagerResultSetBean> bdmList = null;
	private List<CustomerResultSetBean> customerList = null;

	public List<? extends ProjectManagerResultSetBean> getProjectManagerList() {
		return projectManagerList;
	}

	public void setProjectManagerList(
			List<? extends ProjectManagerResultSetBean> projectManagerList) {
		this.projectManagerList = projectManagerList;
	}

	public List<BussinessDevlopmentManagerResultSetBean> getBdmList() {
		return bdmList;
	}

	public List<CustomerResultSetBean> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<CustomerResultSetBean> customerList) {
		this.customerList = customerList;
	}

	public void setBdmList(List<BussinessDevlopmentManagerResultSetBean> bdmList) {
		this.bdmList = bdmList;
	}

	public List<SeniorProjectManagerResultSetBean> getSeniorProjectManagersList() {
		return seniorProjectManagersList;
	}

	public void setSeniorProjectManagersList(
			List<SeniorProjectManagerResultSetBean> seniorProjectManagersList) {
		this.seniorProjectManagersList = seniorProjectManagersList;
	}

	public List<RevenueExpenceTableResultSetBean> getRevenueExpenceList() {
		return revenueExpenceList;
	}

	public void setRevenueExpenceList(
			List<RevenueExpenceTableResultSetBean> revenueExpenceList) {
		this.revenueExpenceList = revenueExpenceList;
	}

	public List<? extends JobOwnerResultSetBean> getJobOwnerList() {
		return jobOwnerList;
	}

	public void setJobOwnerList(
			List<? extends JobOwnerResultSetBean> jobOwnerList) {
		this.jobOwnerList = jobOwnerList;
	}

	public List<JobOwnerTableResultSetBean> getDetailedList() {
		return detailedList;
	}

	public void setDetailedList(List<JobOwnerTableResultSetBean> detailedList) {
		this.detailedList = detailedList;
	}

	private List<JobOwnerTableResultSetBean> detailedList = null;

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
