package com.ilex.reports.bean.laborCost;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class LaborCostSrPmBean {

	private String xCordinateDate = null;
	private String yCordinateLabor = null;
	private String yCordinateTechnician = null;

	private String yCordinatePM1 = null;
	private String yCordinatePM2 = null;
	private String yCordinatePM3 = null;
	private String yCordinatePM4 = null;
	private String yCordinatePM5 = null;

	private String namePM1 = null;
	private String namePM2 = null;
	private String namePM3 = null;
	private String namePM4 = null;
	private String namePM5 = null;

	private String graphXML = null;
	private String startDate = null;
	private String endDate = null;

	public String getxCordinateDate() {
		return xCordinateDate;
	}

	public void setxCordinateDate(String xCordinateDate) {
		this.xCordinateDate = xCordinateDate;
	}

	public String getyCordinateLabor() {
		return yCordinateLabor;
	}

	public void setyCordinateLabor(String yCordinateLabor) {
		this.yCordinateLabor = yCordinateLabor;
	}

	public String getyCordinateTechnician() {
		return yCordinateTechnician;
	}

	public void setyCordinateTechnician(String yCordinateTechnician) {
		this.yCordinateTechnician = yCordinateTechnician;
	}

	public String getyCordinatePM1() {
		return yCordinatePM1;
	}

	public void setyCordinatePM1(String yCordinatePM1) {
		this.yCordinatePM1 = yCordinatePM1;
	}

	public String getyCordinatePM2() {
		return yCordinatePM2;
	}

	public void setyCordinatePM2(String yCordinatePM2) {
		this.yCordinatePM2 = yCordinatePM2;
	}

	public String getyCordinatePM3() {
		return yCordinatePM3;
	}

	public void setyCordinatePM3(String yCordinatePM3) {
		this.yCordinatePM3 = yCordinatePM3;
	}

	public String getyCordinatePM4() {
		return yCordinatePM4;
	}

	public void setyCordinatePM4(String yCordinatePM4) {
		this.yCordinatePM4 = yCordinatePM4;
	}

	public String getyCordinatePM5() {
		return yCordinatePM5;
	}

	public void setyCordinatePM5(String yCordinatePM5) {
		this.yCordinatePM5 = yCordinatePM5;
	}

	public String getNamePM1() {
		return namePM1;
	}

	public void setNamePM1(String namePM1) {
		this.namePM1 = namePM1;
	}

	public String getNamePM2() {
		return namePM2;
	}

	public void setNamePM2(String namePM2) {
		this.namePM2 = namePM2;
	}

	public String getNamePM3() {
		return namePM3;
	}

	public void setNamePM3(String namePM3) {
		this.namePM3 = namePM3;
	}

	public String getNamePM4() {
		return namePM4;
	}

	public void setNamePM4(String namePM4) {
		this.namePM4 = namePM4;
	}

	public String getNamePM5() {
		return namePM5;
	}

	public void setNamePM5(String namePM5) {
		this.namePM5 = namePM5;
	}

	public String getGraphXML() {
		return graphXML;
	}

	public void setGraphXML(String graphXML) {
		this.graphXML = graphXML;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
