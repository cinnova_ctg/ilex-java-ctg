package com.ilex.reports.bean.laborCost;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.ilex.reports.common.LabelValue;

public class LaborCostByLocationBean {
	private String location = null;
	private String laborRate = null;
	private String numberCount = null;
	private String lastDate = null;
	private String viewFilter = null;
	private ArrayList<LabelValue> viewFilterList = null;
	private ArrayList<LaborCostByLocationBean> lcByLocationList = null;
	private String startMonth;
	private String startYear;
	private String endYear;
	private String endMonth;

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public String getStartYear() {
		return startYear;
	}

	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	public String getEndYear() {
		return endYear;
	}

	public void setEndYear(String endYear) {
		this.endYear = endYear;
	}

	public String getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}

	private String orderBy = null;
	private String reportTitle = null;

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLaborRate() {
		return laborRate;
	}

	public void setLaborRate(String laborRate) {
		this.laborRate = laborRate;
	}

	public String getNumberCount() {
		return numberCount;
	}

	public void setNumberCount(String numberCount) {
		this.numberCount = numberCount;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}

	public String getViewFilter() {
		return viewFilter;
	}

	public void setViewFilter(String viewFilter) {
		this.viewFilter = viewFilter;
	}

	public ArrayList<LabelValue> getViewFilterList() {
		return viewFilterList;
	}

	public void setViewFilterList(ArrayList<LabelValue> viewFilterList) {
		this.viewFilterList = viewFilterList;
	}

	public ArrayList<LaborCostByLocationBean> getLcByLocationList() {
		return lcByLocationList;
	}

	public void setLcByLocationList(
			ArrayList<LaborCostByLocationBean> lcByLocationList) {
		this.lcByLocationList = lcByLocationList;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
