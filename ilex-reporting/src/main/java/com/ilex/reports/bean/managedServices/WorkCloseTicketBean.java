package com.ilex.reports.bean.managedServices;

import java.io.Serializable;

public class WorkCloseTicketBean  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String date;
	private String name;
	private String installationNotes;
	private String ticketClosed;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInstallationNotes() {
		return installationNotes;
	}
	public void setInstallationNotes(String installationNotes) {
		this.installationNotes = installationNotes;
	}
	public String getTicketClosed() {
		return ticketClosed;
	}
	public void setTicketClosed(String ticketClosed) {
		this.ticketClosed = ticketClosed;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	

}
