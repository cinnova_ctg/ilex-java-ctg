package com.ilex.reports.bean.pvsUsage;

public class PVSCoutnBean {
	private int justifcationId;
	private String justifcationName;
	private int pmId;
	private String pmName;
	private int days_7_count =0;
	private int days_30_count=0;
	private int days_60_count=0;
	private int days_90_count=0;
	private int yearToDate_count=0;
	private int totalCount=0;
	private int days_7_count_all_jobs =0;
	private int days_30_count_all_jobs=0;
	private int days_60_count_all_jobs=0;
	private int days_90_count_all_jobs=0;
	private int yearToDate_count_all_jobs=0;
	
	public int getJustifcationId() {
		return justifcationId;
	}
	public void setJustifcationId(int justifcationId) {
		this.justifcationId = justifcationId;
	}
	public String getJustifcationName() {
		return justifcationName;
	}
	public void setJustifcationName(String justifcationName) {
		this.justifcationName = justifcationName;
	}
	
	public int getDays_7_count() {
		return days_7_count;
	}
	public void setDays_7_count(int days_7_count) {
		this.days_7_count = days_7_count;
	}
	public int getDays_30_count() {
		return days_30_count;
	}
	public void setDays_30_count(int days_30_count) {
		this.days_30_count = days_30_count;
	}
	public int getDays_60_count() {
		return days_60_count;
	}
	public void setDays_60_count(int days_60_count) {
		this.days_60_count = days_60_count;
	}
	public int getDays_90_count() {
		return days_90_count;
	}
	public void setDays_90_count(int days_90_count) {
		this.days_90_count = days_90_count;
	}
	public int getYearToDate_count() {
		return yearToDate_count;
	}
	public void setYearToDate_count(int yearToDate_count) {
		this.yearToDate_count = yearToDate_count;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getPmId() {
		return pmId;
	}
	public void setPmId(int pmId) {
		this.pmId = pmId;
	}
	public String getPmName() {
		return pmName;
	}
	public void setPmName(String pmName) {
		this.pmName = pmName;
	}
	public int getDays_7_count_all_jobs() {
		return days_7_count_all_jobs;
	}
	public void setDays_7_count_all_jobs(int days_7_count_all_jobs) {
		this.days_7_count_all_jobs = days_7_count_all_jobs;
	}
	public int getDays_30_count_all_jobs() {
		return days_30_count_all_jobs;
	}
	public void setDays_30_count_all_jobs(int days_30_count_all_jobs) {
		this.days_30_count_all_jobs = days_30_count_all_jobs;
	}
	public int getDays_60_count_all_jobs() {
		return days_60_count_all_jobs;
	}
	public void setDays_60_count_all_jobs(int days_60_count_all_jobs) {
		this.days_60_count_all_jobs = days_60_count_all_jobs;
	}
	public int getDays_90_count_all_jobs() {
		return days_90_count_all_jobs;
	}
	public void setDays_90_count_all_jobs(int days_90_count_all_jobs) {
		this.days_90_count_all_jobs = days_90_count_all_jobs;
	}
	public int getYearToDate_count_all_jobs() {
		return yearToDate_count_all_jobs;
	}
	public void setYearToDate_count_all_jobs(int yearToDate_count_all_jobs) {
		this.yearToDate_count_all_jobs = yearToDate_count_all_jobs;
	}
	
	
}
