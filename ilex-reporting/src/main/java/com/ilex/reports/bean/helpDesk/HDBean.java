package com.ilex.reports.bean.helpDesk;

import java.io.Serializable;

public class HDBean implements Serializable {

	private String ticketCount;
	private String nextAction;
	private String rootCause;
	private String configItem;
	private String category;
	private String urgency;
	private String clientName;
	private String name;
	private String ticketid;
	private String updatecount;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name==null){
			this.name="";
			
		}
		else
		this.name = name;

		
		}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getTicketCount() {
		return ticketCount;
	}

	public void setTicketCount(String ticketCount) {
		this.ticketCount = ticketCount;
	}

	public String getNextAction() {
		return nextAction;
	}

	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}

	public String getRootCause() {
		return rootCause;
	}

	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	public String getConfigItem() {
		return configItem;
	}

	public void setConfigItem(String configItem) {
		this.configItem = configItem;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public String getTicketid() {
		return ticketid;
	}

	public void setTicketid(String ticketid) {
		this.ticketid = ticketid;
	}

	public String getUpdatecount() {
		return updatecount;
	}

	public void setUpdatecount(String updatecount) {
		this.updatecount = updatecount;
	}

}
