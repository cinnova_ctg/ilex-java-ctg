package com.ilex.reports.bean.managedServices;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.ilex.reports.common.multiAxis.bean.ChartXmlDataSet;

public class TicketCountBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String graphXML;
	private Set<String> xCordinates;
	private List<ChartXmlDataSet>chartXmlDataSets;
	public String getGraphXML() {
		return graphXML;
	}
	public void setGraphXML(String graphXML) {
		this.graphXML = graphXML;
	}

	public List<ChartXmlDataSet> getChartXmlDataSets() {
		return chartXmlDataSets;
	}
	public void setChartXmlDataSets(List<ChartXmlDataSet> chartXmlDataSets) {
		this.chartXmlDataSets = chartXmlDataSets;
	}
	public Set<String> getxCordinates() {
		return xCordinates;
	}
	public void setxCordinates(Set<String> xCordinates) {
		this.xCordinates = xCordinates;
	}

}
