package com.ilex.reports.bean.minutemanUtilization;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.ilex.reports.logic.minutemanUtilization.MinutemanUtilizationResultSetBean;

public class MinutemanUtilizationBean {
	private List<MinutemanUtilizationResultSetBean> firstUsageList = null;
	private ArrayList<String> xCordinateDates = null;
	private String graphXML = null;
	private String startDate = null;
	private String endDate = null;
	private String reportSubTitle = "";

	public String getTotalMM() {
		return totalMM;
	}

	public ArrayList<String> getxCordinateDates() {
		return xCordinateDates;
	}

	public void setxCordinateDates(ArrayList<String> xCordinateDates) {
		this.xCordinateDates = xCordinateDates;
	}

	public String getGraphXML() {
		return graphXML;
	}

	public void setGraphXML(String graphXML) {
		this.graphXML = graphXML;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getReportSubTitle() {
		return reportSubTitle;
	}

	public void setReportSubTitle(String reportSubTitle) {
		this.reportSubTitle = reportSubTitle;
	}

	public void setTotalMM(String totalMM) {
		this.totalMM = totalMM;
	}

	public String getUsedMM() {
		return usedMM;
	}

	public void setUsedMM(String usedMM) {
		this.usedMM = usedMM;
	}

	public String getUnUsedMM() {
		return unUsedMM;
	}

	public void setUnUsedMM(String unUsedMM) {
		this.unUsedMM = unUsedMM;
	}

	private String totalMM;
	private String usedMM;
	private String unUsedMM;

	public List<MinutemanUtilizationResultSetBean> getFirstUsageList() {
		return firstUsageList;
	}

	public void setFirstUsageList(
			List<MinutemanUtilizationResultSetBean> firstUsageList) {
		this.firstUsageList = firstUsageList;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}
}
